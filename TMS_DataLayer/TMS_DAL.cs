﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using TMS_BusinessObjects;

namespace TMS_DataLayer
{
    public class TMS_DAL
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
        public DataTable GetCompletedandPendingCountDal(int Year, int EmpID, int DeptID = 0)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetCompletedAndPendingCountForJRandTarget]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramYear = new SqlParameter("@Year", SqlDbType.Int);
                cmd.Parameters.Add(paramYear).Value = Year;
                SqlParameter paramempID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = EmpID;
                SqlParameter paramDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetCompletedandPendingCountForATCDal(int Year, int EmpID, int DeptID = 0)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ToGetCompletedAndPendingCountBasedOnMonthForATC]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramYear = new SqlParameter("@Year", SqlDbType.Int);
                cmd.Parameters.Add(paramYear).Value = Year;
                SqlParameter paramempID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = EmpID;
                SqlParameter paramDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetOverDueCountBalDal(int EmpID, int DeptID = 0)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ToGetOverDueCountForJRandTarget]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramempID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = EmpID;
                SqlParameter paramDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetTraineeOverDueCountDal(int EmpID, int DeptID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ToGetTraineeOverDueCountForJRandTarget]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramempID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = EmpID;
                SqlParameter paramDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetTraineePendingCountDal(int EmpID, int Year, int DeptID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ToGetTraineePendingCountForJRandTarget]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramempID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = EmpID;
                SqlParameter paramYear = new SqlParameter("@Year", SqlDbType.Int);
                cmd.Parameters.Add(paramYear).Value = Year;
                SqlParameter paramDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetClassroomTrainingCountDal(int EmpID, string TargetType)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ToGetIsClassroomTrainingCount]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramempID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = EmpID;
                SqlParameter paramTargetType = new SqlParameter("@TargetType", SqlDbType.VarChar);
                cmd.Parameters.Add(paramTargetType).Value = TargetType;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Jquery datatables
        public DataTable GetJR_LISTDb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch,
            string sSearch_TraineeName, string sSearch_JR_Name, string sSearch_JR_Version,
          string sSearch_HOD_Name, string sSearch_DepartmentName, string sSearch_DesignationName, string sSearch_StatusName,
            int Year, int empID, int DeptID, int FilterType, string Status, out int TotalCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetEmployeeJR_List]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;

                SqlParameter paramsSearch_TraineeName = new SqlParameter("@sSearch_TraineeName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TraineeName).Value = sSearch_TraineeName;

                SqlParameter paramsSearch_JR_Name = new SqlParameter("@sSearch_JR_Name", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_JR_Name).Value = sSearch_JR_Name;

                SqlParameter paramsSearch_JR_Version = new SqlParameter("@sSearch_JR_Version", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_JR_Version).Value = sSearch_JR_Version;

                SqlParameter paramsSearch_HOD_Name = new SqlParameter("@sSearch_HOD_Name", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_HOD_Name).Value = sSearch_HOD_Name;

                SqlParameter paramsSearch_DepartmentName = new SqlParameter("@sSearch_DepartmentName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_DepartmentName).Value = sSearch_DepartmentName;

                SqlParameter paramsSearch_DesignationName = new SqlParameter("@sSearch_DesignationName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_DesignationName).Value = sSearch_DesignationName;

                SqlParameter paramsSearch_StatusName = new SqlParameter("@sSearch_StatusName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_StatusName).Value = sSearch_StatusName;


                SqlParameter paramYear = new SqlParameter("@Year", SqlDbType.Int);
                cmd.Parameters.Add(paramYear).Value = Year;
                SqlParameter paramempID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = empID;
                SqlParameter paramDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlParameter paramStatus = new SqlParameter("@Status", SqlDbType.VarChar);
                cmd.Parameters.Add(paramStatus).Value = Status;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                TotalCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetTQ_DocsDb(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int trainerEmpID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetDocumentWithTraineeQuestionsForTrainer]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramTrainerEmpID = new SqlParameter("@TrainerEmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramTrainerEmpID).Value = trainerEmpID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = displayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = displayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = sortCol;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetTrainingFileDAL(int jRID)
        {

            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTrainingFilePaths]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramjRID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramjRID).Value = jRID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To get Trainer Answers
        public DataTable GetTA_DocsDb(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int trainerEmpID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetDocumentWithTrainerAnswersForTrainee]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramTrainerEmpID = new SqlParameter("@TraineeEmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramTrainerEmpID).Value = trainerEmpID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = displayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = displayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = sortCol;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetTQ_PreviousAnswersDB(int TQ_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ToGetPreviousAnswers]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmTQ_ID = new SqlParameter("@TraineeQuestionID", SqlDbType.Int);
                cmd.Parameters.Add(spmTQ_ID).Value = TQ_ID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetTQ_ConversationDb(int iTQ_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTQ_Conversation]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmTQ_ID = new SqlParameter("@TQ_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmTQ_ID).Value = iTQ_ID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetHodJrEvaluationDetailsDb(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, string sSearch_JR_Name,string sSearch_JR_Version,
            string sSearch_TraineeName, string sSearch_TraineeDeptName, string sSearch_JR_AssignedBy, string sSearch_JR_AssignedDate, int iHodEmpID, int NotificationID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJrDetailsForHodEvaluation]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramHodEmpID = new SqlParameter("@HodEmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramHodEmpID).Value = iHodEmpID;
                SqlParameter paramNotificationID = new SqlParameter("@NotificationID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = displayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = displayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = sortCol;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;

                SqlParameter paramJR_Name = new SqlParameter("@sSearch_JR_Name", SqlDbType.VarChar);
                cmd.Parameters.Add(paramJR_Name).Value = sSearch_JR_Name;
                SqlParameter paramJR_Version = new SqlParameter("@sSearch_JR_Version", SqlDbType.VarChar);
                cmd.Parameters.Add(paramJR_Version).Value = sSearch_JR_Version;
                SqlParameter paramTraineeName = new SqlParameter("@sSearch_TraineeName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramTraineeName).Value = sSearch_TraineeName;
                SqlParameter paramTraineeDeptName = new SqlParameter("@sSearch_TraineeDeptName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramTraineeDeptName).Value = sSearch_TraineeDeptName;
                SqlParameter paramJR_AssignedBy = new SqlParameter("@sSearch_JR_AssignedBy", SqlDbType.VarChar);
                cmd.Parameters.Add(paramJR_AssignedBy).Value = sSearch_JR_AssignedBy;
                SqlParameter paramJR_AssignedDate = new SqlParameter("@sSearch_JR_AssignedDate", SqlDbType.VarChar);
                cmd.Parameters.Add(paramJR_AssignedDate).Value = sSearch_JR_AssignedDate;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetReadDurationDal(int itTS_ID, int iempID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TTS_GetReadDocDuration]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramitTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramitTS_ID).Value = itTS_ID;
                SqlParameter paramiempID = new SqlParameter("@TraineeID", SqlDbType.Int);
                cmd.Parameters.Add(paramiempID).Value = iempID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Incident in General
        public DataTable GetIncidentInGeneralDal(int TTS_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetIncidentRecord_InGeneral]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramitTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramitTS_ID).Value = TTS_ID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        //Get JR TrainingList 
        public DataTable GetJR_TrainingListDb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int empID, int FilterType, int NotificationID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJR_TrainingList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = empID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramNotificationID = new SqlParameter("@NotificationID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //JR Training Regular
        public DataTable GetJR_TrainingRegularDb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int empID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJR_RegularTrainingDetails_List]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramempID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = empID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //JR Training Retrospective
        public DataTable GetJR_TrainingRetrospectiveDb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int empID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJR_RetrospectiveTrainingDetails_List]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramempID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = empID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void MarkCommentAsAnswerDb(string markedCommentID, string empID, string tQ_ID, out int resultStatus)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TQ_CommentAsAnswer]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmTraineeQuestionID = new SqlParameter("@TraineeQuestionID", SqlDbType.Int);
                cmd.Parameters.Add(spmTraineeQuestionID).Value = tQ_ID;
                SqlParameter spmTQ_CommentID = new SqlParameter("@TQ_CommentID", SqlDbType.Int);
                cmd.Parameters.Add(spmTQ_CommentID).Value = markedCommentID;
                SqlParameter spmMarkedByEmpID = new SqlParameter("@MarkedByEmpID", SqlDbType.Int);
                cmd.Parameters.Add(spmMarkedByEmpID).Value = empID;
                SqlParameter spmResult = new SqlParameter("@Result", SqlDbType.Int);
                cmd.Parameters.Add(spmResult).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                resultStatus = Convert.ToInt32(cmd.Parameters["@Result"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        //JR Training Doc Details
        public DataTable GetJR_TrainingDocDetailsDb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0,
            string sSearch, int jrID, int Dept_ID, string TrainingOn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJrDocDetailsList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramJR_ID).Value = jrID;
                SqlParameter paramDept_ID = new SqlParameter("@Dept_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramDept_ID).Value = Dept_ID;
                SqlParameter paramTrainingOn = new SqlParameter("@TrainingOn", SqlDbType.VarChar);
                cmd.Parameters.Add(paramTrainingOn).Value = TrainingOn;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetJR_DeptTrainerDetailsDb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0,
           string sSearch, int jrID, int DeptID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJR_DeptTrainerDetails]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramJR_ID).Value = jrID;
                SqlParameter paramDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable GetJR_PendingListdb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int EmpID, int FilterType, int NotificationID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJR_PendingListForHOD]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramEmpID).Value = EmpID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramNotificationID = new SqlParameter("@NotificationID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public DataTable GetJR_ReviewListdb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int EmpID, int FilterType, int NotificationID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJR_ReviewListForQA]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramEmpID).Value = EmpID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramNotificationID = new SqlParameter("@NotificationID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetJrTS_ReviewListdb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int EmpID, int FilterType, int NotificationID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JrTS_ReviewListForQA]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDept_ID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramDept_ID).Value = EmpID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramNotificationID = new SqlParameter("@NotificationID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetJrTS_PendingListdb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int EmpID, int FilterType, int NotificationID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JrTS_PendingListForHOD]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramEmpID).Value = EmpID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramNotificationID = new SqlParameter("@NotificationID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetJR_TrainerEvaluationDetailsdb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch,string sSearch_JR_Name, 
            string sSearch_JR_Version, string sSearch_TraineeName, string sSearch_TraineeDeptName, string sSearch_JR_AssignedBy, string sSearch_JR_AssignedDate, int TrainerID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJR_TrainerEvaluationDetails]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDept_ID = new SqlParameter("@TrainerID", SqlDbType.Int);
                cmd.Parameters.Add(paramDept_ID).Value = TrainerID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;

                SqlParameter paramJR_Name = new SqlParameter("@sSearch_JR_Name", SqlDbType.VarChar);
                cmd.Parameters.Add(paramJR_Name).Value = sSearch_JR_Name;
                SqlParameter paramJR_Version = new SqlParameter("@sSearch_JR_Version", SqlDbType.VarChar);
                cmd.Parameters.Add(paramJR_Version).Value = sSearch_JR_Version;
                SqlParameter paramTraineeName = new SqlParameter("@sSearch_TraineeName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramTraineeName).Value = sSearch_TraineeName;
                SqlParameter paramTraineeDeptName = new SqlParameter("@sSearch_TraineeDeptName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramTraineeDeptName).Value = sSearch_TraineeDeptName;
                SqlParameter paramJR_AssignedBy = new SqlParameter("@sSearch_JR_AssignedBy", SqlDbType.VarChar);
                cmd.Parameters.Add(paramJR_AssignedBy).Value = sSearch_JR_AssignedBy;
                SqlParameter paramJR_AssignedDate = new SqlParameter("@sSearch_JR_AssignedDate", SqlDbType.VarChar);
                cmd.Parameters.Add(paramJR_AssignedDate).Value = sSearch_JR_AssignedDate;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetJR_TrainedDocsdb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int JR_ID, int TrainerID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJrTrainedDocsForTrainer]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramJR_ID).Value = JR_ID;
                SqlParameter paramTrainerID = new SqlParameter("@TrainerID", SqlDbType.Int);
                cmd.Parameters.Add(paramTrainerID).Value = TrainerID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetJR_EmpTrainedDocsdb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int JR_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJrEmpTrainedDocs]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramJR_ID = new SqlParameter("@JrID", SqlDbType.Int);
                cmd.Parameters.Add(paramJR_ID).Value = JR_ID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetJR_TrainerDetailsdb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int JR_ID, out int IsRetrospective)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JrTS_TrainerDetails]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDept_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramDept_ID).Value = JR_ID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter spmIsRetrospective = cmd.Parameters.Add("@IsJrRetrospective", SqlDbType.Int);
                spmIsRetrospective.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                IsRetrospective = Convert.ToInt32(cmd.Parameters["@IsJrRetrospective"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetJR_RetrospectiveDetailsdb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int JR_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JrTS_ToGetRetrospectiveDetails]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDept_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramDept_ID).Value = JR_ID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetJR_ActionHitorydb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int JR_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JR_ActionHistory]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDept_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramDept_ID).Value = JR_ID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetJrTS_ActionHistorydb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int TS_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JrTS_ActionHistory]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDept_ID = new SqlParameter("@TS_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramDept_ID).Value = TS_ID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetTTS_ActionHistorydb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int TTS_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TTS_ActionHistory]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramTTS_ID).Value = TTS_ID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetJR_TemplateListdb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int EmpID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJR_TemplateForList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramEmpID).Value = EmpID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region FAQ Questions

        public DataTable getDocFAQsAnswerDb(int iquestionID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetDocFAQ_Answer]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParamQuestionID = new SqlParameter("@QuestionID", SqlDbType.Int);
                cmd.Parameters.Add(ParamQuestionID).Value = iquestionID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable getDocFAQsDb(int iPageIndex, int iPageSize, int iDocID, string sSearch, out int RecordCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetDocFAQs]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int);
                cmd.Parameters.Add(paramPageIndex).Value = iPageIndex;
                SqlParameter ParamPageSize = new SqlParameter("@PageSize", SqlDbType.Int);
                cmd.Parameters.Add(ParamPageSize).Value = iPageSize;
                SqlParameter ParamDocID = new SqlParameter("@DocID", SqlDbType.Int);
                cmd.Parameters.Add(ParamDocID).Value = iDocID;
                SqlParameter ParamSearch = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(ParamSearch).Value = sSearch;
                SqlParameter ParamRecordCount = new SqlParameter("@RecordCount", SqlDbType.Int);
                cmd.Parameters.Add(ParamRecordCount).Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                RecordCount = Convert.ToInt32(cmd.Parameters["@RecordCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetDocFAQs_ListDb(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int docID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTQ_DocFAQs_List]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = displayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = displayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = sortCol;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@DocID ", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = docID;
                //SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                //spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                //totalRecordCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void CreteDocFAQDb(int iDoc_ID, string question, string answer, int iEmpID, out int result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_FAQ_CreationOnDocument]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parmiDoc_ID = new SqlParameter("@DocID", SqlDbType.Int);
                cmd.Parameters.Add(parmiDoc_ID).Value = iDoc_ID;
                SqlParameter parmquestion = new SqlParameter("@Question", SqlDbType.VarChar);
                cmd.Parameters.Add(parmquestion).Value = question;
                SqlParameter parmanswer = new SqlParameter("@Answer", SqlDbType.Text);
                cmd.Parameters.Add(parmanswer).Value = answer;
                SqlParameter parmiEmpID = new SqlParameter("@CreatedBy", SqlDbType.Int);
                cmd.Parameters.Add(parmiEmpID).Value = iEmpID;
                SqlParameter spResult = new SqlParameter("@CreationStatus", SqlDbType.Int);
                cmd.Parameters.Add(spResult).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@CreationStatus"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public void ModifyDOCFAQDb(int iDocFAQ_ID, string question, string answer, int iEmpID, int iQuestionStatus, string modifiedReason, out int result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_FAQ_Modifications]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parmiDocFAQ_ID = new SqlParameter("@DocFaqID", SqlDbType.Int);
                cmd.Parameters.Add(parmiDocFAQ_ID).Value = iDocFAQ_ID;
                SqlParameter parmquestion = new SqlParameter("@Question", SqlDbType.VarChar);
                cmd.Parameters.Add(parmquestion).Value = question;
                SqlParameter parmanswer = new SqlParameter("@Answer", SqlDbType.Text);
                cmd.Parameters.Add(parmanswer).Value = answer;
                SqlParameter parmiQuestionStatus = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(parmiQuestionStatus).Value = iQuestionStatus;
                SqlParameter parmiEmpID = new SqlParameter("@ModifiedBy", SqlDbType.Int);
                cmd.Parameters.Add(parmiEmpID).Value = iEmpID;
                SqlParameter parmmodifiedReason = new SqlParameter("@ModifiedReason", SqlDbType.VarChar);
                cmd.Parameters.Add(parmmodifiedReason).Value = modifiedReason;
                SqlParameter spResult = new SqlParameter("@Result", SqlDbType.Int);
                cmd.Parameters.Add(spResult).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public DataTable GetDocFaqQuestionDetailsDb(int iDocQueID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTQ_DocFAQs_Details]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DocFAqID", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDocQueID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetDocFAQ_History_ListDb(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int docFaqID
            //  , out int totalRecordCount
            )
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTQ_DocFAQ_History_List]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = displayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = displayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = sortCol;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@DocFAQ_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = docFaqID;
                //SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                //spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                //totalRecordCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        public DataTable TTS_CreationListBalDB(int iDisplayLength, int iDisplayStart, int iSortCol_0,
           string sSortDir_0, string sSearch, int EmpID, int FilterType)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TTS_CreationList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramEmpID).Value = EmpID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetTraineesOnEvaluationDB(int iDisplayLength, int iDisplayStart, int iSortCol_0,
            string sSortDir_0, string sSearch, int tTS_TrainingSessionID, int evaluateOn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTrainingSessionTrainees]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramTrainingSessionID = new SqlParameter("@TrainingSessionID", SqlDbType.Int);
                cmd.Parameters.Add(paramTrainingSessionID).Value = tTS_TrainingSessionID;
                SqlParameter paramEvaluateOn = new SqlParameter("@EvaluateOn", SqlDbType.Int);
                cmd.Parameters.Add(paramEvaluateOn).Value = evaluateOn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region Training Performance
        public DataTable GetTrainingPerformanceDal(int iDisplayLength, int iDisplayStart, int iSortCol_0,
           string sSortDir_0, string sSearch,int EmpID, int DeptID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS_ML].[AizantIT_SP_GetDeptWiseTraineePerformance]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramEmpID).Value = EmpID;
                SqlParameter paramDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetTraineePerformanceDal(int EmpID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS_ML].[AizantIT_SP_GetTraineePerformance]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramEmpID).Value = EmpID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion
        

        public DataTable GetTrainingSessionDetailsDB(int iDisplayLength, int iDisplayStart, int iSortCol_0,
            string sSortDir_0, string sSearch, int tTS_TrainingSessionID, string ExamType, int TTS_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTrainingSessionDetails]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramTrainingSessionID = new SqlParameter("@TrainingSessionID", SqlDbType.Int);
                cmd.Parameters.Add(paramTrainingSessionID).Value = tTS_TrainingSessionID;
                SqlParameter paramTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramTTS_ID).Value = TTS_ID;
                SqlParameter paramExamType = new SqlParameter("@ExamType", SqlDbType.VarChar);
                cmd.Parameters.Add(paramExamType).Value = ExamType;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Dynamic Table for TMS
        public DataTable GetTMS_ActionHistorydb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int TableID, int BaseID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TMS_HistoryAction]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramTableID = new SqlParameter("@TableID", SqlDbType.Int);
                cmd.Parameters.Add(paramTableID).Value = TableID;
                SqlParameter paramBaseID = new SqlParameter("@BaseID", SqlDbType.Int);
                cmd.Parameters.Add(paramBaseID).Value = BaseID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            } 
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Trainee Questions
        public void SubmitAnswertoQuestionbyTrainerDal(int iTrainerID, int iQuestionID, string Answer, out int result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TrainerRepliedOnTQ]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parmTrainerID = new SqlParameter("@RepliedByEmpID", SqlDbType.Int);
                cmd.Parameters.Add(parmTrainerID).Value = iTrainerID;
                SqlParameter parmquestionID = new SqlParameter("@TQ_ID", SqlDbType.VarChar);
                cmd.Parameters.Add(parmquestionID).Value = iQuestionID;
                SqlParameter parmAnswer = new SqlParameter("@RepliedText", SqlDbType.Text);
                cmd.Parameters.Add(parmAnswer).Value = Answer;
                SqlParameter spResult = new SqlParameter("@ExcecutionStatus", SqlDbType.Int);
                cmd.Parameters.Add(spResult).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@ExcecutionStatus"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public void SubmitCommentToTQDb(int iTQ_ID, int iEmpID, int iRoleID, string comment, out int result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TQ_CommentInsert]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parmEmpID = new SqlParameter("@CommentByEmpID", SqlDbType.Int);
                cmd.Parameters.Add(parmEmpID).Value = iEmpID;
                SqlParameter parmRoleID = new SqlParameter("@CommentorRole", SqlDbType.VarChar);
                cmd.Parameters.Add(parmRoleID).Value = iRoleID;
                SqlParameter parmquestionID = new SqlParameter("@TraineeQuestionID", SqlDbType.VarChar);
                cmd.Parameters.Add(parmquestionID).Value = iTQ_ID;
                SqlParameter parmcomment = new SqlParameter("@CommentText", SqlDbType.Text);
                cmd.Parameters.Add(parmcomment).Value = comment;
                SqlParameter spResult = new SqlParameter("@Result", SqlDbType.Int);
                cmd.Parameters.Add(spResult).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public DataSet getTraineeRaisedQuestionstoTrainerDal(int iTrainerID, int idocID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTraineeAskedQuestionstoTrainer]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                //SqlParameter ParamBaseType = new SqlParameter("@BaseType", SqlDbType.Int);
                //cmd.Parameters.Add(ParamBaseType).Value = ibaseType;
                SqlParameter ParamBaseID = new SqlParameter("@TrainerID", SqlDbType.Int);
                cmd.Parameters.Add(ParamBaseID).Value = iTrainerID;
                SqlParameter ParamDocID = new SqlParameter("@DocID", SqlDbType.Int);
                cmd.Parameters.Add(ParamDocID).Value = idocID;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet getTraineeRaisedQuestionstoTraineeDal(int ibaseType, int ibaseID, int idocID, int itraineeID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTraineeAskedQuestionstoTrainee]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParamBaseType = new SqlParameter("@BaseType", SqlDbType.Int);
                cmd.Parameters.Add(ParamBaseType).Value = ibaseType;
                SqlParameter ParamBaseID = new SqlParameter("@BaseID", SqlDbType.Int);
                cmd.Parameters.Add(ParamBaseID).Value = ibaseID;
                SqlParameter ParamDocID = new SqlParameter("@DocID", SqlDbType.Int);
                cmd.Parameters.Add(ParamDocID).Value = idocID;
                SqlParameter ParamTraineeID = new SqlParameter("@TraineeID", SqlDbType.Int);
                cmd.Parameters.Add(ParamTraineeID).Value = itraineeID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To get Trainee Asked Questions to Trainee
        public DataSet GetTraineeaskedQuestionstoTraineeBalDal(int DocID, int TraineeID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTraineeAskedQuestions]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParamDocID = new SqlParameter("@DocID", SqlDbType.Int);
                cmd.Parameters.Add(ParamDocID).Value = DocID;
                SqlParameter ParamTraineeID = new SqlParameter("@TraineeID", SqlDbType.Int);
                cmd.Parameters.Add(ParamTraineeID).Value = TraineeID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable getLatestAnswertoTQDb(int iquestionID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTQ_LatestAnswer]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParamQuestionID = new SqlParameter("@TraineeQuestionID", SqlDbType.Int);
                cmd.Parameters.Add(ParamQuestionID).Value = iquestionID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public void EvaluateTargetTraineeDB(int trainingSessionID, int traineeID, int trainerID, out int ActionStatus,
            int Opinion = 1, int ExplanationStatus = 0, string Remarks = "")
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TrainerEvaluateTrainee]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parmTrainingSessionID = new SqlParameter("@TrainingSessionID", SqlDbType.Int);
                cmd.Parameters.Add(parmTrainingSessionID).Value = trainingSessionID;
                SqlParameter parmTraineeID = new SqlParameter("@TraineeID", SqlDbType.Int);
                cmd.Parameters.Add(parmTraineeID).Value = traineeID;
                SqlParameter parmTrainerID = new SqlParameter("@TrainerID", SqlDbType.Int);
                cmd.Parameters.Add(parmTrainerID).Value = trainerID;
                SqlParameter parmOpinion = new SqlParameter("@Opinion", SqlDbType.Int);
                cmd.Parameters.Add(parmOpinion).Value = Opinion;
                SqlParameter parmExplanationStatus = new SqlParameter("@ExplanationStatus", SqlDbType.Int);
                cmd.Parameters.Add(parmExplanationStatus).Value = ExplanationStatus;
                SqlParameter parmRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(parmRemarks).Value = Remarks;
                SqlParameter spJRStatus = new SqlParameter("@EvaluationStatus", SqlDbType.Int);
                cmd.Parameters.Add(spJRStatus).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                ActionStatus = Convert.ToInt32(cmd.Parameters["@EvaluationStatus"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void EvaluateJrTraineeDal(int DocID, int JR_ID, int TrainerID, out int EvaluateResult,
            string Opinion, string Remarks = "")
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_InsertJrTrainerOpinion]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parmDocID = new SqlParameter("@DocID", SqlDbType.Int);
                cmd.Parameters.Add(parmDocID).Value = DocID;
                SqlParameter parmJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(parmJR_ID).Value = JR_ID;
                SqlParameter parmTrainerID = new SqlParameter("@TrainerID", SqlDbType.Int);
                cmd.Parameters.Add(parmTrainerID).Value = TrainerID;
                SqlParameter parmOpinion = new SqlParameter("@Opinion", SqlDbType.VarChar);
                cmd.Parameters.Add(parmOpinion).Value = Opinion;
                SqlParameter parmRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(parmRemarks).Value = Remarks;
                SqlParameter spJRStatus = new SqlParameter("@EvaluationStatus", SqlDbType.Int);
                cmd.Parameters.Add(spJRStatus).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                EvaluateResult = Convert.ToInt32(cmd.Parameters["@EvaluationStatus"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public DataTable GetAllDocumentsDb(int ideptID, int idocumentType)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetDocsByDeptAndDocType]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramideptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramideptID).Value = ideptID;
                SqlParameter paramidocumentType = new SqlParameter("@DocumentTypeID", SqlDbType.VarChar);
                cmd.Parameters.Add(paramidocumentType).Value = idocumentType;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public DataSet getAccessibleAndToDoActionDeptsDb(int iempID, string type)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetAccessibleAndToDoActionDepts]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramiempID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramiempID).Value = iempID;
                SqlParameter paramtype = new SqlParameter("@Type", SqlDbType.VarChar);
                cmd.Parameters.Add(paramtype).Value = type;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetPendingTTS_List(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0,
            string sSearch, int empID, int FilterType, int NotificationID, out int TotalCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetPendingTTS_List]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@AuthorID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = empID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramNotificationID = new SqlParameter("@Notification_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                TotalCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetTraineesAttendence(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int TTS_ID, int AttendenceStatus, out int TotalCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTraineesBasedOnAttendence]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramTTS_ID).Value = TTS_ID;
                SqlParameter paramAttendenceStatus = new SqlParameter("@AttendenceStatus", SqlDbType.Int);
                cmd.Parameters.Add(paramAttendenceStatus).Value = AttendenceStatus;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                TotalCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSelfCompleteStatusBalDal(int TTS_ID, int TraineeID, int Status)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_Update_TraineeSelf-completedStatus]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramTTS_ID).Value = TTS_ID;
                SqlParameter paramTraineeID = new SqlParameter("@TraineeID", SqlDbType.Int);
                cmd.Parameters.Add(paramTraineeID).Value = TraineeID;
                SqlParameter paramStatus = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(paramStatus).Value = Status;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int SubmitTargetExamResultDb(
            //int TTS_ID, 
            //int iTTS_SessionID,
            int iTTS_TargetExamID
            , int iTraineeID, bool passedOutStatus, int marks, DataTable dtTraineeAnswerSheet, int explainedStatus, out int result, out int Attempts)
        {
            try
            {

                SqlCommand cmd = new SqlCommand("TMS.[AizantIT_SP_TTS_TraineeExamResultSubmit]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@QueIDAndTraineeAnswer", dtTraineeAnswerSheet);

                //SqlParameter SPTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                //cmd.Parameters.Add(SPTTS_ID).Value = TTS_ID;
                //SqlParameter SPiTTS_SessionID = new SqlParameter("@TTS_SessionID", SqlDbType.Int);
                //cmd.Parameters.Add(SPiTTS_SessionID).Value = iTTS_SessionID;

                SqlParameter SPTTS_TargetExamID = new SqlParameter("@TTS_TargetExamID", SqlDbType.Int);
                cmd.Parameters.Add(SPTTS_TargetExamID).Value = iTTS_TargetExamID;
                SqlParameter SPiTraineeID = new SqlParameter("@Trainee_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPiTraineeID).Value = iTraineeID;

                SqlParameter SPMarks = new SqlParameter("@Marks", SqlDbType.Int);
                cmd.Parameters.Add(SPMarks).Value = marks;

                SqlParameter SPExplainedStatus = new SqlParameter("@ExplainedStatus", SqlDbType.Int);
                cmd.Parameters.Add(SPExplainedStatus).Value = explainedStatus;


                SqlParameter SPpassedOutStatus = new SqlParameter("@PassedOutStatus", SqlDbType.Bit);
                cmd.Parameters.Add(SPpassedOutStatus).Value = passedOutStatus;

                SqlParameter spJRStatus = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(spJRStatus).Direction = ParameterDirection.Output;

                SqlParameter spAttempts = new SqlParameter("@Attempts", SqlDbType.Int);
                cmd.Parameters.Add(spAttempts).Direction = ParameterDirection.Output;

                int Count = cmd.ExecuteNonQuery();

                string Result = cmd.Parameters["@Status"].Value.ToString();
                result = Convert.ToInt32(Result);
                Attempts = Convert.ToInt32(cmd.Parameters["@Attempts"].Value);
                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        #region Admin Role
        public DataTable GetUnderQaApproveJRsDb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetUnderQaApproveJRsForAdmin]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetUnderQaApproveJrTSListDb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {

            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetUnderQaApproveJrTsForAdmin]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetUnderQaApproveTargetTS_ListDb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            try
            {
                //modified by Eswar 'AizantIT_SP_GetUnderQaApproveTargetTSForAdmin' to 'AizantIT_SP_GetReAssignTargetTS_Emps'
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetReAssignTargetTS_Emps]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetJR_EvaluatorDb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetUnderHodEvaluationJRsForAdmin]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int submitModifiedHODorQAforJrDb(int JR_ID, int ModifiedHOD, int ModifiedQA, int ModifiedBy, string Remarks, out int ResultStatus)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JR_ModifyHODorQA]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPJR_ID).Value = JR_ID;
                SqlParameter SPModifiedHOD = new SqlParameter("@ModifiedHOD", SqlDbType.Int);
                cmd.Parameters.Add(SPModifiedHOD).Value = ModifiedHOD;
                SqlParameter SPModifiedQA = new SqlParameter("@ModifiedQA", SqlDbType.Int);
                cmd.Parameters.Add(SPModifiedQA).Value = ModifiedQA;
                SqlParameter SPModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPModifiedBy).Value = ModifiedBy;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = Remarks;
                SqlParameter spJRStatus = new SqlParameter("@ResultStatus", SqlDbType.Int);
                cmd.Parameters.Add(spJRStatus).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                string result = cmd.Parameters["@ResultStatus"].Value.ToString();
                ResultStatus = Convert.ToInt32(cmd.Parameters["@ResultStatus"].Value);
                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int submitModifyHodEvaluatorDb(int JR_ID, int ModifiedHOD, int ModifiedBy, string Remarks, out int ResultStatus)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JrTS_ModifyHodEvaluator]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPJR_ID).Value = JR_ID;
                SqlParameter SPModifiedHOD = new SqlParameter("@ModifiedHOD", SqlDbType.Int);
                cmd.Parameters.Add(SPModifiedHOD).Value = ModifiedHOD;
                SqlParameter SPModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPModifiedBy).Value = ModifiedBy;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = Remarks;
                SqlParameter spJRStatus = new SqlParameter("@ResultStatus", SqlDbType.Int);
                cmd.Parameters.Add(spJRStatus).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                string result = cmd.Parameters["@ResultStatus"].Value.ToString();
                ResultStatus = Convert.ToInt32(cmd.Parameters["@ResultStatus"].Value);
                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public void submitModifiedHODorQAforJRTSDb(int iTS_ID, int iAuthorID, int iReviewerID, int iModifiedByID, string remarks, out int result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JrTS_ModifyHODorQA]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPTS_ID = new SqlParameter("@TS_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPTS_ID).Value = iTS_ID;
                SqlParameter SPModifiedHOD = new SqlParameter("@ModifiedHOD", SqlDbType.Int);
                cmd.Parameters.Add(SPModifiedHOD).Value = iAuthorID;
                SqlParameter SPModifiedQA = new SqlParameter("@ModifiedQA", SqlDbType.Int);
                cmd.Parameters.Add(SPModifiedQA).Value = iReviewerID;
                SqlParameter SPModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPModifiedBy).Value = iModifiedByID;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = remarks;
                SqlParameter spJRStatus = new SqlParameter("@ResultStatus", SqlDbType.Int);
                cmd.Parameters.Add(spJRStatus).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@ResultStatus"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public void submitModifiedHODorQAforTargetTSDb(int iTTS_ID, int iAuthorID, int iReviewerID, int iModifiedByID, string remarks, out int result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TTS_ModifyAuthorOrApprover]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPTTS_ID).Value = iTTS_ID;
                SqlParameter SPModifiedHOD = new SqlParameter("@ModifiedHOD", SqlDbType.Int);
                cmd.Parameters.Add(SPModifiedHOD).Value = iAuthorID;
                SqlParameter SPModifiedQA = new SqlParameter("@ModifiedQA", SqlDbType.Int);
                cmd.Parameters.Add(SPModifiedQA).Value = iReviewerID;
                SqlParameter SPModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPModifiedBy).Value = iModifiedByID;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = remarks;
                SqlParameter spJRStatus = new SqlParameter("@ResultStatus", SqlDbType.Int);
                cmd.Parameters.Add(spJRStatus).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@ResultStatus"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        public void UpdateTrainingDeptStatusDAL(int DocID, int DocVersionID, int JrID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ToUpdateJrTrainingDeptStatus]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDocID = new SqlParameter("@DocumentID", SqlDbType.Int);
                cmd.Parameters.Add(paramDocID).Value = DocID;
                SqlParameter paramDocVersionID = new SqlParameter("@DocVersionID", SqlDbType.Int);
                cmd.Parameters.Add(paramDocVersionID).Value = DocVersionID;
                SqlParameter paramJrID = new SqlParameter("@JrID", SqlDbType.Int);
                cmd.Parameters.Add(paramJrID).Value = JrID;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DataTable GetApproveTTS_List(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int empID, int FilterType, int NotificationID, out int TotalCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetApproveTTS_List]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@ReviewerID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = empID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramNotificationID = new SqlParameter("@Notification_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                TotalCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetQA_ApprovedTTS_ListDb(object iDisplayLength, object iDisplayStart, object iSortCol_0, object sSortDir_0, string sSearch, int trainerID, int NotificationID, out int totalRecordCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetQA_ApprovedTTS_List]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@TrainerID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = trainerID;
                SqlParameter paramNotificationID = new SqlParameter("@Notification_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                totalRecordCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Loading list of sessions to Trainee
        public DataTable GetTraineeSession_ListDb(object iDisplayLength, object iDisplayStart, object iSortCol_0, object sSortDir_0, string sSearch, int traineeID, int DeptID, int FilterType, string TargetType, int Year, string Status, out int totalRecordCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTraineeTrainingSessions]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@TraineeID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = traineeID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlParameter paramTargetType = new SqlParameter("@TargetType", SqlDbType.VarChar);
                cmd.Parameters.Add(paramTargetType).Value = TargetType;
                SqlParameter paramYear = new SqlParameter("@Year", SqlDbType.Int);
                cmd.Parameters.Add(paramYear).Value = Year;
                SqlParameter paramStatus = new SqlParameter("@Status", SqlDbType.VarChar);
                cmd.Parameters.Add(paramStatus).Value = Status;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                totalRecordCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetTraineeOnlineSession_ListDb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int traineeID, int DeptID, int FilterType, string TargetType, int Year, string Status, out int totalRecordCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTraineeOnlineTrainingSessions]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@TraineeID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = traineeID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlParameter paramTargetType = new SqlParameter("@TargetType", SqlDbType.VarChar);
                cmd.Parameters.Add(paramTargetType).Value = TargetType;
                SqlParameter paramYear = new SqlParameter("@Year", SqlDbType.Int);
                cmd.Parameters.Add(paramYear).Value = Year;
                SqlParameter paramStatus = new SqlParameter("@Status", SqlDbType.VarChar);
                cmd.Parameters.Add(paramStatus).Value = Status;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                totalRecordCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        //need to write Sp
        public DataTable GetTrainerEvaluationTTS_ListDb(object iDisplayLength, object iDisplayStart, object iSortCol_0, object sSortDir_0, string sSearch,
            /*string sSearch_TTS_TrainingSessionID, string sSearch_DocumentID,*/ string sSearch_Document, string sSearch_DeptCode, string sSearch_TrainingDate, string sSearch_StartTime,
            string sSearch_EndTime, string sSearch_Trainer, string sSearch_TypeOfTraining, string sSearch_ExamType, int trainerID, string Evaluated, out int totalRecordCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTargetTrainerEvaluation]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;

                //SqlParameter paramsSearch_TTS_TrainingSessionID = new SqlParameter("@sSearch_TTS_TrainingSessionID", SqlDbType.VarChar);
                //cmd.Parameters.Add(paramsSearch_TTS_TrainingSessionID).Value = sSearch_TTS_TrainingSessionID;
                //SqlParameter paramsSearch_DocumentID = new SqlParameter("@sSearch_DocumentID", SqlDbType.VarChar);
                //cmd.Parameters.Add(paramsSearch_DocumentID).Value = sSearch_DocumentID;
                SqlParameter paramsSearch_Document = new SqlParameter("@sSearch_Document", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Document).Value = sSearch_Document;
                SqlParameter paramsSearch_DeptCode = new SqlParameter("@sSearch_DeptCode", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_DeptCode).Value = sSearch_DeptCode;
                SqlParameter paramsSearch_TrainingDate = new SqlParameter("@sSearch_TrainingDate", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TrainingDate).Value = sSearch_TrainingDate;
                SqlParameter paramsSearch_StartTime = new SqlParameter("@sSearch_StartTime", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_StartTime).Value = sSearch_StartTime;
                SqlParameter paramsSearch_EndTime = new SqlParameter("@sSearch_EndTime", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_EndTime).Value = sSearch_EndTime;
                SqlParameter paramsSearch_Trainer = new SqlParameter("@sSearch_Trainer", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Trainer).Value = sSearch_Trainer;
                SqlParameter paramsSearch_TypeOfTraining = new SqlParameter("@sSearch_TypeOfTraining", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TypeOfTraining).Value = sSearch_TypeOfTraining;
                SqlParameter paramsSearch_ExamType = new SqlParameter("@sSearch_ExamType", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_ExamType).Value = sSearch_ExamType;
                SqlParameter paramempID = new SqlParameter("@TrainerID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = trainerID;
                SqlParameter paramEvaluated = new SqlParameter("@OnlyEvalutedRecords", SqlDbType.Char, 1);
                cmd.Parameters.Add(paramEvaluated).Value = Evaluated;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                totalRecordCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetQA_ApprovedTTS_ListForHodAndQaDb(object iDisplayLength, object iDisplayStart, object iSortCol_0, object sSortDir_0, string sSearch,
            string sSearch_DocumentName, string sSearch_DeptCode, string sSearch_TypeOfTraining, string sSearch_TargetType, string sSearch_Author, string sSearch_Reviewer,
            string sSearch_ApproveDate, string sSearch_StatusName,
            int EMP_ID, int Year, int FilterType, int DeptID, int Month, string TargetType, string Status, int SubRefID, out int totalRecordCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetQA_ApprovedTTS_ListForHodAndQA]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;

                SqlParameter paramsSearch_DocumentName = new SqlParameter("@sSearch_DocumentName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_DocumentName).Value = sSearch_DocumentName;

                SqlParameter paramsSearch_DeptCode = new SqlParameter("@sSearch_DeptCode", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_DeptCode).Value = sSearch_DeptCode;

                SqlParameter paramsSearch_TypeOfTraining = new SqlParameter("@sSearch_TypeOfTraining", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TypeOfTraining).Value = sSearch_TypeOfTraining;

                SqlParameter paramsSearch_TargetType = new SqlParameter("@sSearch_TargetType", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TargetType).Value = sSearch_TargetType;

                SqlParameter paramsSearch_Author = new SqlParameter("@sSearch_Author", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Author).Value = sSearch_Author;

                SqlParameter paramsSearch_Reviewer = new SqlParameter("@sSearch_Reviewer", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Reviewer).Value = sSearch_Reviewer;

                SqlParameter paramsSearch_ApproveDate = new SqlParameter("@sSearch_ApproveDate", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_ApproveDate).Value = sSearch_ApproveDate;

                SqlParameter paramsSearch_StatusName = new SqlParameter("@sSearch_StatusName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_StatusName).Value = sSearch_StatusName;

                SqlParameter paramYear = new SqlParameter("@Year", SqlDbType.Int);
                cmd.Parameters.Add(paramYear).Value = Year;
                SqlParameter paramempID = new SqlParameter("@Emp_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = EMP_ID;
                SqlParameter paramDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramMonth = new SqlParameter("@Month", SqlDbType.Int);
                cmd.Parameters.Add(paramMonth).Value = Month;
                SqlParameter paramTargetType = new SqlParameter("@TargetType", SqlDbType.VarChar);
                cmd.Parameters.Add(paramTargetType).Value = TargetType;
                SqlParameter paramStatus = new SqlParameter("@Status", SqlDbType.VarChar);
                cmd.Parameters.Add(paramStatus).Value = Status;

                SqlParameter paramSubRefID = new SqlParameter("@SubRefID", SqlDbType.Int);
                cmd.Parameters.Add(paramSubRefID).Value = SubRefID;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                totalRecordCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable GetTTS_TraineeAssignedListDb(object iDisplayLength, object iDisplayStart, object iSortCol_0, object sSortDir_0, string sSearch, int TTS_ID, int TTSSession_ID, out int totalRecordCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTTS_TraineeAssignedList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramTTS_ID).Value = TTS_ID;
                SqlParameter paramTTSSession_ID = new SqlParameter("@TTSSession_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramTTSSession_ID).Value = TTSSession_ID;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                totalRecordCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getJR_AssignedSopsDb(int ideptID, int iJR_Id)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJR_AssignedDocuments]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmideptID = new SqlParameter("@SopDeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmideptID).Value = ideptID;
                SqlParameter spmiJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmiJR_ID).Value = iJR_Id;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable getTS_AuditDetailsDb(int jR_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetDataForTS_AuditTrail]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmiJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmiJR_ID).Value = jR_ID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getEmpbyDeptIdandRoleIDDb(int ideptID, int iRoleID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetEmpbyDeptIDandRoleID]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmideptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmideptID).Value = ideptID;
                SqlParameter spmiRoleID = new SqlParameter("@RoleID", SqlDbType.Int);
                cmd.Parameters.Add(spmiRoleID).Value = iRoleID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet getAllEmpbyDeptIdandRoleIDdb(int ideptID, int empBindStatus)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetAllEmpbyDeptIDandRoleID]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmideptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmideptID).Value = ideptID;
                SqlParameter spmiRoleID = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(spmiRoleID).Value = empBindStatus;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet dS = new DataSet();
                sda.Fill(dS);
                return dS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Target TS     

        public DataTable getEffectedDocsByTargetTypeDB(int deptID, int targetType, int DocumentType, int TTS_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_GetDocumentsForTTS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmDeptID).Value = deptID;
                SqlParameter spmTargetType = new SqlParameter("@TargetType", SqlDbType.Int);
                cmd.Parameters.Add(spmTargetType).Value = targetType;
                SqlParameter spmDocumentType = new SqlParameter("@DocumentType", SqlDbType.Int);
                cmd.Parameters.Add(spmDocumentType).Value = DocumentType;
                SqlParameter spmTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmTTS_ID).Value = TTS_ID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetCreationYearForJRandTargetDAL()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ToGetCreatedYearForJrandTarget]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetSelfCompleteTTSID_DAL(int TTSID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ToGetTTSID_OfSelfComplete]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmTTSID = new SqlParameter("@TTSID", SqlDbType.Int);
                cmd.Parameters.Add(spmTTSID).Value = TTSID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable BindAssignedDeptDAL(int EmpID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ToLoadAssignDeptForTrainee]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(spmEmpID).Value = EmpID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataSet GetCreationYearForATCDAL()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ToGetCreatedYearForATC]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetPreviousTraineesForRevisionDocDb(int iDeptID, int iDocID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_GetPreviousVersionAttendeedTrainees", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDept_ID = new SqlParameter("@Dept_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmDept_ID).Value = iDeptID;
                SqlParameter spmDoc_ID = new SqlParameter("@Doc_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmDoc_ID).Value = iDocID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable getDocsForRefresherDB(int deptID, int refresherMonth, int CalendarYear, int DocumentType)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_GetRefresherDocumentsForTTS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmDeptID).Value = deptID;
                SqlParameter spmRefresherMonth = new SqlParameter("@RefresherMonth", SqlDbType.Int);
                cmd.Parameters.Add(spmRefresherMonth).Value = refresherMonth;
                SqlParameter spmCalendarYear = new SqlParameter("@CalendarYear", SqlDbType.Int);
                cmd.Parameters.Add(spmCalendarYear).Value = CalendarYear;
                SqlParameter spmDocumentType = new SqlParameter("@DocumentType", SqlDbType.Int);
                cmd.Parameters.Add(spmDocumentType).Value = DocumentType;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable getEffectedDocumentsForGeneralDB(int DeptID, int DocType)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetDocumentsOnEffective]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmDeptID).Value = DeptID;
                SqlParameter spmDocumentType = new SqlParameter("@DocType", SqlDbType.Int);
                cmd.Parameters.Add(spmDocumentType).Value = DocType;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void CreateTargetTrainingDb(TTS_Objects objTTS, out int result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.[AizantIT_SP_TTS_Create]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Trainers", SqlDbType.Structured).Value = objTTS.dsTrainersAndTrainees.Tables["Trainers"];
                cmd.Parameters.Add("@Trainees", SqlDbType.Structured).Value = objTTS.dsTrainersAndTrainees.Tables["Trainees"];
                SqlParameter SPDept_ID = new SqlParameter("@Dept_ID", SqlDbType.Int);
                SqlParameter SPDocument_Type = new SqlParameter("@Document_Type", SqlDbType.Int);
                SqlParameter SPDocument_ID = new SqlParameter("@Document_ID", SqlDbType.Int);
                SqlParameter SPTypeOFTraining = new SqlParameter("@TypeOfTraining", SqlDbType.VarChar, 1);
                SqlParameter SPAuthorBy = new SqlParameter("@Author_By", SqlDbType.Int);
                SqlParameter SPReviewBy = new SqlParameter("@Review_By", SqlDbType.Int);
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                SqlParameter SPTargetType = new SqlParameter("@Target_Type", SqlDbType.Int);
                SqlParameter SPMonthID = new SqlParameter("@Month_ID", SqlDbType.Int);
                SqlParameter SPDueDate = new SqlParameter("@DueDate", SqlDbType.Date);
                SqlParameter SPResult = cmd.Parameters.Add("@Result", SqlDbType.Int);

                cmd.Parameters.Add(SPDept_ID).Value = objTTS.Dept_ID;
                cmd.Parameters.Add(SPDocument_Type).Value = objTTS.Document_Type;
                cmd.Parameters.Add(SPDocument_ID).Value = objTTS.Document_ID;
                cmd.Parameters.Add(SPTypeOFTraining).Value = objTTS.TypeOFTraining;
                cmd.Parameters.Add(SPAuthorBy).Value = objTTS.AuthorEmpId;
                cmd.Parameters.Add(SPReviewBy).Value = objTTS.ReviewEmpId;
                cmd.Parameters.Add(SPRemarks).Value = objTTS.Remarks;
                cmd.Parameters.Add(SPTargetType).Value = objTTS.TargetType;
                cmd.Parameters.Add(SPMonthID).Value = objTTS.MonthID; 
                cmd.Parameters.Add(SPDueDate).Value = objTTS.DueDate;
                SPResult.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
        }

        //Update TargetTraining
        public void UpdateTargetTrainingDb(TTS_Objects objTTS, out int result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TTS_Update]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Trainers", SqlDbType.Structured).Value = objTTS.dsTrainersAndTrainees.Tables["Trainers"];
                cmd.Parameters.Add("@Trainees", SqlDbType.Structured).Value = objTTS.dsTrainersAndTrainees.Tables["Trainees"];
                SqlParameter SPDept_ID = new SqlParameter("@Dept_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPDept_ID).Value = objTTS.Dept_ID;
                SqlParameter SPDocument_Type = new SqlParameter("@Document_Type", SqlDbType.Int);
                cmd.Parameters.Add(SPDocument_Type).Value = objTTS.Document_Type;
                SqlParameter SPDocument_ID = new SqlParameter("@Document_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPDocument_ID).Value = objTTS.Document_ID;
                SqlParameter SPTypeOFTraining = new SqlParameter("@TypeOfTraining", SqlDbType.VarChar, 1);
                cmd.Parameters.Add(SPTypeOFTraining).Value = objTTS.TypeOFTraining;
                SqlParameter SPAuthorBy = new SqlParameter("@AuthorBy", SqlDbType.Int);
                cmd.Parameters.Add(SPAuthorBy).Value = objTTS.AuthorEmpId;
                SqlParameter SPReviewBy = new SqlParameter("@Review_By", SqlDbType.Int);
                cmd.Parameters.Add(SPReviewBy).Value = objTTS.ReviewEmpId;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = objTTS.Remarks;
                SqlParameter SPTargetType = new SqlParameter("@Target_Type", SqlDbType.Int);
                cmd.Parameters.Add(SPTargetType).Value = objTTS.TargetType;
                SqlParameter SPMonthID = new SqlParameter("@Month_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPMonthID).Value = objTTS.MonthID;
                SqlParameter SPYear = new SqlParameter("@Year", SqlDbType.Int);
                cmd.Parameters.Add(SPYear).Value = objTTS.Year;
                SqlParameter SPTargetTS_ID = new SqlParameter("@TargetTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPTargetTS_ID).Value = objTTS.TargetTS_ID;
                SqlParameter SPDueDate = new SqlParameter("@DueDate", SqlDbType.Date);
                cmd.Parameters.Add(SPDueDate).Value = objTTS.DueDate;

                SqlParameter SPResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                SPResult.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
        }
        public DataSet GetTargetTrainingDetailsDb(int iTTS_ID, int AccessEmpID, out int CurrentHistoryStatus, out int TargetCurrentStatus)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTTS_Details]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                SqlParameter spmAccessEmpID = new SqlParameter("@AccessEmpID", SqlDbType.Int);
                SqlParameter spmCurrentStatus = new SqlParameter("@CurrentHistoryStatus", SqlDbType.Int);
                SqlParameter spmTargetCurrentStatus = new SqlParameter("@TargetCurrentStatus", SqlDbType.Int);
                cmd.Parameters.Add(spmTTS_ID).Value = iTTS_ID;
                cmd.Parameters.Add(spmAccessEmpID).Value = AccessEmpID;
                cmd.Parameters.Add(spmCurrentStatus).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(spmTargetCurrentStatus).Direction = ParameterDirection.Output;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                string rlt = cmd.Parameters["@CurrentHistoryStatus"].Value.ToString();
                CurrentHistoryStatus = Convert.ToInt32(rlt);
                string result = cmd.Parameters["@TargetCurrentStatus"].Value.ToString();
                TargetCurrentStatus = Convert.ToInt32(result);
                return ds;
            }
            catch (Exception em)
            {
                throw em;
            }
        }
        //for TTS QA Approval
        public void SubmitTTS_QA_ApprovalDb(int TTS_ID, int ApproverEmpID, int ActionStatus, out int ResultStatus, string Remarks = "")
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_TTS_Approver_Update", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                SqlParameter spmApproverEmpID = new SqlParameter("@ApproverEmpID", SqlDbType.Int);
                SqlParameter spmStatus = new SqlParameter("@Status", SqlDbType.Int);
                SqlParameter spmRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);

                SqlParameter Status = cmd.Parameters.Add("@Result", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(spmTTS_ID).Value = TTS_ID;
                cmd.Parameters.Add(spmApproverEmpID).Value = ApproverEmpID;
                cmd.Parameters.Add(spmStatus).Value = ActionStatus;
                cmd.Parameters.Add(spmRemarks).Value = Remarks;

                cmd.ExecuteNonQuery();
                string Result = cmd.Parameters["@Result"].Value.ToString();
                ResultStatus = Convert.ToInt32(Result);

                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }




        #endregion
        public DataTable getEmployeeCvOrAcDb(int empID, int docType)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_GetEmpCVandAD", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(spmEmpID).Value = empID;
                SqlParameter spmDocType = new SqlParameter("@DocType", SqlDbType.Int);
                cmd.Parameters.Add(spmDocType).Value = docType;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetDatatoCRDb(int JR_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTrainingFileData]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmEmpID = new SqlParameter("@jrID", SqlDbType.Int);
                cmd.Parameters.Add(spmEmpID).Value = JR_ID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }


        //Create Target Sessions Suresh
        public void CreateTargetSessions(int TTSID, int TrainerID, string TrainingDate, string FromTime, string ToTime, string Remarks, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TTS_CreateSession]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPTTS_ID).Value = TTSID;
                SqlParameter SPTrainerID = new SqlParameter("@TrainerID", SqlDbType.Int);
                cmd.Parameters.Add(SPTrainerID).Value = TrainerID;
                SqlParameter SPTrainingDate = new SqlParameter("@TrainingDate", SqlDbType.DateTime);
                cmd.Parameters.Add(SPTrainingDate).Value = TrainingDate;
                SqlParameter SPFromTime = new SqlParameter("@FromTime", SqlDbType.VarChar);
                cmd.Parameters.Add(SPFromTime).Value = FromTime;
                SqlParameter SPToTime = new SqlParameter("@ToTime", SqlDbType.VarChar);
                cmd.Parameters.Add(SPToTime).Value = ToTime;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = Remarks;
                SqlParameter SPResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                SPResult.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public DataTable CreateOnlineTrainingDb(int tTSID, int empid, string DueDate, string Remarks, char IsSelfCompleteAllowed, out int result)
        {
            SqlCommand cmd = new SqlCommand("TMS.[AizantIT_SP_TTS_CreateOnlineSession]", con);
            try
            {
                DataTable dt = new DataTable();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TrainerID", SqlDbType.Int).Value = empid;
                cmd.Parameters.Add("@TTS_ID", SqlDbType.Int).Value = tTSID;
                SqlParameter SPDueDate = new SqlParameter("@DueDate", SqlDbType.DateTime);
                cmd.Parameters.Add(SPDueDate).Value = DueDate;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = Remarks;
                SqlParameter SPIsSelfCompleteAllowed = new SqlParameter("@IsSelfCompleteAllowed", SqlDbType.Char, 1);
                cmd.Parameters.Add(SPIsSelfCompleteAllowed).Value = IsSelfCompleteAllowed;
                cmd.Parameters.Add("@ActionStatus", SqlDbType.Int).Direction = ParameterDirection.Output;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                result = Convert.ToInt32(cmd.Parameters["@ActionStatus"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable getTTS_OnlineSessionDetailsDb(int itTS_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTTS_OnlineTrainingDetails]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmitTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmitTS_ID).Value = itTS_ID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //ReSchedule Target Sessions Suresh
        public void ReScheduleTargetSessions(int TTSID, string TrainingDate, string FromTime, string ToTime, string Remarks, int TrainerID, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TTS_ReSecheduleSession]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPTTS_ID).Value = TTSID;
                SqlParameter SPTrainerID = new SqlParameter("@Trainer_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPTrainerID).Value = TrainerID;
                SqlParameter SPTrainingDate = new SqlParameter("@TrainingDate", SqlDbType.DateTime);
                cmd.Parameters.Add(SPTrainingDate).Value = TrainingDate;
                SqlParameter SPFromTime = new SqlParameter("@FromTime", SqlDbType.VarChar);
                cmd.Parameters.Add(SPFromTime).Value = FromTime;
                SqlParameter SPToTime = new SqlParameter("@ToTime", SqlDbType.VarChar);
                cmd.Parameters.Add(SPToTime).Value = ToTime;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = Remarks;
                SqlParameter SPResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                SPResult.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public DataTable GetTTS_TraingSessionList(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int TTSID, out int TotalCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTTS_TrainingSessionList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramTTSID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramTTSID).Value = TTSID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                TotalCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Get Traning Session Details
        public DataSet GetTrainingSessionDetails(int TTS_ID, out int SessionsCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_TrainingSessionDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmTTS_ID).Value = TTS_ID;
                SqlParameter spmTotalSessionCount = cmd.Parameters.Add("@TotalSessionCount", SqlDbType.Int);
                spmTotalSessionCount.Direction = ParameterDirection.Output;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                SessionsCount = Convert.ToInt32(cmd.Parameters["@TotalSessionCount"].Value);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //For getting Active Docs
        public DataTable GetActiveDocsDal(int DocID, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_DocQuestionnaireInactive]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDocID = new SqlParameter("@DocumentID", SqlDbType.Int);
                cmd.Parameters.Add(spmDocID).Value = DocID;
                SqlParameter spmTotalResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                spmTotalResult.Direction = ParameterDirection.Output;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //For Updating Jr Docs for Oral Evaluation
        public DataTable JRDocToSubmitOralDal(int DocID, int JR_ID, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JRDocToSubmitOral]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDocID = new SqlParameter("@DocID", SqlDbType.Int);
                cmd.Parameters.Add(spmDocID).Value = DocID;
                SqlParameter spmJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmJR_ID).Value = JR_ID;
                SqlParameter spmTotalResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                spmTotalResult.Direction = ParameterDirection.Output;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //To get Trainer Opinion
        public DataTable GetTrainerOpinionDal(int DocID, int JR_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJrTrainerOpinion]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDocID = new SqlParameter("@DocID", SqlDbType.Int);
                cmd.Parameters.Add(spmDocID).Value = DocID;
                SqlParameter spmJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmJR_ID).Value = JR_ID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        // Take Attandence to Trainees in TTS insert
        public void InsertTraineesAttandence(int TTS_TrainingSessionsID, int TrainerID, DataTable dtTrainees, char ExamType, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TTS_TakeAttandence]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPTTS_TrainingSessionsID = new SqlParameter("@TTS_TrainingSessionsID", SqlDbType.Int);
                cmd.Parameters.Add(SPTTS_TrainingSessionsID).Value = TTS_TrainingSessionsID;
                SqlParameter SPTrainerID = new SqlParameter("@Trainer_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPTrainerID).Value = TrainerID;
                cmd.Parameters.AddWithValue("@Trainees", dtTrainees);
                SqlParameter SPExamType = new SqlParameter("@ExamType", SqlDbType.Char);
                cmd.Parameters.Add(SPExamType).Value = ExamType;
                SqlParameter spmResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                spmResult.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        #region JR Training       
        public DataTable getTrainingFileDataToViewDb(int jR_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTrainingFile_Record]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmjR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmjR_ID).Value = jR_ID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetJrTrainingDetailsDb(int JR_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJR_TrainingDetails]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmEmpID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmEmpID).Value = JR_ID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Employee Jr Operation      
        public DataSet getHOD_ListAndQA_ListAndJR_ListByDeptIDdb(int iDeptID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.[AizantIT_SP_GetHOD_ListAndQA_ListAndJR_ListByDeptID]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmiDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmiDeptID).Value = iDeptID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                cmd.Dispose();
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getEmpbyDefaultDeptIDandRoleIDForGeneraldb(int iDeptID, int iDocID, int iRoleID, int IsEmpActive = 0, int IsIncident = 0)//IsEmpActive=0 Active Emp's, 1= All Emp's.
        {
            try
            {
                SqlCommand cmd;
                if (IsIncident == 0)
                {
                    cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTTSTraineesForGeneralType]", con);
                }
                else
                {
                    cmd = new SqlCommand("[QMS].[AizantIT_SP_GetTTSTraineesForGeneralType]", con);
                }
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmDeptID).Value = iDeptID;
                SqlParameter spmDocID = new SqlParameter("@DocID", SqlDbType.Int);
                cmd.Parameters.Add(spmDocID).Value = iDocID;
                SqlParameter spmiRoleID = new SqlParameter("@Role_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmiRoleID).Value = iRoleID;
                SqlParameter spmIsEmpActive = new SqlParameter("@IsEmpActive", SqlDbType.Int);
                cmd.Parameters.Add(spmIsEmpActive).Value = IsEmpActive;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                cmd.Dispose();
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable EditTraineesForGeneralTrainingdb(int iDeptID, int iDocID, int iTTSID, int iRoleID, int IsEmpActive = 0, int IncidentSp = 0)//IsEmpActive=0 Active Emp's, 1= All Emp's.IncidentSp=1 for QMS 0 for TMS
        {
            try
            {
                SqlCommand cmd;
                if (IncidentSp == 0)
                {
                    cmd = new SqlCommand("[TMS].[AizantIT_SP_EditTTSTraineesForGeneralType]", con);
                }
                else
                {
                    cmd = new SqlCommand("[QMS].[AizantIT_SP_EditTTSTraineesForGeneralType]", con);
                }
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmDeptID).Value = iDeptID;
                SqlParameter spmDocID = new SqlParameter("@DocID", SqlDbType.Int);
                cmd.Parameters.Add(spmDocID).Value = iDocID;
                SqlParameter spmTTSID = new SqlParameter("@TTSID", SqlDbType.Int);
                cmd.Parameters.Add(spmTTSID).Value = iTTSID;
                SqlParameter spmiRoleID = new SqlParameter("@Role_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmiRoleID).Value = iRoleID;
                SqlParameter spmIsEmpActive = new SqlParameter("@IsEmpActive", SqlDbType.Int);
                cmd.Parameters.Add(spmIsEmpActive).Value = IsEmpActive;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                cmd.Dispose();
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable getJrAssignedEmployees(int DeptID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.[AizantIT_SP_GetJrAssignedEmployees]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmDeptID).Value = DeptID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                cmd.Dispose();
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void JR_TemplateOperationsDb(string TemplateName, string Description, int DeptID, int CreateOrModifiedBy, string Operation, out int Status, int JrID = 0)
        {
            try
            {
                if (Operation == "Insert")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JR_Template]", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter spmTemplateName = new SqlParameter("@TemplateName", SqlDbType.VarChar);
                    SqlParameter spmJR_Description = new SqlParameter("@JR_Description", SqlDbType.VarChar);
                    SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                    SqlParameter spmCreateOrModifiedBy = new SqlParameter("@CreateOrModifiedBy", SqlDbType.Int);
                    SqlParameter spmOperation = new SqlParameter("@Operation", SqlDbType.VarChar);
                    SqlParameter spmStatus = cmd.Parameters.Add("@Status", SqlDbType.Int);

                    cmd.Parameters.Add(spmTemplateName).Value = TemplateName;
                    cmd.Parameters.Add(spmJR_Description).Value = Description;
                    cmd.Parameters.Add(spmDeptID).Value = DeptID;
                    cmd.Parameters.Add(spmCreateOrModifiedBy).Value = CreateOrModifiedBy;
                    cmd.Parameters.Add(spmOperation).Value = Operation;

                    spmStatus.Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    Status = Convert.ToInt32(cmd.Parameters["@Status"].Value);
                    cmd.Dispose();
                }
                else
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JR_Template]", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter spmTemplateName = new SqlParameter("@TemplateName", SqlDbType.VarChar);
                    SqlParameter spmJR_Description = new SqlParameter("@JR_Description", SqlDbType.VarChar);
                    SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                    SqlParameter spmCreateOrModifiedBy = new SqlParameter("@CreateOrModifiedBy", SqlDbType.Int);
                    SqlParameter spmOperation = new SqlParameter("@Operation", SqlDbType.VarChar);
                    SqlParameter spmJrID = new SqlParameter("@JrID", SqlDbType.Int);

                    SqlParameter spmStatus = cmd.Parameters.Add("@Status", SqlDbType.Int);

                    cmd.Parameters.Add(spmTemplateName).Value = TemplateName;
                    cmd.Parameters.Add(spmJR_Description).Value = Description;
                    cmd.Parameters.Add(spmDeptID).Value = DeptID;
                    cmd.Parameters.Add(spmCreateOrModifiedBy).Value = CreateOrModifiedBy;
                    cmd.Parameters.Add(spmOperation).Value = Operation;
                    cmd.Parameters.Add(spmJrID).Value = JrID;

                    spmStatus.Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    Status = Convert.ToInt32(cmd.Parameters["@Status"].Value);
                    cmd.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public DataSet getTsForModificationDb(int TS_ID)
        {
            try
            { 
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_getDataForTsModification]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmTS_ID = new SqlParameter("@TS_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmTS_ID).Value = TS_ID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable getJR_TemplateListDb(int DeptID, int EmpID = 0)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_GetJR_TemplatesList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmDeptID).Value = DeptID;
                if (EmpID != 0)
                {
                    SqlParameter spmEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                    cmd.Parameters.Add(spmEmpID).Value = EmpID;
                }
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                cmd.Dispose();
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetEmployeeJrDb(int JrID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_GetEmpJR_Details", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmJR_ID).Value = JrID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                cmd.Dispose();
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string getJR_DescriptionDb(int JrTemplateID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_GetJR_Description", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmJrID = new SqlParameter("@JRID", SqlDbType.Int);
                cmd.Parameters.Add(spmJrID).Value = JrTemplateID;
                con.Open();
                string result = cmd.ExecuteScalar().ToString();
                cmd.Dispose();
                return result;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public void EmpJR_OperationsDb(string Operation, EmpJrObjects objEmpJr, out int ResultStatus, string ModifiedBy = "")
        {
            try
            {
                string Result = "";
                con.Open();
                if (Operation == "Insert")
                {
                    SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_JrOperations", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                    SqlParameter spmEmpID = new SqlParameter("@TraineeEmpID", SqlDbType.Int);
                    SqlParameter spmJR_TemplateID = new SqlParameter("@JR_TemplateID", SqlDbType.Int);
                    SqlParameter spmJrName = new SqlParameter("@JrName", SqlDbType.VarChar);
                    SqlParameter spmJrDescriptions = new SqlParameter("@JrDescriptions", SqlDbType.VarChar);
                    SqlParameter spmAssignedBy = new SqlParameter("@AssignedBy", SqlDbType.Int);
                    SqlParameter spmRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                    SqlParameter spmCurrentStatus = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                    SqlParameter spmOperation = new SqlParameter("@Operation", SqlDbType.VarChar);
                    SqlParameter spmApproveOrDeclinedBy = new SqlParameter("@ApproveOrDeclinedBy", SqlDbType.Int);

                    SqlParameter Status = cmd.Parameters.Add("@ResultStatus", SqlDbType.Int);
                    Status.Direction = ParameterDirection.Output;

                    cmd.Parameters.Add(spmApproveOrDeclinedBy).Value = objEmpJr.ApprovedOrDeclinedBy;
                    cmd.Parameters.Add(spmDeptID).Value = objEmpJr.DeptID;
                    cmd.Parameters.Add(spmEmpID).Value = objEmpJr.EmpID;
                    cmd.Parameters.Add(spmJR_TemplateID).Value = objEmpJr.JR_TemplateID;
                    cmd.Parameters.Add(spmJrName).Value = objEmpJr.Jr_Name;
                    cmd.Parameters.Add(spmJrDescriptions).Value = objEmpJr.JrDescription;
                    cmd.Parameters.Add(spmAssignedBy).Value = objEmpJr.AssignedBy;
                    cmd.Parameters.Add(spmRemarks).Value = objEmpJr.Remarks;
                    cmd.Parameters.Add(spmCurrentStatus).Value = objEmpJr.CurrentStatus;
                    cmd.Parameters.Add(spmOperation).Value = Operation;

                    cmd.ExecuteNonQuery();
                    Result = cmd.Parameters["@ResultStatus"].Value.ToString();
                    cmd.Dispose();
                }
                else
                {
                    if (ModifiedBy == "QA")
                    {
                        SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_JrOperations", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                        SqlParameter spmEmpID = new SqlParameter("@TraineeEmpID", SqlDbType.Int);
                        SqlParameter spmJrName = new SqlParameter("@JrName", SqlDbType.VarChar);
                        SqlParameter spmRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                        SqlParameter spmApproveOrDeclinedBy = new SqlParameter("@ApproveOrDeclinedBy", SqlDbType.Int);
                        SqlParameter spmJrID = new SqlParameter("@JrID", SqlDbType.Int);
                        SqlParameter spmCurrentStatus = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                        SqlParameter spmOperation = new SqlParameter("@Operation", SqlDbType.VarChar);

                        SqlParameter Status = cmd.Parameters.Add("@ResultStatus", SqlDbType.Int);
                        Status.Direction = ParameterDirection.Output;

                        cmd.Parameters.Add(spmJrID).Value = objEmpJr.JrID;
                        cmd.Parameters.Add(spmDeptID).Value = objEmpJr.DeptID;
                        cmd.Parameters.Add(spmEmpID).Value = objEmpJr.EmpID;
                        cmd.Parameters.Add(spmJrName).Value = objEmpJr.Jr_Name;
                        cmd.Parameters.Add(spmRemarks).Value = objEmpJr.Remarks;
                        cmd.Parameters.Add(spmApproveOrDeclinedBy).Value = objEmpJr.ApprovedOrDeclinedBy;
                        cmd.Parameters.Add(spmCurrentStatus).Value = objEmpJr.CurrentStatus;
                        cmd.Parameters.Add(spmOperation).Value = Operation;

                        cmd.ExecuteNonQuery();
                        Result = cmd.Parameters["@ResultStatus"].Value.ToString();
                        cmd.Dispose();
                    }
                    else
                    {
                        SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_JrOperations", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                        SqlParameter spmEmpID = new SqlParameter("@TraineeEmpID", SqlDbType.Int);
                        SqlParameter spmJR_TemplateID = new SqlParameter("@JR_TemplateID", SqlDbType.Int);
                        SqlParameter spmJrName = new SqlParameter("@JrName", SqlDbType.VarChar);

                        SqlParameter spmAssignedBy = new SqlParameter("@AssignedBy", SqlDbType.Int);

                        SqlParameter spmJrDescriptions = new SqlParameter("@JrDescriptions", SqlDbType.VarChar);
                        SqlParameter spmRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                        SqlParameter spmJrID = new SqlParameter("@JrID", SqlDbType.Int);
                        SqlParameter spmCurrentStatus = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                        SqlParameter spmOperation = new SqlParameter("@Operation", SqlDbType.VarChar);

                        SqlParameter Status = cmd.Parameters.Add("@ResultStatus", SqlDbType.Int);
                        Status.Direction = ParameterDirection.Output;

                        cmd.Parameters.Add(spmJrID).Value = objEmpJr.JrID;
                        cmd.Parameters.Add(spmDeptID).Value = objEmpJr.DeptID;
                        cmd.Parameters.Add(spmJR_TemplateID).Value = objEmpJr.JR_TemplateID;
                        cmd.Parameters.Add(spmEmpID).Value = objEmpJr.EmpID;
                        cmd.Parameters.Add(spmJrName).Value = objEmpJr.Jr_Name;

                        cmd.Parameters.Add(spmAssignedBy).Value = objEmpJr.AssignedBy;

                        cmd.Parameters.Add(spmJrDescriptions).Value = objEmpJr.JrDescription;
                        cmd.Parameters.Add(spmRemarks).Value = objEmpJr.Remarks;
                        cmd.Parameters.Add(spmCurrentStatus).Value = objEmpJr.CurrentStatus;
                        cmd.Parameters.Add(spmOperation).Value = Operation;

                        cmd.ExecuteNonQuery();
                        Result = cmd.Parameters["@ResultStatus"].Value.ToString();
                        cmd.Dispose();
                    }
                }
                ResultStatus = Convert.ToInt32(Result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }



        public DataTable GetJRsToAcceptDb(int empID, int status)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_GetEmployeeJR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpId = new SqlParameter("@EmpID", SqlDbType.Int);
                SqlParameter SPStatus = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpId).Value = empID;
                cmd.Parameters.Add(SPStatus).Value = status;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                cmd.Dispose();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int AcceptMyJRDb(int JRID, int EmpID, int Status, out int ResultStatus)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_EmpAcceptedJR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPJRID = new SqlParameter("@JRID", SqlDbType.Int);
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                SqlParameter SPStatus = new SqlParameter("@Status", SqlDbType.Int);

                SqlParameter SpResultStatus = cmd.Parameters.Add("@ResultStatus", SqlDbType.Int);
                SpResultStatus.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(SPJRID).Value = JRID;
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                cmd.Parameters.Add(SPStatus).Value = Status;
                int Count = cmd.ExecuteNonQuery();
                string Result = cmd.Parameters["@ResultStatus"].Value.ToString();
                ResultStatus = Convert.ToInt32(Result);
                cmd.Dispose();
                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region TMS_DashBoard
        public DataTable GetATC_DocumentDetailsDb(int ideptID, int iMonth)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_GetATC_DetailsByDeptIDandMonth", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmDeptID).Value = ideptID;
                SqlParameter spmMonth = new SqlParameter("@Month", SqlDbType.Int);
                cmd.Parameters.Add(spmMonth).Value = iMonth;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                cmd.Dispose();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetDocCountandMonthDb(int ideptID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_GetATC_DetailsByDeptID", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmDeptID).Value = ideptID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                cmd.Dispose();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetDataTODashBoardDb(int EmpID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_GetDataToTMSDashBoard", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmEmp_ID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(spmEmp_ID).Value = EmpID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                cmd.Dispose();
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region JR_Template
        public DataTable getTemplateDetailsDb(int itempID)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_GetJR_TemplateDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@JR_TemplateID", itempID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        #endregion

        #region Time
        public int ConfirmOralExamSopReadDb(int iJR_ID, int isopID, int iexamTypeConfirm, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_TraineeConfirmedOralExam", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter SPJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPJR_ID).Value = iJR_ID;

                SqlParameter SPsopID = new SqlParameter("@DocumentID", SqlDbType.Int);
                cmd.Parameters.Add(SPsopID).Value = isopID;

                SqlParameter SPexamTypeConfirm = new SqlParameter("@ExamTypeConfirm", SqlDbType.Int);
                cmd.Parameters.Add(SPexamTypeConfirm).Value = iexamTypeConfirm;

                SqlParameter spJRStatus = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(spJRStatus).Direction = ParameterDirection.Output;

                // cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;

                int Count = cmd.ExecuteNonQuery();

                Result = Convert.ToInt32(cmd.Parameters["@Status"].Value);

                // Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);

                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public DataSet getExamResultDataDb(int BaseID, int BaseType, int DocID = 0)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetExamResultData]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmBaseID = new SqlParameter("@BasedID", SqlDbType.Int);
                cmd.Parameters.Add(spmBaseID).Value = BaseID;
                SqlParameter spmBaseType = new SqlParameter("@BaseType", SqlDbType.Int);
                cmd.Parameters.Add(spmBaseType).Value = BaseType;
                if (DocID != 0)
                {
                    SqlParameter spmDocID = new SqlParameter("@DocumentID", SqlDbType.Int);
                    cmd.Parameters.Add(spmDocID).Value = DocID;
                }
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SubmitJRExamResultDb(int iJR_ID, int iDocID, bool passedOutStatus, int marks, DataTable dtTraineeAnswerSheet, string ExplainedStatus, out int iResult, out int Attempts)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.[AizantIT_SP_JrTraineeExamSubmission]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@QueIDAndTraineeAnswer", dtTraineeAnswerSheet);
                SqlParameter parmJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(parmJR_ID).Value = iJR_ID;
                SqlParameter parmDocID = new SqlParameter("@DocumentID", SqlDbType.Int);
                cmd.Parameters.Add(parmDocID).Value = iDocID;
                SqlParameter parmMarks = new SqlParameter("@Marks", SqlDbType.VarChar);
                cmd.Parameters.Add(parmMarks).Value = marks;
                SqlParameter parmExplainedStatus = new SqlParameter("@ExplainedStatus", SqlDbType.VarChar, 2);
                cmd.Parameters.Add(parmExplainedStatus).Value = ExplainedStatus;
                SqlParameter parmPassedOutStatus = new SqlParameter("@PassedOutStatus", SqlDbType.Bit);
                cmd.Parameters.Add(parmPassedOutStatus).Value = passedOutStatus;
                SqlParameter parmStatus = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(parmStatus).Direction = ParameterDirection.Output;
                SqlParameter parmAttempts = new SqlParameter("@Attempts", SqlDbType.Int);
                cmd.Parameters.Add(parmAttempts).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                iResult = Convert.ToInt32(cmd.Parameters["@Status"].Value);
                Attempts = Convert.ToInt32(cmd.Parameters["@Attempts"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public DataSet getSopQuestionnaireForExamDb(int isopID, out int ExamType)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetDocQuestionnaireForExam]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmSop_ID = new SqlParameter("@DocumentID", SqlDbType.Int);
                SqlParameter spmSopExamtype = new SqlParameter("@DocumentExamtype", SqlDbType.Int);
                cmd.Parameters.Add(spmSop_ID).Value = isopID;
                cmd.Parameters.Add(spmSopExamtype).Direction = ParameterDirection.Output;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                string rlt = cmd.Parameters["@DocumentExamtype"].Value.ToString();
                ExamType = Convert.ToInt32(rlt);
                return ds;
            }
            catch (Exception em)
            {
                throw em;
            }
        }

        public void GetTTS_StatusCloseDAL(string BaseID, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTTS_StatusClose]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parmBaseID = new SqlParameter("@BaseID", SqlDbType.Int);
                cmd.Parameters.Add(parmBaseID).Value = BaseID;
                SqlParameter spmResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                spmResult.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public void ToGetTTS_StatusCloseDAL(string TTSID, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ToGetTTS_StatusClose]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parmTTSID = new SqlParameter("@TTSID", SqlDbType.Int);
                cmd.Parameters.Add(parmTTSID).Value = TTSID;
                SqlParameter spmResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                spmResult.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public DataTable getFeedbackQuestionListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int FeedbackID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetFeedbackQuestionList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramFeedbackID = new SqlParameter("@FeedbackID", SqlDbType.Int);
                cmd.Parameters.Add(paramFeedbackID).Value = FeedbackID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable getFeedbackHistoryListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int FeedbackID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetFeedbackHistoryList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramFeedbackID = new SqlParameter("@FeedbackID", SqlDbType.Int);
                cmd.Parameters.Add(paramFeedbackID).Value = FeedbackID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable FeedbackForCreationDb(int FeedbackType, out int FeedbackStatus)
        {
            try
            {

                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetFeedbackForCreation]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmSop_ID = new SqlParameter("@FeedbackType", SqlDbType.Int);
                SqlParameter spmSopExamtype = new SqlParameter("@FeedbackStatus", SqlDbType.Int);
                cmd.Parameters.Add(spmSop_ID).Value = FeedbackType;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                //if (dt==null)
                //{
                //   FeedbackStatus = 2;
                //}
                //else
                //{
                //    FeedbackStatus = 1;
                //}
                if (dt.Rows.Count > 0)
                {
                    FeedbackStatus = 1;
                }
                else
                {
                    FeedbackStatus = 2;
                }
                return dt;
            }
            catch (Exception em)
            {
                throw em;
            }
        }

        public int InsertFeedbackFormdb(int EmpID, int BaseID, int BasedOn, DataTable dtFeedback, string FeedbackComments, out int Result)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TakeEmployeeFeedback]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                SqlParameter spmBaseID = new SqlParameter("@BaseID", SqlDbType.Int);
                SqlParameter spmBasedOn = new SqlParameter("@BasedOn", SqlDbType.Int);
                SqlParameter spmFeedbackComment = new SqlParameter("@FeedbackComment", SqlDbType.VarChar);
                cmd.Parameters.Add(spmEmpID).Value = EmpID;
                cmd.Parameters.Add(spmBaseID).Value = BaseID;
                cmd.Parameters.Add(spmBasedOn).Value = BasedOn;
                cmd.Parameters.AddWithValue("@Feedback", dtFeedback);
                cmd.Parameters.Add(spmFeedbackComment).Value = FeedbackComments;
                SqlParameter SPFeedBackResult = cmd.Parameters.Add("@FeedBackResult", SqlDbType.Int);
                SPFeedBackResult.Direction = ParameterDirection.Output;
                int i = cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@FeedBackResult"].Value);
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public DataSet ViewEmployeeFeedbackDb(int EmpFeedbackID, int FeedbackType)
        {
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_ViewEmployeeFeedback]", con);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@EmpFeedbackID", EmpFeedbackID);
                sda.SelectCommand.Parameters.AddWithValue("@FeedbackType", FeedbackType);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataTable ViewRemarksdb(int SessionID)
        {
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_GetTrainingSessionRemarks]", con);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@SessionID", SessionID);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataSet getDocInfoDb(int jR_ID, int docID, int docVersionID, out int docVersionStatus)
        {
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_GetTrainingDocumentDetails]", con);

                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter spmJrID = new SqlParameter("@JR_ID", SqlDbType.Int);
                SqlParameter spmDocID = new SqlParameter("@DocumentID", SqlDbType.Int);
                SqlParameter spmDocVersionID = new SqlParameter("@DocVersionID", SqlDbType.Int);
                SqlParameter spmIsActiveVersion = new SqlParameter("@DocVersionStatus", SqlDbType.Int);

                sda.SelectCommand.Parameters.Add(spmJrID).Value = jR_ID;
                sda.SelectCommand.Parameters.Add(spmDocID).Value = docID;
                sda.SelectCommand.Parameters.Add(spmDocVersionID).Value = docVersionID;
                sda.SelectCommand.Parameters.Add(spmIsActiveVersion).Direction = ParameterDirection.Output;

                DataSet ds = new DataSet();
                sda.Fill(ds);
                docVersionStatus = Convert.ToInt32(sda.SelectCommand.Parameters["@DocVersionStatus"].Value);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateDocDurationDb(int JR_ID, int docVersionID, int docID, int Hours, int Minutes, int Seconds)
        {
            try
            {
                con.Open();
                // SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_Trainee_SOP_Time_Insert]", con); old
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_RecordReadTimeOnDocument]", con);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmJrID = new SqlParameter("@JrID", SqlDbType.Int);
                SqlParameter spmDocID = new SqlParameter("@DocID", SqlDbType.Int);
                SqlParameter spmDocVersionID = new SqlParameter("@DocVersionID", SqlDbType.Int);
                SqlParameter spmHours = new SqlParameter("@Hours", SqlDbType.Int);
                SqlParameter spmMinutes = new SqlParameter("@Minutes", SqlDbType.Int);
                SqlParameter spmSeconds = new SqlParameter("@Seconds", SqlDbType.Int);

                cmd.Parameters.Add(spmJrID).Value = JR_ID;
                cmd.Parameters.Add(spmDocID).Value = docID;
                cmd.Parameters.Add(spmDocVersionID).Value = docVersionID;
                cmd.Parameters.Add(spmHours).Value = Hours;
                cmd.Parameters.Add(spmMinutes).Value = Minutes;
                cmd.Parameters.Add(spmSeconds).Value = Seconds;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        //Target Doc Read Duration
        public void UpdateTraineeDocDurationDb(int TTS_ID, int TraineeIDs, int Hours, int Minutes, int Seconds)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_UpdateTraineeReadTimeOnDocument]", con);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                SqlParameter spmTraineeIDs = new SqlParameter("@TraineeIDs", SqlDbType.Int);
                SqlParameter spmHours = new SqlParameter("@Hours", SqlDbType.Int);
                SqlParameter spmMinutes = new SqlParameter("@Minutes", SqlDbType.Int);
                SqlParameter spmSeconds = new SqlParameter("@Seconds", SqlDbType.Int);

                cmd.Parameters.Add(spmTTS_ID).Value = TTS_ID;
                cmd.Parameters.Add(spmTraineeIDs).Value = TraineeIDs;
                cmd.Parameters.Add(spmHours).Value = Hours;
                cmd.Parameters.Add(spmMinutes).Value = Minutes;
                cmd.Parameters.Add(spmSeconds).Value = Seconds;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable getDocDataToViewDb(int DocID, int DocVersionID, int BaseOn, int BaseID)
        {
            try
            {
                //SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_TrainingSopContent]", con); old
                SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_TrainingDocumentContent]", con);

                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@DocumentID", DocID);
                sda.SelectCommand.Parameters.AddWithValue("@DocVersionID", DocVersionID);
                sda.SelectCommand.Parameters.AddWithValue("@BaseOn", BaseOn);
                sda.SelectCommand.Parameters.AddWithValue("@BaseID", BaseID);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ActiveDocumentContentDb(int DocID)
        {
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_ActiveDocumentContent]", con);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@DocumentID", DocID);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region HOD Evalution

        public void EvalutedJrByHodDb(int JrID, int HodEmpID, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_EvaluateJrByHod]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmJrID = new SqlParameter("@JrID", SqlDbType.Int);
                SqlParameter spmHodEmpID = new SqlParameter("@HodEmpID", SqlDbType.Int);
                cmd.Parameters.Add(spmJrID).Value = JrID;
                cmd.Parameters.Add(spmHodEmpID).Value = HodEmpID;
                SqlParameter spmEvalationStatus = cmd.Parameters.Add("@EvalationStatus", SqlDbType.Int);
                spmEvalationStatus.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@EvalationStatus"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
        }
        #endregion

        #region Trainer Evaluation
        //need to review
        public void SubmitTrainerEvaluationForJRDb(int JR_ID, int DocID, int Trainer_EmpID, string Explained_Status, out int result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JR_TrainerEvaluation]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPTrainer_EmpID = new SqlParameter("@Trainer_EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPTrainer_EmpID).Value = Trainer_EmpID;
                SqlParameter SPJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPJR_ID).Value = JR_ID;
                SqlParameter SPDocID = new SqlParameter("@DocumentID", SqlDbType.Int);
                cmd.Parameters.Add(SPDocID).Value = DocID;
                SqlParameter SPOpinion_Status = new SqlParameter("@ExplainStatus", SqlDbType.VarChar, 2);
                cmd.Parameters.Add(SPOpinion_Status).Value = Explained_Status;
                SqlParameter SPResult = cmd.Parameters.Add("@EvaluationStatus", SqlDbType.Int);
                SPResult.Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@EvaluationStatus"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Training Schedule
        public DataSet getViewTDb(int jR_ID)
        {
            try
            {
                //SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_ViewTS]", con);
                //sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                //sda.SelectCommand.Parameters.AddWithValue("@JR_ID", jR_ID);
                //DataTable dt = new DataTable();
                //sda.Fill(dt);
                //return dt;

                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ViewTS]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@JR_ID", jR_ID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SubmitTS_ApprovalDb(int empid, int jR_ID, int iStatus, string comments, out int result)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_TS_Approval", con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ApprovalBy", SqlDbType.Int).Value = empid;
                cmd.Parameters.Add("@JR_ID", SqlDbType.Int).Value = jR_ID;
                cmd.Parameters.Add("@Status", SqlDbType.Int).Value = iStatus;
                cmd.Parameters.Add("@ApprovalComments", SqlDbType.VarChar).Value = comments;
                cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public void CreateTsDb(DataSet ds, int JR_ID, int CreatedBy, int Reviewer, string HOD_Remarks, out int result)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_CreateTS", con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Trainers", SqlDbType.Structured).Value = ds.Tables["Trainers"];
                cmd.Parameters.Add("@Documents", SqlDbType.Structured).Value = ds.Tables["Sops"];
                cmd.Parameters.Add("@DeptDuration", SqlDbType.Structured).Value = ds.Tables["Durations"];
                cmd.Parameters.Add("@JR_ID", SqlDbType.Int).Value = JR_ID;
                cmd.Parameters.Add("@Reviewer", SqlDbType.Int).Value = Reviewer;
                cmd.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = HOD_Remarks;
                cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = CreatedBy;
                cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public void ModifyTsDb(DataSet ds, int jR_ID, int empID, string HOD_Remarks, out int result)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ModifyTS]", con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Trainers", SqlDbType.Structured).Value = ds.Tables[0];
                cmd.Parameters.Add("@Documents", SqlDbType.Structured).Value = ds.Tables[1];
                cmd.Parameters.Add("@DeptDuration", SqlDbType.Structured).Value = ds.Tables[2];
                cmd.Parameters.Add("@JR_ID", SqlDbType.Int).Value = jR_ID;
                cmd.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = HOD_Remarks;
                cmd.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = empID;
                cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable GetDeptWiseSOPsDb(int DeptID)
        {
            try
            {
                //SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_GetDeptSops]", con); old
                SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_GetDeptDocuments]", con);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@DeptID", DeptID);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Questionnaire
        public DataTable GetActiveDocsDb(int iDepID, int DocType)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetActiveDocuments]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPiDepID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(SPiDepID).Value = iDepID;
                SqlParameter SPDocType = new SqlParameter("@DocType", SqlDbType.Int);
                cmd.Parameters.Add(SPDocType).Value = DocType;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void QuestionnaireDb(QuestionnaireObjects objQue, DataTable dtOptions, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.[AizantIT_SP_Questionnaire]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@QuestionOptions", dtOptions);
                SqlParameter SPCreatedBy = new SqlParameter("@Created_ModifiedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPCreatedBy).Value = objQue.createdOrModifiedBy;
                SqlParameter SPOperation = new SqlParameter("@Operation", SqlDbType.VarChar);
                cmd.Parameters.Add(SPOperation).Value = objQue.Operation;
                SqlParameter SPQuestion = new SqlParameter("@QuestionTitle", SqlDbType.VarChar);
                cmd.Parameters.Add(SPQuestion).Value = objQue.Question_Descriptipon;
                SqlParameter SPAnswer = new SqlParameter("@Answer", SqlDbType.VarChar);
                cmd.Parameters.Add(SPAnswer).Value = objQue.Question_Answer;
                SqlParameter SPSOP_ID = new SqlParameter("@DocumentID", SqlDbType.Int);
                cmd.Parameters.Add(SPSOP_ID).Value = objQue.DocID;
                SqlParameter SPQueID = new SqlParameter("@QuestionId", SqlDbType.Int);
                cmd.Parameters.Add(SPQueID).Value = objQue.QueID;
                SqlParameter SPStatus = new SqlParameter("@StatusID", SqlDbType.Int);
                cmd.Parameters.Add(SPStatus).Value = objQue.CurrentStatus;
                cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public DataTable GetDeptWiseDocsForQuestionnaireApprovalOrRevertDb(int ideptID, int iEmPId, string type, int DocType)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetQuestionnaireByStatus]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPideptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(SPideptID).Value = ideptID;
                SqlParameter SPiEmPId = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPiEmPId).Value = iEmPId;
                SqlParameter SPtype = new SqlParameter("@Type", SqlDbType.VarChar);
                cmd.Parameters.Add(SPtype).Value = type;
                SqlParameter SPDocType = new SqlParameter("@DocType", SqlDbType.VarChar);
                cmd.Parameters.Add(SPDocType).Value = DocType;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet gvDocQuestionnaireDb(int DocumentID, int EmpID, out int OnTraining)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetDocQuestionnaireForCreator]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPDocumentID = new SqlParameter("@DocumentID", SqlDbType.Int);
                cmd.Parameters.Add(SPDocumentID).Value = DocumentID;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                cmd.Parameters.Add("@TrainingCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                OnTraining = Convert.ToInt32(cmd.Parameters["@TrainingCount"].Value);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataSet gvSopQuestionnaireApprovalDb(int SopID, int Status, int EmpID)
        {
            try
            {
                //SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetSopQuestionnaireForApproval]", con); old
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetDocQuestionnaireForApproval]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPSOP_ID = new SqlParameter("@DocumentID", SqlDbType.Int);
                cmd.Parameters.Add(SPSOP_ID).Value = SopID;
                SqlParameter SPStatus = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(SPStatus).Value = Status;
                SqlParameter SPApprovalEmpID = new SqlParameter("@Approval_EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPApprovalEmpID).Value = EmpID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataSet GetQuestionnairetoEditDb(int queID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetQuestionnaireToEdit]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@QueID", queID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void DeleteQuestionnaireDb(int queID, out int DelStatus)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.[AizantIT_SP_Questionnaire_Delete]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPQueID = new SqlParameter("@QueID", SqlDbType.Int);
                cmd.Parameters.Add(SPQueID).Value = queID;
                cmd.Parameters.Add("@DelStatus", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                DelStatus = Convert.ToInt32(cmd.Parameters["@DelStatus"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public int InActiveQuestionnaireDb(int SopID, int queID, int Status, int EmpID, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.[AizantIT_SP_Questionnaire_InActive]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPSOP_ID = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(SPSOP_ID).Value = Status;
                SqlParameter SPQueID = new SqlParameter("@QueID", SqlDbType.Int);
                cmd.Parameters.Add(SPQueID).Value = queID;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPSopID = new SqlParameter("@DocumentID", SqlDbType.Int);
                cmd.Parameters.Add(SPSopID).Value = SopID;
                cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public int RevertQuestionnaireDb(int queID, string revertReason, int Empid, int Status, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.[AizantIT_SP_Questionnaire_Revert]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPRevertReason = new SqlParameter("@RevertReason", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRevertReason).Value = revertReason;
                SqlParameter SPQueID = new SqlParameter("@QueID", SqlDbType.Int);
                cmd.Parameters.Add(SPQueID).Value = queID;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = Empid;
                SqlParameter SPStatus = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(SPStatus).Value = Status;
                cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public int AcceptQuestionnaireDb(int iSopid, int empid, int iStatus, DataTable dtQuestionIds)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.[AizantIT_SP_Questionnaire_Accept]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@QuestionIDs", dtQuestionIds);
                SqlParameter SPSopID = new SqlParameter("@DocumentID", SqlDbType.Int);
                cmd.Parameters.Add(SPSopID).Value = iSopid;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = empid;
                SqlParameter SPStatus = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(SPStatus).Value = iStatus;
                int Count = cmd.ExecuteNonQuery();
                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public DataSet getModuleWiseDeptsAndDocTypesDb(int empID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetModuleWiseDeptsAndDocTypes]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = empID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public DataTable DocTypedb()
        {
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_DocumentType]", con);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int RetJustificationNote_Insert(EmpJrObjects objEmpJrObjects, out int Status)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JustificationNote_Insert]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.VarChar);
                SqlParameter SPRQ_JustificationNote = new SqlParameter("@RQ_JustificationNote", SqlDbType.VarChar);//
                SqlParameter SPCommentDate = new SqlParameter("@CommentDate", SqlDbType.VarChar);
                cmd.Parameters.Add("@Status", SqlDbType.Int).Direction = ParameterDirection.Output;

                cmd.Parameters.Add(SPJR_ID).Value = objEmpJrObjects.JrID;
                cmd.Parameters.Add(SPEmpID).Value = objEmpJrObjects.EmpID;
                cmd.Parameters.Add(SPRQ_JustificationNote).Value = objEmpJrObjects.RQ_JustificationNote;
                cmd.Parameters.Add(SPCommentDate).Value = objEmpJrObjects.CommentDate;
                int count = cmd.ExecuteNonQuery();
                Status = Convert.ToInt32(cmd.Parameters["@Status"].Value);
                cmd.Dispose();
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public DataTable GetRetrospectiveNoteData(int jR_ID)
        {
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_GetJustificationNote]", con);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@JR_ID", jR_ID);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region ATC
        public void SubmitInitializeATCDb(int iDeptID, int year, int iInitiatedBy, string remarks, int iAtcAuthor_ID, int iHOD_Approval_ID, int iQA_Approval_ID, string Notification_Link, out int creationStatus)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_InitializeATC]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(SPDeptID).Value = iDeptID;
                SqlParameter SPYear = new SqlParameter("@CalendarYear", SqlDbType.Int);
                cmd.Parameters.Add(SPYear).Value = year;
                SqlParameter SPEmpID = new SqlParameter("@InitiateBy", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = iInitiatedBy;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = remarks;
                SqlParameter SPNotificationLink = new SqlParameter("@NotificationLink", SqlDbType.VarChar);
                cmd.Parameters.Add(SPNotificationLink).Value = Notification_Link;
                SqlParameter SPiAtcAuthor_ID = new SqlParameter("@ATC_Author", SqlDbType.Int);
                cmd.Parameters.Add(SPiAtcAuthor_ID).Value = iAtcAuthor_ID;
                SqlParameter SPiHOD_Approval_ID = new SqlParameter("@ATC_HOD_Approval", SqlDbType.Int);
                cmd.Parameters.Add(SPiHOD_Approval_ID).Value = iHOD_Approval_ID;
                SqlParameter SPiQA_Approval_ID = new SqlParameter("@ATC_QA_Approval", SqlDbType.Int);
                cmd.Parameters.Add(SPiQA_Approval_ID).Value = iQA_Approval_ID;
                cmd.Parameters.Add("@CreationStatus", SqlDbType.Int).Direction = ParameterDirection.Output;
                int count = cmd.ExecuteNonQuery();
                creationStatus = Convert.ToInt32(cmd.Parameters["@CreationStatus"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public DataTable GetAssigned_ATC_List(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int empID, int NotificationID, out int TotalCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetAssigned_ATC]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@AuthorID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = empID;
                SqlParameter paramNotificationID = new SqlParameter("@Notification_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                TotalCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetInitialize_ATC(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int empID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetInitialized_ATC]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@EmpID ", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = empID;
                // SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                //spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                //TotalCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To get JR Regular Records
        public DataTable GetJR_RegularRecordsDal(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, int JR_ID, int TrainerID, out int IsRetrospective)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJR_TrainingRegularRecords]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramempJR_ID).Value = JR_ID;
                SqlParameter paramempTrainerID = new SqlParameter("@TrainerID", SqlDbType.Int);
                cmd.Parameters.Add(paramempTrainerID).Value = TrainerID;
                SqlParameter spmIsRetrospective = cmd.Parameters.Add("@IsJrRetrospective", SqlDbType.Int);
                spmIsRetrospective.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                IsRetrospective = Convert.ToInt32(cmd.Parameters["@IsJrRetrospective"].Value);
                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To get JR Retrospective Records
        public DataTable GetJR_RetrospectiveRecordsDal(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, int JR_ID, int TrainerID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetJR_TrainingRetrospectiveRecords]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramempJR_ID).Value = JR_ID;
                SqlParameter paramempTrainerID = new SqlParameter("@TrainerID", SqlDbType.Int);
                cmd.Parameters.Add(paramempTrainerID).Value = TrainerID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable get_ATC_HistoryListdB(int iDisplayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int aTC_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetATC_HistoryList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = displayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = sortCol;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@ATC_ID ", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = aTC_ID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetATC_PendingList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
            string sSortDir_0, string sSearch, int empID, int FilterType, int NotificationID, out int TotalCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetATC_PendingList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@AuthorID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = empID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramNotificationID = new SqlParameter("@Notification_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                TotalCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetATC_Hod_Approval(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int empID, int FilterType, int NotificationID, out int TotalCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetATC_ReviewerList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@ReviewerID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = empID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramNotificationID = new SqlParameter("@Notification_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                TotalCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetATC_QA_Approval(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int empID, int FilterType, int NotificationID, out int TotalCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetATC_ApproverList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@ApproverID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = empID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramNotificationID = new SqlParameter("@Notification_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                TotalCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //TTS_Exam Result
        public DataTable GetTTS_ExamDetailsdb(int iDisplayLength, int iDisplayStart, int iSortCol_0,
           string sSortDir_0, string sSearch, int TTS_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTTS_TraineeExamDetails]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = TTS_ID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateInitializedATCDb(int iATC_ID, int iModifiedBy_ID, string remarks, int iATC_Author_ID, int iATC_HOD_Approval_ID, int iATC_QA_Approval_ID, out int creationStatus)
        {
            try
            {
                //Not in use
                //SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ModifyInitializeATC]", con);
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_UpdateInitializeATC]", con);

                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPATC_ID = new SqlParameter("@ATC_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPATC_ID).Value = iATC_ID;
                SqlParameter SPModifiedBy_ID = new SqlParameter("@ModifiedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPModifiedBy_ID).Value = iModifiedBy_ID;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = remarks;
                SqlParameter SPiAtcAuthor_ID = new SqlParameter("@ModifiedATC_Author", SqlDbType.Int);
                cmd.Parameters.Add(SPiAtcAuthor_ID).Value = iATC_Author_ID;
                SqlParameter SPiHOD_Approval_ID = new SqlParameter("@ModifiedATC_HOD_Approval", SqlDbType.Int);
                cmd.Parameters.Add(SPiHOD_Approval_ID).Value = iATC_HOD_Approval_ID;
                SqlParameter SPiQA_Approval_ID = new SqlParameter("@ModifiedATC_QA_Approval", SqlDbType.Int);
                cmd.Parameters.Add(SPiQA_Approval_ID).Value = iATC_QA_Approval_ID;
                //SqlParameter Status = cmd.Parameters.Add("@creationStatus", SqlDbType.Int);
                //Status.Direction = ParameterDirection.Output;
                //SqlDataAdapter da = new SqlDataAdapter(cmd);
                //DataTable dt = new DataTable();
                //da.Fill(dt);
                //string Result = cmd.Parameters["@creationStatus"].Value.ToString();
                //creationStatus = Convert.ToInt32(Result);
                //return dt;

                cmd.Parameters.Add("@ModifiedStatus", SqlDbType.Int).Direction = ParameterDirection.Output;
                int count = cmd.ExecuteNonQuery();
                creationStatus = Convert.ToInt32(cmd.Parameters["@ModifiedStatus"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public DataSet getATC_DataforModificationDb(int aTC_ID)
        {
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_ATC_GetDataForAuthorModification]", con);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@ATC_ID", aTC_ID);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get Initialise ATC Details
        public DataSet GetInitializeATCDetails(int iATC_ID)
        {
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_GetInitializeATC_Details]", con);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@ATC_ID", iATC_ID);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // To Get Effected Docs For ATC Creation 
        public DataSet GetEffectedDocsforAtcCreationdb(int DocumentTypeID, int DeptID)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetDocumentsOnEffective]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DocType", DocumentTypeID);
                cmd.Parameters.AddWithValue("@DeptID", DeptID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Annual Training Calender View icon 
        public DataSet GetAnnualTrainingCalenderView(int AtcID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("TMS.[AizantIT_SP_ATC_GetDetailstoView]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AtcID", AtcID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // for getting to JQuery datatable
        public DataTable GetATC_List(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int empID, int NotificationID, out int TotalCount)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetATC_List]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramempID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = empID;
                SqlParameter paramNotificationID = new SqlParameter("@Notification_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramNotificationID).Value = NotificationID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                TotalCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        // AnnualTraining Calender Creation
        public int InsertTrainingCalender(int DeptID, int Year, int EmpID, string Remarks, DataTable dtResult, int InitializeATC_ID, out int CreationStatus)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_CreateTrainingCalendar]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(SPDeptID).Value = DeptID;
                SqlParameter SPYear = new SqlParameter("@Year", SqlDbType.Int);
                cmd.Parameters.Add(SPYear).Value = Year;
                SqlParameter SPEmpID = new SqlParameter("@CreatedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = Remarks;
                cmd.Parameters.AddWithValue("@Calendar", dtResult);
                SqlParameter SPInitializeATC_ID = new SqlParameter("@ATC_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPInitializeATC_ID).Value = InitializeATC_ID;
                cmd.Parameters.Add("@CreationStatus", SqlDbType.Int).Direction = ParameterDirection.Output;
                //SqlParameter Status = cmd.Parameters.Add("@CreationStatus", SqlDbType.Int);
                //Status.Direction = ParameterDirection.Output;
                int count = cmd.ExecuteNonQuery();
                CreationStatus = Convert.ToInt32(cmd.Parameters["@CreationStatus"].Value);
                cmd.Dispose();
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        // AnnualTraining Calender Update by Trainer
        public void UpdateTrainingCalenderTrainer(int AtcID, int ActionBy, string Remarks, DataTable dtResult, int ActionActor, int ActionState, out int CreationStatus)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_Trainer_ModifyTrainingCalendar]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPAtcID = new SqlParameter("@AtcID", SqlDbType.Int);
                cmd.Parameters.Add(SPAtcID).Value = AtcID;
                SqlParameter SPActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(SPActionBy).Value = ActionBy;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = Remarks;
                cmd.Parameters.AddWithValue("@Calendar", dtResult);
                SqlParameter SPActionActor = new SqlParameter("@ActionActor", SqlDbType.Int);
                cmd.Parameters.Add(SPActionActor).Value = ActionActor;
                SqlParameter SPActionState = new SqlParameter("@ActionState", SqlDbType.Int);
                cmd.Parameters.Add(SPActionState).Value = ActionState;
                cmd.Parameters.Add("@CreationStatus", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                CreationStatus = Convert.ToInt32(cmd.Parameters["@CreationStatus"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        // AnnualTraining Calender Update By Hod And QA
        public void UpdateTrainingCalender(int AtcID, int ActionBy, string Remarks, int ActionActor, int ActionState, out int CreationStatus)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ModifyTrainingCalendar]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPAtcID = new SqlParameter("@AtcID", SqlDbType.Int);
                cmd.Parameters.Add(SPAtcID).Value = AtcID;
                SqlParameter SPActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(SPActionBy).Value = ActionBy;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = Remarks;
                SqlParameter SPActionActor = new SqlParameter("@ActionActor", SqlDbType.Int);
                cmd.Parameters.Add(SPActionActor).Value = ActionActor;
                SqlParameter SPActionState = new SqlParameter("@ActionState", SqlDbType.Int);
                cmd.Parameters.Add(SPActionState).Value = ActionState;
                cmd.Parameters.Add("@CreationStatus", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                CreationStatus = Convert.ToInt32(cmd.Parameters["@CreationStatus"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        #endregion
        public DataSet GetRoleWiseEmployees(int DeptID, int IsEmpActive = 0)
        {
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_GetRoleWiseEmpForTTS]", con);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@DeptID", DeptID);
                sda.SelectCommand.Parameters.AddWithValue("@IsEmpActive", IsEmpActive);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetEmployeeJrList_View(int JR_ID)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_EmployeeJRListView", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@JR_ID", JR_ID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get JR Training List
        public DataSet GetJrTrainingList_Viewdb(int JR_ID)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JR_Content]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@JR_ID", JR_ID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Trainee Asked Question
        public void TraineeAskQuestiondb(int TraineeID, string Question, int DocID, int BaseID, int BasedOn, out int Result)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TraineeAskedQuestion]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramTraineeID = new SqlParameter("@TraineeID", SqlDbType.Int);
                cmd.Parameters.Add(paramTraineeID).Value = TraineeID;
                SqlParameter paramQuestion = new SqlParameter("@Question", SqlDbType.VarChar);
                cmd.Parameters.Add(paramQuestion).Value = Question;
                SqlParameter paramDocID = new SqlParameter("@DocID", SqlDbType.Int);
                cmd.Parameters.Add(paramDocID).Value = DocID;
                SqlParameter paramBaseID = new SqlParameter("@BaseID", SqlDbType.Int);
                cmd.Parameters.Add(paramBaseID).Value = BaseID;
                SqlParameter paramBasedOn = new SqlParameter("@BasedOn", SqlDbType.Int);
                cmd.Parameters.Add(paramBasedOn).Value = BasedOn;
                SqlParameter Status = cmd.Parameters.Add("@Result", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                string Count = cmd.Parameters["@Result"].Value.ToString();
                Result = Convert.ToInt32(Count);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int InseretJR_PrintLogDb(int iJR_ID, int iEmpID, int iTF_Type)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_JR_PrintLog_Insert]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPJR_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPJR_ID).Value = iJR_ID;
                SqlParameter SPiEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPiEmpID).Value = iEmpID;
                SqlParameter SPiTF_Type = new SqlParameter("@TF_Type", SqlDbType.Int);
                cmd.Parameters.Add(SPiTF_Type).Value = iTF_Type;
                int count = cmd.ExecuteNonQuery();
                cmd.Dispose();
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region FeedbackForm
        //Feedback Creation
        public void CreateFeedbackFormDb(string Title, int FeedbackType, int EmpID, string Remarks, int Status, int FeedbackID, out int FeedbackResult)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_CreateFeedback]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPTitle = new SqlParameter("@Title", SqlDbType.VarChar);
                cmd.Parameters.Add(SPTitle).Value = Title;
                SqlParameter SPFeedbackType = new SqlParameter("@FeedbackType", SqlDbType.Int);
                cmd.Parameters.Add(SPFeedbackType).Value = FeedbackType;
                SqlParameter SPEmpID = new SqlParameter("@CreatedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = Remarks;
                SqlParameter SPStatus = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(SPStatus).Value = Status;
                SqlParameter SPFeedbackID = new SqlParameter("@FeedbackID", SqlDbType.Int);
                cmd.Parameters.Add(SPFeedbackID).Value = FeedbackID;
                //SqlParameter spFeedBackID = cmd.Parameters.Add("@FeedbackID", SqlDbType.Int);
                //spFeedBackID.Direction = ParameterDirection.Output;
                SqlParameter spFeedbackResult = cmd.Parameters.Add("@FeedBackResult", SqlDbType.Int);
                spFeedbackResult.Direction = ParameterDirection.Output;
                int count = cmd.ExecuteNonQuery();
                cmd.Dispose();
                // FeedbackID = Convert.ToInt32(cmd.Parameters["@FeedbackID"].Value);
                //string Result = cmd.Parameters["@FeedbackID"].Value.ToString();
                //FeedbackID = Convert.ToInt32(Result);
                FeedbackResult = Convert.ToInt32(cmd.Parameters["@FeedBackResult"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        //Feedback Update 
        public void UpdateFeedbackForm(string Title, int FeedbackType, int EmpID, string Remarks, int Status, int FeedbackID, out int FeedBackResult)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_UpdateFeedbackForm]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPTitle = new SqlParameter("@Title", SqlDbType.VarChar);
                cmd.Parameters.Add(SPTitle).Value = Title;
                //SqlParameter SPFeedbackType = new SqlParameter("@FeedbackType", SqlDbType.Int);
                //cmd.Parameters.Add(SPFeedbackType).Value = FeedbackType;
                SqlParameter SPEmpID = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = Remarks;
                SqlParameter SPStatus = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(SPStatus).Value = Status;
                SqlParameter SPFeedbackID = new SqlParameter("@FeedbackID", SqlDbType.Int);
                cmd.Parameters.Add(SPFeedbackID).Value = FeedbackID;
                SqlParameter spFeedbackResult = cmd.Parameters.Add("@FeedBackResult", SqlDbType.Int);
                spFeedbackResult.Direction = ParameterDirection.Output;
                int count = cmd.ExecuteNonQuery();
                cmd.Dispose();
                string Result = cmd.Parameters["@FeedBackResult"].Value.ToString();
                FeedBackResult = Convert.ToInt32(Result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        //Insert Feedback Questions
        public int CreateFeedbackQuestions(int FeedbackID, string Question, string Remarks, int EmpID, string Title, int FeedbackType, out int FeedbackResultID, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_CreateFeedbackQuestions]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPFeedbackID = new SqlParameter("@FeedbackID", SqlDbType.Int);
                cmd.Parameters.Add(SPFeedbackID).Value = FeedbackID;
                SqlParameter SPQuestion = new SqlParameter("@Question", SqlDbType.VarChar);
                cmd.Parameters.Add(SPQuestion).Value = Question;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = Remarks;
                SqlParameter SPEmpID = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPTitle = new SqlParameter("@Title", SqlDbType.VarChar);
                cmd.Parameters.Add(SPTitle).Value = Title;
                SqlParameter SPFeedbackType = new SqlParameter("@FeedbackType", SqlDbType.Int);
                cmd.Parameters.Add(SPFeedbackType).Value = FeedbackType;
                SqlParameter SpFeedbackResultID = cmd.Parameters.Add("@FeedbackResultID", SqlDbType.Int);
                SpFeedbackResultID.Direction = ParameterDirection.Output;
                SqlParameter SpResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                SpResult.Direction = ParameterDirection.Output;
                int count = cmd.ExecuteNonQuery();
                cmd.Dispose();
                FeedbackResultID = Convert.ToInt32(cmd.Parameters["@FeedbackResultID"].Value.ToString());
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value.ToString());
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        //Feedback Form Questions Update
        public int UpdateFeedbackFormQuestions(int EmpID, string Question, string Remarks, int Status, int QuestionID, int FeedbackID, int FeedbackType, out int Result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_UpdateFeedbackQuestions]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPQuestion = new SqlParameter("@Question", SqlDbType.VarChar);
                cmd.Parameters.Add(SPQuestion).Value = Question;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = Remarks;
                SqlParameter SPStatus = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(SPStatus).Value = Status;
                SqlParameter SPQuestionID = new SqlParameter("@QuestionID", SqlDbType.Int);
                cmd.Parameters.Add(SPQuestionID).Value = QuestionID;
                SqlParameter SPFeedbackID = new SqlParameter("@FeedbackID", SqlDbType.Int);
                cmd.Parameters.Add(SPFeedbackID).Value = FeedbackID;
                SqlParameter SPFeedbackType = new SqlParameter("@FeedbackType", SqlDbType.Int);
                cmd.Parameters.Add(SPFeedbackType).Value = FeedbackType;
                SqlParameter SpResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                SpResult.Direction = ParameterDirection.Output;
                int count = cmd.ExecuteNonQuery();
                cmd.Dispose();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value.ToString());
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        //To Get FeedBackMaster Details.
        public DataTable GetFeedbackMasterDetailsDb(int FeedbackType)
        {
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter("[TMS].[AizantIT_SP_GetFeedbackMaster]", con);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@FeedbackType", FeedbackType);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        //To Get Employee Training Records
        public DataTable GetEmpTrainingRecords(int iDisplayLength, int iDisplayStart, int iSortCol_0,
            string sSortDir_0, string sSearch, int EmpID, string sSearch_DocumentName, string sSearch_DocRefNo, string sSearch_DeptName, string sSearch_ModeOfTraining, string sSearch_Opinion,
                  string sSearch_EvaluatedBy, string sSearch_EvaluatedDate, string sSearch_TrainingType, string sSearch_ReadDuration, out string TotalDuration)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetEmployeeTrainingRecords]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@EmpID ", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = EmpID;

                SqlParameter paramDocumentName = new SqlParameter("@sSearch_DocumentName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramDocumentName).Value = sSearch_DocumentName;
                SqlParameter paramDocRefNo = new SqlParameter("@sSearch_DocRefNo", SqlDbType.VarChar);
                cmd.Parameters.Add(paramDocRefNo).Value = sSearch_DocRefNo;
                SqlParameter paramDeptName = new SqlParameter("@sSearch_DeptName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramDeptName).Value = sSearch_DeptName;
                SqlParameter paramModeOfTraining = new SqlParameter("@sSearch_ModeOfTraining", SqlDbType.VarChar);
                cmd.Parameters.Add(paramModeOfTraining).Value = sSearch_ModeOfTraining;
                SqlParameter paramOpinion = new SqlParameter("@sSearch_Opinion", SqlDbType.VarChar);
                cmd.Parameters.Add(paramOpinion).Value = sSearch_Opinion;
                SqlParameter paramEvaluatedBy = new SqlParameter("@sSearch_EvaluatedBy", SqlDbType.VarChar);
                cmd.Parameters.Add(paramEvaluatedBy).Value = sSearch_EvaluatedBy;
                SqlParameter paramEvaluatedDate = new SqlParameter("@sSearch_EvaluatedDate", SqlDbType.VarChar);
                cmd.Parameters.Add(paramEvaluatedDate).Value = sSearch_EvaluatedDate;
                SqlParameter paramTrainingType = new SqlParameter("@sSearch_TrainingType", SqlDbType.VarChar);
                cmd.Parameters.Add(paramTrainingType).Value = sSearch_TrainingType;
                SqlParameter paramReadDuration = new SqlParameter("@sSearch_ReadDuration", SqlDbType.VarChar);
                cmd.Parameters.Add(paramReadDuration).Value = sSearch_ReadDuration;
                               
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                SqlParameter spmTotalDuration = cmd.Parameters.Add("@TotalDuration", SqlDbType.VarChar, 400);
                spmTotalDuration.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                TotalDuration = cmd.Parameters["@TotalDuration"].Value.ToString();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetMyTrainingRecordsDal(int iDisplayLength, int iDisplayStart, int iSortCol_0,
            string sSortDir_0, string sSearch, int EmpID, int FilterType, int Year, int DeptID, string TargetType)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetMyTrainingRecords]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@EmpID ", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = EmpID;
                SqlParameter paramFilterType = new SqlParameter("@FilterType", SqlDbType.Int);
                cmd.Parameters.Add(paramFilterType).Value = FilterType;
                SqlParameter paramYear = new SqlParameter("@Year", SqlDbType.Int);
                cmd.Parameters.Add(paramYear).Value = Year;
                SqlParameter paramDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlParameter paramTargetType = new SqlParameter("@TargetType", SqlDbType.VarChar);
                cmd.Parameters.Add(paramTargetType).Value = TargetType;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Get Document Wise Training Records List
        public DataTable GetDocWiseTrainingRecordsDb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int DocumentID, string FromDate,
            string ToDate, string sSearch_Document, string sSearch_VersionNumber, string sSearch_TraineeEmpCode, string sSearch_TraineeName, string sSearch_TrainingDate, string sSearch_TrainerName, string sSearch_Type)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetDocumentTrainingReports]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@DocumentID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = DocumentID;
                SqlParameter paramFromDate = new SqlParameter("@FromDate", SqlDbType.VarChar);
                if (FromDate == "")
                {
                    cmd.Parameters.Add(paramFromDate).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add(paramFromDate).Value = FromDate;
                }
                SqlParameter paramToDate = new SqlParameter("@ToDate", SqlDbType.VarChar);
                if (ToDate == "")
                {
                    cmd.Parameters.Add(paramToDate).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add(paramToDate).Value = ToDate;
                }

                SqlParameter paramsSearch_Document = new SqlParameter("@sSearch_Document", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Document).Value = sSearch_Document;
                SqlParameter paramsSearch_VersionNumber = new SqlParameter("@sSearch_VersionNumber", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_VersionNumber).Value = sSearch_VersionNumber;
                SqlParameter paramsSearch_TraineeEmpCode = new SqlParameter("@sSearch_TraineeEmpCode", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TraineeEmpCode).Value = sSearch_TraineeEmpCode;
                SqlParameter paramsSearch_TraineeName = new SqlParameter("@sSearch_TraineeName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TraineeName).Value = sSearch_TraineeName;
                SqlParameter paramsSearch_TrainingDate = new SqlParameter("@sSearch_TrainingDate", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TrainingDate).Value = sSearch_TrainingDate;
                SqlParameter paramsSearch_TrainerName = new SqlParameter("@sSearch_TrainerName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TrainerName).Value = sSearch_TrainerName;
                SqlParameter paramssSearch_Type = new SqlParameter("@sSearch_Type", SqlDbType.VarChar);
                cmd.Parameters.Add(paramssSearch_Type).Value = sSearch_Type;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Get Document Wise On going Training Records List
        public DataTable GetDocWiseOnGoingTrainingRecordsDb(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0,
            string sSearch, int DocumentID,
                string sSearch_Document,
                string sSearch_VersionNumber,
                string sSearch_TraineeEmpCode,
                string sSearch_TraineeName,
                string sSearch_TrainingStatus,
                string sSearch_TrainerName,
                string sSearch_Type
            )
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetDocumentOnGoingTrainingDetails]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramempID = new SqlParameter("@DocumentID", SqlDbType.Int);
                cmd.Parameters.Add(paramempID).Value = DocumentID;

                SqlParameter paramsSearch_Document = new SqlParameter("@sSearch_Document", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Document).Value = sSearch_Document;
                SqlParameter paramsSearch_VersionNumber = new SqlParameter("@sSearch_VersionNumber", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_VersionNumber).Value = sSearch_VersionNumber;
                SqlParameter paramsSearch_TraineeEmpCode = new SqlParameter("@sSearch_TraineeEmpCode", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TraineeEmpCode).Value = sSearch_TraineeEmpCode;
                SqlParameter paramsSearch_TraineeName = new SqlParameter("@sSearch_TraineeName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TraineeName).Value = sSearch_TraineeName;
                SqlParameter paramsSearch_TrainingStatus = new SqlParameter("@sSearch_TrainingStatus", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TrainingStatus).Value = sSearch_TrainingStatus;
                SqlParameter paramsSearch_TrainerName = new SqlParameter("@sSearch_TrainerName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TrainerName).Value = sSearch_TrainerName;
                SqlParameter paramssSearch_Type = new SqlParameter("@sSearch_Type", SqlDbType.VarChar);
                cmd.Parameters.Add(paramssSearch_Type).Value = sSearch_Type;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PostTQ_ToFAQsDb(DataTable dtTqIDs, int TrainerEmpID, out int resultStatus)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_PostTQ_ToFAQ]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmTrainerEmpID = new SqlParameter("@TrainerEmpID", SqlDbType.Int);
                SqlParameter spmResult = cmd.Parameters.Add("@ExcecutionStatus", SqlDbType.Int);

                cmd.Parameters.Add(spmTrainerEmpID).Value = TrainerEmpID;
                cmd.Parameters.AddWithValue("@TQ_IDs", dtTqIDs);
                spmResult.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                resultStatus = Convert.ToInt32(cmd.Parameters["@ExcecutionStatus"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        //insert jr training file into DB
        public void InsertJrTrainingFileIntoDB(int jrid, DataTable TrainingFiles)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_InsertJRTrainingFile]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@jrTrainingFile", TrainingFiles);
                SqlParameter spmjrid = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(spmjrid).Value = jrid;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        //Get PrintLog Training File List
        public DataTable GetPrintLogTrainingFileDB(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int JR_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetTrainingFileLog]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDept_ID = new SqlParameter("@JR_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramDept_ID).Value = JR_ID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetATC_MRTList(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int ATC_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_GetATC_MRT_List]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramATC_ID = new SqlParameter("@ATC_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramATC_ID).Value = ATC_ID;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                // totalRecordCount = Convert.ToInt32(cmd.Parameters["@TotalRecordCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        #region TMS Modification by 27 Nov 2019
        public int CloseTheTargetTraining(int TTS_ID, int EmpID, string Comments)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_CloseTargetTraining]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPTTS_ID).Value = TTS_ID;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPComments = new SqlParameter("@Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(SPComments).Value = Comments;
                SqlParameter spResultStatus = new SqlParameter("@ResultStatus", SqlDbType.Int);
                cmd.Parameters.Add(spResultStatus).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                return Convert.ToInt32(cmd.Parameters["@ResultStatus"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }



        public DataTable CancelledTraineesList(int TTS_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_ToGetCancelledTraineesList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramTTS_ID).Value = TTS_ID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        //public DataTable GetInActiveTraineesStatus(DataTable dtViewStateTrainees)
        //{
        //    SqlCommand cmd = new SqlCommand("[TMS].[AizantIT_SP_TraineeEmployeeStatus]", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    SqlParameter paramTrainees = new SqlParameter("@TraineeIDs", SqlDbType.Structured);
        //    cmd.Parameters.Add(paramTrainees).Value = dtViewStateTrainees;
        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataTable dt = new DataTable();
        //    da.Fill(dt);
        //    return dt;
        //}

        //public int GetDocumentRecommendedCountDb(int traineeID)
        //{
        //    try
        //    {
        //        SqlCommand cmd = new SqlCommand("TMS.AizantIT_SP_GetDocumentRecommendationCount", con);
        //        con.Open();
        //        cmd.CommandType = CommandType.StoredProcedure;

        //        SqlParameter SPtraineeID = new SqlParameter("@traineeID", SqlDbType.Int);
        //        cmd.Parameters.Add(SPtraineeID).Value = traineeID;

        //        int Count = (Int32)cmd.ExecuteScalar();

        //        return Count;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}
    }
}

