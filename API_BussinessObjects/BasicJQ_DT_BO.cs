﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace API_BussinessObjects
{
    public class BasicJQ_DT_BO//:IDisposable
    {
        public int DisplayLength { get; set; }
        public int DisplayStart { get; set; }
        public int SortCol { get; set; }
        public string SortDir { get; set; }
        public string Search { get; set; }
        //private int _DisplayLength;
        //public int DisplayLength {
        //    get { return _DisplayLength; }
        //    set { _DisplayLength = int.Parse(System.Web.HttpContext.Current.Request.Params["iDisplayLength"]); }
        //}
        //private int _DisplayStart;
        //public int DisplayStart
        //{
        //    get { return _DisplayStart; }
        //    set { _DisplayStart = int.Parse(System.Web.HttpContext.Current.Request.Params["iDisplayStart"]); }
        //}
        //private int _SortCol;
        //public int SortCol
        //{
        //    get { return _SortCol; }
        //    set { _SortCol = int.Parse(System.Web.HttpContext.Current.Request.Params["iSortCol_0"]); }
        //}
        //private string _SortDir;
        //public string SortDir
        //{
        //    get { return _SortDir; }
        //    set { _SortDir = System.Web.HttpContext.Current.Request.Params["sSortDir_0"].ToString(); }
        //}
        //private string _Search;
        //public string Search {
        //    get { return _Search; }
        //    set { _Search = System.Web.HttpContext.Current.Request.Params["sSearch"].ToString(); }
        //}
        public int TotalRecordCount { get; set; }
        //private int _TotalRecordCount;
        //public int TotalRecordCount
        //{
        //    get { return _TotalRecordCount; }
        //    set { _TotalRecordCount = 0; }
        //}
        public BasicJQ_DT_BO()
        {
            DisplayLength = int.Parse(System.Web.HttpContext.Current.Request.Params["iDisplayLength"]);
            DisplayStart = int.Parse(System.Web.HttpContext.Current.Request.Params["iDisplayStart"]);
            SortCol = int.Parse(System.Web.HttpContext.Current.Request.Params["iSortCol_0"]);
            SortDir = System.Web.HttpContext.Current.Request.Params["sSortDir_0"].ToString();
            Search = System.Web.HttpContext.Current.Request.Params["sSearch"].ToString();
        }
        //public static BasicJQ_DT_BO ClassObjBasicJQ_DT_BO;
        //public static BasicJQ_DT_BO GetBasicJQ_DT_BOInstance()
        //{
        //    ClassObjBasicJQ_DT_BO = null;
        //    ClassObjBasicJQ_DT_BO = new BasicJQ_DT_BO();
        //    return ClassObjBasicJQ_DT_BO;
        //}

        //For IDisposable Implementation
        //private bool disposed = false;
        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (!disposed)
        //    {
        //        if (disposing)
        //        {
        //            DisplayLength = 0;
        //            DisplayStart = 0;
        //            SortCol = 0;
        //            SortDir = "asc";
        //            Search = "";
        //        }
        //        disposed = true;
        //    }
        //}
        //~BasicJQ_DT_BO()
        //{
        //    Dispose(false);
        //}

    }
}
