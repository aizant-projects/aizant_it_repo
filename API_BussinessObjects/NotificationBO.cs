﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BussinessObjects
{
   public class NotificationBO
    {

        public int RowNumber { get; set; }
        public int NotificationID { get; set; }
        public string Description { get; set; }
        public string GeneratedDateStr { get; set; }
        public string ModuleName { get; set; }
        public string NotificationLink { get; set; }
        public string ReferenceLink { get; set; }
        public string NotifyStatus { get; set; }

        public NotificationBO(int _RowNumber, int _NotificationID, string _Description, 
            string _GeneratedDateStr, string _Module,string _NotificationLink,string _ReferenceLink,string _NotifyStatus)
        {
            RowNumber = _RowNumber;
            NotificationID = _NotificationID;
            Description = _Description;
            GeneratedDateStr = _GeneratedDateStr;
            ModuleName = _Module;
            NotificationLink = _NotificationLink;
            ReferenceLink = _ReferenceLink;
            NotifyStatus = _NotifyStatus;
        }

        public NotificationBO(int _RowNumber, int _NotificationID,string _Description,
           string _NotifyStatus)
        {
            RowNumber = _RowNumber;
            NotificationID = _NotificationID;
            Description = _Description;
            NotifyStatus = _NotifyStatus;
        }
        public NotificationBO()
        {

        }
    }

}
