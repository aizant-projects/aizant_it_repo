﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BussinessObjects
{
    public class LandingPageBO
    {
        //Html Elements
        public bool ShowUMSModule { get; set; }
        public bool ShowDMSModule { get; set; }
        public bool ShowTMSModule { get; set; }
        public bool ShowVMSModule { get; set; }
        public bool ShowQMSModule { get; set; }
        public bool ShowPMSModule { get; set; }
        public bool ShowRMSModule { get; set; }
        public bool[] ShowModules { get; set; }

        //Navigation URLs
        public string UMSNavigateUrl { get; set; }
        public string DMSNavigateUrl { get; set; }
        public string TMSNavigateUrl { get; set; }
        public string VMSNavigateUrl { get; set; }
        public string QMSNavigateUrl { get; set; }
        public string PMSNavigateUrl { get; set; }
        public string RMSNavigateUrl { get; set; }
        public string[] NavigateUrls { get; set; }
        public string[] ModuleImages { get; set; }
        public string[] ModuleNames { get; set; }

        public int ModuleCount { get; set; }

        public string ExpireMessage { get; set; }
    }
}
