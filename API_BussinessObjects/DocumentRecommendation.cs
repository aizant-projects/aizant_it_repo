﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BussinessObjects
{
   public class DocumentRecommendation
    {
        public int DocVersionID { get; set; }
        public string DocumentName { get; set; }
        public int DocRecommendationCount { get; set; }
        public int RowNumber { get; set; }
        public DocumentRecommendation(int _DocVersionID, string _DocumentName, int _DocRecommendationCount, int _RowNumber)
        {
            DocVersionID = _DocVersionID;
            DocumentName = _DocumentName;
            DocRecommendationCount = _DocRecommendationCount;
            RowNumber = _RowNumber;
        }
        public DocumentRecommendation(string _DocumentName, int _RowNumber)
        {
            DocumentName = _DocumentName;
            RowNumber = _RowNumber;
        }
    }
}
