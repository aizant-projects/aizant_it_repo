﻿using Aizant_API_Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMS_BusinessObjects.jQueryParamsObject;

namespace API_TMS_BusinessLayer1
{
    public class API_TMS_BAL
    {
        public List<ScheduledJRandTT_PendingListObjects> GetScheduledJRandTT_PendingListObjects(ScheduledJRandTT_PendingListObjects objTMS_DT_BO)
        {
            try
            {
                List<ScheduledJRandTT_PendingListObjects> objJRandTT_Pendinglist = new List<ScheduledJRandTT_PendingListObjects>();
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    dynamic objJRandTT_PendingListQuery = dbAizantIT_QMS_Audit.AizantIT_SP_ScheduledJRAndTT_PendingList(objTMS_DT_BO.DisplayLength,
                   objTMS_DT_BO.DisplayStart,
                   objTMS_DT_BO.SortCol,
                   objTMS_DT_BO.SortDir,
                   objTMS_DT_BO.Search != null ? objTMS_DT_BO.Search.Trim() : "",
                   objTMS_DT_BO.SearchTrainee != null ? objTMS_DT_BO.SearchTrainee.Trim() : "",
                   objTMS_DT_BO.SearchDocument != null ? objTMS_DT_BO.SearchDocument.Trim() : "",
                   objTMS_DT_BO.SearchDocVersion != null ? objTMS_DT_BO.SearchDocVersion.Trim() : "",
                   objTMS_DT_BO.SearchDepartment != null ? objTMS_DT_BO.SearchDepartment.Trim() : "",
                   objTMS_DT_BO.SarchTypeOfTraining != null ? objTMS_DT_BO.SarchTypeOfTraining.Trim() : "",
                   objTMS_DT_BO.TraineeID);
                    foreach (var item in objJRandTT_PendingListQuery)
                    {
                        if (objTMS_DT_BO.TotalRecordCount == 0)
                            objTMS_DT_BO.TotalRecordCount = (int)item.TotalCount;
                        objJRandTT_Pendinglist.Add(new ScheduledJRandTT_PendingListObjects(
                           item.Trainee, item.DocName, item.DocVersion, item.DocDepartment, item.TrainingType));
                    }
                }
                return objJRandTT_Pendinglist;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<ScheduledJRandTargetTraineePendingList> GetScheduledJRandTT_TraineePendingListObjects(ScheduledJRandTargetTraineePendingList objTMS_DT_BO)
        {
            try
            {
                List<ScheduledJRandTargetTraineePendingList> objJRandTT_TraineePendinglist = new List<ScheduledJRandTargetTraineePendingList>();
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    dynamic objJRandTT_TraineePending = dbAizantIT_QMS_Audit.AizantIT_SP_JRAndTT_TraineePendingList(objTMS_DT_BO.DisplayLength,
                   objTMS_DT_BO.DisplayStart,
                   objTMS_DT_BO.SortCol,
                   objTMS_DT_BO.SortDir,
                   objTMS_DT_BO.Search != null ? objTMS_DT_BO.Search.Trim() : "",
                   objTMS_DT_BO.HeadEmpID);
                    foreach (var item in objJRandTT_TraineePending)
                    {
                        if (objTMS_DT_BO.TotalRecordCount == 0)
                            objTMS_DT_BO.TotalRecordCount = (int)item.TotalCount;
                        objJRandTT_TraineePendinglist.Add(new ScheduledJRandTargetTraineePendingList(item.TraineeID,
                           item.Trainee, item.Department, item.TrainingPendingCount));
                    }
                }
                return objJRandTT_TraineePendinglist;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
