﻿namespace Aizant_Enums
{
    public static class AizantEnums
    {
        public enum Modules
        {
            AdminSetup = 1,
            DMS=2,
            TMS=3,
            QMS=4,
            Product=5,
            VMS=6,
            RA=7,
            VONTR=8
        }
        public enum AdminSetup_UserRole
        {
            UserAdmin_UMS = 1,
            GlobalAdmin_UMS = 50,
            ProductAdmin=14
        }
        public enum DMS_UserRole
        {
            Admin_DMS = 2,
            Approver_DMS = 9,
            Reviewer_DMS = 10,
            Author_DMS = 11,
            Initiator_DMS = 12,
            Authorizer_DMS = 13,
            Reader_DMS = 23,
            DocController_DMS = 30,
            PrintReviewer_DMS = 32
        }

        public enum TMS_UserRole
        {
            HOD_TMS = 3,
            Admin_TMS = 4,
            QA_TMS = 5,
            Trainee_TMS = 6,
            Trainer_TMS = 7,
            Dept_Head=59
        }

        public enum RA_UserRole
        {
            RA_Admin = 39,
            RA_Lead=40,
            RA_Approver=41,
            RA_HOD=42
        }
        public enum Product_Admin
        {
            Product_Admin=14
        }
        public enum RA_DossierHistoryStatus
        {
            Product_Created=1,
            Submitted_for_Approval=2,
            Modified_Product=3,
            Reverted_Product=4,
            Review_Accepted_Date_Submitted=5,
            Goal_Date_Submitted=6,
            Product_Approved_by_RA_Approver=7,
            Dossier_Completed =8,
            Re_Assigned_Author=9,
            Re_Assigned_Approver=10
        }

        public enum RA_DeficiencyHistoryStatus
        {
            Deficiency_Created=1,
            Submitted_for_Approval=2,
            Deficiency_Modified=3,
            Deficiency_Reverted = 4,
            Deficiency_Approved =5,
            Deficiency_Completed=6,
            Re_Assigned_Approver=7,
            Re_Assigned_Author = 8
        }

        public enum RA_ObservationHistoryStatus
        {
           ObservationInitiated=1,
           ObservationAccepted=2,
           ObservationCompletionSubmitted=3,
           Re_Assigned_ResponsiblePerson=4,
           Observation_Created =5,
           Observation_Modified=6
        }
        public enum VMS_UserRole
        {
            Admin = 8,
            HR = 17,
            HelpDesk = 18,
            MaterialStore = 19,
            MaterialManager = 20,
            MaterialUser = 21,
            Security = 22,
            BusinessDevelopment = 28
        }
        public enum QMS_NoteToFileListTypes
        {
            CreatedList = 1,
            InitiatorRevertedList,
            HOD_ReviewList,
            HOD_RevertedList,
            QaReviewList,
            QaRevertedList,
            HeadQaReviewList,
            MainList,
            PednigCharttList,
            RejectedChartList,
            ApproveChartList,
            NTFManage
        }

        public enum QMS_IncidentListTypes
        {
            CreatedList = 1,
            InitiatorRevertedList,
            HOD_ReviewList,
            HOD_RevertedList,
            QaReviewList,
            QaRevertedList,
            HeadQaReviewList,
            InvestigatorReviewList,
            InvestigationReviewByHodList,
            HodActionPlanList,//10
            InvestigatorRevertedList,//11
            MainList,//12
            PendingList,//13
            RejectedList,//14
            ApprovedList,//15
            VerifiedList,//16
            ReportIncidentList,//17
            ClosureList,//18
            OverDueList,//19
            IncidentManage//20
        }
        public enum QMS_CCNListTypes
        {
            CCNCreatedList = 1,//CTransL 1
            Initiator_RevertedList,//RL 2
            CCNHodReview,//HR 3
            QA_AssessmentList,//QA 4
            HODImpactpoints,//HA 5
            HOD_RevertedImpactpoints,//HARvrtd 6
            HOD_ImpactPointsMultiNotifn,//HA_MultiNotifn 7
            HOD_RevertImpactpointMultiRevertNotifn,//HA_MultiRevertNotifn 8
            QA_RevertedList,//QART 9
            HeadQA_Approval,//HQA 10
            CCN_QAClose,//QAClose 11
            QA_Verification,//QAVerify 12
            CCN_Reports,//Reports 13
            CCN_MainList,//CCNMainList 14
            C_Pending,//15
            C_Rejected,//16
            C_Approved,//17
            C_Verify,//18
            C_Closer,//19
            C_DueDate,//20
                      // HODRT,//21
            filterInitiator,//21
            CCN_Manage//22
        }

        public enum QMS_ErrataListTypes
        {
            E_InitiatorList = 1,
            E_HODReviewList,
            E_InitiatorRevertedList,
            E_QAReviewList,
            E_HodRevertedList,
            E_HQAReviewList,
            E_QAverification,
            E_MainList,
            E_PendingList,
            E_ApprovedList,
            E_RejectedList,
            E_Verify,
            E_Manage,//13 For Admin Manage Roles
            E_Complete=15
        }
        public enum QMS_Roles
        {
            Initiator = 24,
            HOD = 25,
            QA = 26,
            HeadQA = 27,
            User = 29,
            AdminRole = 31,
            Investigator = 33
        }
        public enum Product_Roles
        {
            Product_Admin = 14,
            Product_Approver = 15,
            Product_User = 16,
        }

        public enum QMS_Audit_Roles
        {
            AuditViewer = 34,
            AuditHOD = 35,
            AuditQA = 36,
            AuditHeadQA = 37,
            AuditAdmin = 38
        }

        public enum TmsBaseType
        {
            JR_Training = 1,
            Target_Training
        }

        public enum JR_Status
        {
            JR_Assigned_to_Trainee = 1,
            JR_Accepted_By_Trainee,
            JR_Delined_By_QA,
            JR_Approved_By_QA,
            Training_Schedule_Assigned_to_Trainee,
            Training_Schedule_Declined_By_QA,
            Training_Schedule_Approved_By_QA,
            HOD_Evaluation,
            QA_Evaluation,
            Training_Completed
        }

        public enum Questionnaire_Status
        {
            Questionnaire_Created = 1,
            Questionnaire_Reverted,
            Questionnaire_Approved,
            Questionnaire_InActive
        }
        public enum Incident_Status
        {
            Initiated_by_Initiator = 1,
            Modified_by_Initiator = 2,
            Approved_by_HOD = 3,
            Reverted_by_HOD = 4,
            Rejected_by_HOD = 5,
            Approved_by_QA = 6,
            Reverted_to_HOD_by_QA = 7,
            Rejected_by_QA = 8,
            Completed_Invest_by_Investigator = 9,
            Updated_Invest_by_Investigator = 10,
            Incd_with_Invest_Reverted_to_Investigator_by_HOD = 11,
            HOD_Approved_and_added_ActionPlan_Training = 12,
            Incd_with_Invest_Approved_by_QA = 13,
            Incd_with_Invest_Revert_to_Invest_by_QA = 14,
            Incd_with_Invest_Modified_ActionPlan_by_HOD = 15,
            Incd_with_Invest_Approved_by_HeadQA = 16,
            Incd_Modified_by_HOD = 17,
            Incd_with_Invest_Rejected_by_HeadQA = 18,
            Incd_with_Invest_Reverted_to_Invest_by_HeadQA = 19,
            Incd_with_Invest_Reverted_to_HOD_by_HeadQA = 20,
            At_ActionPlan = 21,
            HOD_Deleted_ActionPlan = 22,
            Dept_HOD_Reviewed_Incd_with_Investigation = 23,
            Incd_Reverted_to_Initiator_by_QA = 24,
            Incd_with_Invest_Revert_to_HOD_by_QA = 25,
            Rejected_by_QA_with_Invest = 26,
            Verified_by_QA = 27
        }
    }
}
