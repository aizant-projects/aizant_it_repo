﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace AizantIT_PharmaApp_WebAPI.Common
{
    public class WebAPI_Authentication: AuthorizationFilterAttribute
    {
        /// <summary>
        /// read requested header and validated
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            try
            {
                // var ActiveUserSession = FetchFromHeader(actionContext);

                //if (identity != null)
                //{
                //    var securityService = actionContext.ControllerContext.Configuration.DependencyResolver.GetService(typeof(ILoginService)) as ILoginService;
                //    if (securityService.TokenAuthentication(identity))
                //    {
                //        CurrentThread.SetPrincipal(new GenericPrincipal(new GenericIdentity(identity), null), null, null);
                //    }
                //    else
                //    {
                //        actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                //        return;
                //    }
                //}
                //else
                //{
                //    actionContext.Response = new HttpResponseMessage(HttpStatusCode.BadRequest);
                //    return;
                //}
                if (!IsValidUserRequested(actionContext))
                {
                    actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);//returns 401 status code (UnAuthorised).
                    return;
                }
                base.OnAuthorization(actionContext);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// retrive header detail from the request 
        /// </summary>
        /// <param name="actionContext"></param>
        /// <returns></returns>
        private string FetchFromHeader(HttpActionContext actionContext)
        {
            string requestToken = null;

            //var authRequest1 = actionContext.Request.Headers.Authorization.Parameter;
            //if (authRequest != null && !string.IsNullOrEmpty(authRequest.Scheme) && authRequest.Scheme == "Basic")
            //    requestToken = authRequest.Parameter;

            var ActiveUserSession = actionContext.Request.Headers.GetValues("ActiveUserSession").First();
            var RequestedEmpID = actionContext.Request.Headers.GetValues("RequestedEmpID").First();

            return requestToken;
        }
        private bool IsValidUserRequested(HttpActionContext actionContext)
        {
            try
            {
                if (actionContext.Request.Headers.Contains("ActiveUserSession") && actionContext.Request.Headers.Contains("RequestedEmpID"))
                {
                    var ActiveUserSession = actionContext.Request.Headers.GetValues("ActiveUserSession").First();
                    var RequestedEmpID = actionContext.Request.Headers.GetValues("RequestedEmpID").First();
                    if (ActiveUserSession == ActiveUserSession && RequestedEmpID == RequestedEmpID)//Check it from DB
                        return true;
                    else
                        return false;
                }
                else
                    return false;
               
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}