﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace AizantIT_PharmaApp_WebAPI.Common.CustomFilters
{
    public class CustWebApiAuthorization: AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            try
            {
                if (!IsValidUserRequested(actionContext))
                {
                    actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);//returns 401 status code (UnAuthorised).
                    return;
                }
                base.OnAuthorization(actionContext);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool IsValidUserRequested(HttpActionContext actionContext)
        {
            try
            {
                if (actionContext.Request.Headers.Contains("ActiveUserSession") && actionContext.Request.Headers.Contains("RequestedEmpID"))
                    return new API_BusinessLayer.API_BAL().IsUserAuthenticated(
                        actionContext.Request.Headers.GetValues("ActiveUserSession").First(),
                        actionContext.Request.Headers.GetValues("RequestedEmpID").First());//Check it from DB
                
                else
                    return false;

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}