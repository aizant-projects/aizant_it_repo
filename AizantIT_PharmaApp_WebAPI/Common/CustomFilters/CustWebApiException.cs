﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace AizantIT_PharmaApp_WebAPI.Common.CustomFilters
{
    public class CustWebApiException: ExceptionFilterAttribute, IExceptionFilter
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            //base.OnException(actionExecutedContext);    
            string exceptionMessage = string.Empty;
            if (actionExecutedContext.Exception.InnerException == null)
            {
                exceptionMessage = actionExecutedContext.Exception.Message;
            }
            else
            {
                exceptionMessage = actionExecutedContext.Exception.InnerException.Message;
            }
            //We can log this exception message to the file or database.  
            //var response = newHttpResponseMessage(HttpStatusCode.InternalServerError);
            //{
            //    Content = newStringContent(“An unhandled exception was thrown by service.”),  
            //        ReasonPhrase = "Internal Server Error.Please Contact your Administrator.";
            //};
            //actionExecutedContext.Response = response;
            //int LineNo = (new StackTrace(actionExecutedContext.Exception, true).GetFrame((new StackTrace(actionExecutedContext.Exception, true)).FrameCount - 1).GetFileLineNumber());
            exceptionMessage = exceptionMessage.Replace("\n", " ").Replace("\r", " ");
            var response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new StringContent(exceptionMessage),  
                    ReasonPhrase = "Error while executing Controller <b>"+ actionExecutedContext.ActionContext.ControllerContext.ControllerDescriptor.ControllerName 
                    + "</b> & action <b>" + actionExecutedContext.ActionContext.ActionDescriptor.ActionName + "</b> is : " + exceptionMessage
            };
            actionExecutedContext.Response = response;
            //actionExecutedContext.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }
    }
}