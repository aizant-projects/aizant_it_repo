﻿using Aizant_API_Entities;
using QMS_BO.QMS_Dashboard_BO;
using QMS_BusinessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;

namespace AizantIT_PharmaApp_WebAPI.Controllers.QMS
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DashboardController : ApiController
    {
        private AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
        //public IHttpActionResult GetDashboardCards(int EmpID)
        //{
        //    QMS_BAL Qms_bal = new QMS_BAL();

        //    DataSet ds = new DataSet();
        //    ds = Qms_bal.BalDashboard(Convert.ToInt32(EmpID));
        //    DataSet dsCCN = new DataSet();
        //    dsCCN = Qms_bal.BalGetdashboardCCN(Convert.ToInt32(EmpID));
        //    return Ok(dsCCN);
        //}
        Dashboard_QMS ObjQE = new Dashboard_QMS();
        [HttpPost]
        public IHttpActionResult ManagementChart(Models.ManagementChartBO obj)
        {

            DataTable dtDeptlst = new DataTable();
            dtDeptlst.Columns.Add("DeptID");
            DataRow dr;
            foreach (var item in obj.DeptID)
            {
                dr = dtDeptlst.NewRow();
                dr["DeptID"] = item;
                dtDeptlst.Rows.Add(dr);

            }

            //int x = 0;
            //if (Array.Exists(obj.QualityEventType, element => element == 1) && Array.Exists(obj.QualityEventType, element => element != 2))          //&& element != 2 && element != 3 && element != 4))
            //{
            //    x = 1;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element != 1 && element == 2 && element != 3 && element != 4))
            //{
            //    x = 2;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element != 1 && element != 2 && element == 3 && element != 4))
            //{
            //    x = 3;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element != 1 && element != 2 && element != 3 && element == 4))
            //{
            //    x = 4;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element == 1 && element == 2 && element != 3 && element != 4))
            //{
            //    x = 12;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element == 1 && element != 2 && element == 3 && element != 4))
            //{
            //    x = 13;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element == 1 && element != 2 && element != 3 && element == 4))
            //{
            //    x = 14;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element != 1 && element == 2 && element == 3 && element != 4))
            //{
            //    x = 23;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element != 1 && element == 2 && element != 3 && element == 4))
            //{
            //    x = 24;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element == 1 && element != 2 && element == 3 && element == 4))
            //{
            //    x = 34;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element == 1 && element == 2 && element == 3 && element != 4))
            //{
            //    x = 123;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element == 1 && element == 2 && element != 3 && element == 4))
            //{
            //    x = 124;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element == 1 && element != 2 && element == 3 && element == 4))
            //{
            //    x = 134;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element != 1 && element == 2 && element == 3 && element == 4))
            //{
            //    x = 234;
            //}
            //else if (Array.Exists(obj.QualityEventType, element => element == 1 && element == 2 && element == 3 && element == 4))
            //{
            //    x = 1234;
            //}
            //else
            //{
            //    x = 0;
            //}


            DataTable dtQElst = new DataTable();
            dtQElst.Columns.Add("QualityEventType");
            DataRow dre;
            foreach (var item in obj.QualityEventType)
            {
                dre = dtQElst.NewRow();

                dre["QualityEventType"] = item;
                dtQElst.Rows.Add(dre);
            }

            DataSet ds = new DataSet();

            Dashboard_QMS ObjQE = new Dashboard_QMS();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<ManagementChart> ManagementCharts = new List<ManagementChart>();

            //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["NwindDbContext"].ToString());
            //con.Open();
            //SqlCommand cmd = new SqlCommand("[QMS].[DASHBOARD_SP_GetEventsCount]", con);
            ////mention that we are working with sp
            //cmd.CommandType = CommandType.StoredProcedure;

            ////pass the parameter
            //SqlDataAdapter dr = new SqlDataAdapter(cmd);
            //dr.Fill(ds);
            ds = Qms_bal.GetManagementChartDataBAL(dtDeptlst, dtQElst, obj.FromDate, obj.ToDate);

            if (ds.Tables.Count > 0)
            {
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    ManagementCharts.Add(new ManagementChart(item["DepartmentName"].ToString(),
                        Convert.ToInt32(item["Errata_count_Deptwise"]),
                        Convert.ToInt32(item["NTF_count_Deptwise"]),
                        Convert.ToInt32(item["CCN_count_Deptwise"]),
                        Convert.ToInt32(item["Incident_count_Deptwise"])));
                }
            }


            ManagementChartList mcl = new ManagementChartList();
            mcl.Mchart_bind = ManagementCharts;
            return Json(mcl);
        }

        public IHttpActionResult GetdepartmentList()
        {
            DataSet dept = new DataSet();
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            List<GetDeptFromEventTable> dept_list = new List<GetDeptFromEventTable>();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[DASHBOARD_SP_GetDepartmentFromEventTable]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            //pass the parameter
            SqlDataAdapter dr = new SqlDataAdapter(cmd);
            dr.Fill(dept);

            if (dept.Tables.Count > 0)
            {
                foreach (DataRow item in dept.Tables[0].Rows)
                {
                    dept_list.Add(new GetDeptFromEventTable(Convert.ToInt32(item["DeptID"]),
                        item["DepartmentName"].ToString())
                       );
                }
            }
            GetDeptFromEventTableList depFeve = new GetDeptFromEventTableList();
            depFeve.DeptFevent_list = dept_list;
            return Json(depFeve);
        }
        public IHttpActionResult GetQualityEventList()
        {
            AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
            var qualityEventList = (from t1 in dbEF.AizantIT_QualityEvent
                                    where ((t1.QualityEvent_TypeID == 1) || (t1.QualityEvent_TypeID == 2) ||
                                    (t1.QualityEvent_TypeID == 3) || (t1.QualityEvent_TypeID == 4)
                                    )
                                    select new { t1.QualityEvent_TypeID, t1.QualityEvent_TypeName }).ToList();
            return Json(qualityEventList);
        }

        [HttpGet]
        public IHttpActionResult BindIncidentCategory()
        {
            var incCat = (from t1 in dbEF.AizantIT_TypeOfIncident
                          select new
                          {
                              t1.TypeofCategoryID,
                              t1.TypeofIncident
                          }).ToList();
            return Json(new { data = incCat });
        }

        public IHttpActionResult StatusChartForQualityEvents(Models.StatusChartBO objstatus)
        {
            DataTable dtDeptlst = new DataTable();
            dtDeptlst.Columns.Add("DeptID");
            DataRow dr;
            foreach (var item in objstatus.DeptID)
            {
                dr = dtDeptlst.NewRow();

                dr["DeptID"] = item;
                dtDeptlst.Rows.Add(dr);
            }
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            DataSet ds = new DataSet();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<GetQualityEventStatusCount> StatusCount = new List<GetQualityEventStatusCount>();
            ds = Qms_bal.GetStatusChartForQualityEventsBAL(dtDeptlst, objstatus.QualityEventType, objstatus.CCNClassification, objstatus.CCNCategory,
                objstatus.IncidentClassification, objstatus.IncidentCategory, objstatus.FromDate, objstatus.ToDate);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dtr in ds.Tables[0].Rows)
                {
                    StatusCount.Add(new GetQualityEventStatusCount(dtr["EventName"].ToString(), Convert.ToInt32(dtr["TOTRecords"])));

                }
            }
            GetQualityEventStatusCountList GqCL = new GetQualityEventStatusCountList();
            GqCL.QeStatus_List = StatusCount;
            return Json(GqCL);
        }

        [HttpPost]
        public IHttpActionResult OverdueChart(Models.Overdue objdue)
        {
            DataTable dtDeptlst = new DataTable();
            dtDeptlst.Columns.Add("DeptID");
            DataRow dr;
            foreach (var item in objdue.DeptID)
            {
                dr = dtDeptlst.NewRow();

                dr["DeptID"] = item;
                dtDeptlst.Rows.Add(dr);
            }
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            DataSet ds = new DataSet();
            QMS_BAL Qms_bal = new QMS_BAL();

            List<GetOverdueCount> OverdueCount = new List<GetOverdueCount>();
            ds = Qms_bal.GetOverdueChartBAL(dtDeptlst, objdue.QualityEventType, objdue.CCNClassification, objdue.CCNCategory,
                objdue.IncidentClassification, objdue.IncidentCategory, objdue.FromDate, objdue.ToDate);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dtr in ds.Tables[0].Rows)
                {
                    OverdueCount.Add(new GetOverdueCount(dtr["EventName"].ToString(), Convert.ToInt32(dtr["TOTRecords"])));

                }
            }
            GetOverdueCountList GOL = new GetOverdueCountList();
            GOL.Overdue_List = OverdueCount;
            return Json(GOL);


        }
        
        [HttpPost]
        public IHttpActionResult GetPerformanceChartCount(PerformanceChartCountBO pobj)
        {
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            DataSet ds = new DataSet();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<GetPerformanceChartCount> PerformanceCount = new List<GetPerformanceChartCount>();
            ds = Qms_bal.GetPerformanceChartCountBAL(pobj);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dpr in ds.Tables[0].Rows)
                {
                    PerformanceCount.Add(new GetPerformanceChartCount(dpr["EventMonth"].ToString(),
                        Convert.ToInt32(dpr["TOTRecords"]),
                        Convert.ToDouble(dpr["DepartmentalAvg"]),
                        Convert.ToDouble(dpr["OrganizationalAvg"])));
                }
            }

            GetPerformanceChartCountList GPL = new GetPerformanceChartCountList();
            GPL.Performance_List = PerformanceCount;
            return Json(GPL);
        }

        [HttpPost]
        public IHttpActionResult GetPerformanceChartTAT(PerformanceChartCountBO ptobj)
        {
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            DataSet ds = new DataSet();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<GetPerformanceChartCount> PerformanceCount = new List<GetPerformanceChartCount>();
            ds = Qms_bal.GetPerformanceChartTATBAL(ptobj);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dpr in ds.Tables[0].Rows)
                {
                    PerformanceCount.Add(new GetPerformanceChartCount(dpr["EventMonth"].ToString(),
                        Convert.ToInt32(dpr["AvgTAT_Days"]),
                        Convert.ToDouble(dpr["DepartmentalAvg"]),
                        Convert.ToDouble(dpr["OrganizationalAvg"])));
                }
            }
            GetPerformanceChartCountList GPL = new GetPerformanceChartCountList();
            GPL.Performance_List = PerformanceCount;
            return Json(GPL);
        }

        [HttpPost]
        public IHttpActionResult GetCCNList(CCNList lobj)
        {
            DataTable dtDeptlst = new DataTable();
            dtDeptlst.Columns.Add("DeptID");
            DataRow dr;
            foreach (var item in lobj.DeptID)
            {
                dr = dtDeptlst.NewRow();

                dr["DeptID"] = item;
                dtDeptlst.Rows.Add(dr);
            }
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            DataSet ds = new DataSet();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<GetCCNListBO> CCNListforChart = new List<GetCCNListBO>();
            ds = Qms_bal.GetCCNListBAL(dtDeptlst, lobj.showtype, lobj.CCNClassification, lobj.CCNCategory, lobj.FromDate
                , lobj.ToDate);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dpr in ds.Tables[0].Rows)
                {
                    CCNListforChart.Add(new GetCCNListBO(dpr["CCN_No"].ToString(),
                        dpr["DepartmentName"].ToString(),
                        dpr["CreatedBy"].ToString(),
                        dpr["CreatedDate"].ToString(),
                        dpr["DueDate"].ToString(), dpr["Change Classification"].ToString(),
                        dpr["StatusName"].ToString(),
                        Convert.ToInt32(dpr["NoOfDueDays"]),
                        dpr["AssignTo"].ToString())
                       );
                }
            }
            GetCCNListBOList CCNL = new GetCCNListBOList();
            CCNL.CCN_List = CCNListforChart;
            return Json(CCNL);
        }
        [HttpPost]
        public IHttpActionResult GetIncidentList(IncidentList ilobj)
        {
            DataTable dtDeptlst = new DataTable();
            dtDeptlst.Columns.Add("DeptID");
            DataRow dr;
            foreach (var item in ilobj.DeptID)
            {
                dr = dtDeptlst.NewRow();
                dr["DeptID"] = item;
                dtDeptlst.Rows.Add(dr);
            }
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            DataSet ds = new DataSet();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<GetIncidentListBO> IncidentListforChart = new List<GetIncidentListBO>();
            ds = Qms_bal.GetIncidentListBAL(dtDeptlst, ilobj.showtype, ilobj.IncidentClassification, ilobj.IncidentCategory, ilobj.FromDate
                , ilobj.ToDate);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dpr in ds.Tables[0].Rows)
                {
                    IncidentListforChart.Add(new GetIncidentListBO(dpr["IncidentNumber"].ToString(),
                        dpr["Department"].ToString(),
                        dpr["Type of Incident"].ToString(),
                        dpr["Category"].ToString(),
                        dpr["CreatedBy"].ToString(),
                        dpr["DateofReport"].ToString(),
                        dpr["DateofOccurrence"].ToString(),
                        dpr["DueDate"].ToString(),
                        Convert.ToInt32(dpr["NoofDueDays"]),
                        dpr["Status"].ToString(),
                        dpr["AssignTo"].ToString()
                       ));
                }
            }
            GetIncidentListBOList INL = new GetIncidentListBOList();
            INL.Incident_List = IncidentListforChart;
            return Json(INL);
        }
        [HttpPost]
        public IHttpActionResult GetErrataList(ErrataList eobj)
        {
            DataTable dtDeptlst = new DataTable();
            dtDeptlst.Columns.Add("DeptID");
            DataRow dr;
            foreach (var item in eobj.DeptID)
            {
                dr = dtDeptlst.NewRow();
                dr["DeptID"] = item;
                dtDeptlst.Rows.Add(dr);
            }
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            DataSet ds = new DataSet();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<GetErrataListBO> ErrataListforChart = new List<GetErrataListBO>();
            ds = Qms_bal.GetErrataListBAL(dtDeptlst, eobj.showtype, eobj.FromDate
                , eobj.ToDate);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dpr in ds.Tables[0].Rows)
                {
                    ErrataListforChart.Add(new GetErrataListBO(dpr["ErrataNumber"].ToString(),
                        dpr["Department"].ToString(),
                        dpr["Reference Document Number"].ToString(),
                        dpr["Document Name"].ToString(),
                        dpr["CreatedBy"].ToString(),
                        dpr["CreatedDate"].ToString(),

                        dpr["Status"].ToString(),
                        dpr["AssignTo"].ToString()
                       ));
                }
            }
            GetErrataListBOList ERL = new GetErrataListBOList();
            ERL.Errata_List = ErrataListforChart;
            return Json(ERL);
        }
        [HttpPost]
        public IHttpActionResult GetNTFList(NTFList nobj)
        {
            DataTable dtDeptlst = new DataTable();
            dtDeptlst.Columns.Add("DeptID");
            DataRow dr;
            foreach (var item in nobj.DeptID)
            {
                dr = dtDeptlst.NewRow();
                dr["DeptID"] = item;
                dtDeptlst.Rows.Add(dr);
            }
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            DataSet ds = new DataSet();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<GetNTFListBO> NTFListforChart = new List<GetNTFListBO>();
            ds = Qms_bal.GetNTFListBAL(dtDeptlst, nobj.showtype, nobj.FromDate
                , nobj.ToDate);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dpr in ds.Tables[0].Rows)
                {
                    NTFListforChart.Add(new GetNTFListBO(dpr["NoteToFileNumber"].ToString(),
                        dpr["Department"].ToString(),
                        dpr["Reference Document Number"].ToString(),
                        dpr["Document Name"].ToString(),
                        dpr["CreatedBy"].ToString(),
                        dpr["CreatedDate"].ToString(),
                        dpr["Status"].ToString(),
                        dpr["AssignTo"].ToString()
                       ));
                }
            }
            GetNTFListBOList NTFL = new GetNTFListBOList();
            NTFL.NTF_List = NTFListforChart;
            return Json(NTFL);
        }
        [HttpPost]
        public IHttpActionResult GetIncidentListViewForPerformanceChart(PerformanceChartCountBO PLobj)
        {
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            DataSet ds = new DataSet();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<GetPerformanceChartListIncident> PchartList = new List<GetPerformanceChartListIncident>();
            ds = Qms_bal.GetIncidentListViewForPerformanceChartBAL(PLobj);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dpr in ds.Tables[0].Rows)
                {
                    PchartList.Add(new GetPerformanceChartListIncident(
                        dpr["Month"].ToString(),
                        dpr["IncidentNumber"].ToString(),
                        dpr["Department"].ToString(),
                        dpr["Type of Incident"].ToString(),
                        dpr["Category"].ToString(),
                        dpr["CreatedBy"].ToString(),
                        dpr["DateofOccurrence"].ToString(),
                        dpr["DateofReport"].ToString(),
                        dpr["CreatedDate"].ToString(),
                        dpr["DueDate"].ToString(),
                        dpr["CompletedDate"].ToString(),
                        dpr["TAT"].ToString()));

                }
            }
            GetPerformanceChartListIncidentList GPL = new GetPerformanceChartListIncidentList();
            GPL.Pchart_List = PchartList;
            return Json(GPL);
        }
        [HttpPost]
        public IHttpActionResult GetCCNListViewForPerformanceChart(PerformanceChartCountBO pobj)
        {
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            DataSet ds = new DataSet();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<GetPerformanceChartListCCN> PchartList = new List<GetPerformanceChartListCCN>();
            ds = Qms_bal.GetCCNListViewForPerformanceChartBAL(pobj);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dpr in ds.Tables[0].Rows)
                {
                    PchartList.Add(new GetPerformanceChartListCCN(dpr["Month"].ToString(),
                        dpr["CCN_No"].ToString(),
                        dpr["Department"].ToString(),
                        dpr["CreatedBy"].ToString(),
                        dpr["CreatedDate"].ToString(),
                        dpr["DueDate"].ToString(),
                        dpr["CompletedDate"].ToString(),
                        dpr["ChangeClassification"].ToString(),
                        dpr["TAT"].ToString()
                        ));
                }
            }
            GetPerformanceChartListCCNList GPL = new GetPerformanceChartListCCNList();
            GPL.Cchart_List = PchartList;
            return Json(GPL);
        }
        [HttpPost]
        public IHttpActionResult GetErrataListViewForPerformanceChart(PerformanceChartCountBO pobj)
        {
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            DataSet ds = new DataSet();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<GetPerformanceChartListErrata> PchartList = new List<GetPerformanceChartListErrata>();
            ds = Qms_bal.GetErrataListViewForPerformanceChartBAL(pobj);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dpr in ds.Tables[0].Rows)
                {
                    PchartList.Add(new GetPerformanceChartListErrata(dpr["Month"].ToString(),
                        dpr["ErrataNumber"].ToString(),
                        dpr["Department"].ToString(),
                        dpr["CreatedBy"].ToString(),
                        dpr["CreatedDate"].ToString(),
                        dpr["CompletedDate"].ToString(),
                        dpr["ReferenceDocumentNumber"].ToString(),
                        dpr["DocumentName"].ToString(),
                        dpr["TAT"].ToString()
                        ));
                }
            }
            GetPerformanceChartListErrataList GPL = new GetPerformanceChartListErrataList();
            GPL.Echart_List = PchartList;
            return Json(GPL);
        }
        [HttpPost]
        public IHttpActionResult GetNTFListViewForPerformanceChart(PerformanceChartCountBO pobj)
        {
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            DataSet ds = new DataSet();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<GetPerformanceChartListNTF> PchartList = new List<GetPerformanceChartListNTF>();
            ds = Qms_bal.GetNTFListViewForPerformanceChartBAL(pobj);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dpr in ds.Tables[0].Rows)
                {
                    PchartList.Add(new GetPerformanceChartListNTF(dpr["Month"].ToString(),
                        dpr["NoteToFileNumber"].ToString(),
                        dpr["Department"].ToString(),
                        dpr["CreatedBy"].ToString(),
                        dpr["Reference Doc Number"].ToString(),
                        dpr["Document Name"].ToString(),
                        dpr["CreatedDate"].ToString(),
                        dpr["CompletedDate"].ToString(),
                        dpr["TAT"].ToString()
                        ));
                }
            }
            GetPerformanceChartListNTFList GPL = new GetPerformanceChartListNTFList();
            GPL.Nchart_List = PchartList;
            return Json(GPL);
        }

        public IHttpActionResult GetQualityEventListforCAPAChart()
        {
            AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
            var qualityEventList = (from t1 in dbEF.AizantIT_QualityEvent
                                    where ((t1.QualityEvent_TypeID == 1) || (t1.QualityEvent_TypeID == 2) ||
                                    (t1.QualityEvent_TypeID == 4) || (t1.QualityEvent_TypeID == 6) || (t1.QualityEvent_TypeID == 7)
                                    || (t1.QualityEvent_TypeID == 8)
                                    )
                                    select new { t1.QualityEvent_TypeID, t1.QualityEvent_TypeName }).ToList();
            return Json(qualityEventList);
        }
        [HttpPost]
        public IHttpActionResult CapaBarChart(CapaBarChart clobj)
        {
            DataTable dtDeptlst = new DataTable();
            dtDeptlst.Columns.Add("DeptID");
            DataRow dr;
            foreach (var item in clobj.DeptID)
            {
                dr = dtDeptlst.NewRow();
                dr["DeptID"] = item;
                dtDeptlst.Rows.Add(dr);
            }
            DataTable dtQElst = new DataTable();
            dtQElst.Columns.Add("QualityEventType");
            DataRow dre;
            foreach (var item in clobj.QualityEventType)
            {
                dre = dtQElst.NewRow();

                dre["QualityEventType"] = item;
                dtQElst.Rows.Add(dre);
            }
            DataSet ds = new DataSet();
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<CapaBarChartBO> ManagementCharts = new List<CapaBarChartBO>();
            ds = Qms_bal.GetCAPAChartDataBAL(dtDeptlst, dtQElst, clobj.FromDate, clobj.ToDate);

            if (ds.Tables.Count > 0)
            {
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    ManagementCharts.Add(new CapaBarChartBO(item["DepartmentName"].ToString(),
                        Convert.ToInt32(item["NTF_count_Deptwise"]),
                        Convert.ToInt32(item["CCN_count_Deptwise"]),
                        Convert.ToInt32(item["Incident_count_Deptwise"]),
                        Convert.ToInt32(item["OOS"]),
                        Convert.ToInt32(item["OOT"]),
                        Convert.ToInt32(item["Market_Complaint"])));
                }
            }
            CapaBarChartBOList cpcl = new CapaBarChartBOList();
            cpcl.CapaCount = ManagementCharts;
            return Json(cpcl);

        }

        [HttpPost]
        public IHttpActionResult StatusChartForCAPA(CapaStatusParam objstatus)
        {
            DataTable dtDeptlst = new DataTable();
            dtDeptlst.Columns.Add("DeptID");
            DataRow dr;
            foreach (var item in objstatus.DeptID)
            {
                dr = dtDeptlst.NewRow();
                dr["DeptID"] = item;
                dtDeptlst.Rows.Add(dr);
            }
            Dashboard_QMS ObjQE = new Dashboard_QMS();
            DataSet ds = new DataSet();
            QMS_BAL Qms_bal = new QMS_BAL();
            List<GetQualityEventStatusCount> StatusCount = new List<GetQualityEventStatusCount>();
            ds = Qms_bal.GetCAPAStatusChartBAL(dtDeptlst, objstatus.QualityEventType,
                objstatus.TypeofAction, objstatus.FromDate, objstatus.ToDate);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dtr in ds.Tables[0].Rows)
                {
                    StatusCount.Add(new GetQualityEventStatusCount(dtr["EventName"].ToString(), Convert.ToInt32(dtr["TOTRecords"])));
                }
            }
            GetQualityEventStatusCountList GqCL = new GetQualityEventStatusCountList();
            GqCL.QeStatus_List = StatusCount;
            return Json(GqCL);
        }
    }
        

}
