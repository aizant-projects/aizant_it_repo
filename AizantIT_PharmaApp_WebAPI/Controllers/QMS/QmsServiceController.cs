﻿using Aizant_API_Entities;
using QMS_BO.QMS_CAPA_BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using TMS_BusinessLayer;

namespace AizantIT_PharmaApp_WebAPI.Controllers.QMS
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class QmsServiceController : ApiController
    {
        // GET: api/QmsService
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/QmsService/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/QmsService
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/QmsService/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/QmsService/5
        public void Delete(int id)
        {
        }
        //[HttpGet]
        //public IHttpActionResult GetNotefileListData()
        //{
        //       AizantIT_DevEntities db = new AizantIT_DevEntities();
        //    HistoryBo objBo = new HistoryBo();
        //    List<HistoryBo> historylst = new List<HistoryBo>();
        //    if (objBo.IncidentStatusTTSID == 16)//TTS History HQA Approved 
        //    {
        //        TMS_BAL objTMS_Bal = new TMS_BAL();
        //        DataTable dt = objTMS_Bal.GetTTS_ActionHistory(objBo.iDisplayLength, objBo.iDisplayStart, objBo.iSortCol_0, objBo.sSortDir_0, "", Convert.ToString(objBo.EventID));
        //        DataView dv = dt.DefaultView;
        //        dv.Sort = "ActionDate desc";
        //        DataTable sortedDT = dv.ToTable();
        //        if (sortedDT.Rows.Count > 0)
        //        {
        //            objBo.totalRecordsCount = Convert.ToInt32(sortedDT.Rows[0]["TotalCount"]);
        //            for (int i = 0; i < sortedDT.Rows.Count; i++)
        //            {
        //                historylst.Add(
        //            new HistoryBo(Convert.ToInt32(sortedDT.Rows[i]["RowNumber"]), 0, sortedDT.Rows[i]["ActionRole"].ToString(), sortedDT.Rows[i]["ActionStatus"].ToString(), sortedDT.Rows[i]["ActionBy"].ToString(), sortedDT.Rows[i]["ActionDate"].ToString(),
        //            sortedDT.Rows[i]["Remarks"].ToString(), "", 0, "", "", ""));
        //            }
        //        }
        //    }
        //    else
        //    {
        //        var GetHistoryDetails = db.AizantIT_SP_GetHistoryTransactions(
        //            objBo.iDisplayLength,
        //            objBo.iDisplayStart,
        //            objBo.iSortCol_0,
        //            objBo.sSortDir_0,
        //            objBo.sSearch,
        //            objBo.EventID,
        //            objBo.EventTypeID
        //            );
        //        foreach (var item in GetHistoryDetails)
        //        {
        //            if (objBo.totalRecordsCount == 0)
        //            {
        //                objBo. totalRecordsCount = Convert.ToInt32(item.TotalCount);
        //            }
        //            historylst.Add(new HistoryBo(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.ActionHistoryID), item.ActionRole, item.ActionStatus
        //                , item.ActionBy, item.ActionDate, item.Comments == "" ? "--NA--" : item.Comments, Convert.ToDateTime(item.TargetDate).ToString("dd MMM yyyy") == "01 Jan 1900" ? "-NA-" : Convert.ToDateTime(item.TargetDate).ToString("dd MMM yyyy"),
        //                Convert.ToInt32(item.CommLength), item.offComments, item.AssigneTo == "NA" ? "-NA-" : item.AssigneTo + " (" + item.AssignedToRoleID + ")", item.AssignedToRoleID));
        //        }
        //    }
        //   return  Json(new
        //    {
        //        hasError = false,
        //        iTotalRecords = objBo.totalRecordsCount,
        //        iTotalDisplayRecords = objBo.totalRecordsCount,
        //        aaData = historylst
        //    });
            
        //}
    }
}
