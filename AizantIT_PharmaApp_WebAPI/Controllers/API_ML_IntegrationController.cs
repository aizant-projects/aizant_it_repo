﻿using API_BusinessLayer;
using API_BussinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using TMS_BusinessLayer;

namespace AizantIT_PharmaApp_WebAPI.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class API_ML_IntegrationController : ApiController
    {
        #region ML Feature Enable
        public IHttpActionResult GetMLFeatureChecking(int ModuleID)
        {
            API_BAL objAPI_BAL = new API_BAL();
            var _ML_FeatureIDs = objAPI_BAL.GetMLFeatureChecking(ModuleID);
            return Json(_ML_FeatureIDs);
        }
        #endregion
        #region DocumentRecomended
        //public IHttpActionResult GetDocumentRecomendedList()
        //{
        //    BasicJQ_DT_BO objTMS_ML_DT_BO = new BasicJQ_DT_BO();
        //    var TrainingPerformanceListObj = new API_BAL().GetDocumentRecommendationList(objTMS_ML_DT_BO);
        //    return Json(new
        //    {
        //        iTotalRecords = objTMS_ML_DT_BO.TotalRecordCount,
        //        iTotalDisplayRecords = objTMS_ML_DT_BO.TotalRecordCount,
        //        aaData = TrainingPerformanceListObj
        //    });
        //}
        //Training Document Recommended list
        public IHttpActionResult GetTrainingDocumentRecommendedList(int TraineeID)
        {
            BasicJQ_DT_BO objTMS_ML_DT_BO = new BasicJQ_DT_BO();
            var TrainingPerformanceListObj = new API_BAL().GetTrainingDocumentRecommendedList(objTMS_ML_DT_BO, TraineeID);
            return Json(new
            {
                iTotalRecords = objTMS_ML_DT_BO.TotalRecordCount,
                iTotalDisplayRecords = objTMS_ML_DT_BO.TotalRecordCount,
                aaData = TrainingPerformanceListObj
            });
        }
        #endregion
        public IHttpActionResult GetDocumentRecommendedCount(int TraineeID)
        {
            API_BAL objAPI_BAL = new API_BAL();
            var DocRecommendedCount = objAPI_BAL.GetDocumentRecommendedCount(TraineeID);
            return Json(DocRecommendedCount);
        }

    }
}