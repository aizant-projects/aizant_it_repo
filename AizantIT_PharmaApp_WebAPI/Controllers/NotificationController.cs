﻿using API_BusinessLayer;
using API_BussinessObjects;
using System.Web.Http;
using System.Web.Http.Cors;


namespace AizantIT_PharmaApp_WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NotificationController : ApiController
    {
        public int GetNotificationCount(int EmpID=0)
        {
            API_BAL objAPI_BAL = new API_BAL();
            return objAPI_BAL.GetNotificationCount(EmpID);
        }
        public IHttpActionResult GetNotificationData(int EmpID)
        {
            API_BAL objAPI_BAL = new API_BAL();
            string top5UserNotifications = objAPI_BAL.GetTop5UserNotifications(EmpID, out int TotalCount);
            return Json(new { UserTop5Notifications = top5UserNotifications, UserTotalNotifications = TotalCount });
        }
        [HttpPost]
        public void SubmitNotifications(int Notification_ID, int Emp_ID)
        {
            API_BAL objAPI_BAL = new API_BAL();
            objAPI_BAL.SubmitNotifications(Notification_ID,Emp_ID);
        }
        public IHttpActionResult GetUserNotificationsList(int EmpID)
        {
           int DisplayLength = int.Parse(System.Web.HttpContext.Current.Request.Params["iDisplayLength"]);
           int DisplayStart = int.Parse(System.Web.HttpContext.Current.Request.Params["iDisplayStart"]);
            int TotalRecordCount = 0;
            API_BAL objAPI_BAL = new API_BAL();
            var objUserNotificationsList = objAPI_BAL.GetNotificationsToUserList(DisplayStart, DisplayLength,EmpID, out TotalRecordCount );
            return Json(new
            {
                iTotalRecords = TotalRecordCount,
                iTotalDisplayRecords = TotalRecordCount,
                aaData = objUserNotificationsList
            });
        }
    }
}