﻿using AizantIT_PharmaApp_WebAPI.Common.CustomFilters;
using API_BussinessObjects;
using API_TMS_BusinessLayer1;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
//using System.Web.Http.Controllers;
using System.Web.Http.Cors;
using TMS_BusinessLayer;
using TMS_BusinessObjects;
using TMS_BusinessObjects.jQueryParamsObject;
using TMS_ML_BusinessLayer;

namespace AizantIT_PharmaApp_WebAPI.Controllers.TMS
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [CustWebApiAuthorization] 
    //[Route("api/TMS/{controller}/{action}/{id}")]
    public class TmsServiceController : ApiController
    {
        // GET: api/TmsService
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/TmsService/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/TmsService
        public void Post([FromBody]string value)
        {

        }

        // PUT: api/TmsService/5
        public void Put(int id, [FromBody]string value)
        {

        }

        // DELETE: api/TmsService/5
        public void Delete(int id)
        {

        }
        [HttpGet]
        //Called From ScheduledJRAndTT_PendingList.aspx
        public IHttpActionResult ScheduledJRAndTT_PendingList()
        {
            ScheduledJRandTT_PendingListObjects objTMS_DT_BO = new ScheduledJRandTT_PendingListObjects();
            var TrainingSessionEvaluations = new API_TMS_BAL().GetScheduledJRandTT_PendingListObjects(objTMS_DT_BO);
            var result = new
            {
                iTotalRecords = objTMS_DT_BO.TotalRecordCount,
                iTotalDisplayRecords = objTMS_DT_BO.TotalRecordCount,
                aaData = TrainingSessionEvaluations
            };
            return Ok(result);
        }
        [HttpGet]
        //Called From ScheduledJRAndTT_PendingList.aspx
        public IHttpActionResult ScheduledJRAndTT_TraineePendingList()
        {
            ScheduledJRandTargetTraineePendingList objTMS_DT_BO = new ScheduledJRandTargetTraineePendingList();
            var TraineePendingRecords = new API_TMS_BAL().GetScheduledJRandTT_TraineePendingListObjects(objTMS_DT_BO);
            var result = new
            {
                iTotalRecords = objTMS_DT_BO.TotalRecordCount,
                iTotalDisplayRecords = objTMS_DT_BO.TotalRecordCount,
                aaData = TraineePendingRecords
            };
            return Ok(result);
        }
        [HttpGet] 
        //[Route("api/TMS/TmsService/TrainingSessionTrainees")]
        public IHttpActionResult TargetTrainerEvaluationList()
        {
            TrainerEvaluationListObject objTMS_DT_BO = new TrainerEvaluationListObject();
                var TrainingSessionEvaluations = new TMS_BAL().GetTargetTrainerEvaluationList(objTMS_DT_BO);
                var result = new
                {
                    iTotalRecords = objTMS_DT_BO.TotalRecordCount,
                    iTotalDisplayRecords = objTMS_DT_BO.TotalRecordCount,
                    aaData = TrainingSessionEvaluations
                };
                return Ok(result);
        }
        [HttpGet]
        public HttpResponseMessage CloseTheTargetTraining(int TTS_ID,string Comments)
        {
            return Request.CreateResponse(HttpStatusCode.OK, new TMS_BAL().CloseTheTargetTraining(TTS_ID, Convert.ToInt32(new UMS_BusinessLayer.UMS_BAL().Decrypt(Request.Headers.GetValues("RequestedEmpID").First())),Comments));
        }

        [HttpGet]
        public HttpResponseMessage GetCancelledTraineesList(int TTS_ID)
        {
            return Request.CreateResponse(HttpStatusCode.OK, new TMS_BAL().CancelledTraineesList(TTS_ID));
        }

        [HttpGet]
        public IHttpActionResult JRTemplateList()
        {
            BasicJQ_DT_BO objBasicJQ_DT_BO = new BasicJQ_DT_BO();
            var JRTemplateList = new TMS_BAL().Get_JRTemplateList(objBasicJQ_DT_BO);
            var result = new
            {
                iTotalRecords = objBasicJQ_DT_BO.TotalRecordCount,
                iTotalDisplayRecords = objBasicJQ_DT_BO.TotalRecordCount,
                aaData = JRTemplateList
            };
            return Ok(result);
        }

        //JR PendingList
        [HttpGet]
        public IHttpActionResult GetJR_PendingList()
        {
            BasicJQ_DT_BO objBasicJQ_DT_BO = new BasicJQ_DT_BO();
            var JRPendingList = new TMS_BAL().Get_JRPendingList(objBasicJQ_DT_BO);
            var result = new
            {
                iTotalRecords = objBasicJQ_DT_BO.TotalRecordCount,
                iTotalDisplayRecords = objBasicJQ_DT_BO.TotalRecordCount,
                aaData = JRPendingList
            };
            return Ok(result);
        }

    }
}
