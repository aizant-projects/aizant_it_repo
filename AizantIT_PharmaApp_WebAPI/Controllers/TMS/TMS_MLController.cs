﻿//using Aizant_API_Entities;
using AizantIT_PharmaApp_WebAPI.Common.CustomFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using TMS_BusinessLayer;
using TMS_BusinessObjects.jQueryParamsObject;
using TMS_ML_BusinessLayer;

namespace AizantIT_PharmaApp_WebAPI.Controllers
{
    //[CustWebApiException] //No need to write here if it is registered in WebAPiConfig.cs
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    //[CustWebApiAuthorization]
    public class TMS_MLController : ApiController
    {
        // GET: api/TMS_ML
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}
        //// GET: api/TMS_ML/5
        //public string Get(int id)
        //{
        //    return "value";
        //}
        // POST: api/TMS_ML
        public void Post([FromBody]string value)
        {
        }
        // PUT: api/TMS_ML/5
        public void Put(int id, [FromBody]string value)
        {
        }
        // DELETE: api/TMS_ML/5
        public void Delete(int id)
        {

        }
        //Called from TMS_ML_RelatedObservationsonDoc.js
        [HttpGet]
        public HttpResponseMessage GetObservationsListonDocID(int DocID)
        {
            TMS_ML_BAL objTMS_ML_BAL = new TMS_ML_BAL();
            return Request.CreateResponse(HttpStatusCode.OK, objTMS_ML_BAL.GetDocumentRelatedObservations(DocID));
        }

        #region Training Performance
        [HttpGet]
        //[Route("api/TMS_ML/GetTrainingPerformance")]
        public IHttpActionResult GetTrainingPerformance()
        {
            TrainingPerformanceListObjects objTMS_ML_DT_BO = new TrainingPerformanceListObjects();
            var TrainingPerformanceListObj = new TMS_BAL().GetTrainingPerformanceBal(objTMS_ML_DT_BO);
            //var result = ;
            return Json(new
            {
                iTotalRecords = objTMS_ML_DT_BO.TotalRecordCount,
                iTotalDisplayRecords = objTMS_ML_DT_BO.TotalRecordCount,
                aaData = TrainingPerformanceListObj
            });
        }
        [HttpGet]
        //[Route("api/TMS_ML/GetTraineePerformance")]
        public IHttpActionResult GetTraineePerformance(int EmpID)
        {
            var TraineePerformanceListObj = new TMS_BAL().GetTraineePerformanceBal(EmpID);
            return Json(TraineePerformanceListObj);
        }
        #endregion
    }
}
