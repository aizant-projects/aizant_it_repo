﻿using AizantIT_PharmaApp_WebAPI.Common.CustomFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace AizantIT_PharmaApp_WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            //Enabling the Cross Origin Accessibility.
            config.EnableCors();

            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                 name: "DefaultApi2",
                 routeTemplate: "api/{controller}/{action}/{id}",
                 defaults: new { id = RouteParameter.Optional }
             );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            

            //For Exception Handling
            config.Filters.Add(new CustWebApiException());
            // config.Services.Replace(typeof(IExceptionHandler), new CustGlobalExceptionHandler());

        }
    }
}
