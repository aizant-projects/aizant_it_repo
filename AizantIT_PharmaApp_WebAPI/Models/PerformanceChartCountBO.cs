﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp_WebAPI.Models
{
    public class PerformanceChartCountBO
    {
        public int DeptID { get; set; }
        public int QualityEventType { get; set; }
        public int CCNClassification { get; set; }
        public int CCNCategory { get; set; }
        public int IncidentClassification { get; set; }
        public int IncidentCategory { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}