﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp_WebAPI.Models
{
    public class ManagementChartBO
    {

        public int[] DeptID { get; set; }
        public int[] QualityEventType { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

    }
}