﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_BO
{
    public class CompanyBO
    {
        private int _Mode;
        public int Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }

        private int _CompanyID;
        public int CompanyID
        {
            get { return _CompanyID; }
            set { _CompanyID = value; }
        }

        private string _CompanyCode;
        public string CompanyCode
        {
            get { return _CompanyCode; }
            set { _CompanyCode = value; }
        }

        private string _CompanyName;
        public string CompanyName
        {
            get { return _CompanyName; }
            set { _CompanyName = value; }
        }

        private string _CompanyDescription;
        public string CompanyDescription
        {
            get { return _CompanyDescription; }
            set { _CompanyDescription = value; }
        }

        private string _CompanyPhoneNo1;
        public string CompanyPhoneNo1
        {
            get { return _CompanyPhoneNo1; }
            set { _CompanyPhoneNo1 = value; }
        }

        private string _CompanyPhoneNo2;
        public string CompanyPhoneNo2
        {
            get { return _CompanyPhoneNo2; }
            set { _CompanyPhoneNo2 = value; }
        }

        private string _CompanyFaxNo1;
        public string CompanyFaxNo1
        {
            get { return _CompanyFaxNo1; }
            set { _CompanyFaxNo1 = value; }
        }


        private string _CompanyFaxNo2;
        public string CompanyFaxNo2
        {
            get { return _CompanyFaxNo2; }
            set { _CompanyFaxNo2 = value; }
        }

        private string _CompanyEmailID;
        public string CompanyEmailID
        {
            get { return _CompanyEmailID; }
            set { _CompanyEmailID = value; }
        }

        private string _CompanyWebUrl;
        public string CompanyWebUrl
        {
            get { return _CompanyWebUrl; }
            set { _CompanyWebUrl = value; }
        }

        private string _CompanyAddress;
        public string CompanyAddress
        {
            get { return _CompanyAddress; }
            set { _CompanyAddress = value; }
        }


        private string _CompanyLocation;
        public string CompanyLocation
        {
            get { return _CompanyLocation; }
            set { _CompanyLocation = value; }
        }

        private string _CompanyCity;
        public string CompanyCity
        {
            get { return _CompanyCity; }
            set { _CompanyCity = value; }
        }

        private string _CompanyState;
        public string CompanyState
        {
            get { return _CompanyState; }
            set { _CompanyState = value; }
        }

        private string _CompanyCountry;
        public string CompanyCountry
        {
            get { return _CompanyCountry; }
            set { _CompanyCountry = value; }
        }


        private string _CompanyPinCode;
        public string CompanyPinCode
        {
            get { return _CompanyPinCode; }
            set { _CompanyPinCode = value; }
        }

        private byte[] _CompanyLogo;
        public byte[] CompanyLogo
        {
            get { return _CompanyLogo; }
            set { _CompanyLogo = value; }
        }

        private string _CompanyStartDate;
        public string CompanyStartDate
        {
            get { return _CompanyStartDate; }
            set { _CompanyStartDate = value; }
        }
        private int _createdBY;
        public int createdBY
        {
            get { return _createdBY; }
            set { _createdBY = value; }
        }

    }

}
