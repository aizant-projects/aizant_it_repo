﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_BO
{
    public class EmployeeLists
    {
        public int EmpID { get; set; }
        public int RowNumber { get; set; }
        public string EmpCode { get; set; }
        public string Name { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public string OfficialEmailID { get; set; }
        public string MobileNo { get; set; }
        public string RoleCount { get; set; }
        public int DeptID { get; set; }
        public string StartDate { get; set; }
        public string Status { get; set; }
        public string LoginID { get; set; }
        public string Locked { get; set; }
        public string Photo { get; set; }
    }
}
