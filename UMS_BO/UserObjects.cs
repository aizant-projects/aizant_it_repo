﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_BO
{
    public class UserObjects
    {
        //For User Creation
        public int EmpID { get; set; }
        public string EmpCode { get; set; }
        public string EmpFirstName { get; set; }
        public string EmpLastName { get; set; }
        public DateTime EmpDOB { get; set; }
        public int EmpDepartment { get; set; }
        public int EmpDesignation { get; set; }
        public string EmpLoginID { get; set; }
        public int EmpCurrentLocation { get; set; }
        public string EmpEmailID { get; set; }
        public string EmpMobileNo { get; set; }
        public string EmpEmgNo { get; set; }
        public string EmpTelNo { get; set; }
        public int EmpCountry { get; set; }
        public int EmpState { get; set; }
        public int EmpCity { get; set; }
        public string EmpAddress { get; set; }
        public byte[] EmpPhoto { get; set; }
        public char EmpGender { get; set; }
        public char EmpActiveStatus { get; set; }
        public char EmpLockStatus { get; set; }
        public string EmpStartDate { get; set; }
        public string EmpEndDate { get; set; }
        public string EmpBloodGroup { get; set; }
        public string EmpCharacter { get; set; }
        public int LockedStatus { get; set; }
        //For Department Creation

        //For Designation Creation
        //public int DesignationDept { get; set; }
        //public string DesignationName { get; set; }
        //public int AllDesignations { get; set; }
        //public int SelectedDesigDepartment { get; set; }
        //public string TypeofInsert { get; set; }

        //For UserLogin


        //For DropDownList
        public int SelectedCountryID { get; set; }
        public int SelectedDepartmentID { get; set; }
        public int RoleID { get; set; }
    }
}
