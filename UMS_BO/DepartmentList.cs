﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_BO
{
   public class DepartmentList
    {
        public int RowNumber { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string DeptCode { get; set; }
    }
}
