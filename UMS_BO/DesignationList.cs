﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_BO
{
    public class DesignationList
    {
        public int RowNumber { get; set; }
        public int DesignationID { get; set; }
        public string DesignationName { get; set; }
    }
}
