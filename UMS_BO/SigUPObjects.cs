﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_BO
{
    public class SigUPObjects
    {
        public string UserLoginID { get; set; }
        public string ConfirmPassword { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityAnswer { get; set; }

        //public SigUPObjects(string UserLoginID, string SecurityQuestion, string ConfirmPassword, string SecurityAnswer)
        //{
        //    this.UserLoginID = UserLoginID;
        //    this.SecurityQuestion = SecurityQuestion;
        //    this.ConfirmPassword = ConfirmPassword;
        //    this.SecurityAnswer = SecurityAnswer;

        //}
    }
    //public class ListItems
    //{
    //    public List<RolesObjects> Role { get; set; }
    //}
    public class RolesObjects
    {
        public string ModuleID { get; set; }
        public string ModuleName { get; set; }

        public string RoleID { get; set; }
        public string RoleName { get; set; }

        public string DeptID { get; set; }
        public string DepartmentName { get; set; }

        //public RolesObjects(string ModuleID, string ModuleName)
        //{
        //    this.ModuleID = ModuleID;
        //    this.ModuleName = ModuleName;
        //}
    }


}
