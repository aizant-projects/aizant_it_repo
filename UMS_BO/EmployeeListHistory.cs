﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_BO
{
   public class EmployeeListHistory
    {
        public string EmpCode { set; get; }
        public string Employee { set; get; }
        public string LoginID { set; get; }
        public string Login_Time { set; get; }
        public string Logout_Time { set; get; }
        public string UserHostAddress { set; get; }
        public string UserClientAddress { set; get; }


        public string sSearch_EmpCode { set; get; }
        public string sSearch_Employee { set; get; }
        public string sSearch_LoginID { set; get; }
        public string sSearch_Login_Time { set; get; }
        public string sSearch_Logout_Time { set; get; }
        public string sSearch_UserHostAddress { set; get; }
        public string sSearch_UserClientAddress { set; get; }
    }
}
