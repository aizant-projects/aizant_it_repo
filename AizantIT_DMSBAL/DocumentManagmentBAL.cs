﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AizantIT_DMSDAL;
using AizantIT_DMSBO;
using System.Data.SqlClient;


namespace AizantIT_DMSBAL
{
    public class DocumentManagmentBAL
    {
        DocumentManagmentDAL objDocumentDAL = new DocumentManagmentDAL();

        #region dropdownFilling
        public DataTable DMS_GetDocumentNameList(DocObjects doc)
        {
            List<string> DMS_List = new List<string>();
            DataTable DMS_Dt = new DataTable();
            try
            {
                DMS_Dt = objDocumentDAL.DMS_GetDocumentNameList(doc);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetDepartmentList()
        {
            try
            {
                DataTable DMS_INS = objDocumentDAL.DMS_GetDepartmentList();
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetFormNumberList(DocObjects doc)
        {
            try
            {
                DataTable DMS_INS = objDocumentDAL.DMS_GetFormNumberList(doc);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetDocRevertedByList(DocObjects doc)
        {
            try
            {
                DataTable DMS_INS = objDocumentDAL.DMS_GetDocRevertedByList(doc);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion dropdownFilling

        #region jquerydatatable
        public DataTable DMS_GetReviewDocuments(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                DataTable DMS_INS = objDocumentDAL.DMS_GetReviewDocuments(_objJQDataTableBO);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion jquerydatatable

        #region getComments/time
        public DataTable DMS_GetComments(string documentID)
        {
            try
            {
                DataTable DMS_INS = objDocumentDAL.DMS_GetComments(documentID);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_SaveReviewComments(ReviewObjects reviewObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_SaveReviewComments(reviewObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_UpdateReviewedTimeToReviewer(ReviewObjects reviewObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_UpdateReviewedTimeToReviewer(reviewObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetPreviousDocumentStatus(DocObjects doc)
        {
            try
            {
                DataTable DMS_INS = objDocumentDAL.DMS_GetPreviousDocumentStatus(doc);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #region GetFormComment
        public DataTable DMS_GetLatestFormComment(string FormVersionID)
        {
            try
            {
                return objDocumentDAL.DMS_GetLatestFormComment(FormVersionID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetFormComment
        #endregion getComments/time

        #region DocumentActions
        public int DMS_UpdateReviewstaus(ReviewObjects reviewObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_UpdateReviewstaus(reviewObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_ApproveDocByReviewer(ReviewObjects reviewObjects, ref SqlTransaction _Transaction, ref SqlConnection DMS_Con)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_ApproveDocByReviewer(reviewObjects,ref _Transaction, ref  DMS_Con);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
            public int DMS_ApproveDocByReviewer(ReviewObjects reviewObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_ApproveDocByReviewer(reviewObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_UpdateCommetaToReviewer(DocUploadBO reviewObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_UpdateCommetaToReviewer(reviewObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #region DocRejectByAdmin
        public int DMS_RejectDocByAdmin(DocObjects doc)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_RejectDocByAdmin(doc);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion DocRejectByAdmin
        #endregion DocumentActions

        #region UpdateDocPrivacy
        public int DMS_UpdateDocPrivacyLevel(DocObjects doc, bool isPublic)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_UpdateDocPrivacyLevel(doc, isPublic);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion UpdateDocPrivacy

        #region EmpModuleDepartmentFilling 
        public DataTable DMS_EmpModuleDepartmentFilling(DocObjects doc)
        {
            try
            {
                DataTable DMS_INS = objDocumentDAL.DMS_EmpModuleDepartmentFilling(doc);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion EmpModuleDepartmentFilling

        #region DocRequestActions
        #region DocPrintReqApproveByReviewer
        public int DMS_DocReqApprovedByReviewer(DocPrintRequest docReqObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_DocReqApprovedByReviewer(docReqObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion DocPrintReqApproveByReviewer
        #region DocPrintReqRejectByReviewer
        public int DMS_DocReqRejectedByReviewer(DocPrintRequest docReqObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_DocReqRejectedByReviewer(docReqObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion DocPrintReqRejectByReviewer
        #region DocPrintReqApproveByController
        public int DMS_DocReqApprovedByController(DocPrintRequest docReqObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_DocReqApprovedByController(docReqObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion DocPrintReqApproveByController
        public int DMS_DocPrintReqByRequestor(DocPrintRequest docReqObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_DocPrintReqByRequestor(docReqObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_DocDownloadReqByRequestor(DocPrintRequest docReqObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_DocDownloadReqByRequestor(docReqObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion DocRequestActions

        #region FormActions
        #region FormApproveByController
        public int DMS_ApproveFormbyController(DocObjects doc)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_ApproveFormbyController(doc);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormApproveByController
        #region FormRevertByController
        public int DMS_RevertFormbyController(DocObjects doc)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_RevertFormbyController(doc);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormRevertByController
        #region FormRejectByController
        public int DMS_RejectFormbyController(DocObjects doc)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_RejectFormbyController(doc);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormRejectByController
        #region FormWithdrawByController
        public int DMS_WithdrawFormbyController(DocObjects doc)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_WithdrawFormbyController(doc);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormRejectByController
        #region ApproveFormRevisionbyController
        public int DMS_ApproveFormRevisionbyController(DocObjects doc)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_ApproveFormRevisionbyController(doc);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion ApproveFormRevisionbyController
        #endregion FormActions

        #region FormRequestActions
        #region FormPrintReqRejectByReviewer
        public int DMS_FormPrintReqReject(DocPrintRequest docReqObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_FormPrintReqReject(docReqObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormPrintReqRejectByReviewer
        #region FormPrintReqApproveByController
        public int DMS_FormReqApprovedByController(DocPrintRequest docReqObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_FormReqApprovedByController(docReqObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormPrintReqApproveByController
        #region FormPrintReqApproveByReviewer
        public int DMS_FormReqApprovedByReviewer(DocPrintRequest docReqObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_FormReqApprovedByReviewer(docReqObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormPrintReqApproveByReviewer
        public int DMS_FormPrintReqByRequestor(DocPrintRequest docReqObjects)
        {
            try
            {
                int DMS_INS = objDocumentDAL.DMS_FormPrintReqByRequestor(docReqObjects);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormRequestActions                                                      

        #region DocumentTypeDetails
        public DataTable DMS_GetIsTraining(int DocumentTypeID)
        {
            try
            {
                DataTable DMS_INS = objDocumentDAL.DMS_GetIsTraining(DocumentTypeID);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion DocumentTypeDetails
        //#region DocumentName Linq Queries
        //var innerJoinResult = from s in studentList // outer sequence
        //                      join st in standardList //inner sequence 
        //                      on s.StandardID equals st.StandardID // key selector 
        //                      select new
        //                      { // result selector 
        //                          StudentName = s.StudentName,
        //                          StandardName = st.StandardName
        //                      };
        //#endregion DocumentName Linq Queries

        //public int DMS_RejectDocByReviewer(string Comments, string RID, string DocumentID, string ReviewedBy)
        //{

        //    try
        //    {
        //        int DMS_INS = objDocumentDAL.DMS_RejectDocByReviewer(Comments, RID, DocumentID, ReviewedBy);
        //        return DMS_INS;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }

        //}
        //public DataTable DMS_GetApproverDocuments(string ApprovedBy)
        //{

        //    try
        //    {
        //        DataTable DMS_INS = objDocumentDAL.DMS_GetApproverDocuments(ApprovedBy);
        //        return DMS_INS;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}
        //public int DMS_SaveapproverComments(string Comments, string AID, string DocumentID, string ApprovedBy)
        //{

        //    try
        //    {
        //        int DMS_INS = objDocumentDAL.DMS_SaveApproverComments(Comments, AID, DocumentID, ApprovedBy);
        //        return DMS_INS;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}
        //public int DMS_UpdateCommetaToApprover(string Comments, string AID, string DocumentID, string approvedBy)
        //{

        //    try
        //    {
        //        int DMS_INS = objDocumentDAL.DMS_UpdateCommetaToApprover(Comments, AID, DocumentID, approvedBy);
        //        return DMS_INS;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }

        //}
        //public int DMS_RejectDocByApprover(string Comments, string AID, string DocumentID, string ApprovedBy)
        //{

        //    try
        //    {
        //        int DMS_INS = objDocumentDAL.DMS_RejectDocByApprover(Comments, AID, DocumentID, ApprovedBy);
        //        return DMS_INS;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }

        //}
        //public int DMS_ApproveDocByApprover(string AID, string DocumentID, string ApprovedBy)
        //{

        //    try
        //    {
        //        int DMS_INS = objDocumentDAL.DMS_ApproveDocByApprover(AID, DocumentID, ApprovedBy);
        //        return DMS_INS;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }

        //}
        //public DataTable DMS_GetDocumentContent(string DMS_SearhValue)
        //{
        //    DataTable DMS_Dt = new DataTable();
        //    try
        //    {
        //        DMS_Dt = objDocumentDAL.DMS_GetDocumentContent(DMS_SearhValue);
        //        return DMS_Dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}
        //public int DMS_DocumentContentUpdation(string DocumentID, string DocumentContent, string EditBy, string Comments)
        //{

        //    try
        //    {
        //        int DMS_INS = objDocumentDAL.DMS_DocumentContentUpdation(DocumentID, DocumentContent, EditBy, Comments);
        //        return DMS_INS;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}
        //#region MultiQA
        //public int DMS_MultiQAReviewstatus(ReviewObjects reviewObjects)
        //{

        //    try
        //    {
        //        int DMS_INS = objDocumentDAL.DMS_MultiQAReviewstatus(reviewObjects);
        //        return DMS_INS;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}
        //#endregion MultiQA        
    }
}
