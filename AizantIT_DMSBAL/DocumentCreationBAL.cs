﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AizantIT_DMSDAL;
using AizantIT_DMSBO;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace AizantIT_DMSBAL
{
    public class DocumentCreationBAL
    {
        DocumentCreationDAL objDocumentDAL = new DocumentCreationDAL();

        #region DropdownFilling        
        public DataTable DMS_GetApproverReviewerList(DocObjects doc)
        {
            try
            {
                string Val_A;
                if (doc.dept == "All")
                {
                    Val_A = "0";
                }
                else
                {
                    Val_A = doc.dept;
                }
                doc.dept = Val_A;
                return objDocumentDAL.DMS_GetApproverReviewerList(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetRequestTypeList()
        {
            try
            {
                return objDocumentDAL.DMS_GetRequestTypeList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetApproverList(DocObjects doc)
        {
            List<string> DMS_List = new List<string>();
            DataTable DMS_Dt = new DataTable();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_GetApproverList(doc);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetApproverReviewerSelectedList(string DocumentID, int DocStatus)
        {
            try
            {
                return objDocumentDAL.DMS_GetApproverReviewerSelectedList(DocumentID, DocStatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #region GetControllers
        public DataTable DMS_GetControllersList()
        {
            try
            {
                return objDocumentDAL.DMS_GetControllersList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetControllers
        #region GetPrintReviewersList
        public DataTable DMS_GetPrintReviewersList(DocObjects doc)
        {
            try
            {
                string Val_A;
                if (doc.dept == "All")
                {
                    Val_A = "0";
                }
                else
                {
                    Val_A = doc.dept;
                }
                doc.dept = Val_A;
                return objDocumentDAL.DMS_GetPrintReviewersList(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetPrintReviewersList
        public DataTable DMS_GetCreatorList(DocObjects doc)
        {
            List<string> DMS_List = new List<string>();
            DataTable DMS_Dt = new DataTable();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_GetCreatorList(doc);

                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataTable DMS_GetCopyType()
        {
            try
            {
                return objDocumentDAL.DMS_GetCopyType();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion DropdownFilling

        #region TemplateActions
        public int DMS_InsertTemplate(ReviewObjects reviewObjects)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_InsertTemplate(reviewObjects);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_DeleteTemplate(ReviewObjects reviewObjects)
        {
            try
            {
                return objDocumentDAL.DMS_DeleteTemplate(reviewObjects);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion TemplateActions

        #region ExistingDocs
        public int DMS_ExistingDocumentInsertion(DocObjects doc, bool isPublic)
        {
            try
            {
                return objDocumentDAL.DMS_ExistingDocumentInsertion(doc,isPublic);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion ExistingDocs

        #region DocumentType
        public DataTable DMS_GetProcessList()
        {
            try
            {
                return objDocumentDAL.DMS_GetProcessTypeList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetRenewYearsBal()
        {
            try
            {
                return objDocumentDAL.DMS_GetRenewYearsDal();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_OtherDocumentTypeInsertion(DocObjects doc, bool training)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_OtherDocumentTypeInsertion(doc, training);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int DMS_OtherDocumentTypeInsertionUpdate(DocObjects doc, bool training)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_OtherDocumentTypeInsertionUpdate(doc, training);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataTable DMS_GetDocumentTypeComments(string DocTypeID)
        {
            try
            {
                return objDocumentDAL.DMS_GetDocumentTypeComments(DocTypeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion DocumentType

        #region NewDocInitiation
        public int DMS_DocumentCreation(DocObjects doc, bool isPublic)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_DocumentCreation(doc, isPublic);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_DocumentEdit(DocObjects doc, bool isPublic)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_DocumentEdit(doc, isPublic);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion NewDocInitiation
        #region RevisionDocInitiation
        public int DMS_DocumentUpdation(DocObjects doc, bool isPublic)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_DocumentUpdation(doc, isPublic);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_DocumentUpdationEdit(DocObjects doc, bool isPublic)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_DocumentUpdationEdit(doc, isPublic);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion RevisionDocInitiation
        #region WithdrawDocInitiation
        public int DMS_DocumentUpdationWithdrawn(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_DocumentUpdationWithdrawn(doc);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion WithdrawDocInitiation
        #region RenewDocInitiation
        public int DMS_DocumentInitiationRenew(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_DocumentInitiationRenew(doc);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_DocumentUpdationRenew(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_DocumentUpdationRenew(doc);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion RenewDocInitiation

        #region effectivedate
        public int DMS_inserteffectivedate(DocObjects doc, ref SqlTransaction transaction, ref SqlConnection DMS_Con)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_inserteffectivedate(doc, ref transaction, ref DMS_Con);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion effectivedate
        #region renewdate
        public int DMS_insertRenewdate(DocObjects doc,ref SqlTransaction Transaction,ref SqlConnection DMS_Con)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_insertRenewdate(doc,ref Transaction,ref DMS_Con);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion renewdate

        #region Searches
        public DataTable DMS_DocumentNameSearch(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            try
            {
                DMS_Dt = objDocumentDAL.DMS_DocumentNameSearch(doc);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_DocumentNumberSearch(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            try
            {
                DMS_Dt = objDocumentDAL.DMS_DocumentNumberSearch(doc);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_TemplateNameSearch(ReviewObjects reviewObjects)
        {
            DataTable DMS_Dt = new DataTable();
            try
            {
                DMS_Dt = objDocumentDAL.DMS_TemplateNameSearch(reviewObjects);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_FormNameSearch(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            try
            {
                DMS_Dt = objDocumentDAL.DMS_FormNameSearch(doc);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_FormNumberSearch(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            try
            {
                DMS_Dt = objDocumentDAL.DMS_FormNumberSearch(doc);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_LatestActiveDoc(int VersionID)
        {
            DataTable DMS_Dt = new DataTable();
            try
            {
                DMS_Dt = objDocumentDAL.DMS_LatestActiveDoc(VersionID);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_LatestActiveForm(int FormVersionID)
        {
            DataTable DMS_Dt = new DataTable();
            try
            {
                DMS_Dt = objDocumentDAL.DMS_LatestActiveForm(FormVersionID);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion Searches

        #region jquerydatatable
        public DataTable DMS_GetDocumentTypeList(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                return objDocumentDAL.DMS_GetDocumentTypeList(_objJQDataTableBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetTemplateNameList(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                return objDocumentDAL.DMS_GetTemplateNameList(_objJQDataTableBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetNewDocQAList(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                return objDocumentDAL.DMS_GetNewDocQAList(_objJQDataTableBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetNewDocIntiatorList(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                return objDocumentDAL.DMS_GetNewDocIntiatorList(_objJQDataTableBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetmanagedocumentList(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                return objDocumentDAL.DMS_GetManageDocumentList(_objJQDataTableBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_Listofdocuments(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                return objDocumentDAL.DMS_Listofdocuments(_objJQDataTableBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_Listofeffectivedocuments(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                return objDocumentDAL.DMS_Listofeffectivedocuments(_objJQDataTableBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_ExtendDocument(JQDataTableBO _objJQDataTableBO, out int TotalRecordCount)
        {
            try
            {
                return objDocumentDAL.DMS_ExtendDocument(_objJQDataTableBO,out TotalRecordCount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetNewDocCreatorList(JQDataTableBO _objJQDataTableBO)
        {
            DataTable DMS_Dt = new DataTable();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_GetNewDocCreatorList(_objJQDataTableBO);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetPendingDocCreatorList(JQDataTableBO _objJQDataTableBO)
        {
            DataTable DMS_Dt = new DataTable();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_GetPendingDocCreatorList(_objJQDataTableBO);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #region webservice data
        public DataTable DMS_GetExistingDocuments(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                return objDocumentDAL.DMS_GetExistingDocuments(_objJQDataTableBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion webservice data
        #region AdminDocumentList
        public DataTable DMS_AdminListOFDocuments(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                return objDocumentDAL.DMS_AdminListOFDocuments(_objJQDataTableBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion AdminDocumentList
        public DataTable DMS_GetWithdrawDocList(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                return objDocumentDAL.DMS_GetWithdrawDocList(_objJQDataTableBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion jquerydatatable

        #region GetDetailsFormDB
        public DataTable DMS_GetDocByID(string DMS_DocumentID, ref SqlTransaction _transaction, ref SqlConnection DMS_Con)
        {
            DataTable DMS_Dt = new DataTable();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_GetDocByID(DMS_DocumentID, ref _transaction, ref DMS_Con);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetDocByID(string DMS_DocumentID)
        {
            DataTable DMS_Dt = new DataTable();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_GetDocByID(DMS_DocumentID);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }       
        public DataTable DMS_GetTempalteContent(string Tempalte_ID)
        {
            DataTable DMS_Dt = new DataTable();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_GetTempalteContent(Tempalte_ID);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetRejectCommentsByID(string DMS_DocumentID, string ProcessID)
        {
            DataTable DMS_Dt = new DataTable();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_GetRejectCommentsByID(DMS_DocumentID,ProcessID);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetsavedCommentsByID(ReviewObjects reviewObjects)
        {
            try
            {
                return objDocumentDAL.DMS_GetsavedCommentsByID(reviewObjects);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }        
        public DataTable DMS_GetNoOFAttempts(string DMS_DocumentID)
        {
            DataTable DMS_Dt = new DataTable();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_GetNoOFAttempts(DMS_DocumentID);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetsavedTimeByID(ReviewObjects reviewObjects)
        {
            try
            {
                return objDocumentDAL.DMS_GetsavedTimeByID(reviewObjects);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #region Version
        public DataTable DMS_GetVersionID(DocObjects doc)
        {
            try
            {
                DataTable DMS_INS = objDocumentDAL.DMS_GetVersionID(doc);
                return DMS_INS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion Version
        #region DocumentRevisionValidation
        public int DMS_DocumentTrainingCount(int ActionType, int DocumentID, int VersionID)
        {
            try
            {
                return objDocumentDAL.DMS_DocumentTrainingCount(ActionType, DocumentID, VersionID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DMS_PendingDocRequestCount(int VersionID)
        {
            try
            {
                return objDocumentDAL.DMS_PendingDocRequestCount(VersionID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DMS_PendingFormRequestCount(int VersionID, int Status)
        {           
            try
            {
                return objDocumentDAL.DMS_PendingFormRequestCount(VersionID,Status);
            }
            catch (Exception e)
            {
                throw e;
            }           
        }
        public int DMS_PendingFormRequestsByUserCount(int VersionID, int Status)
        {
            try
            {
                return objDocumentDAL.DMS_PendingFormRequestsByUserCount(VersionID, Status);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public bool TMS_AllowDocumentActions(int ActionType, int DocumentID, int VersionID)
        {
            try
            {
                return objDocumentDAL.TMS_AllowDocumentActions(ActionType, DocumentID, VersionID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DMS_AllowDocumentActions(int ActionType, int DocumentID, int VersionID)
        {
            try
            {
                return objDocumentDAL.DMS_AllowDocumentActions(ActionType, DocumentID, VersionID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion DocumentRevisionValidation
        #region NotificationDocVersionID
        public DataTable DMS_NotificationDocVersionID(int NotificationID)
        {
            try
            {
                return objDocumentDAL.DMS_NotificationDocVersionID(NotificationID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion NotificationDocVersionID
        public DataTable DMS_NotificationFormVersionID(int NotificationID)
        {
            try
            {
                return objDocumentDAL.DMS_NotificationFormVersionID(NotificationID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_NotificationDocRequestID(int NotificationID)
        {
            try
            {
                return objDocumentDAL.DMS_NotificationDocRequestID(NotificationID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_NotificationFormRequestID(int NotificationID)
        {
            try
            {
                return objDocumentDAL.DMS_NotificationFormRequestID(NotificationID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        #region ReportFirstPageData
        public DataSet DMS_ShowFirstPageDetails(string DMS_VersionID, ref SqlTransaction _transaction, ref SqlConnection DMS_Con)
        {
            DataSet DMS_Dt = new DataSet();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_ShowFirstPageDetails(DMS_VersionID, ref  _transaction, ref  DMS_Con);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
            public DataSet DMS_ShowFirstPageDetails(string DMS_VersionID)
        {
            DataSet DMS_Dt = new DataSet();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_ShowFirstPageDetails(DMS_VersionID);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion ReportFirstPageData
        public DataTable DMS_GetTempRevertedReviewersDetails(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_GetTempRevertedReviewersDetails(doc);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_DocComments(DocObjects doc)
        {
          
            DataTable DMS_Dt = new DataTable();
            //int DMS_Ins;
            try
            {                
                DMS_Dt = objDocumentDAL.DMS_DocComments(doc);                
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetFormCopyNum(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_GetFormCopyNum(doc);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetDetailsFormDB

        #region CreatorActions 
        public int DMS_UpdateContentByID(ReviewObjects reviewObjects)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_UpdateContentByID(reviewObjects);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_DocumentToReviewer(DocUploadBO reviewObjects)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_DocumentToReviewer(reviewObjects);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion CreatorActions 

        #region QAActions
        public int DMS_ApproveDocument(ReviewObjects reviewObjects)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_ApproveDocument(reviewObjects);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_RejectDocument(ReviewObjects reviewObjects)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_RejectDocument(reviewObjects);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion QAActions

        #region Referral links
        public DataTable DMS_GetInternalDocumentsbyDepartment(int DocId)
        {
            try
            {
                return objDocumentDAL.DMS_GetInternalDocumentsbyDepartment(DocId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DMS_insertLinkReferences(ReferralBO obj_referralBO)
        {
            try
            {
                return objDocumentDAL.DMS_insertLinkReferences(obj_referralBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DMS_insertReferalInternalDocs(List<ReferralBO> lst_referralBO)
        {
            try
            {
                return objDocumentDAL.DMS_insertReferalInternalDocs(lst_referralBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DMS_insertReferalExternalFiles(ReferralBO obj_ReferralBO)
        {
            try
            {
                return objDocumentDAL.DMS_insertReferalExternalFiles(obj_ReferralBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DMS_GetLinkReferences(int VersionID)
        {
            try
            {
                return objDocumentDAL.DMS_GetLinkReferences(VersionID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable DMS_GetReferalInternalDocs(int VersionID)
        {
            try
            {
                return objDocumentDAL.DMS_GetReferalInternalDocs(VersionID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable DMS_GetReferalExternalFiles(int VersionID)
        {
            try
            {
                return objDocumentDAL.DMS_GetReferalExternalFiles(VersionID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DMS_GetExistingFileCount(string File_Name,int VersionID)
        {
            try
            {
                return objDocumentDAL.DMS_GetExistingFileCount(File_Name, VersionID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public byte[] DMS_GetExternalFileContent(int FileID)
        {
            try
            {
                return objDocumentDAL.DMS_GetExternalFileContent(FileID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DMS_DeleteExternalFiles(int @Ref_FileID)
        {
            try
            {
                return objDocumentDAL.DMS_DeleteExternalFiles(@Ref_FileID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable DMS_TempCrudExternalFiles(TempExternalFileBO tempExternalFileBO)
        {
            try
            {
                return objDocumentDAL.DMS_TempCrudExternalFiles(tempExternalFileBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion Referral links

        #region StatusNoficationCards
        public int RevertedInitator(int EmpID, string Status)
        {
            try
            {
                return objDocumentDAL.RevertedInitator(EmpID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #region StatusNoficationByRequestType
        public DataTable StatusNoficationByRequestType_Dt(int EmpID, string Status, int RequestTypeID)
        {
            try
            {
                return objDocumentDAL.StatusNoficationByRequestType_dt(EmpID, Status, RequestTypeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int StatusNoficationByRequestType(int EmpID, string Status, int RequestTypeID)
        {
            try
            {
                return objDocumentDAL.StatusNoficationByRequestType(EmpID, Status, RequestTypeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion StatusNoficationByRequestType
        #region GetPrintDistributionNotificationCards
        public DataSet DMS_GetPrintDistributionNotificationCards(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetPrintDistributionNotificationCards(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetPrintDistributionNotificationCards
        #endregion StatusNoficationCards

        #region Charts
        #region HomeDocumentListChart
        public DataTable DMS_HomeDocumentListChart(int DepartmentID, int DocuentTypeID, int mode, int EmpID)
        {
            try
            {
                return objDocumentDAL.DMS_HomeDocumentListChart(DepartmentID,DocuentTypeID,mode,EmpID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion HomeDocumentListChart
        #region RecentlyRevisedDocumentsChart
        public DataTable DMS_RecentlyRevisedDocumentsChart(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_RecentlyRevisedDocumentsChart(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion RecentlyRevisedDocumentsChart
        #region DocumentsReachingReviewDateChart
        public DataTable DMS_ReachingReviewDateChart(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_ReachingReviewDateChart(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion DocumentsReachingReviewDateChart
        #region DocumentStatusChart
        public DataTable DMS_DocumentStatusChart(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_DocumentStatusChart(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion DocumentStatusChart
        #endregion Charts

        #region DocRequest
        #region SubmitDocPrintRequest
        public int DMS_DocPrintRequestSubmission(DocObjects doc, out int DocReqID)
        {
            try
            {
                return objDocumentDAL.DMS_DocPrintRequestSubmission(doc,out DocReqID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion SubmitDocPrintRequest
        #region UpdateDocPrintRequestUsers
        public int DMS_UpdateDocPrintRequestUsers(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_UpdateDocPrintRequestUsers(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion UpdateDocPrintRequestUsers
        #region GetDocPrintReqList
        public DataSet DMS_DocPrintReqList(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                return objDocumentDAL.DMS_DocPrintReqList(_objJQDataTableBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetDocPrintReqList
        #region GetDocPrintReqDetails
        public DataTable DMS_GetDocPrintReqDetails(string DMS_DocReqID)
        {
            try
            {
                return objDocumentDAL.DMS_GetDocPrintReqDetails(DMS_DocReqID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetDocPrintReqDetails
        #region GetDocDistributionHistory
        public DataTable DMS_GetDocDistributionHistory(string DMS_DocReqID)
        {
            try
            {
                return objDocumentDAL.DMS_GetDocDistributionHistory(DMS_DocReqID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetDocDistributionHistory       
        #region GetDocReqStatus
        public DataTable DMS_GetDocReqLatestStatus(string DMS_DocReqID)
        {
            try
            {
                return objDocumentDAL.DMS_GetDocReqLatestStatus(DMS_DocReqID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetDocReqStatus
        #endregion DocRequest

        #region Form
        #region FormCreation
        public int DMS_FormInsertion(DocUploadBO doc)
        {
            try
            {
                return objDocumentDAL.DMS_FormInsertion(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormCreation
        #region UpdateFormUsers
        public int DMS_UpdateFormUsers(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_UpdateFormUsers(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion UpdateFormUsers
        #region GetFormList
        public DataSet DMS_GetFormList(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                return objDocumentDAL.DMS_GetFormList(_objJQDataTableBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetFormList
        #region GetFormContent
        public DataTable DMS_GetFormContent(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            //int DMS_Ins;
            try
            {
                DMS_Dt = objDocumentDAL.DMS_GetFormContent(doc);
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetFormContent
        #region GetFormHistory
        public DataTable DMS_GetFormHistory(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetFormHistory(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetFormHistory
        #region GetFormDetails
        public DataTable DMS_GetFormDetails(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetFormDetails(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetFormDetails
        public DataTable DMS_FormHistoryList(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_FormHistoryList(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #region FormUpdate
        public int DMS_FormUpdation(DocUploadBO doc)
        {
            try
            {
                return objDocumentDAL.DMS_FormUpdation(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormUpdate
        #region FormRevision
        public int DMS_FormRevision(DocUploadBO doc)
        {
            try
            {
                return objDocumentDAL.DMS_FormRevision(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormRevision
        #region FormWithdraw
        public int DMS_FormWithdraw(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_FormWithdraw(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormWithdraw
        #region FormRevisionUpdate
        public int DMS_FormRevisionUpdate(DocUploadBO doc)
        {
            try
            {
                return objDocumentDAL.DMS_FormRevisionUpdate(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormRevision
        #endregion Form

        #region FormRequest
        #region SubmitFormPrintRequest
        public int DMS_FormPrintRequestSubmission(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_FormPrintRequestSubmission(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion SubmitFormPrintRequest
        #region UpdateFormPrintRequestUsers
        public int DMS_UpdateFormPrintRequestUsers(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_UpdateFormPrintRequestUsers(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion UpdateFormPrintRequestUsers
        #region GetFormPrintRequestList
        public DataSet DMS_GetFormPrintRequestList(JQDataTableBO _objJQDataTableBO)
        {
            try
            {
                return objDocumentDAL.DMS_GetFormPrintRequestList(_objJQDataTableBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetFormPrintRequestList
        #region GetFormPrintReqDetails
        public DataSet DMS_GetFormPrintReqDetails(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetFormPrintReqDetails(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetFormPrintReqDetails
        #region GetFormPrintReqHistory
        public DataTable DMS_GetFormPrintReqHistory(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetFormPrintReqHistory(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetFormPrintReqHistory
        #region GetEndSNoofPreviousFormRequest
        public DataTable DMS_GetEndSNoofFormRequest(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetEndSNoofFormRequest(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetEndSNoofPreviousFormRequest
        #endregion FormRequest

        #region FormReturn
        public int DMS_SubmitFormReturn(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_SubmitFormReturn(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_VerifyFormReturn(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_VerifyFormReturn(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetFormReqReturnDetails(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetFormReqReturnDetails(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetFormReturnReportDetails(FormPrintRequest formPrint)
        {
            try
            {
                return objDocumentDAL.DMS_GetFormReturnReportDetails(formPrint);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion FormReturn

        #region TempDocuments
        public int DMS_InsertDocumentLocation(DocObjects doc, ref SqlTransaction _Transaction, ref SqlConnection DMS_Con)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_InsertDocumentLocation(doc, ref  _Transaction, ref  DMS_Con);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
            public int DMS_InsertDocumentLocation(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_InsertDocumentLocation(doc);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetTempDocumentPath(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetTempDocumentPath(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetTempDocumentDetails(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetTempDocumentDetails(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetEveryoneTempDocumentPath(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetEveryoneTempDocumentPath(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetEveryoneChangedDocumentPath(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetEveryoneChangedDocumentPath(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int DMS_UpdateSaveStatus(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_UpdateSaveStatus(doc);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetSaveStatus(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetSaveStatus(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetDocumentLocationList(DocObjects docref,ref SqlTransaction _transaction, ref SqlConnection DMS_Con)
        {
            try
            {
                return objDocumentDAL.DMS_GetDocumentLocationList(docref, ref _transaction, ref  DMS_Con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetDocumentLocationList(DocObjects docref)
        {
            try
            {
                return objDocumentDAL.DMS_GetDocumentLocationList(docref);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion TempDocuments

        #region TempForms
        public int DMS_InsertFormLocation(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                DMS_Ins = objDocumentDAL.DMS_InsertFormLocation(doc);
                return DMS_Ins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable DMS_GetFormLocationList(DocObjects doc)
        {
            DataTable DMS_Dt = new DataTable();
            int DMS_Ins;
            try
            {
                return objDocumentDAL.DMS_GetFormLocationList(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion TempForms

        #region ViewDocument
        public DataTable DMS_GetPhysicalPathforViewing(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetPhysicalPathforViewing(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion ViewDocument

        #region Document Recommendations
        public int DMS_DeleteDocumentRecommendations(ReviewObjects reviewObjects)
        {
            try
            {
                return objDocumentDAL.DMS_DeleteDocumentRecommendations(reviewObjects);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion Document Recommendations
        //public DataTable BindListViewer(int DeptID)
        //{
        //    try
        //    {
        //        return objDocumentDAL.BindListViewer(DeptID);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}
        //public DataTable GetDeptCode(int EmpID)
        //{
        //    try
        //    {
        //        return objDocumentDAL.GetDeptCode(EmpID);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}
        //public DataTable DMS_GetUserRoles()
        //{
        //    DataTable DMS_Dt = new DataTable();
        //    int DMS_Ins;
        //    try
        //    {
        //        DMS_Dt = objDocumentDAL.DMS_GetUserRoles();
        //        return DMS_Dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}
        //public DataSet DMS_GetEmpDeptList(int EmpID)
        //{
        //    try
        //    {
        //        return objDocumentDAL.DMS_GetEmpDeptList(EmpID);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }

        //}
        //public DataTable DMS_GetSopList(DocObjects doc)
        //{
        //    try
        //    {
        //        string Val_A;
        //        if (doc.dept == "All")
        //        {
        //            Val_A = "0";
        //        }
        //        else
        //        {
        //            Val_A = doc.dept;
        //        }

        //        doc.dept = Val_A;

        //        return objDocumentDAL.DMS_GetsopList(doc);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }

        //}
        //public DataTable DMS_AutoGenerateDocumentID()
        //{
        //    DataTable DMS_Dt = new DataTable();
        //    try
        //    {
        //        DMS_Dt = objDocumentDAL.DMS_AutoGenerateDocumentID();
        //        return DMS_Dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}
        //public DataTable DMS_GetDocumentDetailsByID(string DMS_DocumentID)
        //{
        //    DataTable DMS_Dt = new DataTable();
        //    int DMS_Ins;
        //    try
        //    {
        //        DMS_Dt = objDocumentDAL.DMS_GetDocumentDetailsByID(DMS_DocumentID);
        //        return DMS_Dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}
        //public DataTable DMS_GetContent(string DMS_DocumentID)
        //{
        //    DataTable DMS_Dt = new DataTable();
        //    int DMS_Ins;
        //    try
        //    {
        //        DMS_Dt = objDocumentDAL.DMS_GetContent(DMS_DocumentID);
        //        return DMS_Dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}
        public DataTable DMS_GetDocumentTypeList(DocObjects doc)
        {
            try
            {
                return objDocumentDAL.DMS_GetDocumentTypeList1(doc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int DMS_MultipleEditRestriction(int FormVID)
        {
            try
            {
                return objDocumentDAL.DMS_MultipleEditRestriction(FormVID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public DataTable DMS_GetDocumentTypeID()
        //{
        //    DataTable DMS_Dt = new DataTable();
        //    try
        //    {
        //        DMS_Dt = objDocumentDAL.DMS_GetDocumentTypeID();
        //        return DMS_Dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}
        //#region DocInitiationNotification
        //public int DocInitiationNotification(DocObjects doc)
        //{
        //    int DMS_Return;
        //    try
        //    {
        //        DMS_Return = objDocumentDAL.DocInitiationNotification(doc);
        //        return DMS_Return;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}
        //#endregion DocInitiationNotification
        //public void ListBoxItems(ListBox source, ListBox destination, string text)
        //{
        //    if (text == "R")
        //    {
        //        for (int i = source.Items.Count - 1; i >= 0; i--)
        //        {
        //            if (source.Items[i].Selected)
        //            {

        //                source.Items.Remove(source.Items[i]);
        //            }
        //        }
        //    }
        //    if (text == "A")
        //    {
        //        for (int i = source.Items.Count - 1; i >= 0; i--)
        //        {
        //            if (source.Items[i].Selected)
        //            {


        //                destination.Items.Add(source.Items[i]);
        //                source.Items.Remove(source.Items[i]);
        //            }
        //        }
        //    }

        //}
        //public void defaultListBoxItems(ListBox ls1, ListBox ls2)
        //{

        //    for (int i = 0; i < ls1.Items.Count; i++)
        //    {
        //        ls2.Items.Add(ls1.Items[i]);

        //    }
        //}
        //public DataTable GetListBoxItems(ListBox ls, DataTable dt)
        //{


        //    try
        //    {
        //        List<int> list = new List<int> { };

        //        for (int i = dt.Rows.Count - 1; i >= 0; i--)
        //        {
        //            for (int j = 0; j < ls.Items.Count; j++)
        //            {
        //                string a = dt.Rows[i]["EmpID"].ToString();
        //                string b = ls.Items[j].ToString();

        //                if (a == ls.Items[j].Value)
        //                {
        //                    list.Add(i);
        //                }

        //            }
        //            continue;
        //        }

        //        foreach (int f in list)
        //        {
        //            dt.Rows.RemoveAt(f);
        //        }

        //        return dt;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}

        public DataTable DMS_GetMainDocumentDetailsList(JQDataTableBO _objJQDataTableBO,int DocumetID)
        {
            try
            {
                return objDocumentDAL.DMS_GetMainDocumentDetailsList(_objJQDataTableBO,DocumetID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #region GetPrinterNames
        public DataSet BAL_GetPrinterNaems()
        {
            try
            {
                return objDocumentDAL.DAL_GetPrinterNamest();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion GetDocPrintReqList

    }
}
