﻿using Aizant_API_Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AizantIT_DMSBAL
{
    class DocNameBAL
    {
        public string GetNameBasedonDocumentID(int DocumentID,char status)
        {
            var DocumentName = "";
            using (AizantIT_DevEntities dbAizantIT_DMS_DocName = new AizantIT_DevEntities())
            {
                if (status == 'A')//A-Active
                {
                    DocumentName = (from t1 in dbAizantIT_DMS_DocName.AizantIT_DocVersionTitle
                                    join t2 in dbAizantIT_DMS_DocName.AizantIT_DocVersion on t1.DocTitleID equals t2.DocTitleID
                                    where t2.DocumentID == DocumentID && t2.Status == "A"
                                    select t1.Title).SingleOrDefault();
                }
                else if (status == 'E')//E-Effective
                {
                    DocumentName = (from t1 in dbAizantIT_DMS_DocName.AizantIT_DocVersionTitle
                                    join t2 in dbAizantIT_DMS_DocName.AizantIT_DocVersion on t1.DocTitleID equals t2.DocTitleID
                                    where t2.DocumentID == DocumentID && (t2.Status == "A" || t2.Status == "R") && t2.EffectiveDate != null
                                    select t1.Title).SingleOrDefault();
                }
                else if (status == 'L')//L-Latest
                {
                    DocumentName = (from t1 in dbAizantIT_DMS_DocName.AizantIT_DocVersionTitle
                                    join t2 in dbAizantIT_DMS_DocName.AizantIT_DocVersion on t1.DocTitleID equals t2.DocTitleID
                                    where t2.VersionID == (from t3 in dbAizantIT_DMS_DocName.AizantIT_DocVersion
                                                           where t3.DocumentID == DocumentID
                                                           select t3.VersionID).Max()
                                    select t1.Title).SingleOrDefault();
                }
            }
            return DocumentName;
        }
        public string GetNameBasedonDocumentVersionID(int VersionID)
        {
            using (AizantIT_DevEntities dbAizantIT_DMS_DocName = new AizantIT_DevEntities())
            {
                var DocumentName = (from t1 in dbAizantIT_DMS_DocName.AizantIT_DocVersionTitle
                               join t2 in dbAizantIT_DMS_DocName.AizantIT_DocVersion on t1.DocTitleID equals t2.DocTitleID
                               where t2.VersionID == VersionID
                               select t1.Title).SingleOrDefault();
                return DocumentName;
            }         
        }
    }
}
