﻿using Aizant_API_Entities;
using API_BussinessObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace API_BusinessLayer
{
    public class API_BAL
    {
        public bool IsModuleIdExists(int ModuleId)
        {
            try
            {
                using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
                {
                    return dbContext.AizantIT_Modules.Where(a => a.ModuleID == ModuleId).Any();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool IsUserAuthenticated(string SessionID, string EncryptedEmpID)
        {
            try
            {
                UMS_BusinessLayer.UMS_BAL objUMSBAL = new UMS_BusinessLayer.UMS_BAL();
                int EmpID = Convert.ToInt32(objUMSBAL.Decrypt(EncryptedEmpID));
                using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
                {
                    if (dbContext.AizantIT_UserLoginMaster.Where(a => a.ActiveUserSession == SessionID && a.EmpID == EmpID).Any())
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Notification
        public int GetNotificationCount(int EmpID)
        {
            using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
            {
                var UnseenCount = dbContext.AizantIT_SP_GetUserNotificationCount(EmpID).SingleOrDefault();
                return Convert.ToInt32(UnseenCount);
            }
        }
        public string GetTop5UserNotifications(int EmpID, out int TotalCount)
        {
            using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
            {
                var NotificationData = dbContext.AizantIT_SP_GetTop5UserNotifications(EmpID);

                StringBuilder strNotifcation = new StringBuilder();

                string _Type = "Top5Notification";
                int _TotalCount = 0;
                foreach (AizantIT_SP_GetTop5UserNotifications_Result item in NotificationData)
                {
                    if (_TotalCount < 5)
                    {
                        string _Tooltip = item.NotificationLink == "#" ? "Click On Close Button To Remove the Notification." : "Click to Visit The Page.";
                        //string RefLink = GetNotificationReferenceLink(item.NotificationLink, Convert.ToInt32(item.NotificationID), item.ModuleName);

                        //strNotifcation.Append(
                        // string.Format(@"<div title='{0}' class='panel_list_notificaton_{1}' id='{2}'>
                        // <a href='{3}' onclick='RedirectTo(this);' class='notification_name col-lg-11 float-left' >{4}</a><span onclick='{5}'
                        // class=' col-lg-1  float-left CloseNotification' title='Click to Clear'>x</span>
                        // <div class='col-lg-12  float-left'><div class='float-left moudle_label_{6}' >{6}</div><div class='float-right moudle_time'>{7}</div></div></div>",
                        //     item.NotificationLink == "#" ? "Click On Close Button To Remove the Notification." : "Click to Visit The Page.",
                        //     item.Status.ToString(),
                        //     item.NotificationID.ToString(),
                        //     RefLink,
                        //     item.Description.ToString(),
                        //     "ClearNotify(" + item.NotificationID + "," + EmpID + ")",
                        //     item.ModuleName,
                        //     item.GeneratedDateStr));
                        strNotifcation.Append(GetUserNotificationDescription(_Tooltip, item.NotificationLink, Convert.ToInt32(item.NotificationID), item.ModuleName, item.GeneratedDate.ToString(), item.Description, item.Status.ToString(), EmpID, _Type));

                    }
                    _TotalCount++;
                }
                if (_TotalCount == 0)
                {
                    strNotifcation.Append("<div id='ntyEmpty' style='text-align:center;font-weight:bold;'>No Notifications To Show</div>");
                }
                TotalCount = _TotalCount;
                return strNotifcation.ToString();
            }
        }
        public List<NotificationBO> GetNotificationsToUserList(int DisplayStart, int DisplayLength, int EmpID, out int TotalRecordCount)
        {
            using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
            {
                List<NotificationBO> objNotificationBO = new List<NotificationBO>();

                string _Type = "AllNotification";
                ObjectParameter objParm = new ObjectParameter("TotalRecords", typeof(int));
                var UserNotificationList = dbContext.AizantIT_SP_GetUserNotificationsList(EmpID, DisplayLength, DisplayStart, "", objParm);

                //foreach (var item in UserNotificationList)
                //{
                //    string _Status = (item.Status == 1 ? (item.NotificationLink == "#" ? "Not Checked" : "NA") : "checked");
                //    string ReferenceLink = GetNotificationReferenceLink(item.NotificationLink, Convert.ToInt32(item.NotificationID), item.ModuleName);
                //    objNotificationBO.Add(new NotificationBO(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.NotificationID), item.Description, item.GeneratedDate.ToString(), item.ModuleName, item.NotificationLink, ReferenceLink, (item.Status == 1 ? (item.NotificationLink == "#" ? "Not Checked" : "NA") : "checked")));

                //}

                foreach (var item in UserNotificationList)
                {
                    var _Tooltip = item.NotificationLink == "#" ? "" : "Click to Visit The Page.";
                    string _Description = GetUserNotificationDescription(_Tooltip, item.NotificationLink, item.NotificationID, item.ModuleName, item.GeneratedDate.ToString(), item.Description, item.Status.ToString(), EmpID, _Type);
                    objNotificationBO.Add(new NotificationBO(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.NotificationID), _Description, item.Status.ToString()));
                }

                TotalRecordCount = Convert.ToInt32(objParm.Value);
                return objNotificationBO;
            }
        }
        public void SubmitNotifications(int Notification_ID, int Emp_ID)
        {
            int Notify_status = 3;
            int[] empIDs = new int[] { Emp_ID };
            using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
            {
                DataTable dtEmpID = new DataTable();
                dtEmpID.Columns.Add("EmpID");
                DataRow dr;
                foreach (int EmpID in empIDs)
                {
                    dr = dtEmpID.NewRow();
                    dr["EmpID"] = EmpID;
                    dtEmpID.Rows.Add(dr);
                }
                SqlParameter ParamNotification_ID = new SqlParameter("@NotificationID", Notification_ID);
                SqlParameter ParamNotifyStatus = new SqlParameter("@NotificationStatus", Notify_status);
                SqlParameter ParamEmpIDs = new SqlParameter("@NotifyToEmpIDs", dtEmpID);
                ParamEmpIDs.TypeName = "[dbo].[AizantIT_Employee_IDs]";
                dbContext.Database.ExecuteSqlCommand("exec [dbo].[AizantIT_SP_Notify_Status_Update] @NotificationID,@NotificationStatus,@NotifyToEmpIDs", ParamNotification_ID, ParamNotifyStatus, ParamEmpIDs);
            }
        }
        //public string GetNotificationReferenceLink(string NotificationLink,int NotificationID,string ModuleName)
        //{
        //    string ReferenceLink = "#";
        //    if (NotificationLink.Contains('?'))
        //    {
        //        ReferenceLink = NotificationLink + "&Notification_ID=" + NotificationID;
        //    }
        //    else
        //    {
        //        ReferenceLink = NotificationLink + "?Notification_ID=" + NotificationID;
        //    }
        //    if (ModuleName == "QMS")
        //    {
        //        ReferenceLink = "/" + ReferenceLink;
        //        if (NotificationLink.Contains('#'))
        //        {
        //            ReferenceLink = "#";
        //        }
        //    }
        //    return ReferenceLink;
        //}
        //Notification Description 
        public string GetUserNotificationDescription(string _Tooltip, string NotificationLink, int NotificationID, string ModuleName, string GeneratedDate, string Description, string Status, int EmpID, string _Type)
        {
            // string ReferenceLink = GetNotificationReferenceLink(NotificationLink, NotificationID,ModuleName);
            string ReferenceLink = "#";
            if (NotificationLink.Contains('?'))
            {
                ReferenceLink = NotificationLink + "&Notification_ID=" + NotificationID;
            }
            else
            {
                ReferenceLink = NotificationLink + "?Notification_ID=" + NotificationID;
            }
            if (ModuleName == "QMS")
            {
                ReferenceLink = "/" + ReferenceLink;
                if (NotificationLink.Contains('#'))
                {
                    ReferenceLink = "#";
                }
            }

            string _DescriptionLink = string.Format(@"<div title='{0}' class='panel_list_notificaton_{1}' id='{2}'>
                         <{3} href='{4}' onclick='RedirectTo(this);' class='notification_name col-lg-11 float-left' >{5}</{3}><span onclick='{6}'
                         class=' col-lg-1  float-left CloseNotification text-right' title='Click to Clear'>x</span>
                         <div class='col-lg-12  float-left'><div class='float-left moudle_label_{7}' >{7}</div><div class='float-right moudle_time'>{8}</div></div></div>",
                 _Tooltip,
                 Status,
                 NotificationID.ToString(),
                 _Tooltip == "" ? "span" : "a",
                 ReferenceLink,
                 Description,
                 _Type == "Top5Notification" ? "ClearNotify(" + NotificationID + "," + EmpID + ",this)" : "ClearNotifyTolist(" + NotificationID + "," + EmpID + ",this)",
                 ModuleName,
                 GeneratedDate
                 );
            return _DescriptionLink;
        }
        #endregion
        #region ML Enabled
        public int[] GetMLFeatureChecking(int ModuleId)
        {
            using (var dbContext = new AizantIT_DevEntities())
            {
                var ML_FeatureIDs = (from T1 in dbContext.AizantIT_IsML_FeaturesEnabled
                                     join T2 in dbContext.AizantIT_ML_FeatureLinkUpModule on T1.ML_FeatureID equals T2.ML_FeatureID
                                     where (T2.SourceModuleID == ModuleId || T2.DestinationModuleID == ModuleId) && T1.IsEnabled == true
                                     select T1.ML_FeatureID).ToArray();
                return ML_FeatureIDs;
            }
        }
        #endregion
        #region DocumentRecomended
        //public List<DocumentRecommendation> GetDocumentRecommendationList(BasicJQ_DT_BO objTMS_DT_BO)
        //{
        //    using (var dbContext = new AizantIT_DevEntities())
        //    {
        //        try
        //        {
        //            var totalRecordsCount = 0;
        //            List<DocumentRecommendation> objDocumentRecommendationList = new List<DocumentRecommendation>();
        //            var DocumentRecommendationData = dbContext.AizantIT_SP_GetDocumentRecommendation_List(objTMS_DT_BO.DisplayLength, objTMS_DT_BO.DisplayStart,
        //                objTMS_DT_BO.SortCol, objTMS_DT_BO.SortDir, objTMS_DT_BO.Search);

        //            foreach (var item in DocumentRecommendationData)
        //            {
        //                if (totalRecordsCount == 0)
        //                {
        //                    totalRecordsCount = Convert.ToInt32(item.TotalCount);
        //                }
        //                objDocumentRecommendationList.Add(new DocumentRecommendation(Convert.ToInt32(item.DocVersionID), item.DocumentName, Convert.ToInt32(item.DocRecommendationCount), Convert.ToInt32(item.RowNumber)));
        //            }
        //            objTMS_DT_BO.TotalRecordCount = totalRecordsCount;
        //            return objDocumentRecommendationList;
        //        }
        //        catch (Exception ex)
        //        {
        //            throw new Exception(ex.Message);
        //        }
        //    }
        //}
        //Training Document Recommended List
        public List<DocumentRecommendation> GetTrainingDocumentRecommendedList(BasicJQ_DT_BO objTMS_DT_BO, int TraineeID)
        {
            using (var dbContext = new AizantIT_DevEntities())
            {
                try
                {
                    var totalRecordsCount = 0;
                    List<DocumentRecommendation> objTrainingDocumentRecommendedList = new List<DocumentRecommendation>();
                    var trainingDocumentRecommendedData = dbContext.AizantIT_SP_GetTrainingDocumentRecommended_List(objTMS_DT_BO.DisplayLength, objTMS_DT_BO.DisplayStart,
                        objTMS_DT_BO.SortCol, objTMS_DT_BO.SortDir, objTMS_DT_BO.Search, TraineeID);

                    foreach (var item in trainingDocumentRecommendedData)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objTrainingDocumentRecommendedList.Add(new DocumentRecommendation(item.DocumentName, Convert.ToInt32(item.RowNumber)));
                    }
                    objTMS_DT_BO.TotalRecordCount = totalRecordsCount;
                    return objTrainingDocumentRecommendedList;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        #endregion


        public int GetDocumentRecommendedCount(int TraineeID)
        {
            using (var dbContext = new AizantIT_DevEntities())
            {
                var DocRecommendedCount = dbContext.AizantIT_SP_GetDocumentRecommendationCount(TraineeID).SingleOrDefault();
                return Convert.ToInt32(DocRecommendedCount);
            }
        }

    }
}
