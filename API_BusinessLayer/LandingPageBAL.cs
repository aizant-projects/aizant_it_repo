﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UMS_BusinessLayer;

namespace API_BusinessLayer
{    
    public class LandingPageBAL
    {
        API_BussinessObjects.LandingPageBO landingPage = new API_BussinessObjects.LandingPageBO();
        UMS_BAL UMBAL = new UMS_BAL();
        public API_BussinessObjects.LandingPageBO HomePage(DataSet ds,String str,int i)
        {
            if (i == 0)
            {
                if (str != null)
                {
                    int EmpID = Convert.ToInt32(ds.Tables[0].Rows[0]["EmpID"]);
                    DataSet ds1 = UMBAL.ShowExpireDate(EmpID, out int RemainingCount, out int Status);
                    string expireMsg = "";
                    if (Status == 1)
                    {
                        expireMsg = "Your password will expire in " + (RemainingCount == 1 ? RemainingCount + " day" : RemainingCount + " days") + ", Please reset your password.";
                    }
                    landingPage.ExpireMessage = expireMsg;
                }
            }
            else if (i == 1)
            {
                int ModuleRoleCount = 0;
                DataTable dtUserModuleRoles = ds.Tables[i];
                for(int j = 0; j < dtUserModuleRoles.Rows.Count; i++)
                {
                    DataRow row = dtUserModuleRoles.Rows[j];
                    bool[] showModules= { };
                    showModules.SetValue(true, j);
                    string[] navigateUrls = { };
                    string[] moduleImages = { };
                    string[] moduleNames = { };
                    if (row["ModuleID"].ToString() == "1")
                    {
                        ModuleRoleCount ++;                        
                        landingPage.ShowModules= showModules;
                        navigateUrls.SetValue("", j);
                        //landingPage.NavigateUrls =;
                        moduleImages.SetValue("../Images/Landing/icon3.png", j);
                        landingPage.ModuleImages =moduleImages;
                        moduleNames.SetValue("Administration<br/>Setup", j);
                        landingPage.ModuleNames = moduleNames;
                    }
                    else if(row["ModuleID"].ToString() == "2")
                    {

                    }
                }
            }
            return landingPage;
        }
    }
}
