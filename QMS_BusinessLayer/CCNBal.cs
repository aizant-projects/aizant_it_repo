﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QMS_BO.QMS_CAPABO;
using QMS_BO.QMS_ChangeControl_BO;
using QMS_DataLayer;

namespace QMS_BusinessLayer
{
    public class CCNBal
    {
        CCNDal QMS_DAL = new CCNDal();

        public DataSet GetCCNListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch,  int Role, /*int CCN_ID,*/
            string QualityEvent, int RoleIDCharts, int DeptidDb, string FromDateDb, string ToDateDb,int EmployeeID
            , string sSearch_CCN_Number, string sSearch_Department, string sSearch_Block, string sSearch_Initiatedby
            , string sSearch_Classification, string sSearch_CCN_Date, string sSearch_CurrentStatus)
        {
            try
            {
                return QMS_DAL.GetCCNListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, Role, /*CCN_ID,*/ QualityEvent,
                    RoleIDCharts, DeptidDb, FromDateDb, ToDateDb,EmployeeID
                    ,sSearch_CCN_Number,sSearch_Department,sSearch_Block,sSearch_Initiatedby
            ,sSearch_Classification,sSearch_CCN_Date,sSearch_CurrentStatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable CCNInsertBal(CCNMaster cCN, string Status,int IsTempRefID)
        {
                return QMS_DAL.CCNInsertDal(cCN, Status, IsTempRefID);
        }

        public int HODActionCCNBal(CCNMaster cCNMaster)
        {
            try
            {
                return QMS_DAL.HODActionCCNDal(cCNMaster);
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }
        public int QAActionCCNBal(CCNMaster cCNMaster)
        {
            try
            {
                return QMS_DAL.QAActionCCNDal(cCNMaster);
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }
        public int HeadQAActionCCNBal(CCNMaster cCNMaster)
        {
            try
            {
                return QMS_DAL.HeadQAActionCCNDal(cCNMaster);
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }
        public int AssessmentRevertHeadQABAL(CCNMaster cCNMaster)
        {
            try
            {
                return QMS_DAL.AssessmentRevertHeadQADAL(cCNMaster);
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }
        public int RefershAssessmentHeadQAPAgeBAL(CCNMaster cCNMaster)
        {
            try
            {
                return QMS_DAL.RefershAssessmentHeadQAPAgeDAL(cCNMaster);
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }
        
        public int AssessmenT_ActionsCCNBal(CCNMaster cCNMaster)
        {
            try
            {
                return QMS_DAL.AssessmenT_ActionsCCNDal(cCNMaster);
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }
        public int QAAction_CCNCloserBal(CCNClose objCCN)
        {
            try
            {
                return QMS_DAL.QAAction_CCNCloserDal(objCCN);
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }
        public void DeleteCapaTemp(int AssessmentId,string Status, int QualityEventTypeID)
        {
            try
            {
                 QMS_DAL.DeleteCapaTemp(AssessmentId, Status, QualityEventTypeID);
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }
        public int DeleteCCN_ActionPlanAndCCCRecordBal(int id, string operation, int ActionBy, string ActionFileNo)
        {
            try
            {
                return QMS_DAL.DeleteCCN_ActionPlanAndCCCRecordsDal(id, operation, ActionBy, ActionFileNo);
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }

        public DataTable AddCCN_CommitteBal(int EmpID, int UinqueID, int cCNid, int blockid, int empid1, int deptid, string opeation, int assStatus)
        {
            try
            {
                return QMS_DAL.AddCCN_CommitteDal(EmpID, UinqueID, cCNid, blockid, empid1, deptid, opeation, assStatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int AddCCN_DepartmentBlockCommiteeBAL(DataTable dtDeptImpactlst)
        {
            try
            {
                return QMS_DAL.AddCCN_DepartmentBlockCommiteeDal(dtDeptImpactlst);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int AddCCN_HODimpactPointInsertBal(int EmpID, DataTable dtImpactlst,int CCNID, string comments, int DeptidEmpty, int blockEmpty, int Status, int AssessmentID)
        {
            try
            {
                return QMS_DAL.AddCCN_ImpactPointDal(EmpID, dtImpactlst,CCNID, comments, DeptidEmpty, blockEmpty, Status, AssessmentID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable CCNSplitDeptStringBal(string cCNMaster)
        {
            try
            {
                return QMS_DAL.CCNSplitDeptStringDal(cCNMaster);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCCN_DeptAssBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int CCNID)
        {
            try
            {

                return QMS_DAL.GetCCN_DeptAssListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, CCNID);
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }
        public DataSet GetCCN_VerificationBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int CCNID)
        {
            try
            {

                return QMS_DAL.GetCCN_VerificationListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, CCNID);
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }
        public DataSet GetCCNAssessmentIdBAL( int CCNID)
        {
            try
            {

                return QMS_DAL.GetCCNAssessmentDAL( CCNID);
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }
        public DataSet GetHodAttachments(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int CCNID,int AssessID,string Status)
        {
            try
            {
                return QMS_DAL.GetHodAttachmentsDAL(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, CCNID, AssessID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetddlBindingCCNCreate(int empid)
        {
            try
            {
                return QMS_DAL.GetDropdownbindingCCNCreateDAL(empid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateAssessmentHODsBal(int HodEmpId, int AssessmentID,int CCNID,int ActionBy,string AssessmentComments, out int Result)
        {
            try
            {
                return QMS_DAL.UpdateAssessmentHODsDAL(HodEmpId, AssessmentID, CCNID,ActionBy, AssessmentComments, out Result);
            }catch(Exception ex)
            {
                throw ex;
            }
        }
        public int ModifiedAssignedEmployeeInAdminBAL(int EmpId, int ccnID,int Status, int ActionBy, string ReassignComments, out int Result)
        {
            try
            {
                return QMS_DAL.ModifiedAssignedEmployeeInAdminDAL(EmpId, ccnID, Status,ActionBy,ReassignComments, out Result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int ModifiedCAPA_EmployeesInAdminManageBAL(int EmpId, int capaid, int Status, int ActionBy, string GetReAssignComments, out int Result)
        {
            try
            {
                return QMS_DAL.ModifiedCAPA_EmployeesInAdminManageDAL(EmpId, capaid, Status, ActionBy,GetReAssignComments, out Result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int QASaveDraftBAL(CCNMaster cCNMaster)
        {
            try
            {
                return QMS_DAL.QAActionSaveDraft(cCNMaster);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
