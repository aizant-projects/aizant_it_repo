﻿using QMS_DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using QMS_BO.QMS_Errata_BO;
using QMS_BO.QMS_CAPABO;
using QMS_BO.QMS_NoteTofile_BO;
using QMS_BO.QMS_Incident_BO;
using QMS_BO.QMS_Dashboard_BO;

namespace QMS_BusinessLayer
{
    public class QMS_BAL
    {
        QMS_DAL QMS_DAL = new QMS_DAL();
        public DataSet GetActionPlanLISTBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            try
            {
                return QMS_DAL.GetActionPlanLISTDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetBlockMasterLISTBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            try
            {
                return QMS_DAL.GetBlockMasterLISTDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetImpactPointLISTBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            try
            {
                return QMS_DAL.GetImpactPointLISTDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetTypeChangeMasterLISTBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            try
            {
                return QMS_DAL.GetTypeChangeMasterLISTDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetImpactPointAndDeptLinkBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            try
            {
                return QMS_DAL.GetImpactPointAndDeptLinkDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCAPAListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, 
            string sSearch, string Role, int RoleIDCharts, int DeptidDb, string FromDateDb,
            string ToDateDb, int CapaQualityEventType, int OverDueCapaRoleID, int RptDeptID, string RptFrmDate, string RptToDate,
            int LoginEmpID,int RptQualityEvent, int isAll,
            string sSearch_CAPA_Number, string sSearch_Department, string sSearch_QualityEventName, string sSearch_QualityEvent_Number, string sSearch_EmployeeName
                    , string sSearch_CAPA_Date, string sSearch_TargetDate, string sSearch_CurrentStatus)
        {
            try
            {
                return QMS_DAL.GetCAPAListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, 
                    sSearch,Role, RoleIDCharts, DeptidDb, FromDateDb, ToDateDb, CapaQualityEventType,
                    OverDueCapaRoleID, RptDeptID, RptFrmDate, RptToDate, LoginEmpID, RptQualityEvent, isAll,
                     sSearch_CAPA_Number, sSearch_Department, sSearch_QualityEventName, sSearch_QualityEvent_Number, sSearch_EmployeeName
                    , sSearch_CAPA_Date, sSearch_TargetDate, sSearch_CurrentStatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        public DataSet GetCCNListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int EmpID, int HOD, int QA, int Head_QA, string Role, int CCN_ID, int Qadmin, string QualityEvent, int RoleIDCharts, int DeptidDb, string FromDateDb, string ToDateDb)
        {
            try
            {
                return QMS_DAL.GetCCNListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, EmpID, HOD, QA, Head_QA, Role, CCN_ID, Qadmin, QualityEvent, RoleIDCharts, DeptidDb, FromDateDb, ToDateDb);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetCCNReportListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0,
            string sSearch,string Role, int DeptidRT, string FromDateRt, string ToDateRt,int EmployeeID, int isAll,
            string sSearch_CCN_Number, string sSearch_Department, string sSearch_Initiatedby
            , string sSearch_Classification, string sSearch_CCN_Date, string sSearch_Due_Date, string sSearch_CurrentStatus)//
        {
            try
            {
                return QMS_DAL.GetCCNReportListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, 
                    Role, DeptidRT, FromDateRt, ToDateRt, EmployeeID, isAll,
                     sSearch_CCN_Number, sSearch_Department, sSearch_Initiatedby
            , sSearch_Classification, sSearch_CCN_Date, sSearch_Due_Date, sSearch_CurrentStatus);//
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetAssignedImpactPoints(int dept, int blockid)
        {
            try
            {
                return QMS_DAL.GetAssignedImpactPointDal(dept, blockid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable CapaInsertBal(CapaModel capaModel)
        {
            try
            {
                return QMS_DAL.CapaInsertDal(capaModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetddlBindingCAPACreateBAL(int empid)
        {
            try
            {

                return QMS_DAL.GetddlBindingCAPACreateDAL(empid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet BalDashboard(int Empid)
        {
            try
            {
                return QMS_DAL.Getdashboarddal(Empid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet BalGetdashboardCCN(int Empid)
        {
            try
            {
                return QMS_DAL.GetdashboardCCN(Empid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable CapaReminderBAL1(CapaReminder capaModel, string Role,out int Result)
        {
            try
            {
                return QMS_DAL.CapaReminderDal(capaModel, Role,out Result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCapaHistoryBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int CapaId)
        {
            try
            {
                return QMS_DAL.GetCapaHistoryDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, CapaId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet CapaTranaction(int CAPAID, string TypeOfTransaction)
        {
            try
            {
                return QMS_DAL.Get_TransactionCapa(CAPAID, TypeOfTransaction);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet CapaTranactionInsertUpdateBAL(CapaModel objcapabo, out int ResultOputparm)
        {
            try
            {
                return QMS_DAL.Get_TransactionCapaInsertUpdateAction(objcapabo,out ResultOputparm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public DataSet GetCAPAVerificationBAL(MainListCapaBO objCapa,out int Result)
        {
            try
            {
                return QMS_DAL.GetCAPAVerificationDAL(objCapa, out Result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet CAPAVerificationApproveRevertBAL(MainListCapaBO objCAPA,out int Result)
        {
            try
            {
                return QMS_DAL.CAPAVerificationApprovalAndRevertDAL(objCAPA, out Result);
            }catch(Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCAPAHQAApprovalBAL(MainListCapaBO objCapa, out int Result)
        {
            try
            {
                return QMS_DAL.GetCAPAHQAApprovalDAL(objCapa, out Result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCAPACloseBAL(MainListCapaBO objCapa, out int Result, out int AllCpapCompleted)
        {
            try
            {
                return QMS_DAL.GetCAPACloseDAL(objCapa, out Result, out AllCpapCompleted);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCapaFileUploadListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int CapaID)
        {
            try
            {
                return QMS_DAL.GetCapaFileUploadListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, CapaID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public DataSet GetCCNFileUploadListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int CCNID)
        //{
        //    try
        //    {
        //        return QMS_DAL.GetCCNFileUploadListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, CCNID);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public DataTable CapaUpdateBal(CapaModel capaModel, out int ResultStatus)
        {
            try
            {
                return QMS_DAL.CapaUpdateDal(capaModel,out ResultStatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable CapaEmpStatusBal(CapaModel capaModel,out int ResultStatus)
        {
            try
            {
                return QMS_DAL.CapaEmpStatus(capaModel,out ResultStatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region NoteFile
        public DataSet GetNoteToFileListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int ShowType, int UserEmpID, int RoleIDCharts,
            int DeptidDb, string FromDateDb, string ToDateDb, string sSearch_NoteToFileNo, string sSearch_DeptName, string sSearch_ReferenceNumber, string sSearch_DocTitle, string sSearch_PreparedBy
            , string sSearch_DocCreateDate, string sSearch_CurrentStatus)
        {
            try
            {

                return QMS_DAL.GetNoteToFileListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, ShowType,
                    UserEmpID, RoleIDCharts, DeptidDb, FromDateDb, ToDateDb,
                     sSearch_NoteToFileNo,  sSearch_DeptName,  sSearch_ReferenceNumber,  sSearch_DocTitle,  sSearch_PreparedBy
            ,  sSearch_DocCreateDate,  sSearch_CurrentStatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable NoteToFileInsertBAL(NoteFileBO NTFBO)
        {
            try
            {
                return QMS_DAL.NoteToFileInsertDal(NTFBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateNoteToFileBAL(NoteFileBO NTFBO)
        {
            try
            {
                return QMS_DAL.UpdateNoteToFileDal(NTFBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int RevertHODBAL(NoteFileBO NTFBO)
        {
            try
            {
                return QMS_DAL.RevertHODDAL(NTFBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public int NTFCAPAInsertBal(NoteFileBO NTFBO, string action)
        //{

        //    return QMS_DAL.NTFCapaInsertDal(NTFBO, action);
        //}
        public DataSet GetEventsCAPAListBAL(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int EventsBaseID, int QualityEventTypeID,int AssessmentID)
        {
            try
            {
                return QMS_DAL.GetEventsCAPAListDAL(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, EventsBaseID, QualityEventTypeID, AssessmentID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int NTFSubmittedToQABal(NoteFileBO NTFBO)
        {
            try
            {
                return QMS_DAL.NTFSubmittedToQADal(NTFBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Electronic Sign Verification
        public void ElectronicSignVerification(NoteFileBO NTFBO, out int SignVerified)
        {
            try
            {
                NTFBO.Esign = Encrypt(NTFBO.Password);
                QMS_DAL.ElectronicSignVerificationDb(NTFBO, out SignVerified);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable BalGetAutoGenaratedNumber(int Deptid, int QualityNo)
        {
            try
            {
                DataTable dt = new DataTable();
                return QMS_DAL.DalGetAutoGenaratedNumber(Deptid, QualityNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void BalCAPAAutoGenaratedNumber(int Deptid, out string CAPANumber, out int CAPA_MaxNo)
        {
            try
            {
                QMS_DAL.DalCAPAAutoGenaratedNumber(Deptid, out CAPANumber, out CAPA_MaxNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region PasswordEncription/Decription
        public string Encrypt(string clearText)
        {
            try
            {
                string EncryptionKey = "AizantGlobalITAnalytics";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
                return clearText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string Decrypt(string cipherText)
        {
            try
            {
                string EncryptionKey = "AizantGlobalITAnalytics";
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
                return cipherText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Errata
        public DataSet GetErrataListDalBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, string Role, int EmployeeID, int RoleIDCharts, int DeptidDb, string FromDateDb, string ToDateDb, 
             string sSearch_DeptName, string sSearch_ErrataFileNo, string sSearch_DocumentNo, string sSearch_DocTitle, string sSearch_EmpName,
            string sSearch_Initiator_SubmitedDate, string sSearch_SectionNo, string sSearch_Status)
        {
            try
            {

                return QMS_DAL.GetErrataListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, Role, EmployeeID, RoleIDCharts, DeptidDb, FromDateDb, ToDateDb,
                    sSearch_DeptName, sSearch_ErrataFileNo, sSearch_DocumentNo, sSearch_DocTitle, sSearch_EmpName,
                    sSearch_Initiator_SubmitedDate, sSearch_SectionNo, sSearch_Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable ErrataFileInsertBal(ErrataBO ERTABO, string Status)
        {
            try
            {
                return QMS_DAL.ErrataFileInsertDal(ERTABO, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ElectronicSignVerification(ErrataBO ERATA, out int SignVerified)
        {
            try
            {
                ERATA.Esign = Encrypt(ERATA.Password);
                QMS_DAL.ElectronicSignVerificationDb(ERATA, out SignVerified);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int ErrataRevertBAL(ErrataBO ERATA)
        {
            try
            {
                return QMS_DAL.ErrataRevertDal(ERATA);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int ErrataSubmittedToQABal(ErrataBO ERTABO, string Status, DataTable dtImpactlst)
        {
            try
            {
                return QMS_DAL.ErrataSubmittedToQADal(ERTABO, Status, dtImpactlst);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Incident
        public DataSet GetIncidentList(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int UserID, int ShowType, int RoleIDCharts, int DeptidDb, string FromDateDb, string ToDateDb, string IncidentFileterEvent,
            string sSearch_IRNumber, string sSearch_DeptName, string sSearch_DocumentNo, string sSearch_DocumentName, string sSearch_EmpName, string sSearch_CreatedDate, string sSearch_OccurrenceDate, string sSearch_IncidentDueDate, string sSearch_CurrentStatus)
        {
            try
            {
                return QMS_DAL.GetIncidentListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, UserID, ShowType, RoleIDCharts, DeptidDb, FromDateDb, ToDateDb, IncidentFileterEvent,
                   sSearch_IRNumber, sSearch_DeptName, sSearch_DocumentNo, sSearch_DocumentName, sSearch_EmpName, sSearch_CreatedDate, sSearch_OccurrenceDate, sSearch_IncidentDueDate, sSearch_CurrentStatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //For getting Incident Report List
        public DataSet GetIncidentReportList(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int UserID, int deptIdReport, string FromDateReport, string ToDateReport,int Role, int isAll,
            string sSearch_IRNumber, string sSearch_DeptName, string sSearch_EmpName, string sSearch_CreatedDate, string sSearch_OccurrenceDate, string sSearch_IncidentDueDate, string sSearch_CurrentStatus, string sSearch_TypeofIncidentName)
        {
            try
            {
                return QMS_DAL.GetIncidentReportListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, UserID, deptIdReport, FromDateReport, ToDateReport, Role,isAll,
                    sSearch_IRNumber, sSearch_DeptName, sSearch_EmpName, sSearch_CreatedDate, sSearch_OccurrenceDate, sSearch_IncidentDueDate, sSearch_CurrentStatus, sSearch_TypeofIncidentName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable IncidentInsertBal(IncidentBO incidentBO, string Operation)
        {
            try
            {
                return QMS_DAL.IncidentInsertDal(incidentBO, Operation);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetIncidentPreliminaryAssessmentListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int IncidentMasterID, int isAll)
        {
            try
            {
                return QMS_DAL.GetIncidentPreliminaryAssessmentListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID, isAll);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetIncidentAnyotherinvestigationBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int IncidentMasterID, int isAll)
        {
            try
            {
                return QMS_DAL.GetIncidentAnyotherinvestigationDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID, isAll);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetIncidentWhoarethepeopleinvolvedListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int IncidentMasterID)
        {
            try
            {
                return QMS_DAL.GetIncidentWhoarethepeopleinvolvedListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable SendIncidentNotificationBal(int IncidentMasterID, int EmpID, int Action)
        {
            try
            {
                return QMS_DAL.SendIncidentNotificationDal(IncidentMasterID, EmpID, Action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public int IncidentRevertAndRejectBal(IncidentBO objIncidentBO, int Action_No, int WhomToRevertID)
        {
            try
            {
                return QMS_DAL.IncidentRevertAndRejectDal(objIncidentBO, Action_No, WhomToRevertID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public DataSet GetRemainderExitsorNot(int EmpID, int CapaID)
        {
            try
            {
                return QMS_DAL.GetRemainderExitsorNot(EmpID, CapaID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int InsertInvestigationDetailsBal(IncidentBO objincidentBO, DataTable dtImpactlst, DataTable dtHasAffedtedlst, DataTable dtWasAffedtedlst, DataTable dtReviewlst)
        {
            try
            {
                return QMS_DAL.InsertInvestigationDetailsDal(objincidentBO, dtImpactlst, dtHasAffedtedlst, dtWasAffedtedlst, dtReviewlst);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetIncidentHasAffectedDocumentListtBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int IncidentMasterID)
        {
            try
            {
                return QMS_DAL.GetIncidentHasAffectedDocumentListtDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int InsertInvestigationQNImpactingDetailsBal(IncidentBO objincidentBO, DataTable dtQNImpactlst)
        {
            try
            {
                return QMS_DAL.InsertInvestigationQNImpactingDetailsDal(objincidentBO, dtQNImpactlst);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetIncidentFileUploadListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int IncidentMasterID)
        {
            try
            {
                return QMS_DAL.GetIncidentFileUploadListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetIncidentInitiatorFileUploadListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int IncidentMasterID, int Status)
        {
            try
            {
                return QMS_DAL.GetIncidentInitiatorFileUploadListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetTotalCountToChartBal(int roletype, int Deptid, int employeeid, string fromDate, string toDate)
        {
            try
            {
                return QMS_DAL.GetTotalCountToChartDal(roletype, Deptid, employeeid, fromDate, toDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetTotalCountToCAPADeptWiseBal(int roletype, int employeeid, string fromDate, string toDate)
        {
            try
            {
                return QMS_DAL.GetTotalCountToCAPADeptWiseDal(roletype, employeeid, fromDate, toDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet capaStatusInnerChartBAL(string Argument, int RoleType, int employeeid, string FromDate, string ToDate, int QulaityEventTypeID)
        {
            try
            {
                return QMS_DAL.capaStatusInnerChartDAl(Argument, RoleType, employeeid, FromDate, ToDate, QulaityEventTypeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetPendingTotalCountToChartBal(string QualityEvent, int RoleType, int eventid, int employeeid, string FromDate, string ToDate)
        {
            try
            {
                return QMS_DAL.GetPendingTotalCountToChartDAL(QualityEvent, RoleType, eventid, employeeid, FromDate, ToDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetManagementChartDataBAL(DataTable dtDeptlst, DataTable dtQElst, string FromDate, string ToDate)
        {
            return QMS_DAL.GetManagementChartDataDAL(dtDeptlst, dtQElst, FromDate, ToDate);
        }


        public DataSet GetStatusChartForQualityEventsBAL(DataTable dtDeptlst, int QualityEventType, int CCNClassification,
           int CCNCategory, int IncidentClassification, int IncidentCategory, string FromDate, string ToDate)
        {
            return QMS_DAL.GetStatusChartForQualityEventsDAL(dtDeptlst, QualityEventType, CCNClassification, CCNCategory, IncidentClassification, IncidentCategory, FromDate, ToDate);
        }

        public DataSet GetOverdueChartBAL(DataTable dtDeptlst, int QualityEventType, int CCNClassification,
           int CCNCategory, int IncidentClassification, int IncidentCategory, string FromDate, string ToDate)
        {
            return QMS_DAL.GetOverdueChartDAL(dtDeptlst, QualityEventType, CCNClassification, CCNCategory, IncidentClassification, IncidentCategory, FromDate, ToDate);
        }

        //public DataSet GetPerformanceChartCountBAL(int DeptID, int QualityEventType, int CCNClassification,
        //   int CCNCategory, int IncidentClassification, int IncidentCategory, string FromDate, string ToDate)
        //{
        //    return QMS_DAL.GetPerformanceChartCountDAL(DeptID, QualityEventType, CCNClassification,
        //       CCNCategory, IncidentClassification, IncidentCategory, FromDate, ToDate);
        //}

        public DataSet GetPerformanceChartCountBAL(QMS_BO.QMS_Dashboard_BO.PerformanceChartCountBO pobj)
        {
            return QMS_DAL.GetPerformanceChartCountDAL(pobj);
        }

        public DataSet GetPerformanceChartTATBAL(QMS_BO.QMS_Dashboard_BO.PerformanceChartCountBO ptobj)
        {
            return QMS_DAL.GetPerformanceChartTATDAL(ptobj);
        }

        public DataSet GetCCNListBAL(DataTable dtDeptlst, int showtype, int CCNClassification, int CCNCategory, string FromDate
                , string ToDate)
        {
            return QMS_DAL.GetCCNListDAL(dtDeptlst, showtype, CCNClassification, CCNCategory, FromDate
                , ToDate);
        }
        public DataSet GetIncidentListBAL(DataTable dtDeptlst, int showtype, int IncidentClassification, int IncidentCategory, string FromDate
               , string ToDate)
        {
            return QMS_DAL.GetIncidentListDAL(dtDeptlst, showtype, IncidentClassification, IncidentCategory, FromDate
                , ToDate);
        }
        public DataSet GetErrataListBAL(DataTable dtDeptlst, int showtype, string FromDate
               , string ToDate)
        {
            return QMS_DAL.GetErrataListDAL(dtDeptlst, showtype, FromDate
                , ToDate);
        }
        public DataSet GetNTFListBAL(DataTable dtDeptlst, int showtype, string FromDate
              , string ToDate)
        {
            return QMS_DAL.GetNTFListDAL(dtDeptlst, showtype, FromDate
                , ToDate);
        }

        public DataSet GetIncidentListViewForPerformanceChartBAL(PerformanceChartCountBO PLobj)
        {
            return QMS_DAL.GetIncidentListViewForPerformanceChartDAL(PLobj);
        }
        public DataSet GetCCNListViewForPerformanceChartBAL(PerformanceChartCountBO PLobj)
        {
            return QMS_DAL.GetCCNListViewForPerformanceChartDAL(PLobj);
        }
        public DataSet GetErrataListViewForPerformanceChartBAL(PerformanceChartCountBO PLobj)
        {
            return QMS_DAL.GetErrataListViewForPerformanceChartDAL(PLobj);
        }
        public DataSet GetNTFListViewForPerformanceChartBAL(PerformanceChartCountBO PLobj)
        {
            return QMS_DAL.GetNTFListViewForPerformanceChartDAL(PLobj);
        }
        public DataSet GetCAPAChartDataBAL(DataTable dtDeptlst, DataTable dtQElst, string FromDate, string ToDate)
        {
            return QMS_DAL.GetCAPAChartDataDAL(dtDeptlst, dtQElst, FromDate, ToDate);
        }
        public DataSet GetCAPAStatusChartBAL(DataTable dtDeptlst, int QualityEventType, int TypeofAction,
           string FromDate, string ToDate)
        {
            return QMS_DAL.GetCAPAStatusChartDAL(dtDeptlst, QualityEventType, TypeofAction, FromDate, ToDate);
        }

        public DataSet GetIncidentDepartmentReviewHodListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int IncidentMasterID)
        {
            try
            {
                return QMS_DAL.GetIncidentDepartmentReviewHodListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SubmitHODCapaDetailsBal(IncidentBO IncidentBO)
        {
            try
            {
                return QMS_DAL.SubmitHODCapaDetailsDal(IncidentBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteInvestigationFormisTempBal(int IncidentMasterID)
        {
            try
            {
                QMS_DAL.DeleteInvestigationFormisTempDal(IncidentMasterID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteTTSwhenNoBal(int IncidentMasterID, int UserID)
        {
            try
            {
                QMS_DAL.DeleteTTSwhenNoDal(IncidentMasterID, UserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public DataSet GetEmpNamesbasedonRoleIDBal(int RoleID, String Status)
        {
            try
            {
                return QMS_DAL.GetEmpNamesbasedonRoleIDDal(RoleID,Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

            public void CreateTargetTraining(TTS_QIObjects objTTS, out int result)
        {
            try
            {
                QMS_DAL.CreateTargetTrainingDb(objTTS, out result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateTargetTraining(TTS_QIObjects objTTS, out int result)
        {
            try
            {
                QMS_DAL.UpdateTargetTrainingDb(objTTS, out result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetTargetTrainingDetails(string TTS_ID, int EmpID, out int CurrentHistoryStatus, out int TargetCurrentStatus)
        {
            try
            {
                int iTTS_ID = Convert.ToInt32(TTS_ID);
                return QMS_DAL.GetTargetTrainingDetailsDb(iTTS_ID, EmpID, out CurrentHistoryStatus, out TargetCurrentStatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetTTSIDBal(int IncidentMasterID1)
        {
            try
            {
                return QMS_DAL.GetTTSIDDal(IncidentMasterID1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetIstrainingExitsornotBal(int IncidentMasterID)
        {
            try
            {
                return QMS_DAL.GetIstrainingExitsornotDal(IncidentMasterID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetIncidentTTS_ListBal(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int IncidentMasterID)
        {
            try
            {
                return QMS_DAL.GetIncidentTTS_ListDb(displayLength, displayStart, sortCol, sSortDir, sSearch, IncidentMasterID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int IncidentHasAffectedDocInsertandUpdateBal(IncidentBO IncidentBO, string Operation)
        {
            try
            {
                return QMS_DAL.IncidentHasAffectedDocInsertandUpdateDal(IncidentBO, Operation);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetCapaCompltFileuploadBAL(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int CAPAID, string Status, string VerificationID = null)
        {
            try
            {
                return QMS_DAL.GetCapaCompltFileUploadDAL(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, CAPAID, Status, VerificationID );
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
        public DataSet GetAccessibleDepartmentsBAL(int EmpId,int RoleID,int ModuelID)
        {
            try
            {
                return QMS_DAL.GetAccessibleDepartmentsDAL(EmpId, RoleID, ModuelID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public DataSet GetCapaAuditDetailsBAL(int ResponseID)
        {
            try
            {
                return QMS_DAL.GetCapaAuditDetailsDAL(ResponseID);
            }catch(Exception ex)
            {
                throw ex;
            }
        }

        // For Getting Incident GetFileName
        public DataSet GetIncidentFileNameBal(int IncidentMasterID)
        {
            try
            {
                return QMS_DAL.GetIncidentFileNameDal(IncidentMasterID);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //For Getting Change Control GetFileName
        public DataSet GetCCNFileNameBal(int CCNID)
        {
            try
            {
                return QMS_DAL.GetCCNFileNameDal(CCNID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int NTFAdminManageBal(NoteFileBO NTFBO, out int Result)
        {
            try
            {
                return QMS_DAL.NTFAdminManageDAL(NTFBO, out Result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int ERTAdminManageBal(ErrataBO ERTBO, out int Result)
        {
            try
            {
                return QMS_DAL.ERTAdminManageDAL(ERTBO, out Result);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        public int INCAdminManageBal(IncidentBO INCBO, out int Result)
        {
            try
            {
                return QMS_DAL.INCAdminManageDAL(INCBO, out Result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //For Updation Review Hod's in Incident
        public int UpdateRevieewHodsBal(int HodEmpId, int UniqueID, int IncidentMasterID, int ActionBy, string ReviewHODComments, out int Result)
        {
            try
            {
                return QMS_DAL.UpdateReviewHODsDAL(HodEmpId, UniqueID, IncidentMasterID, ActionBy, ReviewHODComments, out Result);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        //End
        public DataSet GetEmployeesDeptandRoleWiseInAdminManageBAL(int DeptId,int QualityEvnet)
        {
            try
            {
                return QMS_DAL.GetEmployeesDeptandRoleWiseInAdminManageDAL(DeptId, QualityEvnet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        //Bal method for insert Map Quality Notifications
        public int AddMapQualityNotificationBal(DataTable dtMapQultyNtf)
        {
            try
            {
                return QMS_DAL.AddMapQualityNotificationDAL(dtMapQultyNtf);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Bal method for insert Map INCIDENT
        public int AddMapINCI_InsertBal(DataTable dtMapIncident_lst)
        {
            try
            {
                return QMS_DAL.AddMapINCI_InsertDal(dtMapIncident_lst);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //For Retrieving Data from Single SP for Report View
        public DataSet GetIncident_Report_From_SingleSP_BAL(int IncidentMasterID)
        {
            try
            {
                return QMS_DAL.GetIncident_Report_From_SingleSP_DAL(IncidentMasterID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetIncident_TTS_DateBAL(int IncidentID)
        {
            try
            {
                return QMS_DAL.GetIncident_TTS_DateDAL(IncidentID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetMapIncidentNumberBal(int IncidentMasterID, int Deptid)
        {
            try
            {
               return QMS_DAL.GetMapIncidentNumberDal(IncidentMasterID, Deptid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetAllEmployeesBal(int IncidentMasterID, int Deptid)
        {
            try
            {
                return QMS_DAL.GetAllEmployeesDal(IncidentMasterID, Deptid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //end
        #region SingleTonBAL
        //public static QMS_BAL ClassObjQMS_BAL;
        //public static QMS_BAL GetInstance()
        //{
        //    if (ClassObjQMS_BAL == null)
        //    {
        //        ClassObjQMS_BAL = new QMS_BAL();
        //    }
        //    return ClassObjQMS_BAL;
        //}
        //public DataSet GetErrataDetailsBAL()
        //{
        //    return QMS_DAL.GetInstanceDAL().GetErrataList();
        //}
        #endregion
        public DataTable GetMapQualityNotifBal(int EventTypeID, int DeptID, int IncidentMasterID)
        {
            try
            {
                return QMS_DAL.GetMapQualityNotifDal(EventTypeID, DeptID, IncidentMasterID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
