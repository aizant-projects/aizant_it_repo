﻿//using AizantIT_PharmaApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UMS_BO;
using UMS_DataLayer;

namespace UMS_BusinessLayer
{
    public class UMS_BAL
    {
        UMS_DAL objUMS_DAL;

        #region Notifications
        public DataTable getNotificationsData(string empID)
        {
            DataTable dt = new DataTable();
            try
            {
                objUMS_DAL = new UMS_DAL();
                int EmpID = Convert.ToInt32(empID);
                return objUMS_DAL.getNotificationsDataDb(EmpID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void submitNotifications(string notification_ID, int notify_status, int[] empIDs)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int Notification_ID = Convert.ToInt32(notification_ID);
                DataTable dtEmpID = new DataTable();
                dtEmpID.Columns.Add("EmpID");
                DataRow dr;
                foreach (int EmpID in empIDs)
                {
                    dr = dtEmpID.NewRow();
                    dr["EmpID"] = EmpID;
                    dtEmpID.Rows.Add(dr);
                }
                objUMS_DAL.SubmitNotificationsDb(Notification_ID, notify_status, dtEmpID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        //Electronic Sign Verification
        public void ElectronicSignVerification(string sign, int EmpID, out int SignVerified)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                string Esign = Encrypt(sign);
                objUMS_DAL.ElectronicSignVerificationDb(Esign, EmpID, out SignVerified);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable getEmployeeDesignation(int EmpID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                return objUMS_DAL.getEmployeeDesignationDb(EmpID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable getModuleWiseDepts(int EmpID, int ModuleID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtResult = objUMS_DAL.getModuleWiseDeptsDb(EmpID, ModuleID);
                return dtResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable getRoleWiseDepts(int EmpID, int[] RoleIDs)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtRoles = new DataTable();
                dtRoles.Columns.Add("RoleID");
                DataRow dr;
                foreach (int RoleID in RoleIDs)
                {
                    dr = dtRoles.NewRow();
                    dr["RoleID"] = RoleID;
                    dtRoles.Rows.Add(dr);
                }
                DataTable dtResult = objUMS_DAL.getRoleWiseDeptsDb(EmpID, dtRoles);
                return dtResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #region AppSetUP
        public DataTable GetSessionTime()
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                return objUMS_DAL.GetSessionTime();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet AutoNumber(out string Status)
        {
            try
            {
                UMS_DAL objUMS_DAL = new UMS_DAL();
                return objUMS_DAL.AutoNumber(out Status);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet GetEmpCode(out int Status)
        {
            try
            {
                UMS_DAL objUMS_DAL = new UMS_DAL();
                return objUMS_DAL.GetEmpCode(out Status);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public int AddEmpCode(UserObjects Uobdal)
        {
            try
            {
                UMS_DAL objUMS_DAL = new UMS_DAL();
                return objUMS_DAL.AddEmpCode(Uobdal);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SecurityProfileBal(int TenureofPassword, int LockOutAttempts, int ExpiryAlertBefore, int SessionTimeout, string operation, int EmpId, string Comments, int RoleID)
        {
            UMS_DAL objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.SecurityProfileDal(TenureofPassword, LockOutAttempts, ExpiryAlertBefore, SessionTimeout, operation, EmpId, Comments, RoleID);
        }
        public DataTable SecurityProfileBal1(int TenureofPassword, int LockOutAttempts, int ExpiryAlertBefore, int SessionTimeout, string operation)
        {
            UMS_DAL objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.SecurityProfileDal1(TenureofPassword, LockOutAttempts, ExpiryAlertBefore, SessionTimeout, operation);
        }
        public DataTable GetSecurityProfileHistory()
        {
            UMS_DAL objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.GetSecurityProfileHistory();
        }

        #endregion

        #region UMSDashboard
        public int GetTotalUsers(string Operation)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                return objUMS_DAL.GetTotalUsers(Operation);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable getAccessibleAndToDoActionDepts(string empID, string v)
        {
            throw new NotImplementedException();
        }

        public DataTable ModulesWithRolesChart(int ModuelID, int RoleID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                return objUMS_DAL.ModulesWithRolesChart(ModuelID, RoleID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable BindDepartmetChart()
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                return objUMS_DAL.BindDepartmetChart();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable RolesChartBAL(int DeptID, int ModuleID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                return objUMS_DAL.RolesChartDAL(DeptID, ModuleID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //roles binddropdown 
        public DataTable BindDropDownRoles(int ModuelID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                return objUMS_DAL.BindDropDownRoles(ModuelID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Employee
        public int LoginIDCheckBAL(string LoginID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int count = objUMS_DAL.LoginIDCheckDAL(LoginID);
                return count;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetEmployeeDocments(string EmpCode,int CreateBy, string Action)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.GetEmployeeDocments(EmpCode, CreateBy, Action);
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int EmployeeDocCreationBal(int EmpID,int CreatedBy,DataTable dataTable, ref SqlConnection Sqlcon, ref SqlTransaction _Transaction)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int count1 = objUMS_DAL.EmployeeDocCreationDal(EmpID, CreatedBy, dataTable, ref Sqlcon, ref _Transaction);
                return count1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int EmployeeDoc(string EmpCode, int DocType, Byte[] bytes, string FileType, string FileName,int CreateBy, out int count)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int count1 = objUMS_DAL.EmployeeDoc(EmpCode, DocType, bytes, FileType, FileName, CreateBy, out count);
                return count1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int AddUserDetailsBAL(UserObjects Uobbal, int CreatedByEmpID, ref SqlConnection Sqlcon, ref SqlTransaction _Transaction)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
              int EmpId=  objUMS_DAL.AddUserDetailsDAL(Uobbal, CreatedByEmpID, ref  Sqlcon, ref  _Transaction); //Method in DAL
                return EmpId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public int LoginIDAndEmpCodeCheckBAL(string LoginID, string EmpCode, out int VerifyLoginID, out int VerifyEmpCode)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int count = objUMS_DAL.LoginIDAndEmpCodeCheckDAL(LoginID, EmpCode, out VerifyLoginID, out VerifyEmpCode);
                return count;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetUserDetails(int EmpID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.GetUserDetails(EmpID);
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int EditUserDetailsBAL(UserObjects Uobbal, int Empid)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int count = objUMS_DAL.EditUserDetailsDAL(Uobbal, Empid); //Method in DAL
                return count;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //public int AddDepartmentBAL(string DeptName, String DeptCode, out int VerifyDeptName)
        //{
        //    try
        //    {
        //        objUMS_DAL = new UMS_DAL();
        //        int count = objUMS_DAL.AddDepartmentDAL(DeptName, DeptCode,out VerifyDeptName);
        //        return count;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}
        #endregion

        #region EmployeeList
        public DataTable GVEmployeeList(string serachTerm, int PageSize, int PageIndex, out int RecordCount)
        {
            try
            {
                UMS_DAL objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.GVEmployeeList(serachTerm, PageSize, PageIndex, out RecordCount);
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int GetUMSTotalCount(string Operation)
        {
            UMS_DAL objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.GetUMSTotalCount(Operation);
        }
        public DataTable GetUMSLIST(int iDisplayLength, int iDisplayStart, int iSortCol_0,
       string sSortDir_0, string sSearch, string sSearch_DeptCode, string sSearch_DepartmentName, string OPeration, int ShowType, string DeptCode, int RoleID)
        {
            UMS_DAL objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.GetUMSLIST(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, sSearch_DeptCode, sSearch_DepartmentName, OPeration, ShowType, DeptCode, RoleID);
        }
        
        public DataTable GetEmployeeAuditHistory(int EmpID,int ModuleID)
        {
            UMS_DAL objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.GetEmployeeAuditHistory(EmpID, ModuleID);
        }
        
        public DataTable GetUMSEmployeeLIST(int iDisplayLength, int iDisplayStart, int iSortCol_0,
        string sSortDir_0, string sSearch, string sSearch_EmpCode, string sSearch_Name, string sSearch_DepartmentName, string sSearch_DesignationName,
        string sSearch_StartDate, string sSearch_Status, string OPeration, int ShowType, string DeptCode, int RoleID)
        {
            UMS_DAL objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.GetUMSEmployeeLIST(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, sSearch_EmpCode, sSearch_Name,
                sSearch_DepartmentName, sSearch_DesignationName,sSearch_StartDate, sSearch_Status, OPeration, ShowType, DeptCode, RoleID);
        }
            #endregion

            #region EmployeeLoginListHistory
            public DataTable GetUMS_BAL_LoginHistoryLIST(int iDisplayLength, int iDisplayStart, int iSortCol_0,
            string sSortDir_0, string sSearch, string sSearch_EmpCode, string sSearch_EmpName, string sSearch_LoginID, string sSearch_Login_Time,
            string sSearch_Logout_Time, string sSearch_UserHostAddress, string sSearch_UserClientAddress)
        {
            UMS_DAL objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.GetUMS_DAL_LoginHistoryLIST(iDisplayLength, iDisplayStart, iSortCol_0,
             sSortDir_0, sSearch, sSearch_EmpCode, sSearch_EmpName, sSearch_LoginID, sSearch_Login_Time,
             sSearch_Logout_Time, sSearch_UserHostAddress, sSearch_UserClientAddress);
        }

        #endregion

        #region DesignationList
        public DataTable GetDesignationList(int PageSize, int PageIndex, out int RecordCount)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.GetDesignationList(PageSize, PageIndex, out RecordCount);
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetDesigantionDetails(int desgID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.GetDesigantionDetails(desgID);
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int DesignationtBAL(string Operation, int EmpID, int desgid, string designationName, out int VerifyDesigName, int RoleID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int count = objUMS_DAL.DesignationtDAL(Operation, EmpID, desgid, designationName, out VerifyDesigName, RoleID);
                return count;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetDesignationHistory(int desgID)
        {
            objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.GetDesignationHistory(desgID);
        }
        #endregion

        #region DepartmentList
        public DataTable GetDepartmentList(int PageSize, int PageIndex, out int RecordCount)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.GetDepartmentList(PageSize, PageIndex, out RecordCount);
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetDepartmentDetails(int DeptID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.GetDepartmentDetails(DeptID);
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetDepartmentHistory(int DeptID)
        {
            objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.GetDepartmentHistory(DeptID);

        }

        #region User Logged In Check
        public void UserSessionUpdate(string userSessionID, string loginID, string UserHostAddress, string UserClientAddress, out int logPKID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                objUMS_DAL.UserSessionUpdateDB(userSessionID, loginID, UserHostAddress, UserClientAddress,out logPKID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void UserLogoutUpdate(string logPkid)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                objUMS_DAL.UserLogoutUpdate(logPkid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void RemoveActiveUserSession(string loginID, string password)
        {
            try
            {
                password = Encrypt(password);
                objUMS_DAL = new UMS_DAL();
                objUMS_DAL.RemoveActiveUserSessionDB(loginID, password);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetUserLoginHistory(int EMPID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtUH = objUMS_DAL.GetUserLoginHistory(EMPID);
                return dtUH;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
            #endregion

            public int UpdateEmpLockBal(string EmpCode, string comments, out int status, int CeatedBy, int RoleID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int count = objUMS_DAL.UpdateEmpLockDal(EmpCode, comments, out status, CeatedBy, RoleID);
                return count;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int DepartmentBAL(string Operation, int EmpID, int Deptid, string departmentName, string departmentCode, out int VerifyDept, int RoleID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int count = objUMS_DAL.DepartmentDAL(Operation, EmpID, Deptid, departmentName, departmentCode, out VerifyDept, RoleID);
                return count;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        #endregion

        #region PasswordEncription/Decription
        public string Encrypt(string clearText)
        {
            try
            {
                string EncryptionKey = "AizantGlobalITAnalytics";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
                return clearText;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string Decrypt(string cipherText)
        {
            try
            {
                string EncryptionKey = "AizantGlobalITAnalytics";
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
                return cipherText;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region UserLogin
        public int AddSignUpBAL(SigUPObjects Uobbal, out int VerifyUserID,out int StartDateStatus)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                if (Uobbal.ConfirmPassword != "")
                {
                    Uobbal.ConfirmPassword = Encrypt(Uobbal.ConfirmPassword);
                }
                if (Uobbal.SecurityAnswer != "")
                {
                    Uobbal.SecurityAnswer = Encrypt(Uobbal.SecurityAnswer);
                }
                int count = objUMS_DAL.AddSignUpDAL(Uobbal, out VerifyUserID,out StartDateStatus);
                return count;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet ShowExpireDate(int EmpID, out int RemainingCount, out int Status)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataSet dsBal = objUMS_DAL.ShowExpireDate(EmpID, out RemainingCount, out Status);
                return dsBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet CheckLoginBAL(SigUPObjects Uobbal, out int RemainingCount, out int Status, out char LoggedInStatus, out int isAdminResetPWD)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                Uobbal.ConfirmPassword = Encrypt(Uobbal.ConfirmPassword);
                DataSet dsBal = objUMS_DAL.CheckLoginDAL(Uobbal, out RemainingCount, out Status, out LoggedInStatus, out  isAdminResetPWD);
                return dsBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //Not in use
        //public void ResetWrongAttempts()
        //{
        //    try
        //    {
        //        objUMS_DAL = new UMS_DAL();
        //        objUMS_DAL.ResetWrongAttempts();
        //    }
        //    catch(Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}
        #endregion

        #region LoginsForActivation
        public DataTable GVLoginActivation()
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.GVLoginActivation();
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int UnLock(int EmpID, int UnlockedBy)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int count = objUMS_DAL.UnLock(EmpID, UnlockedBy);
                return count;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DropDowns
        public DataTable BindCountriesBAL()
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.BindCountriesDAL();
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable BindStatesBAL(int CountryID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.BindStatesDAL(CountryID);
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable BindCityBAL(int CountryID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.BindCityDAL(CountryID);
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable BindDepartmentsBAL()
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.BindDepartmentsDAL();
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable BindDesignationsBAL(int SelectedDepartmentID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.BindDesignationsDAL(SelectedDepartmentID);
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable BindAllDesignationsBAL()
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtBal = objUMS_DAL.BindAllDesignationsDAL();
                return dtBal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Get Employess For DropDown
        public DataTable GetEmployess(string DeptID = "" , int IsActive = 0)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dt = new DataTable();
                if (DeptID != "")
                {
                    int iDeptID = Convert.ToInt32(DeptID);
                    dt = objUMS_DAL.GetEmployessDb(iDeptID,IsActive);
                }
                else
                {
                    dt = objUMS_DAL.GetEmployessDb();
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Roles
        public DataTable GetModules()
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dt = objUMS_DAL.GetModules();
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet GetModuleRoles(string EmpID, string ModuleID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataSet ds = objUMS_DAL.GetModuleRoles(EmpID, ModuleID);
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string AssignEmpRoles(DataTable dtRoles, string EmpID, int CreatedBy, string ModuleID, DataTable dtDepts, int RoleId)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                objUMS_DAL.AssignRolesToEmpDb(dtRoles, EmpID, CreatedBy, ModuleID, dtDepts, RoleId);
                return "Roles Assigned to the Employee.";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetEmpModuleUnAssignedRoles(DataTable dtEmpRoles, string ModuleID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dtRoles = objUMS_DAL.GetModuleRolesDbOld(ModuleID);
                return dtRoles;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetModuleRolesonModuleId(string ModuleID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                DataTable dt = objUMS_DAL.GetModuleRolesDb(ModuleID);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void DeleteRolesToOdule(string EmpID, int CreatedBy, string ModuleID, int RoleId)
        {
            objUMS_DAL = new UMS_DAL();
            objUMS_DAL.DeleteRolesToOdule(EmpID, CreatedBy, ModuleID, RoleId);
        }
        #endregion

        #region ForgetPassword
        public DataTable ForgotPassword(string LoginID, string EmailID, string Operation, int EmpID, string SecurityAnswer, string NewPassword)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                if (SecurityAnswer != "")
                {
                    SecurityAnswer = Encrypt(SecurityAnswer);
                }
                if (NewPassword != "")
                {
                    NewPassword = Encrypt(NewPassword);
                }
                DataTable dt = objUMS_DAL.ForgotPassword(LoginID, EmailID, Operation, EmpID, SecurityAnswer, NewPassword);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region MyProfile
        public DataSet GetDataToMyProfile(int EmpID)
        {
            objUMS_DAL = new UMS_DAL();
            DataSet dt = objUMS_DAL.GetDataToMyProfile(EmpID);
            if (dt.Tables[0].Rows.Count > 0)
            {
                dt.Tables[0].Rows[0]["Answer"] = Decrypt(dt.Tables[0].Rows[0]["Answer"].ToString());
            }
            return dt;
        }
        public int MyprofileSecurityUpdate(int EmpID, string Question, string Answer)
        {
            //Update Security Question and (Answer) Encrypt Formatt
            objUMS_DAL = new UMS_DAL();
            if (Answer != "")
            {
                Answer = Encrypt(Answer);
            }
            return objUMS_DAL.MyprofileSecurityUpdate(EmpID, Question, Answer);
        }

        public DataTable MyprofileChangePassword(int EmpID, string Password, string Comments)
        {
            //Change Password Encrypt and Save to DB
            objUMS_DAL = new UMS_DAL();
            if (Password != "")
            {
                Password = Encrypt(Password);
            }
            return objUMS_DAL.MyprofileChangePassword(EmpID, Password, Comments);
        }

        public void ResetPassword(int EmpID, string Password, out int Status, UserObjects UOB, int isAdmin)
        {
            //Change Password Encrypt and Save to DB
            objUMS_DAL = new UMS_DAL();
            if (Password != "")
            {
                Password = Encrypt(Password);
            }
            objUMS_DAL.ResetPassword(EmpID, Password, out Status, UOB, isAdmin);
        }

        #endregion

        public string BindApplicantImage(byte[] applicantImage)
        {
            string base64ImageString = ConvertBytesToBase64(applicantImage);
            string Imgurl = "data:image/jpg;base64," + base64ImageString;
            return Imgurl;
        }
        public string ConvertBytesToBase64(byte[] imageBytes)
        {
            return Convert.ToBase64String(imageBytes);
        }

        #region Company
        public DataTable CompanyQuery(CompanyBO objcompanyBO)
        {
            objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.CompanyQuery(objcompanyBO);
        }
        #endregion company

        #region Get Employees
        public DataTable GetEmployeesByDeptIDandRoleID_BAL(int DeptID, int RoleID,bool OnlyActiveEmp,int EmpNameType= 1)
        {
            objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.GetEmployeesByDeptIDandRoleID_DAL(DeptID, RoleID,OnlyActiveEmp,EmpNameType);
        }
        #endregion

        #region Get Employess based on Module and Roles For DropDown
        public DataTable GetUMSEmployeeRole(int empid, int moduleid, int roleid, out bool IsAccessible)
        {
            objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.GetUMSEmployeeRole(empid, moduleid, roleid,out IsAccessible);
        }
        #endregion
        #region Emp wise AssignRoles
        public int EmpRolesAssign(int EmpID, int ModuleID, DataTable dtDepts, int RoleID, int CreatedBy)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();

                return objUMS_DAL.RolesToEmpAssignDb(EmpID, ModuleID, dtDepts, RoleID, CreatedBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string EmployeeRoleDelete(int EmpID, int ModuleID, int RoleID, int CreatedBy)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int result = objUMS_DAL.EmployeeRoleDelete(EmpID, ModuleID, RoleID, CreatedBy);

                if (result == 0)
                {
                    return "Error";
                }
                else
                {
                    return "Success";

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string RoleDepartmentsDelete(int EmpID, int ModuleID, int RoleId, int deptId, int CreatedBy)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int balResult = objUMS_DAL.RoleDepartmentsDelete(EmpID, ModuleID, RoleId, deptId, CreatedBy);
                if (balResult == 1)
                {
                    return "Success";
                }
                else
                {
                    return "Error";
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string RoleDepartmentsChecking(int EmpID, int ModuleID, int RoleID, int DeptID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int result = objUMS_DAL.RoleDepartmentsChecking(EmpID, ModuleID, RoleID, DeptID);
                if (result == 0)
                {
                    return "Error";
                }
                else
                {
                    return "Success";
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public string RoleDeleteChecking(int EmpID, int ModuleID, int RoleID)
        {
            try
            {
                objUMS_DAL = new UMS_DAL();
                int result = objUMS_DAL.RoleDeleteChecking(EmpID, ModuleID, RoleID);

                if (result == 0)
                {
                    return "Error";
                }
                else
                {
                    return "Success";
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public DataTable GetDepartmentsBasedOnRoleID(int empid, int moduleid, int roleid)
        {
            objUMS_DAL = new UMS_DAL();
            return objUMS_DAL.GetDepartmentsBasedOnRoleID(empid, moduleid, roleid);
        }
        #endregion


        ///*Notification*/

        //public int GetNotificationCount(int EmpID)
        //{
        //    return new UMS_DAL().GetNotificationCount(EmpID);
        //}

        //public DataTable GetNotificationsData(int EmpID,out int TotalCount)
        //{
        //    DataTable dt = new DataTable();
        //    try
        //    {
        //        objUMS_DAL = new UMS_DAL();
        //        return objUMS_DAL.GetNotificationsDataIntoDb(EmpID, out TotalCount);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //public void SubmitNotifications(int Notification_ID, int Notify_Status, int[] empIDs)
        //{
        //    try
        //    {
        //        objUMS_DAL = new UMS_DAL();
        //        DataTable dtEmpID = new DataTable();
        //        dtEmpID.Columns.Add("EmpID");
        //        DataRow dr;
        //        foreach (int EmpID in empIDs)
        //        {
        //            dr = dtEmpID.NewRow();
        //            dr["EmpID"] = EmpID;
        //            dtEmpID.Rows.Add(dr);
        //        }
        //        objUMS_DAL.SubmitNotificationsToDb(Notification_ID, Notify_Status, dtEmpID);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}
    }

}
