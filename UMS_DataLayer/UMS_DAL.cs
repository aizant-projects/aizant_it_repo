﻿//using AizantIT_PharmaApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//using System.Web.Script.Serialization;
using UMS_BO;

namespace UMS_DataLayer
{
    public class UMS_DAL
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
        // SqlConnection con = new SqlConnection("Data Source = .; Integrated Security = True; Initial Catalog = AizantIT");

        #region Notifications
        public DataTable getNotificationsDataDb(int empID)
        {
            try
            {
                //SqlCommand cmd = new SqlCommand("dbo.AizantIT_SP_GetNotificationsToUser", con);
                SqlCommand cmd = new SqlCommand("dbo.AizantIT_SP_Notification_Get", con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmEmp_ID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(spmEmp_ID).Value = empID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                cmd.Dispose();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SubmitNotificationsDb(int notification_ID, int notify_status, DataTable EmpID)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("[dbo].[AizantIT_SP_Notify_Status_Update]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmnotification_ID = new SqlParameter("@NotificationID", SqlDbType.Int);
                SqlParameter spmnotify_status = new SqlParameter("@NotificationStatus", SqlDbType.Int);
                SqlParameter spmEmpID = new SqlParameter("@NotifyToEmpIDs", SqlDbType.Structured);
                cmd.Parameters.Add(spmnotification_ID).Value = notification_ID;
                cmd.Parameters.Add(spmnotify_status).Value = notify_status;
                cmd.Parameters.Add(spmEmpID).Value = EmpID;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion
        //Electronic sign verification
        public void ElectronicSignVerificationDb(string esign, int EmpID, out int SignVerified)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UMS.[AizantIT_SP_ElectronicSign_Verify]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPPassword = new SqlParameter("@Password", SqlDbType.VarChar);
                cmd.Parameters.Add(SPPassword).Value = esign;
                SqlParameter Verify = cmd.Parameters.Add("@SignVerified", SqlDbType.Int);
                Verify.Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                SignVerified = Convert.ToInt32(cmd.Parameters["@SignVerified"].Value);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        #region AppSetUp
        public DataTable SecurityProfileDal1(int TenureofPassword, int LockOutAttempts, int ExpiryAlertBefore, int SessionTimeout, string operation)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlDataAdapter sda = new SqlDataAdapter("UMS.[AizantIT_SP_SecurityProfile]", con);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;


                SqlParameter SPEmpID1 = new SqlParameter("@TenureofPassword", SqlDbType.Int);
                sda.SelectCommand.Parameters.Add(SPEmpID1).Value = TenureofPassword;

                SqlParameter SPPassword1 = new SqlParameter("@LockOutAttempts", SqlDbType.Int);

                sda.SelectCommand.Parameters.Add(SPPassword1).Value = LockOutAttempts;

                SqlParameter Exipirydate1 = new SqlParameter("@ExpiryAlertBefore", SqlDbType.Int);

                sda.SelectCommand.Parameters.Add(Exipirydate1).Value = ExpiryAlertBefore;

                SqlParameter SessionTime1 = new SqlParameter("@SessionTime", SqlDbType.Int);

                sda.SelectCommand.Parameters.Add(SessionTime1).Value = SessionTimeout;

                SqlParameter OPera1 = new SqlParameter("@operation", SqlDbType.VarChar);

                sda.SelectCommand.Parameters.Add(OPera1).Value = operation;
                sda.SelectCommand.Parameters.AddWithValue("@Comments", "");
                sda.SelectCommand.Parameters.AddWithValue("@Created_ModifiedBy", 0);
                sda.SelectCommand.Parameters.AddWithValue("@RoleId", 0);
                con.Open();
                if (operation == "Select")
                {

                    sda.Fill(dt);
                }


                return dt;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

        }
        //public DataTable GetEmpCode()
        //{
        //    try
        //    {
        //        SqlDataAdapter da = new SqlDataAdapter("select EmpCode from [dbo].[AizantIT_AutoIncrementNumber]", con);
        //        DataTable dt = new DataTable();
        //        da.Fill(dt);
        //        return dt;
        //    }
        //    catch(Exception ex)
        //    {
        //        throw;
        //    }
        //}
        #region SecurityProfile
        public DataTable SecurityProfileDal(int TenureofPassword, int LockOutAttempts, int ExpiryAlertBefore, int SessionTimeout, string operation, int EmpId, string Comments, int RoleID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("UMS.[AizantIT_SP_SecurityProfile]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@TenureofPassword", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = TenureofPassword;
                SqlParameter SPPassword = new SqlParameter("@LockOutAttempts", SqlDbType.Int);
                cmd.Parameters.Add(SPPassword).Value = LockOutAttempts;
                SqlParameter Exipirydate = new SqlParameter("@ExpiryAlertBefore", SqlDbType.Int);
                cmd.Parameters.Add(Exipirydate).Value = ExpiryAlertBefore;
                SqlParameter SessionTime = new SqlParameter("@SessionTime", SqlDbType.Int);
                cmd.Parameters.Add(SessionTime).Value = SessionTimeout;
                SqlParameter OPera = new SqlParameter("@operation", SqlDbType.VarChar);
                cmd.Parameters.Add(OPera).Value = operation;
                cmd.Parameters.AddWithValue("@Comments", Comments);
                cmd.Parameters.AddWithValue("@Created_ModifiedBy", EmpId);
                cmd.Parameters.AddWithValue("@RoleId", RoleID);
                con.Open();

                cmd.ExecuteNonQuery();
                return dt;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }
        public DataTable GetSecurityProfileHistory()
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("UMS.AizantIT_SP_GetSecurityProfileHistory", con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion SecurityProfile
        public DataTable GetEmpCode()
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("UMS.AizantIT_SP_RetrieveEmpCode", con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public DataTable GetSessionTime()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_SessionTimeOut", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet AutoNumber(out string Status)
        {
            try
            {

                con.Open();
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_EmpCodeAutoIncrement]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter Verify = cmd.Parameters.Add("@Status", SqlDbType.VarChar, 20);
                Verify.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                Status = cmd.Parameters["@Status"].Value.ToString();
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetEmpCode(out int Status)
        {
            try
            {

                con.Open();
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_RetrieveEmpCode]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter Verify = cmd.Parameters.Add("@Status", SqlDbType.Int);
                Verify.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                Status = Convert.ToInt32(cmd.Parameters["@Status"].Value.ToString());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int AddEmpCode(UserObjects Uobdal)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_AddEmpCode]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpCode = new SqlParameter("@EmpCode", SqlDbType.VarChar);
                SqlParameter SPEmpCharacter = new SqlParameter("@Empcharacter", SqlDbType.VarChar);
                cmd.Parameters.Add(SPEmpCharacter).Value = Uobdal.EmpCharacter;
                cmd.Parameters.Add(SPEmpCode).Value = Uobdal.EmpCode;

                int i = cmd.ExecuteNonQuery();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #endregion
        #region UMSDashboard
        //UMSdashboard Total count
        public int GetTotalUsers(string Operation)
        {
            try
            {
                int TotalCount = 0;
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetTotalUsers", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Oparation", Operation);
                TotalCount = (int)cmd.ExecuteScalar();
                return TotalCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable BindDropDownRoles(int ModuelID)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_BindDLRolesChart", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPModuelID = new SqlParameter("@ModuelID", SqlDbType.Int);
                cmd.Parameters.Add(SPModuelID).Value = ModuelID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        //Dashboard Chart
        public DataTable BindDepartmetChart()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_DepartmentChart", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable ModulesWithRolesChart(int ModuelID, int RoleID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_BindChartModuleWithRoles", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPModuleID = new SqlParameter("@ModuleID", SqlDbType.Int);
                cmd.Parameters.Add(SPModuleID).Value = ModuelID;
                SqlParameter SPRoleID = new SqlParameter("@RoleID", SqlDbType.Int);
                cmd.Parameters.Add(SPRoleID).Value = RoleID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region User Logged in Check        
        public void RemoveActiveUserSessionDB(string loginID, string password)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_RemoveActiveUserSession]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LoginID", loginID);
                cmd.Parameters.AddWithValue("@Password", password);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public void UserSessionUpdateDB(string userSessionID, string loginID, string UserHostAddress, string UserClientAddress, out int logPKID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_UpdateLoggedInSession]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LoginID", loginID);
                cmd.Parameters.AddWithValue("@UserSessionID", userSessionID);
                cmd.Parameters.AddWithValue("@UserHostAddress", UserHostAddress);
                cmd.Parameters.AddWithValue("@UserClientAddress", UserClientAddress);
                SqlParameter OutputPKID = cmd.Parameters.Add("@logPKID", SqlDbType.Int);
                OutputPKID.Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                logPKID = Convert.ToInt32(cmd.Parameters["@logPKID"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public void UserLogoutUpdate(string logPkid)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_UpdateLogoutHistory]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LogPkid", logPkid);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public DataTable GetUserLoginHistory(int EMPID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_GetLoginHistory]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpId", EMPID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        public DataTable RolesChartDAL(int DeptID, int ModuleID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_BindRolesChart]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ModuelID", ModuleID);
                cmd.Parameters.AddWithValue("@DeptID", DeptID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        #endregion
        #region Employee  
        public int LoginIDCheckDAL(string LoginID)
        {
            try
            {
                int count;
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_LoginIdCheck", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter SPLoginIDCheck = new SqlParameter("@LoginID", SqlDbType.VarChar);
                cmd.Parameters.Add(SPLoginIDCheck).Value = LoginID;

                SqlParameter LoginIDStatus = cmd.Parameters.Add("@Status", SqlDbType.Int);
                LoginIDStatus.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery(); // MISSING

                string Result = cmd.Parameters["@Status"].Value.ToString();
                count = Convert.ToInt32(Result);

                cmd.Dispose();
                return count;
            }
            catch (Exception ex)
            {
                // string msg = ex.Message;
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        //Not in Use
        //public void ResetWrongAttempts()
        //{
        //    try
        //    {
        //        SqlCommand cmd = new SqlCommand("dbo.AizantIT_SP_ExecuteDailyJobSps", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //        DataTable dt = new DataTable();
        //        da.Fill(dt);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public int EmployeeDocCreationDal(int EmpID, int CreatedBy, DataTable dataTable, ref SqlConnection Sqlcon, ref SqlTransaction _Transaction)
        {
            try
            {

                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_EmployeeDocumentCreation", Sqlcon);
                cmd.Transaction = _Transaction;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPCreatedBy).Value = CreatedBy;

                SqlParameter SPDocTypeID = new SqlParameter("@myTable", SqlDbType.Structured);
                cmd.Parameters.Add(SPDocTypeID).Value = dataTable;
                int count = cmd.ExecuteNonQuery();
                return 1;
            }
            catch (Exception ex)
            {
                // string msg = ex.Message;
                throw ex;
            }
            finally
            {

            }
        }

        public int EmployeeDoc(string EmpCode, int DocTypeID, Byte[] bytes, string FileType, string FileName, int CreateBy, out int VerifyDoc1)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_EmployeeDocmentInsert", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter SPEmpCode = new SqlParameter("@EmpCode", SqlDbType.VarChar);
                cmd.Parameters.Add(SPEmpCode).Value = EmpCode;
                SqlParameter SPDocTypeID = new SqlParameter("@DocTypeID", SqlDbType.Int);
                cmd.Parameters.Add(SPDocTypeID).Value = DocTypeID;
                SqlParameter SPbytes = new SqlParameter("@Data", SqlDbType.VarBinary);
                cmd.Parameters.Add(SPbytes).Value = bytes;
                SqlParameter filetype1 = new SqlParameter("@FileType", SqlDbType.VarChar);
                cmd.Parameters.Add(filetype1).Value = FileType;
                SqlParameter FileName1 = new SqlParameter("@FileName", SqlDbType.VarChar);
                cmd.Parameters.Add(FileName1).Value = FileName;
                SqlParameter VerifyDoc = cmd.Parameters.Add("@VerifyDoc", SqlDbType.Int);

                SqlParameter SPCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPCreatedBy).Value = CreateBy;

                VerifyDoc.Direction = ParameterDirection.Output;


                int count = cmd.ExecuteNonQuery();
                int VerifyDoc11 = Convert.ToInt32(cmd.Parameters["@VerifyDoc"].Value);
                VerifyDoc1 = Convert.ToInt32(VerifyDoc11);
                cmd.Dispose();
                return VerifyDoc1;
            }
            catch (Exception ex)
            {
                // string msg = ex.Message;
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }



        public int LoginIDAndEmpCodeCheckDAL(string LoginID, string EmpCode, out int VerifyLoginID, out int VerifyEmpCode)
        {
            try
            {
                int count;
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_LoginIdAndEmpCodeCheck", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter SPLoginIDCheck = new SqlParameter("@LoginID", SqlDbType.VarChar);
                cmd.Parameters.Add(SPLoginIDCheck).Value = LoginID;
                SqlParameter SPEmpCodeCheck = new SqlParameter("@EmpCode", SqlDbType.VarChar);
                cmd.Parameters.Add(SPEmpCodeCheck).Value = EmpCode;

                SqlParameter LoginIDStatus = cmd.Parameters.Add("@VerifyLoginID", SqlDbType.Int);
                SqlParameter EmpCodeStatus = cmd.Parameters.Add("@VerifyEmpCode", SqlDbType.Int);

                //SqlParameter output = cmd.Parameters.Add("@Result", SqlDbType.Int);
                // cmd.Parameters.Add(Status).Value = 0;
                LoginIDStatus.Direction = ParameterDirection.Output;
                EmpCodeStatus.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();
                // string retunvalue = (string)cmd.Parameters["@Status"].Value;
                int verifylodinid = Convert.ToInt32(cmd.Parameters["@VerifyLoginID"].Value);
                VerifyLoginID = Convert.ToInt32(verifylodinid);
                int verifyEmpcode = Convert.ToInt32(cmd.Parameters["@VerifyEmpCode"].Value);
                VerifyEmpCode = Convert.ToInt32(verifyEmpcode);

                string Result = cmd.Parameters["@VerifyLoginID"].Value.ToString();
                count = Convert.ToInt32(Result);

                cmd.Dispose();
                return count;
            }
            catch (Exception ex)
            {
                // string msg = ex.Message;
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public int AddUserDetailsDAL(UserObjects Uobdal, int CreatedByEmpID, ref SqlConnection Sqlcon, ref SqlTransaction _Transaction)
        {
            try
            {
                Sqlcon = con;
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                _Transaction = Sqlcon.BeginTransaction("EmpTransaction");
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_CreateUser", Sqlcon);
                cmd.Transaction = _Transaction;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpCode = new SqlParameter("@EmpCode", SqlDbType.VarChar);
                SqlParameter SPFirstName = new SqlParameter("@FirstName", SqlDbType.VarChar);
                SqlParameter SPLastName = new SqlParameter("@LastName", SqlDbType.VarChar);
                SqlParameter SPDOB = new SqlParameter("@DOB", SqlDbType.Date);
                SqlParameter SPBloodGroup = new SqlParameter("@BloodGroup", SqlDbType.VarChar);
                SqlParameter SPDesignationID = new SqlParameter("@DesignationID", SqlDbType.Int);
                SqlParameter SPEmailID = new SqlParameter("@EmailID", SqlDbType.VarChar);
                SqlParameter SPMobile = new SqlParameter("@Mobile", SqlDbType.VarChar);
                SqlParameter SPEmergencyContactNo = new SqlParameter("@EmergencyContactNO", SqlDbType.VarChar);
                SqlParameter SPTelNo = new SqlParameter("@TelNo", SqlDbType.VarChar);
                SqlParameter SPCountryID = new SqlParameter("@CountryID", SqlDbType.Int);
                SqlParameter SPStateID = new SqlParameter("@StateID", SqlDbType.Int);
                SqlParameter SPCityID = new SqlParameter("@CityID", SqlDbType.Int);
                SqlParameter SPAddress = new SqlParameter("@Address", SqlDbType.VarChar);
                SqlParameter SPGender = new SqlParameter("@Gender", SqlDbType.Char);
                SqlParameter SPEmpStatus = new SqlParameter("@EmpStatus", SqlDbType.Char);
                SqlParameter SPStartDate = new SqlParameter("@StartDate", SqlDbType.Date);
                SqlParameter SPPhoto = new SqlParameter("@Photo", SqlDbType.Image);
                SqlParameter SPLoginID = new SqlParameter("@LoginID", SqlDbType.VarChar);
                SqlParameter SPDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                SqlParameter SPCreated_ModifiedBy = new SqlParameter("@Created_ModifiedBy", SqlDbType.Int);


                //  SqlParameter SPCurrentLocation = new SqlParameter("@CurrentLocation", SqlDbType.Int);
                //   SqlParameter SPLocked = new SqlParameter("@Locked", SqlDbType.Char);

                // SqlParameter SPEndDate = new SqlParameter("@EndDate", SqlDbType.Date);
                cmd.Parameters.Add(SPCreated_ModifiedBy).Value = CreatedByEmpID;
                cmd.Parameters.Add(SPEmpCode).Value = Uobdal.EmpCode;
                cmd.Parameters.Add(SPFirstName).Value = Uobdal.EmpFirstName;
                cmd.Parameters.Add(SPLastName).Value = Uobdal.EmpLastName;
                cmd.Parameters.Add(SPDOB).Value = Uobdal.EmpDOB;
                if (Uobdal.EmpBloodGroup == "")
                {
                    cmd.Parameters.Add(SPBloodGroup).Value = DBNull.Value;
                }
                else
                    cmd.Parameters.Add(SPBloodGroup).Value = Uobdal.EmpBloodGroup;
                cmd.Parameters.Add(SPDesignationID).Value = Uobdal.EmpDesignation;
                cmd.Parameters.Add(SPEmailID).Value = Uobdal.EmpEmailID;
                cmd.Parameters.Add(SPMobile).Value = Uobdal.EmpMobileNo;
                cmd.Parameters.Add(SPEmergencyContactNo).Value = Uobdal.EmpEmgNo;
                cmd.Parameters.Add(SPTelNo).Value = Uobdal.EmpTelNo;
                cmd.Parameters.Add(SPCountryID).Value = Uobdal.EmpCountry;
                cmd.Parameters.Add(SPStateID).Value = Uobdal.EmpState;
                cmd.Parameters.Add(SPCityID).Value = Uobdal.EmpCity;
                cmd.Parameters.Add(SPAddress).Value = Uobdal.EmpAddress;
                cmd.Parameters.Add(SPGender).Value = Uobdal.EmpGender;
                cmd.Parameters.Add(SPEmpStatus).Value = Uobdal.EmpActiveStatus;
                if (Uobdal.EmpStartDate == "")
                    cmd.Parameters.Add(SPStartDate).Value = DBNull.Value;
                else
                    cmd.Parameters.Add(SPStartDate).Value = Uobdal.EmpStartDate;
                byte[] photo = Uobdal.EmpPhoto;
                if (photo == null)
                {
                    cmd.Parameters.Add(SPPhoto).Value = DBNull.Value;
                }
                else
                    cmd.Parameters.Add(SPPhoto).Value = Uobdal.EmpPhoto;

                cmd.Parameters.Add(SPLoginID).Value = Uobdal.EmpLoginID;
                cmd.Parameters.Add(SPDeptID).Value = Uobdal.EmpDepartment;
                cmd.Parameters.AddWithValue("@RoleID", Uobdal.RoleID);


                SqlParameter OutputEmpId = cmd.Parameters.Add("@OutputEmpId", SqlDbType.Int);
                OutputEmpId.Direction = ParameterDirection.Output;

                //cmd.Parameters.Add(SPCurrentLocation).Value = Uobdal.EmpCurrentLocation;
                //    cmd.Parameters.Add(SPLocked).Value = Uobdal.EmpLockStatus;

                // cmd.Parameters.Add(SPEndDate).Value = Uobdal.EmpEndDate;

                cmd.ExecuteNonQuery();

                int Result = Convert.ToInt32(cmd.Parameters["@OutputEmpId"].Value.ToString());

                //if(count>0)
                //{
                //    Response.write("<script>alert('Inserted')</script>");
                //}

                return Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataTable GetEmployeeDocments(string EmpCOde, int CreatedBy, string actionType)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_EmployeeDocVieworDelete]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter DocID1 = new SqlParameter("@DocID", SqlDbType.VarChar);
                cmd.Parameters.Add(DocID1).Value = EmpCOde;
                SqlParameter SPCreateBy = new SqlParameter("@CreatedBy", SqlDbType.VarChar);
                cmd.Parameters.Add(SPCreateBy).Value = CreatedBy;
                SqlParameter actionType1 = new SqlParameter("@ActionType", SqlDbType.VarChar);
                cmd.Parameters.Add(actionType1).Value = actionType;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetUserDetails(int EmpID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_GetUserDetails]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter Empid = new SqlParameter("@EmpID", SqlDbType.VarChar);
                cmd.Parameters.Add(Empid).Value = EmpID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int EditUserDetailsDAL(UserObjects Uobdal, int Empid)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_UpdateUser", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                SqlParameter SPFirstName = new SqlParameter("@FirstName", SqlDbType.VarChar);
                SqlParameter SPLastName = new SqlParameter("@LastName", SqlDbType.VarChar);
                SqlParameter SPDOB = new SqlParameter("@DOB", SqlDbType.Date);
                SqlParameter SPBloodGroup = new SqlParameter("@BloodGroup", SqlDbType.VarChar);
                SqlParameter SPDesignationID = new SqlParameter("@DesignationID", SqlDbType.Int);
                SqlParameter SPEmailID = new SqlParameter("@EmailID", SqlDbType.VarChar);
                SqlParameter SPMobile = new SqlParameter("@Mobile", SqlDbType.VarChar);
                SqlParameter SPEmergencyContactNo = new SqlParameter("@EmergencyContactNO", SqlDbType.VarChar);
                SqlParameter SPTelNo = new SqlParameter("@TelNo", SqlDbType.VarChar);
                SqlParameter SPCountryID = new SqlParameter("@CountryID", SqlDbType.Int);
                SqlParameter SPStateID = new SqlParameter("@StateID", SqlDbType.Int);
                SqlParameter SPCityID = new SqlParameter("@CityID", SqlDbType.Int);
                SqlParameter SPAddress = new SqlParameter("@Address", SqlDbType.VarChar);
                SqlParameter SPGender = new SqlParameter("@Gender", SqlDbType.Char);
                SqlParameter SPEmpStatus = new SqlParameter("@EmpStatus", SqlDbType.Char);
                SqlParameter SPStartDate = new SqlParameter("@StartDate", SqlDbType.Date);
                SqlParameter SPPhoto = new SqlParameter("@Photo", SqlDbType.Image);
                SqlParameter SPDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                //  SqlParameter SPCurrentLocation = new SqlParameter("@CurrentLocation", SqlDbType.Int);
                SqlParameter SPLocked = new SqlParameter("@Locked", SqlDbType.Char);
                SqlParameter SPEndDate = new SqlParameter("@EndDate", SqlDbType.Date);
                SqlParameter SPCreated_ModifiedBy = new SqlParameter("@Created_ModifiedBy", SqlDbType.Int);

                cmd.Parameters.Add(SPCreated_ModifiedBy).Value = Empid;
                cmd.Parameters.Add(SPEmpID).Value = Uobdal.EmpID;
                cmd.Parameters.Add(SPFirstName).Value = Uobdal.EmpFirstName;
                cmd.Parameters.Add(SPLastName).Value = Uobdal.EmpLastName;
                cmd.Parameters.Add(SPDOB).Value = Uobdal.EmpDOB;
                if (Uobdal.EmpBloodGroup == "")
                {
                    cmd.Parameters.Add(SPBloodGroup).Value = DBNull.Value;
                }
                else
                    cmd.Parameters.Add(SPBloodGroup).Value = Uobdal.EmpBloodGroup;
                cmd.Parameters.Add(SPDesignationID).Value = Uobdal.EmpDesignation;
                cmd.Parameters.Add(SPEmailID).Value = Uobdal.EmpEmailID;
                cmd.Parameters.Add(SPMobile).Value = Uobdal.EmpMobileNo;
                cmd.Parameters.Add(SPEmergencyContactNo).Value = Uobdal.EmpEmgNo;
                cmd.Parameters.Add(SPTelNo).Value = Uobdal.EmpTelNo;
                cmd.Parameters.Add(SPCountryID).Value = Uobdal.EmpCountry;
                cmd.Parameters.Add(SPStateID).Value = Uobdal.EmpState;
                cmd.Parameters.Add(SPCityID).Value = Uobdal.EmpCity;
                cmd.Parameters.Add(SPAddress).Value = Uobdal.EmpAddress;
                cmd.Parameters.Add(SPGender).Value = Uobdal.EmpGender;
                cmd.Parameters.Add(SPEmpStatus).Value = Uobdal.EmpActiveStatus;
                if (Uobdal.EmpStartDate == "")
                    cmd.Parameters.Add(SPStartDate).Value = DBNull.Value;
                else
                    cmd.Parameters.Add(SPStartDate).Value = Uobdal.EmpStartDate;
                if (Uobdal.EmpEndDate == "")
                    cmd.Parameters.Add(SPEndDate).Value = DBNull.Value;
                else
                    cmd.Parameters.Add(SPEndDate).Value = Uobdal.EmpEndDate;
                byte[] photo = Uobdal.EmpPhoto;
                if (photo == null)
                {
                    cmd.Parameters.Add(SPPhoto).Value = DBNull.Value;
                }
                else
                    cmd.Parameters.Add(SPPhoto).Value = Uobdal.EmpPhoto;
                cmd.Parameters.Add(SPDeptID).Value = Uobdal.EmpDepartment;

                //cmd.Parameters.Add(SPCurrentLocation).Value = Uobdal.EmpCurrentLocation;
                cmd.Parameters.Add(SPLocked).Value = Uobdal.EmpLockStatus;
                cmd.Parameters.AddWithValue("@RoleID", Uobdal.RoleID);
                int Count = cmd.ExecuteNonQuery();
                //if(count>0)
                //{
                //    Response.write("<script>alert('Inserted')</script>");
                //}
                cmd.Dispose();

                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region EmployeeList
        public int GetUMSTotalCount(string Operation)
        {
            try
            {
                int totalDesignationCount = 0;

                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetDetailsDatatable", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Operation", Operation);
                totalDesignationCount = (int)cmd.ExecuteScalar();
                return totalDesignationCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable GetUMSLIST(int iDisplayLength, int iDisplayStart, int iSortCol_0,
        string sSortDir_0, string sSearch, string sSearch_Code, string sSearch_Name, string OPeration, int ShowType, string DeptCode, int RoleID)
        {
            string query = "";
            //if (OPeration == "Employee")
            //{
            //    query = "UMS.AizantIT_SP_GetEmployeeList";
            //}
            //else 
            if (OPeration == "Department")
            {
                query = "UMS.AizantIT_SP_GetDepartmentList";
            }
            else if (OPeration == "Designation")
            {
                query = "[UMS].[AizantIT_SP_GetDesignationList]";
            }
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
            SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
            SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
            cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
            SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
            SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSearchString).Value = sSearch;

            switch(OPeration)
            {
                case "Department":
                    SqlParameter paramsSearch_DeptCode = new SqlParameter("@sSearch_DeptCode", SqlDbType.VarChar);
                    cmd.Parameters.Add(paramsSearch_DeptCode).Value = sSearch_Code;
                    SqlParameter paramsSearch_DepartmentName = new SqlParameter("@sSearch_DepartmentName", SqlDbType.VarChar);
                    cmd.Parameters.Add(paramsSearch_DepartmentName).Value = sSearch_Name;
                    break;
                case "Designation":
                    SqlParameter paramsSearch_DesignationName = new SqlParameter("@sSearch_DesignationName", SqlDbType.VarChar);
                    cmd.Parameters.Add(paramsSearch_DesignationName).Value = sSearch_Name;
                    break;
            }

            SqlParameter paramShowType = new SqlParameter("@ShowType", SqlDbType.Int);
            cmd.Parameters.Add(paramShowType).Value = ShowType;
            if (ShowType == 4 || ShowType == 5)
            {
                SqlParameter paramDeptCode = new SqlParameter("@DeptCode", SqlDbType.VarChar);
                cmd.Parameters.Add(paramDeptCode).Value = DeptCode;
            }
            if (ShowType == 5)
            {
                SqlParameter paramRoles = new SqlParameter("@RoleID", SqlDbType.Int);
                cmd.Parameters.Add(paramRoles).Value = RoleID;
            }
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }


        public DataTable GVEmployeeList(string SearchTerm, int PageSize, int PageIndex, out int RecordCount)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetEmployeeList1", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter SearchTerms = new SqlParameter("@SearchTerm", SqlDbType.VarChar);
                cmd.Parameters.Add(SearchTerms).Value = SearchTerm;
                SqlParameter Pagesize = new SqlParameter("@PageSize", SqlDbType.Int);
                cmd.Parameters.Add(Pagesize).Value = PageSize;
                SqlParameter Pageindex = new SqlParameter("@PageIndex", SqlDbType.Int);
                cmd.Parameters.Add(Pageindex).Value = PageIndex;
                //cmd.Parameters.Add("@RecordCount", SqlDbType.Int, 4);
                //cmd.Parameters("@RecordCount").Direction = ParameterDirection.Output;

                SqlParameter Status = cmd.Parameters.Add("@RecordCount", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dtdal = new DataTable();
                da.Fill(dtdal);
                string Result = cmd.Parameters["@RecordCount"].Value.ToString();
                RecordCount = Convert.ToInt32(Result);

                return dtdal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public DataTable GetEmployeeAuditHistory(int EmpID, int ModuleID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_GetEmployeeAuditHistory]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter Empid = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(Empid).Value = EmpID;
                SqlParameter ModuleId = new SqlParameter("@ModuleID", SqlDbType.Int);
                cmd.Parameters.Add(ModuleId).Value = ModuleID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        //EmployeeList
        public DataTable GetUMSEmployeeLIST(int iDisplayLength, int iDisplayStart, int iSortCol_0,
       string sSortDir_0, string sSearch, string sSearch_EmpCode, string sSearch_Name, string sSearch_DepartmentName, string sSearch_DesignationName,
        string sSearch_StartDate, string sSearch_Status, string OPeration, int ShowType, string DeptCode, int RoleID)
        {
            string query = "UMS.AizantIT_SP_GetEmployeeList";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
            SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
            SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
            cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
            SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
            SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSearchString).Value = sSearch;

          
            SqlParameter paramsSearch_EmpCode = new SqlParameter("@sSearch_EmpCode", SqlDbType.VarChar);
            cmd.Parameters.Add(paramsSearch_EmpCode).Value = sSearch_EmpCode;
            SqlParameter paramsSearch_Name = new SqlParameter("@sSearch_Name", SqlDbType.VarChar);
            cmd.Parameters.Add(paramsSearch_Name).Value = sSearch_Name;
            SqlParameter paramsSearch_DepartmentName = new SqlParameter("@sSearch_DepartmentName", SqlDbType.VarChar);
            cmd.Parameters.Add(paramsSearch_DepartmentName).Value = sSearch_DepartmentName;
            SqlParameter paramsSearch_DesignationName = new SqlParameter("@sSearch_DesignationName", SqlDbType.VarChar);
            cmd.Parameters.Add(paramsSearch_DesignationName).Value = sSearch_DesignationName;
            SqlParameter paramsSearch_StartDate = new SqlParameter("@sSearch_StartDate", SqlDbType.VarChar);
            cmd.Parameters.Add(paramsSearch_StartDate).Value = sSearch_StartDate;
            SqlParameter paramsSearch_Status = new SqlParameter("@sSearch_Status", SqlDbType.VarChar);
            cmd.Parameters.Add(paramsSearch_Status).Value = sSearch_Status;
        
            SqlParameter paramShowType = new SqlParameter("@ShowType", SqlDbType.Int);
            cmd.Parameters.Add(paramShowType).Value = ShowType;
            if (ShowType == 4 || ShowType == 5)
            {
                SqlParameter paramDeptCode = new SqlParameter("@DeptCode", SqlDbType.VarChar);
                cmd.Parameters.Add(paramDeptCode).Value = DeptCode;
            }
            if (ShowType == 5)
            {
                SqlParameter paramRoles = new SqlParameter("@RoleID", SqlDbType.Int);
                cmd.Parameters.Add(paramRoles).Value = RoleID;
            }

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }


            #endregion

            #region EmployeeLoginListHistory
            public DataTable GetUMS_DAL_LoginHistoryLIST(int iDisplayLength, int iDisplayStart, int iSortCol_0,
           string sSortDir_0, string sSearch, string sSearch_EmpCode, string sSearch_EmpNameName, string sSearch_LoginID, string sSearch_Login_Time,
           string sSearch_Logout_Time, string sSearch_UserHostAddress, string sSearch_UserClientAddress)
        {
            string query = "[UMS].[AizantIT_SP_GetLoginHistoryListAll]";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;

            SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;

            SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
            cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;

            SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;

            SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSearchString).Value = sSearch;

            SqlParameter param_sSearch_EmpCode = new SqlParameter("@sSearch_EmployeeCode", SqlDbType.VarChar);
            cmd.Parameters.Add(param_sSearch_EmpCode).Value = sSearch_EmpCode;

            SqlParameter param_sSearch_EmpName = new SqlParameter("@sSearch_EmpNameName", SqlDbType.VarChar);
            cmd.Parameters.Add(param_sSearch_EmpName).Value = sSearch_EmpNameName;

            SqlParameter param_sSearch_LoginID = new SqlParameter("@sSearch_LoginID", SqlDbType.VarChar);
            cmd.Parameters.Add(param_sSearch_LoginID).Value = sSearch_LoginID;

            SqlParameter param_sSearch_Login_Time = new SqlParameter("@sSearch_LoginTime", SqlDbType.VarChar);
            cmd.Parameters.Add(param_sSearch_Login_Time).Value = sSearch_Login_Time;

            SqlParameter param_sSearch_Logout_Time = new SqlParameter("@sSearch_LogoutTime", SqlDbType.VarChar);
            cmd.Parameters.Add(param_sSearch_Logout_Time).Value = sSearch_Logout_Time;

            SqlParameter param_sSearch_UserHostAddress = new SqlParameter("@sSearch_UserHostAddress", SqlDbType.VarChar);
            cmd.Parameters.Add(param_sSearch_UserHostAddress).Value = sSearch_UserHostAddress;

            SqlParameter param_sSearch_UserClientAddress = new SqlParameter("@sSearch_UserClientAddress", SqlDbType.VarChar);
            cmd.Parameters.Add(param_sSearch_UserClientAddress).Value = sSearch_UserClientAddress;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        #endregion

        #region DesignationList
        public DataTable GetDesignationList(int PageSize, int PageIndex, out int RecordCount)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetDesignationList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter Pagesize = new SqlParameter("@PageSize", SqlDbType.Int);
                cmd.Parameters.Add(Pagesize).Value = PageSize;
                SqlParameter Pageindex = new SqlParameter("@PageIndex", SqlDbType.Int);
                cmd.Parameters.Add(Pageindex).Value = PageIndex;
                //cmd.Parameters.Add("@RecordCount", SqlDbType.Int, 4);
                //cmd.Parameters("@RecordCount").Direction = ParameterDirection.Output;

                SqlParameter Status = cmd.Parameters.Add("@RecordCount", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dtdal = new DataTable();
                da.Fill(dtdal);
                string Result = cmd.Parameters["@RecordCount"].Value.ToString();
                RecordCount = Convert.ToInt32(Result);

                return dtdal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable GetDesigantionDetails(int desgID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].AizantIT_SP_GetDesignationsForEdit", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter Empid = new SqlParameter("@DesgID", SqlDbType.Int);
                cmd.Parameters.Add(Empid).Value = desgID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int DesignationtDAL(string Operation, int EmpID, int desgid, string designationName, out int VerifyDesigName, int RoleID)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_Designation", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SpDesgId = new SqlParameter("@DesgID", SqlDbType.Int);
                SqlParameter SPDesgName = new SqlParameter("@DesignationName", SqlDbType.VarChar);
                SqlParameter SpModifyID = new SqlParameter("@Created_ModifiedBy", SqlDbType.Int);
                SqlParameter SPOperation = new SqlParameter("@Operation", SqlDbType.VarChar);
                cmd.Parameters.Add(SPOperation).Value = Operation;
                cmd.Parameters.Add(SpModifyID).Value = EmpID;
                cmd.Parameters.Add(SpDesgId).Value = desgid;
                cmd.Parameters.Add(SPDesgName).Value = designationName;
                cmd.Parameters.AddWithValue("@RoleId", RoleID);
                SqlParameter Verify = cmd.Parameters.Add("@IsDesigExists", SqlDbType.Int);
                Verify.Direction = ParameterDirection.Output;
                int Count = cmd.ExecuteNonQuery();

                int verify = Convert.ToInt32(cmd.Parameters["@IsDesigExists"].Value);
                VerifyDesigName = Convert.ToInt32(verify);
                //if(count>0)
                //{
                //    Response.write("<script>alert('Inserted')</script>");
                //}
                cmd.Dispose();

                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable GetDesignationHistory(int desgID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].AizantIT_SP_GetDesignationHistory", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter Empid = new SqlParameter("@DesignationID", SqlDbType.Int);
                cmd.Parameters.Add(Empid).Value = desgID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region DepartmentList
        public DataTable GetDepartmentList(int PageSize, int PageIndex, out int RecordCount)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetDepartmentList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter Pagesize = new SqlParameter("@PageSize", SqlDbType.Int);
                cmd.Parameters.Add(Pagesize).Value = PageSize;
                SqlParameter Pageindex = new SqlParameter("@PageIndex", SqlDbType.Int);
                cmd.Parameters.Add(Pageindex).Value = PageIndex;
                //cmd.Parameters.Add("@RecordCount", SqlDbType.Int, 4);
                //cmd.Parameters("@RecordCount").Direction = ParameterDirection.Output;

                SqlParameter Status = cmd.Parameters.Add("@RecordCount", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dtdal = new DataTable();
                da.Fill(dtdal);
                string Result = cmd.Parameters["@RecordCount"].Value.ToString();
                RecordCount = Convert.ToInt32(Result);

                return dtdal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable GetDepartmentDetails(int DeptID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].AizantIT_SP_GetDepartmentsForEdit", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter Empid = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(Empid).Value = DeptID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int UpdateEmpLockDal(string EmpCde, string comments, out int status, int CeatedBy, int RoleID)
        {
            try
            {
                con.Open();
                //SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_UpdateDepartment", con);
                SqlCommand cmd = new SqlCommand("[UMS].AizantIT_SP_UpdateEmployeeLock", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SpDeptId = new SqlParameter("@EmpCode", SqlDbType.Int);
                cmd.Parameters.Add(SpDeptId).Value = EmpCde;
                SqlParameter Spcomments = new SqlParameter("@Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(Spcomments).Value = comments;
                SqlParameter Status = cmd.Parameters.Add("@status", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@Created_ModifiedBy", CeatedBy);
                cmd.Parameters.AddWithValue("@RoleID", RoleID);
                int Count = cmd.ExecuteNonQuery();
                string Result = cmd.Parameters["@status"].Value.ToString();
                status = Convert.ToInt32(Result);
                return Count;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public int DepartmentDAL(string Operation, int EmpID, int DeptID, string DeptCode, String DeptName, out int VerifyDept, int RoleID)
        {
            try
            {
                con.Open();
                //SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_UpdateDepartment", con);
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_Department", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SpDeptId = new SqlParameter("@DeptID", SqlDbType.Int);
                SqlParameter SPDeptCode = new SqlParameter("@DeptCode", SqlDbType.VarChar);
                SqlParameter SPDeptName = new SqlParameter("@DepartmentName", SqlDbType.VarChar);
                SqlParameter SPModifiedBy = new SqlParameter("@Created_ModifiedBy", SqlDbType.Int);
                SqlParameter SPOperation = new SqlParameter("@Operation", SqlDbType.VarChar);
                SqlParameter Verify = cmd.Parameters.Add("@IsDeptExists", SqlDbType.Int);
                Verify.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(SPOperation).Value = Operation;
                cmd.Parameters.Add(SPModifiedBy).Value = EmpID;
                cmd.Parameters.Add(SpDeptId).Value = DeptID;
                cmd.Parameters.Add(SPDeptCode).Value = DeptName;
                cmd.Parameters.Add(SPDeptName).Value = DeptCode;
                cmd.Parameters.AddWithValue("@RoleId", RoleID);
                int Count = cmd.ExecuteNonQuery();

                int verify = Convert.ToInt32(cmd.Parameters["@IsDeptExists"].Value);
                VerifyDept = Convert.ToInt32(verify);
                //if(count>0)
                //{
                //    Response.write("<script>alert('Inserted')</script>");
                //}
                cmd.Dispose();

                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable GetDepartmentHistory(int DeptID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_GetDepartmentHistory]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter Deptid = new SqlParameter("@DepartmentID", SqlDbType.Int);
                cmd.Parameters.Add(Deptid).Value = DeptID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region UserLogin
        public DataSet ShowExpireDate(int EmpID, out int RemainingCount, out int Status)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_CheckPasswordExpire]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter Verify = cmd.Parameters.Add("@RemainingCount", SqlDbType.Int);
                Verify.Direction = ParameterDirection.Output;
                SqlParameter Verify1 = cmd.Parameters.Add("@Status", SqlDbType.Int);
                Verify1.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                RemainingCount = Convert.ToInt32(cmd.Parameters["@RemainingCount"].Value);
                Status = Convert.ToInt32(cmd.Parameters["@Status"].Value);
                return ds;
            }
            catch (SqlException sqlEx)
            {
                switch (sqlEx.Number)
                {
                    case -1:
                        throw new Exception("Database Connection Error. Contact Admin");
                    default:
                        throw new Exception(sqlEx.Number + " Database Error Occurred, Contact Admin.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int AddSignUpDAL(SigUPObjects Uobdal, out int VerifyUserID,out int StartDateStatus)
        {
            try
            {
                int count;
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_SignUP", con);
                cmd.CommandType = CommandType.StoredProcedure;
                //SqlParameter SPOperation = new SqlParameter("@Operation", SqlDbType.VarChar);
                //cmd.Parameters.Add(SPOperation).Value = "Insert";
                SqlParameter SPLoginID = new SqlParameter("@LoginID", SqlDbType.VarChar);
                cmd.Parameters.Add(SPLoginID).Value = Uobdal.UserLoginID;
                SqlParameter SPPassword = new SqlParameter("@Password", SqlDbType.VarChar);
                cmd.Parameters.Add(SPPassword).Value = Uobdal.ConfirmPassword;
                SqlParameter SPSecurityQuestion = new SqlParameter("@SecurityQuestion", SqlDbType.VarChar);
                cmd.Parameters.Add(SPSecurityQuestion).Value = Uobdal.SecurityQuestion;
                SqlParameter SPSecurityAnswer = new SqlParameter("@SecurityAnswer", SqlDbType.VarChar);
                cmd.Parameters.Add(SPSecurityAnswer).Value = Uobdal.SecurityAnswer;
                SqlParameter Verify = cmd.Parameters.Add("@SignUpStatus", SqlDbType.Int);
                Verify.Direction = ParameterDirection.Output;
                SqlParameter spStartDateStatus = cmd.Parameters.Add("@StartDateStatus", SqlDbType.Int);
                spStartDateStatus.Direction = ParameterDirection.Output;
                con.Open();
                count = cmd.ExecuteNonQuery();
                //if (cmd.Parameters["@SignUpStatus"].Value != DBNull.Value)
                //{
                int verify = Convert.ToInt32(cmd.Parameters["@SignUpStatus"].Value);
                StartDateStatus = Convert.ToInt32(cmd.Parameters["@StartDateStatus"].Value);
                VerifyUserID = Convert.ToInt32(verify);
                //}
                //else
                //    VerifyUserID = 0;
                cmd.Dispose();
                return count;
            }
            catch (SqlException sqlEx)
            {
                switch (sqlEx.Number)
                {
                    case -1:
                        throw new Exception("Database Connection Error. Contact Admin");
                    default:
                        throw new Exception(sqlEx.Number + "' Database Error Occurred, Contact Admin.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataSet CheckLoginDAL(SigUPObjects Uobdal, out int RemainingCount,
            out int Status, out char LoggedInStatus, out int isAdminResetPWD)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_LoginCheckWithAttemptedPasswords", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPLoginID = new SqlParameter("@LoginID", SqlDbType.VarChar);
                cmd.Parameters.Add(SPLoginID).Value = Uobdal.UserLoginID;
                SqlParameter SPPassword = new SqlParameter("@Password", SqlDbType.VarChar);
                cmd.Parameters.Add(SPPassword).Value = Uobdal.ConfirmPassword;
                SqlParameter Verify = cmd.Parameters.Add("@RemainingCount", SqlDbType.Int);
                Verify.Direction = ParameterDirection.Output;
                SqlParameter Verify1 = cmd.Parameters.Add("@ExpireStatus", SqlDbType.Int);
                Verify1.Direction = ParameterDirection.Output;
                SqlParameter spmLoggedInStatus = cmd.Parameters.Add("@LoggedInStatus", SqlDbType.Char, 1);
                spmLoggedInStatus.Direction = ParameterDirection.Output;
                SqlParameter spmisAdminResetPWD = cmd.Parameters.Add("@isAdminResetPWD", SqlDbType.Int);
                spmisAdminResetPWD.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                RemainingCount = Convert.ToInt32(cmd.Parameters["@RemainingCount"].Value);
                Status = Convert.ToInt32(cmd.Parameters["@ExpireStatus"].Value);
                //SessionIdleTimeout= Convert.ToInt32(cmd.Parameters["@SessionIdleTimeOut"].Value);
                LoggedInStatus = Convert.ToChar(cmd.Parameters["@LoggedInStatus"].Value);
                isAdminResetPWD = Convert.ToInt32(cmd.Parameters["@isAdminResetPWD"].Value);
                return ds;
            }
            catch (SqlException sqlEx)
            {
                switch (sqlEx.Number)
                {
                    case -1:
                        throw new Exception("Database Connection Error. Contact Admin");
                    default:
                        throw new Exception(sqlEx.Number + " Database Error Occurred, Contact Admin.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region LoginsForActivation
        public DataTable GVLoginActivation()
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetEmployeesToUnlock", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dtdal = new DataTable();
                da.Fill(dtdal);
                return dtdal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public int UnLock(int EmpID, int UnlockedBy)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_UnLockEmployee", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPUnlockedBy = new SqlParameter("@UnlockedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPUnlockedBy).Value = UnlockedBy;

                int id = cmd.ExecuteNonQuery();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region BindingDropDowns
        public DataTable BindCountriesDAL()
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetCountries", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dtdal = new DataTable();
                da.Fill(dtdal);
                return dtdal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable BindStatesDAL(int CountryID)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetStates", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPSelectedCOuntryID = new SqlParameter("@CountryID", SqlDbType.Int);
                cmd.Parameters.Add(SPSelectedCOuntryID).Value = CountryID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dtdal = new DataTable();
                da.Fill(dtdal);
                return dtdal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable BindCityDAL(int CountryID)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetCitys", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPSelectedCOuntryID = new SqlParameter("@CountryID", SqlDbType.Int);
                cmd.Parameters.Add(SPSelectedCOuntryID).Value = CountryID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dtdal = new DataTable();
                da.Fill(dtdal);
                return dtdal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable BindDepartmentsDAL()
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetDepartments", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dtdal = new DataTable();
                da.Fill(dtdal);
                return dtdal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable BindDesignationsDAL(int SelectedDepartmentID)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_Designation", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPSelectedDeptID = new SqlParameter("@DeptID", SqlDbType.VarChar);
                cmd.Parameters.Add(SPSelectedDeptID).Value = SelectedDepartmentID;
                SqlParameter SPOperation = new SqlParameter("@Operation", SqlDbType.VarChar);
                cmd.Parameters.Add(SPOperation).Value = "Select";

                SqlParameter SPDesignationName = new SqlParameter("@DesignationName", SqlDbType.VarChar);
                cmd.Parameters.Add(SPDesignationName).Value = DBNull.Value;
                SqlParameter Created_ModifiedBy = new SqlParameter("@Created_ModifiedBy", SqlDbType.VarChar);
                cmd.Parameters.Add(Created_ModifiedBy).Value = DBNull.Value;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dtdal = new DataTable();
                da.Fill(dtdal);
                return dtdal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public DataTable BindAllDesignationsDAL()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetAllDesignations", con);
                cmd.CommandType = CommandType.StoredProcedure;
                //SqlParameter SPSelectedDept = new SqlParameter("@DeptID", SqlDbType.Int);
                //cmd.Parameters.Add(SPSelectedDept).Value = Uobdal.SelectedDesigDepartment;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dtdal = new DataTable();
                da.Fill(dtdal);
                return dtdal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public DataTable GetEmployessDb(int DeptID = 0, int IsActive = 0)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_GetEmployees]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DeptID", DeptID);
                cmd.Parameters.AddWithValue("@IsActive", IsActive);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable getEmployeeDesignationDb(int EmpID)
        {
            try
            {
                //if(con.State!=ConnectionState.Open)
                //{
                //    con.Open();
                //}
                //SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetEmpDesignation", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@EmpID", EmpID);               
                //if (cmd.ExecuteScalar()!=null)
                //{
                //    return cmd.ExecuteScalar().ToString();
                //}
                //else
                //{
                //    return "";
                //}  
                SqlCommand cmd = new SqlCommand("[UMS].AizantIT_SP_GetEmpDesignation", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpID", EmpID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Get Roles
        public DataSet GetModuleRoles(string empID, string moduleID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].AizantIT_SP_GetModuleWiseRolesDepts", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpID", empID);
                cmd.Parameters.AddWithValue("@ModuleID", moduleID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetModuleRolesDb(string moduleID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_GetModuleWiseRoleList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ModuleID", moduleID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetModuleRolesDbOld(string moduleID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_GetModuleRoles]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ModuleID", moduleID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetModules()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_GetModules]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetEmployees()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_GetEmployees]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AssignRolesToEmpDb(DataTable dt, string EmpID, int CreatedBy, string ModuleID, DataTable dtDepts, int RoleId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_ModuleWiseUserRolesAndDepts]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpRoles", dt);
                SqlParameter SPCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPCreatedBy).Value = CreatedBy;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPModuleID = new SqlParameter("@ModuleID", SqlDbType.Int);
                cmd.Parameters.Add(SPModuleID).Value = ModuleID;
                cmd.Parameters.AddWithValue("@DeptIDs", dtDepts);
                cmd.Parameters.AddWithValue("@RoleID", RoleId);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public void DeleteRolesToOdule(string EmpID, int CreatedBy, string ModuleID, int RoleId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_DeleteModuleRoles]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPCreatedBy).Value = CreatedBy;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPModuleID = new SqlParameter("@ModuleID", SqlDbType.Int);
                cmd.Parameters.Add(SPModuleID).Value = ModuleID;
                cmd.Parameters.AddWithValue("@RoleID", RoleId);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region ForgetPassword
        public DataTable ForgotPassword(string LoginID, string EmailID, string Operation, int EmpID, string SecurityAnswer, string NewPassword)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_ForgotPassword", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPLoginID = new SqlParameter("@LoginID", SqlDbType.VarChar);
                SqlParameter SPEmailID = new SqlParameter("@EmailID", SqlDbType.VarChar);
                SqlParameter SPSecurityAnswer = new SqlParameter("@SecurityAnswer", SqlDbType.VarChar);
                SqlParameter SPOperation = new SqlParameter("@Operation", SqlDbType.VarChar);
                SqlParameter SPEmpID = new SqlParameter("@InputEmpID", SqlDbType.Int);
                SqlParameter SPNewPassword = new SqlParameter("@Password", SqlDbType.VarChar);
                cmd.Parameters.Add(SPNewPassword).Value = NewPassword;
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                cmd.Parameters.Add(SPOperation).Value = Operation;

                cmd.Parameters.Add(SPSecurityAnswer).Value = SecurityAnswer;
                cmd.Parameters.Add(SPLoginID).Value = LoginID;
                cmd.Parameters.Add(SPEmailID).Value = EmailID;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion        

        // get Module Wise Departments
        public DataTable getModuleWiseDeptsDb(int EmpID, int ModuleID)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetModuleWiseDepts", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpID", EmpID);
                cmd.Parameters.AddWithValue("@ModuleID", ModuleID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public DataTable getRoleWiseDeptsDb(int EmpID, DataTable RoleIDs)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.[AizantIT_SP_GetRoleWiseDepts]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpID", EmpID);
                cmd.Parameters.AddWithValue("@RoleIDs", RoleIDs);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        #region MyProfile
        public DataSet GetDataToMyProfile(int EmpID)
        {
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_GetDataToMyProfile", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpID", EmpID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                //throw ex;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }
            return ds;
        }
        public int MyprofileSecurityUpdate(int EmpID, string Question, string Answer)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_MyProfile_UpdateSecurity", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                SqlParameter SPQuestion = new SqlParameter("@Question", SqlDbType.VarChar);
                SqlParameter SPAnswer = new SqlParameter("@Answer", SqlDbType.VarChar);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                cmd.Parameters.Add(SPQuestion).Value = Question;
                cmd.Parameters.Add(SPAnswer).Value = Answer;
                int count = cmd.ExecuteNonQuery();

                cmd.Dispose();
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public DataTable MyprofileChangePassword(int EmpID, string Password, string Comments)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UMS.AizantIT_SP_ChangePassword", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@InputEmpID", SqlDbType.Int);
                SqlParameter SPPassword = new SqlParameter("@Password", SqlDbType.VarChar);
                SqlParameter SPComments = new SqlParameter("@Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                cmd.Parameters.Add(SPPassword).Value = Password;
                cmd.Parameters.Add(SPComments).Value = Comments;
                //int count = cmd.ExecuteNonQuery();
                //cmd.Dispose();
                //return count;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public void ResetPassword(int EmpID, string Password, out int Status, UserObjects UOB, int isAdmin)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_ResetPassword]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@InputEmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPPassword = new SqlParameter("@Password", SqlDbType.VarChar);
                cmd.Parameters.Add(SPPassword).Value = Password;
                SqlParameter spmTS_Created = cmd.Parameters.Add("@Status", SqlDbType.Int);
                spmTS_Created.Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@LogEmpId", UOB.EmpID);
                cmd.Parameters.AddWithValue("@RoleID", UOB.RoleID);
                cmd.Parameters.AddWithValue("@isAdmin", isAdmin);

                cmd.ExecuteNonQuery();
                Status = Convert.ToInt32(cmd.Parameters["@Status"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region Company
        public DataTable CompanyQuery(CompanyBO objcompanyBO)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_Company]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@mode", objcompanyBO.Mode);
                cmd.Parameters.AddWithValue("@CompanyID", objcompanyBO.CompanyID);
                cmd.Parameters.AddWithValue("@CompanyCode", objcompanyBO.CompanyCode);
                cmd.Parameters.AddWithValue("@CompanyName", objcompanyBO.CompanyName);
                cmd.Parameters.AddWithValue("@CompanyDescription", objcompanyBO.CompanyDescription);
                cmd.Parameters.AddWithValue("@CompanyPhoneNo1", objcompanyBO.CompanyPhoneNo1);
                cmd.Parameters.AddWithValue("@CompanyPhoneNo2", objcompanyBO.CompanyPhoneNo2);
                cmd.Parameters.AddWithValue("@CompanyFaxNo1", objcompanyBO.CompanyFaxNo1);
                cmd.Parameters.AddWithValue("@CompanyFaxNo2", objcompanyBO.CompanyFaxNo2);
                cmd.Parameters.AddWithValue("@CompanyEmailID", objcompanyBO.CompanyEmailID);
                cmd.Parameters.AddWithValue("@CompanyWebUrl", objcompanyBO.CompanyWebUrl);
                cmd.Parameters.AddWithValue("@CompanyAddress", objcompanyBO.CompanyAddress);
                cmd.Parameters.AddWithValue("@CompanyLocation", objcompanyBO.CompanyLocation);
                cmd.Parameters.AddWithValue("@CompanyCity", objcompanyBO.CompanyCity);
                cmd.Parameters.AddWithValue("@CompanyState", objcompanyBO.CompanyState);
                cmd.Parameters.AddWithValue("@CompanyCountry", objcompanyBO.CompanyCountry);
                cmd.Parameters.AddWithValue("@CompanyPinCode", objcompanyBO.CompanyPinCode);
                cmd.Parameters.AddWithValue("@CompanyLogo", objcompanyBO.CompanyLogo);
                cmd.Parameters.AddWithValue("@CompanyStartDate", objcompanyBO.CompanyStartDate);
                cmd.Parameters.AddWithValue("@createdBy", objcompanyBO.createdBY);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion company

        #region Get Employees
        public DataTable GetEmployeesByDeptIDandRoleID_DAL(int iDeptID, int iRoleID, bool OnlyActiveEmp,int EmpNameType=1)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_ToGetEmployeesByDeptIDandRoleID]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(spmDeptID).Value = iDeptID;
                SqlParameter spmiRoleID = new SqlParameter("@RoleID", SqlDbType.Int);
                cmd.Parameters.Add(spmiRoleID).Value = iRoleID;
               
                SqlParameter spmOnlyActiveEmp = new SqlParameter("@OnlyActiveEmp", SqlDbType.Bit);
                cmd.Parameters.Add(spmOnlyActiveEmp).Value = OnlyActiveEmp;
                SqlParameter spmEmpNameType = new SqlParameter("@EmpNameType", SqlDbType.Int);
                cmd.Parameters.Add(spmEmpNameType).Value = EmpNameType;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                cmd.Dispose();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public DataTable GetUMSEmployeeRole(int empId, int moduleId, int roleId, out bool IsAccessible)
        {
            string query = "";

            query = "UMS.AizantIT_SP_GetRoleWiseDepartmentList";

            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramEmpid = new SqlParameter("@empid", SqlDbType.Int);
            cmd.Parameters.Add(paramEmpid).Value = empId;

            SqlParameter paramModuleid = new SqlParameter("@moduleid", SqlDbType.Int);
            cmd.Parameters.Add(paramModuleid).Value = moduleId;

            SqlParameter paramRoleid = new SqlParameter("@roleId", SqlDbType.Int);
            cmd.Parameters.Add(paramRoleid).Value = roleId;

            SqlParameter spmIsAccessibleDepts = new SqlParameter("@IsAccessibleDepts", SqlDbType.Bit);
            cmd.Parameters.Add(spmIsAccessibleDepts);
            spmIsAccessibleDepts.Direction = ParameterDirection.Output;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            IsAccessible = true;
            if (roleId != 0)
            {
                IsAccessible = Convert.ToBoolean(spmIsAccessibleDepts.Value);
            }
            return dt;
        }

        public int RolesToEmpAssignDb(int EmpID, int ModuleID, DataTable dtDepts, int RoleID, int CreatedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_AssignEmployeeRoles]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPModuleID = new SqlParameter("@ModuleID", SqlDbType.Int);
                cmd.Parameters.Add(SPModuleID).Value = ModuleID;
                SqlParameter SPRoleId = new SqlParameter("@RoleID", SqlDbType.Int);
                cmd.Parameters.Add(SPRoleId).Value = RoleID;
                SqlParameter SPCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPCreatedBy).Value = CreatedBy;
                cmd.Parameters.AddWithValue("@DeptList", dtDepts);
                SqlParameter statusOut = cmd.Parameters.Add("@Status", SqlDbType.Int);
                statusOut.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();

                int i = Convert.ToInt32(cmd.Parameters["@Status"].Value);
                return i;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();

            }

        }

        public int EmployeeRoleDelete(int EmpID, int ModuleID, int RoleID, int CreatedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_EmployeeRoleDelete]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPModuleID = new SqlParameter("@ModuleID", SqlDbType.Int);
                cmd.Parameters.Add(SPModuleID).Value = ModuleID;
                cmd.Parameters.AddWithValue("@RoleID", RoleID);
                SqlParameter SPCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPCreatedBy).Value = CreatedBy;
                SqlParameter statusOut = cmd.Parameters.Add("@Status", SqlDbType.Int);
                statusOut.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                int i = Convert.ToInt32(cmd.Parameters["@Status"].Value);
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public int RoleDepartmentsDelete(int EmpID, int ModuleID, int RoleID, int DeptID, int CreatedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_EmployeeRoleDepartmentDelete]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPModuleID = new SqlParameter("@ModuleID", SqlDbType.Int);
                cmd.Parameters.Add(SPModuleID).Value = ModuleID;
                cmd.Parameters.AddWithValue("@RoleID", RoleID);
                cmd.Parameters.AddWithValue("@DeptID", DeptID);
                SqlParameter SPCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Int);
                cmd.Parameters.Add(SPCreatedBy).Value = CreatedBy;
                SqlParameter statusOut = cmd.Parameters.Add("@Status", SqlDbType.Int);
                statusOut.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                int i = Convert.ToInt32(cmd.Parameters["@Status"].Value);
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public int RoleDepartmentsChecking(int EmpID, int ModuleID, int RoleID, int DeptID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_EmployeeRoleDepartmentDeleteChecking]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPModuleID = new SqlParameter("@ModuleID", SqlDbType.Int);
                cmd.Parameters.Add(SPModuleID).Value = ModuleID;
                cmd.Parameters.AddWithValue("@RoleID", RoleID);
                cmd.Parameters.AddWithValue("@DeptId", DeptID);
                SqlParameter statusOut = cmd.Parameters.Add("@Status", SqlDbType.Int);
                statusOut.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                int i = Convert.ToInt32(cmd.Parameters["@Status"].Value);
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }

        }
        public int RoleDeleteChecking(int EmpID, int ModuleID, int RoleID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_EmployeeRoleDeleteChecking]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = EmpID;
                SqlParameter SPModuleID = new SqlParameter("@ModuleID", SqlDbType.Int);
                cmd.Parameters.Add(SPModuleID).Value = ModuleID;
                cmd.Parameters.AddWithValue("@RoleID", RoleID);
                SqlParameter statusOut = cmd.Parameters.Add("@Status", SqlDbType.Int);
                statusOut.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                int i = Convert.ToInt32(cmd.Parameters["@Status"].Value);
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }

        }
        public DataTable GetDepartmentsBasedOnRoleID(int empId, int moduleId, int roleId)
        {
            string query = "";

            query = "UMS.AizantIT_SP_GetDepartmentsBasedOnRoleID";

            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramEmpid = new SqlParameter("@empid", SqlDbType.Int);
            cmd.Parameters.Add(paramEmpid).Value = empId;

            SqlParameter paramModuleid = new SqlParameter("@moduleid", SqlDbType.Int);
            cmd.Parameters.Add(paramModuleid).Value = moduleId;

            SqlParameter paramRoleid = new SqlParameter("@roleId", SqlDbType.Int);
            cmd.Parameters.Add(paramRoleid).Value = roleId;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        /*Notification*/

        //public int GetNotificationCount(int EmpID)
        //{
        //    SqlCommand cmd = new SqlCommand("[dbo].[AizantIT_SP_GetUserNotificationCount]", con);
        //    con.Open();
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
        //    cmd.Parameters.Add(SPEmpID).Value = EmpID;
        //   return Convert.ToInt32(cmd.ExecuteScalar());
        //}

        //public DataTable GetNotificationsDataIntoDb(int empID ,out int TotalCount)
        //{
        //    try
        //    {
             
        //        SqlCommand cmd = new SqlCommand("[dbo].[AizantIT_SP_GetTop5UserNotifications]", con);
        //        cmd.CommandTimeout = 300;
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        SqlParameter spmEmp_ID = new SqlParameter("@EmpID", SqlDbType.Int);
        //        cmd.Parameters.Add(spmEmp_ID).Value = empID;
        //        SqlParameter statusOut = cmd.Parameters.Add("@TotalCount", SqlDbType.Int);
        //        statusOut.Direction = ParameterDirection.Output;
        //        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        //        DataTable dt = new DataTable();
        //        sda.Fill(dt);
        //        TotalCount = Convert.ToInt32(cmd.Parameters["@TotalCount"].Value);
        //        cmd.Dispose();
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void SubmitNotificationsToDb(int Notification_ID, int Notify_Status, DataTable EmpID)
        //{
        //    try
        //    {
        //        con.Open();
        //        SqlCommand cmd = new SqlCommand("[dbo].[AizantIT_SP_Notify_Status_Update]", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        SqlParameter spmnotification_ID = new SqlParameter("@NotificationID", SqlDbType.Int);
        //        SqlParameter spmnotify_status = new SqlParameter("@NotificationStatus", SqlDbType.Int);
        //        SqlParameter spmEmpID = new SqlParameter("@NotifyToEmpIDs", SqlDbType.Structured);
        //        cmd.Parameters.Add(spmnotification_ID).Value = Notification_ID;
        //        cmd.Parameters.Add(spmnotify_status).Value = Notify_Status;
        //        cmd.Parameters.Add(spmEmpID).Value = EmpID;
        //        cmd.ExecuteNonQuery();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        con.Close();
        //        con.Dispose();
        //    }
        //}
    }
}
