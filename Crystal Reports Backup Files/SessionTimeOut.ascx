﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SessionTimeOut.ascx.cs" Inherits="AizantIT_PharmaApp.UserControls.SessionTimeOut" %>

<%--Session Time Out Popup--%>
<asp:HiddenField ID="hdfResult" runat="server" Value="0"/>
<div class="modal1 fade" id="logout_popup" tabindex="-1" role="dialog" style="z-index:100000" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" style="top: 25%">
        <div class="modal-content" style="width:80%;left: 15%;">
            <div class="col-lg-12 " style="background:#fff; border-radius:6px;border:2px solid #07889a">
                <div class="col-lg-12 pull-left top">
                    <div class="col-lg-2 pull-left">
                        <asp:Image ID="Image1" runat="server" ImageAlign="Left" style="margin-left: -31px;" ImageUrl="../Images/TMS_Images/fs-sand-timer.gif" />

                        <i class="fs-sand-timer"></i>
                    </div>
                    <div class="col-lg-10 pull-left" style="text-align:center">
                        <h4 style="color:#07889a">Your session is about to expire!</h4>
                        <p style="font-size: 15px;">You will be logged out in <span id="timer" style="display: inline; font-size: 30px;color:#07889a; font-style: bold"></span> seconds.</p>
                        <p style="font-size: 15px;">Do you want to stay signed in?</p>
                    </div>
                </div>
            
            
            <div class="col-lg-10 col-lg-offset-1 pull-left top bottom" style="margin-bottom:10px">
                <asp:UpdatePanel runat="server" ID="upYesbtn_session" UpdateMode="Conditional">
                    <ContentTemplate>
                        <%--<a href="javascript:;" onclick="resetTimer()" class="btn btn-primary" aria-hidden="true">Yes, Keep me signed in</a>--%>
                        <asp:Button ID="btnYes" runat="server" Text="Yes, Keep me signed in" CssClass="btn btn-primary pull-left col-lg-6" style="padding:8px !important; margin-right:10px;" OnClick="btnYes_Click" OnClientClick="closeModelpop();" />
                    </ContentTemplate>
                </asp:UpdatePanel>

                <a href="<%=ResolveUrl("~/UserLogin.aspx")%>" class="btn btn-danger col-lg-5 pull-left" aria-hidden="true" style="padding:8px !important" onclick="btnSessionKill_Click">No, Sign me out</a>
            </div>
        </div>
            </div>
    </div>
</div>

<script>
    var timer;
    var result = (((parseInt('<%=hdfResult.Value%>' - '2') * 60) * 1000) - 2000);
    var count = 0; max_count = 120, coundown = null;
    function SessionTimer(state) {
        if (state == "start") {
            if (coundown != null) {
                clearInterval(coundown);
                count = 0;
            }
            timer = setTimeout(function () {                      
                $('#logout_popup').modal('show');
                var remaining_time = max_count;
                coundown = setInterval(function () {
                    count = count + 1;
                    remaining_time = max_count - count;
                    $('#timer').html(remaining_time);   
                    if (remaining_time==0) {
                        window.open("<%=ResolveUrl("~/UserLogin.aspx")%>", "_self"); 
                    }
                }, 1000);                
            }, result);
        }
        else {
            clearTimeout(timer);   
            timer = 'undefined';
            SessionTimer('start');
        }
    }

    function closeModelpop() {       
        $('#logout_popup').modal('hide');
    }
    
</script>


