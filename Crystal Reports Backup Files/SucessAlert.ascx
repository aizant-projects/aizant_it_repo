﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SucessAlert.ascx.cs" Inherits="AizantIT_PharmaApp.UserControls.SucessAlert" %>

   <div id="ModalSuccess1" class="modal confirmation_popup" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background: #8ac5ff">
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body" style="padding: 0px">
                <div class="col-lg-12 padding-none top" style="padding: 10px">
                    <div class="success_icon col-lg-2">
                        <img src="<%=ResolveUrl("~/Images/CustomAlerts/Confirm.png")%>" style="width: 50px" alt="Smiley face" />
                    </div>
                    <p id="msgSuccess1" class="col-lg-10 capitalize" style="padding: 4px 0px; max-height: 128px; overflow-x: auto;"></p>
                </div>
            </div>capitalize

                    <div class="modal-footer">
                         <asp:Button ID="Button1" CssClass="btn btnConfirmYes true" runat="server" Text="Yes" OnClick="btnYes_Click1" />
                    <asp:Button ID="btnNo" Class="btn-cancel_popup" runat="server" Text="No" OnClick="btnNo_Click" />
                    </div>
                </div>
            </div>
        </div>
<script>
    function custAlertMsgSucess(message, alertType, okEvent = '') {
        if (alertType == 'confirm') {
            document.getElementById("msgSuccess1").innerHTML = message;
            $('#ModalSuccess1').modal({ backdrop: 'static', keyboard: false });
        }
    }
</script>

