﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="ScheduledJRAndTT_PendingList.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Reports.ScheduledJRAndTT_PendingList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="mainContainer" runat="server" visible="true">
            <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 bottom">
                <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Scheduled JR and Target Training Pending List"></asp:Label>
            </div>
            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none">
                <table id="tblJRAndTT_TraineePendingList" class="datatable_cust display breakword" style="width: 100%">
                    <thead>
                        <tr>
                            <th>TraineeID</th>
                            <th>Trainee</th>
                            <th>Trainee Department</th>
                            <th>Pending Training</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-------- Trainee Document Details Modal---------->
            <div id="modalTraineeDocDetails" role="dialog" class="modal department fade">
                <div class="modal-dialog modal-lg" style="min-width:80%">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">

                            <h4 class="modal-title">Pending on Training Document(s)</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body float-left col-lg-12 col-sm-12 col-12 col-md-12">
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none">
                                <table id="tblScheduledJRAndTT_PendingList" class="datatable_cust display breakword" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>Trainee</th>
                                            <th>Document</th>
                                            <th>Doc. Version</th>
                                            <th>Doc. Department</th>
                                            <th>Training Type</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
            <!---- End---->

        </div>
    </div>

    <script>
        <!-------- To Load Trainee Document Details Modal---------->
        function viewTraineeDocDetails(TraineeID) {
            $('#modalTraineeDocDetails').modal({ backdrop: 'static', keyboard: false });
            loadScheduledJRandTT_PendingList(TraineeID);
        }
    </script>

    <!-------- Pending Trainee Details---------->
    <script>
        //$('#tblJRAndTT_TraineePendingList thead tr').clone(true).appendTo('#tblJRAndTT_TraineePendingList thead');
        //$('#tblJRAndTT_TraineePendingList thead tr:eq(1) th').each(function (i) {
        //    if (i < 3) {
        //        var title = $(this).text();
        //        $(this).html('<input type="text" placeholder=" Search " />');

        //        $('input', this).on('keyup change', function () {
        //            if (oTableJRAndTT_TraineePendingList.column(i).search() !== this.value) {
        //                oTableJRAndTT_TraineePendingList
        //                    .column(i)
        //                    .search(this.value)
        //                    .draw();
        //            }
        //        });
        //    }
        //    else {
        //        $(this).html('');
        //    }
        //});
        var oTableJRAndTT_TraineePendingList;
        function loadScheduledJRandTT_TraineePendingList() {
            if (oTableJRAndTT_TraineePendingList != null) {
                oTableJRAndTT_TraineePendingList.destroy();
            }
            oTableJRAndTT_TraineePendingList = $('#tblJRAndTT_TraineePendingList').DataTable({
                columns: [
                    { 'data': 'TraineeID' },//0
                    { 'data': 'Trainee' },//1
                    { 'data': 'Department' },//2
                    {
                        "mData": null,
                        // "bSortable": false,
                        "mRender": function (o) { return '<a  href="#" class="grid_button" onclick="viewTraineeDocDetails(' + o.TraineeID + ');">' + o.TrainingPendingCount + '</a>'; }
                    },
                ],
                "orderCellsTop": true,
                "order": [[0, "desc"]],
                "bServerSide": true,
                "aoColumnDefs": [{ "targets": [0], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [1, 2] }],
                "sAjaxSource": AizantIT_WebAPI_URL + "/api/TmsService/ScheduledJRAndTT_TraineePendingList",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "HeadEmpID", "value": '<%=Session["UserDetails"]!=null?(Session["UserDetails"] as System.Data.DataSet).Tables[0].Rows[0]["EmpID"].ToString():""%>' });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "headers": WebAPIRequestHeaderJson,
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            if (result.msgType == "error") {
                                custAlertMsg(result.msg, "error");
                            }
                            else {
                                fnCallback(msg);
                            }
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                var msg = "";
                                if (xhr.status === 0) {
                                    msg = 'No server Connection, Verify Network.';
                                } else if (xhr.status == 404) {
                                    msg = 'Requested page not found. [404]';
                                } else if (xhr.status == 500) {
                                    msg = 'Internal Server Error [500].';
                                }
                                if (msg != "") {
                                    custAlertMsg(msg, "error");
                                }
                            }
                        }
                    });
                }
            });
        }
        loadScheduledJRandTT_TraineePendingList();
    </script>

    <script>
        $('#tblScheduledJRAndTT_PendingList thead tr').clone(true).appendTo('#tblScheduledJRAndTT_PendingList thead');
        var tblScheduledJRAndTT_PendingListColLenght = [150, 560, 100, 50, 20];
        $('#tblScheduledJRAndTT_PendingList thead tr:eq(1) th').each(function (i) {
            $(this).html('<input type="search" placeholder=" Search " maxlength="' + tblScheduledJRAndTT_PendingListColLenght[i] + '" />');
            $('input', this).on('keyup change', function () {
                if (oTableScheduledJRandTT_PendingList.column(i).search() !== this.value) {
                    oTableScheduledJRandTT_PendingList
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });
        var oTableScheduledJRandTT_PendingList;
        function loadScheduledJRandTT_PendingList(TraineeID) {
            if (oTableScheduledJRandTT_PendingList != null) {
                oTableScheduledJRandTT_PendingList.destroy();
            }
            oTableScheduledJRandTT_PendingList = $('#tblScheduledJRAndTT_PendingList').DataTable({
                columns: [
                    { 'data': 'Trainee' },//0
                    { 'data': 'Document' },//1
                    { 'data': 'DocumentVersion' },//2
                    { 'data': 'DocumentDepartment' },//3
                    { 'data': 'TrainingType' },//4
                ],
                "orderCellsTop": true,
                "order": [[0, "desc"]],
                "bServerSide": true,
                "aoColumnDefs": [{ "targets": [0], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [0, 1, 3, 4] }],
                "sAjaxSource": AizantIT_WebAPI_URL + "/api/TmsService/ScheduledJRAndTT_PendingList",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "TraineeID", "value": TraineeID });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "headers": WebAPIRequestHeaderJson,
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            if (result.msgType == "error") {
                                custAlertMsg(result.msg, "error");
                            }
                            else {
                                fnCallback(msg);
                            }
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                var msg = "";
                                if (xhr.status === 0) {
                                    msg = 'No server Connection, Verify Network.';
                                } else if (xhr.status == 404) {
                                    msg = 'Requested page not found. [404]';
                                } else if (xhr.status == 500) {
                                    msg = 'Internal Server Error [500].';
                                    if (xhr.statusText != "" || xhr.statusText != null) {
                                        msg = xhr.statusText;
                                    }
                                }
                                if (msg != "") {
                                    custAlertMsg(msg, "error");
                                }
                            }
                        }
                    });
                }
            });
        }
    </script>
</asp:Content>
