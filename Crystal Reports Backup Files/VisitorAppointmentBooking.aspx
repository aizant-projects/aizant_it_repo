﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VisitorAppointmentBooking.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VisitorRegister.VisitorAppointmentBooking" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">
    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 950px; margin-left: 15%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Visitor Appointment Booking</span>
                    </div>
                </div>
            </div>
            
            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="Row">
                        <div class="col-sm-6">

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div5">
                                    <label class="control-label col-sm-4" id="lblAppointmentId" style="text-align: left; font-weight: lighter">Appointment ID</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtAppointmentId" runat="server" CssClass="form-control" ReadOnly="true" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <asp:UpdatePanel runat="server" ID="upAppointmentDate" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div6">
                                            <label class="control-label col-sm-4" id="lblAppointmentDate" style="text-align: left; font-weight: lighter">Appointment Date<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtAppointmentDate" runat="server" CssClass="form-control" placeholder="Select Appointment Date"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="txtAppointmentDate" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div1">
                                    <label class="control-label col-sm-4" id="lblVisitor Name" style="text-align: left; font-weight: lighter">First Name<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txt_VisitorName" runat="server" CssClass="form-control" placeholder="Enter Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" MaxLength="50" AutoPostBack="true" OnTextChanged="txt_VisitorName_TextChanged"></asp:TextBox>
                                        <asp:AutoCompleteExtender ServiceMethod="GetVisitorAppintmentNameList" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                            TargetControlID="txt_VisitorName" ID="AutoCompleteExtender1" UseContextKey="true" runat="server" FirstRowSelected="false">
                                        </asp:AutoCompleteExtender>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div12">
                                    <label class="control-label col-sm-4" id="lblLast Name" style="text-align: left; font-weight: lighter">Last Name<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" placeholder="Enter Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div2">
                                    <label class="control-label col-sm-4" id="lblMobileNo" style="text-align: left; font-weight: lighter">Mobile No<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txt_MobNo" runat="server" CssClass="form-control" placeholder="Enter Mobile" onkeypress="return ValidatePhoneNo(event);" MaxLength="10" AutoPostBack="true" OnTextChanged="txt_MobNo_TextChanged"></asp:TextBox>
                                        <asp:AutoCompleteExtender ServiceMethod="GetVisitorAppintmentListByMobile" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                            TargetControlID="txt_MobNo" ID="AutoCompleteExtender2" UseContextKey="true" runat="server" FirstRowSelected="false">
                                        </asp:AutoCompleteExtender>
                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div3">
                                    <label class="control-label col-sm-4" id="lblEmail" style="text-align: left; font-weight: lighter">Email</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txt_EmailID" runat="server" CssClass="form-control" placeholder="Enter Email" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div7">
                                    <label class="control-label col-sm-4" id="lblAppointmentTime" style="text-align: left; font-weight: lighter">Appointment Time</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtAppointmentTime" runat="server" CssClass="form-control" placeholder="Enter Appointment Time" TextMode="Time" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div4">
                                    <label class="control-label col-sm-4" id="lblOrganization" style="text-align: left; font-weight: lighter">Organization</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtOrganisationName" runat="server" CssClass="form-control" placeholder="Enter Organization Name" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                        <asp:AutoCompleteExtender ServiceMethod="GetVisitorListByOrganization" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                            TargetControlID="txtOrganisationName" ID="AutoCompleteExtender7" UseContextKey="true" runat="server" FirstRowSelected="false">
                                        </asp:AutoCompleteExtender>
                                    </div>
                                </div>
                            </div>

                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div8">
                                            <label class="control-label col-sm-4" id="lblPurpose" style="text-align: left; font-weight: lighter">Purposeof Visit<span class="mandatoryStar">*</span></label>
                                            <asp:Button ID="btn_purposeofvisit1" runat="server" CssClass="btnnVisit" Text="+" Font-Bold="true" OnClick="addpurposeofvisit1" />
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txt_purposeofvisit" runat="server" CssClass="form-control" placeholder="Enter Purpose Of Visit" Visible="false" AutoPostBack="true" OnTextChanged="txt_purposeofvisit_TextChanged"></asp:TextBox>
                                                <asp:DropDownList ID="ddlVisitorType" runat="server" CssClass="form-control SearchDropDown" ClientIDMode="Static" placeholder="Enter Purpose Of Visit"></asp:DropDownList>
                                                <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Size="Smaller"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div9">
                                            <label class="control-label col-sm-4" id="lblDepartment" style="text-align: left; font-weight: lighter">Department</label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlDeptToVisit" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" OnSelectedIndexChanged="ddlDeptToVisit_SelectedIndexChanged"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlDeptToVisit" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div10">
                                            <label class="control-label col-sm-4" id="lblWhomToVisit" style="text-align: left; font-weight: lighter">Whom To Visit</label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlWhomToVisit" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlWhomToVisit" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div11">
                                    <label class="control-label col-sm-4" id="lblRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txt_Remarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>

                    <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                        <asp:Button ID="btnCreate" Text="Submit" CssClass="button" runat="server" OnClientClick="javascript:return Submitvaidate();" OnClick="btnCreate_Click" />
                        <asp:Button ID="btnCancel" Text="Reset" CssClass="button" runat="server" OnClick="btnCancel_Click" />
                    </div>
                    <div>
                        <asp:Label ID="lblmesg" runat="server" ForeColor="Red" Font-Size="Smaller"></asp:Label>
                    </div>
                    <script type="text/javascript">
                        function Submitvaidate() {
                            var AVisitorName = document.getElementById("<%=txt_VisitorName.ClientID%>").value;
                            var AVisitorLastName = document.getElementById("<%=txtLastName.ClientID%>").value;
                            var VMobile = document.getElementById("<%=txt_MobNo.ClientID%>").value;
                            var VADate = document.getElementById("<%=txtAppointmentDate.ClientID%>").value;
                            var VAPurpose = document.getElementById("<%=ddlVisitorType.ClientID%>").value;
                        errors = [];
                        if (VADate == "") {
                            errors.push("Please Select Appointment Date.");
                        }
                        if (AVisitorName.trim() == "") {
                            errors.push("Please Enter First Name.");
                        }
                        if (AVisitorLastName.trim() == "") {
                            errors.push("Please Enter Last Name.");
                        }
                        if (VMobile == "") {
                            errors.push("Please Enter Mobile Number.");
                        }
                        else {
                            if (VMobile.length != 10) {
                                errors.push("Mobile No Should be 10 Digits.");
                            }
                        }
                        if (VAPurpose == 0) {
                            errors.push("Please Select Purpose of Visit.");
                        }

                        var email = document.getElementById("<%=txt_EmailID.ClientID%>").value;
                        if (email.length > 0) {
                            if (!IsValidEmail(email)) {
                                errors.push("Invalid email address.");
                            }
                        }
                        if (errors.length > 0) {
                            custAlertMsg(errors.join("<br/>"), "error");
                            return false;
                        }
                        }
                    </script>
<script>
                        $("#NavLnkVisitorRegistration").attr("class", "active");
                        $("#VisitorAppointmentBooking").attr("class", "active");
                    </script>

                    <script>
                        $(function () {
                            $('#<%=txtAppointmentDate.ClientID%>').datetimepicker({
                                format: 'DD MMM YYYY',
                                minDate: new Date(),
                            });
                        });
                    </script>

                    <script type="text/javascript">
                        function savealert() {
                            if (!alert('Appointment is booked  sucsessfully')) document.location.href = 'VisitorAppointmentBooking.aspx';
                        }
                    </script>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>




