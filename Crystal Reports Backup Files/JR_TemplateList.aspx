﻿<%@ Page Title="JR Template List" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="JR_TemplateList.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Administration.JR_Template.JR_TemplateList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnJRTemplateID" runat="server" Value="0" />

    <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="float-left float-left col-lg-12 col-xs-12 col-md-12 col-sm-12 padding-none" id="mainContainer" runat="server">
                <div class="grid_header float-left col-lg-12  col-xs-12 col-md-12 col-sm-12  float-left ">
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Text="JR Template List"></asp:Label>
                </div>
                <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none float-left">
                    <div class="float-left col-lg-6 col-sm-6 col-xs-12 col-md-6">
                        <div class="form-horizontal">
                            
                        </div>
                    </div>

                    <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 top float-left bottom padding-none">
                        <table id="tblJR_TemplateList" class="datatable_cust tblJR_TemplateListClass display breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>JR_TemplateID</th>
                                    <th>JR Template</th>
                                    <th>DeptID</th>
                                    <th>Department</th>
                                    <th>Dept Code</th>
                                    <th>Created By</th>
                                    <th>Created Date</th>
                                    <th>Modified By</th>
                                    <th>Modified Date</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnEditJrTemplateList" runat="server" Text="EditJR" OnClick="btnEditJrTemplateList_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    <script>
        function ViewJR_TemplateList(JR_TemplateID) {
            $("#<%=hdnJRTemplateID.ClientID%>").val(JR_TemplateID);
            $("#<%=btnEditJrTemplateList.ClientID%>").click();
        }
    </script>

    <!--JR_Template List jQuery DataTable-->
    <script>
        var oTableJR_TemplateList;
        $(function () {
            loadJrTemplateList();
        });        

        function loadJrTemplateList()
        {
            oTableJR_TemplateList = $('#tblJR_TemplateList').DataTable({
                columns: [
                    { 'data': 'JR_TemplateID' },
                    { 'data': 'JR_TemplateName' },
                    { 'data': 'DeptID' },
                    { 'data': 'DepartmentName' },
                    { 'data': 'DeptCode' },
                    { 'data': 'CreatedByName' },
                    { 'data': 'CreatedDate' },
                    { 'data': 'ModifiedByName' },
                    { 'data': 'ModifiedDate' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) { return '<a class="Edit"  href="#" onclick="ViewJR_TemplateList(' + o.JR_TemplateID + ');"></a>'; }
                    }
                ],
                "order": [[6, "desc"]],
                "aoColumnDefs": [{ "targets": [0, 2, 3], "visible": false }, { className: "textAlignLeft", "targets": [1, 3, 5, 7] }],
                "sAjaxSource": AizantIT_WebAPI_URL+"/api/TmsService/JRTemplateList",
         <%--   "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JR_TemplateListService.asmx/GetJR_TemplateList")%>",--%>
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "headers": WebAPIRequestHeaderJson,
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = msg;
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
    </script>
</asp:Content>
