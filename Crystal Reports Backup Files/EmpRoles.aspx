﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.Master" AutoEventWireup="true" CodeBehind="EmpRoles.aspx.cs" Inherits="AizantIT_PharmaApp.UserManagement.EmpRoles" %>

<%@ Register Src="~/UserControls/SucessAlert.ascx" TagPrefix="uc1" TagName="SucessAlert" %>
<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #ListBoxArea {
            margin-top: 10px;
            padding: 0px;
        }

        .cbStyles label {
            margin-left: 3px;
        }
         .dropdown-menu {
            max-height:300px !important;
        }
        .dropdown-menu > li > a {
    white-space: normal !important;
    font-size: 10pt;
    color: #075f6b !important;
    font-family: seguisb;
    border-bottom: 1px solid #075f6b;
}
        .dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus {
    color: #fff!important;
    text-decoration: none;
    background-color: #075f6b !important;
    outline: 0;
}
        .cbBorder {
            max-height: 200px;
            min-height: 200px;
            border: thin;
            border-color: lightgray;
            border-radius: 5px;
            border-style: solid;
            overflow: auto;
            padding: 5px;
        }

        input[type="radio"], input[type="checkbox"] {
            height: 18px;
            width: 25px;
            margin: 4px 0 0;
            margin-top: 1px \9;
            line-height: normal;
        }

        .roles_header_border table {
            width: 96%;
        }

            .roles_header_border table tr {
                width: 100%;
            }

                .roles_header_border table tr td {
                    width: 100%;
                    background-color: white;
                    margin-left: 10px;
                    border-bottom: 1px solid #07889a;
                    clip: rect(0px, 0px, 0px, 0px);
                    float: left;
                    padding: 2px 10px;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    
    <uc1:SucessAlert runat="server" ID="SucessAlert" />
    <div class=" col-lg-3 padding-none">
        <div class="closebtn"><span>&#9776;</span></div>
        <div style="display: none;" class="closebtn1"><span>&#9776;</span></div>
    </div>
    <div class="col-lg-12 col-sm-12  col-md-12 col-xs-12 padding-none">

        <div class=" col-lg-12 col-sm-12  col-md-12 col-xs-12 grid_area padding-none">
            <div class="col-lg-12 col-sm-12  col-md-12 col-xs-12 padding-none">
                <div id="header">
                    <div class="col-lg-12 col-lg-12 col-sm-12  col-md-12 col-xs-12 header_profile">
                        <div class="col-sm-6">Employee Roles</div>
                        <div class="col-sm-6">
                            <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <asp:HiddenField ID="hdnUserMakeChanges" runat="server" />
                                    <asp:HiddenField ID="hdnSaveChanges" runat="server" />
                                    <asp:HiddenField ID="hdnPreviousModuleID" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-sm-12  col-md-12 col-xs-12 padding-none" style="margin-top: 10px;">

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="col-lg-4 bottom  ">
                                <asp:DropDownList ID="ddlDepartment" CssClass="form-control SearchDropDown selectpicker"  data-size="9" data-live-search="true" runat="server" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="ddlDepartments_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <div class="col-lg-4 bottom">
                                <asp:DropDownList ID="ddlEmployeeList" CssClass="form-control SearchDropDown selectpicker" data-size="9" data-live-search="true" runat="server" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="ddlEmployeeList_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <div class="col-lg-4 bottom ">
                               <asp:DropDownList ID="ddlModulesList" CssClass="form-control SearchDropDown selectpicker" data-size="9" data-live-search="true" runat="server" AutoPostBack="true" Width="100%"  ToolTip="-- Select Module --" OnSelectedIndexChanged="ddlModulesList_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-lg-12 col-sm-12  col-md-12 col-xs-12 selectedbox" style="margin-top: 10px;">
                    <div class="col-xs-6 col-lg-4 col-md-6 col-sm-4 roles_header_border padding-none col-lg-offset-1">
                        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 roles_header" style="text-align: center">Assign Roles</div>
                        <asp:UpdatePanel ID="upAssignRoles" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:CheckBoxList ID="chkAssignRoles" class="class" runat="server" ></asp:CheckBoxList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div data-toggle="buttons">
                        </div>
                    </div>
                    <div class="col-xs-6 col-lg-4 col-md-6 col-sm-6 col-lg-offset-2  padding-none roles_header_border">
                        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 roles_header" style="text-align: center">Select Accessible Department</div>

                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:CheckBoxList ID="cblDepartments" runat="server" CssClass="cbStyles" AutoPostBack="true" OnSelectedIndexChanged="cblDepartments_SelectedIndexChanged" ToolTip="Select Accessible Departments">
                                </asp:CheckBoxList>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="" data-toggle="buttons">
                        </div>
                    </div>
                    <div class="col-sm-12 modal-footer Panel_Footer">
                        <div class="Panel_Footer_Buttons">
                            <div style="margin-top: 7px">
                                <asp:UpdatePanel ID="upsubmit" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <input type="button" class="float-right btn btn-cancel_popup" value="Delete" onclick="ConfirmDelete();">
                                         <asp:Button ID="btnDelete1"  runat="server" Text="Delete" style="display:none;" OnClick="btnDelete1_Click"  />
                                        
                                        <asp:Button ID="btnCancel" class="float-right btn btn-cancel_popup" runat="server" Text="Cancel" PostBackUrl="~/UserManagement/EmployeeList.aspx" ToolTip="Cancel" />
                                        <asp:Button ID="btnReset" class="float-right btn btn-signup_popup" runat="server" Text="Reset" OnClick="btnReset_Click" ToolTip=" Reset" />
                                        <asp:Button ID="btnSubmit" class="float-right btn btn-signup_popup" runat="server" Text="Submit"  OnClick="btnSubmit_Click" ToolTip="Submit" Style="margin-right: 7px !important;" />
                                       
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="Panel_Footer_Message_Label" hidden="hidden">
                            <asp:UpdatePanel ID="uplblMsg" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <asp:Label ID="lblMsg" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row Panel_Frame" style="margin-top: 30px" hidden="hidden">


        <hr style="margin-bottom: 5px; margin-top: 10px" />
        <div id="body" class="col-sm-12">
            <div class="row">
                <div class="col-sm-12" id="ListBoxArea">
                    <asp:UpdatePanel ID="upEmpRoleDept" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-sm-4">
                                <span style="color: darkgray">Available Roles</span>
                                <asp:ListBox ID="lbFirst" runat="server" SelectionMode="Multiple" CssClass="form-control" Height="200px" ToolTip="Select Available Roles"></asp:ListBox>
                            </div>
                            <div class="col-sm-1" style="margin-top: 5%">
                                <div class="btn-group" id="divbbtnAddRemoveRoles" style="padding-left: 20px" runat="server">

                                    <asp:Button ID="btnAddRoles" runat="server" class="btn btn-default" Text=">" Style="margin-bottom: 10px; font-weight: bold; padding: 12px; padding-right: 12px; border-top-right-radius: 10px; border-bottom-right-radius: 10px; border-bottom-left-radius: 0px; border-top-left-radius: 0px;"
                                        OnClick="btnAddRoles_Click" Enabled="false" ToolTip="Click to Add Roles" />
                                    <asp:Button ID="btnRemoveRoles" runat="server" class="btn btn-default" Text="<" Style="font-weight: bold; padding: 12px; padding-right: 12px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 10px; border-top-left-radius: 10px;"
                                        OnClick="btnRemoveRoles_Click" Enabled="false" ToolTip="Click to Remove Roles" />

                                </div>
                            </div>
                            <div class="col-sm-4">
                                <span style="color: darkgray">Assigned Roles</span>
                                <asp:ListBox ID="lbSecond" runat="server" SelectionMode="Multiple" CssClass="form-control" Height="200px" ToolTip="Select Assigned Roles"></asp:ListBox>
                            </div>
                            <div class="col-sm-3">
                                <span style="color: darkgray">Select Accessible Departments</span>
                                <div class="cbBorder">
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>


    </div>
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <script>
        function custAlertMsgSucess1() {
            custAlertMsgSucess('Do You Want To Save the Changes', 'confirm', true);
        }
        function ConfirmAlertDelete() {
            custAlertMsg('Do you want to Remove Roles for this Module ?', 'confirm', true);
            document.getElementById("<%=hdnUserMakeChanges.ClientID%>").value = "btnDeleteClick";
        }
        function ConfirmDelete() {
            errors = [];
            if (document.getElementById('<%=ddlEmployeeList.ClientID%>').value == 0) {
                errors.push("Select Employee");
            }
            if (document.getElementById('<%=ddlModulesList.ClientID%>').value == 0) {
                errors.push("Select Module");
            }
            if (errors.length > 0) {
                document.getElementById("msgError").innerHTML = errors.join("<br/>");
                $("#ModalError").modal();
                $("#btnErrorOk").focus();
                return false;
            }
            ConfirmAlertDelete();
            
        }
    </script>
    <script>   
        function OnModuleChange() {

            var UserMakeChanges = document.getElementById('<%=hdnUserMakeChanges.ClientID%>').value;
            if (UserMakeChanges != "N") {
                if (document.getElementById('<%=chkAssignRoles.ClientID%>').length > 0) {
                    var Departments = document.getElementById("<%=cblDepartments.ClientID%>");
                    var checkbox = Departments.getElementsByTagName("input");
                    var counter = 0;
                    errors = [];
                    for (var i = 0; i < checkbox.length; i++) {
                        if (checkbox[i].checked) {
                            counter++;
                        }
                    }
                    if (counter == 0) {
                        errors.push("Select Atleast one Accessible Department");
                    }
                   

                    else {
                        
                       
                    }
                }
                else {
              
                }
            }
        }
    </script>

    <script>

        $(function () {
            $("#<%=btnSubmit.ClientID%>").click(function () {
                errors = [];
                if (document.getElementById('<%=ddlDepartment.ClientID%>').value == 0) {
                    errors.push("Select Department");
                }
                if (document.getElementById('<%=ddlEmployeeList.ClientID%>').value == 0) {
                    errors.push("Select Employee");
                }
                if (document.getElementById('<%=ddlModulesList.ClientID%>').value == 0) {
                    errors.push("Select Module");
                }
                if (document.getElementById('<%=lbSecond.ClientID%>').length > 0) {
                    var CHK = document.getElementById("<%=cblDepartments.ClientID%>");
                    var checkbox = CHK.getElementsByTagName("input");
                    var counter = 0;
                    for (var i = 0; i < checkbox.length; i++) {
                        if (checkbox[i].checked) {
                            counter++;
                        }
                    }
                    if (1 > counter) {
                        errors.push("Select Atleast one Accessible Department");
                    }
                }

                if (errors.length > 0) {
                    document.getElementById("msgError").innerHTML = errors.join("<br/>");
                    $("#ModalError").modal();
                    $("#btnErrorOk").focus();
                    return false;
                }
                else {
                    return true;
                }
            });
        });
    </script>

    <script>
        $("#NavLnkEmpRoles").attr("class", "active");
    </script>
</asp:Content>
