﻿<%@ Page Title="TMS Dash Board" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="TMS_Dashboard.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TMS_Dashboard" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

<%@ Register Src="~/UserControls/SessionTimeOut.ascx" TagPrefix="uc1" TagName="SessionTimeOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <style>
       

    </style>
    <div class="col-lg-12 col-lg-12 col-xs-12 col-md-12 col-sm-12 dms_outer_border grid_area_tms">

        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 top">

            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 dashboard_msg" id="divNoTask" runat="server" visible="false">"No tasks for today Enjoy your day...!"</div>
            <!--QA Role :  5-->
            <asp:HyperLink ID="hlApproveJR" NavigateUrl="~/TMS/JR/JR_ReviewList.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divEmpApprovedJR" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12  outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">JR(s) for Approval</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/approve.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lbEmpApprovedJR" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlApproveTS" NavigateUrl="~/TMS/TS/JrTS_ReviewList.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divHodAssignedTS" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12  outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">JR Training Schedule(s) for Approval</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/approve.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lbHodAssignedTS" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlQA_TTS_Approval" NavigateUrl="~/TMS/TargetTrainingSchedule/TTS_ApprovalList.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divQA_TTS_Approval" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">Target Training(s) Approval</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  padding-none ">
                                <img src="../Images/TmsDashboard/pending.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  ">
                                <asp:Label ID="lblQA_TTS_Approval" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlQA_ATC_Approval" NavigateUrl="~/TMS/ATC/List/ATC_QA_Approve.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divQA_ATC_Approval" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">QA Approval Annual Training Calendar(s)</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/pending.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lblQA_ATC_Approval" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <!--HOD Role :10-->
            <asp:HyperLink ID="hlQAApprovedJR" NavigateUrl="~/TMS/TS/JrTS_PendingList.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divQaApprovedJR" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">Assign Training Schedule to JR</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 pull-left padding-none ">
                                <img src="../Images/TmsDashboard/assign.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 pull-right">
                                <asp:Label ID="lbQaApprovedJR" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlQADeclinedJR" NavigateUrl="~/TMS/JR/JR_PendingList.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divQaDeclinedJR" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12  outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">QA Reverted JR(s)</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/reverted_questions.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lbQadeclinedJR" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlQADeclinedTS" NavigateUrl="~/TMS/TS/JrTS_PendingList.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divQaDeclinedTS" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">QA Reverted JR Training Schedule(s)</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/reverted_questions.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lbQaDeclinedTS" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlHODEvalution" NavigateUrl="~/TMS/Training/JR_Training/Evaluation/HodEvaluation.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-right_div fade-inTms one" id="divHODTrainingEvalution" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">JR HOD Evaluation</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/evaluation.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  ">
                                <asp:Label ID="lbHODTrainingEvalution" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlHOD_TTS_RefresherDocForThisMonth" NavigateUrl="~/TMS/TargetTrainingSchedule/TTS_Master.aspx?TTS_Type=Create" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divHOD_TTS_RefresherDocForThisMonth" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">Refresher Doc(s) For This Month</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  padding-none ">
                                <img src="../Images/TmsDashboard/pending.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  ">
                                <asp:Label ID="lblHOD_TTS_RefresherDocForThisMonth" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlHOD_TTS_NewSOP_Pending" NavigateUrl="~/TMS/TargetTrainingSchedule/TTS_Master.aspx?TTS_Type=Create" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divHOD_TTS_NewSOP_Pending" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">New Doc(s) For Target Training</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  padding-none ">
                                <img src="../Images/TmsDashboard/pending.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  ">
                                <asp:Label ID="lblHOD_TTS_NewSOP_Pending" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlHOD_TTS_RevisionSOP_Pending" NavigateUrl="~/TMS/TargetTrainingSchedule/TTS_Master.aspx?TTS_Type=Create" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divHOD_TTS_RevisionSOP_Pending" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">Revision Doc(s) for Target Training</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  padding-none ">
                                <img src="../Images/TmsDashboard/pending.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  ">
                                <asp:Label ID="lblHOD_TTS_RevisionSOP_Pending" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlHOD_Que_ToApproval" NavigateUrl="~/TMS/Administration/Questionnaire/ApproveQuestionnaire.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divHOD_Que_ToApproval" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12  outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">Questionnaire to Approve</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  padding-none">
                                <img src="../Images/TmsDashboard/question_to_approve.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  ">
                                <asp:Label ID="lblHOD_Que_ToApproval" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlQA_RevertedTTS" NavigateUrl="~/TMS/TargetTrainingSchedule/TTS_Pending.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divQA_RevertedTTS" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12  outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">QA Reverted Target Training(s)</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  padding-none">
                                <img src="../Images/TmsDashboard/reverted_questions.png" class="dashboard-image" alt="Revert" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  ">
                                <asp:Label ID="lblQA_RevertedTTS" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlHOD_ATCApproval" NavigateUrl="~/TMS/ATC/List/ATC_Hod_Approval.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divHOD_ATCApproval" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">HOD to Review Annual Training Calendar(s)</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/pending.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lblHOD_ATCApproval" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <!-- For Trainer  Role: 6-->
            <asp:HyperLink ID="hlTraineerEvalution" NavigateUrl="~/TMS/Training/JR_Training/Evaluation/JR_TrainerEvaluationDetails.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divTrainerTrainingEvalution" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">JR Trainer Evaluation</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  padding-none ">
                                <img src="../Images/TmsDashboard/evaluation.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  ">
                                <asp:Label ID="lbTrainerTrainingEvalution" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlTrainer_Que_ToModify" NavigateUrl="~/TMS/Administration/Questionnaire/Questionnaire.aspx?Reverted='Yes'" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divTrainer_Que_ToModify" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">HOD Reverted Questionnaire</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  padding-none ">
                                <img src="../Images/TmsDashboard/reverted_questions.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lbTrainer_Que_ToModify" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlTrainer_TTSTrainerEvaluation" NavigateUrl="~/TMS/Training/TargetTrainerEvaluation.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divTrainer_TTS_Evaluation" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">Target Training Evaluation</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/evaluation.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lblTrainer_TTS_Evaluation" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlTrainer_TTS_SessionCreationOrModification" NavigateUrl="~/TMS/TargetTrainingSchedule/TTS_ApprovedList.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divTrainer_TTS_SessionCreationOrModification" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">Target Training Session(s)</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/pending.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lblTrainer_TTS_SessionCreationOrModification" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlTrainer_ATC_CreatePending" NavigateUrl="~/TMS/ATC/List/CreateATC_List.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divTrainer_ATC_CreatePending" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">Annual Training Calendar(s) To Create</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/pending.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lblTrainer_ATC_CreatePending" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlTrainer_ATC_QAorHODReverted" NavigateUrl="~/TMS/ATC/List/ATC_PendingList.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divTrainer_ATC_QAorHODReverted" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">Reverted Annual Training Calendar(s)</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/pending.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lblTrainer_ATC_QAorHODReverted" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <!--employee or trainee Role : 4-->
            <asp:HyperLink ID="hlAcceptMyJR" NavigateUrl="~/TMS/JR/AcceptMyJR.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divHodAssignedJR" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12  outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">Accept My JR</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/accept.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lbHodAssignedJR" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlJR_Training" NavigateUrl="~/TMS/Training/JR_Training/JR_TrainingList.aspx" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divJR_Training" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">JR Doc(s) to Complete</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/pending.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lblSopsCount" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlTTS_ClasRoomExamPending" NavigateUrl="~/TMS/Training/TargetTraining.aspx?Tab=ClassRoom" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divTarget_TrainingClassroom" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">Target Training Exam(s) to Complete</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/pending.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lblTargetPendingClassroomExamsCount" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="hlTTS_TraineeSessionsToAttend" NavigateUrl="~/TMS/Training/TargetTraining.aspx?Tab=ClassRoom" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divTTS_TraineeSessionsToAttend" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">Training Session(s) to Attend</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/pending.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lblTTS_TraineeSessionsToAttend" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
            <asp:HyperLink ID="lhTTS_OnlineExamPending" NavigateUrl="~/TMS/Training/TargetTraining.aspx?Tab=Online" runat="server">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 padding-none fade-inTms one" id="divTTS_OnlineExamPending" runat="server" visible="false">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard  pull-left">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard">Online Target Exam(s) to Complete</div>
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none ">
                                <img src="../Images/TmsDashboard/pending.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 ">
                                <asp:Label ID="lblTTS_OnlineExamPending" CssClass="link_dashboard" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:HyperLink>
        </div>
    </div>
    <uc1:SessionTimeOut runat="server" ID="SessionTimeOut" />


</asp:Content>
