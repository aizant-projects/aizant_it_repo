﻿<%@ Page Title="Re-Assign Emps" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="ModifyActionEmployees.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.ModifyActionEmployees" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:HiddenField ID="hdnBaseID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDeptID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnEmpBindStatus" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTraineeID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnAuthor" runat="server" Value="0" />
    <asp:HiddenField ID="hdnReviewer" runat="server" Value="0" />
    <asp:HiddenField ID="hdnApprover" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTTS_CurrentStatusID" runat="server" Value="0" />
     <asp:HiddenField ID="hdnIsReviewerEnabled" runat="server" Value="0" />

    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text="Your Are Not Authorized to view this Page. Contact Admin.">                
            </asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12">Re-Assign Employees</div>
            <div class="float-left col-lg-4 col-sm-12 col-12 col-md-12 top">
                <label class=" padding-none  control-label custom_label_answer">Select Process Type</label>
                <asp:DropDownList ID="ddlType" CssClass="regulatory_dropdown_style form-control ddlTypeClass selectpicker"
                    data-size="5" runat="server">
                    <asp:ListItem Text="-Select Process Type-" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Job Responsibility (JR)" Value="1"></asp:ListItem>
                    <asp:ListItem Text="JR Training Schedule" Value="2"></asp:ListItem>
                    <asp:ListItem Text="JR Evaluation" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Target Training Schedule" Value="4"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                <div id="divTypeTable">
                    <table id="tblTypeEmployees" class="datatable_cust display breakword" style="width: 100%">
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-----Modal Popup Action Employees--->
    <div id="modalActionEmp" class="modal department fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Re-Assign Employee(s)</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group padding-none">
                        <div class=" padding-none">
                            <asp:UpdatePanel ID="upModalActionEmps" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div id="divAuthorDropdown" runat="server">
                                        <asp:Label ID="lblAuthors" runat="server" CssClass="padding-none control-label label-style custom_label_answer"><span class="mandatoryStar">*</span></asp:Label>
                                        <div>
                                            <asp:DropDownList ID="ddlAuthor" data-live-search="true" data-size="6" onchange="ddlAuthorChange();" CssClass="regulatory_dropdown_style selectpicker form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div id="divReviewerDropdown" runat="server">
                                        <label class="padding-none control-label custom_label_answer">Reviewer<span class="mandatoryStar">*</span></label>
                                        <div>
                                            <asp:DropDownList ID="ddlReviewer" data-live-search="true" data-size="6" onchange="ddlReviewerChange();" CssClass="regulatory_dropdown_style selectpicker form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div id="divApproverDropdown" runat="server">
                                        <label class="padding-none control-label custom_label_answer">Approver<span class="mandatoryStar">*</span></label>
                                        <div>
                                            <asp:DropDownList ID="ddlApprover" data-live-search="true" data-size="6" CssClass="regulatory_dropdown_style selectpicker form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div>
                                        <label class="padding-none control-label custom_label_answer">Reason<span class="mandatoryStar">*</span></label>
                                        <textarea id="txtRemarks" cols="20" class="form-control" rows="3" runat="server" placeholder="Enter Reason"></textarea>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel ID="upbtnEmpSubmit" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Button ID="btnEmployeesSubmit" CssClass=" btn-signup_popup" runat="server" Text="Submit" OnClientClick="return SubmitValidation();" OnClick="btnEmployeesSubmit_Click" />
                            <button type="button" class=" btn-cancel_popup" data-dismiss="modal">Cancel</button>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div id="divButtons" style="display: none">
        <asp:UpdatePanel ID="upFormButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnBindEmployees" runat="server" Text="Button" OnClick="btnBindEmployees_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <!--Create Tables by Dropdown Type Selection-->
    <script>
        $(".ddlTypeClass").change(function () {
            LoadMAE_Datatable();
        });
    </script>

    <script>
        function LoadMAE_Datatable() {
            var ddlSelected = $('.ddlTypeClass option:selected').val();
            var tblRows = $('#tblTypeEmployees tr').length;
            if (tblRows > 0) {
                if (otblTypeEmployees != null) {
                    otblTypeEmployees.destroy();
                }
                $('#tblTypeEmployees').empty();
            }
            var tableHeaderMarkUp = "";
            if (ddlSelected == "1") {
                tableHeaderMarkUp = "<thead><tr>" +
                    "<th>JR_ID</th>" +
                    "<th>DeptID</th>" +
                    "<th>StatusID</th>" +
                    "<th>TraineeID</th>" +
                    "<th>AuthorID</th>" +
                    "<th>ReviewerID</th>" +
                    "<th>JR Name</th>" +
                    "<th>Version</th>" +
                    "<th>Department</th>" +
                    "<th>Trainee</th>" +
                    "<th>Author</th>" +
                    "<th>Reviewer</th>" +
                    "<th>Status</th>" +
                    "<th>View</th>" +
                    "</tr></thead>";
                $("#tblTypeEmployees").append(tableHeaderMarkUp);
                loadDatatableJS(1);
            }
            else if (ddlSelected == "2") {
                tableHeaderMarkUp = "<thead><tr>" +
                    "<th>JrTS_ID</th>" +
                    "<th>DeptID</th>" +
                    "<th>StatusID</th>" +
                    "<th>TraineeID</th>" +
                    "<th>AuthorID</th>" +
                    "<th>ReviewerID</th>" +
                    "<th>JR Name</th>" +
                    "<th>Version</th>" +
                    "<th>Department</th>" +
                    "<th>Trainee</th>" +
                    "<th>Author</th>" +
                    "<th>Reviewer</th>" +
                    "<th>Status</th>" +
                    "<th>View</th>" +
                    "</tr></thead>";
                $("#tblTypeEmployees").append(tableHeaderMarkUp);
                loadDatatableJS(2);
            }
            else if (ddlSelected == "3") {
                tableHeaderMarkUp = "<thead><tr>" +
                    "<th>JR_ID</th>" +
                    "<th>DeptID</th>" +
                    "<th>HOD_EmpID</th>" +
                    "<th>JR_Name</th>" +
                    "<th>Version</th>" +
                    "<th>Department</th>" +
                    "<th>Trainee</th>" +
                    "<th>Evaluator[HOD]</th>" +
                    "<th>View</th>" +
                    "</tr></thead>";
                $("#tblTypeEmployees").append(tableHeaderMarkUp);
                loadDatatableJS(3);
            }
            else if (ddlSelected == "4") {
                tableHeaderMarkUp = "<thead><tr>" +
                    "<th>TTS_ID</th>" +
                    "<th>DeptID</th>" +
                    "<th>StatusID</th>" +
                    "<th>AuthorID</th>" +
                    "<th>ReviewerID</th>" +
                    "<th>Document</th>" +
                    "<th>Department</th>" +
                    "<th>Target Type</th>" +
                    "<th>Author</th>" +
                    "<th>Reviewer</th>" +
                    "<th>Status</th>" +
                    "<th>View</th>" +
                    "</tr></thead>";
                $("#tblTypeEmployees").append(tableHeaderMarkUp);
                loadDatatableJS(4);
            }
        }
    </script>

    <!--Load jQuery Datatable Script-->
    <script>
        var otblTypeEmployees;
        function loadDatatableJS(forType) {
            var cols = [];
            var serviceUrl;
            var colDefs;
            if (forType == 1) {
                cols = [
                    { 'data': 'JR_ID' },
                    { 'data': 'DeptID' },
                    { 'data': 'StatusID' },
                    { 'data': 'TraineeID' },
                    { 'data': 'AuthorID' },
                    { 'data': 'ReviewerID' },
                    { 'data': 'JR_Name' },
                    { 'data': 'Version' },
                    { 'data': 'Department' },
                    { 'data': 'Trainee' },
                    { 'data': 'Author' },
                    { 'data': 'Reviewer' },
                    { 'data': 'Status' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            return '<a class="view" href="#" onclick="setupModelOpen(' + o.JR_ID + ',' +
                                o.DeptID + ',' + o.StatusID + ',' + o.TraineeID + ',' + o.AuthorID + ',' + o.ReviewerID + ',0,1);"></a>';
                        }
                    }
                ];
                colDefs = [{ "targets": [0, 1, 2, 3, 4, 5], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [8, 9, 10, 11, 12] }];
                serviceUrl = "<%=ResolveUrl("~/TMS/WebServices/ModifyActionEmployees.asmx/JrActionEmps")%>";
            }
            else if (forType == 2) {
                cols = [
                    { 'data': 'JrTS_ID' },
                    { 'data': 'DeptID' },
                    { 'data': 'StatusID' },
                    { 'data': 'TraineeID' },
                    { 'data': 'AuthorID' },
                    { 'data': 'ReviewerID' },
                    { 'data': 'JR_Name' },
                    { 'data': 'Version' },
                    { 'data': 'Department' },
                    { 'data': 'Trainee' },
                    { 'data': 'Author' },
                    { 'data': 'Reviewer' },
                    { 'data': 'Status' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            if (o.StatusID == "0") {
                                return '<a class="view" href="#" onclick="setupModelOpen(' + o.JrTS_ID + ',' + o.DeptID + ',' + o.StatusID + ',' + o.TraineeID + ',' + o.AuthorID + ',0,0,2);"></a>';
                            }
                            else {
                                return '<a class="view" href="#" onclick="setupModelOpen(' + o.JrTS_ID + ',' + o.DeptID + ',' + o.StatusID + ',' + o.TraineeID + ',' + o.AuthorID + ',' + o.ReviewerID + ',0,2);"></a>';
                            }

                        }
                    }
                ];
                colDefs = [{ "targets": [0, 1, 2, 3, 4, 5], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [6, 8, 9, 10, 11, 12] }];
                serviceUrl = "<%=ResolveUrl("~/TMS/WebServices/ModifyActionEmployees.asmx/JrTsActionEmps")%>";
            }
            else if (forType == 3) {
                cols = [
                    { 'data': 'JR_ID' },
                    { 'data': 'DeptID' },
                    { 'data': 'EvaluatroHodEmpID' },
                    { 'data': 'JR_Name' },
                    { 'data': 'JR_Version' },
                    { 'data': 'Department' },
                    { 'data': 'TraineeName' },
                    { 'data': 'EvaluatorHodName' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            return '<a class="view" href="#" onclick="setupModelOpen(' + o.JR_ID + ',' + o.DeptID + ',0,0,' + o.EvaluatroHodEmpID + ',0,0,3);"></a>';
                        }
                    }
                ];
                colDefs = [{ "targets": [0, 1, 2], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [3, 5, 6, 7] }];
                serviceUrl = "<%=ResolveUrl("~/TMS/WebServices/ModifyActionEmployees.asmx/JrEvaluator")%>";
            }
            else if (forType == 4) {
                cols = [
                    { 'data': 'TTS_ID' },
                    { 'data': 'DeptID' },
                    { 'data': 'StatusID' },
                    { 'data': 'AuthorID' },
                    { 'data': 'ReviewerID' },
                    { 'data': 'Document' },
                    { 'data': 'Department' },
                    { 'data': 'TargetType' },
                    { 'data': 'Author' },
                    { 'data': 'Reviewer' },
                    { 'data': 'Status' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) { return '<a class="view" href="#" onclick="setupModelOpen(' + o.TTS_ID + ',' + o.DeptID + ',' + o.StatusID + ',0,' + o.AuthorID + ',' + o.ReviewerID + ',0,4);"></a>'; }
                    }
                ];
                colDefs = [{ "targets": [0, 1, 2, 3, 4], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [5, 6, 7, 8, 9, 10] }];
                serviceUrl = "<%=ResolveUrl("~/TMS/WebServices/ModifyActionEmployees.asmx/TargetTsActionEmps")%>";
            }

            otblTypeEmployees = $('#tblTypeEmployees').DataTable({
                columns: cols,
                "aoColumnDefs": colDefs,
                "sAjaxSource": serviceUrl,
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
    </script>

    <!--For Modal PopUp-->
    <script>
        function CustomAlertmsginMAE(msg, msgType) {
            if (msgType == "success") {
                CloseActionEmpPopup();
                LoadMAE_Datatable();
            }
            custAlertMsg(msg, msgType);
        }
        function CloseActionEmpPopup() {
            $('#modalActionEmp').modal('hide');
        }
        function openActionEmpPopup() {
            if ($("#<%=ddlAuthor.ClientID%>").has('option').length > 0) {
                $("#<%=ddlAuthor.ClientID%> option:selected").removeAttr("selected");
                $("#<%=ddlAuthor.ClientID%> option[value='" + $("#<%=hdnAuthor.ClientID%>").val() + "']").attr('selected', 'selected');
            }
            if ($("#<%=ddlReviewer.ClientID%>").has('option').length > 0) {
                $("#<%=ddlReviewer.ClientID%> option:selected").removeAttr("selected");
                $("#<%=ddlReviewer.ClientID%> option[value='" + $("#<%=hdnReviewer.ClientID%>").val() + "']").attr('selected', 'selected');
            }
            if ($("#<%=ddlApprover.ClientID%>").has('option').length > 0) {
                $("#<%=ddlApprover.ClientID%> option:selected").removeAttr("selected");
                $("#<%=ddlApprover.ClientID%> option[value='" + $("#<%=hdnApprover.ClientID%>").val() + "']").attr('selected', 'selected');
            }
            $('#modalActionEmp').modal({ show: true, backdrop: 'static', keyboard: false });
        }

      
        function setupModelOpen(baseID, deptId, TTS_CurrentStatusID, traineeID, authorID, reviewerID, approverID, baseType) {
            $("#<%=hdnBaseID.ClientID%>").val(baseID);
            $("#<%=hdnDeptID.ClientID%>").val(deptId);
            $("#<%=hdnEmpBindStatus.ClientID%>").val(0);
            $("#<%=hdnTraineeID.ClientID%>").val(traineeID);
            $("#<%=hdnAuthor.ClientID%>").val(authorID);
            $("#<%=hdnReviewer.ClientID%>").val(reviewerID);
            $("#<%=hdnApprover.ClientID%>").val(approverID);
            $("#<%=txtRemarks.ClientID%>").val("");
            $("#<%=hdnTTS_CurrentStatusID.ClientID%>").val(TTS_CurrentStatusID);
            if (TTS_CurrentStatusID > "3") {
                $("#<%=hdnIsReviewerEnabled.ClientID%>").val(1);
            }
            else {
                $("#<%=hdnIsReviewerEnabled.ClientID%>").val(0);
            }
            if (baseType == 1) {
                $("#<%=hdnEmpBindStatus.ClientID%>").val(2);
            }
            else if (baseType == 2) {
                if (TTS_CurrentStatusID == "0") {
                    $("#<%=hdnEmpBindStatus.ClientID%>").val(1);
                }
                else {
                    $("#<%=hdnEmpBindStatus.ClientID%>").val(2);
                }
            }
            else if (baseType == 3) {
                $("#<%=hdnEmpBindStatus.ClientID%>").val(1);
            }
            else if (baseType == 4) {
                $("#<%=hdnEmpBindStatus.ClientID%>").val(2);
            }

            $("#<%=btnBindEmployees.ClientID%>").click();
        }
    </script>

    <!--Action Employees Selection-->
    <script>       
        function ddlAuthorChange() {
            $('#<%=ddlAuthor.ClientID%>').find('option').removeAttr('selected');
            var ddlSelectedVal = $('#<%=ddlAuthor.ClientID%> option:selected').val();
            $('#<%=ddlAuthor.ClientID%> option[value="' + ddlSelectedVal + '"]').attr("selected", true);
            var Author = $('#<%=ddlAuthor.ClientID%> option:selected').val();
            var Reviewer = $('#<%=ddlReviewer.ClientID%> option:selected').val();
            if (Author == Reviewer) {
                $('#<%=ddlReviewer.ClientID%>').find('option').removeAttr('selected');
                $('#<%=ddlReviewer.ClientID%> option[value="0"]').attr("selected", true);
                $('#<%=ddlReviewer.ClientID%>').selectpicker('val', '0');
            }
        };
        function ddlReviewerChange() {
            $('#<%=ddlReviewer.ClientID%>').find('option').removeAttr('selected');
            var ddlSelectedVal = $('#<%=ddlReviewer.ClientID%> option:selected').val();
            $('#<%=ddlReviewer.ClientID%> option[value="' + ddlSelectedVal + '"]').attr("selected", true);
            var Reviewer = $('#<%=ddlReviewer.ClientID%> option:selected').val();
            var Author = $('#<%=ddlAuthor.ClientID%> option:selected').val();
            if (Author == Reviewer) {
                $('#<%=ddlAuthor.ClientID%>').find('option').removeAttr('selected');
                $('#<%=ddlAuthor.ClientID%> option[value="0"]').attr("selected", true);
                $('#<%=ddlAuthor.ClientID%>').selectpicker('val', '0');
            }
        };
    </script>

    <!--Validate Action Employee Submission-->
    <script>
        function SubmitValidation() {
            var EmpBindStatus = $("#<%=hdnEmpBindStatus.ClientID%>").val();
            var Author = document.getElementById('<%=ddlAuthor.ClientID%>');
            var Reviewer = document.getElementById('<%=ddlReviewer.ClientID%>');
            var Approver = document.getElementById('<%=ddlApprover.ClientID%>');
            var Remarks = document.getElementById('<%=txtRemarks.ClientID%>').value.trim();
            var IsReviewerEnable = $("#<%=hdnIsReviewerEnabled.ClientID%>").val();
            errors = [];
            var checkAuthor = 'N', checkReviewer = 'N', checkApprover = 'N';
            if (EmpBindStatus == 1 || EmpBindStatus == 2 || EmpBindStatus == 3) {
                checkAuthor = 'Y';
                if (Author.value == 0) {
                    errors.push("Select Author");
                }
            }
            if (EmpBindStatus == 2 || EmpBindStatus == 3) {
                checkReviewer = 'Y';
                if (Reviewer.value == 0 && IsReviewerEnable !="1" ) {
                    errors.push("Select Reviewer");
                }
            }
            if (EmpBindStatus == 3) {
                checkApprover = 'Y';
                if (Approver.value == 0) {
                    errors.push("Select Approver");
                }
            }
            if (Remarks == "") {
                errors.push("Enter Purpose of modification in Reason Field.");
            }
            var UpdateCount = 0;
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                if (Author.value != $("#<%=hdnAuthor.ClientID%>").val() && checkAuthor == 'Y') {
                    UpdateCount++;
                }
                if (Reviewer.value != $("#<%=hdnReviewer.ClientID%>").val() && checkReviewer == 'Y') {
                    UpdateCount++;
                }
                if (Approver.value != $("#<%=hdnApprover.ClientID%>").val() && checkApprover == 'Y') {
                    UpdateCount++;
                }
                if (UpdateCount > 0) {
                    ShowPleaseWait('Show');
                    return true;
                }
                else {
                    custAlertMsg("There is no changes to Modify.", "info");
                    return false;
                }
            }
        }
    </script>

    <script>
        function HideImg() {
            ShowPleaseWait('hide');
        }
    </script>
</asp:Content>
