﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.master" AutoEventWireup="true" CodeBehind="SecuritySettings.aspx.cs" Inherits="AizantIT_PharmaApp.UserManagement.SecuritySettings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<%--<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Aizant Login</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    --%>


    <link href="<%# ResolveUrl("~/Content/bootstrap.css")%>" rel="stylesheet" />
    <script src="<%# ResolveUrl("~/Scripts/jquery-3.2.1.min.js")%>"></script>
    <script src="<%# ResolveUrl("~/Scripts/moment.min.js")%>"></script>
    <link href="<%# ResolveUrl("~/Content/font-awesome.css")%>" rel="stylesheet" />
    <link href="<%# ResolveUrl("~/Content/font-awesome.min.css")%>" rel="stylesheet" />
    <script src="<%# ResolveUrl("~/Scripts/bootstrap.min.js")%>"></script>
    <script src="<%# ResolveUrl("~/AppScripts/GlobalScripts.js")%>"></script>
    <link href="<%# ResolveUrl("~/AppCSS/AppStyles.css")%>" rel="stylesheet" />
    <link rel="shortcut icon" href="<%# ResolveUrl("~/favicon.ico")%>" />
    <link href="<%# ResolveUrl("~/AppCSS/CustAlerts/custAlert.css")%>" rel="stylesheet" />
    <link href="<%# ResolveUrl("~/AppCSS/UserLogin/UserLoginStyles.css")%>" rel="stylesheet" />

<%--</head>
    --%>
<%--<body class="bg_login">
    <form id="myform" runat="server">
    <link href="<%=ResolveUrl("~/Content/font-awesome.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/AppCSS/Main_style.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/Scripts/jquery-ui.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/Content/bootstrap.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/AppCSS/VMS_CSS/AppStyles.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/Content/bootstrap-datetimepicker.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/Content/bootstrap-datetimepicker.min.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/Scripts/select2-4.0.3/dist/css/select2.min.css")%>" rel="stylesheet" />
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.2.1.min.js")%>"></script>
    <script src="<%=ResolveUrl("~/Scripts/bootstrap.min.js")%>"></script>
    <script src="<%=ResolveUrl("~/Scripts/moment.min.js")%>"></script>
    <script src="<%=ResolveUrl("~/Scripts/bootstrap-datetimepicker.js")%>"></script>
   
    <script src="<%=ResolveUrl("~/Scripts/bootstrap-datetimepicker.min.js")%>"></script>
    
    <script src="<%=ResolveUrl("~/Scripts/select2-4.0.3/dist/js/select2.min.js")%>"></script>     
    --%>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
   
    <asp:HiddenField ID="HFMyProEmpID" runat="server" />
    <asp:HiddenField ID="hdnQuestion" runat="server" />
    <asp:HiddenField ID="hdnAnswer" runat="server" />

     <!--------  Settings popup-------------->
			<%--<div id="Settings" class="modal department fade" role="dialog">--%>
			  <%--<div class="modal-dialog modal-lg">--%>
				<!-- Modal content-->
				<%--<div class="modal-content">	--%>
				  <%--<div class="modal-header">--%>
					<%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
					<h4 class="modal-title">Settings</h4>
				 <%-- </div>--%>
				  <%--<div class="modal-body">--%>
					<ul class="nav nav-tabs tab_grid">
					  <li class="active"><a data-toggle="tab" href="#home">Change Password</a></li>
					  <li><a data-toggle="tab" href="#menu1">Security Questions</a></li>
					  
					</ul>

					<div class="tab-content">
					  <div id="home" class="tab-pane fade in active">
					  <form class="form-horizontal top col-lg-offset-2">
						  <div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">Old Password <span class="smallred_label">*</span></label>
							<div class="col-sm-4">
							  <input type="password" runat="server" class="form-control" id="txtCurrentPwd" placeholder="Enter Old Password" autocomplete="off" onkeypress="return NoSpace(event)" maxlength="250"/>
							</div>
						  </div>
						   <div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">New Password <span class="smallred_label">*</span></label>
							<div class="col-sm-4">
							  <input type="password" runat="server" class="form-control" id="txtNewPassword" placeholder="Enter New Password" data-toggle="tooltip" data-placement="bottom" title=" Password must contain atleast 8 characters,one uppercase,one lowercase,one special character and one number." autocomplete="off" onkeypress="return NoSpace(event)" maxlength="250"/>
							</div>
						  </div>
						   <div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">Confirm Password <span class="smallred_label">*</span></label>
							<div class="col-sm-4">
							  <input type="password" runat="server" class="form-control" id="txtConfirmPassword" placeholder="Enter Confirm Password" data-toggle="tooltip" data-placement="bottom" title="Password and Confirm Password should be same" autocomplete="off" maxlength="250" onkeypress="return NoSpace(event);"/>
							</div>
						  </div>
						  <div class="top" style="text-align:right;">

					<%--<button type="button" class="btn btn-approve_popup   " data-dismiss="modal">Create</button>--%>
                    <asp:UpdatePanel ID="upbtnpwdCreatePopup" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <asp:Button ID="btnPwdSubmit" type="button" runat="server" CssClass="btn btn-approve_popup" data-dismiss="modal" Text="Submit" OnClientClick=" return  ValidateChangePassword();" OnClick="btnPwdSubmit_Click"/>
					    </ContentTemplate>
                    </asp:UpdatePanel>

                    <button id="btnCanclePwd" type="button" class="btn btn-canceldms_popup" runat="server" onclick="ModelPopupClear();">Cancel</button>
				  </div>
						 </form>
					  </div>
					  <div id="menu1" class="tab-pane fade">
						 <form class="form-horizontal top">
						<label for="inputEmail" class=" col-lg-12 padding-none   custom_label_answer pull-left">Security Questions <span class="smallred_label">*</span><span class="" style="float: right;">
                                                    <%--<a href="#">
                                                        <img id="btnEdit" src="../Images/GridActionImages/edit.png" alt="Edit">
                                                    </a>--%></span></label>
						<div class=" padding-none">
							<textarea class="form-control" rows="5" id="txtSecQuestion" runat="server"></textarea>
						</div>
						<label for="inputEmail3" class=" padding-none  control-label custom_label_answer">Security Answer <span class="smallred_label">*</span></label>
						<div class=" padding-none">
							<textarea class="form-control" rows="5" id="txtSecAnswer" runat="server"></textarea>
						</div>
						
						</form>
						
						<div class="top" style="text-align:right;">
					<%--<button type="button" class="btn btn-approve_popup   " data-dismiss="modal">Update</button>--%>
                    <asp:Button ID="btnSecUpdate" CssClass="btn btn-approve_popup" runat="server" Text="Update" OnClick="btnSecUpdate_Click" OnClientClick="javascript:return validateSecurityQandAns();"/>
					<button type="button" class="btn btn-canceldms_popup" data-dismiss="modal">Cancel</button>
				  </div>
						
					  </div>
					  <div id="menu2" class="tab-pane fade">
						<h3>Menu 2</h3>
						<p>Some content in menu 2.</p>
					  </div>
					</div>
					
				<%--  </div>
				  
				</div>

			  </div>
			</div>--%>
<!-------- End Settings-------------->


     <script>
        //This is for Validate Change password( Current and New and Confirm Password)
        function ValidateChangePassword() {
           <%-- var txtCurrentPwd = document.getElementById("<%=txtCurrentPwd.ClientID%>").value;--%>
            var txtCurrentPwd = document.getElementById("#<%=txtCurrentPwd.ClientID%>");
            var txtNewPwd = document.getElementById("<%=txtNewPassword.ClientID%>").value;
            var txtConfirmPwd = document.getElementById("<%=txtConfirmPassword.ClientID%>").value;
            var undefChar = /[\ _]/;
            var undefChars = /[\ _]/;
            errors = [];
            if (txtCurrentPwd == '') {
                errors.push("Enter Current Password")
            }
            if (txtNewPwd != "") {

                if ((txtNewPwd.length < 8) || (txtNewPwd.length > 15)) {
                    errors.push("Password must contain Minimum 8 characters");
                }
                if (txtNewPwd.search(/[a-z]/) < 0) {
                    errors.push("Your Password must contain atleast one Lower Case");
                }
                if (txtNewPwd.search(/[A-Z]/) < 0) {
                    errors.push("Your Password must contain atleast one Upper Case");
                }
                if (txtNewPwd.search(/[0-9]/) < 0) {
                    errors.push("Your Password must contain atleast one Number");
                }
                if (txtNewPwd.search(/[@#&*]/) < 0) {
                    errors.push("Password must contain atleast one @,#,&,* character");
                }
                if (txtCurrentPwd == txtNewPwd) {
                    errors.push("New Password should not be same as old password")
                }
            }
            if (txtNewPwd == '') {
                errors.push("Enter New Password");

            }
            if (txtConfirmPwd != "") {
                if (txtNewPwd != txtConfirmPwd) {
                    errors.push(" New Password and Confirm Password should match");
                }
            }
            if (txtConfirmPwd == '') {
                errors.push("Enter Confirm Password");
            }

            if (errors.length > 0) {
                document.getElementById('msgError').innerHTML = errors.join("<br/>");
                $('#ModalError').modal();
                $("#btnErrorOk").focus();
                return false;
            }
            else {
                return true;
            }
        }
    </script>

    <script>
        //This is for clearing the Fields
        function ModelPopupClear() {
            document.getElementById("<%=txtCurrentPwd.ClientID%>").value = "";
            document.getElementById("<%=txtNewPassword.ClientID%>").value = "";
            document.getElementById("<%=txtConfirmPassword.ClientID%>").value = "";
        }
    </script>

      <script type="text/javascript">
          //required fields for Security Question and Answer
          function validateSecurityQandAns() {
              var SecurityQuestion = document.getElementById("<%=txtSecQuestion.ClientID%>").value;
            var SecurityAnswer = document.getElementById("<%=txtSecAnswer.ClientID%>").value;
            errors = [];
            if (SecurityQuestion == '') {
                errors.push("Enter Security Question");
            }
            if (SecurityQuestion != '') {
                if (SecurityQuestion.length <= 3) {
                    errors.push("SecurityQuestion Should Be Atleast 4 Characters");
                }
            }
            if (SecurityAnswer == '') {
                errors.push("Enter Security Answer");
            }
            if (SecurityAnswer != '') {
                if (SecurityAnswer.length <= 1) {
                    errors.push("SecurityAnswer Should Be Atleast 2 Characters");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
          }
    </script>

    <script>
          function Reset() {
              document.getElementById("<%=txtNewPassword.ClientID%>").value = "";
                document.getElementById("<%=txtConfirmPassword.ClientID%>").value = "";
                document.getElementById("<%=txtCurrentPwd.ClientID%>").value = "";
          }
          //for clearing the signup page
          function ClearChangePwd() {
              Reset();
              $("#ChangePasswordwdModal").modal('hide');
          }
          function ClearChangePwd1() {
              Reset();

              $("#ChangePasswordwdModal").modal('show');
          }
    </script>

       <%-- </form>--%>
<%--</body>
</html>--%>
</asp:Content>