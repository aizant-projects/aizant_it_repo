﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyProfile1.aspx.cs" Inherits="AizantIT_PharmaApp.UserManagement.MyProfile1" %>

<%@ Register Src="~/UserControls/SessionTimeOut.ascx" TagPrefix="uc1" TagName="SessionTimeOut" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>

<body>
     <!--From MainMaster-->
    <link href="<%=ResolveUrl("~/Content/bootstrap.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/Content/font-awesome.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/Editor/site.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/src/richtext.min.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/Scripts/jquery-ui.css")%>" rel="stylesheet" />
        <script src="<%=ResolveUrl("~/Scripts/jquery-3.2.1.min.js")%>"></script>
        <script src="<%=ResolveUrl("~/Scripts/jquery-ui.js")%>"></script>
        <script src="<%=ResolveUrl("~/Scripts/bootstrap.min.js")%>"></script>
        <script src="<%=ResolveUrl("~/AppCSS/src/jquery.richtext.js")%>"></script>
        <link href="<%=ResolveUrl("~/Scripts/select2-4.0.3/dist/css/select2.min.css")%>" rel="stylesheet" />
        <script src="<%=ResolveUrl("~/Scripts/select2-4.0.3/dist/js/select2.min.js")%>"></script>
        <script src="<%=ResolveUrl("~/Scripts/moment.min.js")%>"></script>
        <script src="<%=ResolveUrl("~/Scripts/bootstrap-datetimepicker.min.js")%>"></script>
        <script src="<%=ResolveUrl("~/Scripts/select2-4.0.3/accordion.js")%>"></script>
        <link href="<%=ResolveUrl("~/AppCSS/AppStyles.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/LeftNaviStyle.css")%>" rel="stylesheet" />
        <script src="<%=ResolveUrl("~/AppScripts/GlobalScripts.js")%>"></script>
        <link href="<%=ResolveUrl("~/AppCSS/CustAlerts/custAlert.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/Main_style.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/Calender.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/UserLogin/UserLoginStyles.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/MyProfileStyles.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/Content/bootstrap-select.css")%>" rel="stylesheet" />
        <script src="<%=ResolveUrl("~/Scripts/bootstrap-select.min.js")%>"></script>
    <!--End Main Master-->



    <form id="form1" runat="server">
           <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:HiddenField ID="HFMyProEmpID" runat="server" />
    <asp:UpdatePanel ID="upQuestionnaire" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnQuestion" runat="server" />
            <asp:HiddenField ID="hdnAnswer" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class=" col-lg-12">
        <div class=" col-lg-12 outer_border_profile">


            <div class="col-lg-12 padding-none">

                <div class="header_profile col-lg-12">My Profile </div>


                <div class="col-lg-12  padding-none">
                    <div class="col-lg-12 userprofile_bg">
                        <div class="col-lg-2">
                            <asp:Image class="img-circle userprofile_img" ID="MyImage" runat="server" Height="192px" Width="190px" />
                        </div>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="col-lg-4">
                                    <div class="col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">Employee code </label>
                                        <div class="col-sm-8 padding-none profile_defined">
                                            <span class="semiColStyle">:</span><asp:Label ID="lblEmployeeCode" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class=" col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">First Name </label>
                                        <div class="col-sm-8 padding-none profile_defined">
                                            <span class="semiColStyle">:</span><asp:Label ID="lblFirstName" runat="server"></asp:Label>

                                        </div>
                                    </div>
                                    <div class=" col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">Last Name </label>
                                        <div class="col-sm-8 padding-none profile_defined">
                                            <span class="semiColStyle">:</span><asp:Label ID="lblLastName" runat="server"></asp:Label>

                                        </div>
                                    </div>
                                    <div class=" col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">Date of Birth </label>
                                        <div class="col-sm-8 padding-none profile_defined">
                                            <span class="semiColStyle">:</span><asp:Label ID="lblDob" runat="server"></asp:Label>

                                        </div>
                                    </div>
                                    <div class=" col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">Gender </label>
                                        <div class="col-sm-8 padding-none profile_defined">
                                            <span class="semiColStyle">:</span><asp:Label ID="lblGender" runat="server"></asp:Label>

                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">Email  </label>
                                        <div class="col-sm-8 padding-none profile_defined">
                                            <span class="semiColStyle">:</span><asp:Label ID="lblEmail" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">Blood Group  </label>
                                        <div class="col-sm-8 padding-none profile_defined">
                                            <span class="semiColStyle">:</span><asp:Label ID="lblBloodGroup" runat="server"></asp:Label>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-6" style="border-left: 2px solid #fff;">
                                    <div class="col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">Department </label>
                                        <div class="col-sm-8 padding-none profile_defined">

                                            <span class="semiColStyle">:</span><asp:Label ID="lblDeparment" runat="server"></asp:Label>

                                        </div>
                                    </div>
                                    <div class=" col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">Designation </label>
                                        <div class="col-sm-8 padding-none profile_defined">


                                            <span class="semiColStyle">:</span><asp:Label ID="lblDesignation" runat="server"></asp:Label>


                                        </div>
                                    </div>
                                    <div class=" col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">Start Date </label>
                                        <div class="col-sm-8 padding-none profile_defined">
                                            <span class="semiColStyle">:</span><asp:Label ID="lblStartDate" runat="server" Text="NA"></asp:Label>
                                        </div>
                                    </div>
                                    <div class=" col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">Mobile </label>
                                        <div class="col-sm-8 padding-none profile_defined">
                                            <span class="semiColStyle">:</span><asp:Label ID="lblMobile" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class=" col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">State</label>
                                        <div class="col-sm-8 padding-none profile_defined">
                                            <span class="semiColStyle">:</span><asp:Label ID="lblState" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">Country</label>
                                        <div class="col-sm-8 padding-none profile_defined">
                                            <span class="semiColStyle">:</span>
                                            <asp:Label ID="lblCountry" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label for="inputEmail3" class=" padding-none profile_label col-sm-4">Address</label>
                                        <div class="col-sm-8 padding-none profile_defined">
                                            <span class="semiColStyle">:</span>
                                            <asp:Label runat="server" ID="txtAddress"></asp:Label>
                                        </div>
                                    </div>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="col-lg-12 padding-none top">
                        <div class="col-lg-12 padding-right_div">
                            <div class="header_profile col-lg-12">Roles</div>
                            <div class="col-lg-12 panel_full_profile">
                                <asp:UpdatePanel runat="server" ID="upModule" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-group padding-none col-lg-12 top pull-left">
                                            <label>Modules</label>
                                            <asp:DropDownList ID="ddlModulesList" runat="server" AutoPostBack="true" Style="margin-bottom: 2%; width: 290px" CssClass="form-control" Font-Bold="true" OnSelectedIndexChanged="ddlModule_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                        <!---Assigned Roles----->
                                        <div class="form-group padding-right_div col-lg-6  pull-left">
                                            <label>Assigned Roles</label>
                                            <div class="col-lg-12 top pull-left padding-none assigned_roles_list">
                                                <asp:DataList ID="DataListRoles" runat="server" Width="100%">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "RoleName") %>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>

                                        </div>
                                        <div class="form-group padding-none col-lg-6  pull-left">
                                            <label>Accessible Department</label>
                                            <div class="col-lg-12 top pull-left padding-none assigned_roles_list">
                                                <asp:DataList ID="DataListDept" runat="server" Width="100%">
                                                    <ItemTemplate>

                                                        <%# DataBinder.Eval(Container.DataItem, "DepartmentName") %>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlModulesList" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                       <%-- <div class="col-lg-6 padding-left_div">
                            <div class="header_profile col-lg-12">Security Settings</div>
                            <div class="col-lg-12 panel_full_profile">
                                <div class=" pull-right col-lg-12 top padding-none" style="margin-top: 6px">
                                    <asp:UpdatePanel ID="upOpenChangePwd" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnOpenChangePwd" runat="server" type="button" Text="Change Password" data-toggle="modal" data-target="#ChangePasswordwdModal" CssClass="btn btn-dashboard pull-right" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>
                                <asp:UpdatePanel runat="server" ID="upSecuritytab" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class=" col-lg-12 padding-none">
                                            <label class=" col-sm-12  padding-none ">
                                                Security Questions<span class="smallred">*</span> <span class="top" style="float: right;">
                                                    <a href="#">
                                                        <img id="btnEdit" src="../images/profile/edit.png" onclick="SecurityEnableDisable();" alt="Edit" />
                                                    </a></span>
                                            </label>
                                            <div class="col-sm-12 padding-none">
                                                <asp:TextBox ID="txtSecQuestion" runat="server" CssClass="form-control" Enabled="false" TextMode="MultiLine" Rows="3" MaxLength="250" onkeypress="return NoSpaceAllowUserQue(event);"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="col-lg-12  padding-none">
                                            <label class=" col-sm-12  padding-none">Security Answer<span class="smallred">*</span></label>
                                            <div class="col-sm-12 padding-none">
                                                <asp:TextBox ID="txtSecAnswer" CssClass="form-control" Enabled="false" MaxLength="20" runat="server" Height="40" onpaste="return false" onkeypress="return NoSpaceAllowUserAns(event);"></asp:TextBox>

                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class=" pull-right col-lg-12 padding-none" style="margin-top: 6px">
                                    <asp:UpdatePanel ID="upButtons" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="pull-right col-lg-12 padding-none">
                                                <%--<input type="button" id="btnEdit" value="Edit" onclick="SecurityEnableDisable();" class="btn-signup_popup" />--%>
                                              <%--  <asp:Button ID="btnCancel" runat="server" type="button" Style="visibility: hidden" OnClick="btnCancel_Click" Text="Cancel" class="pull-right btn btn-cancel_popup" />
                                                <asp:Button ID="btnUpdate" runat="server" Text="Update" Style="visibility: hidden" CssClass="  btn-signup_popup pull-right" OnClientClick="javascript:return validateSecurityQandAns();" OnClick="btnUpdate_Click" />

                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                            </div>

                        </div>--%>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- Modal popup For Change Password-->
    <div class="modal fade" id="ChangePasswordwdModal" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header Panel_Header" style="padding: 8px">
                    <button type="button" class="close" data-dismiss="modal" onclick="ModelPopupClear();">&times;</button>
                    <h4 class="modal-title" style="color: aliceblue">Change Password</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-left: 10px; margin-right: 10px">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="div1" runat="server" visible="true">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Current Password<span class="smallred">*</span></label>
                                        <div class="col-sm-8" style="margin-bottom: 5px">
                                            <input type="password" class="form-control" id="txtCurrentPwd" runat="server" placeholder="Enter New Password" autocomplete="off" onkeypress="return NoSpace(event)" maxlength="250" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">New Password<span class="smallred">*</span></label>
                                        <div class="col-sm-8" style="margin-bottom: 5px">
                                            <input type="password" class="form-control" id="txtNewPassword" runat="server" placeholder="Enter New Password" data-toggle="tooltip" data-placement="bottom" title=" Password must contain atleast 8 characters,one uppercase,one lowercase,one special character and one number." autocomplete="off" onkeypress="return NoSpace(event)" maxlength="250" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Confirm Password<span class="smallred">*</span></label>
                                        <div class="col-sm-8" style="margin-bottom: 5px">
                                            <input type="password" class="form-control" id="txtConfirmPassword" runat="server" placeholder="Enter Confirm Password" data-toggle="tooltip" data-placement="bottom" title="Password and Confirm Password should be same" autocomplete="off" maxlength="250" onkeypress="return NoSpace(event);" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="modal-footer" style="padding-bottom: 2px; padding-top: 2px; margin-top: 5px; text-align: right">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional" style="display: inline-block">
                        <ContentTemplate>
                            <asp:Button ID="btnSubmit" type="button" Class="btn-signup_popup" runat="server" Text="Submit" OnClientClick=" return  ValidateChangePassword();" OnClick="btnSubmit_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" data-dismiss="modal" id="btnCanclePwd" runat="server" class="btn-cancel_popup" onclick="ModelPopupClear();">Cancel</button>
                </div>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Always" style="padding: 5px">
                    <ContentTemplate>
                        <asp:Label ID="Label1" runat="server" Text="" ForeColor="Red"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <uc1:SessionTimeOut runat="server" ID="SessionTimeOut" />

    <script type="text/javascript">
        //This is for Security Question and Answer Enable/Disable.
        <%--function SecurityEnableDisable() {
            document.getElementById('<%=txtSecAnswer.ClientID%>').removeAttribute("disabled");
            document.getElementById('<%=txtSecQuestion.ClientID%>').removeAttribute("disabled");
            document.getElementById('<%=btnUpdate.ClientID%>').style.visibility = "visible";
            document.getElementById('<%=btnCancel.ClientID%>').style.visibility = "visible";
            document.getElementById("btnEdit").style.display = "none";
        }--%>
    </script>

    <script type="text/javascript">
        //This is for Hide/Show Model Popup (hide:CurrentPwdModel) (Show:ChangePwdModel)
        function OpenModal() {
            $('#ChangePwdModal').modal('show');
            $('#CurrentPwdModal').modal('hide');
        }
    </script>
    <script type="text/javascript">
        //This is for Hide/Show Model Popup (hide:ChangePwdModel) (Show:ChangePwdModel)
        function ChangePwdShow() {
            $('#ChangePwdModal').modal('show');
        }
    </script>
    <script type="text/javascript">
            //This is for Hide/Show Model Popup (hide:ChangePwdModel) (Show:ChangePwdModel)
            function ChangePwdHide() {
                $('#ChangePwdModal').modal('hide');
            }
    </script>


    <script type="text/javascript">
        //required fields for Security Question and Answer
       <%-- function validateSecurityQandAns() {
            var SecurityQuestion = document.getElementById("<%=txtSecQuestion.ClientID%>").value;
            var SecurityAnswer = document.getElementById("<%=txtSecAnswer.ClientID%>").value;
            errors = [];
            if (SecurityQuestion == '') {
                errors.push("Enter Security Question");
            }
            if (SecurityQuestion != '') {
                if (SecurityQuestion.length <= 3) {
                    errors.push("SecurityQuestion Should Be Atleast 4 Characters");
                }
            }
            if (SecurityAnswer == '') {
                errors.push("Enter Security Answer");
            }
            if (SecurityAnswer != '') {
                if (SecurityAnswer.length <= 1) {
                    errors.push("SecurityAnswer Should Be Atleast 2 Characters");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }--%>
    </script>
    <script>
        //This is for Validate Change password( Current and New and Confirm Password)
       <%-- function ValidateChangePassword() {
            var txtCurrentPwd = document.getElementById("<%=txtCurrentPwd.ClientID%>").value;
            var txtNewPwd = document.getElementById("<%=txtNewPassword.ClientID%>").value;
            var txtConfirmPwd = document.getElementById("<%=txtConfirmPassword.ClientID%>").value;
            var undefChar = /[\ _]/;
            var undefChars = /[\ _]/;
            errors = [];
            if (txtCurrentPwd == '') {
                errors.push("Enter Current Password")
            }
            if (txtNewPwd != "") {

                if ((txtNewPwd.length < 8) || (txtNewPwd.length > 15)) {
                    errors.push("Password must contain Minimum 8 characters");
                }
                if (txtNewPwd.search(/[a-z]/) < 0) {
                    errors.push("Your Password must contain atleast one Lower Case");
                }
                if (txtNewPwd.search(/[A-Z]/) < 0) {
                    errors.push("Your Password must contain atleast one Upper Case");
                }
                if (txtNewPwd.search(/[0-9]/) < 0) {
                    errors.push("Your Password must contain atleast one Number");
                }
                if (txtNewPwd.search(/[@#&*]/) < 0) {
                    errors.push("Password must contain atleast one @,#,&,* character");
                }
                if (txtCurrentPwd == txtNewPwd) {
                    errors.push("New Password should not be same as old password")
                }
            }
            if (txtNewPwd == '') {
                errors.push("Enter New Password");

            }
            if (txtConfirmPwd != "") {
                if (txtNewPwd != txtConfirmPwd) {
                    errors.push(" New Password and Confirm Password should match");
                }
            }
            if (txtConfirmPwd == '') {
                errors.push("Enter Confirm Password");
            }

            if (errors.length > 0) {
                document.getElementById('msgError').innerHTML = errors.join("<br/>");
                $('#ModalError').modal();
                $("#btnErrorOk").focus();
                return false;
            }
            else {
                return true;
            }
        }--%>
    </script>

    <script>
       <%-- $(document).keydown(function (event) {
            if (event.keyCode == 27) {
                var clickButton = document.getElementById("<%=btnCancel.ClientID %>");
                var clickButton = document.getElementById("<%=btnCanclePwd.ClientID %>");
                clickButton.click();
            }
        });--%>
    </script>


    <script>
        //This is for clearing the Model Popup Fields
        function ModelPopupClear() {
            document.getElementById("<%=txtCurrentPwd.ClientID%>").value = "";
                document.getElementById("<%=txtNewPassword.ClientID%>").value = "";
                document.getElementById("<%=txtConfirmPassword.ClientID%>").value = "";
        }
    </script>

    <script>
            function Reset() {
                document.getElementById("<%=txtNewPassword.ClientID%>").value = "";
            document.getElementById("<%=txtConfirmPassword.ClientID%>").value = "";
            document.getElementById("<%=txtCurrentPwd.ClientID%>").value = "";
            }
            //for clearing the signup page
            function ClearChangePwd() {
                Reset();
                $("#ChangePasswordwdModal").modal('hide');
            }
            function ClearChangePwd1() {
                Reset();

                $("#ChangePasswordwdModal").modal('show');
            }
    </script>

    <script type="text/javascript">
        //count for Security Question
        function characterCounter(controlId, countControlId, maxCharlimit) {

            if (controlId.value.length > maxCharlimit)
                controlId.value = controlId.value.substring(0, maxCharlimit);
            else
                countControlId.value = maxCharlimit - controlId.value.length;
        }

    </script>

       
    </form>
</body>
</html>
