﻿<%@ Page Title="Emp Training Records" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="EmpTrainingRecords.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Reports.EmpTrainingRecords" %>

<%@ Register Src="~/UserControls/TMS_UserControls/ucFeedback.ascx" TagPrefix="uc1" TagName="ucFeedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:HiddenField ID="hdnEmpFeedbackID" runat="server" Value="0" />
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="col-12 padding-none" id="Div3" runat="server">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">
                    <asp:Label ID="lblEmpTrainingRecordsTitle" runat="server" Font-Bold="true" Text="Employee Training Records"></asp:Label>
                </div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  padding-none top">
                    <asp:UpdatePanel ID="upDropdowns" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="float-left col-lg-4 col-sm-12 col-12 col-md-4 padding-none">
                                <label class="float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label">Department<span class="mandatoryStar">*</span></label>
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                    <asp:DropDownList ID="ddlAccesibleDepartments" runat="server" CssClass="regulatory_dropdown_style  selectpicker form-control"
                                        data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlAccesibleDepartments_SelectedIndexChanged"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="float-left col-lg-4 col-sm-12 col-12 col-md-4 padding-none">
                                <label class="float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label">Employee<span class="mandatoryStar">*</span></label>
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                    <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="regulatory_dropdown_style  selectpicker form-control" data-live-search="true" data-size="7">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div id="divDuration" style="display: none;" class="float-left col-lg-4 col-sm-12 col-12 col-md-4 padding-none">
                               <label class="float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label"> Trained Duration</label>
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                            <input id="lblTotalDuration" class="login_input_sign_up form-control float-left col-lg-12 col-sm-12 col-12 col-md-12" disabled></input>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none" style="overflow: auto; margin-top: 10px">
                    <table id="tblEmpTrainingRecords" class="datatable_cust tblEmpTrainingRecordsClass breakword display" style="width: 100%">
                        <thead>
                            <tr>
                                <th>DocumentID</th>
                                <th>Document</th>
                                <th>Ref.No</th>
                                <th>Department</th>
                                <th>Mode of Training</th>
                                <th>Opinion</th>
                                <th>Evaluated By</th>
                                <th>Evaluated Date</th>
                                <th>Training Type</th>
                                <th>Read Duration</th>
                                <th>View FF</th>
                            </tr>
                        </thead>
                    </table>
                    <div class="col-12 grid_legend_tms">'FF' : Feedback Form ; 'NS' : Not Satisfactory and 'S' :  Satisfactory</div>
                </div>
            </div>
        </div>
    </div>
    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upServerButtons" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:Button ID="btnViewFeedBack" runat="server" Text="Button" OnClick="btnViewFeedBack_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <uc1:ucFeedback runat="server" ID="ucFeedback" />

    <script>
        $(document).ready(function () {
            empSelection();
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            empSelection();
        });

        function empSelection() {
            $('#<%=ddlEmployee.ClientID%>').on('change', function () {
                ReloadTableEmpTrainingRecords();
            });
        }
    </script>

    <!--Employee Training Records Datatable-->
    <script>
    $('#tblEmpTrainingRecords thead tr').clone(true).appendTo('#tblEmpTrainingRecords thead');
    $('#tblEmpTrainingRecords thead tr:eq(1) th').each(function (i) {
        if (i < 10) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search " />');

            $('input', this).on('keyup change', function () {
                if (oTableEmpTrainingRecords.column(i).search() !== this.value) {
                    oTableEmpTrainingRecords
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
            if (i == 0)
            {
                $(this).html('');
            }
        }
        else {
            $(this).html('');
        }
    });
        var oTableEmpTrainingRecords = $('#tblEmpTrainingRecords').DataTable(
            {
                "bServerSide": false,
                "aoColumnDefs": [{ "targets": [0], "visible": false }]
        });
        $(function (){
            loadEmpTrainingRecords();
        });
        function loadEmpTrainingRecords()
        {
            if (oTableEmpTrainingRecords != null)
            {
                oTableEmpTrainingRecords.destroy();
            }

          oTableEmpTrainingRecords = $('#tblEmpTrainingRecords').DataTable({
                columns: [
                    { 'data': 'DocumentID' },
                    { 'data': 'DocumentName' },
                    { 'data': 'DocRefNo' },
                    { 'data': 'DeptName' },
                    { 'data': 'ModeOfTraining' },
                    { 'data': 'Opinion' },
                    { 'data': 'EvaluatedBy' },
                    { 'data': 'EvaluatedDate' },
                    { 'data': 'TrainingType' },
                    { 'data': 'ReadDuration' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            if (o.EmpFeedbackID == 0) {
                                return 'N/A';
                            }
                            else {
                                return '<a class="feedback_tms"  href="#" title="Feedback" onclick="ViewEmp_Feedback(' + o.EmpFeedbackID + ');"></a>';
                            }
                        }
                    }
                ],
                "orderCellsTop": true,
                "order": [[7, "desc"]],
                "bServerSide": true,
                "aoColumnDefs": [{ "targets": [0], "visible": false },{ className: "textAlignLeft", "targets": [1, 2, 3, 4, 5, 6, 8, 9] },{ "bSortable": false, "aTargets": [ 9 ] }
                ],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/EmpTrainingRecords_ListService.asmx/GetEmpTrainingRecordsDataTable")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "EmpID", "value": $('#<%=ddlEmployee.ClientID%>').val() });//Passing Trainee ID
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            var totalDuration = json["iTotalDuration"];
                            var TotalRecords = json["iTotalRecords"];
                            if (TotalRecords > 0) {
                                $('#lblTotalDuration').val(totalDuration);
                                $('#divDuration').show();
                            }
                            else {
                                $('#divDuration').hide();
                            }
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
        //To Reload Datatable
        function ReloadTableEmpTrainingRecords() {
            loadEmpTrainingRecords();
        }
    </script>
    <script>
        function ViewEmp_Feedback(EmpFeedbackID) {
            $('#<%=hdnEmpFeedbackID.ClientID%>').val(EmpFeedbackID);
            $("#<%=btnViewFeedBack.ClientID%>").click();
        }
    </script>
</asp:Content>
