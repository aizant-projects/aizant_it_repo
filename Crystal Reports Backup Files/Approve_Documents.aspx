﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Approve_Documents.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentApprover.Approve_Documents" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%=ResolveUrl("~/AppCSS/DMS/DMS_StyleSheet1.css")%>" rel="stylesheet" />
    <style>
        table tbody tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            height: 30px;
        }

        table tbody tr td {
            height: 30px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            background-color: #07889a !important;
        }

        .datalistcss {
            overflow-x: hidden;
            height: 180px;
            border-color: #CCCCCC;
            border-width: 1px;
            border-style: Solid;
            border-collapse: collapse;
            padding: 5px;
            border-radius: 5px;
        }

        .header_profile {
            background: #07889a;
            padding: 10px;
            color: #fff;
            font-size: 12pt;
            font-family: seguisb;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 padding-none float-left">
        <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 dms_outer_border float-left padding-none">
            <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div id="divMainContainer" runat="server" visible="true">
                <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 padding-none float-right ">
                    <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 float-left">Approve Documents</div>
                        <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                            <div class="col-12 float-left padding-none top">
                            <table id="dtDates" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Version ID</th>
                                        <th>ID</th>
                                        <th>Document Number</th>
                                        <th>Document Name</th>
                                        <th>Department</th>
                                        <th>Document Type</th>
                                        <th>Initiated By</th>
                                        <th>Created By</th>
                                        <th>Request Type</th>
                                        <th>authorCount</th>
                                        <th>Action</th>
                                        <th>vDocumentID</th>
                                        <th>Training</th>
                                        <th>History</th>
                                    </tr>
                                </thead>
                            </table>
                                </div>
                            <div class=" col-lg-12 float-left bottom">
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/approve_training.png")%>" />
                                <b class="grid_icon_legend">Under Target Training </b>
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/approve.png")%>" />
                                <b class="grid_icon_legend">Action</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class=" modal-dialog modal-lg" style="min-width: 85%">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <asp:UpdatePanel runat="server" UpdateMode="Always">
                        <ContentTemplate>                           
                            <span id="span1" runat="server"></span>                         
                        </ContentTemplate>
                    </asp:UpdatePanel>
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                    <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div align="center" class="col-lg-12 history_grid">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("GivenBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role"  ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("CommentedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Time Spent">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Time" runat="server" Text='<%# Eval("Time") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action"  ItemStyle-HorizontalAlign="Left" ItemStyle-Width="22%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments"  ItemStyle-HorizontalAlign="Left" ItemStyle-Width="26%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>' ></asp:Label>
                                                <asp:TextBox ID="txt_Comments" class="col-12" runat="server" ReadOnly="true" Rows="4" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM" >Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL" >Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned To">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->
    <asp:HiddenField ID="hdfPKID" runat="server" />
    <asp:HiddenField ID="hdfPID" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRequestType" runat="server" Value="0" />
    <asp:UpdatePanel runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" OnClientClick="ShowPleaseWait('show');"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        $('#dtDates thead tr').clone(true).appendTo('#dtDates thead');
        $('#dtDates thead tr:eq(1) th').each(function (i) {
            if (i < 10) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (tblReload.column(i).search() !== this.value) {
                        tblReload
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDates').wrap('<div class="dataTables_scroll" />');
        var tblReload = $('#dtDates').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'VersionID' },
                { 'data': 'Id' },
                { 'data': 'DocumentNumber' },
                { 'data': 'DocumentName' },
                { 'data': 'Department' },
                { 'data': 'DocumentType' },
                { 'data': 'Initiator' },
                { 'data': 'Creator' },
                { 'data': 'Request Type' },
                { 'data': 'authorCount' },
                { 'data': 'TrainingCount' },
                { 'data': 'vDocumentID' },
                { 'data': 'IsTraining' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History" data-toggle="modal" data-target="#myCommentHistory"  onclick=ViewCommentHistory(' + o.VersionID + ')></a>'; }
                }
            ],
            "aoColumnDefs": [{ "targets": [0,1, 7, 10, 12, 13], "visible": false, "searchable": false }, { "targets": [1, 2], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 1, 11] }, { "className": "dt-body-left", "targets": [3, 4, 5, 6, 8, 9] },
            {
                targets: [9], render: function (a, b, data, d) {
                    if (data.RequestType == "New Document") {
                        return 'New';
                    } else if (data.RequestType == "Revision Of Existing Document") {
                        return 'Revision';
                    }
                    else if (data.RequestType == "Withdrawal Document") {
                        return 'Withdraw';
                    }
                    return "N/A";
                }
            },
            {
                targets: [11], render: function (a, b, data, d) {
                    if (data.authorCount == 0 && data.TrainingCount != true && data.IsTraining == true) {
                        return '<a class="approve_training" title="Under Target Training" href=/DMS/DocumentApprover/ApproveDocuments.aspx?VersionID=' + data.VersionID + '&PID=' + data.Id + '&Training=1' + '&DocumentID=' + data.vDocumentID + '>' + '' + '</a>';
                    } else {
                        return '<a class="view_req" title="Action" href=/DMS/DocumentApprover/ApproveDocuments.aspx?VersionID=' + data.VersionID + '&PID=' + data.Id + '&Training=0' + '&DocumentID=' + data.vDocumentID + '>' + '' + '</a>';
                    }
                    return "N/A";
                }
            },],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetApproveDocList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "ReqTypeID", "value": <%=hdnRequestType.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDates").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
        function reloadtable() {
            tblReload.ajax.reload();
            $('#usES_Modal').modal('hide');
            $("#myDocDetails").modal('hide');
            $("#myReview").modal('hide');
        }
        $('.test').hide()
    </script>
    <script>
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfPKID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
    </script>
    <script>
        function hideImg() {
            ShowPleaseWait('hide');
        }
    </script>
</asp:Content>
