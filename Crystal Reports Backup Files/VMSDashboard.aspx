﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VMSDashboard.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VMSDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="VMSBody" runat="server">
    <asp:hiddenfield id="hdnLoadCharts" runat="server" value="0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href='<%=ResolveUrl("~/DevExtremeContent/dx.common.css")%>' rel="stylesheet" />
    <link href='<%=ResolveUrl("~/DevExtremeContent/dx.light.css")%>' rel="stylesheet" />
    <script src='<%=ResolveUrl("~/DevExtremeScripts/dx.all.js")%>'></script>
    <div>
        <div class="form-group col-sm-4  padding-none float-left" runat="server" id="dvMultiRoles">
            <label class="control-label col-sm-5 text-right float-left top" id="lblAccessableRoles">Access Role :</label>
            <div class="col-sm-7 padding-none float-left">
                <asp:dropdownlist id="ddlVMSAccessableRoles" runat="server" autopostback="true" cssclass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker regulatory_dropdown_style drop_down padding-none" onselectedindexchanged="ddlVMSAccessableRoles_SelectedIndexChanged"></asp:dropdownlist>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none  float-left">
        <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none panel_list_bg  float-left" id="divcsscard" runat="server" style="position: relative; z-index: 9;">
            <div class="col-lg-3 col-3 col-sm-6 col-md-4  float-left padding-right_div f1_container" id="divReverted" runat="server" visible="false">
                <div class="shadow f1_card col-lg-12  float-left">
                    <div class="front face outer_dashboard1  float-left">
                        <div style="position: absolute; right: 0; top: 20;">
                            <img src="../Images/common_dashboard/reverted.png" class="" alt="Approve" />
                        </div>
                        <div class="col-lg-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Reverted Requests</div>
                        <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none  float-left">
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none float-left ">
                                <img src="../Images/common_dashboard/revetred_small.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6  float-left">
                                <span class="link_dashboard">
                                    <asp:label id="lblRevertedinitiation" cssclass="link_dashboard" runat="server" text="0"></asp:label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="back face center outer_dashboard1  float-left">
                        <div class="col-lg-12 col-12 col-sm-12 col-md-12 float-left  padding-none">

                            <div class="col-lg-12 padding-none  float-left">

                                <div class="col-lg-12 padding-none  float-left">
                                    <div class=" col-lg-12 padding-none  float-right ">
                                        <ul class="col-lg-12 col-12 col-sm-12 col-md-12 flip_list style-2 ">

                                            <li id="liReverted" runat="server"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-3 col-sm-6 col-md-4  float-left padding-right_div f1_container " id="divApprover" runat="server" visible="false">
                <div class="shadow f1_card col-lg-12  float-left">
                    <div class="front face outer_dashboard">
                        <div style="position: absolute; right: 0; opacity: 0.2; top: 20;">
                            <img src="../Images/common_dashboard/approve_big.png" class="" alt="Approve" />
                        </div>
                        <div class="col-lg-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Pending Approval</div>
                        <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none  float-left">
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none  float-left">
                                <img src="../Images/common_dashboard/approve_small.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6  float-left">
                                <span class="link_dashboard">
                                    <asp:label id="lblApproverQA" cssclass="link_dashboard" runat="server" text="0"></asp:label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="back face center outer_dashboard  float-left" style="height: 120px !important;">
                        <div class="col-lg-12 col-12 col-sm-12 col-md-12 float-left padding-none" id="divReviewViewDetails">
                            <div class="col-lg-12 padding-none  float-left">
                                <div class="col-lg-12 padding-none  float-left">
                                    <div class=" col-lg-12 padding-none  float-right ">
                                        <ul class="col-lg-12 col-12 col-sm-12 col-md-12 flip_list style-2 ">
                                            <li id="liApprove" runat="server"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="graphic" runat="server" class="col-md-12 padding-none bottom float-left">
        <div class=" bottom">
            <div class="col-md-12 padding-none  float-left grid_panel_full" id="DocumentStatusChart" runat="server">
                <div class="grid_header  col-lg-12 col-12 col-sm-12 col-md-12  float-left">
                    <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none float-left">Visitor Dashboard</div>
                    <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none  float-left">
                        <input type="button" id="btn_VisitorFilter" value="Visitor Filter" class="btn-signup_popup float-right" onclick="OpenVisitorFilter();" />
                    </div>
                </div>
                <div class="col-md-12  grid_panel_full_vms bottom float-left">
                    <div class="demo-container col-lg-6 col-6 col-md-6 col-sm-6 float-left">
                        <div id="PurposeOfVisitFunnelchart"></div>
                    </div>
                    <div class="demo-container col-lg-6 col-6 col-md-6 col-sm-6 float-left">
                        <div id="DateWisePiechart"></div>
                    </div>
                </div>
                <div class="grid_header  col-lg-12 col-12 col-sm-12 col-md-12  float-left">
                    <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none float-left">Vehicle Dashboard</div>
                    <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none  float-left">
                        <input type="button" id="btn_VehicleFilter" value="Vehicle Filter" class="btn-signup_popup float-right" onclick="OpenVehicleFilter();" />
                    </div>
                </div>
                <div class="col-md-12  grid_panel_full_vms bottom float-left">
                    <div class="demo-container">
                        <div id="VehicleBarchart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-12 col-sm-12 col-md-12 col-sm-12 col-12 col-md-12 dashboard_msgVMS float-left" id="divNoTask" runat="server" visible="false">"Tasks are not available ...!"</div>

    <!--------Charts Filter VIEW-------------->
    <div id="mpop_ChartFilter" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm " style="min-width: 30%; height: 500px;">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    Chart Filter
                     <button type="button" class="close" data-dismiss="modal" style="width: 50px">&times;</button>
                </div>
                <div class="col-md-12 col-lg-12 col-12 col-sm-12" style="background-color: #fff">
                    <select id="ddlReportType" class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none selectpicker drop_down  regulatory_dropdown_style" onchange="FillVisitorData();">
                        <option value="1" title="Week">Week</option>
                        <option value="2" title="Month">Month</option>
                        <option value="3" title="Year">Year</option>
                    </select>
                    <input id="txtFromYear" placeholder="Enter From Year" class="form-control login_input_sign_up" maxlength="4" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
                    <input id="txtToYear" placeholder="Enter To Year" class="form-control login_input_sign_up" maxlength="4" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
                    <input id="txtMonthYear" placeholder="Enter Year" class="form-control login_input_sign_up" maxlength="4" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
                    <div id="divMonthSelect">
                        <select id="ddlfromMonths" class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none selectpicker drop_down regulatory_dropdown_style">
                            <option value="1" title="January">January</option>
                            <option value="2" title="February">February</option>
                            <option value="3" title="March">March</option>
                            <option value="4" title="April">April</option>
                            <option value="5" title="May">May</option>
                            <option value="6" title="June">June</option>
                            <option value="7" title="July">July</option>
                            <option value="8" title="August">August</option>
                            <option value="9" title="September">September</option>
                            <option value="10" title="October">October</option>
                            <option value="11" title="November">November</option>
                            <option value="12" title="December">December</option>
                        </select>
                        <select id="ddlToMonth" class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none selectpicker drop_down regulatory_dropdown_style">
                            <option value="1" title="January">January</option>
                            <option value="2" title="February">February</option>
                            <option value="3" title="March">March</option>
                            <option value="4" title="April">April</option>
                            <option value="5" title="May">May</option>
                            <option value="6" title="June">June</option>
                            <option value="7" title="July">July</option>
                            <option value="8" title="August">August</option>
                            <option value="9" title="September">September</option>
                            <option value="10" title="October">October</option>
                            <option value="11" title="November">November</option>
                            <option value="12" title="December">December</option>
                        </select>
                    </div>
                    <input type='text' autocomplete="off" runat="server" id="txtFromDate" class="form-control login_input_sign_up" placeholder="DD MMM YYYY" onpaste="return false" />
                    <input type='text' autocomplete="off" runat="server" id="txtToDate" class="form-control login_input_sign_up" placeholder="DD MMM YYYY" onpaste="return false" />
                </div>
                <div style="background: none; padding: 0px;">
                    <input type="button" id="btn_DateWiseSubmit" value="Submit" class="top bottom btn-signup_popup pull-right" />
                </div>
            </div>
        </div>
    </div>
    <!--------End Charts Filter VIEW-------------->
    <script>
        $(function () {
            loadDatePickers();
            getDate();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            loadDatePickers();
            getDate();
        });

        function loadDatePickers() {
            $('#<%=txtFromDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false
            });
            $('#<%=txtToDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false
            });
        }

        $('#<%=txtFromDate.ClientID%>').on("dp.change", function (e) {
            $('#<%=txtToDate.ClientID%>').val("");
            $('#<%=txtToDate.ClientID%>').data("DateTimePicker").maxDate(false);
            $('#<%=txtToDate.ClientID%>').data("DateTimePicker").minDate(e.date);
            var SetMaxDateValue = new Date(new Date(e.date).getFullYear(), new Date(e.date).getMonth(), new Date(e.date).getDate() + 6, 23, 59, 59);
            $('#<%=txtToDate.ClientID%>').data("DateTimePicker").maxDate(new Date(new Date(e.date).getFullYear(), new Date(e.date).getMonth(), new Date(e.date).getDate() + 6, 23, 59, 59));
            $('#<%=txtToDate.ClientID%>').val(SetMaxDateValue.format('dd MMM yyyy'));
        });
    </script>
    <script type="text/javascript">
        function getDate() {
            try {
                var m_names = new Array("Jan", "Feb", "Mar",
                    "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                    "Oct", "Nov", "Dec");
                var dtTO = new Date();
                var curr_date = dtTO.getDate();
                var curr_month = dtTO.getMonth();
                var curr_year = dtTO.getFullYear();
                var dtFrom = new Date(dtTO.getTime() - (6 * 24 * 60 * 60 * 1000));
                var Todate = document.getElementById("<%=txtToDate.ClientID%>");
                Todate.value = curr_date + " " + m_names[curr_month] + " " + curr_year;

                var Fromdate = document.getElementById("<%=txtFromDate.ClientID%>");
                var curr_date1 = dtFrom.getDate();
                var curr_month1 = dtFrom.getMonth();
                var curr_year1 = dtFrom.getFullYear();
                Fromdate.value = curr_date1 + " " + m_names[curr_month1] + " " + curr_year1;
            }
            catch{ }
        }
    </script>
    <script>
        function DateWisePiechartData() {
            var ReportType = document.getElementById("ddlReportType").value;
            var FromMonth = document.getElementById("ddlfromMonths").value;
            var ToMonth = document.getElementById("ddlToMonth").value;
            var FromDate = document.getElementById('<%=txtFromDate.ClientID%>').value;
            var ToDate = document.getElementById('<%=txtToDate.ClientID%>').value;
            var FromYear = 0;
            var MonthYear = 0;
            var ToYear = 0;
            var Mode = 0;
            if (ReportType == 1) {
                Mode = 1;
            }
            else if (ReportType == 2) {
                Mode = 2;
            }
            else if (ReportType == 3) {
                Mode = 3;
            }

            if (document.getElementById("txtToYear").value != "") {
                var ToYear = document.getElementById("txtToYear").value;
            }
            if (document.getElementById("txtFromYear").value != "" && Mode != 2) {
                var FromYear = document.getElementById("txtFromYear").value;
            }
            else if (document.getElementById("txtMonthYear").value != "") {
                var FromYear = document.getElementById("txtMonthYear").value;
            }
            var formData = {
                InDateTime: FromDate,
                OutDateTime: ToDate,
                FromMonth: FromMonth,
                ToMonth: ToMonth,
                FromYear: FromYear,
                ToYear: ToYear,
                Mode: Mode
            };

            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": '<%=ResolveUrl("~/VMS/WebService/VMS_Service.asmx/GetDateWiseVisitChart")%>',
                "data": formData,
                "success": function (msg) {
                    loadDWV_Chart(msg.d);
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }

        function loadDWV_Chart(resultData) {
            $("#DateWisePiechart").dxPieChart({
                dataSource: JSON.parse(resultData),
                commonSeriesSettings: {
                    argumentField: "ArgumentText",
                    ignoreEmptyPoints: true,
                },
                series: [
                    { valueField: "ValueCount" }
                ],
                legend: {
                    verticalAlignment: "bottom",
                    horizontalAlignment: "center",
                    itemTextPosition: 'top'
                },
                title: "Date-Wise Visitors",
                tooltip: {
                    enabled: true,
                    location: "edge",
                    customizeTooltip: function (arg) {
                        return {
                            text: arg.argumentText + " : " + arg.valueText
                        };
                    }
                },
                onPointClick: function (e) {
                    var point = e.target;
                    var argument = point.argument;
                    var val = point.series.name;
                    NavigateLinkForDateWiseVisitorsRecords(argument, val);
                }
            });
            CloseFilter();
        }
        function NavigateLinkForDateWiseVisitorsRecords(argument, val) {
 var ReportType = document.getElementById("ddlReportType").value;
            var FromMonth = document.getElementById("ddlfromMonths").value;
            var ToMonth = document.getElementById("ddlToMonth").value;
            var FromDate = document.getElementById('<%=txtFromDate.ClientID%>').value;
            var ToDate = document.getElementById('<%=txtToDate.ClientID%>').value;            
            var FromYear = 0;
            var MonthYear = 0;
            var ToYear = 0;
            var Mode = 0;
            if (ReportType == 1) {
                Mode = 1;
            }
            else if (ReportType == 2) {
                Mode = 2;
            }
            else if (ReportType == 3) {
                Mode = 3;
            }

            if (document.getElementById("txtToYear").value != "") {
                var ToYear = document.getElementById("txtToYear").value;
            }
            if (document.getElementById("txtFromYear").value != "" && Mode != 2) {
                var FromYear = document.getElementById("txtFromYear").value;
            }
            else if (document.getElementById("txtMonthYear").value != "") {
                var FromYear = document.getElementById("txtMonthYear").value;
            }
            var url = '<%=ResolveUrl("~/VMS/VisitorRegister/VisitorChartDetails.aspx")%>' + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '&Mode=' + Mode + '&FromYear=' + FromYear + '&ToYear=' + ToYear + '&FromMonth=' + FromMonth + '&ToMonth=' + ToMonth + '&TimePeriod=' + argument;
            window.open(url, "_self");
        }
    </script>
    <script>
        function PurposeOfVisitFunnelchartData() {
            var ReportType = document.getElementById("ddlReportType").value;
            var FromMonth = document.getElementById("ddlfromMonths").value;
            var ToMonth = document.getElementById("ddlToMonth").value;
            var FromDate = document.getElementById('<%=txtFromDate.ClientID%>').value;
            var ToDate = document.getElementById('<%=txtToDate.ClientID%>').value;
            var FromYear = 0;
            var MonthYear = 0;
            var ToYear = 0;
            var Mode = 0;
            if (ReportType == 1) {
                Mode = 1;
            }
            else if (ReportType == 2) {
                Mode = 2;
            }
            else if (ReportType == 3) {
                Mode = 3;
            }
            if (document.getElementById("txtToYear").value != "") {
                var ToYear = document.getElementById("txtToYear").value;
            }
            if (document.getElementById("txtFromYear").value != "" && Mode != 2) {
                var FromYear = document.getElementById("txtFromYear").value;
            }
            else if (document.getElementById("txtMonthYear").value != "") {
                var FromYear = document.getElementById("txtMonthYear").value;
            }
            var formData = {
                InDateTime: FromDate,
                OutDateTime: ToDate,
                FromMonth: FromMonth,
                ToMonth: ToMonth,
                FromYear: FromYear,
                ToYear: ToYear,
                Mode: Mode
            };

            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": '<%=ResolveUrl("~/VMS/WebService/VMS_Service.asmx/GetPurpose_Of_VisitChart")%>',
                "data": formData,
                "success": function (msg) {
                    loadPOV_Chart(msg.d);
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }

        function loadPOV_Chart(resultData) {
            $("#PurposeOfVisitFunnelchart").dxFunnel({
                dataSource: JSON.parse(resultData),
                commonSeriesSettings: {
                    barWidth: 100
                },
                argumentField: "ArgumentText",
                valueField: "ValueCount",
                palette: "Soft Pastel",
                "export": {
                    enabled: false
                },
                title: "Purpose of Visit",
                tooltip: {
                    enabled: true,
                    location: "edge",
                    customizeTooltip: function (arg) {
                        return {
                            text: arg.item.argument + " : " + arg.value
                        };
                    }
                },
                item: {
                    border: {
                        visible: true
                    }
                },
                label: {
                    visible: true,
                    position: "inside",
                    backgroundColor: "none",
                    customizeText: function (e) {
                        return "<span style='font-size: 14px'>" +
                            e.item.value;
                    }
                },
                onItemClick: function (e) {
                    var argument = e.item.argument;
                    NavigateLinkForPurposeofVisitRecords(argument);
                }
            });
            CloseFilter();
        }
        function NavigateLinkForPurposeofVisitRecords(argument) {
            var url = '<%=ResolveUrl("~/VMS/VisitorRegister/VisitorRegister.aspx")%>';
            window.open(url, "_self");
        }
    </script>
    <script>
        function VehicleBarchartData() {
            var ReportType = document.getElementById("ddlReportType").value;
            var FromMonth = document.getElementById("ddlfromMonths").value;
            var ToMonth = document.getElementById("ddlToMonth").value;
            var FromDate = document.getElementById('<%=txtFromDate.ClientID%>').value;
            var ToDate = document.getElementById('<%=txtToDate.ClientID%>').value;
            var FromYear = 0;
            var MonthYear = 0;
            var ToYear = 0;
            var Mode = 0;
            if (ReportType == 1) {
                Mode = 1;
            }
            else if (ReportType == 2) {
                Mode = 2;
            }
            else if (ReportType == 3) {
                Mode = 3;
            }
            if (document.getElementById("txtToYear").value != "") {
                var ToYear = document.getElementById("txtToYear").value;
            }
            if (document.getElementById("txtFromYear").value != "" && Mode != 2) {
                var FromYear = document.getElementById("txtFromYear").value;
            }
            else if (document.getElementById("txtMonthYear").value != "") {
                var FromYear = document.getElementById("txtMonthYear").value;
            }
            var formData = {
                InDateTime: FromDate,
                OutDateTime: ToDate,
                FromMonth: FromMonth,
                ToMonth: ToMonth,
                FromYear: FromYear,
                ToYear: ToYear,
                Mode: Mode
            };

            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": '<%=ResolveUrl("~/VMS/WebService/VMS_Service.asmx/GetVehicleChart")%>',
                "data": formData,
                "success": function (msg) {
                    loadVehicle_Chart(msg.d);
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }

        function loadVehicle_Chart(resultData) {
            $("#VehicleBarchart").dxChart({
                dataSource: JSON.parse(resultData),
                commonSeriesSettings: {
                    argumentField: "ArgumentText",
                    type: "bar",
                    ignoreEmptyPoints: true,
                    barWidth: 50
                },
                series: [
                    {
                        valueField: "ValueCount",
                        name: "VechileNo",
                        label: {
                            visible: true,
                            position: "inside",
                            backgroundColor: "none",
                            customizeText: function (e) {
                            }
                        }
                    },
                ],
                legend: {
                    verticalAlignment: "bottom",
                    horizontalAlignment: "center",
                    itemTextPosition: 'top'
                },
                title: "Vehicle",
                tooltip: {
                    enabled: true,
                    location: "edge",
                    customizeTooltip: function (arg) {
                        return {
                            text: arg.argumentText + " : " + arg.valueText
                        };
                    }
                },
                onPointClick: function (e) {
                    var point = e.target;
                    var argument = point.argument;
                    var val = point.series.name;
                    NavigateLinkForVehicleRecords(argument, val);
                }
            });
            CloseFilter();
        }
        function NavigateLinkForVehicleRecords(argument, val) {
            var url = '<%=ResolveUrl("~/VMS/VehicleRegister/Vechile_Register.aspx")%>';
            window.open(url, "_self");
        }
    </script>
    <!----To Load Calendar ---->
    <script>
        $(document).ready(function () {
            //Binding Code
            DateWisePiechartData();
            PurposeOfVisitFunnelchartData();
            VehicleBarchartData();
            LoadFill();
        });
    </script>
    <script>
        function OpenVisitorFilter() {
            $('#mpop_ChartFilter').modal({ show: true, backdrop: 'static', keyboard: false });
            $('#<%=hdnLoadCharts.ClientID%>').val("0");
        }
        function OpenVehicleFilter() {
            $('#mpop_ChartFilter').modal({ show: true, backdrop: 'static', keyboard: false });
            $('#<%=hdnLoadCharts.ClientID%>').val("1");
        }
        function CloseFilter() {
            $('#mpop_ChartFilter').modal('hide');
        }
    </script>
    <script>
        $("#btn_DateWiseSubmit").click(function () {
            var ReportType = document.getElementById("ddlReportType").value;
            var FromMonth = document.getElementById("ddlfromMonths").value;
            var ToMonth = document.getElementById("ddlToMonth").value;
            var FromDate = document.getElementById('<%=txtFromDate.ClientID%>').value;
            var ToDate = document.getElementById('<%=txtToDate.ClientID%>').value;
            var FromYear = document.getElementById("txtFromYear").value;
            var MonthYear = document.getElementById("txtMonthYear").value;
            var ToYear = document.getElementById("txtToYear").value;
            var LoadCharts = document.getElementById('<%=hdnLoadCharts.ClientID%>').value;
            errors = [];
            if (ReportType == 1) {
                if (FromDate == "") {
                    errors.push("Select From Date.");
                }
                if (ToDate == "") {
                    errors.push("Select To Date.");
                }
                //if (Date.parse(ToDate) < Date.parse(FromDate)) {
                //    errors.push("To Date should be greater than From Date.");
                //}
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                    return false;
                }
                else {
                    if (LoadCharts == "0") {
                        DateWisePiechartData();
                        PurposeOfVisitFunnelchartData();
                    }
                    else {
                        VehicleBarchartData();
                    }
                    return true;
                }
            }
            else if (ReportType == 2) {
                if (MonthYear == "") {
                    errors.push("Enter Month Year.");
                }
                if (FromMonth == "0") {
                    errors.push("Select From Month.");
                }
                if (ToMonth == "0") {
                    errors.push("Select To Month.");
                }
                if (parseInt(ToMonth) < parseInt(FromMonth)) {
                    errors.push("To Month should be greater than or equal to From Month.");
                }
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                    return false;
                }
                else {
                    if (LoadCharts == "0") {
                        DateWisePiechartData();
                        PurposeOfVisitFunnelchartData();
                    }
                    else {
                        VehicleBarchartData();
                    }
                    return true;
                }
            }
            else if (ReportType == 3) {
                if (FromYear == "") {
                    errors.push("Enter From Year.");
                }
                if (ToYear == "") {
                    errors.push("Enter To Year.");
                }
                if (ToYear < FromYear) {
                    errors.push("To Year should be greater than or equal to From Year.");
                }
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                    return false;
                }
                else {
                    if (LoadCharts == "0") {
                        DateWisePiechartData();
                        PurposeOfVisitFunnelchartData();
                    }
                    else {
                        VehicleBarchartData();
                    }
                    return true;
                }
            }
        });
    </script>
    <script>
        function FillVisitorData() {
            var ReportType = document.getElementById("ddlReportType").value;
            var Mode = 0;
            if (ReportType == 1) {
                Mode = 1;
                $("#divMonthSelect").hide();
                $('#<%=txtFromDate.ClientID%>').show();
                $('#<%=txtToDate.ClientID%>').show();
                $("#txtFromYear").hide();
                $("#txtMonthYear").hide();
                $("#txtToYear").hide();
            }
            else if (ReportType == 2) {
                Mode = 2;
                $("#divMonthSelect").show();
                $('#<%=txtFromDate.ClientID%>').hide();
                $('#<%=txtToDate.ClientID%>').hide();
                $("#txtFromYear").hide();
                $("#txtMonthYear").show();
                $("#txtToYear").hide();
                $("#txtMonthYear").val(<%=DateTime.Now.Year.ToString() %>);
            }
            else if (ReportType == 3) {
                Mode = 3;
                $("#divMonthSelect").hide();
                $('#<%=txtFromDate.ClientID%>').hide();
                $('#<%=txtToDate.ClientID%>').hide();
                $("#txtFromYear").show();
                $("#txtMonthYear").hide();
                $("#txtToYear").show();
                $("#txtFromYear").val(<%=DateTime.Now.AddYears(-1).Year.ToString() %>);
                $("#txtToYear").val(<%=DateTime.Now.Year.ToString() %>);
            }
        }
        function LoadFill() {
            var Mode = 1;
            $("#ddlReportType").val('1');
            $("#divMonthSelect").hide();
            $('#<%=txtFromDate.ClientID%>').show();
            $('#<%=txtToDate.ClientID%>').show();
            $("#txtFromYear").hide();
            $("#txtMonthYear").hide();
            $("#txtToYear").hide();
        }
    </script>
</asp:Content>
