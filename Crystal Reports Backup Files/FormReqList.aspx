﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="FormReqList.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentsList.FormReqList" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right btn btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
        <div class=" col-md-12 col-lg-12 col-12 col-sm-12 dms_outer_border float-left padding-none">
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left" id="divMainContainer" runat="server" visible="true">
                <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-right ">
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-md-12 col-lg-12 col-12 col-sm-12 float-left">Form Print List</div>
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                           <div class="col-12 float-left top padding-none bottom">
                            <table id="dtDocList" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>FormRequestID</th>
                                        <th>Request Number</th>
                                        <th>Form Name</th>
                                        <th>Request Purpose</th>
                                        <th>No.of Copies</th>
                                        <th class="formaction">Print</th>
                                        <th>View Request</th>
                                        <th>View Form</th>
                                        <th>History</th>
                                        <th>FormVersionID</th>
                                    </tr>
                                </thead>
                            </table>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-------- Request VIEW-------------->
    <div id="myModal_request" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="max-width: 98%">
            <!-- Modal content-->
            <div class="modal-content col-md-12 col-lg-12 padding-none">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <span id="span_FileTitle" runat="server"></span>
                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div id="div_RequestedBy" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblRequestedBy" runat="server" Text="Requested By" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_RequestedBy" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Requested By" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_NoofCopies" class="col-md-2 col-lg-2 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblNoofCopies" runat="server" Text="No.of Copies" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_NoofCopies" runat="server" CssClass="form-control login_input_sign_up" TextMode="Number" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Project" class="col-md-4 col-lg-4 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label1" runat="server" Text="Project Number" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_ProjectNumber" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Project Number" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_DocName" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label4" runat="server" Text="Form Name" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_DocumentName" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Form Name" AutoPostBack="true" Rows="2" MaxLength="500" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Purpose" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblComments" runat="server" Text="Purpose" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_Purpose" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Purpose" Width="100%" Rows="2" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_ReviewedBy" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group bottom float-left">
                                <asp:Label ID="Label6" runat="server" Text="Reviewed By" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_ReviewedBy" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_DocController" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group bottom float-left">
                                <asp:Label ID="Label7" runat="server" Text="Document Controller" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_DocController" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-12 text-right">
                    <button data-dismiss="modal" class=" btn-cancel_popup">Close</button>
                                </div>
                </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!-------- End Request VIEW-------------->

    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 85%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">                           
                            <span id="span4" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("ActionBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-Width="11%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("ActionDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="13%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-Width="29%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                <asp:TextBox ID="txt_Comments" runat="server" ReadOnly="true" Rows="2" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM">Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL">Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned To">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->

    <!--------Form Return Edit-------------->
    <div id="myFormReturnEdit" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 90%; height: 500px">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel2">
                    <ContentTemplate>
                        <asp:HiddenField ID="hdfPrintCopyCount" runat="server" />
                        <div class="modal-header">                          
                            <span id="span2" runat="server"></span>
                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div id="div_RequestedBy" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label2" runat="server" Text="Requested By" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_RequestedByReturn" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Requested By" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_NoofCopies" class="col-md-6 col-lg-2 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label3" runat="server" Text="No.of Copies" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_NoofCopiesReturn" runat="server" CssClass="form-control login_input_sign_up" TextMode="Number" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Project" class="col-md-6 col-lg-4 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label5" runat="server" Text="Project Number" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_ProjectNumberReturn" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Project Number" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_DocName" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label8" runat="server" Text="Form Name" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_DocumentNameReturn" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Form Name" AutoPostBack="true" Rows="2" MaxLength="500" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Purpose" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label9" runat="server" Text="Purpose" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_PurposeReturn" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Purpose" Width="100%" Rows="2" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_ReviewedBy" class="col-md-6 col-lg-4 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label10" runat="server" Text="Reviewed By" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_ReviewedByReturn" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_DocController" class="col-md-6 col-lg-4 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label13" runat="server" Text="Document Controller" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_DocControllerReturn" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_NoofUsedCopies" class="col-md-6 col-lg-2 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label11" runat="server" Text="No.of Copies Used" CssClass="label-style"></asp:Label><span class="smallred_label">*</span>
                                <asp:TextBox ID="txt_NoofUsedCopies" runat="server" CssClass="form-control login_input_sign_up" TextMode="Number" AutoPostBack="true" OnTextChanged="txt_NoofUsedCopies_TextChanged" TabIndex="1" onkeypress="return RestrictMinus(event);" min="0"></asp:TextBox>
                            </div>
                            <div id="div_NoofReturnedCopies" class="col-md-6 col-lg-2 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label15" runat="server" Text="No.of Copies Returned" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_NoofReturnedCopies" runat="server" CssClass="form-control login_input_sign_up" TextMode="Number" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_CopyNumbers" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label14" runat="server" Text="Unused Copy Numbers" CssClass="label-style"></asp:Label>
                                <textarea ID="txt_CopyNumbers" runat="server" class="form-control" placeholder="Enter Unused Copy Numbers" MaxLength="300" TabIndex="2" onkeypress="return onlyCommaAndNumbers(this,event);"></textarea>
                            </div>
                            <div id="div_Remarks" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label12" runat="server" Text="Remarks" CssClass="label-style"></asp:Label><span class="smallred_label">*</span>
                                <textarea ID="txt_Remarks" runat="server" class="form-control" placeholder="Enter Remarks" MaxLength="300" TabIndex="3"></textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <div class="col-12 text-right">
                            <input type="button" id="btn_Submit" runat="server" class="btn btn-signup_popup" onclick="return commentRequiredReturn();" value="Return" tabindex="4" />
                            <asp:Button ID="btn_Cancel" Text="Cancel" data-dismiss="modal" CssClass="btn btn-cancel_popup" runat="server" CausesValidation="false" TabIndex="5" />
                        </div></div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Form Return Edit-------------->

    <!-------- Form VIEW-------------->
    <div id="myFormView" class="modal department fade" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 90%; height: 500px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpHeading">
                        <ContentTemplate>
                            <span id="span1" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divPDF_Viewer">
                        <noscript>
                            Please Enable JavaScript.
                        </noscript>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------End Form VIEW-------------->

    <div id="myModalPrint" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm" style="min-width: 30%;">
            <!-- Modal content-->
            <div class="modal-content col-lg-12 padding-none">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <span id="span3" runat="server">Print Info</span>
                             <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                        <div class="modal-body ">
                            <div class="col-lg-12 float-left">
                                <asp:Literal ID="literaldownload1" runat="server"></asp:Literal>

                                <asp:Label runat="server" ID="lblDownload">Wait until Print finishes, then click on OK.</asp:Label>
                            </div>
                            <input id="btnOk" type="button" class="float-right  btn-signup_popup top bottom" onclick="reloadformPrint();" value="OK" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdfRID" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdfRole" runat="server" Value="0" />
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <asp:HiddenField ID="hdfViewDivID" runat="server" />
    <asp:UpdatePanel ID="UpGridButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnRequestView" runat="server" Text="Submit" OnClick="btnRequestView_Click" Style="display: none" />
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" />
            <asp:Button ID="btnPrintDocument" runat="server" Text="Submit" Style="display: none" OnClick="btnPrintDocument_Click" OnClientClick="showPrint();" />
            <asp:Button ID="btnEditFormReturn" runat="server" Text="Submit" Style="display: none" OnClick="btnEditFormReturn_Click" />
            <asp:Button ID="btnFormView" runat="server" Text="Submit" OnClick="btnFormView_Click" Style="display: none" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        $('#dtDocList thead tr').clone(true).appendTo('#dtDocList thead');
        $('#dtDocList thead tr:eq(1) th').each(function (i) {
            if (i < 6) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (oTable.column(i).search() !== this.value) {
                        oTable
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDocList').wrap('<div class="dataTables_scroll" />');
        var oTable = $('#dtDocList').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'FormReqID' },
                { 'data': 'RequestNumber' },
                { 'data': 'FormName' },
                { 'data': 'RequestPurpose' },
                { 'data': 'NoofCopies' },
                { 'data': 'ActionStatusID' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="view_req" title="View" data-toggle="modal" data-target="#myModal_request" onclick=ViewRequest(' + o.FormReqID + ')></a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="view" title="View" data-toggle="modal" data-target="#myFormView" onclick=ViewForm(' + o.FormVersionID + ')></a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History" data-toggle="modal" data-target="#myCommentHistory" onclick=ViewCommentHistory(' + o.FormReqID + ')></a>'; }
                },

                { 'data': 'FormVersionID' },
            ],
            "responsive": true,
            "paging": true,
            "search": {
                "regex": true
            },
            "aoColumnDefs": [{ "targets": [0,1, 10], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 6] }, { "className": "dt-body-left", "targets": [3, 4] },
            {
                targets: [6], render: function (a, b, data, d) {

                    if (data.ActionStatusID == 3) {
                        return '<a class="print" title="Print" onclick=PrintDocument(' + data.FormReqID + ',' + data.FormVersionID + ')></a>';
                    } else if (data.ActionStatusID == 7) {
                        return '<a class="return" data-toggle="modal" data-target="#myFormReturnEdit" title="Return" onclick=EditFormReturn(' + data.FormReqID + ',' + data.ActionStatusID + ')></a>';
                    }
                    return "N/A";
                }
            },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetFormPrintReqList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
        function reloadtable() {
            oTable.ajax.reload();
            $('#myModal_request').modal('hide');
            $('#usES_Modal').modal('hide');
        }
        function reloadreturntable() {
            oTable.ajax.reload();
            $('#myFormReturnEdit').modal('hide');
            $('#usES_Modal').modal('hide');
        }
    </script>
    <script>
        function ViewRequest(V_ID) {
            document.getElementById("<%=hdfRID.ClientID%>").value = V_ID;
            var btnRV = document.getElementById("<%=btnRequestView.ClientID %>");
            btnRV.click();
        }
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfRID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
        function PrintDocument(PK_ID, FV_ID) {
            document.getElementById("<%=hdfRID.ClientID%>").value = PK_ID;
            document.getElementById("<%=hdfVID.ClientID%>").value = FV_ID;
            var btnPD = document.getElementById("<%=btnPrintDocument.ClientID %>");
            btnPD.click();
        }
        function EditFormReturn(PK_ID, Action) {
            document.getElementById("<%=hdfRID.ClientID%>").value = PK_ID;
            document.getElementById("<%=hdnAction.ClientID%>").value = Action;
            var btnEF = document.getElementById("<%=btnEditFormReturn.ClientID %>");
            btnEF.click();
        }
        function ViewForm(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnFV = document.getElementById("<%=btnFormView.ClientID %>");
            btnFV.click();
        }
    </script>
    <script type="text/javascript">
        function commentRequiredReturn() {
            var NoofUsedCopies = document.getElementById("<%=txt_NoofUsedCopies.ClientID%>").value;
            var NoofCopiesReturn = document.getElementById("<%=txt_NoofCopiesReturn.ClientID%>").value;
            var CopyNumbers = document.getElementById("<%=txt_CopyNumbers.ClientID%>").value;
            var Remarks = document.getElementById("<%=txt_Remarks.ClientID%>").value;
            var NoofReturnedCopies = document.getElementById("<%=txt_NoofReturnedCopies.ClientID%>").value;
            var validation;
            // comma separated string data from hidden field
            var PrintCopyNumbers = document.getElementById("<%=hdfPrintCopyCount.ClientID%>").value; 
            // convert comma separated Print Copies to array
            var PrintCopyNumbersArr = PrintCopyNumbers.split(',');
            // convert comma separated return copies to array
            var ReturnCopyNumbersArr = CopyNumbers.split(',');
            errors = [];
            if (NoofUsedCopies.trim() == "") {
                errors.push("Enter number of copies used.");
            }
            else if (parseInt(NoofUsedCopies) > parseInt(NoofCopiesReturn)) {
                errors.push("Number of used copies must not be greater than number of copies issued.");
            }
            else if (parseInt(NoofUsedCopies) < 0) {
                errors.push("Number of used copies must not be less than zero.");
            }
            if (parseInt(NoofUsedCopies) != parseInt(NoofCopiesReturn)) {
                if (CopyNumbers.trim() == "") {
                    errors.push("Enter Unused copy numbers.");
                }
                else {
                    // check length of array with return count
                    if (ReturnCopyNumbersArr.length == NoofReturnedCopies) {
                        
                        // check return copies array with Print Copies array whether it's exists or not
                        for (var i = 0; i < ReturnCopyNumbersArr.length; i++) {
                            if ((PrintCopyNumbersArr.indexOf(ReturnCopyNumbersArr[i]))==-1) {
                                errors.push("Enter Valid Copy Number/s");
                                break;
                            }
                        }
                    } 
                    else {
                        errors.push("No.of Unused Copy Numbers should be equal to No.of Copies Returned");
                    }                                                                                                 
                }
            }
            if (CopyNumbers.indexOf(",") == 0) {
                errors.push("Copy number Starting with comma is not Allowed.");           
            }
            if (Remarks.trim() == "") {
                errors.push("Enter remarks.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ReturnAction();
                return true;
            }
        }
    </script>
    <script>
        function PrintAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Print";
            openElectronicSignModal();
        }
        function ReturnAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Return";
            openElectronicSignModal();
        }
    </script>
    <script>
        function openElectronicSignModal() {
            openUC_ElectronicSign();
        }
        function CloseBrowser() {
            window.close();
        }
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script>
        function RestrictMinus(e) {
            if (e.keyCode === 45 || e.keyCode === 109 || e.keyCode === 189) {
                return false;
            }
            else {
                return true;
            }
        };
        function onlyCommaAndNumbers(txt, event) {

            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode == 44) {                
                return true;
            }
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function ViewPDFForm() {
            $('#myFormView').modal({ show: true, backdrop: 'static', keyboard: false });
            PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', "3", $('#<%=hdfVID.ClientID%>').val(), "0", "0", "#divPDF_Viewer");
        }
        function showPrint() {
            $('#myModalPrint').modal({ show: true, backdrop: 'static', keyboard: false });
        }
        function reloadformPrint() {
            window.open("<%=ResolveUrl("~/DMS/DocumentsList/FormReqList.aspx")%>", "_self");
        }
        function changeHeaderText() {
            var FormPrint = "<%=this.Session["FormPrint"].ToString()%>";
            if (FormPrint == "n") {
                $(".grid_header").text("Form Return List");
                $(".formaction.sorting_disabled").text("Return");
            }
            else {
                $(".grid_header").text("Form Print List");
                //$(".formaction").text("Print");
            }
        }
        function HidePopup() {
            $('#usES_Modal').modal('hide');
        }
    </script>
    <div class="col-lg-12" style="position: absolute; top: -13px; left: 0">
        <asp:UpdateProgress ID="upActionProgress" runat="server" AssociatedUpdatePanelID="UpGridButtons">
            <ProgressTemplate>
                <div class="" style="position: absolute; height: 93vh; background-color: #3e3e3e; z-index: 1; width: 100%; opacity: 0.5;"></div>
                <div class="ProgressModal" style="z-index: 99; position: relative; padding: 10pc 21pc; left: 9px">
                    <div class="ProgressCenter">
                        <img alt="" src="<%=ResolveUrl("~/Images/TMS_Images/please_wait.gif")%>" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
</asp:Content>
