﻿<%@ Page Title="JR Training List" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="JR_TrainingList.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Training.JR_Training.JR_TrainingList" %>

<%@ Register Src="~/UserControls/TMS_UserControls/JR_TrainingFile.ascx" TagPrefix="uc1" TagName="JR_TrainingFile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnJR_ID" runat="server" />
            <asp:HiddenField ID="hdnPrint" runat="server" Value="false" />
            <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
             <asp:HiddenField runat="server" ID="hdnNotificationID" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div  id="divMainContainer" runat="server">
            <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="true">JR Training List</asp:Label>
            </div>
            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none" style="overflow: auto; margin-top: 10px">
                <table id="tblJR_TrainingList" class="datatable_cust tblJR_TrainingListClass display breakword" style="width: 100%">
                    <thead>
                        <tr>
                            <th>JR_ID</th>
                            <th>JR Name</th>
                            <th>Version</th>
                            <th>Assigned By</th>
                            <th>Status</th>
                             <th>StatusID</th>
                            <th>View JR</th>
                            <th>View TF</th>
                            <th>Training</th>
                        </tr>
                    </thead>
                </table>
                <div class="col-12 grid_legend_tms">'JR': Job Responsibilities ; 'TF': Training File</div>
            </div>
        </div>
    </div>

    <%--modal popup to load Jr Description--%>
    <div class="modal fade department" id="JRDescriptionModal" role="dialog">
        <div class="modal-dialog" style="min-width: 80%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="color: aliceblue">Job Responsibility</h4>
                     <button type="button" class="close " data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="row tmsModalContent">
                        <div class="col-12">
                            <div style="border: 1px solid #d4d4d4; height: 500px; overflow: auto; border-radius: 4px;">
                                <asp:UpdatePanel ID="uptext" runat="server" UpdateMode="Conditional" style="margin: 10px">
                                    <ContentTemplate>
                                        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:UpdateProgress ID="upActionProgress" runat="server" AssociatedUpdatePanelID="upActionButtons">
        <ProgressTemplate>
            <div class="ProgressModal">
                <div class="ProgressCenter">
                    <img alt="" src="<%=ResolveUrl("~/Images/TMS_Images/please_wait.gif")%>" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

   <%-- <!-- Modal GetTraineeDetailsForTF -->
    <div class="modal fade department" id="modelTrainingFile" role="dialog">
        <div class="modal-dialog modal-lg"  style="min-width: 98%;">
            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 0px">
                <div class="modal-header">
                    
                    <h4 class="modal-title Panel_Title">Training File</h4>
                    <button type="button" class="close " data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body tmsModalBody" style="overflow: visible;">
                    <div class="col-12 float-left tmsModalContent">
                        <asp:UpdatePanel ID="upTrainingFile" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-sm-12 float-left">
                                    <div class="form-horizontal col-sm-12 float-left padding-none">
                                        <div class="form-group col-sm-12 float-left padding-none">
                                             <label class="control-label float-left padding-none col-sm-12">Select Training File :</label>
                                            <div class="col-sm-6 float-left padding-none">
                                                <asp:DropDownList ID="ddlTrainingFileView" runat="server" CssClass="form-control SearchDropDown selectpicker show-tick" Width="300px" OnSelectedIndexChanged="ddlTrainingFileView_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="0">Complete Training File</asp:ListItem>
                                                    <asp:ListItem Value="1">Job Description</asp:ListItem>
                                                    <asp:ListItem Value="2">Training Schedule</asp:ListItem>
                                                    <asp:ListItem Value="3">Retrospective Qualification Form</asp:ListItem>
                                                    <asp:ListItem Value="4">Training Record</asp:ListItem>
                                                    <asp:ListItem Value="5">Training Questionnaire</asp:ListItem>
                                                    <asp:ListItem Value="6">Training Certificate</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-2 float-left" runat="server" id="divPrint">
                                            </div>
                                        </div>
                                        <div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 float-left" style="overflow: auto; max-height: 600px; text-align: center; font-size: 23px; padding: 30px; color: #07889a;">
                                    <asp:Literal ID="Literal2" runat="server" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnViewJR_Details" runat="server" Text="Button" OnClick="btnViewJR_Details_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upActionBtns" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnViewTraining" runat="server" Text="Button" OnClick="btnViewTraining_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <uc1:JR_TrainingFile runat="server" id="JR_TrainingFile" />

    <!-- Action Buttons Operations -->
    <script>
        function viewJR_TrainingDetails(JrID) {
            $("#<%=hdnJR_ID.ClientID%>").val(JrID);
            $("#<%=btnViewJR_Details.ClientID%>").click();
        }
       
        function viewJR_Training(JrID) {
            $("#<%=hdnJR_ID.ClientID%>").val(JrID);
            $("#<%=btnViewTraining.ClientID%>").click();
        }

        function OpenModelViewJR() {
            $('#JRDescriptionModal').modal({ backdrop: 'static', keyboard: false });
        }
        //function OpenModelTrainingFile() {
        //    $('#modelTrainingFile').modal({ backdrop: 'static', keyboard: false });
        //}
    </script>

    <!--For JR Training jQuery Datatable-->
    <script>
        function getUrlVars() {
            var vars = [], hash;
            var url = window.location.href;
            var hashes = url.slice(url.indexOf('?') + 1).split('&');
            if (url.indexOf("#") > 0) {
                var hashes = url.slice(url.indexOf('?') + 1, url.indexOf('#')).split('&');
            }

            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        var table = $('#tblJR_TrainingList').DataTable({
            columns: [
                { 'data': 'JR_ID' },
                { 'data': 'JR_Name' },
                { 'data': 'JR_Version' },
                { 'data': 'AssignedBy' },
                { 'data': 'StatusName' },
                { 'data': 'StatusID' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        return '<a class="view"  href="#" title="View JR" onclick="viewJR_TrainingDetails(' + o.JR_ID + ');"></a>';
                    }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        if (o.StatusID == "10") {
                            return '<a class="trainingfile_tms" title="Training File" href="#" onclick="viewEmpTF_Details(' + o.JR_ID + ');"></a>';
                        }
                        else {
                            return '';
                        }
                    }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        if (o.StatusID >= "7") {
                            return '<a class="Training_tms" title="Training" href="#" onclick="viewJR_Training(' + o.JR_ID + ');"></a>';
                        }
                        else {
                            return '';
                        }
                    }
                }
            ],
            "aoColumnDefs": [{ "targets": [0, 5], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [1, 3, 4] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JR_TrainingListService.asmx/GetJR_TrainingList")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "FilterType", "value": getUrlVars()["FilterType"] }, { "name": "NotificationID", "value": <%=hdnNotificationID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblJR_TrainingList").show();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });

    </script>

</asp:Content>
