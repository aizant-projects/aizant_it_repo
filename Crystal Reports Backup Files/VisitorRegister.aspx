﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VisitorRegister.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VisitorRegister.VisitorRegister" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>


<asp:Content ID="Content1" ContentPlaceHolderID="VMSBody" runat="server">
    
    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnPkID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnPkIDAppointment" runat="server" Value="0" />
            <asp:HiddenField ID="hdnVisitorName" runat="server" Value="" />
            <asp:HiddenField ID="hdnCustom" runat="server" Value="" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdnCheckout" runat="server" />
    <asp:HiddenField ID="hdnMinDate" runat="server" />

    <style>
        table tbody tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            padding: 4px;
            border-right: 1px solid #fff;
        }

        .dxreControl .dxreView {
            background-color: #E0E0E0;
            height: 392px !important;
        }

        .dxreView {
            position: relative;
            overflow: auto;
            padding: 0 10px;
            -webkit-overflow-scrolling: touch;
            -webkit-appearance: none;
            -ms-touch-action: pan-y pinch-zoom;
            touch-action: pan-y pinch-zoom;
            height: 455px !important;
        }

        .CreatorGrid thead tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            padding: 4px;
            border-right: 1px solid #fff;
        }

        table tbody tr td {
            text-align: center;
            height: 30px;
        }

        .CreatorGrid tbody tr td {
            text-align: center;
            height: 30px;
            width: 20px;
        }
                .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            background-color: #07889a !important;
        }
    </style>

    <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup top" PostBackUrl="~/VMS/VMS_HomePage.aspx" Text="Dashboard" />

    <div class="col-md-12 col-lg-12 col-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>

    <div class="col-md-12 col-lg-12 col-12 padding-none  float-left" id="divMainContainer" runat="server" visible="true">
     
         <ul class="nav nav-tabs tab_grid">
            <li id="Visitortab1" class=" nav-item" runat="server"><a class="NewDocumentTab  Visitortab1 nav-link active" data-toggle="tab" href="#TabPanel1">Visitors</a></li>
            <li id="Appointmenttab2" runat="server" class="nav-item"><a class="ReverteTab Appointmenttab2  nav-link" data-toggle="tab" href="#TabPanel2">Appointments</a></li>
        </ul>
        <div class="tab-content">
            <div id="TabPanel1" class="NewDocumentTabBody tab-pane fade  active show">
                
                    <div class=" col-md-12 col-lg-12 col-sm-12 col-12 vms_outer_border float-left top padding-none">
                       
                            <div class="col-md-12 col-lg-12 col-sm-12 col-12 grid_panel_full padding-none float-left">
                                <div class="  col-md-12 col-lg-12 col-sm-12 col-12 float-left padding-none">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 visitor_lists grid_header ">Visitor List</div>
                                </div>
                                <div class="col-12 float-right padding-right_div">
                                      <asp:Button ID="btnNavCreateVisitor" runat="server" CssClass="float-right  btn-signup_popup top" OnClick="btnNavCreateVisitor_Click" Text="New Visitor" />

                                </div>

                                <div class="col-md-12 col-lg-12 col-sm-12 col-12 grid_container top bottom float-left padding-none">

                                    <table id="dtVisitorList" class="tblVisitorRegister datatable_cust display" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>S.No</th><%--0--%>
                                                <th>S.No</th><%--1--%>
                                                <th>Visitor ID</th><%--2--%>
                                                <th>Visitor Name</th><%--3--%>
                                                <th>Mobile No.</th><%--4--%>
                                                <th>Department</th><%--5--%>
                                                <th>Whom To Visit</th><%--6--%>
                                                <th>Purpose of Visit</th><%--7--%>
                                                <th>Organization</th><%--8--%>
                                                <th>Check In Time</th><%--9--%>
                                                <th>Check Out Time</th><%--10--%>
                                                <th>Edit</th><%--11--%>
                                                <th>Print</th><%--12--%>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                        
                        </div>
                    
              </div>
            </div>
            <div id="TabPanel2" class="NewDocumentTabBody1 tab-pane fade">
            
                    <div class=" col-md-12 col-lg-12 col-sm-12 col-12 vms_outer_border float-left top padding-none">
                        
                            <div class="col-md-12 col-lg-12 col-sm-12 col-12 grid_panel_full padding-none float-left">
                                <div class=" grid_header col-md-12 col-lg-12 col-sm-12 col-12 float-left ">
                                    <div class=" float-left">
                                        Appointment List
                                    </div>
                                    <asp:UpdatePanel ID="upbtnNavCreateAppointment" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class=" float-right">
                                                
                                                <asp:Button ID="btnNavCreateAppointment" CssClass="float-right  btn-signup_popup" runat="server" Text="New Appointment" OnClick="btnNavCreateAppointment_Click" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                           
                        <div class="col-md-12 col-lg-12 col-sm-12 col-12 grid_container top bottom float-left padding-none">
                            <table id="dtAppointment" class="display tblVisitorApp_ListClass datatable_cust" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Test</th><!--0-->
                                        <th>S.No</th><!--1-->
                                        <th>Visitor ID</th><!--2-->
                                        <th>Visitor Name</th><!--3-->
                                        <th>Mobile No</th><!--4-->
                                        <th>Purpose of Visit</th><!--5-->
                                        <th>Organization</th><!--6-->
                                        <th>Department</th><!--7-->
                                        <th>Whom To Visit</th><!--8-->
                                        <th>Appointment Date</th><!--9-->
                                        <th>Status</th><!--10-->
                                        <th>Edit</th><!--11-->
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                        </div>
        </div>
    </div>
    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnEdit_List" runat="server" OnClick="btnEdit_List_Click" Text="Button" />
                <asp:Button ID="btnAppointmentEdit_List" runat="server" OnClick="btnAppointmentEdit_List_Click" Text="Button" />
                <asp:Button ID="btnCheckOut" runat="server" OnClick="btnCheckOut_Click" Text="Button" />
                <asp:Button ID="btnPrintPass" runat="server" OnClick="btnPrintPass_Click" Text="Button" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!--Modal PopUp for Visitor Appointment Register-->
    <div id="ModalVisitorAppointment" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="background: #fff; float: left; border: none; width: 940px;">
                <div class="modal-header">
                    <h4 class="modal-title col-sm-8">Visitor Appointment Booking</h4>
                    <button type="button" class="close col-sm-4" style="text-align: right;" data-dismiss="modal">&times;</button>
                </div>
                <div class="col-lg-12 float-left">
                    <asp:UpdatePanel runat="server" ID="upAppointment" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-sm-12 padding-none float-left">
                                <div class="col-sm-12  padding-none top float-left">
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div5">
                                        <label class="control-label col-sm-12" id="lblAppointmentId">Appointment ID</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtAppointmentId" runat="server" CssClass="form-control login_input_sign_up" Enabled="false" onkeypress="return event.keyCode != 13;" MaxLength="20"></asp:TextBox>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div1">
                                                <label class="control-label col-sm-12" id="lblVisitor Name">First Name<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txt_VisitorName" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" MaxLength="50" AutoPostBack="true" OnTextChanged="txt_VisitorName_TextChanged"></asp:TextBox>
                                                    <ajaxToolkit:AutoCompleteExtender ServiceMethod="GetVisitorAppintmentNameList" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                                        TargetControlID="txt_VisitorName" ID="AutoCompleteExtender1" UseContextKey="true" runat="server" FirstRowSelected="false">
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div12">
                                        <label class="control-label col-sm-12" id="lblLast Name">Last Name<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel runat="server" ID="uptxtMobile">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div2">
                                                <label class="control-label col-sm-12" id="lblMobileNo">Mobile No<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txt_MobNo" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Mobile" onkeypress="return ValidatePhoneNo(event);" MaxLength="10" AutoPostBack="true" OnTextChanged="txt_MobNo_TextChanged"></asp:TextBox>
                                                    <ajaxToolkit:AutoCompleteExtender runat="server" ServiceMethod="GetVisitorAppintmentListByMobile" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                                        TargetControlID="txt_MobNo" ID="AutoCompleteExtender2" UseContextKey="true" FirstRowSelected="false">
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div3">
                                        <label class="control-label col-sm-12" id="lblEmail">Email</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_EmailID" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Email" onkeypress="return event.keyCode != 13;" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel runat="server" ID="upAppointmentDate" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div6">
                                                <label class="control-label col-sm-12" id="lblAppointmentDate">Appointment Date<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtAppointmentDate" runat="server" CssClass="form-control login_input_sign_up" placeholder="Select Appointment Date"></asp:TextBox>
                                                    <input type='text' runat="server" id="txtdate" class="form-control login_input_sign_up" placeholder="DD Month YYYY" onpaste="return false" visible="false" />
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div7">
                                        <label class="control-label col-sm-12" id="lblAppointmentTime">Appointment Time</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtAppointmentTime" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Appointment Time" TextMode="Time" onkeypress="return event.keyCode != 13;" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div4">
                                        <label class="control-label col-sm-12" id="lblOrganization">Organization</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtOrganisationName" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Organization Name" onkeypress="return event.keyCode != 13;" MaxLength="50"></asp:TextBox>
                                            <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender7" runat="server" ServiceMethod="GetVisitorListByOrganization" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                                TargetControlID="txtOrganisationName" UseContextKey="true" FirstRowSelected="false">
                                            </ajaxToolkit:AutoCompleteExtender>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel runat="server" ID="upPurposeofVisit" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div8">
                                                <label class="control-label col-sm-10" id="lblPurpose">Purpose of Visit<span class="mandatoryStar">*</span></label>
                                                <asp:Button ID="btn_purposeofvisit1" runat="server" CssClass="btnnVisit float-right" Text="+" Font-Bold="true" OnClick="addpurposeofvisit1" />
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txt_purposeofvisit" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Purpose Of Visit" Visible="false" AutoPostBack="true" OnTextChanged="txt_purposeofvisit_TextChanged"></asp:TextBox>
                                                    <asp:DropDownList ID="ddlVisitorType" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" placeholder="Enter Purpose Of Visit"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="col-12 float-left padding-none">
                                    <asp:UpdatePanel runat="server" ID="upDepartment" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div9">
                                                <label class="control-label col-sm-12" id="lblDepartment">Department</label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlDeptToVisit" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddlDeptToVisit_SelectedIndexChanged"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdatePanel runat="server" ID="upWhomtovisit" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div10">
                                                <label class="control-label col-sm-12" id="lblWhomToVisit">Whom To Visit</label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlWhomToVisit" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                        </div>
                                    <div class="form-group col-lg-12 padding-none float-left" runat="server" id="div11">
                                        <label class="control-label col-sm-12" id="lblRemarks">Remarks</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_Remarks" runat="server" CssClass="form-control " placeholder="Enter Remarks" TextMode="MultiLine" Rows="2" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 padding-none float-left" runat="server" id="divcomments" visible="false">
                                        <label class="control-label col-sm-12" id="lblComments">Comments<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtVEditComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="2" MaxLength="100" ></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 top bottom float-left" style="text-align: right;">
                                <asp:UpdatePanel ID="upAppointmentButtons" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Button ID="btnCreate" Text="Submit" CssClass=" btn-signup_popup" runat="server" OnClientClick="javascript:return Submitvaidate();" OnClick="btnCreate_Click" />
                                        <asp:Button ID="btnCancel" Text="Reset" CssClass=" btn-revert_popup" runat="server" OnClick="btnCancel_Click" />
                                        <asp:Button ID="btnAppointmentUpdate" Text="Update" CssClass=" btn-signup_popup" runat="server" OnClientClick="javascript:return Updatevalidate();" OnClick="btnAppointmentUpdate_Click" Visible="false" />
                                        <button type="button" class=" btn-cancel_popup" data-dismiss="modal" onclick="OpenModelViewVisitor();">Cancel</button>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div>
                                <asp:Label ID="lblmesg" runat="server" ForeColor="Red" Font-Size="Smaller"></asp:Label>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!--End ModalPopUp for Visitor Appointment Register-->

    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />

 

    <!-------- File VIEW Print in Model-------------->
    <div id="myModal_lock" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 86%; height: 500px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <asp:UpdatePanel ID="UpHeading" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <span id="span_VisitorTitle" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpViewDL" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div >
                                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!-------- End File VIEW Print in Model-------------->

    <!--------Start Confirm Box for Update-------------->
    <script>
        function ConfirmAlertUpdate() {
            custAlertMsg('Do you want to update', 'confirm', true);
            document.getElementById("<%=hdnCustom.ClientID%>").value = "Update";
        }
    </script>
    <!--------End Confirm Box for Update-------------->

    <!--Validation Visitor Appointment Register-->
    <script type="text/javascript">
        function Submitvaidate() {
            var AVisitorName = document.getElementById("<%=txt_VisitorName.ClientID%>").value;
            var AVisitorLastName = document.getElementById("<%=txtLastName.ClientID%>").value;
             var VMobile = document.getElementById("<%=txt_MobNo.ClientID%>").value;
             var VADate = document.getElementById("<%=txtAppointmentDate.ClientID%>").value;
            errors = [];
            if (AVisitorName.trim() == "") {
                errors.push("Please enter first name.");
            }
            if (AVisitorLastName.trim() == "") {
                errors.push("Please enter last name.");
            }
            if (VMobile == "") {
                errors.push("Please enter mobile number.");
            }
            else {
                if (VMobile.length != 10) {
                    errors.push("Mobile no should be 10 digits.");
                }
            }
            if (VADate == "") {
                errors.push("Please select appointment date.");
            }
            var ddlPurposeofVisit = $('#<%=ddlVisitorType.ClientID%>');
            var isVisible5 = ddlPurposeofVisit.is(':visible');
            if (isVisible5 == true) {
                var VPurpose = document.getElementById("<%=ddlVisitorType.ClientID%>").value;
                    if (VPurpose == 0) {
                        errors.push("Please select purpose of visit.");
                    }
                }
                var txtPurposeofVisit = $('#<%=txt_purposeofvisit.ClientID%>');
                var isVisible6 = txtPurposeofVisit.is(':visible');
                if (isVisible6 == true) {
                    var VtxtPurpose = document.getElementById("<%=txt_purposeofvisit.ClientID%>").value;
                    if (VtxtPurpose.trim() == "") {
                        errors.push("Please enter purpose of visit.");
                    }
                }

            var email = document.getElementById("<%=txt_EmailID.ClientID%>").value;
            if (email.length > 0) {
                if (!IsValidEmail(email)) {
                    errors.push("Invalid email address.");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
        }
    </script>

    <script>
        function Updatevalidate() {
            var AVisitorName = document.getElementById("<%=txt_VisitorName.ClientID%>").value;
             var AVisitorLastName = document.getElementById("<%=txtLastName.ClientID%>").value;
             var VMobile = document.getElementById("<%=txt_MobNo.ClientID%>").value;
            var VADate = document.getElementById("<%=txtAppointmentDate.ClientID%>").value;
            var VAPurpose = document.getElementById("<%=ddlVisitorType.ClientID%>").value;
            var Comments = document.getElementById("<%=txtVEditComments.ClientID%>").value;
            errors = [];
            if (VADate == "") {
                errors.push("Please select appointment date.");
            }
            if (AVisitorName.trim() == "") {
                errors.push("Please enter first name.");
            }
            if (AVisitorLastName.trim() == "") {
                errors.push("Please enter last name.");
            }
            if (VMobile == "") {
                errors.push("Please enter mobile number.");
            }
            else {
                if (VMobile.length != 10) {
                    errors.push("Mobile no should be 10 digits");
                }
            }
            if (VAPurpose == 0 || VAPurpose == "") {
                errors.push("Please select purpose of visit.");
            }
            if (Comments == 0) {
                errors.push("Please enter comments.");
            }

            var email = document.getElementById("<%=txt_EmailID.ClientID%>").value;
            if (email.length > 0) {
                if (!IsValidEmail(email)) {
                    errors.push("Invalid email address.");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            ConfirmAlertUpdate();
        }
    </script>
    <!--End Validation Visitor Appointment Register-->

   
    <!----To Load Calendar for Appointment Date---->
    <script>
        $(document).ready(function () {
            //Binding Code
            $(function () {
                $('#<%=txtAppointmentDate.ClientID%>').datetimepicker({
                    format: 'DD MMM YYYY',
                  
                });
            });
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                //Binding Code Again
                $(function () {
                    $('#<%=txtAppointmentDate.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                         
                    });
                });
            }
        });
    </script>
    <!----End To Load Calendar for Appointment Date---->

    <!----Visitor Related Funtions get id from Datatable---->
    <script>
        function ViewVisitor_List(SNo) {
            $("#<%=hdnPkID.ClientID%>").val(SNo);
              $("#<%=btnEdit_List.ClientID%>").click();
        }
        function CheckOutVisitor(SNo) {
            $("#<%=hdnPkID.ClientID%>").val(SNo);
              $("#<%=btnCheckOut.ClientID%>").click();
          }
          function VisitorPassPrint(SNo) {
              $("#<%=hdnPkID.ClientID%>").val(SNo);
            $("#<%=btnPrintPass.ClientID%>").click();
          }
    </script>
    <!----End Visitor Related Funtions get id from Datatable---->

    <!--Visitor Jquery Datatable-->
    <script>
          //function visitordt() {
              $('#dtVisitorList').wrap('<div class="dataTables_scroll" />');
              var otable = $('#dtVisitorList').DataTable({
                  columns: [
                      { 'data': 'SNo' },//0
                      { 'data': 'RowNumber' },//1
                      { 'data': 'VisitorID' },//2
                      { 'data': 'VisitorName' },//3
                      { 'data': 'MobileNo' },//4
                      { 'data': 'DeptName' },//5
                      { 'data': 'EmpName' },//6
                      { 'data': 'PurposeofVisitTxt' },//7
                      { 'data': 'Organization' },//8
                      { 'data': 'InDateTime' },//9
                      { 'data': 'OutDateTime' },//10
                      {
                          "mData": null,
                          "bSortable": false,
                          "mRender": function (o) { return '<a  class="Edit" title="Edit" href="#" onclick="ViewVisitor_List(' + o.SNo + ');">' + '' + '</a>'; }
                      }
                  ],
                 
                  "lengthMenu": [[10, 15, 25, 100], [10, 15, 25, 100]],
                  "aoColumnDefs": [{ "targets": [0, 1], "visible": false, "searchable": false }, { className: "dt-body-left", "targets": [2, 3, 5, 6, 7, 8] },
                  {
                      targets: [10], render: function (a, b, data, d) {
                          if (data.OutDateTime == 'N/A') {
                              return '<a class="btngridCheckOut_VMS" href="#" title="Checkout" onclick="CheckOutVisitor(' + data.SNo + ');">' + '' + '</a>';
                          } else {
                              return data.OutDateTime;
                          }
                          return '';
                      }
                  },
                  {
                      targets: [12], render: function (a, b, data, d) {
                          if (data.OutDateTime == 'N/A') {
                              return '<a  class="print" title="Print" href="#" onclick="VisitorPassPrint(' + data.SNo + ');">' + '' + '</a>';
                          } else {
                              return '<a class="print" title="Print" href="#" onclick="VisitorPassPrint(' + data.SNo + ');">' + '' + '</a>';
                          }
                          return '';
                      }
                  },],
                  "order": [[1, "desc"]],
                  'bAutoWidth': false,
                  "sServerMethod": 'Post',
                  "sAjaxSource": '<%= ResolveUrl("/VMS/WebService/VMS_Service.asmx/GetVisitorList" )%>',
                  "fnServerData": function (sSource, aoData, fnCallback) {
                      aoData.push();                      
                      $.ajax({
                          "dataType": 'json',
                          "contentType": "application/json; charset=utf-8",
                          "type": "GET",
                          "url": sSource,
                          "data": aoData,
                          "success": function (msg) {
                              var json = jQuery.parseJSON(msg.d);
                              fnCallback(json);
                              $("#dtVisitorList").show();
                              $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                              $(".tblVisitorRegister").css({ "width": "100%" });
                          },
                          error: function (xhr, textStatus, error) {
                              if (typeof console == "object") {
                                  console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                              }
                          }
                      });
                  }
              });
          //}

        function ReloadVisitortable() {
            otable.ajax.reload();
        }
    </script>
    <!--End Visitor Jquery Datatable-->

    <!----Appointment Related Funtion get id from Datatable---->
    <script>
        function ViewAppointment_List(Appointment_SNO) {
            $("#<%=hdnPkIDAppointment.ClientID%>").val(Appointment_SNO);            
            $("#<%=btnAppointmentEdit_List.ClientID%>").click();
        }
    </script>
    <!----End Appointment Related Funtion get id from Datatable---->
  
    <!--Appointment Jquery Datatable-->
    <script>
        

            $('#dtAppointment').wrap('<div class="dataTables_scroll" />');
            var oAtable = $('#dtAppointment').DataTable({
                columns: [
                    { 'data': 'Appointment_SNO' },//0
                    { 'data': 'RowNumber' },//1
                    { 'data': 'Appointment_ID' },//2
                    { 'data': 'Appointment_NAME' },//3
                    { 'data': 'Appointment_MOBILE_NO' },//4
                    { 'data': 'PurposeofVisitTxt' },//5
                    { 'data': 'Appointment_ORGANISATION' },//6
                    { 'data': 'DeptName' },//7
                    { 'data': 'EmpName' },//8
                    { 'data': 'Appointment_DATE' },//9
                    { 'data': 'AppointmentStatusName' },//10
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) { return '<a  class="Edit" title="Edit" href="#" onclick="ViewAppointment_List(' + o.Appointment_SNO + ');">' + '' + '</a>'; }
                    }
                ],
               
                "lengthMenu": [[10, 15, 25, 100], [10, 15, 25, 100]],
                "aoColumnDefs": [{ "targets": [0,1, 10], "visible": false, "searchable": false },
                {
                    "className": "dt-body-left", "targets": [2, 3, 4, 5, 6, 7, 8]
                }],
                "order": [[1, "desc"]],                             
                'bAutoWidth': false, 
                "sServerMethod": "Post",
                "sAjaxSource": '<%= ResolveUrl("/VMS/WebService/VMS_Service.asmx/GetAppointmentList" )%>',
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push();
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#dtAppointment").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblVisitorApp_ListClass").css({ "width": "100%" });

                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        //}

        function Reloadtable() {
            oAtable.ajax.reload();
            $('#ModalVisitorAppointment').modal('hide');
           
        }
    </script>
    <!--End Appointment Jquery Datatable-->

    <!----Visitor to clear fields open Model---->
    <script>
        function OpenModelViewVisitor() {
            document.getElementById("<%=txt_VisitorName.ClientID%>").value = '';
            document.getElementById("<%=txtLastName.ClientID%>").value = '';
            document.getElementById("<%=txtAppointmentDate.ClientID%>").value = '';
            document.getElementById("<%=txt_MobNo.ClientID%>").value = '';
            document.getElementById("<%=txt_EmailID.ClientID%>").value = '';
            document.getElementById("<%=txtAppointmentTime.ClientID%>").value = '';
            document.getElementById("<%=txtOrganisationName.ClientID%>").value = '';
            document.getElementById("<%=txt_Remarks.ClientID%>").value = '';
            $('#ModalVisitorAppointment').modal('show');
        }
    </script>
    <!----End Visitor to clear fields open Model---->

    <!----Visitor to open Model Edit Click---->
    <script>
        function OpenModelVisitorEdit() {
            $('#ModalVisitorAppointment').modal('show');
        }
    </script>

     <script>
         $(document).ready(function () {
               $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust().responsive.recalc();
    });    


    //$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
    //    var target = $(e.target).attr("href"); // activated tab
    //    $($.fn.dataTable.tables( true ) ).css('width', '100%');
    //    $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
    //} ); 

         });
          </script>

    <!----End Visitor to open Model Edit Click---->

    <!----Get Client Datetime for Visitor CheckOut---->
    <script>
        window.onload = function () {
            getVisitorCheckoutDate();
        };
        function getVisitorCheckoutDate() {
            var dtTO = new Date();
            var Todate = document.getElementById("<%=hdnCheckout.ClientID%>");
            Todate.value = formatDate(dtTO);
        }
        function formatDate(dateObj) {
            var m_names = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");
            var curr_date = dateObj.getDate();
            var curr_month = dateObj.getMonth();
            var curr_year = dateObj.getFullYear();
            var curr_min = dateObj.getMinutes();
            var curr_hr = dateObj.getHours();
            var curr_sc = dateObj.getSeconds();
            return curr_date + " " + m_names[curr_month] + " " + curr_year + " " + curr_hr + ":" + curr_min + ":" + curr_sc;
        }
    </script>
    <!----End Get Client Datetime for Visitor CheckOut---->

    <!----To Hide Print Icon---->
    <script>
        function HidePrintbtn(pr, co) {
            var table = $('#dtVisitorList').DataTable();
            if (pr == 0)
                table.column(12).visible(false);
            if (co == 0)
                table.column(10).visible(false);
        }
    </script>
    <!----To Hide Print Icon---->
   
   <script>
        function myfunctionVisitortab1() {
            
            $('.Visitortab1').addClass('active');
            $('.Appointmenttab2').removeClass('active');
            $('.NewDocumentTabBody').addClass('active');
            $('.NewDocumentTabBody').addClass('show');
            $('.NewDocumentTabBody1').removeClass('active');
            $('.NewDocumentTabBody1').removeClass('show');
            // });
        }
        function myfunctionAppointmenttab2() {
            
            $('.Visitortab1').removeClass('active');
            $('.Appointmenttab2').addClass('active');
            $('.NewDocumentTabBody').removeClass('active');
            $('.NewDocumentTabBody').removeClass('show');
            $('.NewDocumentTabBody1').addClass('active');
            $('.NewDocumentTabBody1').addClass('show');
            
        }
   </script>
  
    
</asp:Content>
