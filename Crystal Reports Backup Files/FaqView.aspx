﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="FaqView.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TestSamples.FaqView" %>

<%@ Register Src="~/UserControls/TMS_UserControls/ucViewDocFAQs.ascx" TagPrefix="uc1" TagName="ucViewDocFAQs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
   <script src="<%=ResolveUrl("~/AppScripts/Pager.js")%>"></script>
 
    <uc1:ucViewDocFAQs runat="server" id="ucViewDocFAQs" />
    <script>
        $('#modalDocFAQs').modal({ backdrop: 'static', keyboard: false });
    </script>
</asp:Content>
