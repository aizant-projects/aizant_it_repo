﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AizantIT_PharmaApp.Admin.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Admin Login</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="../Content/bootstrap.css" rel="stylesheet" />
    <script src="../Scripts/jquery-3.2.1.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
        <link href="<%# ResolveUrl("~/AppCSS/AppStyles.css")%>" rel="stylesheet" />
        <link href="<%# ResolveUrl("~/AppCSS/CustAlerts/custAlert.css")%>" rel="stylesheet" />
     <link href="../AppCSS/UserLogin/UserLoginStyles.css" rel="stylesheet" />
</head>
<body class="bg_login">
    
    <form id="form1" runat="server">
       <div class="col-lg-12 padding-none">
            <div class=" col-lg-3 col-md-5 col-sm-12 col-xs-12 login_panel_left  pull-left padding-none login-panel">
                <div class="col-lg-12 logo">
                    <img src="<%= ResolveUrl("~/Images/UserLogin/Aizant_Logo.png")%>" class="center-block img-responsive" alt="Aizant" />
                </div>
                 <div id="signIn" runat="server">
                    <div class="col-sm-12 login_icon_bg">
                        <div class="col-lg-12 col-lg-offset-3  col-sm-offset-3 user_login">
                            Admin Login
                        </div>
                    </div>
                    <div class="form-horizontal ">
                        <div class="col-lg-12 login_details"  style="margin-top:10px;">
                                  <div class="form-group">
                                        <label for="txtLoginUserName" class="col-sm-12 control-label custom_label_login ">Login ID</label>
                                        <div class="col-sm-12">
                                           <input type="text" class="form-control" id="txtUserName" placeholder="Login ID" runat="server" required="required"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtLoginPassword" class="col-sm-12 control-label custom_label_login">Password</label>
                                        <div class="col-sm-12">
                                              <input type="password" class="form-control" id="txtPassword" placeholder="Password" runat="server" required="required"/>
                                        </div>
                                    </div>
                                    
                               <div class="form-group">
                                <div class="col-sm-12" style="margin-top:10px;">
                                     <asp:Button ID="btnLogin" CssClass="col-sm-6  pull-right btn btn-signup" runat="server" Text="Log In" OnClick="btnLogin_Click"/>
                                 </div>
                            </div>
                           
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <asp:Label ID="lblMsg" runat="server"  ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                             <div class="form-group">
                                <div class="col-sm-12" style="opacity:0.6">
                                    <img src="<%= ResolveUrl("~/Images/UserLogin/Aizant_bg.png")%>" class="center-block img-responsive" width="29%" alt="Aizant" />
                                    </div>
                                </div>
                             <div class="form-group">
                                <div class="col-sm-12  copyrights" >
                                    Copyright © 2018 Aizant Global Analytics, All Rights Reserved
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                </div>
        <div>
       </div>
           <
    </form>
</body>
</html>
