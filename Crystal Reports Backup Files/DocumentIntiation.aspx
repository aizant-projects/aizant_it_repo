﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.Master" AutoEventWireup="true" CodeBehind="DocumentIntiation.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentIntiator.DocumentIntiation" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<link href="<%=ResolveUrl("~/AppCSS/DMS/DMS_StyleSheet1.css")%>" rel="stylesheet" />--%>
    <style type="text/css">
        .hidden {
            display: none;
        }
       table tbody tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            height: 36px;
        }

         table tbody tr td {
            text-align: center;
            height: 30px;
        }

        .buttoninitiation {
            position: fixed !important;
            bottom: 15px;
            z-index: 999;
            left: -11px;
        }

       
        .bottom {
            margin-bottom: 10px;
        }

        .ajax__calendar_body {
            z-index: 100004;
        }

        textarea {
            resize: none;
        }

        .dropdown-menu {
            max-height: auto !important;
        }
        .btnnewdoc_edit {
    background: url(../../Images/GridActionImages/edit.png) no-repeat;
    padding: 3px 12px;
    text-align: center;
    border: none;
    cursor: pointer;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:LinkButton ID="lnk_Dashboard" runat="server" class=" btn-signup_popup float-right launch-modal" PostBackUrl="~/DMS/DMSHomePage.aspx">Dashboard</asp:LinkButton>
    </div>
    <asp:UpdatePanel ID="UpdatePanel7" UpdateMode="Always" runat="server">
        <ContentTemplate>
            <div class="  col-lg-12 col-12 col-md-12 col-sm-12 float-left padding-none">
                <div class=" col-lg-12 col-12 col-md-12 col-sm-12 float-left padding-none dms_outer_borderScroll">
                    <div class="col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left " id="divMainContainer" runat="server" visible="true">
                        <div class="panel panel-default ">
                            <div class="  grid_header   dms_header">
                                <asp:Label ID="lblheding" runat="server"></asp:Label>
                            </div>
                            <div class="panel-body dms_btn_border" style="background: none;">
                                <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left grid_panel_full border_top_none ">
                                    <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none top float-left ">
                                        <div class="form-group  paddin-none col-md-3 col-lg-3 col-6 col-sm-6 float-left">
                                            <asp:Label ID="lbl_requestType" runat="server" Text="Request Type"  CssClass="label_name"></asp:Label><span id="spanReqType" class="smallred_label">*</span>
                                            <asp:DropDownList ID="ddl_requestType" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker regulatory_dropdown_style drop_down padding-none" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddl_requestType_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div id="id1" class="form-group input-group col-md-3 col-lg-3 col-6 col-sm-6" >
                                            <asp:Label ID="Label10" runat="server" Visible="false" Text="Document ID"  CssClass="label_name"></asp:Label>
                                            <asp:TextBox ID="txt_DocumentID" runat="server" CssClass="form-control" Visible="false" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group  col-md-3 col-lg-3 col-6 col-sm-6 float-left" id="div_dept1" style="display:none;">
                                            <asp:Label ID="Label5" runat="server" Text="Department "  CssClass="label_name"></asp:Label><span id="spanDept" class="smallred_label">*</span>
                                            <asp:UpdatePanel ID="UpddlDeptname" UpdateMode="Always" runat="server">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddl_DeptName" runat="server" data-size="9" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker regulatory_dropdown_style drop_down padding-none " data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddl_DeptName_SelectedIndexChanged"></asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div id="type1" class="form-group  col-md-2 col-lg-3 col-6 col-sm-6 float-left" style="display:none;">
                                            <asp:Label ID="Label2" runat="server" Text="Document Type"  CssClass="label_name"></asp:Label><span id="spanDocType" class="smallred_label">*</span>
                                            <asp:UpdatePanel ID="Upddl_DocumentType" runat="server" UpdateMode="Always">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddl_DocumentType" runat="server" data-size="9" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker regulatory_dropdown_style drop_down padding-none " data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddl_DocumentType_SelectedIndexChanged"></asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <asp:UpdatePanel ID="UpDocNub" runat="server">
                                            <ContentTemplate>
                                                <div class="form-group  col-md-4 col-lg-3 col-6 col-sm-6 float-left" id="DocumentNumber" style="display:none;">
                                                    <div class="col-12 padding-none">
                                                        <asp:Label ID="Label3" runat="server" Text="Document Number"  CssClass="label_name"></asp:Label><span id="spanDocNo" class="smallred_label">*</span>
                                                        <asp:Button ID="btnDocNumcheck" CssClass="check_button float-right" runat="server" Text="Check!" OnClick="btnDocNumcheck_Click" ToolTip="Document Number Availability" OnClientClick="showImg();"/>
                                                    </div>
                                                    <asp:TextBox ID="txt_DocumentNumber" runat="server" CssClass="form-control login_input_sign_up"  placeholder="Enter Document Number" MaxLength="30" autocomplete="off" onkeypress="return RestrictPercentageSquareBraces(event);"></asp:TextBox>
                                                    <asp:Label ID="lbl_number" runat="server" CssClass="label_status" Visible="false"></asp:Label>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                     <div class="col-12 float-left padding-none" id="hidecntrls2" style="display:none;">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <div id="div_Namenew1" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                                <div class="col-12 padding-none">
                                                    <asp:Label ID="Label4" runat="server" Text="Document Name"  CssClass="label_name"></asp:Label><span id="spanDocName" class="smallred_label">*</span>                                                   
                                                    <asp:Button ID="btnDocNamecheck" CssClass="check_button  float-right" runat="server" Text="Check!" OnClick="btnDocNamecheck_Click" ToolTip="Document Name Availability" OnClientClick="showImg();"/>
                                                </div>
                                                <%--<textarea id="txt_DocumentName" runat="server" class="form-control" placeholder="Enter Document Name"  maxlength="10" onkeypress="return RestrictPercentageSquareBraces(event);"></textarea>--%>
                                                <textarea id="txt_DocumentName" runat="server" class="form-control" placeholder="Enter Document Name"  maxlength="300" onkeypress="return RestrictPercentageSquareBraces(event);"></textarea>
                                                <asp:Label ID="lblStatus" runat="server" CssClass="label_status" Visible="false"></asp:Label>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdatePanel ID="UpDocumentName" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div id="div_NameUpdate1" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                                <asp:Label ID="Label6" runat="server" Text="Document Name"  CssClass="label_name"></asp:Label><span class="smallred_label">*</span>
                                                <asp:Button ID="btnNewDocName" CssClass=" btnnewdoc_edit float-right" runat="server" OnClick="btnNewDocName_Click" ToolTip="Enter New Document Name"/>
                                                <asp:DropDownList ID="ddl_AutoDocName" data-size="9" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 padding-none selectpicker regulatory_dropdown_style drop_down " data-live-search="true" placeholder="Select Document" AutoPostBack="true" OnSelectedIndexChanged="ddl_AutoDocName_SelectedIndexChanged"></asp:DropDownList>
                                            </div>
                                       </ContentTemplate>
                                        <Triggers>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <%--<div class="form-group col-md-6 col-lg-6 col-12 col-sm-12 float-left" id="div6">
                                        <asp:Label ID="Label11" runat="server" Text="Document Name"  CssClass="label_name"></asp:Label>
                                        <asp:TextBox ID="txt_AutoDocName" runat="server" CssClass="form-control" ReadOnly="true" placeholder="Select Document"></asp:TextBox>
                                    </div>--%>
                                    <div id="div_Purpose" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                        <asp:Label ID="lblComments" runat="server"  CssClass="label_name"></asp:Label><span id="spanComments" class="smallred_label">*</span>
                                        <textarea id="txt_Comments" runat="server" class="form-control" MaxLength="500" onkeypress="return ValidateFreeText(event);"></textarea>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-12 col-sm-12  padding-none float-left">
                                        <asp:UpdatePanel ID="upversid" runat="server">
                                            <ContentTemplate>
                                                <div class="form-group  col-md-6 col-xl-6  col-lg-6 col-6 col-sm-6 float-left" id="div_Version">
                                                    <asp:Label ID="Label9" runat="server" Text="Version"  CssClass="label_name"></asp:Label>
                                                    <asp:TextBox ID="txt_VersionID" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        
                                        <div id="divPublic" class="form-group  col-md-6 col-xl-6 col-12 col-lg-6 col-sm-12 float-left">
                                            <asp:Label ID="lblRadiobutton" runat="server" Text="Privacy"  CssClass="label_name"></asp:Label><span class="smallred_label">*</span>
                                            <asp:RadioButtonList runat="server" ID="RadioButtonList1" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Public" Value="yes" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Private" Value="no"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                         <div class="col-md-12 col-lg-12 col-12 col-sm-12  padding-none float-left">
                                             <div id="div_initiator" class="col-md-4 col-xl-4  col-lg-4 col-6 col-sm-6 test form-group float-left">
                                                    <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none">
                                                        <asp:Label ID="LabelInit" runat="server" Text="Initiator"  CssClass="label_name"></asp:Label><span class="smallred_label">*</span>
                                                    </div>
                                                    <asp:DropDownList ID="ddl_Initiator" data-size="9" runat="server" CssClass=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none selectpicker regulatory_dropdown_style drop_down float-left" data-live-search="true" placeholder="Select Initiator Name/ID"></asp:DropDownList>
                                                </div>
                                         <asp:UpdatePanel ID="Upddl_createdBy" runat="server">
                                            <ContentTemplate>
                                                <div id="div_Userroles11" class="col-md-4 col-xl-4  col-lg-4 col-6 col-sm-6 test form-group float-left">
                                                    <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none">
                                                        <asp:Label ID="Label7" runat="server" Text="Author"  CssClass="label_name"></asp:Label><span class="smallred_label">*</span>
                                                    </div>
                                                    <asp:DropDownList ID="ddl_CreatedBy" data-size="9" runat="server" CssClass=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none selectpicker regulatory_dropdown_style  drop_down float-left" data-live-search="true" placeholder="Select Creator Name/ID" AutoPostBack="true" OnSelectedIndexChanged="ddl_CreatedBy_SelectedIndexChanged"></asp:DropDownList>
                                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="UPAuthorize" runat="server">
                                            <ContentTemplate>
                                                <div id="div1" class="col-md-4 col-xl-4  col-lg-4 col-sm-6 form-group float-left">
                                                    <asp:Label ID="Label8" runat="server" Text="Authorizer"  CssClass="label_name"></asp:Label>
                                                    <asp:DropDownList ID="ddl_authorizedBy" data-size="9" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 padding-none selectpicker regulatory_dropdown_style drop_down " data-live-search="true" OnSelectedIndexChanged="ddl_authorizedBy_SelectedIndexChanged" AutoPostBack="true" placeholder="Select Authorizer Name/ID" ClientIDMode="Static"></asp:DropDownList>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddl_authorizedBy" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                             </div>
                                    <div class="form-group float-right col-md-4 col-lg-4 col-6 col-sm-6 padding-none float-left" style="display: none;">
                                        <asp:Button ID="btnApply" runat="server" CssClass=" btn-sub float-right" Text="Apply" Visible="false" />
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-12 col-sm-12 top   float-left " id="divQA">
                                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 top padding-none bottom float-left">
                                            <div class="grid_header">
                                                Roles
                                            </div>
                                            <div class="padding-none col-md-12 col-lg-12 col-12 col-sm-12 bottom float-left border_top_none grid_panel_full">
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <div class="form-group col-md-12 col-lg-3 col-12 col-sm-12 float-left">
                                                            <label class="padding-none col-md-12 col-lg-12 col-12 col-sm-12 control-label custom_label_answer">Role<span class="smallred_label">*</span></label>
                                                            <asp:DropDownList ID="ddlRoles" data-size="9" runat="server" data-live-search="true" CssClass="padding-none col-md-12 col-lg-12 col-12 col-sm-12 selectpicker regulatory_dropdown_style" OnSelectedIndexChanged="ddlRoles_SelectedIndexChanged" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <div class="form-group col-md-12 col-lg-3 col-12 col-sm-12 float-left">
                                                            <label class="padding-none col-md-12 col-lg-12 col-12 col-sm-12 control-label custom_label_answer">Department<span class="smallred_label">*</span></label>
                                                            <asp:DropDownList ID="ddlDepartmentQA" data-size="9" runat="server" data-live-search="true" CssClass="padding-none col-md-12 col-lg-12 col-12 col-sm-12 selectpicker regulatory_dropdown_style" OnSelectedIndexChanged="ddlDepartmentQA_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <div class="form-group  col-md-12 col-lg-3 col-12 col-sm-12 float-left">
                                                            <label id="lblEmployeeList" class="padding-none col-md-12 col-lg-12 col-12 col-sm-12 control-label custom_label_answer">Employee<span class="smallred_label">*</span></label>
                                                            <asp:ListBox ID="lstQA" SelectionMode="Multiple" data-live-search="true" data-size="9" CssClass="getselecteditems col-lg-12 col-sm-12 col-12 col-md-12 selectpicker padding-none regulatory_dropdown_style"  data-selected-text-format="count" data-live-search-placeholder="Select Employees" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstQA_SelectedIndexChanged"></asp:ListBox><%--onchange="MoveToTop(this)" AutoPostBack="true" OnSelectedIndexChanged="lstQA_SelectedIndexChanged"  --%>
                                                            <asp:ListBox ID="SinglelstQA" data-size="9" CssClass=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none selectpicker regulatory_dropdown_style" data-live-search="true" data-selected-text-format="count" data-live-search-placeholder="Select Employee" runat="server" SelectionMode="Single" Visible="false"></asp:ListBox>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-lg-3 col-md-12 col-12 col-sm-12 bottom top_panel float-left top">
                                                            <asp:Button type="button" ID="btnQA" runat="server" Text="Add" CssClass=" btn-signup_popup" OnClientClick="return AddQA();" Style="margin-top: 10px; text-align: right;" ValidationGroup="rev" OnClick="btnQA_Click" />
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <asp:UpdatePanel ID="approverDivGV" runat="server" UpdateMode="Always">
                                                <ContentTemplate>
                                                    <div class="col-lg-12  ReivewerGrid float-left bottom padding-none" id="RolesDivGV" runat="server" visible="false">
                                                        <div class="panel-heading_title col-md-12 col-lg-12 col-12 col-sm-12 float-left">
                                                            Selected Employees
                                                        </div>
                                                        <div class="col-12 float-left grid_panel_full ">
                                                        <asp:GridView ID="gvApprover" CssClass="col-md-12 col-lg-12 col-12 col-sm-12  top bottom" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvApprover_RowDataBound" OnRowCommand="gvApprover_RowCommand">
                                                            <Columns>
                                                                <asp:BoundField DataField="Role" HeaderText="Role" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                                                <asp:BoundField DataField="EmpName" HeaderText="Employee" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                                                <asp:BoundField DataField="DeptCode" HeaderText="Dept Code" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="lblEmpId" Text='<%#Eval("EmpID")%>' CssClass="hidden" runat="server" Font-Bold="true"></asp:TextBox>
                                                                        <asp:TextBox ID="lblDeptID" Text='<%#Eval("DeptID")%>' runat="server" Font-Bold="true" Visible="false"></asp:TextBox>
                                                                        <asp:TextBox ID="lblStatus" Text='<%#Eval("Status")%>' runat="server" Font-Bold="true" Visible="false"></asp:TextBox>
                                                                        <asp:TextBox ID="lblDocStatus" Text='<%#Eval("DocStatus")%>' runat="server" Font-Bold="true" Visible="false"></asp:TextBox>
                                                                        <asp:TextBox ID="lblActiveStatus" Text='<%#Eval("ActiveStatus")%>' runat="server" Font-Bold="true" Visible="false"></asp:TextBox>
                                                                        <asp:LinkButton ID="lnkEdit" Text="" CommandArgument='<%# Container.DataItemIndex %>' Font-Size="Large" CommandName="edt" CssClass="Edit" runat="server" Font-Bold="true" Visible="false"></asp:LinkButton>
                                                                        <asp:LinkButton ID="lnkDelete" Text="" CommandArgument='<%# Eval("EmpID") %>' Style="padding: 3px !important" runat="server" CssClass="delete_icon" OnClientClick="return GetSelectedRowDelete(this)" OnClick="lnkDelete_Click"><i class="glyphicon glyphicon-trash" title="Remove" style="color:red"></i></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                            <div class="float-left col-12 padding-none" id="divinactive" style="display:none">
                                                        <div class=" float-left inactive_emp_leg"></div><span class="inactive_text"> In-Active Employee  </span>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="col-md-12 col-lg-12 col-12 col-sm-12 form-group bottom float-left" id="divgvcomments" style="">
                                                <asp:Label ID="lbl_Comments" runat="server" Text="Comments" CssClass="label_name"></asp:Label>
                                                <asp:TextBox ID="txt_RevertComments" runat="server" TextMode="MultiLine" CssClass="form-control" Rows="2" placeholder="Enter Comments" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="col-lg-12 form-group bottom float-left" id="divRemarks">
                                        <asp:Label ID="lbl_Remarks" runat="server" CssClass="label_name">Remarks:<span class="smallred_label">*</span></asp:Label>
                                        <textarea ID="txt_Remarks" runat="server" class="form-control" MaxLength="500"></textarea>
                                    </div>
                                </div>
                                     <div class="col-md-12   float-left bottom">
                                <div id="div_buttons1" runat="server" visible="false" align="right">
                                    <asp:UpdatePanel ID="upcancelDocInition" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btn_Update" Text="Update" CssClass=" btn-signup_popup" runat="server" OnClick="btn_Update_Click" ValidationGroup="DMS_DC" />
                                            <input type="button" id="btn_Reject" runat="server" class=" btn-revert_popup" onclick="return commentRequiredReject();" value="Reject"/>
                                            <asp:Button ID="btn_Submit" Text="Submit" CssClass=" btn-signup_popup" runat="server" OnClick="btn_Submit_Click" ValidationGroup="DMS_DC" />
                                            <asp:Button ID="btn_Reset" Text="Reset" CssClass=" btn-revert_popup" runat="server" OnClick="btn_Reset_Click" CausesValidation="false" />
                                            <asp:Button ID="btn_Cancel" Text="Cancel" CssClass=" btn-cancel_popup" runat="server" OnClick="btn_Cancel_Click" CausesValidation="false" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            </div>
                              </div>
                           
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdnDeleteEmpIDRow" runat="server" />
    <asp:HiddenField ID="hdnDeleteRoleRow" runat="server" />
    <asp:HiddenField ID="hdnEditDeptIDRow" runat="server" />
    <asp:HiddenField ID="hdnReviewer" Value="0" runat="server" />
    <asp:HiddenField ID="hdnConfirm" runat="server" />
    <asp:HiddenField ID="hdfPublic" runat="server" />
    <asp:HiddenField ID="hdfDocNameChange" Value="0" runat="server" />
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
     
    <script>
        $(document).ready(function () {
            $('.closebtn').on('click', function () {
                $('#paragraph2').addClass('col-12');
                $('#paragraph2').removeClass('col-10');
                $('#paragraph1').hide();
                $('.closebtn1').show();
                $('.closebtn').hide();
            });
            $('.closebtn1').on('click', function () {
                $('#paragraph2').addClass('col-10');
                $('#paragraph2').removeClass('col-12');
                $('#paragraph1').show();
                $('.closebtn').show();
                $('.closebtn1').hide();
            });
            $('.historyCalend').on('click', function () {
                $('.comments_Section').hide();
                $('.historyCalend').hide();
                $('.commentsCalend').show();
                $('.history_Section').show();
            });
            $('.commentsCalend').on('click', function () {
                $('.comments_Section').show();
                $('.commentsCalend').hide();
                $('.historyCalend').show();
                $('.history_Section').hide();
            });
            $('.btn_assign').on('click', function () {
                $(input, this).removeAttr('checked');
                $(this).removeClass('active');
            });
            var table = $('#example').DataTable({
                fixedHeader: {
                    header: true
                }
            });
            var selector = '.landing_links ul li a';
            $(selector).on('click', function () {
                $(selector).removeClass('active');
                $(this).addClass('active');
            });
        });
    </script>
    
    <script>
        function ReloadManagePage() {
            window.open("<%=ResolveUrl("~/DMS/DocumentIntiator/Manage_Documents.aspx")%>", "_self");
        }
        function ReloadRevertPage() {
            window.open("<%=ResolveUrl("~/DMS/DocumentIntiator/Reverted_Documents.aspx")%>", "_self");
        }
        function ReloadInitiationPage() {
            window.open("<%=ResolveUrl("~/DMS/DocumentIntiator/DocumentIntiation.aspx")%>", "_self");
        }
        function ReloadDashboard() {
            window.open("<%=ResolveUrl("~/DMS/DMSHomePage.aspx")%>", "_self");
        }
    </script>
    <script>
        function selectDocInit() {
            //$("#DocumentNumber").hide();
            //$("#div_dept1").hide();
            //$("#div_Namenew1").hide();
            //$("#div_Userroles11").hide();
            //$("#div_Userroles21").hide();
            //$("#div_Version").hide();
            //$("#div_Userroles31").hide();
            //$("#div_buttons1").hide();
            //$("#div_NameUpdate1").hide();
            //$("#type1").hide();
            //$("#div1").hide();
            //$("#div_Purpose").hide();
            //$("#divgvcomments").hide();
            //$("#id1").hide();
            //$("#div6").hide();
            //$("#divQA").hide();
            //$("#divreviewer").hide();
            //$("#divApprover").hide();
            //$("#divRemarks").hide();
            //$("#divPublic").hide();
            $("#hidecntrls2").hide();
        }
        function NewDocInt() {
            $("#DocumentNumber,#hidecntrls2").show();
            $("#spanDocNo").show();
            $("#div_dept1").show();
            $("#div_Namenew1").show();           
            //$("#div_initiator").hide();
            $("#div_Userroles11").show();
            $("#div_Userroles21").hide();
            $("#div_Version").show();
            $("#div_Userroles31").hide();
            $("#div_buttons1").show();
            $("#div_NameUpdate1").hide();
            $("#type1").show();
            $("#div1").show();
            $("#div_Purpose").show();
            $("#divgvcomments").hide();
            $("#id1").hide();
            //$("#div6").hide();
            $("#divQA").show();
            $("#divreviewer").show();
            $("#divApprover").show();
            $("#approverDivGV").hide();
            $("#reviewerdiv").hide();
            $("#divRemarks").hide();
        }
        function hideDivReviewer() {
            $("#divreviewer").show();
        }
        function RevisionDocInt() {
            $("#DocumentNumber,#hidecntrls2").show();
            $("#spanDocNo").hide();
            $("#div_dept1").show();
            $("#div_Namenew1").hide();
            //$("#div_initiator").hide();
            $("#div_Userroles11").show();
            $("#div_Userroles21").hide();
            $("#div_Version").show();
            $("#div_Userroles31").hide();
            $("#div_buttons1").show();
            $("#div_NameUpdate1").show();
            $("#type1").show();
            $("#div1").show();
            $("#div_Purpose").show();
            $("#divgvcomments").hide();
            $("#id1").hide();
            //$("#div6").hide();
            $("#divQA").show();
            $("#divreviewer").show();
            $("#divApprover").show();
            $("#divRemarks").hide();
            $("#btnNewDocName").show();
            $("#hdfDocNameChange").val(0);
        }
        function RevisionDocIntNameChange() {
            $("#DocumentNumber,#hidecntrls2").show();
            $("#spanDocNo").hide();
            $("#div_dept1").show();
            $("#div_Namenew1").show();
            //$("#div_initiator").hide();
            $("#div_Userroles11").show();
            $("#div_Userroles21").hide();
            $("#div_Version").show();
            $("#div_Userroles31").hide();
            $("#div_buttons1").show();
            $("#div_NameUpdate1").hide();
            $("#type1").show();
            $("#div1").show();
            $("#div_Purpose").show();
            $("#divgvcomments").hide();
            $("#id1").hide();
            //$("#div6").hide();
            $("#divQA").show();
            $("#divreviewer").show();
            $("#divApprover").show();
            $("#divRemarks").hide();
            $("#btnNewDocName").show();
            $("#hdfDocNameChange").val(1);
        }
        function WithdrawDocInt() {
            $("#DocumentNumber,#hidecntrls2").show();
            $("#spanDocNo").hide();
            $("#div_dept1").show();
            $("#div_Namenew1").hide();
            //$("#div_initiator").hide();
            $("#div_Userroles11").hide();
            $("#div_Userroles21").hide();
            $("#div_Version").show();
            $("#div_Userroles31").hide();
            $("#div_buttons1").show();
            $("#div_NameUpdate1").show();

            $("#type1").show();
            $("#div1").hide();
            $("#div_Purpose").show();
            $("#divgvcomments").hide();
            $("#id1").hide();
            //$("#div6").hide();
            $("#divQA").show();
            $("#divreviewer").hide();
            $("#divApprover").hide();
            $("#divRemarks").hide();
        }
    </script>
    <script>
        function NewDocMan() {
            $("#DocumentNumber,#hidecntrls2").show();
            $("#spanDocNo").hide();
            $("#div_dept1").show();
            $("#div_Namenew1").show();
            //$("#div_initiator").show();
            $("#div_Userroles11").show();
            $("#div_Userroles21").hide();
            $("#div_Version").show();
            $("#div_Userroles31").hide();
            $("#div_buttons1").show();
            $("#div_NameUpdate1").hide();
            $("#type1").show();
            $("#div1").show();
            $("#div_Purpose").show();
            $("#divgvcomments").hide();
            $("#spanReqType").hide();
            $("#spanDept").hide();
            $("#spanDocName").hide();
            $("#spanDocType").hide();
            $("#spanComments").hide();
            $("#id1").hide();
            //$("#div6").hide();
            $("#divQA").show();
            $("#divreviewer").show();
            $("#divApprover").show();
            $("#divRemarks").show();
        }
        function NewDocRev() {
            $("#DocumentNumber,#hidecntrls2").show();
            $("#div_dept1").show();
            $("#div_Namenew1").show();
            //$("#div_initiator").hide();
            $("#div_Userroles11").show();
            $("#div_Userroles21").hide();
            $("#div_Version").show();
            $("#div_Userroles31").hide();
            $("#div_buttons1").show();
            $("#div_NameUpdate1").hide();
            $("#type1").show();
            $("#div1").show();
            $("#div_Purpose").show();
            $("#divgvcomments").show();
            //$("#div6").hide();
            $("#id1").show();
            $("#divQA").show();
            $("#divApprover").show();
            $("#divRemarks").hide();
            $("#divPublic").show();
            $("#btnNewDocName").show();
        }
    </script>
    <script>
        function RevisionDocMan() {
            $("#DocumentNumber,#hidecntrls2").show();
            $("#spanDocNo").hide();
            $("#div_dept1").show();
            $("#div_Namenew1").show();
            //$("#div_initiator").show();
            $("#div_Userroles11").show();
            $("#div_Userroles21").hide();
            $("#div_Version").show();
            $("#div_Userroles31").hide();
            $("#div_buttons1").show();
            $("#div_NameUpdate1").hide();
            $("#type1").show();
            $("#div1").show();
            $("#div_Purpose").show();
            $("#divgvcomments").hide();
            //$("#div6").hide();
            $("#id1").hide();
            $("#spanReqType").hide();
            $("#spanDept").hide();
            $("#spanDocName").hide();
            $("#spanDocType").hide();
            $("#spanComments").hide();
            $("#divQA").show();
            $("#divApprover").show();
            $("#divRemarks").show();
            $("#divPublic").show();
        }
        function RevisionDocRev() {
            $("#DocumentNumber,#hidecntrls2").show();
            $("#spanDocNo").hide();
            $("#div_dept1").show();
            $("#div_Namenew1").show();
            //$("#div_initiator").hide();
            $("#div_Userroles11").show();
            $("#div_Userroles21").hide();
            $("#div_Version").show();
            $("#div_Userroles31").hide();
            $("#div_buttons1").show();
            $("#div_NameUpdate1").hide();
            $("#type1").show();
            $("#div1").show();
            $("#div_Purpose").show();
            $("#divgvcomments").show();
            //$("#div6").hide();
            $("#id1").hide();
            $("#divQA").show();
            $("#divApprover").show();
            $("#divRemarks").hide();
            $("#divPublic").show();
            $("#btnNewDocName").show();           
        }
        function WithdrawDocRev() {
            $("#DocumentNumber,#hidecntrls2").show();
            $("#spanDocNo").hide();
            $("#div_dept1").show();
            $("#div_Namenew1").show();
            //$("#div_initiator").hide();
            $("#div_Userroles11").hide();
            $("#div_Userroles21").hide();
            $("#div_Version").show();
            $("#div_Userroles31").hide();
            $("#div_buttons1").show();
            $("#div_NameUpdate1").hide();
            $("#type1").show();
            $("#div1").hide();
            $("#div_Purpose").show();
            $("#divgvcomments").show();
            //$("#div6").hide();
            $("#id1").hide();
            $("#divQA").show();
            $("#divreviewer").hide();
            $("#divApprover").show();
            $("#divRemarks").hide();
        }
        function WithdrawDocMan() {
            $("#DocumentNumber,#hidecntrls2").show();
            $("#spanDocNo").hide();
            $("#div_dept1").show();
            $("#div_Namenew1").show();
            //$("#div_initiator").hide();
            $("#div_Userroles11").hide();
            $("#div_Userroles21").hide();
            $("#div_Version").show();
            $("#div_Userroles31").hide();
            $("#div_buttons1").show();
            $("#div_NameUpdate1").hide();
            $("#type1").show();
            $("#div1").hide();
            $("#div_Purpose").show();
            $("#divgvcomments").hide();
            //$("#div6").hide();
            $("#id1").hide();
            $("#divQA").show();
            $("#divreviewer").hide();
            $("#divApprover").show();
            $("#divRemarks").show();
        }
    </script>
    <script>
        function GetSelectedRowDelete(lnk) {
            var row = lnk.parentNode.parentNode;
            var lblEmpID = row.cells[3].getElementsByTagName("input")[0].value;
            var lblRole = row.cells[0].innerText;
            var hdnDeleteEmpIDRow = document.getElementById('<%=hdnDeleteEmpIDRow.ClientID%>');
            var hdnDeleteRoleRow = document.getElementById('<%=hdnDeleteRoleRow.ClientID%>');
            hdnDeleteEmpIDRow.value = lblEmpID;
            hdnDeleteRoleRow.value = lblRole;
            ConformAlertApprover();
        }
    </script>
    <script>
        function GetSelectedRowEdit(lnk) {
            var row = lnk.parentNode.parentNode;
            var lblEmpID = row.cells[3].getElementsByTagName("input")[0].value;
            var lblDeptID = row.cells[3].getElementsByTagName("input")[1].value;
            var lblRole = row.cells[0].innerText;
            var hdnDeleteEmpIDRow = document.getElementById('<%=hdnDeleteEmpIDRow.ClientID%>');
            var hdnEditDeptIDRow = document.getElementById('<%=hdnEditDeptIDRow.ClientID%>');
            var hdnDeleteRoleRow = document.getElementById('<%=hdnDeleteRoleRow.ClientID%>');
            hdnDeleteEmpIDRow.value = lblEmpID;
            hdnDeleteRoleRow.value = lblRole;
            hdnEditDeptIDRow.value = lblDeptID;
        }
    </script>
    <script>
        function ConformAlertUpdate() {
            document.getElementById("<%=hdnConfirm.ClientID%>").value = 'Update';
            custAlertMsg('Are you sure you want to Update the Document Details?', 'confirm', true);
        }
        function ConformAlertApprover() {
            document.getElementById("<%=hdnConfirm.ClientID%>").value = 'Delete';
            custAlertMsg('Are you sure you want to Remove employee from this Document process?', 'confirm', true);
        }
        function ConformAlertSubmit() {
            document.getElementById("<%=hdnConfirm.ClientID%>").value = 'Submit';
            var rb = document.getElementById("<%=RadioButtonList1.ClientID%>");
            var radio = rb.getElementsByTagName("input");
            if (radio[0].checked) {
                document.getElementById("<%=hdfPublic.ClientID%>").value = 'Public';
                        }
                        else if (radio[1].checked) {
                            document.getElementById("<%=hdfPublic.ClientID%>").value = 'Private';
                        }
                        custAlertMsg('Are you sure you want to Submit the Document Details <br/> and document as ' + document.getElementById("<%=hdfPublic.ClientID%>").value + '?', 'confirm', true);
        }
    </script>
    <script type="text/javascript">
        function ReloadRevertedPage() {
            window.open("<%=ResolveUrl("~/DMS/DocumentIntiator/Reverted_Documents.aspx")%>", "_self");
        }
        function ReloadManagepage() {
            window.open("<%=ResolveUrl("~/DMS/DocumentIntiator/Manage_Documents.aspx")%>", "_self");
                    }
    </script>
    <script type="text/javascript">                   
        function AddQA() {
            var QS = '<%=Request.QueryString["From"]%>';

                        var Department = document.getElementById("<%=ddlDepartmentQA.ClientID%>").value;
                        if (QS == "M") {
                            var Approver = document.getElementById("<%=SinglelstQA.ClientID%>").value;
                        }
                        else {
                            var Approver = document.getElementById("<%=lstQA.ClientID%>").value;
                        }
                        var Role = document.getElementById("<%=ddlRoles.ClientID%>").value;
                        errors = [];
                        if (Role == 0) {
                            errors.push("Select Role.");
                        }
                        if (Department == 0) {
                            errors.push("Select Department.");
                        }
                        if (Approver == "") {
                            errors.push("Select At-least one Employee.");
                        }                        
                        if (errors.length > 0) {
                            custAlertMsg(errors.join("<br/>"), "error");
                            return false;
                        }
                        else {
                            showImg();
                            return true;
                        }
                    }
                    //validations for DocumentIntiation page
                    function ValidationDocumentIntiation() {
                        var RequestType = document.getElementById("<%=ddl_requestType.ClientID%>").value;
                        var DocumentType = document.getElementById("<%= ddl_DocumentType.ClientID%>").value;
                        var DeptName = document.getElementById("<%= ddl_DeptName.ClientID%>").value;
                        var DocumentNumber = document.getElementById("<%= txt_DocumentNumber.ClientID%>").value;
                        var DocumentName = document.getElementById("<%= txt_DocumentName.ClientID%>").value;
                        var Comments = document.getElementById("<%= txt_Comments.ClientID%>").value;
                        var Author = document.getElementById("<%= ddl_CreatedBy.ClientID%>").value;
                        var ddlDocumentName = document.getElementById("<%= ddl_AutoDocName.ClientID%>").value;
                        var rb = document.getElementById("<%=RadioButtonList1.ClientID%>");
                        var radio = rb.getElementsByTagName("input");
                        var isChecked = false;
                        for (var i = 0; i < radio.length; i++) {
                            if (radio[i].checked) {
                                isChecked = true;
                                break;
                            }
                        }
                        var expr = /^([\w-\.]+)@(|(([\w-]+\.)+))(\]?)$/;
                        errors = [];
                        if (RequestType == 1) {
                            if (DeptName == 0) {
                                errors.push(" Select Department.");
                            }
                            if (DocumentType == 0) {
                                errors.push(" Select Document Type.");
                            }
                            if (DocumentNumber == "") {
                                errors.push(" Enter Document Number.");
                            }
                            if (DocumentName == "") {
                                errors.push(" Enter Document Name.");
                            }
                            if (DocumentName != "") {
                                if (expr.test(DocumentName)) {
                                    errors.push("Undefined Characters not Allowed.")
                                }
                            }
                            if (Comments == "") {
                                errors.push(" Enter Purpose of Initiation.");
                            } if (Author == 0) {
                                errors.push(" Select Author.");
                            }
                            if (!isChecked) {
                                errors.push("Select privacy level.");
                            }
                            if (errors.length > 0) {
                                custAlertMsg(errors.join("<br/>"), "error");
                                return false;
                            }
                            else {
                                return true;
                            }
                        }
                        if (RequestType == 2) {
                            if (DeptName == 0) {
                                errors.push(" Select Department.");
                            }
                            if (DocumentType == 0) {
                                errors.push(" Select Document Type.");
                            }
                            if (DocumentName == "" || ddlDocumentName == 0) {
                                errors.push(" Select Document Name.");
                            }
                            if (Comments == "") {
                                errors.push(" Enter Reason for Revision.");
                            }
                            if (Author == 0) {
                                errors.push(" Select Author.");
                            }
                            if (errors.length > 0) {
                                custAlertMsg(errors.join("<br/>"), "error");
                                return false;
                            }
                            else {
                                return true;
                            }
                        }
                        if (RequestType == 3) {
                            if (DeptName == 0) {
                                errors.push(" Select Department.");
                            }
                            if (DocumentType == 0) {
                                errors.push(" Select Document Type.");
                            }
                            if (ddlDocumentName == 0) {
                                errors.push(" Select Document Name.");
                            }
                            if (Comments == "") {
                                errors.push(" Enter Reason for  Withdrawal.");
                            }
                            if (errors.length > 0) {
                                custAlertMsg(errors.join("<br/>"), "error");
                                return false;
                            }
                            else {
                                return true;
                            }
                        }
                        ConformAlertSubmit();
                    }
                    function UpdateDocumentInitiation() {
                        var RequestType = document.getElementById("<%=ddl_requestType.ClientID%>").value;
                        var DocumentType = document.getElementById("<%= ddl_DocumentType.ClientID%>").value;
                        var DeptName = document.getElementById("<%= ddl_DeptName.ClientID%>").value;
                        var DocumentNumber = document.getElementById("<%= txt_DocumentNumber.ClientID%>").value;
                        var DocumentName = document.getElementById("<%= txt_DocumentName.ClientID%>").value;
                        var Comments = document.getElementById("<%= txt_Comments.ClientID%>").value;
                        var Author = document.getElementById("<%= ddl_CreatedBy.ClientID%>").value;
                        var Remarks = document.getElementById("<%= txt_Remarks.ClientID%>").value;
                        var rb = document.getElementById("<%=RadioButtonList1.ClientID%>");
                        var radio = rb.getElementsByTagName("input");
                        var isChecked = false;
                        for (var i = 0; i < radio.length; i++) {
                            if (radio[i].checked) {
                                isChecked = true;
                                break;
                            }
                        }
                        var expr = /^([\w-\.]+)@(|(([\w-]+\.)+))(\]?)$/;
                        var QS = '<%=Request.QueryString["From"]%>';
                        errors = [];
                        if (RequestType == 1) {
                            if (DocumentNumber == "") {
                                errors.push(" Enter Document Number.");
                            }
                            if (DocumentType == 0) {
                                errors.push(" Select Document Type.");
                            }
                            if (DeptName == 0) {
                                errors.push(" Select Department.");
                            }
                            if (DocumentName == "") {
                                errors.push(" Enter Document Name");
                            }
                            if (DocumentName != "") {
                                if (expr.test(DocumentName)) {
                                    errors.push(" Undefined Characters not Allowed.")
                                }
                            }
                            if (Comments == "") {
                                errors.push(" Enter Purpose of Initiation.");
                            } if (Author == 0) {
                                errors.push(" Select Author.");
                            }
                            if (!isChecked) {
                                errors.push("Select privacy level.");
                            }
                            if (QS == "M") {
                                if (Remarks == "") {
                                    errors.push(" Enter Remarks.");
                                }
                            }
                            if (errors.length > 0) {
                                custAlertMsg(errors.join("<br/>"), "error");
                                return false;
                            }
                            else {
                                return true;
                            }
                        }
                        if (RequestType == 2) {
                            if (DocumentType == 0) {
                                errors.push(" Select Document Type.");
                            }
                            if (DeptName == 0) {
                                errors.push(" Select Department.");
                            }
                            if (DocumentName == "") {
                                errors.push(" Enter Document Name.");
                            }
                            if (Comments == "") {
                                errors.push("Enter Reason for Revision.");
                            }
                            if (Author == 0) {
                                errors.push(" Select Author.");
                            }
                            if (QS == "M") {
                                if (Remarks == "") {
                                    errors.push(" Enter Remarks.");
                                }
                            }
                            if (errors.length > 0) {
                                custAlertMsg(errors.join("<br/>"), "error");
                                return false;
                            }
                            else {
                                return true;
                            }
                        }
                        if (RequestType == 3) {
                            if (DocumentType == 0) {
                                errors.push(" Select Document Type.");
                            }
                            if (DeptName == 0) {
                                errors.push(" Select Department.");
                            }
                            if (DocumentName == "") {
                                errors.push(" Enter Document Name.");
                            }
                            if (Comments == "") {
                                errors.push("Enter Reason for Withdrawal.");
                            }
                            if (QS == "M") {
                                if (Remarks == "") {
                                    errors.push(" Enter Remarks.");
                                }
                            }
                            if (errors.length > 0) {
                                custAlertMsg(errors.join("<br/>"), "error");
                                return false;
                            }
                            else {
                                return true;
                            }
                        }
                        ConformAlertUpdate();
                    }
    </script>
    <script>
                    function disableRadioPublic() {
                        $('#lblRadioPublic').removeClass('active');
                        $('#lblRadioPrivate').addClass('active');
                    }
                    function enableRadioPublic() {
                        $('#lblRadioPublic').addClass('active');
                        $('#lblRadioPrivate').removeClass('active');
                    }
    </script>
    <script>
                    function MakeDropdownOpen() {
                        $("#<%=lstQA.ClientID%>").selectpicker("refresh");
                        $(".dropdown-menu").addClass('show');
                    }
                    function commentRequiredReject() {
                        var comment = document.getElementById("<%=txt_Remarks.ClientID%>").value;
                        errors = [];
                        if (comment.trim() == "") {
                            errors.push("Enter Remarks.");
                        }
                        if (errors.length > 0) {
                            custAlertMsg(errors.join("<br/>"), "error");
                            return false;
                        }
                        else {
                            ConformAlertReject();
                            return true;
                        }
                    }
                    function ConformAlertReject() {
                        document.getElementById("<%=hdnConfirm.ClientID%>").value = 'Reject';
            custAlertMsg('Are you sure you want to Reject the Document Details?', 'confirm', true);
        }            
    </script>
    <script>
                    function showImg() {
                        ShowPleaseWait('show');
                    }
                    function hideImg() {
                        ShowPleaseWait('hide');
                    }
                    function HidePopup() {
                        $('#usES_Modal').modal('hide');
                    }
                    
    </script>
  <script>
                    $(function () {
                        InsertDocNumTitle();
                    });

                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_endRequest(function () {
                        InsertDocNumTitle();
                    });
                    function InsertDocNumTitle() {
                            var text_val;
                            $("#<%=txt_DocumentNumber.ClientID%>").on('keyup', function () {
                            text_val = $(this).val();
                            $("#<%=txt_DocumentNumber.ClientID%>").attr('title', text_val);
                        });
                    }
                  
  </script>
    <script>
                    function showInactive() {
                        $("#divinactive").show();
                    }
                    function hideInactive() {
                        $("#divinactive").hide();
                    }
                    function showInactiveStyle() {
                        $("#lblEmployeeList").addClass("regulatory_dropdown_inactive");
                    }
                    function hideInactiveStyle() {
                        $("#lblEmployeeList").removeClass("regulatory_dropdown_inactive");
                    }
    </script>
</asp:Content>
