﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="FormPendingList.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DMSAdmin.FormPendingList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="pull-right btn btn-approve_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">
        <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12 dms_outer_border">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none" id="divMainContainer" runat="server" visible="true">
                <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none pull-right ">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grid_panel_full padding-none">
                        <div class="grid_header col-md-12 col-lg-12 col-xs-12 col-sm-12 ">Pending Form List</div>
                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 top bottom">
                            <table id="dtDocList" class="display" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>VersionID</th>
                                        <th>Form Number</th>
                                        <th>Form Name</th>
                                        <th>Form Description</th>
                                        <th>Version</th>
                                        <th>Department</th>
                                        <th>RefDocNumber</th>
                                        <th>Creator</th>
                                        <th>Approver</th>
                                        <th>Action</th>
                                        <th>Request Type</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRequestType" runat="server" Value="0" />
    <script>
        $('#dtDocList').wrap('<div class="dataTables_scroll" />');
        var oTable = $('#dtDocList').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'FormVersionID' },
                { 'data': 'FormNumber' },
                { 'data': 'FormName' },
                { 'data': 'Purpose' },
                { 'data': 'VersionNumber' },
                { 'data': 'dept' },
                { 'data': 'documentNUmber' },
                { 'data': 'CreatedBy' },
                { 'data': 'oldApprovedBY' },
                { 'data': 'ActionName' },
                { 'data': 'FormRequestType' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="view_tms" title="Edit" href=/DMS/DMSAdmin/FormCreation.aspx?FormVersionID=' + o.FormVersionID + '&from=p>' + '' + '</a>'; }
                },
            ],
            "responsive": true,
            "paging": true,
            "search": {
                "regex": true
            },
            "aoColumnDefs": [{ "targets": [1, 4, 7, 10], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0] }, { "className": "dt-body-left", "targets": [3, 4, 6, 8, 9, 10, 11] },
            ],
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetFormPendingList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "ReqTypeID", "value": <%=hdnRequestType.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    </script>
</asp:Content>
