﻿<%@ Page Title="JR TS Evaluation" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="JR_TrainerEvaluationDetails.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Training.JR_TrainerEvaluationDetails" %>

<%--<%@ Register Src="~/UserControls/TMS_UserControls/Exam/ucExamResultSheet.ascx" TagPrefix="uc1" TagName="ucExamResultSheet" %>--%>
<%@ Register Src="~/UserControls/TMS_UserControls/ucJrTrainedRecords.ascx" TagPrefix="uc1" TagName="ucJrTrainedRecords" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hdnTrainerID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnJR_ID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnDocID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnExamID" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="col-12 padding-none float-left" id="Div3" runat="server">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">JR Trainer Evaluation</div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                    <table id="tblJR_TrainerEvaluationDetails" class="datatable_cust tblJR_TrainerEvaluationDetailsClass display breakword" style="width: 100%">
                        <thead>
                            <tr>
                                <th>JR_ID</th>
                                <th>JR Name</th>
                                <th>Version</th>
                                <th>Trainee</th>
                                <th>Department</th>
                                <th>Assigned By</th>
                                <th>Assigned Date</th>
                                <th>View</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!--------JR Trained Document Details Modal---------->
<%--    <div id="modalTrainedDocs" role="dialog" class="modal department fade">
        <div class="modal-dialog" style="min-width:95%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" onclick="CloseTE_TD_ModalPopUp();" class="close">&times;</button>
                    <h4 class="modal-title">Trained Documents</h4>
                </div>
                <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12" style="background: #fff">
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <table id="tblJR_TrainedDocs" class="datatable_cust display tblJR_TrainedDocsClass breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>DocumentID</th>
                                    <th>EvaluatedStatusID</th>
                                    <th>Document</th>
                                    <th>Version</th>
                                    <th>Department</th>
                                    <th>Attempts</th>
                                    <th>Qualified On</th>
                                    <th>Marks</th>
                                    <th>Duration</th>
                                    <th>Evaluation</th>
                                    <th>View AS</th>
                                </tr>
                            </thead>
                        </table>
                        <div style="color: #098293; font-weight: bold">AS: Answer Sheet</div>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
    <!---- End---->


    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnViewJRTrainedDocs" runat="server" Text="viewJR" />
              <%--  <asp:Button ID="btnViewAnswerSheet" runat="server" Text="viewAnswerSheet" OnClick="btnViewAnswerSheet_Click" />--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
   <%-- <uc1:ucExamResultSheet runat="server" ID="ucExamResultSheet" />--%>
    <uc1:ucJrTrainedRecords runat="server" ID="ucJrTrainedRecords" />
    <script>
        function viewTrainedDocs(JR_ID) {
            GetRegularRecords(JR_ID, <%=hdnTrainerID.Value%>, true);
            GetRetrospectiveRecords(JR_ID, <%=hdnTrainerID.Value%>);
        }
       
    </script>

    <!--JR_TrainerEvaluation Details jQuery DataTable-->
    <script>
        $.ajaxSetup({
            cache: false
        });

    $('#tblJR_TrainerEvaluationDetails thead tr').clone(true).appendTo('#tblJR_TrainerEvaluationDetails thead');
    $('#tblJR_TrainerEvaluationDetails thead tr:eq(1) th').each(function (i) {
        if (i < 7) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search " />');

            $('input', this).on('keyup change', function () {
                if (oTableJR_TrainerEvaluationDetails.column(i).search() !== this.value) {
                    oTableJR_TrainerEvaluationDetails
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
            if (i == 0)
            {
                $(this).html('');
            }
        }
        else {
            $(this).html('');
        }
    });
        var oTableJR_TrainerEvaluationDetails = $('#tblJR_TrainerEvaluationDetails').DataTable({
            columns: [
                { 'data': 'JR_ID' },
                { 'data': 'JR_Name' },
                { 'data': 'JR_Version' },
                { 'data': 'TraineeName' },
                { 'data': 'TraineeDeptName' },
                { 'data': 'JR_AssignedBy' },
                { 'data': 'JR_AssignedDate' },

                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="view"  href="#" onclick="viewTrainedDocs(' + o.JR_ID + ');"></a>'; }
                }
            ],
            "fixedColumns": {
                "heightMatch": 'none'
            },
             "orderCellsTop": true,
            "scrollY": "47vh",
            "aoColumnDefs": [{ "targets": [0], "visible": false },{ className: "textAlignLeft", "targets": [1,3,4,5] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTraining/JR_TrainerEvaluationDetailsService.asmx/GetJR_TrainerEvaluationDetails")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "TrainerID", "value": <%=hdnTrainerID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblJR_TrainerEvaluationDetails").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        $(".tblJR_TrainerEvaluationDetailsClass").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }          
        });

        function reloadoTableJR_TrainerEvaluationDetails() {
            oTableJR_TrainerEvaluationDetails.ajax.reload();
        }
      
    </script>

    <!--JR_Trained Document Details jQuery DataTable-->
   <%-- <script>
        var oTableJR_TrainedDocs;        
        function loadTrainedDocumentsInTE() {
            if (oTableJR_TrainedDocs != null) {
                oTableJR_TrainedDocs.destroy();
            }
            oTableJR_TrainedDocs = $('#tblJR_TrainedDocs').DataTable({
                columns: [
                    { 'data': 'DocumentID' },
                    { 'data': 'EvaluatedStatusID' },
                    { 'data': 'Document' },
                    { 'data': 'DocVersion' },
                    { 'data': 'DepartmentName' },
                    { 'data': 'Attempts' },
                    { 'data': 'QualifiedDate' },
                    { 'data': 'Marks' },
                    { 'data': 'ReadDuration' },
                    { 'data': 'EvaluatedStatus' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            if (o.EvaluatedStatusID == "1") {
                                return '<a class="AnsSheet_tms" href="#" onclick="viewAnswerSheet(' + o.DocumentID + ');"></a>';
                            }
                            else {
                                return ''
                            }
                        }
                    }
                ],
                "fixedColumns": {
                    "heightMatch": 'none'
                },
                "scrollY": "250px",
                "aoColumnDefs": [{ "targets": [0, 1], "visible": false }, { className: "textAlignLeft", "targets": [2, 4, 7, 9] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTraining/JR_TrainerEvaluationDetailsService.asmx/GetJR_TrainedDocs")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "JR_ID", "value": $('#<%=hdnJR_ID.ClientID%>').val() }, { "name": "TrainerID", "value": <%=hdnTrainerID.Value%> });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                            $("#tblJR_TrainedDocs").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblJR_TrainedDocsClass").css({ "width": "100%" });
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }             
            });
            
        }
    </script>--%>

  <%--  <script>
        function CloseTE_TD_ModalPopUp() {
            reloadoTableJR_TrainerEvaluationDetails();
            $('#modalTrainedDocs').modal('hide');            
        }
    </script>--%>

</asp:Content>
