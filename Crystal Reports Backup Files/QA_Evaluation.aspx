﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="QA_Evaluation.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Training.QA_Evaluation1" %>

<%@ Register Src="~/UserControls/SessionTimeOut.ascx" TagPrefix="uc1" TagName="SessionTimeOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body
{
    margin: 0;
    padding: 0;
    font-family: Arial;
}
.Progressmodal
{
    position: fixed;
    z-index: 9999;
    height: 100%;
    width: 100%;
    top: 0;
    left:0;
    background-color:rgba(80, 80, 80, 0.45);
    filter: alpha(opacity=60);
    /*opacity: 0.6;*/
    /*-moz-opacity: 0.8;*/
}
.center
{
    z-index: 1000;
    /*margin: 300px auto;*/
    margin-left:40%;
    margin-top:15%;
    /*padding: 10px;*/
    width: 130px;
      border-radius:10px;
    background-color: White;   
    filter: alpha(opacity=100);
    /*opacity: 1;*/
    /*-moz-opacity: 1;*/ 
}
.center img
{
     border-radius:10px;
    height: 250px;
    width: 350px;
}
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnJR_ID" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div id="divMainContainer" runat="server" visible="true" style="margin: 0px 35px">
              <div class="panel panel-default" id="mainContainer" runat="server">
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 Panel_Header_TMS">
                    <div class="col-sm-6">
                        <span class="Panel_Title">
                            <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Text="QA Evaluation"></asp:Label>
                        </span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>

                <div class="row" style="margin: 0px">
                    <div class="col-sm-6">
                        <div class="form-horizontal">
                            <asp:UpdatePanel ID="upddlDepartment" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label">Department<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlDepartment" CssClass="form-control SearchDropDown" runat="server" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true" Width="100%"></asp:DropDownList>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                        <hr class="hr_Line" />
                    </div>
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12" style="overflow: auto; max-height: 460px; min-height: 460px;">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upgvQAEvaluation_JR_List">
                            <ProgressTemplate>
                                <div class="Progressmodal">
                                    <div class="center">                                     
                                        <img alt="" src="<%=ResolveUrl("~/Images/please_wait.gif")%>"/>
                                    </div>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel ID="upgvQAEvaluation_JR_List" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView ID="gvQAEvaluation_JR_List" OnRowCommand="gvQAEvaluation_JR_List_RowCommand" DataKeyNames="JR_ID"
                                    EmptyDataText="No Record's"
                                    runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixHeadJrList"
                                    EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                    <AlternatingRowStyle BackColor="White" />
                                    <RowStyle CssClass="col-xs-12 grid_border" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="JR ID" Visible="false">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditJrID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJrID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:TemplateField HeaderText="EmpID" Visible="false">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditEmpID" runat="server" Text='<%# Bind("TraineeEmpID") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpID" runat="server" Text='<%# Bind("TraineeEmpID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="JR Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditJrName" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJrName" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--   <asp:TemplateField HeaderText="Emp Code">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditEmpCode" runat="server" Text='<%# Bind("TraineeEmpCode") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpCode" runat="server" Text='<%# Bind("TraineeEmpCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Employee Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditEmpName" runat="server" Text='<%# Bind("TraineeName") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("TraineeName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Trainee Department" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditTraineeDeptName" runat="server" Text='<%# Bind("TraineeDeptName") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTraineeDeptName" runat="server" Text='<%# Bind("TraineeDeptName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JR Assigned By" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditJR_AssignedBy" runat="server" Text='<%# Bind("JR_AssignedBy") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJR_AssignedBy" runat="server" Text='<%# Bind("JR_AssignedBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JR Assigned Date" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditJR_AssignedDate" runat="server" Text='<%# Bind("JR_AssignedDate") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJR_AssignedDate" runat="server" Text='<%# Bind("JR_AssignedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JR Evaluated HOD" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditEvaluatedBy" runat="server" Text='<%# Bind("EvaluatedBy") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEvaluatedBy" runat="server" Text='<%# Bind("EvaluatedBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JR Evaluated Date" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditEvaluated_Date" runat="server" Text='<%# Bind("Evaluated_Date") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEvaluated_Date" runat="server" Text='<%# Bind("Evaluated_Date") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Training Record" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkViewTS" runat="server" Font-Underline="true" CommandName="ViewTS" CommandArgument="<%# Container.DataItemIndex %>" ToolTip="click to view Training Record">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="/Images/DocSearch.png" Style="width:35px" />
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Evaluate" SortExpression="Description" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkAttendedOralExam" CssClass="btn TMS_Button btn-sm" runat="server" CommandName="SubmitEvalution" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' ToolTip="Click To Submit Evaluation">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <span class="glyphicon glyphicon-check InBtn_Icon"></span>
                                                            </td>
                                                            <td>
                                                                <span>Submit</span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <!-- Modal popup  for Sops list based on selected department-->
            <div class="modal fade" id="SopsModalView" role="dialog">
                <div class="modal-dialog modal-lg" style="width: 90%">
                    <!-- Modal content-->
                    <div class="modal-content" style="border-radius: 0px">
                        <div class="modal-header tmsModalHeader">
                            <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title Panel_Title">Trained Sop's</h4>
                        </div>
                        <div class="modal-body tmsModalBody">
                            <div class="row tmsModalContent">
                                <div class="col-sm-6">
                                    <div class="form-horizontal">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">JR Trained Department<span class="mandatoryStar">*</span></label>
                                                    <div class="col-sm-7">
                                                        <asp:DropDownList ID="ddlJR_Trained_Depts" CssClass="form-control SearchDropDown" runat="server" OnSelectedIndexChanged="ddlJR_Trained_Depts_SelectedIndexChanged" AutoPostBack="true" Width="100%"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <ul class="nav nav-tabs pull-right">
                                        <li class="active"><a data-toggle="tab" href="#tabgvOralExam" class="tabstyle">Oral Exam</a></li>
                                        <li><a data-toggle="tab" href="#tabgvWrittenExam" class="tabstyle">Written Exam</a></li>
                                    </ul>
                                </div>
                                <div class="tab-content" style="padding-top: 3px">
                                    <div id="tabgvOralExam"class="tab-pane fade in active">
                                        <div class=" col-lg-12 col-sm-12 col-xs-12 col-md-12 divGV_Styles">
                                            <asp:UpdatePanel ID="upGvOralExamSops" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvOralExamSops" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table table-fixed fixHeadExamSops"
                                                        EmptyDataText="No Oral Exam Record's" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" Style="margin-bottom: 0px !important;">
                                                        <AlternatingRowStyle BackColor="White" />
                                                         <RowStyle CssClass="col-xs-12 grid_border" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Sop ID" Visible="false">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditSOP_ID" runat="server" Text='<%# Bind("SopID") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSOP_ID" runat="server" Text='<%# Bind("SopID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--    <asp:TemplateField HeaderText="Trained Department" HeaderStyle-Width="100px">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="SOP No." HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditSop_Number" runat="server" Text='<%# Bind("Sop_Number") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSop_Number" runat="server" Text='<%# Bind("Sop_Number") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditSopVersionID" runat="server" Text='<%# Bind("SopVersionID") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSopVersionID" runat="server" Text='<%# Bind("SopVersionID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SOP Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditSop_Name" runat="server" Text='<%# Bind("Sop_Name") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSop_Name" runat="server" Text='<%# Bind("Sop_Name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField HeaderText="Trained Department" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Read Duration" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditSop_Duration" runat="server" Text='<%# Bind("Sop_Duration") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSop_Duration" runat="server" Text='<%# Bind("Sop_Duration") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Evaluated By" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditEvaluatedBY" runat="server" Text='<%# Bind("EvaluatedTrainerBY") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEvaluatedBY" runat="server" Text='<%# Bind("EvaluatedTrainerBY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Evaluated Date" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditEvaluatedDate" runat="server" Text='<%# Bind("EvaluatedDate") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEvaluatedDate" runat="server" Text='<%# Bind("EvaluatedDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Trainer Opinion" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTrainer_Opinion" runat="server" Text='<%# Bind("Trainer_Opinion") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrainer_Opinion" runat="server" Text='<%# Bind("Trainer_Opinion") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div id="tabgvWrittenExam" class="tab-pane fade">
                                        <div class=" col-lg-12 col-sm-12 col-xs-12 col-md-12 divGV_Styles">
                                            <asp:UpdatePanel ID="upGvViewSops" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvWrittenExamSops" runat="server" AutoGenerateColumns="False" DataKeyNames="SopID,Trainer_ExplanationStatus"
                                                        CssClass="table table-fixed fixHeadwrittenExamSops" OnRowCommand="gvWrittenExamSops_RowCommand"
                                                        EmptyDataText="No Written Exam Record's" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                        <AlternatingRowStyle BackColor="White" />
                                                         <RowStyle CssClass="col-xs-12 grid_border" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Sop ID" Visible="false">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditSOP_ID" runat="server" Text='<%# Bind("SopID") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSOP_ID" runat="server" Text='<%# Bind("SopID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- <asp:TemplateField HeaderText="Trained Department" HeaderStyle-Width="100px">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="SOP Num" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditSop_Number" runat="server" Text='<%# Bind("Sop_Number") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSop_Number" runat="server" Text='<%# Bind("Sop_Number") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditSopVersionID" runat="server" Text='<%# Bind("SopVersionID") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSopVersionID" runat="server" Text='<%# Bind("SopVersionID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SOP Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditSop_Name" runat="server" Text='<%# Bind("Sop_Name") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSop_Name" runat="server" Text='<%# Bind("Sop_Name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="Trained Department" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Read Duration" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditSop_Duration" runat="server" Text='<%# Bind("Sop_Duration") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSop_Duration" runat="server" Text='<%# Bind("Sop_Duration") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Evaluated Trainer" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditEvaluatedBY" runat="server" Text='<%# Bind("EvaluatedTrainerBY") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEvaluatedBY" runat="server" Text='<%# Bind("EvaluatedTrainerBY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Evaluated Date" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditEvaluatedDate" runat="server" Text='<%# Bind("EvaluatedDate") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEvaluatedDate" runat="server" Text='<%# Bind("EvaluatedDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField HeaderText="Marks" HeaderStyle-Width="60px">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditMarks" runat="server" Text='<%# Bind("Marks") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMarks" runat="server" Text='<%# Bind("Marks") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Qualified Date" HeaderStyle-Width="190px">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditExamPassedDate" runat="server" Text='<%# Bind("ExamPassedDate") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExamPassedDate" runat="server" Text='<%# Bind("ExamPassedDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Explanation Status">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditExplainationStatus" runat="server" Text='<%# Bind("Trainer_ExplanationStatus") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExplainationStatus" runat="server" Text='<%# Bind("Trainer_ExplanationStatus") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="Answer Sheet" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkViewExamReport" runat="server" Font-Underline="true" CommandName="ViewExamResult" ToolTip="Click to view Exam Record"
                                                                        CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'>
                                                                        <asp:Image ID="Image1" runat="server" ImageUrl="/Images/DocSearch.png" Style="width:35px" />
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal popup for exam Result-->
            <div class="modal fade" id="SopExamResultModal" role="dialog">
                <div class="modal-dialog modal-lg" style="width: 90%; height: 600px">
                    <div class="modal-content">
                        <div class="modal-header tmsModalHeader">
                            <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title Panel_Title" style="color: aliceblue">Exam Result</h4>
                        </div>
                        <div class="modal-body tmsModalBody">
                            <div class="row form-horizontal">
                                <asp:UpdatePanel ID="upSopExamResultBody" runat="server">
                                    <ContentTemplate>
                                        <div style="margin: 0px 6px 0px 10px">
                                            <div class="col-sm-6">
                                                <span style="font-weight: bold">Exam Attempted Date:</span>
                                                <label class="control-label" style="font-weight: normal" runat="server" id="lblExamDate" title="Exam Date and Time"></label>
                                            </div>
                                            <div class="col-sm-6" style="text-align: right; padding-right: 32px">
                                                <span style="font-weight: bold">Marks :</span>
                                                <label class="control-label" style="font-weight: normal" runat="server" id="lblMarks" title="Marks Obtained In this Sop"></label>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="row tmsModalContent" style="overflow: auto; padding: 0px; max-height: 490px; min-height: 490px;">
                                <asp:UpdatePanel ID="upGvResultQuestionnaire" runat="server" UpdateMode="Conditional" style="margin: 10px">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvResultQuestionnaire" OnRowDataBound="gvResultQuestionnaire_RowDataBound"
                                            DataKeyNames="QuestionID" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered"
                                            EmptyDataText="No Questionnaire's Result" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" ShowHeader="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Question ID" SortExpression="QueID" Visible="false">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <table class="table">
                                                            <tr>
                                                                <td style="border: none">
                                                                    <asp:Label ID="lblGVQuestion" runat="server" Text='<%# Bind("QuestionTitle") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="QuestionOptions">
                                                                    <asp:RadioButtonList ID="rblOptions" Font-Bold="false" AutoPostBack="true" runat="server" Enabled="false"></asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="modal-footer tmsModalContent">
                            <div class="row" style="text-align: left">
                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                    <div class="col-sm-6">                                        
                                        <div class="col-sm-5" style="padding-left: 0px">
                                            <table>
                                                <tr>
                                                    <td style="width: 10px">
                                                        <span class="label label-success" style="display: block; padding: 6px; background-color: green;"></span>
                                                    </td>
                                                    <td>&nbsp;Correct Answer
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-sm-5">
                                            <table>
                                                <tr>
                                                    <td style="width: 10px">
                                                        <span class="label label-success" style="display: block; padding: 5px; background-color: red;"></span>
                                                    </td>
                                                    <td>&nbsp;Wrong Answer
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="upWrittenSoplistFooter" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12" style="padding-left: 30px">
                                            <div class="col-sm-5" style="width: 35%;">
                                                <span>Trainer Explained Wrongly Answered Questions to the Trainee</span>
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:RadioButtonList ID="rblWrongAnsExplained" Font-Bold="false" CssClass="rblOptions" CellSpacing="1" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                    <asp:ListItem Text="N/A" Value="NA"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <uc1:SessionTimeOut runat="server" ID="SessionTimeOut" />

     <script type="text/javascript">
        $(document).ready(function () {
            // Fix up GridView to support THEAD tags            
            fixGvHeader();
            fixHeader();
            fixHeaderJr();

        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixGvHeader();
            fixHeader();
            fixHeaderJr();
        });

         function fixGvHeader() {
             var tbl = document.getElementsByClassName("fixHeadwrittenExamSops");
            // Fix up GridView to support THEAD tags     
            if ($(".fixHeadwrittenExamSops").find("thead").length == 0) {
                $(".fixHeadwrittenExamSops tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadwrittenExamSops thead tr").append($(".fixHeadwrittenExamSops th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadwrittenExamSops tbody tr:first").remove();
                }               
            }
        }
         function fixHeader() {
             var tbl = document.getElementsByClassName("fixHeadExamSops");
            // Fix up GridView to support THEAD tags   
            if ($(".fixHeadExamSops").find("thead").length == 0) {
                $(".fixHeadExamSops tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadExamSops thead tr").append($(".fixHeadExamSops th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadExamSops tbody tr:first").remove();
                }               
            }
        }
         function fixHeaderJr() {
             var tbl = document.getElementsByClassName("fixHeadJrList");
            // Fix up GridView to support THEAD tags 
            if ($(".fixHeadJrList").find("thead").length == 0) {
                $(".fixHeadJrList tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadJrList thead tr").append($(".fixHeadJrList th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadJrList tbody tr:first").remove();
                }               
            }
        }
  </script>

</asp:Content>
