﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.Master" AutoEventWireup="true" CodeBehind="Designation.aspx.cs" Inherits="AizantIT_PharmaApp.Designation.Designation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
    <link href="<%=ResolveUrl("~/AppCSS/CreateUserStyles.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/AppCSS/UserLogin/UserLoginStyles.css")%>" rel="stylesheet" />
     <div class=" col-lg-3 padding-none">
				<div class="closebtn"><span>&#9776;</span></div>
				<div style="display:none;" class="closebtn1"><span>&#9776;</span>
				</div>										
			</div>
    <div class="col-sm-12" style="padding-left: 20%; padding-right: 20%">
        <div class="row Panel_Frame">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6">
                        <span class="Panel_Title">Designation</span>
                    </div>
                    <div class="col-sm-6">
                         <asp:HiddenField ID="HFDesgID" runat="server" />
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-left: 10px; margin-right: 10px">
                        <div class="form-group">
                            <label for="name" class="col-sm-4 control-label">Designation Name<span class="mandatoryStar">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="name" id="txtDesignationName" maxlength="50" runat="server" placeholder="Enter Designation Name" onkeypress="return ValidateAlphaNumericSpace(event)" onpaste="return false" tabindex="1" title="Designation Name" />
                            </div>
                        </div>
                  </div>
            </div>
            <div class="col-sm-12 modal-footer Panel_Footer">
                <div class="Panel_Footer_Buttons" >
                      <div style="margin-right:35%;margin-top:8px">
                              <asp:Button ID="btnCancel" Class="pull-right btn btn-cancel_popup" runat="server" Text="Cancel" OnClick="btnCancel_Click" TabIndex="4" ToolTip="Click to Cancel"/>
                <button type="button" id="btnDesignationClear" runat="server" class="pull-right btn btn-cancel_popup" tabindex="3" title="click to Clear">Clear</button> <asp:Button ID="btnSubmit" Class="pull-right btn btn-signup_popup" runat="server" Text="Submit" OnClientClick="javascript:return DesignationRequired();" OnClick="btnSubmit_Click" TabIndex="2" />
                <%--<asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Update" OnClick="btnUpdate_Click"/>--%>
  
                          </div>
                    </div>
                 <div class="Panel_Footer_Message_Label">
                      <asp:Label ID="lblDesignationError" runat="server" Text="" ForeColor="Red"></asp:Label>
                     </div>
            </div>
        </div>
    </div>
      <!--To Show Custom Alert Messages-->
    <script>
        function custAlertMsg(message, alertType, url = '') {
            if (alertType == 'error') {
                document.getElementById("msgError").innerHTML = message;
                $("#ModalError").modal();
            }
            else if (alertType == 'success') {
                document.getElementById("msgSuccess").innerHTML = message;
                $("#ModalSuccess").modal();
            }
            else if (alertType == 'warning') {
                document.getElementById("msgWarning").innerHTML = message;
                $("#ModalWarning").modal();
            }
            else if (alertType == 'info') {
                document.getElementById("msgInfo").innerHTML = message;
                $("#ModalInfo").modal();
            }
            else if (alertType == 'confirm') {
                document.getElementById("msgInfo").innerHTML = message;
                $("#ModalInfo").modal();
            }
        }
    </script>
   <script>
       //for clearing the Designation
       $('#<%=btnDesignationClear.ClientID%>').click(function () {
           $('#<%=txtDesignationName.ClientID%>').val("");
           document.getElementById("<%=lblDesignationError.ClientID%>").innerHTML = "";
       });
       //for mandatory fields in Designation Modal popu
       function DesignationRequired() {

           var NewDesig = document.getElementById("<%=txtDesignationName.ClientID%>").value.trim();
           errors = [];

           if (NewDesig == "") {
               errors.push("Enter Designation Name");
               document.getElementById("<%=txtDesignationName.ClientID%>").focus();
           }
           if (errors.length > 0) {
               document.getElementById("msgError").innerHTML = errors.join("<br/>");
               $("#ModalError").modal();
               return false;
           }
           else {
               return true;
           }s
       }
   </script>


    <script>
        $("#NavLnkDesignation").attr("class", "active");
        $("#NavLnkCreateDesignation").attr("class", "active");
    </script>
</asp:Content>
