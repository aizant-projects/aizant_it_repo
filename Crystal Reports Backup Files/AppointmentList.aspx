﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="AppointmentList.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VisitorRegister.AppointmentList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">
    <div>
        <div class="col-sm-12" >

            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="grid_header col-lg-12  col-xs-12 col-md-12 col-sm-12 ">Visitor Appointment List</div>                                        
                </div>
            </div>

            <div class="col-sm-12">
                <div class="col-sm-6">
                   
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-horizontal" >
                                <div class="form-group" runat="server" id="div20">
                                    <label class="control-label col-sm-4" id="lblDepartmentsearch" style="text-align: left; font-weight: lighter">Select Department</label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlDepatmentSearchVisitors" CssClass="form-control SearchDropDown" runat="server" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="form-horizontal" >
                        <div class="form-group" runat="server" id="div29">
                            <label class="control-label col-sm-4" id="lblVisitorName" style="text-align: left; font-weight: lighter">Visitor Name</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtSearchVisitorsName" runat="server" CssClass="form-control" placeholder="Enter Visitor Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" MaxLength="50"></asp:TextBox>
                                <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtSearchVisitorsName" UseContextKey="true" FirstRowSelected="false"
                                    ServicePath="~/VMS/VisitorRegister/VisitorAppointmentBooking.aspx" ServiceMethod="GetVisitorAppintmentNameList" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1">
                                </ajaxToolkit:AutoCompleteExtender>
                            </div>
                        </div>
                    </div>

                    <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-horizontal" >
                                <div class="form-group" runat="server" id="div30">
                                    <label class="control-label col-sm-4" id="lblPurposeofvisit" style="text-align: left; font-weight: lighter">Purpose of Visit</label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlSearchVisitorsPurposeofVisit" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="form-horizontal" >
                        <div class="form-group" runat="server" id="div31">
                            <label class="control-label col-sm-4" id="lblFromDate" style="text-align: left; font-weight: lighter">From Date</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtSearchVisitorsFromDate" CssClass="form-control" runat="server" placeholder="Select From Date"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">

                    <div class="form-horizontal" >
                        <div class="form-group" runat="server" id="div32">
                            <label class="control-label col-sm-4" id="lblMobile" style="text-align: left; font-weight: lighter">Visitor Mobile No</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtSearchVisitorsMobileNo" runat="server" CssClass="form-control" placeholder="Enter Mobile No" MaxLength="10" onkeypress="return ValidatePhoneNo(event);"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Mobile No Should be 10 Digits" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtSearchVisitorsMobileNo" ValidationGroup="Visitorlist" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtSearchVisitorsMobileNo" UseContextKey="true" FirstRowSelected="false"
                                    ServicePath="~/VMS/VisitorRegister/VisitorAppointmentBooking.aspx" ServiceMethod="GetVisitorAppintmentListByMobile" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1">
                                </ajaxToolkit:AutoCompleteExtender>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal" >
                        <div class="form-group" runat="server" id="div33">
                            <label class="control-label col-sm-4" id="lblVisitorIDSearch" style="text-align: left; font-weight: lighter">Visitor ID</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtSearchVisitorsID" runat="server" CssClass="form-control" placeholder="Enter Visitor ID"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal" >
                        <div class="form-group" runat="server" id="div34">
                            <label class="control-label col-sm-4" id="labelOrganizationName" style="text-align: left; font-weight: lighter">Organization</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtSearchVisitorsOrganizationName" runat="server" CssClass="form-control" placeholder="Enter Organization"></asp:TextBox>
                                <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender6" runat="server" TargetControlID="txtSearchVisitorsOrganizationName" UseContextKey="true" FirstRowSelected="false"
                                    ServicePath="~/VMS/VisitorRegister/VisitorAppointmentBooking.aspx" ServiceMethod="GetVisitorListByOrganization" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1">
                                </ajaxToolkit:AutoCompleteExtender>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal" >
                        <div class="form-group" runat="server" id="div35">
                            <label class="control-label col-sm-4" id="lblToDate" style="text-align: left; font-weight: lighter">To Date</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtSearchVisitorsToDate" runat="server" CssClass="form-control" placeholder="Select To Date"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                <asp:Button ID="btnFind" Text="Find" CssClass="button" runat="server" OnClick="btnFind_Click" ValidationGroup="Visitorlist" />
                <asp:Button ID="btnReset" Text="Reset" CssClass="button" runat="server" OnClick="btnReset_Click" CausesValidation="false" />
            </div>

            <div class="clearfix">
            </div>

            <asp:GridView ID="AppointmentGridView" PagerStyle-CssClass="GridPager" runat="server" CssClass="table table-bordered table-inverse" Style="text-align: center" HorizontalAlign="Center" DataKeyNames="Appointment_SNO" Font-Size="11px"
                HeaderStyle-Wrap="false" AutoGenerateColumns="False" HeaderStyle-Width="2%" EmptyDataText="Currently No Records Found" CellPadding="3" HeaderStyle-HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" AllowPaging="True" ForeColor="#66CCFF" OnPageIndexChanging="AppointmentGridView_PageIndexChanging" OnRowDataBound="AppointmentGridView_RowDataBound">
                <EmptyDataRowStyle ForeColor="#FF3300" HorizontalAlign="Center" />
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#748CB2" Font-Bold="True" ForeColor="White" Height="30px" />
                <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="White" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#0c99f0" />
                <SortedAscendingHeaderStyle BackColor="#0c99f0" />
                <SortedDescendingCellStyle BackColor="#0c99f0" />
                <SortedDescendingHeaderStyle BackColor="#0c99f0" />
                <Columns>
                    <asp:BoundField DataField="Appointment_SNO" HeaderText="SNO" SortExpression="Appointment_SNO" />
                    <asp:BoundField DataField="Appointment_ID" HeaderText="VISITOR ID" SortExpression="Appointment_ID" />
                    <asp:BoundField DataField="FirstName" HeaderText="VISITOR NAME" SortExpression="FirstName" />
                    <asp:BoundField DataField="Appointment_MOBILE_NO" HeaderText="MOBILE NO" SortExpression="Appointment_MOBILE_NO" />
                    <asp:BoundField DataField="PurposeofVisit" HeaderText="PURPOSE OF VISIT" SortExpression="PurposeofVisit" />
                    <asp:BoundField DataField="Organization" HeaderText="ORGANISATION" SortExpression="Organization" />
                    <asp:BoundField DataField="Department" HeaderText="DEPARTMENT" SortExpression="Department" />
                    <asp:BoundField DataField="EmpName" HeaderText="WHOM TO VISIT" SortExpression="EmpName" />
                    <asp:BoundField DataField="Appointment_DATE" HeaderText="APPOINTMENT DATE" SortExpression="Appointment_DATE" />
                    <asp:BoundField DataField="AppointmentStatusName" HeaderText="APPOINTMENT STATUS" SortExpression="AppointmentStatusName" />
                    <asp:TemplateField HeaderText="EDIT">
                        <ItemTemplate>
                            <asp:LinkButton ID="LnkEditVisitor" runat="server" ToolTip="Click To Edit" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" OnClick="LnkEditVisitor_Click"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            <ajaxToolkit:ModalPopupExtender ID="ModalPopupAppointment" runat="server" TargetControlID="lbtPop" PopupControlID="panelVisitor" RepositionMode="RepositionOnWindowResizeAndScroll" DropShadow="true" PopupDragHandleControlID="panelAddNewTitle" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="panelVisitor" runat="server" Style="display: none; background-color: ghostwhite;" ForeColor="Black" Width="900px" Height="515px">
                <asp:Panel ID="panelv" runat="server" Style="cursor: move; font-family: Tahoma; padding: 8px;" HorizontalAlign="Center" BorderColor="#000000" Font-Bold="true" BackColor="#748CB2" ForeColor="White" Height="35">
                    <b>Modify Visitor Appointment List </b>
                </asp:Panel>
                <asp:LinkButton ID="lbtPop" runat="server"></asp:LinkButton>
                <div class="">
                    <div class="col-sm-6">
                        <div class="form-horizontal" >
                            <div class="form-group" runat="server" id="div26">
                                <label class="control-label col-sm-4" id="lblSno" style="text-align: left; font-weight: lighter">SNo</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtEditSNo" runat="server" CssClass="form-control" ReadOnly="true" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" >
                            <div class="form-group" runat="server" id="div5">
                                <label class="control-label col-sm-4" id="lblAppointmentId" style="text-align: left; font-weight: lighter">Appointment ID</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtAppointmentId" runat="server" CssClass="form-control" ReadOnly="true" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" >
                            <div class="form-group" runat="server" id="div1">
                                <label class="control-label col-sm-4" id="lblVisitor Name" style="text-align: left; font-weight: lighter">Visitor Name<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txt_VisitorName" runat="server" CssClass="form-control" placeholder="Enter Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter First Name" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txt_VisitorName" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                    <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="txt_VisitorName" UseContextKey="true" FirstRowSelected="false" OnClientShown="PopupShown"
                                        ServicePath="~/VMS/VisitorRegister/VisitorAppointmentBooking.aspx" ServiceMethod="GetVisitorAppintmentNameList" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1">
                                    </ajaxToolkit:AutoCompleteExtender>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" >
                            <div class="form-group" runat="server" id="div12">
                                <label class="control-label col-sm-4" id="lblLast Name" style="text-align: left; font-weight: lighter">Last Name<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" placeholder="Enter Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Last Name" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtLastName" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" >
                            <div class="form-group" runat="server" id="div2">
                                <label class="control-label col-sm-4" id="lblMobileNo" style="text-align: left; font-weight: lighter">Mobile No<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txt_MobNo" runat="server" CssClass="form-control" placeholder="Enter Mobile" onkeypress="return ValidatePhoneNo(event);" MaxLength="10"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter Mobile No" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txt_MobNo" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorMobile" runat="server" ErrorMessage="Mobile No Should be 10 Digits" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txt_MobNo" ValidationGroup="Visitor" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txt_MobNo" UseContextKey="true" FirstRowSelected="false" OnClientShown="PopupShown"
                                        ServicePath="~/VMS/VisitorRegister/VisitorAppointmentBooking.aspx" ServiceMethod="GetVisitorAppintmentListByMobile" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1">
                                    </ajaxToolkit:AutoCompleteExtender>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" >
                            <div class="form-group" runat="server" id="div3">
                                <label class="control-label col-sm-4" id="lblEmail" style="text-align: left; font-weight: lighter">Email</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txt_EmailID" runat="server" CssClass="form-control" placeholder="Enter Email" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txt_EmailID" ValidationGroup="Visitor" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" >
                            <div class="form-group" runat="server" id="div6">
                                <label class="control-label col-sm-4" id="lblAppointmentDate" style="text-align: left; font-weight: lighter">Appointment Date<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-7">
                                   
                                    <input type='text' runat="server" id="txtdate" class="form-control" placeholder="DD Month YYYY" onpaste="return false" />
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-sm-6">
                        <div class="form-horizontal" >
                            <div class="form-group" runat="server" id="div7">
                                <label class="control-label col-sm-4" id="lblAppointmentTime" style="text-align: left; font-weight: lighter">Appointment Time</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtAppointmentTime" runat="server" CssClass="form-control" placeholder="Enter Appointment Time" TextMode="Time" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" >
                            <div class="form-group" runat="server" id="div4">
                                <label class="control-label col-sm-4" id="lblOrganization" style="text-align: left; font-weight: lighter">Organization</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtOrganisationName" runat="server" CssClass="form-control" placeholder="Enter Organization Name" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                    <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender5" runat="server" TargetControlID="txtOrganisationName" UseContextKey="true" FirstRowSelected="false" OnClientShown="PopupShown"
                                        ServicePath="~/VMS/VisitorRegister/VisitorAppointmentBooking.aspx" ServiceMethod="GetVisitorListByOrganization" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1">
                                    </ajaxToolkit:AutoCompleteExtender>
                                </div>
                            </div>
                        </div>

                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" >
                                    <div class="form-group" runat="server" id="div8">
                                        <label class="control-label col-sm-4" id="lblPurpose" style="text-align: left; font-weight: lighter">Purposeof Visit<span class="mandatoryStar">*</span></label>
                                        <asp:Button ID="btn_purposeofvisit1" runat="server" CssClass="btnnVisit" Text="+" Font-Bold="true" Visible="false" />
                                        <div class="col-sm-7">
                                           
                                            <asp:DropDownList ID="ddlVisitorType" runat="server" CssClass="form-control" ClientIDMode="Static" placeholder="Enter Purpose Of Visit"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Select Purpose of Visit" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlVisitorType" InitialValue="0" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" >
                                    <div class="form-group" runat="server" id="div9">
                                        <label class="control-label col-sm-4" id="lblDepartment" style="text-align: left; font-weight: lighter">Department</label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlDeptToVisit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDeptToVisit_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlDeptToVisit" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="form-horizontal" >
                                    <div class="form-group" runat="server" id="div10">
                                        <label class="control-label col-sm-4" id="lblWhomToVisit" style="text-align: left; font-weight: lighter">Whom To Visit</label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlWhomToVisit" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlWhomToVisit" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div class="form-horizontal" >
                            <div class="form-group" runat="server" id="div11">
                                <label class="control-label col-sm-4" id="lblRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txt_Remarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" >
                            <div class="form-group" runat="server" id="div27">
                                <label class="control-label col-sm-4" id="lblComments" style="text-align: left; font-weight: lighter">Comments<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtVEditComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Enter Comments" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtVEditComments" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                    <asp:Button ID="btnVisitorUpdate" Text="Update" CssClass="button" runat="server" OnClick="btnVisitorUpdate_Click" UseSubmitBehavior="false" ValidationGroup="Visitor" />
                    <asp:Button ID="btnClose" Text="Close" CssClass="button" runat="server" CausesValidation="false" />
                    <asp:Button ID="btnCancel" Text="Cancel Appointment" CssClass="button" runat="server" CausesValidation="false" OnClick="btnCancel_Click" Visible="false" />
                </div>
            </asp:Panel>

          
            <script>
                //for start date and end date validations startdate should be < enddate
                $(function () {
                    $('#<%=txtSearchVisitorsFromDate.ClientID%>').datetimepicker({
                          format: 'DD MMM YYYY',
                          useCurrent: false,
                      });
                      var startdate = document.getElementById('<%=txtSearchVisitorsFromDate.ClientID%>').value
                      $('#<%=txtSearchVisitorsToDate.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        minDate: startdate,
                        useCurrent: false,
                    });
                    $('#<%=txtSearchVisitorsFromDate.ClientID%>').on("dp.change", function (e) {
                        $('#<%=txtSearchVisitorsToDate.ClientID%>').data("DateTimePicker").minDate(e.date);
                             $('#<%=txtSearchVisitorsToDate.ClientID%>').val("");
                    });
                    $('#<%=txtSearchVisitorsToDate.ClientID%>').on("dp.change", function (e) {
                         });
                });
            </script>
            <script type="text/javascript">
                //for datepickers
                var startdate2 = document.getElementById('<%=txtSearchVisitorsFromDate.ClientID%>').value
                if (startdate2 == "") {
                    $('#<%=txtSearchVisitorsFromDate.ClientID%>').datetimepicker({
                         format: 'DD MMM YYYY',
                         useCurrent: false,
                     });
                     $('#<%=txtSearchVisitorsToDate.ClientID%>').datetimepicker({
                         format: 'DD MMM YYYY',
                         useCurrent: false,
                     });
                 }
            </script>

            <script>
                 $("#NavLnkVisitorRegistration").attr("class", "active");
                 $("#AppointmentList").attr("class", "active");
          </script>
             <script>
                 $(function () {
                     $('#<%=txtdate.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        minDate: new Date('<%=txtdate.Value%>'),
                        useCurrent: false,
                     });
                 });
            </script>
            <script>
                   function PopupShown(sender, args) {
                       sender._popupBehavior._element.style.zIndex = 99999999;
                   }
            </script>
            
        </div>
    </div>
</asp:Content>
