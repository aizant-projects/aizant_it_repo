﻿<%@ Page Title="Feedback Master" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="Feedbackmaster.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Administration.Feedback.Feedbackmaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:UpdatePanel runat="server" ID="upHdnFields" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnFeedbackAction" runat="server" Value="0" />
            <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnFeedbackID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnFeedbackQuestionID" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdnFeedbackQuestionAction" runat="server" Value="0" />

    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12  dms_outer_border">
        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text="Your Are Not Authorized to view this Page. Contact Admin.">            
            </asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div id="divFeedbackMasterHeader">
                <div class="grid_header col-lg-12  col-xs-12 col-md-12 col-sm-12">Feedback Master</div>

                <div class="col-lg-4 col-sm-12 col-xs-12 col-md-12 bottom">
                    <label class="padding-none  control-label custom_label_answer">Select Feedback Type<span class="mandatoryStar">*</span></label>
                    <asp:UpdatePanel runat="server" ID="upHeaderDiv" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlType" CssClass="form-control ddlTypeClass selectpicker" OnSelectedIndexChanged="ddlType_SelectedIndexChanged"
                                AutoPostBack="true" data-size="5" runat="server">
                                <asp:ListItem Text="-- Select Feedback Type --" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Job Responsibility (JR)" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Target Training" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="divFeedbackHeader" style="display: none">
                    <asp:UpdatePanel runat="server" ID="upbtnSubmitFb" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="col-lg-4 col-sm-12 col-xs-12 col-md-12">
                                <label class="padding-none col-lg-12 col-sm-12 col-xs-12 col-md-12 control-label custom_label_answer">Feedback Title<span class="mandatoryStar">*</span></label>
                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                                    <input type="text" class="form-control" runat="server" name="name" id="txtFeedbackTitle" placeholder="Enter Feedback Title" maxlength="50" onkeypress="return ValidateAlpha(event)" onpaste="return false" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-12 col-xs-12 col-md-12">
                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                                    <label class="padding-none col-lg-12 col-sm-12 col-xs-12 col-md-12 control-label custom_label_answer">Status<span class="mandatoryStar">*</span></label>
                                    <asp:DropDownList ID="ddlStatus" CssClass="show-tick form-control" data-live-search="true" runat="server">
                                        <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="In-Active" Value="2" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 ">
                                    <label class="padding-none col-lg-12 col-sm-12 col-xs-12 col-md-12 control-label custom_label_answer">Comments<span class="smallred" id="spanFeedbackComments" runat="server" visible="false">*</span></label>
                                    <textarea class="form-control" textmode="MultiLine" runat="server" id="txtFeedbackComments" placeholder="Enter Feedback Comments" title="Comments" maxlength="250" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                <div class="pull-right">

                                    <asp:Button ID="btnSubmit" runat="server" CssClass=" btn btn-approve_popup" Text="Submit" OnClientClick="return FeedbackMasterSubmitValidate();" OnClick="btnSubmit_Click" />

                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <div id="divFeedbackQuestion" style="display: none">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none grid_panel_hod">
                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12  padding-none">
                            <div class="grid_header col-lg-12 col-sm-12 col-xs-12 col-md-12">Feedback Questions List</div>
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 top">
                                <div class="pull-right padding-none">
                                    <button type="button" class=" btn btn-dashboard" runat="server" id="btnFeedBackHistory" onclick="openFM_Historypopup();" style="margin-bottom: 0px;">History</button>
                                    <button type="button" class=" btn btn-dashboard" runat="server" id="btnCreateNewQuestion" onclick="CreateNewQuestion();" style="margin-bottom: 0px;">Create Question</button>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 top">
                                <div>
                                    <table id="tblFeedbackQuestions" class="tblFeedbackQuestionsClass display" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>FeedbackQuestionID</th>
                                                <th>Question</th>
                                                <th>Status</th>
                                                <th>Edit</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="Panel_Footer_Message_Label">
                            <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-------- Modal For Create and Edit Fedback-------------->
    <div id="modalFMCreateOrEdit" class="modal department fade" role="dialog" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" title="Close">&times;</button>
                    <h4 class="modal-title">
                        <label id="lblFeedbackTitle"></label>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class=" col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                        <%-- <asp:UpdatePanel ID="upFaqModal" runat="server" UpdateMode="Always">
                            <ContentTemplate>--%>
                        <div id="divQuestionStatus" style="display: none">
                            <div class="btn-group padding-none col-sm-12 col-xs-12 col-lg-12 col-sm-12 col-xs-12 col-md-12 col-md-12 pull-left" data-toggle="buttons" id="divRadioBtns">
                                <label for="inputPassword3" class=" col-sm-12 padding-none control-label">Question Status</label>
                                <label class="btn btn-primary_radio col-xs-6 col-lg-6 col-md-6 col-sm-6 active" id="lblRadioActiveFQ" runat="server" title="Make Active">
                                    <asp:RadioButton ID="rbtnActive" runat="server" GroupName="A" Text="Active" />
                                </label>
                                <label class="btn btn-primary_radio col-xs-6 col-lg-6 col-md-6 col-sm-6" id="lblRadioInActiveFQ" runat="server" title="Make InActive">
                                    <asp:RadioButton ID="rbtnInActive" runat="server" GroupName="A" Text="In-Active" />
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-right_div" id="divtxtFeedbackQuestions">
                            <label for="inputEmail3" class=" padding-none col-lg-12 col-sm-12 col-xs-12 col-md-12 control-label custom_label_answer">Question<span class="smallred">*</span></label>
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none" >
                                <textarea class="form-control login_input_sign_up" runat="server" id="txtFeedback_Question" placeholder="Enter Question" maxlength="250" title="Question"></textarea>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-right_div" id="divFaqComments">
                            <label for="inputEmail3" class=" padding-none col-lg-12 col-sm-12 col-xs-12 col-md-12 control-label custom_label_answer">Remarks<span class="smallred" id="spanComments" style="display: none">*</span></label>
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                                <textarea class="form-control login_input_sign_up" runat="server" id="txtFeedback_QuestionComments" maxlength="250" placeholder="Enter Modification Reason" title="Remarks"></textarea>
                            </div>
                        </div>
                        <%--    </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel runat="server" ID="upbtn" UpdateMode="Always" style="display: inline-block;">
                        <ContentTemplate>
                            <asp:Button ID="btnSubmitFeedbackQuestion" CssClass="btn btn-signup_popup" OnClick="btnSubmitFeedbackQuestion_Click"
                                Style="margin-bottom: 13%" OnClientClick="return FeedbackMasterQuestionsSubmitValidate();"
                                runat="server" Text="Submit" ToolTip="Click to Submit" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="btn btn-cancel_popup" data-dismiss="modal" title="Click to Cancel">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--End of Create and Edit Feedback model-->

    <!-------- Modal For Viewing the History of Feedback------------->
    <div id="modelFeedback_History" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" title="Close">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label ID="lblmodelFeedback_HistoryTitle" runat="server" Text="Feedback History"></asp:Label>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class=" col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                        <table id="tblFeedback_History" class="tblFeedback_HistoryClass display" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Status</th>
                                    <th>Action By</th>
                                    <th>Action Date</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                 <div class="modal-footer">
                     </div>
            </div>
        </div>
    </div>
    <!-------- End Feedback History-------------->


    <script>
        
        function ShowHideDiv(ddlSelected, ISQuetionsVisible) {
            if (ddlSelected == "1" || ddlSelected == "2") {
                $("#divFeedbackHeader").show();
            }
            else {
                $("#divFeedbackHeader").hide();
                $("#divFeedbackQuestion").hide();
            }
            if (ISQuetionsVisible == "Yes") {
                $("#divFeedbackQuestion").show();
                loadFeedbackQuestions();
            }
            else {
                $("#divFeedbackQuestion").hide();
            }
        }
        function VisibleFM_Questionsdiv() {
            document.getElementById("<%=txtFeedbackComments.ClientID %>").value = "";
            $("#divFeedbackQuestion").show();
            loadFeedbackQuestions();
        }
        function FeedbackMasterSubmitValidate() {
            var FeedbackType = document.getElementById('<%=ddlType.ClientID%>').value;
            var Title = document.getElementById('<%=txtFeedbackTitle.ClientID%>').value;
            var FeedbackComments = document.getElementById('<%=txtFeedbackComments.ClientID%>').value;
            var FeedbackAction = document.getElementById('<%=hdnFeedbackAction.ClientID%>').value;
            errors = [];
            if (FeedbackType == 0) {
                errors.push("Select FeedbackType");
            }
            if (Title == "") {
                errors.push("Enter Feedback Title");
            }
            if (FeedbackAction == "Update") {
                if (FeedbackComments == "") {
                    errors.push("Enter Modification reason in Comments.");
                }
            }
            if (errors.length > 0) {
                if (FeedbackAction == "Update") {
                    custAlertMsg(errors.join("<br/>"), "error","FocusonFBComments()");
                }
                else {
                custAlertMsg(errors.join("<br/>"), "error");
                }
                return false;
            }

            else {
                return true;
            }

        }
        function FocusonFBComments() {
            $("#<%=txtFeedbackComments.ClientID%>").focus();
        }
        function FeedbackMasterQuestionsSubmitValidate() {
        
            var QuestionTitle = document.getElementById('<%=txtFeedback_Question.ClientID%>').value;
             var QuestionComments = document.getElementById('<%=txtFeedback_QuestionComments.ClientID%>').value;
            var QuestionAction = document.getElementById('<%=hdnFeedbackQuestionAction.ClientID%>').value;
            errors = [];
            if (QuestionTitle == "") {
                errors.push("Enter Question");
            }
            if (QuestionAction == "Update") {
                if (QuestionComments == "") {
                    errors.push("Enter Modification reason in Remarks.");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <!--Get Feedback Deatils by Dropdown-->
    <script>
        function CloseFMQuestionsModalandloadQuestionsTable() {
            loadFeedbackQuestions();
            $('#modalFMCreateOrEdit').modal('hide');
        }
        function ClearDatainQuestionsandReloadQuestionstable() {
            ClearDatainQuestionsFields();
            loadFeedbackQuestions();
        }
        function ClearDatainQuestionsFields() {
            document.getElementById("<%=txtFeedback_QuestionComments.ClientID %>").value = "";
            document.getElementById("<%=txtFeedback_Question.ClientID %>").value = "";
        }
        function EditFeedbackQuestion(FeedbackQuestionID, QuestionTitle, QuestionStatus) {
            ClearDatainQuestionsFields();
            //$("#divtxtFeedbackQuestions").hide();
            $("#<%=txtFeedback_Question.ClientID%>").attr("disabled","disabled");
            $("#<%=hdnFeedbackQuestionAction.ClientID%>").val('Update');
            $("#<%=hdnFeedbackQuestionID.ClientID%>").val(FeedbackQuestionID);
            document.getElementById("<%=txtFeedback_Question.ClientID %>").value = QuestionTitle;
            if (QuestionStatus == "Active") {
                $("#<%=lblRadioInActiveFQ.ClientID%>").removeClass("active");
                $("#<%=lblRadioActiveFQ.ClientID%>").addClass("active");
            }
            else {
                $("#<%=lblRadioActiveFQ.ClientID%>").removeClass("active");
                $("#<%=lblRadioInActiveFQ.ClientID%>").addClass("active");
            }
            $('#lblFeedbackTitle').text("Edit Question");
            $("#spanComments").show();
            document.getElementById("<%=btnSubmitFeedbackQuestion.ClientID %>").value = "Update";
            $("#divQuestionStatus").show();
            $('#modalFMCreateOrEdit').modal({ backdrop: 'static', keyboard: false });
            $("#<%=txtFeedback_QuestionComments.ClientID%>").attr('autofocus', 'true');
        }
        function CreateNewQuestion() {
            ClearDatainQuestionsFields();
            $("#<%=hdnFeedbackQuestionAction.ClientID%>").val('Create');
            $('#lblFeedbackTitle').text("Create New Question");
            //$("#divtxtFeedbackQuestions").show();
            $("#<%=txtFeedback_Question.ClientID%>").removeAttr("disabled");
            $("#spanComments").hide();
            document.getElementById("<%=btnSubmitFeedbackQuestion.ClientID %>").value = "Create";
            $("#divQuestionStatus").hide();
            $('#modalFMCreateOrEdit').modal({ backdrop: 'static', keyboard: false });
            $("#<%=txtFeedback_Question.ClientID%>").attr('autofocus', 'true');
        }
        function openFM_Historypopup() {
            document.getElementById("<%=txtFeedback_QuestionComments.ClientID %>").value = "";           
            $('#modelFeedback_History').modal({ backdrop: 'static', keyboard: false });
        }
        $('#modelFeedback_History').on('shown.bs.modal', function (e) {
            ReloadtblFeedback_History();
        });
    </script>
    <!--For Feedback Questions List Regular jQuery Datatable-->
    <script>
        var oTblFeedbackQuestions;
        function loadFeedbackQuestions() {
            if (oTblFeedbackQuestions != null) {
                oTblFeedbackQuestions.destroy();
            }
            oTblFeedbackQuestions = $('#tblFeedbackQuestions').DataTable({
                columns: [
                    { 'data': 'FeedbackQuestionID' },
                    { 'data': 'QuestionTitle' },
                    { 'data': 'StatusName' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            return '<a class="view_tms" href="#" onclick="EditFeedbackQuestion(\'' + o.FeedbackQuestionID + '\',\'' + o.QuestionTitle + '\',\'' + o.StatusName + '\');"></a>';
                        }
                    }
                ],
                "pagingType": "simple_numbers",
                "processing": true,
                "orderClasses": false,
                "order": [0, "desc"],
                "info": true,
                "scrollY": "50vh",
                "scrollX": true,
                "scrollCollapse": true,
                "aoColumnDefs": [{ "targets": [0], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [1, 2] }],
                "bServerSide": true,
                "language": {
                    searchPlaceholder: "Search Records",
                    infoFiltered: "",
                    emptyTable: "No Records Available"
                },
                "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/FeedbackMaster/FeedbackMaster.asmx/getFeedbackQuestionList")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "FeedbackID", "value": $('#<%=hdnFeedbackID.ClientID%>').val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblFeedbackQuestions").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblFeedbackQuestionsClass").css({ "width": "100%" });
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }

            });
        }
    </script>
    <!--For Feedback History List Regular jQuery Datatable-->
    <script>
        var otblFeedback_History;
      
        function loadFeedback_History() {
            if ($('#<%=hdnFeedbackID.ClientID%>').val() != "0") {
                if (otblFeedback_History != null) {
                    otblFeedback_History.destroy();
                }
                otblFeedback_History = $('#tblFeedback_History').DataTable({
                    columns: [
                        { 'data': 'StatusName' },
                        { 'data': 'ActionBy' },
                        { 'data': 'ActionDate' },
                        { 'data': 'Remarks' }
                    ],
                    "pagingType": "simple_numbers",
                    "processing": true,
                    "orderClasses": false,
                    "order": [[2, "desc"]],
                    "info": true,
                    "scrollY": "250px",
                    "scrollCollapse": true,
                    "aoColumnDefs": [{ className: "textAlignLeft", "targets": [0, 1, 3] }],
                    "bServerSide": true,
                    "language": {
                        searchPlaceholder: "Search Records",
                        infoFiltered: "",
                        emptyTable: "No Records Available"
                    },
                    "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
                    "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/FeedbackMaster/Feedbackmaster.asmx/getFeedbackHistoryList")%>",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({ "name": "FeedbackID", "value": $('#<%=hdnFeedbackID.ClientID%>').val() });
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tblFeedback_History").show();
                                $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                                $(".tblFeedback_HistoryClass").css({ "width": "100%" });
                            },
                            error: function (xhr, textStatus, error) {
                                if (typeof console == "object") {
                                    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                }
                            }
                        });
                    }
                    //fnDrawCallback: function () {
                    //    $('.image-details').bind("click", showDetails);
                    //}
                });
            }
            else {
                otblFeedback_History = $('#tblFeedback_History').DataTable();
            }
        }

        function ReloadtblFeedback_History() {
            loadFeedback_History();
        }
    </script>
</asp:Content>
