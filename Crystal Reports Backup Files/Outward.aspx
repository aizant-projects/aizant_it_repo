﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Outward.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.Reports.Outward" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style7 {
            font-size: x-large;
            text-decoration: underline;
        }
        .auto-style23 {
            font-size: medium;
            text-decoration: underline;
        }
        .auto-style25 {
            text-decoration: underline;
            font-size: large;
        }
        .auto-style26 {
            text-decoration: underline;
        }
        .auto-style27 {
            width: 165px;
        }
        .auto-style29 {
            width: 165px;
            text-align: left;
            padding-left: 150px;
            vertical-align:text-top;
        }
        .auto-style30 {
            width: 280px;
        }
        .auto-style31 {
            width: 280px;
            text-align: left;
            padding-left: 100px;
        }
        .auto-style28 {
            height: 90px;
            width: 249px;
        }
        textarea {
    resize: none;
}
        .auto-style32 {
            width: 165px;
            vertical-align:text-top;
            padding-left: 150px;
            text-align:left;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
         <div class="col-lg-2 ">
                  <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Logo.png" Style="position:relative; left:300px; padding: 0px;" class="navbar-brand" Height="45px" Width="177px" />
              </div>
        <div style="text-align:center">
            <table class="auto-style1" style="width:700px;align=center">
                <tr>
                    <td colspan="2"><span class="auto-style7">Aizant Drug Research Solutions Pvt.Ltd</span><br class="auto-style26" />
                          <span class="auto-style23">Private Limited Sy No. 172 &amp; 173, Apparel Park Road, Dulapally Village, Quthbullapur Mandal, Hyderabad - 500014</span></td>
                </tr>
                <tr>
                    <td class="auto-style25" colspan="2">Outward Material Report</td>
                </tr>
                </table>
            <table style="align-content:center" >
                <tr>
                    <td class="auto-style27">&nbsp;</td>
                    <td class="auto-style31">
                        <asp:Label ID="lblSno" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style29">MaterialType:</td>
                    <td class="auto-style31">
                        <asp:Label ID="lblMType" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style29">Material Name</td>
                    <td class="auto-style31">
                        <asp:Label ID="lblMName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style29">Quantity:</td>
                    <td class="auto-style31">
                        <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style29">GP/DC No:</td>
                    <td class="auto-style31">
                        <asp:Label ID="lblDCNo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style29">Vechile No:</td>
                    <td class="auto-style31">
                        <asp:Label ID="lblVechileNo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style29">Department:</td>
                    <td class="auto-style31">
                        <asp:Label ID="lblDept" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style29">Received By:</td>
                    <td class="auto-style31">
                        <asp:Label ID="lblGivenBy" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style29">Name of the Supplier:</td>
                    <td class="auto-style31">
                        <asp:Label ID="lblNameoftheParty" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style29">Material Out DateTime:</td>
                    <td class="auto-style31">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style32">Description of the Material:</td>
                    <td class="auto-style31">
                          <textarea runat="server" id="txtDes" readonly="readonly" style="background-color: white; padding-left: 5px;overflow:hidden; padding-right: 5px;border:none" class="auto-style28" cols="20" name="S2" rows="1"></textarea></td>
                </tr>
                <tr>
                    <td class="auto-style29">Remarks:</td>
                    <td class="auto-style31">
                          <textarea runat="server" id="txtRemarks" readonly="readonly" style="background-color: white;overflow:hidden; padding-left: 5px;font: padding-right: 5px;border:none" class="auto-style28" cols="20" name="S1" rows="1"></textarea></td>
                </tr>
                <tr>
                    <td class="auto-style27">&nbsp;</td>
                    <td class="auto-style30">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style27">&nbsp;</td>
                    <td class="auto-style30">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style27">&nbsp;</td>
                    <td class="auto-style30">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style27">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                    <td class="auto-style30" style="padding-left:100px">Security Signature</td>
                </tr>
                <tr>
                    <td class="auto-style27">&nbsp;</td>
                    <td class="auto-style30" style="padding-left:100px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:left">Printed On:&nbsp;&nbsp;
                        <asp:Label ID="lblPrintedON" runat="server"></asp:Label>
                    </td>
                    <td style="text-align:left;padding-left:20px">Printed By:&nbsp;
                        <asp:Label ID="lblPrintedBY" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
