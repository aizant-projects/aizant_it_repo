﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="FormPrintReqMainList.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.FormPrintReqMainList" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
        <div class=" col-md-12 col-lg-12 col-12 col-sm-12 dms_outer_border float-left padding-none">
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left" id="divMainContainer" runat="server" visible="true">
                <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-right ">
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-md-12 col-lg-12 col-12 col-sm-12 float-left">Form Print Request Main List</div>
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                           <div class="col-12 float-left top padding-none ">
                            <table id="dtDocList" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>FormRequestID</th>
                                        <th>Request Number</th>
                                        <th>Form Name</th>
                                        <th>Form Number</th>
                                        <th>Department</th>
                                        <th>Request Purpose</th>
                                        <th>No.of Copies</th>
                                        <th>Action Status</th>
                                        <th>View/Verify</th>
                                        <th>History</th>
                                    </tr>
                                </thead>
                            </table>
                               </div>
                            <div class=" col-lg-12  float-left bottom">
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/view_tms.png")%>" /><b class="grid_icon_legend">View Form Request</b>
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/verify.png")%>" /><b class="grid_icon_legend">Verify Form Return</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-------- Request VIEW-------------->
    <div id="myModal_request" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 90%">
            <!-- Modal content-->
            <div class="modal-content col-lg-12 col-md-12  padding-none">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            
                            <span id="span_FileTitle" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div id="div_RequestedBy1" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblRequestedBy" runat="server" Text="Requested By" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_RequestedBy" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Requested By" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_NoofCopies1" class="col-md-2 col-lg-2 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblNoofCopies" runat="server" Text="No.of Copies" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_NoofCopies" runat="server" CssClass="form-control login_input_sign_up" TextMode="Number" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Project1" class="col-md-4 col-lg-4 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label1" runat="server" Text="Project Number" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_ProjectNumber" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Project Number" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_DocName1" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label4" runat="server" Text="Form Name" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_DocumentName" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Form Name" AutoPostBack="true" Rows="2" MaxLength="500" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Purpose1" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblComments" runat="server" Text="Purpose" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_Purpose" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Purpose" Width="100%" Rows="2" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_ReviewedBy1" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group bottom float-left">
                                <asp:Label ID="Label6" runat="server" Text="Reviewed By" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_ReviewedBy" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_DocController1" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group bottom float-left">
                                <asp:Label ID="Label7" runat="server" Text="Document Controller" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_DocController" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-12 text-right">
                    <button data-dismiss="modal" class=" btn-cancel_popup">Close</button>
                                </div>
                </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!-------- End Request VIEW-------------->

    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 85%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                           
                            <span id="span4" runat="server"></span>
                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("ActionBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-Width="11%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("ActionDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="13%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-Width="30%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                <asp:TextBox ID="txt_Comments" runat="server" ReadOnly="true" Rows="2" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM">Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL">Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned To">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->

    <!--------Form Return Edit-------------->
    <div id="myFormReturnEdit" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 90%;">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel2">
                    <ContentTemplate>
                        <div class="modal-header">
                           
                            <span id="span2" runat="server"></span>
                             <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                        <div class="modal-body">
                            <div id="div_RequestedBy" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label2" runat="server" Text="Requested By" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_RequestedByReturn" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Requested By" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_NoofCopies" class="col-md-6 col-lg-2 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label3" runat="server" Text="No.of Copies" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_NoofCopiesReturn" runat="server" CssClass="form-control login_input_sign_up" TextMode="Number" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Project" class="col-md-6 col-lg-4 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label5" runat="server" Text="Project Number" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_ProjectNumberReturn" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Project Number" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_DocName" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label8" runat="server" Text="Form Name" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_DocumentNameReturn" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Form Name" AutoPostBack="true" Rows="2" MaxLength="500" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Purpose" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label9" runat="server" Text="Purpose"></asp:Label>
                                <asp:TextBox ID="txt_PurposeReturn" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Purpose" Width="100%" Rows="2" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_ReviewedBy" class="col-md-6 col-lg-4 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label10" runat="server" Text="Reviewed By" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_ReviewedByReturn" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_DocController" class="col-md-6 col-lg-4 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label13" runat="server" Text="Document Controller" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_DocControllerReturn" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_NoofUsedCopies" class="col-md-6 col-lg-2 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label11" runat="server" Text="No.of Copies Used" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_NoofUsedCopies" runat="server" CssClass="form-control login_input_sign_up" TextMode="Number" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_NoofReturnedCopies" class="col-md-6 col-lg-2 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label15" runat="server" Text="No.of Copies Returned" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_NoofReturnedCopies" runat="server" CssClass="form-control login_input_sign_up" TextMode="Number" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_CopyNumbers" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label14" runat="server" Text="Copy Numbers" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_CopyNumbers" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Copy Numbers" Width="100%" Rows="3" MaxLength="300" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Remarks" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label12" runat="server" Text="Remarks" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_Remarks" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Remarks" Width="100%" Rows="3" MaxLength="300" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Comments" class="col-md-12 col-lg-12 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label16" runat="server" Text="Comments" CssClass="label_name"></asp:Label><span class="smallred_label">*</span>
                                <textarea ID="txt_Comments" runat="server" class="form-control " placeholder="Enter Comments" MaxLength="300"></textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-12 modal-footer float-right">
                            <div class="col-12  text-right">
                            <input type="button" id="btn_Verify" runat="server" class=" btn-signup_popup" onclick="return ConfirmAlertVerify();" value="Verify" tabindex="4" />
                            <asp:Button ID="btn_Cancel" Text="Cancel" data-dismiss="modal" CssClass=" btn-cancel_popup" runat="server" CausesValidation="false" />
                        </div>
                            </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Form Return Edit-------------->
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdfRole" runat="server" />
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:UpdatePanel ID="UpGridButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnRequestView" runat="server" Text="Submit" OnClick="btnRequestView_Click" Style="display: none" OnClientClick="showImg();"/>
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" OnClientClick="showImg();"/>
            <asp:Button ID="btnEditFormReturn" runat="server" Text="Submit" Style="display: none" OnClick="btnEditFormReturn_Click" OnClientClick="showImg();"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        $('#dtDocList thead tr').clone(true).appendTo('#dtDocList thead');
        $('#dtDocList thead tr:eq(1) th').each(function (i) {
            if (i < 9) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (oTable.column(i).search() !== this.value) {
                        oTable
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDocList').wrap('<div class="dataTables_scroll" />');
        var oTable = $('#dtDocList').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'FormReqID' },
                { 'data': 'RequestNumber' },
                { 'data': 'FormName' },
                { 'data': 'FormNumber' },
                { 'data': 'DeptName' },
                { 'data': 'RequestPurpose' },
                { 'data': 'NoofCopies' },
                { 'data': 'ActionName' },
                { 'data': 'ActionStatusID' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History" data-toggle="modal" data-target="#myCommentHistory"  onclick=ViewCommentHistory(' + o.FormReqID + ')></a>'; }
                },
            ],
            "responsive": true,
            "paging": true,
            "search": {
                "regex": true
            },
            "aoColumnDefs": [{ "targets": [0,1, 3], "visible": false, "searchable": false },
            { "bSortable": false, "aTargets": [0, 9] }, { "className": "dt-body-left", "targets": [3, 4, 5, 6, 8] },
            {
                targets: [9], render: function (a, b, data, d) {
                    var RoleID = document.getElementById("<%=hdfRole.ClientID%>").value;
                        if (RoleID == "30" && data.ActionStatusID == 8) {
                            return '<a class="verify" title="verify" data-toggle="modal" data-target="#myFormReturnEdit"  onclick=EditFormReturn(' + data.FormReqID + ',' + data.ActionStatusID + ')></a>';
                        }
                        //else if (RoleID == "32" && data.ActionStatusID == 8) {
                        //    return '<a class="verify" title="verify" data-toggle="modal" data-target="#myFormReturnEdit"  onclick=EditFormReturn(' + data.FormReqID + ',' + data.ActionStatusID + ')></a>';
                        //} 
                        else {
                            return '<a class="view" data-toggle="modal" data-target="#myModal_request" title="View" onclick=ViewRequest(' + data.FormReqID + ')></a>';
                        }
                        return "N/A";
                    }
                },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetFormPrintReqMainList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    </script>
    <script>
        function ViewRequest(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnRV = document.getElementById("<%=btnRequestView.ClientID %>");
            btnRV.click();
        }
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
        function EditFormReturn(PK_ID, Action) {
            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            document.getElementById("<%=hdnAction.ClientID%>").value = Action;
            var btnEF = document.getElementById("<%=btnEditFormReturn.ClientID %>");
            btnEF.click();
        }
    </script>
    <script>
        function ReloadInitiationPage() {
            window.open("<%=ResolveUrl("~/DMS/FormPrintReqMainList.aspx")%>", "_self");
            $('#usES_Modal').modal('hide');
        }
        function ReloadVerifyPage() {
            window.open("<%=ResolveUrl("~/DMS/FormPrintReqMainList.aspx?verify=t")%>", "_self");
            $('#usES_Modal').modal('hide');
        }
        function ConfirmAlertVerify() {
            var comment = document.getElementById("<%=txt_Comments.ClientID%>").value;
            errors = [];
            if (comment.trim() == "") {
                errors.push("Enter comments.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                VerifyAction();
                return true;
            }
        }
        function VerifyAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Verify";
            openElectronicSignModal();
        }
    </script>
    <script>
        function openElectronicSignModal() {
            openUC_ElectronicSign();
        }
        function changeHeaderText() {
            var FormReturnVerify = "<%=this.Session["FormReturnVerify"].ToString()%>";
            if (FormReturnVerify == "t") {
                $(".grid_header").text("Form Return Verify List");
            }
            else {
                $(".grid_header").text("Form Print Request Main List");
            }
        }
    </script>
    <script>
        function showImg() {
            ShowPleaseWait('show');
        }
        function hideImg() {
            ShowPleaseWait('hide');
        }
        function HidePopup() {
            $('#usES_Modal').modal('hide');
        }
    </script>
</asp:Content>
