﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="AizantIT_PharmaApp.UserManagement.ForgotPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Forgot Password</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
   <link href="<%=ResolveUrl("~/Content/bootstrap.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/Content/font-awesome.css")%>" rel="stylesheet" />
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.2.1.min.js")%>"></script>
    <script src="<%=ResolveUrl("~/Scripts/bootstrap.min.js")%>"></script>
    <script src="<%=ResolveUrl("~/AppScripts/GlobalScripts.min.js")%>"></script>
     <link href="<%=ResolveUrl("~/AppCSS/AppStyles.css")%>" rel="stylesheet" />
    <style>
        hr {
            margin-top: 0px;
            margin-bottom: 0px;
        }
    </style>

</head>
<body>   
    <form id="form1" runat="server">
        <asp:HiddenField ID="hdnStep" runat="server" Value="0"/>
        <div class="row">
            <div class="col-sm-12">
                 <asp:HyperLink ID="hlnkGoToLogin" style="float:right;padding-right:10px;font-weight:bold" NavigateUrl="~/UserLogin.aspx" runat="server">Go to Login Page</asp:HyperLink>
            </div>
        </div>
        <div class="container">            
            <div class="row Panel_Frame" style="width:600px; margin-left: 25%">
                <div id="header">
                    <div class="col-sm-12 Panel_Header">
                        <div class="col-sm-6">
                            <span class="Panel_Title">Forgot Password</span>
                        </div>
                        <asp:HiddenField ID="HFEmpID" runat="server" />
                    </div>
                </div>
                <div class="row" style="padding: 10px">
                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <div id="divStep1" runat="server">
                                <div class="form-group">
                                    <label class="control-label col-sm-5">Login ID<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="txtLoginID" runat="server" placeholder="Enter Login ID" maxlength="50" onpaste="return false" title="LoginID" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-5">Email ID<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="txtEmailID" runat="server" placeholder="Enter Email ID" maxlength="200" onpaste="return false" title="EmailID" />
                                    </div>
                                </div>
                            </div>
                            <div id="divStep2" runat="server" visible="false">
                                <div class="form-group">
                                    <label class="control-label col-sm-5">Security Question</label>
                                    <div class="col-sm-7">
                                        <textarea class="form-control" id="txtSecurityQues" rows="3" runat="server" placeholder="Security Question" disabled="disabled" maxlength="200" oncopy="return false" title="Security Question"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-5">Answer<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" autocomplete="off" class="form-control" id="txtUserAnswer" runat="server" placeholder="Enter Security Answer" maxlength="20"  oncopy="return false" onpaste="return false" title="Security Answer"/>
                                    </div>
                                </div>
                            </div>
                            <div id="divStep3" runat="server" visible="false">
                                <div class="form-group">
                                    <label class="control-label col-sm-5">New Password<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="password" class="form-control" id="txtNewPwd" runat="server" placeholder="Enter New Password" autocomplete="off" onkeypress="return NoSpace(event)" maxlength="250"/>
                                        <div class="row">
                                           <div class="col-sm-6 PwdValidation">
                                                <table>
                                                    <tr>
                                                        <td><span id="8char" class="glyphicon glyphicon-remove" style="color: #ff9c9e;"></span>&nbsp;</td><td>Atleast 8 Characters</td>                                                       
                                                    </tr>
                                                    <tr>
                                                         <td><span  id="num" class="glyphicon glyphicon-remove" style="color: #ff9c9e;"></span>&nbsp;</td><td>Atleast 1 Number</td>
                                                    </tr>
                                                </table>                                                                                                
                                            </div>
                                           <div class="col-sm-6 PwdValidation">
                                                 <table>
                                                    <tr>
                                                        <td> <span  id="ucase" class="glyphicon glyphicon-remove" style="color: #ff9c9e;"></span>&nbsp;</td><td>Atleast 1 Upper Case</td>                                                       
                                                    </tr>
                                                    <tr>
                                                         <td><span id="lcase"class="glyphicon glyphicon-remove" style="color: #ff9c9e;"></span>&nbsp;</td><td>Atleast 1 Lower Case</td>
                                                    </tr>
                                                </table>                                                   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-5">Confirm Password<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="password" class="form-control" id="txtConfirmPwd" runat="server" placeholder="Enter Confirm Password" autocomplete="off" maxlength="250" />                                        
                                        <div class="row">
                                             <div class="col-sm-6 PwdValidation">
                                                 <table>
                                                    <tr>
                                                        <td><span id="pwmatch" class="glyphicon glyphicon-remove" style="color: #ff9c9e;"></span>&nbsp;</td><td>Password Match</td>
                                                    </tr>
                                                </table>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
                <div class="col-sm-12 modal-footer Panel_Footer" >
                <div class="Panel_Footer_Buttons">
                            <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Next" OnClientClick="return FormValidate();" OnClick="btnSubmit_Click" />
                            <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                        </div>
                       <div class="Panel_Footer_Message_Label">
                           <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                           </div>
                    </div>
            </div>
        </div>
         <script>
             function ConfirmPage() {
                 var confirm_value = document.createElement("INPUT");
                 confirm_value.type = "hidden";
                 confirm_value.name = "confirm_value";
                 if (confirm("Password Updated, Do you want to redirect to Login page ?")) {
                     confirm_value.value = "Yes";
                     window.open("<%=ResolveUrl("~/UserLogin.aspx")%>", "_self");
                 }
                 else {
                 confirm_value.value = "No";
             }
             document.forms[0].appendChild(confirm_value);
             }
    </script>
         <script>
             function Sucessmsg() {
                 alert("Password Updated!");
                 window.open("<%=ResolveUrl("~/UserLogin.aspx")%>", "_self");
             }
    </script>
    </form>
     
    <script>
        function FormValidate()
        {
            var Step = document.getElementById('<%=hdnStep.ClientID%>').value;
            var txtLoginID = document.getElementById("<%=txtLoginID.ClientID%>").value.trim();
            var txtEmailID = document.getElementById("<%=txtEmailID.ClientID%>").value.trim();
          
                if (txtLoginID == '' && txtEmailID == '') {
                    alert("Enter Login ID  and Email ID");
                    document.getElementById("<%=txtLoginID.ClientID%>").focus();
                  return false;
                }
                if (txtLoginID == '') {
                    alert("Enter Login ID");
                    document.getElementById("<%=txtLoginID.ClientID%>").focus();
                    return false;
                }
                if (txtEmailID == '') {
                    alert("Enter Email ID");
                    document.getElementById("<%=txtEmailID.ClientID%>").focus();
                    return false;
                }
                else if (!IsValidEmail(txtEmailID)) {
                    alert("Invalid Email ID");
                    document.getElementById("<%=txtEmailID.ClientID%>").focus();
                    return false;
                }
              else {
                  return true;
              }
                }            
            if (Step == "1") {
                var txtUserAnswer = document.getElementById('<%=txtUserAnswer.ClientID%>').value.trim();
                
                if (txtUserAnswer == '') {
                    alert("Enter Security Answer");
                    document.getElementById("<%=txtUserAnswer.ClientID%>").focus();
                    return false;
                 }
                else if (txtUserAnswer.length <= 1) {
                    alert("SecurityAnswer Should Be Atleast 2 Characters");
                    document.getElementById("<%=txtUserAnswer.ClientID%>").focus();
                return false;
                }
                else {
                    return true;
                }
            }
            if (Step == "2") {
                var txtNewPwd = document.getElementById("<%=txtNewPwd.ClientID%>").value.trim();
                var txtConfirmPwd = document.getElementById("<%=txtConfirmPwd.ClientID%>").value.trim();
                
                if (txtNewPwd == '' && txtConfirmPwd == '') {
                    alert("Enter New Password  and Confirm Password");
                    document.getElementById("<%=txtNewPwd.ClientID%>").focus();
                    return false;
                }
                if (txtNewPwd == '') {
                    alert("Enter New Password");
                    document.getElementById("<%=txtNewPwd.ClientID%>").focus();
                    return false;
                 }
                if (txtConfirmPwd == '') {
                     alert("Enter Confirm Password");
                     document.getElementById("<%=txtConfirmPwd.ClientID%>").focus();
                    return false;
                }
                else {
                    return vaidatePwd();    
                }
            }
        }
    </script>
    
     <script type="text/javascript">
         function vaidatePwd() {
             var txtPassword = document.getElementById("<%=txtNewPwd.ClientID%>").value.trim();
             var txtConfirmPassword = document.getElementById("<%=txtConfirmPwd.ClientID%>").value.trim();
                      
                var p1 = document.getElementById("<%=txtNewPwd.ClientID%>").value;
                var p2 = document.getElementById("<%=txtConfirmPwd.ClientID%>").value;
                errors = [];
                if (p1.length < 8) {
                    errors.push("Your password must be a minimum of 8 characters");
                }
                if (p1.search(/[a-z]/) < 0) {
                    errors.push("Your password must contain at least one Lower Case.");
                }
                if (p1.search(/[A-Z]/) < 0) {
                    errors.push("Your password must contain at least one Upper Case.");
                }
                if (p1.search(/[0-9]/) < 0) {
                    errors.push("Your password must contain at least one Number.");
                }
                if (errors.length > 0) {
                    alert(errors.join("\n"));
                    return false;
                }
                if (p1 != p2) {
                    alert("Password and Confirm Password should match.");
                    return false;
                }
                else {
                    return true;
                }             
         }
                  

    </script>
   
    <script>
        //To show icons for password validation
        $("input[type=password]").keyup(function () {
            var ucase = new RegExp("[A-Z]+");
            var lcase = new RegExp("[a-z]+");
            var num = new RegExp("[0-9]+");

            if ($("#<%=txtNewPwd.ClientID%>").val().length >= 8) {
                $("#8char").removeClass("glyphicon-remove");
                $("#8char").addClass("glyphicon-ok");
                $("#8char").css("color", "#2be04b");
            } else {
                $("#8char").removeClass("glyphicon-ok");
                $("#8char").addClass("glyphicon-remove");
                $("#8char").css("color", "#ff9c9e");
            }

            if (ucase.test($("#<%=txtNewPwd.ClientID%>").val())) {
                $("#ucase").removeClass("glyphicon-remove");
                $("#ucase").addClass("glyphicon-ok");
                $("#ucase").css("color", "#2be04b");
            } else {
                $("#ucase").removeClass("glyphicon-ok");
                $("#ucase").addClass("glyphicon-remove");
                $("#ucase").css("color", "#ff9c9e");
            }

            if (lcase.test($("#<%=txtNewPwd.ClientID%>").val())) {
                $("#lcase").removeClass("glyphicon-remove");
                $("#lcase").addClass("glyphicon-ok");
                $("#lcase").css("color", "#2be04b");
            } else {
                $("#lcase").removeClass("glyphicon-ok");
                $("#lcase").addClass("glyphicon-remove");
                $("#lcase").css("color", "#ff9c9e");
            }

            if (num.test($("#<%=txtNewPwd.ClientID%>").val())) {
                $("#num").removeClass("glyphicon-remove");
                $("#num").addClass("glyphicon-ok");
                $("#num").css("color", "#2be04b");
            } else {
                $("#num").removeClass("glyphicon-ok");
                $("#num").addClass("glyphicon-remove");
                $("#num").css("color", "#ff9c9e");
            }

            if ($("#<%=txtNewPwd.ClientID%>").val() == $("#<%=txtConfirmPwd.ClientID%>").val() & $("#<%=txtConfirmPwd.ClientID%>").val().length >= 1) {
                $("#pwmatch").removeClass("glyphicon-remove");
                $("#pwmatch").addClass("glyphicon-ok");
                $("#pwmatch").css("color", "#2be04b");
            } else {
                $("#pwmatch").removeClass("glyphicon-ok");
                $("#pwmatch").addClass("glyphicon-remove");
                $("#pwmatch").css("color", "#ff9c9e");
            }
        });
    </script>

     <script>
         //no space in txtPassword
         function nospaces(t) {
             if (t.value.match(/\s/g)) {
                 t.value = t.value.replace(/\s/g, '');
             }
         }
    </script>
</body>
    </html>