﻿<%@ Page Title="JR Audit Trail" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="JR_AuditTrail.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Reports.AuditReports.JR_AuditTrail" %>
<%@ Register Src="~/UserControls/SessionTimeOut.ascx" TagPrefix="uc1" TagName="SessionTimeOut" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
       
        .table-text-center th, .table-text-center td {
            text-align: center;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnTS_Status" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="col-lg-12  col-xs-12 col-md-12 col-sm-12 dms_outer_border">
        <div class="col-lg-12  col-xs-12 col-md-12 col-sm-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div class="panel panel-default" id="mainContainer" runat="server">
             <div class="grid_header col-lg-12  col-xs-12 col-md-12 col-sm-12 "> <asp:Label ID="lblTitle" runat="server" Font-Bold="true">JR Audit Trail</asp:Label></div>
            <div class="col-lg-12  col-xs-12 col-md-12 col-sm-12 padding-none top">
                <asp:UpdatePanel ID="upDropdowns" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-lg-4  col-xs-12 col-md-4 col-sm-4 padding-none">
                            <label class="col-lg-12  col-xs-12 col-md-12 col-sm-12 control-label">Department<span class="mandatoryStar">*</span></label>
                            <div class="col-lg-12  col-xs-12 col-md-12 col-sm-12">
                                <asp:DropDownList ID="ddlAccesibleDepartments" OnSelectedIndexChanged="ddlAccesibleDepartments_SelectedIndexChanged" CssClass="form-control SearchDropDown" runat="server" AutoPostBack="true" Width="100%"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-lg-4  col-xs-12 col-md-4 col-sm-4 padding-none">
                            <label class="col-lg-12  col-xs-12 col-md-12 col-sm-12 control-label">Employee<span class="mandatoryStar">*</span></label>
                            <div class="col-lg-12  col-xs-12 col-md-12 col-sm-12">
                                <asp:DropDownList ID="ddlEmployee" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged" CssClass="form-control SearchDropDown" runat="server" AutoPostBack="true" Width="100%"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-lg-4  col-xs-12 col-md-4 col-sm-4 padding-none">
                            <label class="col-lg-12  col-xs-12 col-md-12 col-sm-12 control-label">Select JR <span class="mandatoryStar">*</span></label>
                            <div class="col-lg-12  col-xs-12 col-md-12 col-sm-12">
                                <asp:DropDownList ID="ddlEmpJRs_List" CssClass="form-control SearchDropDown" runat="server" OnSelectedIndexChanged="ddlEmpJRs_List_SelectedIndexChanged"  AutoPostBack="true" Width="100%"></asp:DropDownList>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="col-lg-12  col-xs-12 col-md-12 col-sm-12 top" style=" overflow: auto; min-height: 532px; max-height: 532px;">
                <input name="txtTerm" class="gridSearchBox" type="search" oninput="GridViewSearch(this, '<%=gvJR_AuditTrail.ClientID %>')" 
                    placeholder="Search" style="width: 258px;margin-bottom: 3px;" />
               <asp:UpdatePanel ID="upGVJR_AuditTrail" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="gvJR_AuditTrail" runat="server" OnRowCommand="gvJR_AuditTrail_RowCommand" OnRowDataBound="gvJR_AuditTrail_RowDataBound"
                            AutoGenerateColumns="False" OnSorting="gvJR_AuditTrail_Sorting" AllowSorting="false"
                           CssClass="table table-fixed fixHead"
                            EmptyDataText="No Records in JR Audit Trail" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" DataKeyNames="JR_ID">
                            <AlternatingRowStyle BackColor="White" />
                             <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                            <Columns>
                                <asp:TemplateField HeaderText="JR ID" Visible="false" HeaderStyle-CssClass="clsDefault">
                                    <ItemTemplate>
                                        <asp:Label ID="lblJR_ID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="JR Name" HeaderStyle-ForeColor="White" SortExpression="JR_Name" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtJR_Name" runat="server" Text='<%# Bind("JR_Name") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblJR_Name" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="JR Action" HeaderStyle-ForeColor="White" SortExpression="JR_Action" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtJR_Action" runat="server" Text='<%# Bind("JR_Action") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblJR_Status" runat="server" Text='<%# Bind("JR_Status") %>' Visible="false"></asp:Label>
                                        <asp:LinkButton ID="lnkViewJR" runat="server" CommandName="ViewJR" CommandArgument='<%# Eval("JR_ID") %>' ToolTip="Click To View JR">                                            
                                             <span><i class="glyphicon glyphicon-new-window "></i>JR Approved</span>
                                        </asp:LinkButton>

                                        <asp:Label ID="lbltextand" runat="server" Text=" and "></asp:Label>

                                        <asp:LinkButton ID="lnkViewTS_Audit" runat="server" CommandName="ViewTS_AuditDetails" CommandArgument='<%# Eval("JR_ID") %>' ToolTip="Click To View TS">
                                         <span><i class="glyphicon glyphicon-new-window "></i> TS Assigned</span>
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="lnkViewTE" runat="server" CommandName="ViewTE" CommandArgument='<%# Eval("JR_ID") %>' ToolTip="Click To View TE">
                                         <span><i class="glyphicon glyphicon-new-window "></i>Trainer Evalution</span>
                                        </asp:LinkButton>

                                        <asp:Label ID="lblJR_Action" runat="server" Text='<%# Bind("JR_Action") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action By" HeaderStyle-ForeColor="White" SortExpression="JR_ActionByName" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtJR_ActionByName" runat="server" Text='<%# Bind("JR_ActionByName") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblJR_ActionByName" runat="server" Text='<%# Bind("JR_ActionByName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action Date" HeaderStyle-ForeColor="White" SortExpression="JR_ActionDate" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtJR_ActionDate" runat="server" Text='<%# Bind("JR_ActionDate") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblJR_ActionDate" runat="server" Text='<%# Bind("JR_ActionDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="JR_Name" HeaderText="JR Name" />
                                <asp:BoundField DataField="JR_Action" HeaderText="JR Action" />
                                <asp:BoundField DataField="JR_ActionByName" HeaderText="Action By" />
                                <asp:BoundField DataField="JR_ActionDate" HeaderText="Action Date" />
                                <asp:TemplateField HeaderText="View">
                                    <ItemTemplate>
                                        <asp:LinkButton Text="View" runat="server" CommandName="Edit" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%> 
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!-- Modal popup  for JR_Training-->
    <div class="modal fade" id="modalTS_AuditTrail" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 90%">
            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 0px">
                <div class="modal-header tmsModalHeader">
                    <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title Panel_Title">Training Audit</h4>
                </div>
                <div class="modal-body tmsModalBody">
                    <div class="row tmsModalContent">
                        <div class="col-sm-12" style="overflow: auto; min-height: 420px; max-height: 420px;">
                           <asp:UpdatePanel ID="upgvTS_AuditTrail" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="gvTS_AuditTrail" runat="server" OnRowCommand="gvTS_AuditTrail_RowCommand" OnRowDataBound="gvTS_AuditTrail_RowDataBound"
                                        AutoGenerateColumns="False" OnSorting="gvTS_AuditTrail_Sorting" AllowSorting="false"
                                         CssClass="table table-fixed fixHeader"
                                        EmptyDataText="No Records in JR Audit Trail" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" DataKeyNames="JR_ID">
                                        <AlternatingRowStyle BackColor="White"/>
                                        <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="JR ID" Visible="false" HeaderStyle-CssClass="clsDefault">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblJR_ID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TS Action" HeaderStyle-ForeColor="White" SortExpression="TS_Action" HeaderStyle-CssClass="col-xs-4" ItemStyle-CssClass="col-xs-4">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtTS_Action" runat="server" Text='<%# Bind("TS_Action") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTS_Status" runat="server" Text='<%# Bind("TS_Status") %>' Visible="false"></asp:Label>
                                                    <asp:LinkButton ID="lnkViewTS" runat="server" CommandName="ViewTS" CommandArgument='<%# Eval("JR_ID") %>' ToolTip="Click To View TS">
                                         <span><i class="glyphicon glyphicon-new-window "></i>TS Approved</span>
                                                    </asp:LinkButton>
                                                    <asp:Label ID="lbltextand" runat="server" Text=" and "></asp:Label>
                                                    <asp:LinkButton ID="lnkViewJR_training" runat="server" CommandName="ViewJR_Training" CommandArgument='<%# Eval("JR_ID") %>' ToolTip="Click To View TE">
                                         <span><i class="glyphicon glyphicon-new-window "></i>JR_Training</span>
                                                    </asp:LinkButton>
                                                    <asp:Label ID="lblTS_Action" runat="server" Text='<%# Bind("TS_Action") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action By" HeaderStyle-ForeColor="White" SortExpression="TS_ActionByName" HeaderStyle-CssClass="col-xs-4" ItemStyle-CssClass="col-xs-4">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtTS_ActionByName" runat="server" Text='<%# Bind("TS_ActionByName") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTS_ActionByName" runat="server" Text='<%# Bind("TS_ActionByName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action Date" HeaderStyle-ForeColor="White" SortExpression="TS_ActionDate" HeaderStyle-CssClass="col-xs-4" ItemStyle-CssClass="col-xs-4">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtTS_ActionDate" runat="server" Text='<%# Bind("TS_ActionDate") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTS_ActionDate" runat="server" Text='<%# Bind("TS_ActionDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                               </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal for JR view -->
    <div class="modal fade" id="modelJR_View" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 90%">
            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 0px">
                <div class="modal-header tmsModalHeader">
                    <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title Panel_Title">JR Details</h4>
                </div>
                <div class="modal-body tmsModalBody">
                    <div class="row tmsModalContent">
                        <asp:UpdatePanel ID="UpJRDetails" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#JR_Description">Description</a></li>
                                    <li><a data-toggle="tab" href="#JR_Comments">Comments History</a></li>
                                </ul>
                                <div class="tab-content" style="padding-top: 5px">
                                    <div id="JR_Description" class="tab-pane fade in active">
                                        <div id="JRrow" style="margin: 0px; padding: 3px 10px; overflow: auto; max-height: 400px; min-height: 400px;">
                                            <div class="col-sm-12">
                                                <div style="border: groove; height: 390px; overflow: auto; border-width: 1px; padding: 10px">
                                                    <asp:Literal ID="ltrJR_Description" runat="server"></asp:Literal>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="JR_Comments" class="tab-pane fade">
                                        <div class="row" style="margin: 0px; padding: 3px 10px; overflow: auto; max-height: 400px; min-height: 400px;">
                                            <asp:GridView ID="gvJR_Commentry" runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixHeadCommentry"
                                                EmptyDataText="No Comment's" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                  <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Role" SortExpression="Role" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtRole" runat="server" Text='<%# Bind("Role") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRole" runat="server" Text='<%# Bind("Role") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action" SortExpression="Action" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtAction" runat="server" Text='<%# Bind("Action") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAction" runat="server" Text='<%# Bind("Action") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Commented By" SortExpression="EmpName" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Commented Date" SortExpression="CommentedDate" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtCommentedDate" runat="server" Text='<%# Bind("CommentedDate") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCommentedDate" runat="server" Text='<%# Bind("CommentedDate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Comment" SortExpression="Comment" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtComment" runat="server" Text='<%# Bind("Comment") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblComment" runat="server" Text='<%# Bind("Comment") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="modal-footer tmsModalContent">
                    <div class="row" style="text-align: center">
                        <%--<button type="button" data-dismiss="modal" runat="server" id="btnModelClose" class="btn TMS_Button">Close</button>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal for TS view -->
    <div class="modal fade" id="modelTS_View" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 90%">
            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 0px">
                <div class="modal-header tmsModalHeader">
                    <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title Panel_Title">Training Schedule Details</h4>
                </div>
                <div class="modal-body tmsModalBody">
                    <div class="row tmsModalContent">
                        <asp:UpdatePanel ID="upGvModalGrids" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#Regular_TS">Regular<sup><asp:Label ID="lblReguralCount" runat="server"></asp:Label></sup></a></li>
                                    <li><a data-toggle="tab" href="#Retrospective_TS">Retrospective<sup><asp:Label ID="lblRetCount" runat="server"></asp:Label></sup></a></li>
                                    <li><a data-toggle="tab" href="#Comments">Comments History</a></li>
                                </ul>
                                <div class="tab-content" style="padding-top: 5px">
                                    <div id="Regular_TS" class="tab-pane fade in active">
                                        <div id="row" style="overflow: auto; max-height: 400px; min-height: 400px;">
                                            <div class="col-sm-12 padding-none">
                                                <%-- <asp:UpdatePanel ID="upGvViewTS" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>--%>
                                                <asp:GridView ID="gvViewTS" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-fixed fixview"
                                                    EmptyDataText="No Training Schedule for Approval" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                    <AlternatingRowStyle BackColor="White" />
                                                      <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S.No." HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Dept Code" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="SOP Numbers" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditSopNums" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSopNums" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Trainers" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditTrainers" runat="server" Text='<%# Bind("Trainers") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTrainers" runat="server" Text='<%# Bind("Trainers") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No.of Days" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDays" runat="server" Text='<%# Bind("Days") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDays" runat="server" Text='<%# Bind("Days") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <%-- </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="Retrospective_TS" class="tab-pane fade">
                                        <div class="row" style="margin: 0px; padding: 3px 10px; overflow: auto; max-height: 400px; min-height: 400px;">
                                            <div class="col-sm-12">
                                                <%-- <asp:UpdatePanel ID="upGvViewTS" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>--%>
                                                <asp:GridView ID="gvRet_TS" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-fixed fixGridView"
                                                    EmptyDataText="No Training Schedule for Approval" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                    <AlternatingRowStyle BackColor="White" />
                                                      <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S.No." HeaderStyle-CssClass="col-xs-4" ItemStyle-CssClass="col-xs-4">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Dept Code" HeaderStyle-CssClass="col-xs-4" ItemStyle-CssClass="col-xs-4">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="SOP Numbers" HeaderStyle-CssClass="col-xs-4" ItemStyle-CssClass="col-xs-4">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditRQ_Sops" runat="server" Text='<%# Bind("RQ_Sops") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRQ_Sops" runat="server" Text='<%# Bind("RQ_Sops") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <%-- </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="Comments" class="tab-pane fade">
                                        <div class="row" style="margin: 0px; padding: 3px 10px; overflow: auto; max-height: 400px; min-height: 400px;">
                                            <%--<asp:UpdatePanel ID="upgvTS_Commentry" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>--%>
                                            <asp:GridView ID="gvTS_Commentry" runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixCommentryHead"
                                                EmptyDataText="No Comment's" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                 <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Role" SortExpression="Role" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtRole" runat="server" Text='<%# Bind("CommenterRole") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRole" runat="server" Text='<%# Bind("CommenterRole") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action" SortExpression="Action" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtAction" runat="server" Text='<%# Bind("Action") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAction" runat="server" Text='<%# Bind("Action") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Commented By" SortExpression="EmpName" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Commented Date" SortExpression="CommentedDate" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtCommentedDate" runat="server" Text='<%# Bind("CommentedDate") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCommentedDate" runat="server" Text='<%# Bind("CommentedDate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Comment" SortExpression="Comment" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtComment" runat="server" Text='<%# Bind("Comment") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblComment" runat="server" Text='<%# Bind("Comment") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <%-- </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="modal-footer tmsModalContent">
                    <div class="row" style="text-align: center">
                        <%--<button type="button" data-dismiss="modal" runat="server" id="btnModelClose" class="btn TMS_Button">Close</button>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal popup  for JR_Training-->
    <div class="modal fade" id="UnderTrainingSopsModalView" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 90%">
            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 0px">
                <div class="modal-header tmsModalHeader">
                    <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title Panel_Title">Training Sop's</h4>
                </div>
                <div class="modal-body tmsModalBody">
                    <div class="row tmsModalContent">
                        <div class="col-sm-6">
                            <div class="form-horizontal">
                                <asp:UpdatePanel ID="upddlJR_TrainingDepts" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Select Trained Department</label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlJR_Training_Depts" CssClass="form-control SearchDropDown" OnSelectedIndexChanged="ddlJR_Training_Depts_SelectedIndexChanged" runat="server" AutoPostBack="true" Width="100%"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="col-sm-12" style="overflow: auto; min-height: 420px; max-height: 420px;">
                            <asp:UpdatePanel ID="upGVSopsJR_Training" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="gvSopsJR_Training" runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixGridHeadJr"
                                        EmptyDataText="No Record's" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" Style="margin-bottom: 0px !important;">
                                        <AlternatingRowStyle BackColor="White" />
                                         <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sop ID" Visible="false">
                                                <EditItemTemplate>
                                                    <asp:Label ID="lblEditSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SOP No." HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                <EditItemTemplate>
                                                    <asp:Label ID="lblEditDocumentNumber" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocumentNumber" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Version" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                <EditItemTemplate>
                                                    <asp:Label ID="lblEditDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SOP Name" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                <EditItemTemplate>
                                                    <asp:Label ID="lblEditDocumentName" runat="server" Text='<%# Bind("Doc_Name") %>'></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%# Bind("Doc_Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Training Dept" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                <EditItemTemplate>
                                                    <asp:Label ID="lblEditTrained_DeptCode" runat="server" Text='<%# Bind("Trained_DeptCode") %>'></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTrained_DeptCode" runat="server" Text='<%# Bind("Trained_DeptCode") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Read Duration" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                <EditItemTemplate>
                                                    <asp:Label ID="lblEditDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                <EditItemTemplate>
                                                    <asp:Label ID="lblEditExamStatus" runat="server" Text='<%# Bind("ExamStatus") %>'></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblExamStatus" runat="server" Text='<%# Bind("ExamStatus") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--trainer evaluated--%>
    <!-- Modal popup  for Sops list based on selected department-->
    <div class="modal fade" id="SopsModalView" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 90%">
            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 0px">
                <div class="modal-header tmsModalHeader">
                    <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title Panel_Title">Trained Sops</h4>
                </div>
                <div class="modal-body tmsModalBody">
                    <div class="row tmsModalContent">
                        <asp:UpdatePanel ID="upddlTrainedDept" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="col-sm-6">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Select Trained Department</label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlJR_Trained_Depts" CssClass="form-control SearchDropDown" runat="server" OnSelectedIndexChanged="ddlJR_Trained_Depts_SelectedIndexChanged" AutoPostBack="true" Width="100%"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="upTrainerEval" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-sm-12">
                                    <ul class="nav nav-tabs pull-left">
                                        <li class="active"><a data-toggle="tab" href="#tabgvOralExam" class="tabstyle">Oral/Practical<sup><asp:Label ID="lblOralRecCount" runat="server"></asp:Label></sup></a></li>
                                        <li><a data-toggle="tab" href="#tabgvWrittenExam" class="tabstyle">Written<sup><asp:Label ID="lblWrittenRecCount" runat="server"></asp:Label></sup></a></li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div id="tabgvOralExam" class="tab-pane fade in active">
                                        <div class=" col-sm-12">
                                            <asp:UpdatePanel ID="upGvOralExamSops" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvOralExamSops" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvOralExamSops_RowDataBound" OnRowCommand="gvOralExamSops_RowCommand"
                                                        CssClass="table table-fixed fixHeadExam"
                                                        EmptyDataText="No Oral Exam Record's" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" Style="margin-bottom: 0px !important;">
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Sop ID" Visible="false">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SOP No." HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditDocumentNumber" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentNumber" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SOP Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditDocumentName" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Trained Dept" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTrained_DeptCode" runat="server" Text='<%# Bind("Trained_DeptCode") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrained_DeptCode" runat="server" Text='<%# Bind("Trained_DeptCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Read Duration" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Evaluated Trainer" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditEvaluatedBY" runat="server" Text='<%# Bind("EvaluatedTrainerBY") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEvaluatedBY" runat="server" Text='<%# Bind("EvaluatedTrainerBY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Evaluated Date" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditEvaluatedDate" runat="server" Text='<%# Bind("EvaluatedDate") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEvaluatedDate" runat="server" Text='<%# Bind("EvaluatedDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Remarks" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2" ControlStyle-ForeColor="#9a9a9c">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrainerComments" runat="server" Text='<%# Bind("TrainerComments") %>' Visible="false"></asp:Label>
                                                                    <asp:LinkButton ID="lnkViewComments" CssClass="fa fa-commenting-o GV_Edit_Icon" runat="server" CommandName="ViewTraineeComments" CommandArgument='<%# Eval("TrainerComments") %>' ToolTip="Click To View"></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <%--<i class="fa fa-commenting-o"></i>--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Trainer Opinion" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTrainer_Opinion" runat="server" Text='<%# Bind("Trainer_Opinion") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrainer_Opinion" runat="server" Text='<%# Bind("Trainer_Opinion") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Exam Type" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditExamType" runat="server" Text='<%# Bind("ExamType") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExamType" runat="server" Text='<%# Bind("ExamType") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div id="tabgvWrittenExam" class="tab-pane fade">
                                        <div class=" col-sm-12 ">
                                            <asp:UpdatePanel ID="upGvViewSops" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvWrittenExamSops" runat="server" AutoGenerateColumns="False" DataKeyNames="DocumentID,Trainer_ExplanationStatus"
                                                        CssClass="table table-fixed fixHeadSop" OnRowCommand="gvWrittenExamSops_RowCommand"
                                                        EmptyDataText="No Written Exam Record's" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Sop ID" Visible="false">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SOP No." HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditDocumentNumber" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentNumber" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SOP Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditDocumentName" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Trained Dept" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTrained_DeptCode" runat="server" Text='<%# Bind("Trained_DeptCode") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrained_DeptCode" runat="server" Text='<%# Bind("Trained_DeptCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Read Duration" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="No of Attempts" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditAttemptedCount" runat="server" Text='<%# Bind("AttemptedCount") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAttemptedCount" runat="server" Text='<%# Bind("AttemptedCount") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Evaluated Trainer" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditEvaluatedBY" runat="server" Text='<%# Bind("EvaluatedTrainerBY") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEvaluatedBY" runat="server" Text='<%# Bind("EvaluatedTrainerBY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Evaluated Date" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditEvaluatedDate" runat="server" Text='<%# Bind("EvaluatedDate") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEvaluatedDate" runat="server" Text='<%# Bind("EvaluatedDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Answer Sheet" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkViewExamReport" runat="server" Font-Underline="true" CommandName="ViewExamResult" ToolTip="Click to view Exam Record"
                                                                        CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'>
                                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/TMS_Images/DocSearch.png" Style="width: 25px" />
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal popup for exam Result-->
    <div class="modal fade" id="SopExamResultModal" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 90%; height: 600px">
            <div class="modal-content">
                <div class="modal-header tmsModalHeader">
                    <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title Panel_Title" style="color: aliceblue">Exam Result</h4>
                </div>
                <div class="modal-body tmsModalBody">
                    <div class="row form-horizontal">
                        <asp:UpdatePanel ID="upSopExamResultBody" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="margin: 0px 6px 0px 10px">
                                    <div class="col-sm-6">
                                        <span style="font-weight: bold">Exam Attempted Date:</span>
                                        <label class="control-label" style="font-weight: normal" runat="server" id="lblExamDate" title="Exam Date and Time"></label>
                                    </div>
                                    <div class="col-sm-6" style="text-align: right; padding-right: 32px">
                                        <span style="font-weight: bold">Marks :</span>
                                        <label class="control-label" style="font-weight: normal" runat="server" id="lblMarks" title="Marks Obtained In this Sop"></label>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="row tmsModalContent" style="overflow: auto; padding: 0px; max-height: 490px; min-height: 490px;">
                        <asp:UpdatePanel ID="upGvResultQuestionnaire" runat="server" UpdateMode="Conditional" style="margin: 10px">
                            <ContentTemplate>
                                <asp:GridView ID="gvResultQuestionnaire" OnRowDataBound="gvResultQuestionnaire_RowDataBound"
                                    DataKeyNames="QuestionID" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered"
                                    EmptyDataText="No Questionnaire's Result" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" ShowHeader="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Question ID" SortExpression="QueID" Visible="false">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <table class="table">
                                                    <tr>
                                                        <td style="color:#2e2e2e; background:#c0d4d7; font-size:12pt; font-weight:bold;">
                                                            <asp:Label ID="lblGVQuestion" runat="server" Text='<%# Bind("QuestionTitle") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="QuestionOptions">
                                                            <asp:RadioButtonList ID="rblOptions" Font-Bold="false" AutoPostBack="true" runat="server" Enabled="false"></asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="modal-footer tmsModalContent">
                    <div class="row" style="text-align: left">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="col-sm-5" style="padding-left: 0px">
                                    <table>
                                        <tr>
                                            <td style="width: 10px">
                                                <span class="label label-success" style="display: block; padding: 6px; background-color: green;"></span>
                                            </td>
                                            <td>&nbsp;Correct Answer
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-sm-5">
                                    <table>
                                        <tr>
                                            <td style="width: 10px">
                                                <span class="label label-success" style="display: block; padding: 5px; background-color: red;"></span>
                                            </td>
                                            <td>&nbsp;Wrong Answer
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="upWrittenSoplistFooter" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-sm-12" style="padding-left: 30px">
                                    <div class="col-sm-5" style="width: 32%;padding:10px">
                                        <span style="font-weight:bold;">Trainer explained wrongly answered questions to the Trainee</span>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:RadioButtonList ID="rblWrongAnsExplained" Font-Bold="false" CssClass="rblOptions" CellSpacing="1" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                            <asp:ListItem Text="N/A" Value="NA"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <%--modal popup to load Trainer Remarks--%>
    <div class="modal fade" id="ModalTrainerRemarks" role="dialog">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header tmsModalHeader">
                    <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title Panel_Title" style="color: aliceblue">Trainer Remarks</h4>
                </div>
                <div class="modal-body tmsModalBody">
                    <asp:UpdatePanel ID="upModalBody" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="row tmsModalContent">
                                <div class="col-sm-12">
                                    <textarea class="form-control" id="txtTrainerRemarks" disabled="disabled" style="height: 100px" cols="12" runat="server" placeholder="Enter Revert Reason" aria-multiline="true" title="Trainer Remarks"></textarea>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer tmsModalContent">
                    <div class="row" style="text-align: center">
                        <%-- <asp:UpdatePanel ID="UpBtnRevert" runat="server" UpdateMode="Conditional" style="display: inline-block">
                        <ContentTemplate>--%>
                        <asp:Button ID="btnClose" CssClass="btn TMS_Button" data-dismiss="modal" runat="server" Text="Close" />
                        <%--   </ContentTemplate>
                    </asp:UpdatePanel>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--<div class=" col-lg-12 pull-right padding-none">
        <div class=" col-lg-12 padding-none">
            <div class=" col-lg-12 grid_area padding-none">

                <div >
                    <div id="header">
                        <div class="col-sm-12 Panel_Header">
                            <div class="col-sm-6" style="padding-left: 3px">
                                <span class="Panel_Title"> JR Audit Rtrail</span>
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                    </div>
                    <div id="body" class="col-sm-12" style="overflow: auto">
                       
                        <div class="col-sm-12" style="padding-right: 0px;padding-top:2%;">
                            <table id="dtJR_List" class="display" cellspacing="0" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>JR Name</th>
                                        <th>Action</th>
                                        <th>Action By</th>
                                        <th>Action Date</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>--%>
    <uc1:SessionTimeOut runat="server" ID="SessionTimeOut" />
    <script>
         //to open the TS Details Modal
         function OpenModelViewTS() {
             $('#modelTS_View').modal({ backdrop: 'static', keyboard: false });
         }
         //to open the JR Details Modal
         function OpenModelViewJR() { 
             $('#modelJR_View').modal({ backdrop: 'static', keyboard: false });
        }
       
     </script>
     <script>
         //for gridview search
         function GridViewSearch(phrase, _id) {
             var words = phrase.value.toLowerCase().split(" ");
             var table = document.getElementById(_id);
             var ele;
             for (var r = 1; r < table.rows.length; r++) {
                 ele = table.rows[r].innerHTML.replace(/<[^>]+>/g, "");
                 var displayStyle = 'none';
                 for (var i = 0; i < words.length; i++) {
                     if (ele.toLowerCase().indexOf(words[i]) >= 0)
                         displayStyle = '';
                     else {
                         displayStyle = 'none';
                         break;
                     }
                 }
                 table.rows[r].style.display = displayStyle;
             }
         }
    </script>

    <script type="text/javascript">
         $(document).ready(function () {
             // Fix up GridView to support THEAD tags            
             fixGvHeader();
             fixHeader();
             fixHeaderCommentry();
             fixHeaderView();
             addThead();
             CommentryHead();
             addTheadJR();
             addTheadExam();
             addTheadSop();
            
         });
         var prm = Sys.WebForms.PageRequestManager.getInstance();
         prm.add_endRequest(function () {
             fixGvHeader();
             fixHeader();
             fixHeaderCommentry();
             fixHeaderView();
             addThead();
             CommentryHead();
             addTheadJR();
             addTheadExam();
             addTheadSop();
         }); 
         
         function fixGvHeader() {    
             var tbl = document.getElementsByClassName("fixHead");
              // Fix up GridView to support THEAD tags     
             if ($(".fixHead").find("thead").length == 0) {               
                 $(".fixHead tbody").before("<thead><tr></tr></thead>");
                 $(".fixHead thead tr").append($(".fixHead th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHead tbody tr:first").remove();
                 }               
             }             
         }
         function fixHeader() {
             var tbl = document.getElementsByClassName("fixHeader");
             // Fix up GridView to support THEAD tags   
             if ($(".fixHeader").find("thead").length == 0) {
                 $(".fixHeader tbody").before("<thead><tr></tr></thead>");
                 $(".fixHeader thead tr").append($(".fixHeader th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHeader tbody tr:first").remove();
                 }               
             }
         }
         function fixHeaderCommentry() {
             var tbl = document.getElementsByClassName("fixHeadCommentry");
              // Fix up GridView to support THEAD tags 
             if ($(".fixHeadCommentry").find("thead").length == 0) {
                 $(".fixHeadCommentry tbody").before("<thead><tr></tr></thead>");
                 $(".fixHeadCommentry thead tr").append($(".fixHeadCommentry th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHeadCommentry tbody tr:first").remove();
                 }               
             }
         }
         function fixHeaderView() {
             var tbl = document.getElementsByClassName("fixview");
              // Fix up GridView to support THEAD tags   
             if ($(".fixview").find("thead").length == 0) {
                 $(".fixview tbody").before("<thead><tr></tr></thead>");
                 $(".fixview thead tr").append($(".fixview th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixview tbody tr:first").remove();
                 }               
             }
         }
         function addThead() {
             var tbl = document.getElementsByClassName("fixGridView");
             // Fix up GridView to support THEAD tags 
             if ($(".fixGridView").find("thead").length == 0) {
                 $(".fixGridView tbody").before("<thead><tr></tr></thead>");
                 $(".fixGridView thead tr").append($(".fixGridView th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixGridView tbody tr:first").remove();
                 }               
             }
         }
         function CommentryHead() {
             var tbl = document.getElementsByClassName("fixCommentryHead");
              // Fix up GridView to support THEAD tags       
             if ($(".fixCommentryHead").find("thead").length == 0) {
                 $(".fixCommentryHead tbody").before("<thead><tr></tr></thead>");
                 $(".fixCommentryHead thead tr").append($(".fixCommentryHead th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixCommentryHead tbody tr:first").remove();
                 }               
             }
         }
         function addTheadJR() {
             var tbl = document.getElementsByClassName("fixGridHeadJr");
             // Fix up GridView to support THEAD tags
             if ($(".fixGridHeadJr").find("thead").length == 0) {
                 $(".fixGridHeadJr tbody").before("<thead><tr></tr></thead>");
                 $(".fixGridHeadJr thead tr").append($(".fixGridHeadJr th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixGridHeadJr tbody tr:first").remove();
                 }               
             }
         }
         function addTheadExam() {
             var tbl = document.getElementsByClassName("fixHeadExam");
              // Fix up GridView to support THEAD tags
             if ($(".fixHeadExam").find("thead").length == 0) {
                 $(".fixHeadExam tbody").before("<thead><tr></tr></thead>");
                 $(".fixHeadExam thead tr").append($(".fixHeadExam th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHeadExam tbody tr:first").remove();
                 }               
             }
         }
         function addTheadSop() {
             var tbl = document.getElementsByClassName("fixHeadSop");
             // Fix up GridView to support THEAD tags    
             if ($(".fixHeadSop").find("thead").length == 0) {
                 $(".fixHeadSop tbody").before("<thead><tr></tr></thead>");
                 $(".fixHeadSop thead tr").append($(".fixHeadSop th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHeadSop tbody tr:first").remove();
                 }               
             }
         }
</script>
    
  <%--  <script>
         function HeaderScroll() {
             var h = parseInt($('.table-fixed thead').css('height'),400);
             if (h < 400) {
                 alert("1");
                 $('.table-fixed thead').css('width', '100%');
             }
             else {
                 alert("2");
                 $('.table-fixed thead').css('width', '99%');
             }
         }
    </script>--%>
       
</asp:Content>
