﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucViewDocFAQs.ascx.cs" Inherits="AizantIT_PharmaApp.UserControls.TMS_UserControls.ucViewDocFAQs" %>
<asp:HiddenField ID="hdnPageIndex" runat="server" Value="0" />
<asp:HiddenField ID="hdnDocID" runat="server" Value="0" />
<asp:HiddenField ID="hdnCurrentPage" runat="server" Value="0" />

<script src="<%=ResolveUrl("~/AppScripts/Pager.min.js")%>"></script>

<!-------- Create FAQ popup-------------->
<div id="modalDocFAQs" class="modal department fade" role="dialog">
    <div class="modal-dialog" style="min-width: 98%;">
        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Frequently Asked Question</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12 padding-none">
                    <div class="col-lg-1 float-left padding-none ">
                        <label for="filter" class="col-lg-8 padding-none">Page Size</label>
                        <select class="selectpicker ddlPageSize col-lg-12 padding-none form-control" onchange="callGetFAQ_Questions('1','Y');" id="ddlPager">
                            <option value="10" selected="selected">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                        </select>
                    </div>
                    <div class="col-lg-4 float-right">
                        <label for="filter" class="col-lg-12 padding-none">Search</label>
                        <input id="txtSearch" type="search" placeholder="Search Question" oninput="callGetFAQ_Questions('1','Y');" onkeypress="RestrictEnterKey(event);" value="" class="form-control" data-type="accordion-search"/>
                        <input id="txtSearch1" type="search" placeholder="Search Question" style="display:none" autocomplete="off" class="form-control" value="" data-type="accordion-search"/>
                    </div>
                </div>
                <div class="col-12 float-left" id="accordionFAQ">
                    <div class="panel panel-default">                        
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body" >
                                <div class="FAQ_Answer">
                                    Loading...
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-none float-left" id="divPager" style="margin-top:20px;"></div>
            </div>
  
        </div>
    </div>
</div>
<!-------- End FAQ question-------------->

<script>
    $("#accordionFAQ").collapse();
</script>

<!--Show FAQ Modal-->
<script>
    function OpenDocFAQPopup(DocID) {       
        $("#<%=hdnDocID.ClientID%>").val(DocID);
        callGetFAQ_Questions('1', 'Y');
        $('#modalDocFAQs').modal({ show: true, backdrop: 'static', keyboard: false });
    }
    function messageinCustomPopupsFAQ(msgDescription, msgType) {
        custAlertMsg(msgDescription, msgType, true);
    }
    
</script>

<!--Load FAQs On Document-->
<script>
    function callGetFAQ_Questions(PageIndex, InitializePager) {
        UserSessionCheck();
        var PageSize = $('.ddlPageSize option:selected').val();
        var DocID = $("#<%=hdnDocID.ClientID%>")[0].value;
        var Search = $("#txtSearch").val();
        var objStr = { "PageIndex": PageIndex, "PageSize": PageSize, "DocID": DocID, "Search": Search };
        var jsonData = JSON.stringify(objStr);
        $.ajax({
            type: "POST",
            url: '<%= Page.ResolveUrl("~/TMS/WebServices/DocumentFAQ/DocumentFAQ_Service.asmx/DocFAQs")%>',
            data: jsonData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                GetFAQ_QuestionsResponse(response, InitializePager, PageIndex);
            },
            failure: function (response) {
                // alert(response.d);
            },
            error: function (response) {
                // alert(response.d);
            }
        });
    }

    function GetFAQ_QuestionsResponse(response, InitializePager, PageIndex) {
        var div = $("#accordionFAQ div").eq(0).clone(true);
        $("#accordionFAQ div").eq(0).remove();
        $("#accordionFAQ").empty();
        var QuestionsObj = response.d;
        var result = $.parseJSON(QuestionsObj);
        var recordsCountValue = result['recordsCount'];
        var recordsValue = result['records'];
        for (var i = 0; i < recordsValue.length; i++) {
            var divQuestions = "<div class='card top'><div class='card-header accordion_faq_tms' style='border-bottom: 1px solid #d4d4d4;'><h4 class='panel-tile float-left title_faq'>" +
                "<a class='collapsed right_icon_acc' data-toggle='collapse' " +
                " href='#collapse" + recordsValue[i].QuestionID + "' onclick='loadAnswer(" + recordsValue[i].QuestionID + ");' style= 'display: block'>" +
                recordsValue[i].QuestionDescription + "</a></h4></div>" +
                "<div id='collapse" + recordsValue[i].QuestionID + "' class=' collapse' data-parent='#accordionFAQ'><div class='card-body'><div class='FAQ_Answer' style='border-bottom: 1px solid #38b8ba;'>Loading..." +
                "</div></div></div></div>";
            $("#accordionFAQ").append(divQuestions);
            div = $("#accordionFAQ div").eq(0).clone(true);
            $("#accordionFAQ").scrollTop();
        }
        if (recordsCountValue == '0') {
            $("#accordionFAQ").prepend("<div class='question_none'>No Questions To Show</div>");
        }
        var totalPageIndex = parseFloat(parseInt(recordsCountValue) / $("#ddlPager").val());
        totalPageIndex = Math.ceil(totalPageIndex);
        if (InitializePager == 'Y') {
            InitPagination(totalPageIndex);
            highlightCurrentPage(1);
        }
        else {
            highlightCurrentPage(PageIndex);
        }      
    }

    function getPageSelectValue(control) {      
        callGetFAQ_Questions($(control).text(), 'N');
    };    

    function loadAnswer(QuestionID) {
        var txtAnswer = $('div#collapse' + QuestionID + ' > div > div').html();
        if (txtAnswer == 'Loading...') {
            callGetFAQAnswer(QuestionID);
        }
    }
</script>

<!--Get the FAQ Answer-->
<script>
    function callGetFAQAnswer(QuestionID) {
        UserSessionCheck();
        var objStr = { "QuestionID": QuestionID };
        var jsonData = JSON.stringify(objStr);
        $.ajax({
            type: "POST",
            url: '<%= Page.ResolveUrl("~/TMS/WebServices/DocumentFAQ/DocumentFAQ_Service.asmx/getDocFAQsAnswer")%>',
            data: jsonData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var Answerresult = $.parseJSON(response.d);
                $('div#collapse' + QuestionID + ' > div > div').html(Answerresult);
            },
            failure: function (response) {
                // alert(response.d);
            },
            error: function (response) {
                alert(response.d);
            }
        });
    }
</script>
