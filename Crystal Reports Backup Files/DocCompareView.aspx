﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocCompareView.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DMSFileView.DocCompareView" %>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
         .dxreControl .dxreView {
            background-color: #E0E0E0;
            height: 1150px !important;
        }

        .dxreView {
            position: relative;
            overflow: auto;
            padding: 0 10px;
            -webkit-overflow-scrolling: touch;
            -webkit-appearance: none;
            -ms-touch-action: pan-y pinch-zoom;
            touch-action: pan-y pinch-zoom;
            height: 1150px !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <link href="<%=ResolveUrl("~/Content/bootstrap.css")%>" rel="stylesheet" />
        <script src="<%=ResolveUrl("~/Scripts/jquery-3.2.1.js")%>"></script>
        <script src="<%=ResolveUrl("~/Scripts/bootstrap.min.js")%>"></script>
        <link href="<%=ResolveUrl("~/Content/diff_styles.css")%>" rel="stylesheet" />
        <script>
            function hideTrackChanges() {
                $('.divTrackChanges').hide();
                $('.divShowDocument').show();
            }
            function showTrackChanges() {
                $('.divTrackChanges').show();
                $('.divShowDocument').hide();
            }

        </script>
        <div class="divTrackChanges">
            <div class="panel_word_panel col-xs-12" style="background: #a3cdd3 !important; color: #056775 !important; font-size: 12pt; font-weight: bold;">
                Track Changes
            <div class="pull-right padding-none text-right">
                <asp:Button ID="Button1" CssClass="btn btn-primary" Style="background: #fff !important; color: #07889a !important; font-size: 10pt; text-align: right" runat="server" Text="Show Modified Document" OnClick="Button1_Click" Visible="false" />
            </div>
            </div>
            <div class="col-lg-6" style="margin-top: 10px;">
                <div id="div2" class="panel_word_panel col-xs-12" runat="server"></div>

                <div runat="server" id="ltFile1" class="col-xs-12" style="overflow: auto; float: left; padding: 10px; border: 1px solid #07889a;"></div>
            </div>
            <div class="col-lg-6" style="margin-top: 10px;">
                <div id="div3" class="panel_word_panel col-xs-12" runat="server"></div>
                <div runat="server" id="ltFile2" class="col-xs-12" style="overflow: auto; padding: 10px; float: left; border: 1px solid #07889a;"></div>
            </div>
        </div>
        <div class="divShowDocument" style="display: none">
            <div class="panel_word_panel col-xs-12" style="background: #a3cdd3 !important; color: #056775 !important; font-size: 12pt; font-weight: bold;">
                Modified Document
           <div class="pull-right padding-none text-right">
               <asp:Button ID="Button2" CssClass="btn btn-primary" Style="background: #fff !important; color: #07889a !important; font-size: 10pt; text-align: right" runat="server" Text="Show Track Changes" OnClientClick="showTrackChanges();" />
           </div>
            </div>
            <div class="col-xs-12">
                <dx:ASPxRichEdit ID="ASPxRichEdit1" runat="server" WorkDirectory="~\App_Data\WorkDirectory" RibbonMode="None" ReadOnly="true" Width="100%" Settings-Behavior-FullScreen="Enabled"></dx:ASPxRichEdit>
            </div>
        </div>
        <asp:Label ID="lblError" runat="server" Visible="false"></asp:Label>
        <asp:HiddenField ID="hdfDocVID" runat="server" />
        <asp:HiddenField ID="hdfFromEmpID" runat="server" />
        <asp:HiddenField ID="hdfFromRoleID" runat="server" />
    </form>
</body>
</html>
