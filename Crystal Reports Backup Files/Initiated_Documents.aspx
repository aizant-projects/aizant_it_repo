﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="Initiated_Documents.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentApprover.Initiated_Documents" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <style>
       
      

        table tbody tr td {
            text-align: center;
            height: 30px;
        }
          </style>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 padding-none float-left">
        <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 dms_outer_border float-left padding-none" style="min-height: 0px !important">
            <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div id="divMainContainer" runat="server" visible="true">
                <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 padding-none float-right ">
                    <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-lg-12 float-left">Approve  Initiation Request</div>
                        <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                             <div class="col-12 top float-left  padding-none">
                            <table id="dtDates" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Version ID</th>
                                        <th>Document Number</th>
                                        <th>Document Name</th>
                                        <th>Department</th>
                                        <th>Document Type</th>
                                        <th>Initiated By</th>
                                        <th>Status</th>
                                        <th>Request Type</th>
                                        <th>Effective Date</th>
                                        <th>Document Status</th>
                                        <th>Review Date</th>
                                        <th>vDocumentID</th>
                                        <th>Training</th>
                                        <th>Action</th>
                                        <th>PendingDocRequestCount</th>
                                        <th>History</th>
                                        <th>RequestTypeID</th>
                                    </tr>
                                </thead>
                            </table>
                                 </div>
                            <div class=" col-lg-12 float-left bottom">
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/approve_training.png")%>" />
                                <b class="grid_icon_legend">Under Training/Pending Document Distribution Request's </b>
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/approve.png")%>" />
                                <b class="grid_icon_legend">Action</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------Document Details VIEW-------------->
    <div id="myModal_docDetails" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog dms_sop_list " style="min-width: 85%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    
                    <span id="span2" runat="server">Initiation Approval</span>
                    <button type="button" class="close" data-dismiss="modal" style="width: 50px;">&times;</button>
                </div>
                 <div class="modal-body Modalpopup-Scroll">
                <div class=" col-lg-12" style="background: #fff;">
                    <asp:UpdatePanel ID="UpDocDeails" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class=" col-lg-12 top padding-none float-left">
                                <div class="form-group  col-lg-3 float-left">
                                    <asp:Label ID="lbl_requestType" runat="server" Text="Request Type" CssClass="label_name"></asp:Label>
                                    <asp:TextBox ID="txt_requestType" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div id="type1" runat="server" class="form-group  col-lg-2 float-left">
                                    <asp:Label ID="Label2" runat="server" Text="Document Type" CssClass="label_name"></asp:Label>
                                    <asp:TextBox ID="txt_documentType" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div class="form-group  col-lg-4 float-left" id="div_dept1" runat="server">
                                    <asp:Label ID="Label5" runat="server" Text="Department" CssClass="label_name"></asp:Label>
                                    <asp:TextBox ID="txt_Department" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div class="form-group  col-lg-3 float-left" id="DocumentNumber" runat="server">
                                    <asp:Label ID="Label3" runat="server" Text="Document Number" CssClass="label_name"></asp:Label>
                                    <asp:TextBox ID="txt_DocumentNumber" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div class="form-group col-lg-4 float-left" id="div3" runat="server" visible="false">
                                </div>
                                <div id="div_Namenew1" class="col-lg-6 form-group float-left" runat="server">
                                    <asp:Label ID="Label4" runat="server" Text="Document Name" CssClass="label_name"></asp:Label>
                                    <asp:TextBox ID="txt_DocumentName" runat="server" TextMode="MultiLine" CssClass="form-control " Height="80px" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div id="div_Purpose" runat="server" class="col-lg-6 form-group float-left">
                                    <asp:Label ID="lblComments" runat="server" CssClass="label_name"></asp:Label>
                                    <asp:TextBox ID="txt_Comments" runat="server" TextMode="MultiLine" CssClass="form-control " Width="100%" Height="80px" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div class="form-group col-lg-1 float-left" id="div_Version" runat="server">
                                    <asp:Label ID="Label9" runat="server" Text="Version" CssClass="label_name"></asp:Label>
                                    <asp:TextBox ID="txt_VersionID" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div id="divPublic" class="form-group  col-lg-3 col-12 col-md-12 col-sm-12 float-left">
                                    <asp:Label ID="lblRadiobutton" runat="server" Text="Privacy" CssClass="label_name"></asp:Label>
                                    <asp:RadioButtonList runat="server" ID="RadioButtonList1" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Public" Value="yes"></asp:ListItem>
                                        <asp:ListItem Text="Private" Value="no"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div id="div_Userroles11" class="col-lg-4 form-group float-left">
                                    <asp:Label ID="Label7" runat="server" Text="Author" CssClass="label_name"></asp:Label>
                                    <asp:TextBox ID="txt_Author" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div id="div1" class="col-lg-4 form-group float-left">
                                    <asp:Label ID="Label8" runat="server" Text="Authorizer" CssClass="label_name"></asp:Label>
                                    <asp:TextBox ID="txt_Authorizer" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div class="col-lg-12 form-group float-left">
                                    <asp:UpdatePanel ID="approverDivGV" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="panel-heading_title col-md-12 col-lg-12 col-12 col-sm-12">
                                               Selected Employees
                                            </div>
                                            <div class="col-12 float-left grid_panel_full ">
                                            <asp:GridView ID="gvApprover" CssClass="padding-right_div col-md-12 col-lg-12 col-12 col-sm-12  top bottom cal" runat="server" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:BoundField DataField="Role" HeaderText="Role" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                                    <asp:BoundField DataField="EmpName" HeaderText="Employee" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                                    <asp:BoundField DataField="DeptCode" HeaderText="DeptCode" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                                </Columns>
                                            </asp:GridView>
                                                </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="col-md-12 col-lg-12 col-12 col-sm-12 form-group float-left">
                                    <asp:Label ID="Label1" runat="server" Text="Comment"  CssClass="label_name"></asp:Label><span class="smallred_label">*</span>
                                    <textarea ID="txt_Reject" runat="server" Style="height: 50px;" class="form-control "  placeHolder="Please Enter Comment" MaxLength="300"></textarea>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class=" col-lg-12 top bottom float-left" style="text-align: right;">
                        <asp:UpdatePanel ID="upBtnsDocDetails" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Button ID="btn_Approve" runat="server" Text="Approve" CssClass=" btn-signup_popup" OnClick="btn_Approve_Click" OnClientClick="return commentRequriedApprove();" />
                                <asp:Button ID="btn_Reject" runat="server" Text="Revert" CssClass=" btn-revert_popup" ValidationGroup="app" OnClick="btn_Reject_Click" OnClientClick="return commentRequried();" />
                                <asp:Button ID="btn_cancel" data-dismiss="modal" OnClientClick="ReasonCancel();" runat="server" Text="Cancel" CssClass=" btn-cancel_popup" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                     </div>
            </div>
        </div>
    </div>
    <!--------End Document Details VIEW-------------->

    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog modal-lg" style="min-width:85%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                           
                            <span id="span1" runat="server"></span>
                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("GivenBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" >
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("CommentedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Time Spent">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Time" runat="server" Text='<%# Eval("Time") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action"  ItemStyle-Width="22%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-Width="26%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                <asp:TextBox ID="txt_Comments" runat="server" ReadOnly="true" Rows="4" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM">Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL">Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned To">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->

    <asp:HiddenField ID="hdfPKID" runat="server" />
    <asp:HiddenField ID="hdfDocID" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdfTraining" runat="server" />
    <asp:HiddenField ID="hdnRequestType" runat="server" Value="0" />
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnViewDocumentDetails" runat="server" Text="Submit" Style="display: none" OnClick="btnViewDocumentDetails_Click" OnClientClick="showImg();"/>
            <asp:Button ID="btnViewDocumentDetails1" runat="server" Text="Submit" Style="display: none" OnClick="btnViewDocumentDetails1_Click" OnClientClick="showImg();"/>
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" OnClientClick="showImg();"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        $('#dtDates thead tr').clone(true).appendTo('#dtDates thead');
        $('#dtDates thead tr:eq(1) th').each(function (i) {
            if (i < 9) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDates').wrap('<div class="dataTables_scroll" />');
        var table = $('#dtDates').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'VersionID' },
                { 'data': 'DocumentNumber' },
                { 'data': 'DocumentName' },
                { 'data': 'Department' },
                { 'data': 'DocumentType' },
                { 'data': 'InitiatedBy' },
                { 'data': 'RecordStatus' },
                { 'data': 'RequestType' },
                { 'data': 'EffectiveDate' },
                { 'data': 'ExpirationDate' },
                { 'data': 'Status' },
                { 'data': 'vDocumentID' },
                { 'data': 'IsTraining' },
                { 'data': 'TrainingCount' },
                { 'data': 'PendingDocRequestCount' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        return '<a  class="summary_latest" title="History" data-toggle="modal" data-target="#myCommentHistory"  onclick=ViewCommentHistory(' + o.VersionID + ')></a>';
                    }
                },
                { 'data': 'RequestTypeID' }
            ],
            "aoColumnDefs": [{ "targets": [0,1, 7, 9, 10, 11, 12, 13,15,17], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 14] }, { "className": "dt-body-left", "targets": [2, 3, 4, 5, 6, 8] },
            {
                targets: [8], render: function (a, b, data, d) {
                    if (data.RequestType == "New Document") {
                        return 'New';
                    } else if (data.RequestType == "Revision Of Existing Document") {
                        return 'Revision';
                    }
                    else if (data.RequestType == "Withdrawal Document") {
                        return 'Withdraw';
                    }
                    return "N/A";
                }
            },
            {
                targets: [14], render: function (a, b, data, d) {
                    if ((data.TrainingCount == false && data.IsTraining == true && data.RequestTypeID == 3) || (data.PendingDocRequestCount != "0")) {
                        return '<a class="approve_training" data-target="#myModal_docDetails" data-toggle="modal" title="Under Training/Pending Document Distribution Requests" onclick=ViewDocument1(' + data.VersionID + ',' + data.TrainingCount + ',' + data.RequestTypeID + ')></a>';
                        //return "<a class='edit_icon_feed Edit' style='margin-left:0%' onclick='EditUOM_Details(" + full.UOM_ID + ",\"" + full.UOM_Name + "\",\"" + full.UOM_Description + "\");'></a>";
                    } else {
                        return '<a class="view_req"  data-target="#myModal_docDetails" data-toggle="modal" title="Action" onclick=ViewDocument(' + data.VersionID + ',' + data.TrainingCount + ',' + data.RequestTypeID + ')></a>';
                    }
                    return "N/A";
                }
            },],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetQADocList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "ReqTypeID", "value": <%=hdnRequestType.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDates").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
        function reloadtable() {
            table.ajax.reload();
            $('#myModal_docDetails').modal('hide');
            $('#usES_Modal').modal('hide');
        }
    </script>
    <script>
        function ViewDocument(PK_ID, T_ID, R_ID) {
            document.getElementById("<%=txt_Reject.ClientID%>").value = '';
            document.getElementById("<%=hdfPKID.ClientID%>").value = PK_ID;
            document.getElementById("<%=hdfTraining.ClientID%>").value = T_ID;
            document.getElementById("<%=hdnRequestType.ClientID%>").value = R_ID;
            var btnVD = document.getElementById("<%=btnViewDocumentDetails.ClientID %>");
            btnVD.click();
        }
        function ViewDocument1(PK_ID, T_ID, R_ID) {
            document.getElementById("<%=txt_Reject.ClientID%>").value = '';
            document.getElementById("<%=hdfPKID.ClientID%>").value = PK_ID;
            document.getElementById("<%=hdfTraining.ClientID%>").value = T_ID;
            document.getElementById("<%=hdnRequestType.ClientID%>").value = R_ID;
            var btnVD1 = document.getElementById("<%=btnViewDocumentDetails1.ClientID %>");
            btnVD1.click();
        }
    </script>
    <script>
        function WithdrawHide() {
            $("#div_Userroles11").hide();
            $("#div1").hide();
            $("#div_Userroles21").hide();
            $("#div_Userroles31").hide();
            $("#approverDivGV").hide();
            $("#reviewerdiv").hide();
        }
        function NonWithdrawShow() {
            $("#div_Userroles11").show();
            $("#div1").show();
            $("#div_Userroles21").show();
            $("#div_Userroles31").show();
        }
        function ReasonCancel() {
            document.getElementById("<%=txt_Reject.ClientID%>").value = "";
        }
    </script>
    <script>
        function openElectronicSignModal() {
            openUC_ElectronicSign();
        }
        function ReloadDocumentApprover() {
            window.open("<%=ResolveUrl("~/DMS/DocumentApprover/Initiated_Documents.aspx")%>", "_self");
        }
        function ClosemyModal_docDetails() {
            $("#myModal_docDetails").modal('hide');
        }
    </script>
    <script type="text/javascript">
        function commentRequried() {
            var comment = document.getElementById("<%=txt_Reject.ClientID%>").value;
            errors = [];
            if (comment.trim() == "") {
                errors.push("Enter comments.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <script type="text/javascript">
        function commentRequriedApprove() {
            var comment = document.getElementById("<%=txt_Reject.ClientID%>").value;
            var training = document.getElementById("<%=hdfTraining.ClientID%>").value;
            var ReqType = document.getElementById("<%=hdnRequestType.ClientID%>").value;
             errors = [];
             if (comment.trim() == "") {
                 errors.push("Enter comments.");
             }
             if (ReqType == 3) {
                 if (training == false) {
                     errors.push("Training's going on for this document.")
                 }
             }             
             if (errors.length > 0) {
                 custAlertMsg(errors.join("<br/>"), "error");
                 return false;
             }
             else {
                 return true;
             }
        }
    </script>
    <script>
         function ViewCommentHistory(PK_ID) {
             document.getElementById("<%=hdfPKID.ClientID%>").value = PK_ID;
             var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
             btnCV.click();
         }
    </script>
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
    <script>
         function showImg() {
             ShowPleaseWait('show');
         }
         function hideImg() {
             ShowPleaseWait('hide');
         }
    </script>
</asp:Content>
