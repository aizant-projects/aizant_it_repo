﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectVMSRole.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.Admin.SelectVMSRole" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
           <%-- <asp:UpdatePanel runat="server" ID="upddlVMSAccessableRoles" UpdateMode="Conditional">
                <ContentTemplate>--%>
                    <div class="form-group col-sm-4  padding-none  padding-none" runat="server" >
                        <label class="control-label col-sm-10" id="lblAccessableRoles">Accessable Roles</label>
                        <div class="col-sm-12">
                            <asp:DropDownList ID="ddlVMSAccessableRoles" runat="server" CssClass="col-md-12 col-lg-12 col-xs-12 col-sm-12 selectpicker drop_down padding-none"></asp:DropDownList>
                        </div>
                        <div class="col-lg-12 padding-none">
                            <div class="float-right top padding-none">
                                <asp:Button ID="btnOk" Text="OK" CssClass="btn btn-approve_popup" runat="server" OnClick="btnOk_Click" />
                                <asp:Button ID="btnCancel" Text="Cancel" CssClass="btn btn-canceldms_popup" runat="server" />
                            </div>
                        </div>
                    </div>
            <%--    </ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
    </form>
</body>
</html>
