﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="TTS_CreationList.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TargetTrainingSchedule.TTS_CreationList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <asp:LinkButton ID="lbTTSList" class="float-right btn-signup_popup" OnClick="lbTTSList_Click" runat="server">Create Target Training Schedule</asp:LinkButton>
       <asp:HiddenField ID="hdnEmpID" runat="server" Value="0"/>
       <asp:HiddenField ID="hdnDeptID" runat="server" Value="0"/>
       <asp:HiddenField ID="hdnDepartmentName" runat="server" Value="0"/>
       <asp:HiddenField ID="hdnDocumentID" runat="server" Value="0"/>
       <asp:HiddenField ID="hdnDocument" runat="server" Value="0"/>
       <asp:HiddenField ID="hdnDocumentTypeID" runat="server" Value="0"/>
       <asp:HiddenField ID="hdnDocType" runat="server" Value="0"/>
       <asp:HiddenField ID="hdnTargetType" runat="server" Value="0"/>
       <asp:HiddenField ID="hdnTargetTypeID" runat="server" Value="0"/>
     
     <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
       <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="float-left col-lg-12 padding-none" id="Div3" runat="server">     
               <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 "><asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Create Target Training Schedule"></asp:Label></div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none" style="overflow-y: auto; margin-top: 10px">
                    <table id="tblTTS_CreationList" class="datatable_cust tblTTS_CreationListClass display breakword" style="width: 100%">
                        <thead>
                            <tr>
                                <th>DeptID</th>
                                <th>Department</th>
                                <th>DocumentID</th>
                                <th>Document</th>
                                <th>DocumentTypeID</th>
                                <th>Document Type</th>
                                <th>Target Type</th>
                                <th>TargetTypeID</th>
                                <th>Create</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

 <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnViewTTS_CreationlList" OnClick="btnViewTTS_CreationlList_Click" runat="server" Text="Button" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script>

        $('#tblTTS_CreationList').on('click', 'tbody .BtnView', function () {
            var data_row = table.row($(this).parents('tr')).data(); // here is the change

            $("#<%=hdnDeptID.ClientID%>").val(data_row.DeptID);
            $("#<%=hdnDepartmentName.ClientID%>").val(data_row.DepartmentName);
            $("#<%=hdnDocumentID.ClientID%>").val(data_row.DocumentID);
            $("#<%=hdnDocument.ClientID%>").val(data_row.DocumentName);
            $("#<%=hdnDocumentTypeID.ClientID%>").val(data_row.DocumentTypeID);
            $("#<%=hdnDocType.ClientID%>").val(data_row.DocumentType);
            $("#<%=hdnTargetType.ClientID%>").val(data_row.TargetType);
            $("#<%=hdnTargetTypeID.ClientID%>").val(data_row.TargetTypeID);
            $("#<%=btnViewTTS_CreationlList.ClientID%>").click();

        });
    </script>

    <!--TTS_CreationlList jQuery DataTable-->
    <script>
        function getUrlVars() {
            var vars = [], hash;
            var url = window.location.href;
            var hashes = url.slice(url.indexOf('?') + 1).split('&');
            if (url.indexOf("#") > 0) {
                var hashes = url.slice(url.indexOf('?') + 1, url.indexOf('#')).split('&');
            }
           
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

       

        var table = $('#tblTTS_CreationList').DataTable({
            columns: [
                { 'data': 'DeptID' },
                { 'data': 'DepartmentName' },
                { 'data': 'DocumentID' },
                { 'data': 'DocumentName' },
                { 'data': 'DocumentTypeID' },
                { 'data': 'DocumentType' },
                { 'data': 'TargetType' },
                { 'data': 'TargetTypeID' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="Create_tms BtnView" title="Create" href="#"></a>'; }
                }
            ],
            "order": [2, "desc"],
            "aoColumnDefs": [{ "targets": [0,2,4,7], "visible": false }, { className: "textAlignLeft", "targets": [1,3,5,6] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/TTS_CreationList")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "FilterType", "value": getUrlVars()["FilterType"]});
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
       
    </script>
   
</asp:Content>
