﻿<%@ Page Title="Initialize ATC" Async="true" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="InitializeATC_List.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Administration.InitializeATC_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnATC_ID" runat="server" />
            <asp:HiddenField ID="hdnDeptName" runat="server" Value="false" />
            <asp:HiddenField ID="hdnCalendarYear" runat="server" Value="false" />
            <asp:HiddenField ID="hdnAuthor" runat="server" Value="false" />
            <asp:HiddenField ID="hdnReviewer" runat="server" Value="false" />
            <asp:HiddenField ID="hdnApprover" runat="server" Value="false" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none" id="divMainContainer" runat="server">
            <div id="accordion1" class="accordion">
                <div class="card">
                    <div class="card-header grid_header" href="#collapseFG" data-toggle="collapse" data-parent="#accordion1">                      
                        <a class="card-title">Initiate Annual Training calendar
                                </a>
                    </div>
                    <div id="collapseFG" class="collapse show" aria-labelledby="collapseFG" data-parent="#accordion1">
                        <div class="card-body ">

                            <asp:UpdatePanel ID="UpInitialiseATC" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none top ">
                                        <div class="form-group float-left col-xl-4 col-lg-4 col-sm-6 col-12 col-md-4">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Department<span class="smallred_label">*</span></label>
                                                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control"
                                                    data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group float-left col-xl-4 col-lg-4 col-sm-6 col-12 col-md-4">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Year<span class="smallred_label">*</span></label>
                                                <asp:DropDownList ID="ddlYear" CssClass="form-control SearchDropDown selectpicker regulatory_dropdown_style float-left col-lg-12 col-sm-12 col-12 col-md-12" data-live-search="true" AutoPostBack="true" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group float-left col-xl-4 col-lg-4 col-sm-6 col-12 col-md-4">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Training Calendar Author<span class="smallred_label">*</span></label>
                                                <asp:DropDownList ID="ddlATC_Author" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control"
                                                    data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlATC_Author_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group float-left col-xl-4 col-lg-4 col-sm-6 col-12 col-md-4">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Approval Training Coordinator<span class="smallred_label">*</span></label>
                                                <asp:DropDownList ID="ddlApprovalHod" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control"
                                                    data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlApprovalHod_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group float-left col-xl-4 col-lg-4 col-sm-6 col-12 col-md-4">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Approval QA<span class="smallred_label">*</span></label>
                                                <asp:DropDownList ID="ddlApprovalQA" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control"
                                                    data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlApprovalQA_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  padding-none">
                                            <div class="form-group float-left col-lg-12 padding-none">
                                                <label for="inputPassword3" class=" col-sm-12 control-label  custom_label_answer">Remarks<span runat="server" visible="false" id="spanRemarks" class="smallred_label">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 ">
                                <div class="form-group float-right">
                                    <asp:UpdatePanel ID="Upbuttons" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <asp:Button ID="btnInitialize" runat="server" CssClass=" btn-signup_popup" Text="Initialize" OnClick="btnInitialize_Click" OnClientClick="javascript:return InitializeRequired();" />
                                            <asp:Button ID="btnUpdateATC" runat="server" CssClass="btn-signup_popup " Text="Update" Visible="false" OnClick="btnUpdateATC_Click" OnClientClick="javascript:return UpdateRequired();" />
                                            <asp:Button ID="btnCancel" runat="server" CssClass=" btn-revert_popup" Text="Reset" OnClick="btnCancel_Click" />

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="card">
                    
                     <div class="card-header grid_header" href="#collapseFG1" data-toggle="collapse" data-parent="#accordion1">
                      
                                <a class="card-title">Initialized Calendar List
                                </a>
                          </div>
                    <div id="div2" runat="server" visible="true">


                        <div id="collapseFG1" class="collapse show" aria-labelledby="collapseFG1" data-parent="#accordion1">
                            <div class="card-body " id="Div3" runat="server">

                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom top padding-none" style="overflow: auto;">
                                    <table id="tblInitializeATC_List" class="datatable_cust tblInitializeATC_ListClass display breakword" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>InitializeATC_ID</th>
                                                <th>DeptID</th>
                                                <th>DeptCode</th>
                                                <th>Department</th>
                                                <th>Calendar Year</th>
                                                <th>Initiated By</th>
                                                <th>Initiated Date</th>
                                                <th>ATC_Author_ID</th>
                                                <th>Author</th>
                                                <th>ATC_HOD_Approval_ID</th>
                                                <th>Reviewer</th>
                                                <th>ATC_QA_Approval_ID</th>
                                                <th>Approver</th>
                                                <th>Current Status</th>
                                                <th>Edit</th>
                                                <th>History</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------- Modal For Viewing the History of FAQuestion------------->
    <div id="modelATC_History" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="min-width: 85%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                     <h4 class="modal-title">
                        <asp:Label ID="lblmodelATC_History" runat="server" Text="ATC History"></asp:Label>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" title="Close">&times;</button>
                   
                </div>
                <div class="modal-body">
                    <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <table id="tblATC_History" class="datatable_cust tblATC_HistoryClass display breakword breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>ATC_HistoryID</th>
                                    <th>Action</th>
                                    <th>Action By</th>
                                    <th>Role</th>
                                    <th>Action Date</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-------- End FAQ History-------------->
    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnEditInitializeATC" OnClick="btnEditInitializeATC_Click" runat="server" Text="Button" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script>
        function EditInitializeATC(ATC_ID, DeptName, CalendarYear, Author, Reviewer, Approver) {
            $("#<%=hdnATC_ID.ClientID%>").val(ATC_ID);
            $("#<%=hdnDeptName.ClientID%>").val(DeptName);
            $("#<%=hdnCalendarYear.ClientID%>").val(CalendarYear);
            $("#<%=hdnAuthor.ClientID%>").val(Author);
            $("#<%=hdnReviewer.ClientID%>").val(Reviewer);
            $("#<%=hdnApprover.ClientID%>").val(Approver);
            $("#<%=txtRemarks.ClientID%>").val('');
            $("#<%=btnEditInitializeATC.ClientID%>").click();
        }
        function ViewATC_History(ATC_ID) {
            $("#<%=hdnATC_ID.ClientID%>").val(ATC_ID);
            $('#modelATC_History').modal({ show: true, backdrop: 'static', keyboard: false });
        }
        $('#modelATC_History').on('shown.bs.modal', function (e) {
            loadATC_HistoryList();
        });
    </script>

    <script>
        var oTblATC_History;
        function loadATC_HistoryList() {
            var ATC_ID = $('#<%=hdnATC_ID.ClientID%>').val();
            if (ATC_ID != "0") {
                if (oTblATC_History != null) {
                    oTblATC_History.destroy();
                }
                oTblATC_History = $('#tblATC_History').DataTable({
                    columns: [
                        { 'data': 'ATC_HistoryID' },
                        { 'data': 'Action' },
                        { 'data': 'Action_By' },
                        { 'data': 'Role' },
                        { 'data': 'ActionDate' },
                        { 'data': 'Remarks' }
                    ],
                    "bAutoWidth": false,
                    "order": [[4, "desc"]],
                    "scrollY": "374px",
                    "aoColumnDefs": [{ "targets": [0], "visible": false }, { className: "textAlignLeft", "targets": [1, 2, 3, 5] }],
                    "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/InitializeATC_ListService.asmx/ATC_HistoryList")%>",
                      "fnServerData": function (sSource, aoData, fnCallback) {
                          aoData.push({ "name": "ATC_ID", "value": ATC_ID });
                          $.ajax({
                              "dataType": 'json',
                              "contentType": "application/json; charset=utf-8",
                              "type": "GET",
                              "url": sSource,
                              "data": aoData,
                              "success": function (msg) {
                                  var json = jQuery.parseJSON(msg.d);
                                  fnCallback(json);
                                  UserSessionCheck();
                                  oTblATC_History.columns.adjust();

                              },
                              error: function (xhr, textStatus, error) {
                                  if (typeof console == "object") {
                                      console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                  }
                              }
                          });
                      },
                      "fnInitComplete": function () {

                      }
                });
            }
            else {
                oTblATC_History = $('#tblATC_History').DataTable();
            }
        }

    </script>
    <!--InitialiseATC_ List jQuery DataTable-->
    <script>      
        var oTblInitializeATC;
        $(function () {
            loadInitializeATC_List();
        });

        function loadInitializeATC_List() {
            var empID = $('#<%=hdnEmpID.ClientID%>').val();
            if (empID != "0") {
                if (oTblInitializeATC != null) {
                    oTblInitializeATC.destroy();
                }
                oTblInitializeATC = $('#tblInitializeATC_List').DataTable({
                    columns: [
                        { 'data': 'ATC_ID' },
                        { 'data': 'DeptID' },
                        { 'data': 'DeptCode' },
                        { 'data': 'DeptName' },
                        { 'data': 'CalendarYear' },
                        { 'data': 'InitiatedBy' },
                        { 'data': 'InitiatedDate' },
                        { 'data': 'ATC_Author_ID' },
                        { 'data': 'Author' },
                        { 'data': 'ATC_HOD_Approval_ID' },
                        { 'data': 'Reviewer' },
                        { 'data': 'ATC_QA_Approval_ID' },
                        { 'data': 'Approver' },
                        { 'data': 'CurrentStatus' },
                        {
                            "mData": null,
                            "bSortable": false,
                            "mRender": function (o) {
                                if (o.CurrentStatus == "7") {
                                    //return 'Completed'
                                    return 'N/A'
                                }
                                else {
                                    return '<a class="Edit"  href="#" onclick="EditInitializeATC(' + o.ATC_ID + ',' + o.DeptID + ',' + o.CalendarYear + ',' + o.ATC_Author_ID + ',' + o.ATC_HOD_Approval_ID + ',' + o.ATC_QA_Approval_ID + ')"></a>';
                                }
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "mRender": function (o) {
                                return '<a class="summary_latest" data-toggle="modal" data-target="#modelATC_History" href="#" onclick="ViewATC_History(' + o.ATC_ID + ')"></a>';
                            }
                        }
                    ],
                    "aoColumnDefs": [{ "targets": [0, 1, 2, 7, 9, 11, 13], "visible": false }, { className: "textAlignLeft", "targets": [3, 5, 8, 9, 10, 12] }],
                    "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/InitializeATC_ListService.asmx/Initialize_ATC")%>",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({ "name": "EmpID", "value": empID });
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                UserSessionCheck();
                                $("#tblInitializeATC_List").show();

                            },
                            error: function (xhr, textStatus, error) {
                                if (typeof console == "object") {
                                    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                }
                            }
                        });
                    }
                });
            }
            else {
                oTblInitializeATC = $('#tblInitializeATC_List').DataTable();
            }
        }


        function showDetails() {
            //so something funky with the data

        }
        function ReloadI_ATC_JQ_Dtable() {
            loadInitializeATC_List();
        }
    </script>

    <script>
        function CustomAlertmsginIATC(msg, msgType) {
            if (msgType == "success") {
                document.getElementById('<%=txtRemarks.ClientID%>').value = "";
                ReloadI_ATC_JQ_Dtable();
            }
            custAlertMsg(msg, msgType);
        }
        function UpdateRequired() {
            var Remarks = document.getElementById("<%=txtRemarks.ClientID%>").value;
            errors = [];
            if (document.getElementById('<%=ddlATC_Author.ClientID%>').value == 0) {
                errors.push("Select Author");
            }
            if (document.getElementById('<%=ddlApprovalHod.ClientID%>').value == 0) {
                errors.push("Select Approval Training Coordinator");
            }
            if (document.getElementById('<%=ddlApprovalQA.ClientID%>').value == 0) {
                errors.push("Select Approval QA");
            }
            if (Remarks.trim() == "") {
                errors.push("Specify Purpose of Modification in Remarks");
            }
            if (errors.length > 0) {
                //custAlertMsg(errors.join("<br/>"), "error");
                custAlertMsg(errors.join("<br/>"), "error", "FocusingControlInitializeRequired('Update')");
                return false;
            }
            else {
                ShowPleaseWait('show');
                return true;
            }
        }
        function FocusingControlInitializeRequired(ActionFor) {
            var remarks = document.getElementById("<%=txtRemarks.ClientID%>").value;
            if (document.getElementById('<%=ddlDepartment.ClientID%>').value == 0) {
                document.getElementById("<%=ddlDepartment.ClientID%>").focus();
            }
            else if (document.getElementById('<%=ddlATC_Author.ClientID%>').value == 0) {
                document.getElementById("<%=ddlATC_Author.ClientID%>").focus();
            }
            else if (document.getElementById('<%=ddlApprovalHod.ClientID%>').value == 0) {
                document.getElementById("<%=ddlApprovalHod.ClientID%>").focus();
            }
            else if (document.getElementById('<%=ddlApprovalQA.ClientID%>').value == 0) {
                document.getElementById("<%=ddlApprovalQA.ClientID%>").focus();
            }
            else if (ActionFor == "Update") {
                if (remarks.trim() == "") {
                    document.getElementById("<%=txtRemarks.ClientID%>").focus();
                }
            }
        }
        function InitializeRequired() {
            errors = [];
            if (document.getElementById('<%=ddlDepartment.ClientID%>').value == 0) {
                errors.push("Select Department");
            }
            if (document.getElementById('<%=ddlATC_Author.ClientID%>').value == 0) {
                errors.push("Select Author");
            }
            if (document.getElementById('<%=ddlApprovalHod.ClientID%>').value == 0) {
                errors.push("Select Approval Training Coordinator");
            }
            if (document.getElementById('<%=ddlApprovalQA.ClientID%>').value == 0) {
                errors.push("Select Approval QA");
            }

            if (errors.length > 0) {
                //custAlertMsg(errors.join("<br/>"), "error");
                custAlertMsg(errors.join("<br/>"), "error", "FocusingControlInitializeRequired('Create')");
                return false;
            }
            else {
                ShowPleaseWait('show');
                return true;
            }
        }
        function ExpandDiv() {
            $('#collapseFG').addClass('panel-collapse collapse in').removeClass('panel-collapse collapse');
        }
    </script>
    <!--JR_Action  History jQuery DataTable-->

</asp:Content>
