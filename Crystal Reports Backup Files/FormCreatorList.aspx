﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="FormCreatorList.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.FormCreatorList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left padding-none">
        <div class=" col-md-12 col-lg-12 col-12 col-sm-12 dms_outer_border float-left padding-none">
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left" id="divMainContainer" runat="server" visible="true">
                <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-right ">
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-md-12 col-lg-12 col-12 col-sm-12 float-left">Reverted Form List</div>
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                           <div class="col-12 float-left top padding-none bottom" >
                            <table id="dtDocList" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>VersionID</th>
                                        <th>Form Number</th>
                                        <th>Form Name</th>
                                        <th>Form Description</th>
                                        <th>Version</th>
                                        <th>Department</th>
                                        <th>RefDocNumber</th>
                                        <th>Creator</th>
                                        <th>Approver</th>
                                        <th>Action</th>
                                        <th>Request Type</th>
                                        <th>Edit</th>
                                        <th>View Form</th>
                                        <th>History</th>
                                    </tr>
                                </thead>
                            </table>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-------- Form VIEW-------------->
    <div id="myFormView" class="modal department fade" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg" style="min-width: 90%; height: 500px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">                    
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpHeading">
                        <ContentTemplate>
                            <span id="span1" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="dviframe" runat="server">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!--------End Form VIEW-------------->

    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="min-width: 85%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">                            
                            <span id="span4" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>                        
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User"  ItemStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("ActionBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("ActionDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role"  ItemStyle-Width="8%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action"  ItemStyle-Width="17%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments"  ItemStyle-Width="37%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                <asp:TextBox ID="txt_Comments" runat="server" ReadOnly="true" Rows="2" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM">Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL">Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned To">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->

    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRequestType" runat="server" Value="0" />
    <asp:UpdatePanel ID="UpGridButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnFormView" runat="server" Text="Submit" Style="display: none" OnClick="btnFormView_Click" OnClientClick="showImg();"/>
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" OnClientClick="showImg();"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        $('#dtDocList thead tr').clone(true).appendTo('#dtDocList thead');
        $('#dtDocList thead tr:eq(1) th').each(function (i) {
            if (i < 12) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (oTable.column(i).search() !== this.value) {
                        oTable
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDocList').wrap('<div class="dataTables_scroll" />');
        var oTable = $('#dtDocList').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'FormVersionID' },
                { 'data': 'FormNumber' },
                { 'data': 'FormName' },
                { 'data': 'Purpose' },
                { 'data': 'VersionNumber' },
                { 'data': 'dept' },
                { 'data': 'documentNUmber' },
                { 'data': 'CreatedBy' },
                { 'data': 'oldApprovedBY' },
                { 'data': 'ActionName' },
                { 'data': 'FormRequestType' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="Edit" title="Edit" href=/DMS/DMSAdmin/Form_Creation.aspx?FormVersionID=' + o.FormVersionID + '&from=c>' + '' + '</a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="view" title="View" href="#" onclick=ViewForm(' + o.FormVersionID + ')></a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History" href="#" onclick=ViewCommentHistory(' + o.FormVersionID + ')></a>'; }
                },

            ],
            "responsive": true,
            "paging": true,
            "search": {
                "regex": true
            },
            "aoColumnDefs": [{ "targets": [0,1, 4, 7], "visible": false, "searchable": false }/*, { "bSortable": false, "aTargets": [0] }*/, { "className": "dt-body-left", "targets": [3, 4, 6, 8, 9, 10, 11] },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetFormCreationList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "ReqTypeID", "value": <%=hdnRequestType.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    </script>
    <script>
        function ViewForm(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnRV = document.getElementById("<%=btnFormView.ClientID %>");
            btnRV.click();
        }
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
        function CloseBrowser() {
            window.close();
        }
    </script>
    <script>
        function showImg() {
            ShowPleaseWait('show');
        }
        function hideImg() {
            ShowPleaseWait('hide');
        }
    </script>
</asp:Content>
