﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocPrintView.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DMSFileView.DocPrintView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <style>
        .dxreView {
            height: 599px !important;
            /*width: 150% !important;*/
        }
        .dxreControlSys {
            height:auto !important;
        }
        .dxrePage {
            height: 1056px !important;
        }
        .dxreControl .dxreBar {
    background-color: #F2F2F2;
    border-top: 1px solid #a7a7a7;
     height:auto !important;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <link href="<%=ResolveUrl("~/Content/bootstrap.min.css")%>" rel="stylesheet" />
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.10.2.min.js")%>"></script>
    <script src="<%=ResolveUrl("~/Scripts/bootstrap.min.js")%>"></script>
    <link href="<%=ResolveUrl("~/AppCSS/DMS/style.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/AppCSS/DMS/DMS_StyleSheet1.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/AppCSS/Main_style.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/AppCSS/Editor/site.css")%>" rel="stylesheet" />
            <link href="<%=ResolveUrl("~/AppCSS/CustAlerts/custAlert.css")%>" rel="stylesheet" />
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
         <div id="divliteral" runat="server" visible="false">
            <asp:Literal ID="literalSop1" runat="server"></asp:Literal>
        </div>
        <div id="richedit2" runat="server" visible="false">
            <dx:ASPxRichEdit ID="ASPxRichEdit2"
                ShowConfirmOnLosingChanges="false" runat="server" Settings-HorizontalRuler-Visibility="Hidden" Settings-DocumentCapabilities-Bookmarks="Disabled"
                Settings-Bookmarks-Visibility="Hidden" StylesRibbon-GroupPopup-Wrap="False" StylesPopupMenu-Item-Cursor="wait" StylesPopupMenu-SubMenuItem-Wrap="False"
                WorkDirectory="~\App_Data\WorkDirectory" ReadOnly="true" Width="100%" RibbonMode="None">
                <Settings>
                    <Behavior Copy="Disabled" Download="Disabled" Cut="Disabled" Paste="Disabled" Printing="Disabled" AcceptsTab="false" Save="Disabled" FullScreen="Enabled" SaveAs="Disabled" CreateNew="Disabled" />
                </Settings>
                <SettingsDocumentSelector EditingSettings-AllowCopy="false"></SettingsDocumentSelector>
            </dx:ASPxRichEdit>
        </div>

        <!---Error popup--->
        <div id="ModalError" class="modal error_popup" role="dialog" style="">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="error_icon pull-left">
                            <img src="<%=ResolveUrl("~/Images/CustomAlerts/error.png")%>" class="pull-left" style="width: 35px" alt="Error" />
                            <h4 class="modal-title pull-left popup_title">Error</h4>
                        </div>
                    </div>
                    <div class="modal-body bottom" style="padding: 12px 0px">
                        <div class="col-lg-12 padding-none" style="padding: 10px; border-bottom: 1px solid #f95a5a;">
                            <asp:UpdatePanel ID="upError" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <p id="msgError" class="col-lg-12" style="padding: 10px 22px; max-height: 300px; overflow-x: auto; font-family: arial; color: #f95a5a; font-weight: bold;" runat="server"></p>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" id="btnErrorOk" class="btn btn-cancel_popup" style="margin-top: 10px !important" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Error Popup-->

        <!-- Success content-->
        <div id="ModalSuccess" class="modal success_popup" role="dialog" style="z-index: 9999">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="success_icon pull-left">
                            <img src="<%=ResolveUrl("~/Images/CustomAlerts/success.png")%>" class="pull-left" style="width: 35px" alt="Success" />
                            <h4 class="modal-title popup_title pull-left">Success</h4>
                        </div>
                    </div>
                    <div class="modal-body" style="padding: 11px 0px;">
                       
                        <asp:UpdatePanel ID="Upsucsess" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                        <div class="col-lg-12 padding-none" style="padding: 10px; border-bottom: 1px solid #3ab16a;">
                            <p id="msgSuccess" class="col-lg-12" style="padding: 0px 22px; max-height: 128px; overflow-x: auto;" runat="server"></p>
                        </div>
                                </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btnSuccessOk" class="btn-success_popup" data-dismiss="modal" style="margin-top: 10px !important" runat="server">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Success content-->
        <asp:HiddenField ID="hdfRID" runat="server" />
    </form>
</body>
</html>
