﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VisitorRegistrationList.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VisitorRegister.VisitorRegistrationList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>




<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">

     <script type="text/javascript">
         function Openpopup(popurl) {
             winpops = window.open(popurl, "", "height=600,width=1000,status=yes,location=no,toolbar=no,menubar=no,scrollbars=yes,target=_parent")
         }
    </script>
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }
    </style>
    
    <style>
        .select2-drop-active {
            position: absolute;
            z-index: 1050;
        }
    </style>
    <asp:HiddenField ID="hdnCheckout" runat="server" />
   

    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 1200px; margin-left: 6%">

            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Visitor's List</span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>

            <div class="Row">
                <div class="col-sm-6">

                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div20">
                                    <label class="control-label col-sm-4" id="lblDepartmentsearch" style="text-align: left; font-weight: lighter">Select Department</label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlDepatmentSearchVisitors" CssClass="form-control SearchDropDown" runat="server" AutoPostBack="true" Visible="false"></asp:DropDownList>
                                        <asp:TextBox ID="txtSearchDepartment" runat="server" CssClass="form-control" placeholder="Enter Department" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div29">
                            <label class="control-label col-sm-4" id="lblVisitorName" style="text-align: left; font-weight: lighter">Visitor Name</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtSearchVisitorsName" runat="server" CssClass="form-control" placeholder="Enter Visitor Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" MaxLength="50"></asp:TextBox>
                                <asp:AutoCompleteExtender ServiceMethod="GetVisitorNameList" ServicePath="~/VMS/VisitorRegister/VisitorRegistration.aspx" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                    TargetControlID="txtSearchVisitorsName" ID="AutoCompleteExtender5" UseContextKey="true" runat="server" FirstRowSelected="false">
                                </asp:AutoCompleteExtender>
                            </div>
                        </div>
                    </div>

                    <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div30">
                                    <label class="control-label col-sm-4" id="lblPurposeofvisit" style="text-align: left; font-weight: lighter">Purpose of Visit</label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlSearchVisitorsPurposeofVisit" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div31">
                            <label class="control-label col-sm-4" id="lblFromDate" style="text-align: left; font-weight: lighter">From Date</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtSearchVisitorsFromDate" CssClass="form-control" runat="server" placeholder="Select From Date"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div32">
                            <label class="control-label col-sm-4" id="lblMobile" style="text-align: left; font-weight: lighter">Visitor Mobile No</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtSearchVisitorsMobileNo" runat="server" CssClass="form-control" placeholder="Enter Mobile No" MaxLength="10" onkeypress="return ValidatePhoneNo(event);"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Mobile No Should be 10 Digits" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtSearchVisitorsMobileNo" ValidationGroup="Visitorlist" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                <asp:AutoCompleteExtender ServiceMethod="GetVisitorListByMobile" ServicePath="~/VMS/VisitorRegister/VisitorRegistration.aspx" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                    TargetControlID="txtSearchVisitorsMobileNo" ID="AutoCompleteExtender4" UseContextKey="true" runat="server" FirstRowSelected="false">
                                </asp:AutoCompleteExtender>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div33">
                            <label class="control-label col-sm-4" id="lblVisitorIDSearch" style="text-align: left; font-weight: lighter">Visitor ID</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtSearchVisitorsID" runat="server" CssClass="form-control" placeholder="Enter Visitor ID"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div34">
                            <label class="control-label col-sm-4" id="labelOrganizationName" style="text-align: left; font-weight: lighter">Organization</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtSearchVisitorsOrganizationName" runat="server" CssClass="form-control" placeholder="Enter Organization"></asp:TextBox>
                                <asp:AutoCompleteExtender ServiceMethod="GetVisitorListByOrganization" ServicePath="~/VMS/VisitorRegister/VisitorRegistration.aspx" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                    TargetControlID="txtSearchVisitorsOrganizationName" ID="AutoCompleteExtender6" UseContextKey="true" runat="server" FirstRowSelected="false">
                                </asp:AutoCompleteExtender>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div35">
                            <label class="control-label col-sm-4" id="lblToDate" style="text-align: left; font-weight: lighter">To Date</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtSearchVisitorsToDate" runat="server" CssClass="form-control" placeholder="Select To Date"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                <asp:Button ID="btnSearchVisitorsFind" Text="Find" CssClass="button" runat="server" OnClick="btnSearchVisitorsFind_Click" ValidationGroup="Visitorlist" OnClientClick="return Submitvalidate()" />
                <asp:Button ID="btnSearchVisitorsReset" Text="Reset" CssClass="button" runat="server" OnClick="btnSearchVisitorsReset_Click" CausesValidation="false" />
            </div>

          
            <div class="clearfix">
            </div>

            <asp:GridView ID="VisitorGridView" PagerStyle-CssClass="GridPager" runat="server" CssClass="table table-bordered table-inverse" Style="text-align: center" HorizontalAlign="Center" DataKeyNames="SNo" Font-Size="11px"
                HeaderStyle-Wrap="false" AutoGenerateColumns="False" HeaderStyle-Width="2%" EmptyDataText="Currently No Records Found" CellPadding="3" HeaderStyle-HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" AllowPaging="True" ForeColor="#66CCFF" OnPageIndexChanging="VisitorGridView_PageIndexChanging" OnRowDataBound="VisitorGridView_RowDataBound">
                <EmptyDataRowStyle ForeColor="#FF3300" HorizontalAlign="Center" />
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#748CB2" Font-Bold="True" ForeColor="White" Height="30px" />
                <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="White" />
                <PagerSettings Mode="NextPreviousFirstLast" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#0c99f0" />
                <SortedAscendingHeaderStyle BackColor="#0c99f0" />
                <SortedDescendingCellStyle BackColor="#0c99f0" />
                <SortedDescendingHeaderStyle BackColor="#0c99f0" />
                <Columns>
                    <asp:TemplateField HeaderText="SNO" SortExpression="SNo">
                        <ItemTemplate>
                            <asp:Label ID="lblGSno" runat="server" Text='<%# Bind("SNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="VisitorID" HeaderText="VISITOR ID" SortExpression="VisitorID" />
                    <asp:TemplateField HeaderText="VISITOR NAME" SortExpression="VisitorName">
                        <ItemTemplate>
                            <asp:Label ID="lblGVisitorName" runat="server" Text='<%#Eval("VisitorName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="MobileNo" HeaderText="MOBILE NO" SortExpression="MobileNo" />
                    <asp:BoundField DataField="DeptName" HeaderText="DEPARTMENT" SortExpression="DeptName" />
                    <asp:BoundField DataField="EmpName" HeaderText="WHOM TO VISIT" SortExpression="EmpName" />
                    <asp:BoundField DataField="PurposeofVisit" HeaderText="PURPOSE OF VISIT" SortExpression="PurposeofVisit" />
                    <asp:BoundField DataField="Organization" HeaderText="ORGANISATION NAME" SortExpression="Organization" />
                    <asp:BoundField DataField="InDateTime" HeaderText="IN DATETIME" SortExpression="InDateTime" />
                    <asp:TemplateField HeaderText="OUT DATE TIME" SortExpression="OutDateTime">
                        <ItemTemplate>
                            <asp:Label ID="lblGVVisitorOutDateTime" runat="server" Text='<%# Bind("OutDateTime") %>'></asp:Label>
                            <asp:LinkButton ID="lnkCheckOutVisitor" runat="server" OnClick="lnkCheckOutVisitor_Click" ForeColor="#ff0000" Visible="false">CheckOut</asp:LinkButton>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PHOTO" SortExpression="PhotoFileData" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVPhoto" runat="server" Text='<%# Bind("PhotoFileData") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPhoto" runat="server" Text='<%#Eval("PhotoFileData")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PRINT">
                        <ItemTemplate>
                            <a href='javascript:Openpopup("<%= ResolveUrl("~/VMS/DevReports/frmDocumentViewer.aspx") + "?VisitorPhoto="  %>"+ "<%# Eval("SNo") %>")'>
                               
                                <asp:Label ID="Label13" runat="server" Style="font-size: 95%" CssClass="glyphicon glyphicon-print"></asp:Label></a>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="VIEW">
                        <ItemTemplate>
                            <asp:LinkButton ID="LnkEditVisitor" runat="server" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" OnClick="LnkEditVisitor_Click"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            
            <asp:ModalPopupExtender ID="ModalPopupVisitor" runat="server" TargetControlID="lbtPop" PopupControlID="panelVisitor" RepositionMode="RepositionOnWindowResizeAndScroll" DropShadow="true" PopupDragHandleControlID="panelAddNewTitle" BackgroundCssClass="modalBackground"></asp:ModalPopupExtender>
            <asp:Panel ID="panelVisitor" runat="server" Style="display: none; background-color: ghostwhite;" ForeColor="Black" Width="1400" Height="700">
                <asp:Panel ID="panelv" runat="server" Style="cursor: move; font-family: Tahoma; padding: 8px;" HorizontalAlign="Center" BorderColor="#000000" Font-Bold="true" BackColor="#748CB2" ForeColor="White" Height="35">
                    <b id="pttlModifyVisitor" runat="server"></b>
                </asp:Panel>
                <asp:LinkButton ID="lbtPop" runat="server"></asp:LinkButton>
                <div class="Row">
                    <div class="col-sm-4">
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div26">
                                <label class="control-label col-sm-4" id="lblSno" style="text-align: left; font-weight: lighter">SNo</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtVEditSNo" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div5">
                                <label class="control-label col-sm-4" id="lblVisitorID" style="text-align: left; font-weight: lighter">Visitor ID</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtVisitorID" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div1">
                                        <label class="control-label col-sm-4" id="lblFirstName" style="text-align: left; font-weight: lighter">First Name<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtVisitorName" runat="server" CssClass="form-control" placeholder="Enter First Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" MaxLength="50"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter First Name" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtVisitorName" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                          
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="txtVisitorName" EventName="TextChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div2">
                                <label class="control-label col-sm-4" id="lblLastName" style="text-align: left; font-weight: lighter">Last Name<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" placeholder="Enter Last Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Last Name" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtLastName" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div3">
                                        <label class="control-label col-sm-4" id="lblMobileNo" style="text-align: left; font-weight: lighter">Mobile<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtMobNo" runat="server" CssClass="form-control" placeholder="Enter Mobile" MaxLength="10" onkeypress="return ValidatePhoneNo(event);"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter Mobile No" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtMobNo" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorMobile" runat="server" ErrorMessage="Mobile No Should be 10 Digits" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtMobNo" ValidationGroup="Visitor" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                           
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="txtMobNo" EventName="TextChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div4">
                                <label class="control-label col-sm-4" id="lblEmail" style="text-align: left; font-weight: lighter">Email ID</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtVisitEmail" runat="server" CssClass="form-control" placeholder="Enter Email"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtVisitEmail" ValidationGroup="Visitor" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div6">
                                <label class="control-label col-sm-4" id="lblOrganization" style="text-align: left; font-weight: lighter">Organization</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtOrganizeName" runat="server" CssClass="form-control" placeholder="Enter Organization"></asp:TextBox>
                                    <asp:AutoCompleteExtender ServiceMethod="GetVisitorListByOrganization" ServicePath="~/VMS/VisitorRegister/VisitorRegistration.aspx" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                        TargetControlID="txtOrganizeName" ID="AutoCompleteExtender7" OnClientShown="PopupShown" UseContextKey="true" runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div7">
                                <label class="control-label col-sm-4" id="lblInDateTime" style="text-align: left; font-weight: lighter">InDateTime</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtIntime" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div8">
                                        <label class="control-label col-sm-4" id="lblIDProofType" style="text-align: left; font-weight: lighter">IDProofType</label><asp:Button ID="btnIDProofType" runat="server" CssClass="btnnVisit" Text="+" Font-Bold="true" Visible="false" OnClick="btnIDProofType_Click" />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtIDProofType" runat="server" CssClass="form-control" placeholder="Enter ID Proof Type" Visible="false"></asp:TextBox>
                                            <asp:DropDownList ID="ddlIDProofType" ClientIDMode="Static" runat="server" CssClass="form-control" placeholder="Enter ID Proof Type"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div9">
                                        <label class="control-label col-sm-4" id="lblIDProofNo" style="text-align: left; font-weight: lighter">ID Proof No</label>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtIdProofNo" runat="server" CssClass="form-control" placeholder="Enter ID Proof No"></asp:TextBox>
                                          
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="txtIdProofNo" EventName="TextChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div10">
                                        <label class="control-label col-sm-4" id="lblPurpose" style="text-align: left; font-weight: lighter">Purposeof Visit<span class="mandatoryStar">*</span></label><asp:Button ID="btn_purposeofvisit" runat="server" Visible="false" CssClass="btnnVisit" Text="+" Font-Bold="true" OnClick="addpurposeofvisit" />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txt_purposeother" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter Purpose Of Visit" Visible="false"></asp:TextBox>
                                            <asp:DropDownList ID="ddlPurposeofVisit" runat="server" CssClass="form-control" AutoPostBack="true" placeholder="Enter Purpose Of Visit"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Select Purpose of Visit" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlPurposeofVisit" InitialValue="0" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div11">
                                <label class="control-label col-sm-4" id="lblNoofVisitors" style="text-align: left; font-weight: lighter">No of Visitors<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtNoofVisitors" runat="server" CssClass="form-control" placeholder="Enter No of Visitors" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Enter No of Visitors" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtNoofVisitors" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter Value Greater Than 0" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtNoofVisitors" ValidationGroup="Visitor" ValidationExpression="^0*[1-9]\d*$"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div18">
                                <label class="control-label col-sm-4" id="lblBelongings" style="text-align: left; font-weight: lighter">Belongings</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtBelongings" runat="server" CssClass="form-control" placeholder="Enter Belongings"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div27">
                                <label class="control-label col-sm-4" id="lblComments" style="text-align: left; font-weight: lighter">Comments<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtVEditComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Enter Comments" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtVEditComments" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div28">
                                        <label class="control-label col-sm-4" id="lblDepartment" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-8">
                                            <asp:DropDownList ID="ddlVEditDept" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlVEditDept_SelectedIndexChanged" Visible="false"></asp:DropDownList>
                                            <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control" placeholder="Enter Department"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Enter Department" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtDepartment" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                          
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlVEditDept" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div12">
                                        <label class="control-label col-sm-4" id="lblWhomToVisit" style="text-align: left; font-weight: lighter">Whom To Visit<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-8">
                                            <asp:DropDownList ID="ddlWhomtoVisit" runat="server" ClientIDMode="Static" CssClass="form-control" AutoPostBack="true" Visible="false"></asp:DropDownList>
                                            <asp:TextBox ID="txtWhomToVisit" runat="server" CssClass="form-control" placeholder="Enter WhomToVisit" onkeypress="return ValidateAlpha(event)"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter Whom To Visit" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtWhomToVisit" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                          
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlWhomtoVisit" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div13">
                                <label class="control-label col-sm-4" id="lblAddress" style="text-align: left; font-weight: lighter">Address<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" placeholder="Enter Address"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Enter Address" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtAddress" ValidationGroup="Visitor"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>

                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div14">
                                        <label class="control-label col-sm-4" id="lblCountry" style="text-align: left; font-weight: lighter">Country</label>
                                        <div class="col-sm-8">
                                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlCountry" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <asp:UpdatePanel runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div15">
                                        <label class="control-label col-sm-4" id="lblState" style="text-align: left; font-weight: lighter">State</label>
                                        <div class="col-sm-8">
                                            <asp:DropDownList ID="ddlState" runat="server" ClientIDMode="Static" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlState" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <asp:UpdatePanel runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div16">
                                        <label class="control-label col-sm-4" id="lblCity" style="text-align: left; font-weight: lighter">City</label>
                                        <div class="col-sm-8">
                                            <asp:DropDownList ID="ddlCities" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlCities" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div17">
                                <label class="control-label col-sm-4" id="lblVehicleNo" style="text-align: left; font-weight: lighter">Vehicle No</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtVechileNo" runat="server" CssClass="form-control" placeholder="Enter Vehicle No"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div19">
                                <label class="control-label col-sm-4" id="lblPlannedOutTime" style="text-align: left; font-weight: lighter">Planned OutTime</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtPlanTimeOut" runat="server" CssClass="form-control" placeholder="Enter Planned Out Time" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div22">
                                <label class="control-label col-sm-4" id="lblIDCardNumber" style="text-align: left; font-weight: lighter">ID Card Number</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtIdCardNo" runat="server" CssClass="form-control" placeholder="Enter ID Card Number"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div23">
                                <label class="control-label col-sm-4" id="lblAccessLocation" style="text-align: left; font-weight: lighter">Access Location</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtAccessLocation" runat="server" CssClass="form-control" placeholder="Enter Access Location"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div24">
                                <label class="control-label col-sm-4" id="lblAccompanyPerson" style="text-align: left; font-weight: lighter">Accompany Person</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtAccompanyPerson" runat="server" CssClass="form-control" placeholder="Enter Accompany Person"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div25">
                                <label class="control-label col-sm-4" id="lblRoomNumber" style="text-align: left; font-weight: lighter">Room Number</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtRoomNo" runat="server" CssClass="form-control" placeholder="Enter Room Number"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div21">
                                <label class="control-label col-sm-4" id="lblRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="col-sm-4">

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server">
                                <div class="col-sm-8">
                                    <div id="webcam">
                                    </div>
                                    <asp:Image ID="MyImage" runat="server" Style="width: 270px; height: 190px" />
                                    <br />
                                   
                                    <br />
                                    <br />
                                    <span id="camStatus"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

               
                <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                    <asp:Button ID="btnVisitorUpdate" Text="Update" CssClass="button" runat="server" OnClick="btnVisitorUpdate_Click1" UseSubmitBehavior="false" ValidationGroup="Visitor" />
                    <asp:Button ID="btnVisitorCancel" Text="Cancel" CssClass="button" runat="server" CausesValidation="false" />
                </div>
                
            </asp:Panel>

            <script>
                //for start date and end date validations startdate should be < enddate
                $(function () {
                    $('#<%=txtSearchVisitorsFromDate.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                    });
                    var startdate = document.getElementById('<%=txtSearchVisitorsFromDate.ClientID%>').value
                    $('#<%=txtSearchVisitorsToDate.ClientID%>').datetimepicker({
                             format: 'DD MMM YYYY',
                             minDate: startdate,
                             useCurrent: false,
                         });
                         $('#<%=txtSearchVisitorsFromDate.ClientID%>').on("dp.change", function (e) {
                             $('#<%=txtSearchVisitorsToDate.ClientID%>').data("DateTimePicker").minDate(e.date);
                             $('#<%=txtSearchVisitorsToDate.ClientID%>').val("");
                         });
                         $('#<%=txtSearchVisitorsToDate.ClientID%>').on("dp.change", function (e) {
                         });
                });
            </script>

            <script type="text/javascript">
                //for datepickers
                var startdate2 = document.getElementById('<%=txtSearchVisitorsFromDate.ClientID%>').value
                if (startdate2 == "") {
                    $('#<%=txtSearchVisitorsFromDate.ClientID%>').datetimepicker({
                             format: 'DD MMM YYYY',
                             useCurrent: false,
                         });
                         $('#<%=txtSearchVisitorsToDate.ClientID%>').datetimepicker({
                             format: 'DD MMM YYYY',
                             useCurrent: false,
                         });
                }
            </script>

            <script>
                     $("#NavLnkVisitorRegistration").attr("class", "active");
                     $("#VisitorRegistrationList").attr("class", "active");
                   </script>

            <script>
                     function PopupShown(sender, args) {
                         sender._popupBehavior._element.style.zIndex = 99999999;
                     }
            </script>

            <script>
                     window.onload = function () {
                         getDate();
                     };
                     function getDate() {
                         var dtTO = new Date();
                         var Todate = document.getElementById("<%=hdnCheckout.ClientID%>");
                         Todate.value = formatDate(dtTO);
                     }
                     function formatDate(dateObj) {
                         var m_names = new Array("Jan", "Feb", "Mar",
                             "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                             "Oct", "Nov", "Dec");
                         var curr_date = dateObj.getDate();
                         var curr_month = dateObj.getMonth();
                         var curr_year = dateObj.getFullYear();
                         var curr_min = dateObj.getMinutes();
                         var curr_hr = dateObj.getHours();
                         var curr_sc = dateObj.getSeconds();
                         return curr_date + " " + m_names[curr_month] + " " + curr_year + " " + curr_hr + ":" + curr_min + ":" + curr_sc;
                     }
            </script>
            <script type="text/javascript">
                 function Submitvalidate() {
                    var VDepartment = document.getElementById("<%=txtSearchDepartment.ClientID%>").value;
                    var VName = document.getElementById("<%=txtSearchVisitorsName.ClientID%>").value;
                    var VPurpose = document.getElementById("<%=ddlSearchVisitorsPurposeofVisit.ClientID%>").value;
                    var VMobile = document.getElementById("<%=txtSearchVisitorsMobileNo.ClientID%>").value;
                    var VOrganization = document.getElementById("<%=txtSearchVisitorsOrganizationName.ClientID%>").value;
                    var VisitorID = document.getElementById("<%=txtSearchVisitorsID.ClientID%>").value;
                    var FromDate = document.getElementById("<%=txtSearchVisitorsFromDate.ClientID%>").value;
                    var ToDate = document.getElementById("<%=txtSearchVisitorsToDate.ClientID%>").value; 
                    if (VDepartment == "" && VName == "" && VPurpose == 0 && VMobile == "" && VOrganization == "" && VisitorID == "" && FromDate == "" && ToDate == "") {
                      
                        custAlertMsg('Please Select or Enter atleast one Field to Find Data', "error");
                    return false;
                     }
                          
                    
                 }

            </script>

           
        </div>
    </div>
   
</asp:Content>
