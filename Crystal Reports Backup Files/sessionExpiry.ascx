﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="sessionExpiry.ascx.cs" Inherits="AizantIT_PharmaApp.UserControls.sessionExpiry" %>
<!-- Idle TimeOut Alert -->
<div class="modal fade" id="IdleTO_Logout_popup" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog " style="top: 25%; min-width: 30%;">
        <div class="modal-content">
            <div class="col-lg-12 " style="background: #fff; padding: 10px; border-radius: 6px; border: 2px solid #07889a">
                <div class="col-lg-12 float-left float-left top">
                    <div class="col-lg-2 float-left float-left">
                        <img src="<%=ResolveClientUrl("~/Images/TMS_Images/fs-sand-timer.gif")%>" alt="It is Session Time Out" style="margin-left: -31px;">
                        <i class="fs-sand-timer"></i>
                    </div>
                    <div class="col-lg-9 float-left float-left" style="text-align: center">
                        <h4 style="color: #07889a">Your session is about to expire!</h4>
                        <p style="font-size: 15px;">You will be logged out in <span id="timer" style="display: inline; font-size: 30px; color: #07889a;"></span>seconds.</p>
                        <p style="font-size: 15px;">Do you want to stay signed in?</p>
                    </div>
                </div>
                <div class="col-lg-12  top bottom" style="margin-bottom: 10px; text-align: center;">
                    <button type="button" class="btn btn-primary float-left float-left col-lg-6" aria-hidden="true" style="padding: 8px !important; margin-right: 10px;" onclick="IdleSessionUpdate();" title="Click to Continue">Yes, Keep me signed in</button>
                    <button type="button" class="btn btn-danger col-lg-5 float-left float-left" aria-hidden="true" style="padding: 8px !important" onclick="IdleSessionLogOut();" title="Click to LogOut">No, Sign me out</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Idle TimeOut Alert-->

<!--Script For  Idle Time Out-->
<script>
    var dbIdleTimeout = readCookie("CookieExpire");
    var IdleTimer;
    var result = (((parseInt(dbIdleTimeout - '2') * 60) * 1000) - 2000);
    var count = 0; var max_count = 120;
    var coundown = null;
    var IsIdleTimeoutPopupOpen = 'Fasle';
    function fnIdleTimer(state) {
        CloseIdleTimeOutModelpop();
        if (state == "start") {
            if (coundown != null) {
                clearInterval(coundown);
                count = 0;
            }
            IdleTimer = setTimeout(function () {
                UserSessionCheck();
                $('#IdleTO_Logout_popup').modal('show');
                IsIdleTimeoutPopupOpen = 'True';
                var remaining_time = max_count;
                coundown = setInterval(function () {
                    count = count + 1;
                    remaining_time = max_count - count;
                    $('#timer').html(remaining_time);
                    if (remaining_time == 0) {
                        IdleSessionLogOut();
                    }
                }, 1000);
            }, result);
        }
        else {
            clearTimeout(IdleTimer);
            fnIdleTimer('start');
        }
    }
    function CloseIdleTimeOutModelpop() {
        $('#IdleTO_Logout_popup').modal('hide');
        IsIdleTimeoutPopupOpen = 'False';
    }
    function IdleSessionUpdate() {
        fnIdleTimer('restart');
        //fnSessionTimer('restart');
        UpdateSessionTimeOut();
    }
    function IdleSessionLogOut() {
        CloseIdleTimeOutModelpop();        
        var Url = '<%= ResolveUrl("~/Common/WebServices/RemoveUserSession.asmx/Remove_UserSession")%>';
            LogOut(Url);         
    }
</script>

<script>
    $(document).ready(function () {
        fnIdleTimer('start');
        fnSessionTimer('start');
        //UpdateSessionTimeOut();
    });

    $(document).click(function () {
        if (IsIdleTimeoutPopupOpen == 'False') {
            fnIdleTimer('restart');
        }
    });
    $(document).keypress(function () {
        if (IsIdleTimeoutPopupOpen == 'False') {
            fnIdleTimer('restart');
        }
    });
    $(document).scroll(function () {
        if (IsIdleTimeoutPopupOpen == 'False') {
            fnIdleTimer('restart');
        }
    });

</script>
<!--End Script Idle TimeOut-->

<!--Begin Script for SessionTimeOut-->
<script>
        var dbSessionTimeout = dbIdleTimeout;
        var SessionTimer;
        var SessionResult = (((parseInt(dbSessionTimeout - '2') * 60) * 1000) - 2000);
        var SessionCount = 0; var Session_max_count = 120;
        var SessionCountdown = null;
        function fnSessionTimer(SessionState) {
            if (SessionState == "start") {
                if (SessionCountdown != null) {
                    clearInterval(SessionCountdown);
                    SessionCount = 0;
                }
                SessionTimer = setTimeout(function () {
                    if (IsIdleTimeoutPopupOpen == 'False') {
                        UpdateSessionTimeOut();
                    }
                }, SessionResult);
            }
            else {
                clearTimeout(SessionTimer);
                fnSessionTimer('start');
            }
        }
</script>
<script type="text/javascript">
        function UpdateSessionTimeOut() {
            UserSessionCheck();
            CheckWetherCK_EmpIDandCK_SessionTO();
            $.ajax({
                type: "POST",
                url: "<%=ResolveUrl("~/Common/WebServices/SessionTime.asmx/UpdateSessionTimeOut")%>",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                    fnSessionTimer('restart');
                },
                failure: function () {
                    console.log("Server Session Update Failed");
                }
            });
        }
</script>

<!--End Script for SessioTimeOut -->
