﻿<%@ Page Title="JR List" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="EmpJR_List.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Reports.EmpJR_List" %>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>
<%@ Register Src="~/UserControls/TMS_UserControls/ucFeedback.ascx" TagPrefix="uc1" TagName="ucFeedback" %>
<%@ Register Src="~/UserControls/TMS_UserControls/ucJrTrainedRecords.ascx" TagPrefix="uc1" TagName="ucJrTrainedRecords" %>
<%@ Register Src="~/UserControls/TMS_UserControls/JR_TrainingFile.ascx" TagPrefix="uc1" TagName="JR_TrainingFile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnJR_ID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnPrint" runat="server" Value="false" />
            <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnEmpFeedbackID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTS_ID" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div id="divServerBtn" style="display: none">
            <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Button ID="btnViewJR_TF" runat="server" Text="Button"/>
                </ContentTemplate>
            </asp:UpdatePanel> 
            <asp:UpdatePanel ID="upActionBtns" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Button ID="btnViewJR_Details" runat="server" Text="Button" OnClick="btnViewJR_Details_Click" />
                    <asp:Button ID="btnAskFeedBack" runat="server" Text="Button" OnClick="btnAskFeedBack_Click" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="col-12 padding-none" id="mainContainer" runat="server">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Text="Employee JR Report"></asp:Label>
                </div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none" style="overflow: auto; margin-top: 10px">
                    <table id="tblEmpJrList" class="display breakword datatable_cust" style="width: 100%">
                        <thead>
                            <tr>
                                <th>JR ID</th>
                                <th>TS ID</th>
                                <th>Trainee</th>
                                <th>JR Name</th>
                                <th>Version</th>
                                <th>Assigned By</th>
                                <th>Department</th>
                                <th>Designation</th>
                                <th>Status</th>
                                <th>StatusID</th>
                                <th>FeedbackStatus</th>
                                <th>View</th>
                                <th>TF</th>
                                <th>Feedback</th>
                                <th>TR</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <div class="col-12 grid_legend_tms">'TF': Training File ; 'TR': Training Record</div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <asp:UpdateProgress ID="upActionProgress" runat="server" AssociatedUpdatePanelID="upActionButtons">
            <ProgressTemplate>
                <div class="ProgressModal">
                    <div class="ProgressCenter">
                        <img alt="" src="<%=ResolveUrl("~/Images/TMS_Images/please_wait.gif")%>" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>

    <!-- Modal Emp JR Details -->
    <div>
        <div class="modal fade department" id="modelEmpJrDetails_View" role="dialog">
            <div class="modal-dialog " style="min-width: 80%">
                <!-- Modal content-->
                <div class="modal-content" style="border-radius: 0px">
                    <div class="modal-header tmsModalHeader">
                        <h4 class="modal-title">JR View</h4>
                        <button type="button" class="close" onclick="DestroyJrActionHistoryDatatables();" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body tmsModalBody" style="overflow-y: auto; max-height: 600px;">
                        <div class="tmsModalContent col-12">
                            <asp:UpdatePanel ID="upJrDetails" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="accordion" id="accordion12">
                                        <div class="card mb-0">
                                            <div class="card-header grid_header  header_col collapsed accordion-toggle tmsModalHeader" data-toggle="collapse" href="#collapsejr">
                                                <a class="card-title Panel_Title">
                                                    <a>JR Details </a>
                                                </a>
                                            </div>
                                            <div id="collapsejr" class="collapse show" role="tabpanel" aria-labelledby="collapsejr" data-parent="#accordion12">
                                                <div class="card-body">
                                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12" style="background: #f2f2f2; padding: 10px; border: 1px solid #d4d4d4">

                                                        <div class="col-sm-6 float-left">
                                                            <div class="form-horizontal col-12 float-left">
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="control-label text-right col-sm-5 float-left">JR Name :</label>
                                                                    <div class="col-sm-7 float-left label_text">
                                                                        <asp:Label ID="lblJrName" runat="server"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="control-label text-right col-sm-5 float-left">Version :</label>
                                                                    <div class="col-sm-7 label_text float-left">
                                                                        <asp:Label ID="lblJrVersion" runat="server"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="control-label text-right col-sm-5 float-left">Emp Code :</label>
                                                                    <div class="col-sm-7 label_text float-left">
                                                                        <asp:Label ID="lblTraineeCode" runat="server"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="control-label text-right col-sm-5 float-left">Emp Name :</label>
                                                                    <div class=" col-sm-7 label_text float-left">
                                                                        <asp:Label ID="lblTraineeName" runat="server"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="control-label text-right float-left col-sm-5">Date of Joining :</label>
                                                                    <div class="col-sm-7 label_text float-left ">
                                                                        <asp:Label ID="lblTraineeDOJ" runat="server"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="control-label text-right col-sm-5 float-left">Prepared By :</label>
                                                                    <div class="col-sm-7 label_text float-left">
                                                                        <asp:Label ID="lblPreparedBy" runat="server"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="control-label text-right col-sm-5 float-left">Prepared Date :</label>
                                                                    <div class="col-sm-7 label_text float-left">
                                                                        <asp:Label ID="lblPreparedDate" runat="server"></asp:Label>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-12 float-left">
                                                            <div class="form-horizontal col-12 float-left">
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="control-label text-right col-sm-5 float-left">Approved By :</label>
                                                                    <div class="col-sm-7 label_text float-left">
                                                                        <asp:Label ID="lblApprovedBy" runat="server"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="control-label text-right col-sm-5 float-left">Approved Date :</label>
                                                                    <div class="col-sm-7 label_text float-left">
                                                                        <asp:Label ID="lblApprovedDate" runat="server"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="control-label text-right col-sm-5 float-left">TS Author :</label>
                                                                    <div class="col-sm-7 label_text float-left">
                                                                        <asp:Label ID="lblTS_PreparedBy" runat="server"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="control-label text-right col-sm-5 float-left">TS Prepared Date :</label>
                                                                    <div class="col-sm-7 label_text float-left">
                                                                        <asp:Label ID="lblTS_PreparedDate" runat="server"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="control-label text-right col-sm-5 float-left">TS Review By :</label>
                                                                    <div class="col-sm-7 label_text float-left">
                                                                        <asp:Label ID="lblTS_ApprovedBy" runat="server"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="control-label text-right col-sm-5 float-left">TS Review Date :</label>
                                                                    <div class="col-sm-7 label_text float-left">
                                                                        <asp:Label ID="lblTS_ApprovedDate" runat="server"></asp:Label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 float-left" style="color: #098293; font-weight: bold; padding: 6px">TS: Training Schedule</div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                    </div>
                                    <ul class="nav nav-tabs tab_grid float-left col-lg-12 col-sm-12 col-12 col-md-12" style="margin-top: 10px;">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#General">Responsibilities</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" runat="server" id="JrTrainerDetailsTab" href="#Trainer_Details">Training Schedule</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" runat="server" id="JrHistoryTab" href="#JR_History">JR History</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" runat="server" id="JrTsHistoryTab" href="#JRTS_History">JR Training Schedule History</a></li>
                                    </ul>
                                    <div class="tab-content float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                        <div id="General" class="tab-pane fade show active">
                                            <div class="form-group">
                                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none " style="overflow: auto; max-height: 350px;">
                                                    <div style="border: 1px solid #d4d4d4; max-height: 285px; overflow: auto; border-width: 1px; padding: 10px 20px;">
                                                        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="Trainer_Details" class="tab-pane fade">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                                                <ul id="ulTrainingscheduleTabs" class="nav nav-tabs tab_grid float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                                    <li id="liRegular" class=" nav-item "><a data-toggle="tab" class="RegularTab nav-link" id="liRegularTab" href="#Regular">Regular</a></li>
                                                    <li class=" nav-item "><a data-toggle="tab" class="RetrospectiveTab nav-link" href="#Retrospective">Retrospective</a></li>
                                                </ul>
                                                <div class="tab-content float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                    <div id="Regular" class="tab-pane fade RegularTabBody ">
                                                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                                                            <table id="tblEmpJRTrainerDetails" class="datatable_cust tblEmpJRTrainerDetailsClass display breakword" style="width: 100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Department</th>
                                                                        <th>Document Numbers</th>
                                                                        <th>Trainers</th>
                                                                        <th>Days</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div id="Retrospective" class="tab-pane fade RetrospectiveTabBody">
                                                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                                                            <table id="tblEmpJrRetrospectiveDetails" class="datatable_cust tblEmpJrRetrospectiveDetailsClass display breakword" style="width: 100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Department</th>
                                                                        <th>Document Numbers</th>
                                                                        <th>DeptCode</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="JR_History" class="tab-pane fade">
                                            <div class="col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                                                <table id="tblEmpJRList_ActionHistory" class="ActionDT datatable_cust tblEmpJRList_ActionHistoryClass display breakword" style="width: 100%">
                                                    <thead>
                                                        <tr>
                                                            <th>JR_HistoryID</th>
                                                            <th>Action</th>
                                                            <th>Action By</th>
                                                            <th>Role</th>
                                                            <th>Action Date</th>
                                                            <th>Remarks</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                        <div id="JRTS_History" class="tab-pane fade">
                                            <div class="-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                                                <table id="tblJrTS_ActionHistory" class="datatable_cust tblJrTS_ActionHistoryClass display breakword">
                                                    <thead>
                                                        <tr>
                                                            <th>Action</th>
                                                            <th>Action By</th>
                                                            <th>Role</th>
                                                            <th>Action Date</th>
                                                            <th>Remarks</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <uc1:ucFeedback runat="server" ID="ucFeedback" />
    <uc1:ucJrTrainedRecords runat="server" ID="ucJrTrainedRecords" />
    <uc1:JR_TrainingFile runat="server" id="JR_TrainingFile" />

    <!--Fix up GridView to support THEAD tags-->
    <script type="text/javascript">
        $(document).ready(function () {
            fixGvHeader();
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixGvHeader();
        });

        function fixGvHeader() {
            var tbl = document.getElementsByClassName("fixHeadJR_Commentry");
            // Fix up GridView to support THEAD tags     
            if ($(".fixHeadJR_Commentry").find("thead").length == 0) {
                $(".fixHeadJR_Commentry tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadJR_Commentry thead tr").append($(".fixHeadJR_Commentry th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadJR_Commentry tbody tr:first").remove();
                }
            }
        }
    </script>

    <script>        
        function disableControl(control) {
            $(control).attr('disabled', 'disabled');
        }
    </script>

    <!-- Action Buttons Operations -->
    <script>
        function viewEmpJR_Details(JrID, TS_ID) {
            $("#<%=hdnJR_ID.ClientID%>").val(JrID);
            $("#<%=hdnTS_ID.ClientID%>").val(TS_ID);
            $("#<%=btnViewJR_Details.ClientID%>").click();
        }

        function viewEmpJR_Feedback(EmpFeedbackID) {
            $('#<%=hdnEmpFeedbackID.ClientID%>').val(EmpFeedbackID);
            $("#<%=btnAskFeedBack.ClientID%>").click();
        }

        function viewJrTrainingRecords(JR_ID) {
            GetRegularRecords(JR_ID, 0, false);
            GetRetrospectiveRecords(JR_ID, 0);
        }
    </script>
    
    <!--Emp JR List jQuery DataTable-->
    <script>  
        $('#tblEmpJrList thead tr').clone(true).appendTo('#tblEmpJrList thead');
        $('#tblEmpJrList thead tr:eq(1) th').each(function (i) {
            if (i < 9) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (oTblEmpJRs.column(i).search() !== this.value) {
                        oTblEmpJRs
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });

        var oTblEmpJRs = $('#tblEmpJrList').DataTable({
            columns: [
                { 'data': 'JR_ID' },
                { 'data': 'TS_ID' },
                { 'data': 'TraineeName' },
                { 'data': 'JR_Name' },
                { 'data': 'JR_Version' },
                { 'data': 'HOD_Name' },
                { 'data': 'DepartmentName' },
                { 'data': 'DesignationName' },
                { 'data': 'StatusName' },
                { 'data': 'CurrentStatus' },
                { 'data': 'EmpFeedbackID' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="view" title="View JR"  href="#" onclick="viewEmpJR_Details(' + o.JR_ID + ',' + o.TS_ID + ');"></a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        if (o.CurrentStatus == "10") {
                            return '<a class="trainingfile_tms" href="#" title="Training File" onclick="viewEmpTF_Details(' + o.JR_ID + ');"></a>';
                        }
                        else {
                            return '';
                        }
                    }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        if (o.EmpFeedbackID != 0) {
                            return '<a class="feedback_tms" href="#" title="View Feedback" onclick="viewEmpJR_Feedback(' + o.EmpFeedbackID + ');"></a>';
                        }
                        else {
                            return '';
                        }
                    }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        if (o.CurrentStatus > 4) {
                            return '<a class="trainedrecords_tms"  href="#" title="Training Record" onclick="viewJrTrainingRecords(' + o.JR_ID + ',' + o.TS_ID + ');"></a>';
                        }
                        else {
                            return '';
                        }
                    }
                }
            ],
            "orderCellsTop": true,
            "aoColumnDefs": [{ "targets": [0, 1, 9, 10], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [2, 3, 5, 6, 7, 8] }, { "bSortable": false, "aTargets": [4] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/EmpJrListService.asmx/GetDatatoEmpJR_Report")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "Year", "value": getUrlVars()["Year"] }, { "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "DeptID", "value": getUrlVars()["DeptID"] }, { "name": "FilterType", "value": getUrlVars()["FilterType"] }, { "name": "Status", "value": getUrlVars()["Status"] });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblEmpJrList").show();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
        
    </script>

    <!--JR Trainer Details-->
    <script>
        var oTblJrTrainerDetails;
        function loadJrTrainerDetails() {
            if (oTblJrTrainerDetails != null) {
                oTblJrTrainerDetails.destroy();
            }
            oTblJrTrainerDetails = $('#tblEmpJRTrainerDetails').DataTable({
                columns: [
                    { 'data': 'Department' },
                    { 'data': 'Document' },
                    { 'data': 'Trainer' },
                    { 'data': 'Days' }
                ],
                "responsive": true,
                "colReorder": true,
                "stateSave": true,
                "aoColumnDefs": [{ className: "textAlignLeft", "targets": [0, 1, 2] }],
                "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/EmpJrListService.asmx/GetJrTS_TrainerDetails")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "JR_ID", "value": $('#<%=hdnJR_ID.ClientID%>').val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            var IsRetrospective = json["iIsRetrospective"];
                            if (IsRetrospective == 0) {
                                $('#ulTrainingscheduleTabs').hide();
                            }
                            else {
                                $('#ulTrainingscheduleTabs').show();
                            }
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }

    </script>

    <!--JR Retrospective Details-->
    <script>
        var oTblJrRetrospectiveDetails;
        function loadJrRetrospectiveDetails() {
            if (oTblJrRetrospectiveDetails != null) {
                oTblJrRetrospectiveDetails.destroy();
            }
            oTblJrRetrospectiveDetails = $('#tblEmpJrRetrospectiveDetails').DataTable({
                columns: [
                    { 'data': 'DeptName' },
                    { 'data': 'DocumentNumber' },
                    { 'data': 'DeptCode' }
                ],
                "responsive": true,
                "colReorder": true,
                "stateSave": true,
                "aoColumnDefs": [{ "targets": [2], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [0, 1] }],
                "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/EmpJrListService.asmx/GetJrTS_RetrospectiveDetails")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "JR_ID", "value": $('#<%=hdnJR_ID.ClientID%>').val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }

    </script>

    <!--JR_Action  History jQuery DataTable-->
    <script>
        var oTblJrHistory;

        function loadJrHistory() {
            if (oTblJrHistory != null) {
                oTblJrHistory.destroy();
            }
            oTblJrHistory = $('#tblEmpJRList_ActionHistory').DataTable({
                columns: [
                   // { 'data': 'JR_HistoryID' },
                    { 'data': 'ActionHistoryID' },//0
                    { 'data': 'ActionStatus' },//1
                    { 'data': 'ActionBy' },//2
                    { 'data': 'ActionRole' },//3
                    { 'data': 'ActionDate' },//4
                    { 'data': 'Remarks' }//5
                ],
                
                "responsive": true,
               "colReorder": true,
                //"stateSave": true,
                "aoColumnDefs": [{ "targets": [0], "visible": false }, { className: "textAlignLeft", "targets": [1, 2, 3, 5] }, {"orderable": false, "targets": [1,2,3,4,5]}],
                "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
                 <%--"sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JR_ActionHistoryService.asmx/GetJR_ActionHistory")%>",--%>
                 "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TMS_ActionHistoryService.asmx/GetTMS_ActionHistory")%>",
                  "fnServerData": function (sSource, aoData, fnCallback) {
                    <%--aoData.push({ "name": "JR_ID", "value": $('#<%=hdnJR_ID.ClientID%>').val() });--%>
                    aoData.push({ "name": "Base_ID", "value": $('#<%=hdnJR_ID.ClientID%>').val() },{ "name": "Table_ID", "value": "2"});
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }

    </script>

    <!--JRTS_Action  History jQuery DataTable-->
    <script>
        var oTableJrTS_ActionHistory;
        function loadJrTS_ActionHistory() {
            if (oTableJrTS_ActionHistory != null) {
                oTableJrTS_ActionHistory.destroy();
            }
            oTableJrTS_ActionHistory = $('#tblJrTS_ActionHistory').DataTable({
                columns: [
                    { 'data': 'ActionStatus' },//0
                    { 'data': 'ActionBy' },//1
                    { 'data': 'ActionRole' },//2
                    { 'data': 'ActionDate' },//3
                    { 'data': 'Remarks' }//4
                ],
                "order": [[0]],
                "scrollY": "250px",
                "aoColumnDefs": [{ className: "textAlignLeft", "targets": [0, 1, 2, 4] },{"orderable": false, "targets": [0,1,2,3,4]}],
            <%--"sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTsActionHistoryService.asmx/GetJrTS_ActionHistory")%>",--%>
                 "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TMS_ActionHistoryService.asmx/GetTMS_ActionHistory")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "Base_ID", "value": $('#<%=hdnTS_ID.ClientID%>').val() },{ "name": "Table_ID", "value": "3"});
                <%--aoData.push({ "name": "TS_ID", "value": $('#<%=hdnTS_ID.ClientID%>').val() });--%>
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                            $("#tblJrTS_ActionHistory").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblJrTS_ActionHistoryClass").css({ "width": "100%" });
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
    </script>

    <!--Open Models-->
    <script>
        function OpenModelViewJR() {
            DestroyJrActionHistoryDatatables();
            $('#modelEmpJrDetails_View').modal({ backdrop: 'static', keyboard: false });
        }
    </script>

    <script>
        //Commented since there's an issue by opening the popup and clicking on collapse.
         //$(document).ready(function () {
         //   var prm = Sys.WebForms.PageRequestManager.getInstance();
         //   prm.add_endRequest(function () {
         //    $('#collapsejr').on('hidden.bs.collapse', function () {
         //       $('.ActionDT').DataTable().columns.adjust();
         //    });
         //    $('#collapsejr').on('shown.bs.collapse', function () {
         //       $('.ActionDT').DataTable().columns.adjust();
         //    });
         //  });
         //});
    </script>

    <!--To destroy Datatables on popup close--->
    <script>
         function DestroyJrActionHistoryDatatables() {
             if (oTblJrHistory != null) {
                 oTblJrHistory.destroy();
                 oTblJrHistory = null;
             }
             if (oTableJrTS_ActionHistory != null) {
                 oTableJrTS_ActionHistory.destroy();
                 oTableJrTS_ActionHistory = null;
             }
             if (oTblJrTrainerDetails != null) {
                 oTblJrTrainerDetails.destroy();
                 oTblJrTrainerDetails = null;
             }
             if (oTblJrRetrospectiveDetails != null) {
                 oTblJrRetrospectiveDetails.destroy();
                 oTblJrRetrospectiveDetails = null;
             }
         }
    </script>

    <!--To Load on Tab click--->
    <script>
        $(document).ready(function () {
            $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
                var anchor = $(e.target).attr('href');
                if (anchor == "#Trainer_Details") {
                    SetRegularTabToActive();
                    loadJrTrainerDetails();
                }
                if (anchor == "#JR_History") {
                    loadJrHistory();
                }
                if (anchor == "#JRTS_History") {
                    loadJrTS_ActionHistory();
                }
                if (anchor == "#Retrospective") {
                    loadJrRetrospectiveDetails();
                }
                if (anchor == "#Regular") {
                    loadJrTrainerDetails();
                }
            });
           
        });
    </script>

    <!--To make active Tab-->
    <script>
        function SetRegularTabToActive() {
            $('.RegularTab').addClass('active');
            $('.RegularTab').addClass('show');
            $('.RetrospectiveTab').removeClass('active');
            $('.RetrospectiveTab').removeClass('show');

            $('.RegularTabBody').addClass('active');
            $('.RegularTabBody').addClass('show');
            $('.RetrospectiveTabBody').removeClass('active');
            $('.RetrospectiveTabBody').removeClass('show');
        }
    </script>

</asp:Content>
