﻿<%@ Page Title="JR TS Pending" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="JrTS_PendingList.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TS.JrTS_PendingList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hdnEmpID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnTS_ID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnTS_Status" Value="0" />
            <asp:HiddenField runat="server" ID="hdnNotificationID" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="col-12 padding-none" id="Div3" runat="server">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">JR Training Schedule Pending List</div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                    <table id="tblJrTS_PendingList" class="datatable_cust tblJrTS_PendingListClass display breakword" style="width:100%">
                        <thead>
                            <tr>
                                <th>TS_ID</th>
                                <th>JR Name</th>
                                <th>Version</th>
                                <th>Trainee</th>
                                <th>Emp.Code</th>
                                <th>Department</th>
                                <th>Dept.Code</th>
                                <th>Designation</th>
                                <th>Reviewer</th>
                                <th>Status</th>
                                <th>StatusID</th>
                                <th>View</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnViewJrTS" runat="server" Text="ViewJR" OnClick="btnViewJrTS_Click"/>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
   
    <!--JrTS Pending List jQuery DataTable-->
    <script>
        function getUrlVars() {
            var vars = [], hash;
            var url = window.location.href;
            var hashes = url.slice(url.indexOf('?') + 1).split('&');
            if (url.indexOf("#") > 0) {
                var hashes = url.slice(url.indexOf('?') + 1, url.indexOf('#')).split('&');
            }

            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        var oTableJrTS_PendingList = $('#tblJrTS_PendingList').DataTable({
            columns: [
                { 'data': 'TS_ID' },
                { 'data': 'JR_Name' },
                { 'data': 'JR_Version' },
                { 'data': 'TraineeName' },
                { 'data': 'EmpCode' },
                { 'data': 'DeptName' },
                { 'data': 'DeptCode' },
                { 'data': 'DesignationName' },
                { 'data': 'TS_Reviewer' },
                { 'data': 'StatusName' },
                { 'data': 'StatusID' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="view"  href="#" onclick="ViewJrTS_PendingList(' + o.TS_ID + ',' + o.StatusID+ ');"></a>'; }
                }
            ],
            "order": [[1, "desc"]],
            "scrollY": "47vh",
            "aoColumnDefs": [{ "targets": [0, 6, 10], "visible": false }, { className: "textAlignLeft", "targets": [1, 3, 4, 5, 6, 7, 8, 9] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTraining/JrTs/JrTS_PendingListService.asmx/GetJrTS_PendingList")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "FilterType", "value": getUrlVars()["FilterType"] }, { "name": "NotificationID", "value": <%=hdnNotificationID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblJrTS_PendingList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        $(".tblJrTS_PendingListClass").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
            
        });
        
    </script>

      <script>
          function ViewJrTS_PendingList(TS_ID,TS_Status) {
              $("#<%=hdnTS_ID.ClientID%>").val(TS_ID);
              $("#<%=hdnTS_Status.ClientID%>").val(TS_Status);
            $("#<%=btnViewJrTS.ClientID%>").click();
          }
    </script>
</asp:Content>
