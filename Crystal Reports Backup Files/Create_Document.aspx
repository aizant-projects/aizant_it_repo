﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true"
    CodeBehind="Create_Document.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentCreator.Create_Document" EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <%-- <link href="<%=ResolveUrl("~/AppCSS/DMS/DMS_StyleSheet1.css")%>" rel="stylesheet" />--%>
    <style>
        table tbody tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            padding: 4px;
            border-right: 1px solid #fff;
        }

        .dxreControl .dxreView {
            background-color: #E0E0E0;
            height: 392px !important;
        }

        .dxreView {
            position: relative;
            overflow: auto;
            padding: 0 10px;
            -webkit-overflow-scrolling: touch;
            -webkit-appearance: none;
            -ms-touch-action: pan-y pinch-zoom;
            touch-action: pan-y pinch-zoom;
            height: 455px !important;
        }

        .CreatorGrid thead tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            padding: 4px;
            border-right: 1px solid #fff;
        }

        table tbody tr td {
            /*text-align: center;*/
            height: 30px;
        }

        .CreatorGrid tbody tr td {
            text-align: center;
            height: 30px;
            width: 20px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            background-color: #07889a !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnNewDocTab" runat="server" Value="NEWTab" />
            <asp:HiddenField ID="hdnGrdiReferesh" runat="server" Value="Link" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class="col-md-12 col-lg-12 col-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div class="col-md-12 col-lg-12 col-12 padding-none float-left" id="divMainContainer" runat="server" visible="true">
        <ul class="nav nav-tabs tab_grid top ">
            <li id="tab1" class="  nav-item"><a class="NewDocumentTab nav-link active" data-toggle="tab" href="#TabPanel1">New Documents</a></li>
            <li id="tab2" class=" nav-item"><a class="RevertedTab nav-link" data-toggle="tab" href="#TabPanel2" onclick="loadRevertedDocumentsDT();">Reverted Documents</a></li>
        </ul>
        <div class="tab-content padding-none">
            <div id="TabPanel1" class="NewDocumentTabBody tab-pane fade  active show">
               
                    <div class=" col-md-12 col-lg-12 col-sm-12 col-12 dms_outer_border_create float-left padding-none top">
                        
                            <div class="col-md-12 col-lg-12 col-sm-12 col-12  padding-none float-left">
                                <div class="grid_header col-md-12 col-lg-12 col-sm-12 col-12 float-left">New Documents</div>
                                <div class="col-md-12 col-lg-12 col-sm-12 col-12 grid_panel_full border_top_none bottom float-left ">
                                     <div class="col-12 top float-left  padding-none">
                                    <table id="dtDates1" class="tblNewDocsClass display datatable_cust" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Version ID</th>
                                                <th>Document Number</th>
                                                <th>Document Name</th>
                                                <th>Department</th>
                                                <th>Document Type</th>
                                                <th>Initiated By</th>
                                                <th>Request Type</th>
                                                <th>Create</th>
                                                <th>Process Type</th>
                                                <th>History</th>
                                            </tr>
                                        </thead>
                                    </table>
                                         </div>
                                    <div class="col-lg-12  float-left bottom">
                                        <img src="<%=ResolveUrl("~/Images/GridActionImages/create_icon.png")%>" /><b class="grid_icon_legend">Create Document</b>
                                        <img src="<%=ResolveUrl("~/Images/GridActionImages/edit.png")%>" /><b class="grid_icon_legend">Modify Document</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               
           
            <div id="TabPanel2" class="RevertedTabBody tab-pane fade">
                
                    <div class=" col-md-12 col-lg-12 col-sm-12 col-12 dms_outer_border_create float-left top padding-none">
                        
                            <div class="col-md-12 col-lg-12 col-sm-12 col-12  padding-none float-left">
                                <div class="grid_header col-md-12 col-lg-12 col-sm-12 col-12 float-left">Reverted Documents</div>
                                <div class="col-md-12 col-lg-12 col-sm-12 col-12 grid_panel_full border_top_none bottom float-left">
                                     <div class="col-12 top bottom float-left  padding-none">
                                    <table id="dtDates2" class="tblRevertedDocsClass display datatable_cust" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Version ID</th>
                                                <th>Document Number</th>
                                                <th>Document Name</th>
                                                <th>Department</th>
                                                <th>Document Type</th>
                                                <th>Initiated By</th>
                                                <th>Request Type</th>
                                                <th>Process Type</th>
                                                <th>Modify</th>
                                                <th>History</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                    </div>
                            </div>
                        </div>
             
                </div>
           
       </div>

    </div>

    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog">
        <div class=" modal-dialog modal-lg" style="min-width:85%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <asp:UpdatePanel runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            
                            <span id="span4" runat="server"></span>
                           
                        </ContentTemplate>
                    </asp:UpdatePanel>
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                                <div align="center" class="col-md-12 col-lg-12 col-12 col-sm-12 history_grid">
                                    <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="User" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("GivenBy") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Role" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("CommentedDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Time Spent">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Time" runat="server" Text='<%# Eval("Time") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="22%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Comments" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="26%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                    <asp:TextBox ID="txt_Comments" runat="server" ReadOnly="true" Rows="4" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                    <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM">Show More</asp:LinkButton>
                                                    <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL">Show Less</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Assigned To">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_AssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <div id="divActionBtns" style="display: none">
        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" OnClientClick="showImg();"/>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="hdfPKID" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRequestType" runat="server" Value="0" />
    <script>           
        $('#dtDates1 thead tr').clone(true).appendTo('#dtDates1 thead');
        $('#dtDates1 thead tr:eq(1) th').each(function (i) {
            if (i < 8) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (NewDocuments.column(i).search() !== this.value) {
                        NewDocuments
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        var NewDocuments = $('#dtDates1').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'VersionID' },
                { 'data': 'DocumentNumber' },
                { 'data': 'DocumentName' },
                { 'data': 'Department' },
                { 'data': 'DocumentType' },
                { 'data': 'Initiator' },
                { 'data': 'Request Type' },
                { 'data': 'Create' },
                { 'data': 'ProcessTypeID' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" href="#" title="History" onclick=ViewCommentHistory(' + o.VersionID + ')></a>'; }
                }
            ],
            "aoColumnDefs": [{ "targets": [0,1, 6, 9], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 1, 8] }, { "className": "dt-body-left", "targets": [2, 3, 4, 5, 7] },
            {
                targets: [7], render: function (a, b, data, d) {
                    if (data.RequestType == "New Document") {
                        return 'New';
                    } else if (data.RequestType == "Revision Of Existing Document") {
                        return 'Revision';
                    }
                    else if (data.RequestType == "Withdrawal Document") {
                        return 'Withdraw';
                    }
                    return "N/A";
                }
            },
            {
                targets: [8], render: function (a, b, data, d) {
                    if (data.RequestType == "New Document") {
                        return '<a  class="create_dms" title="Create" href=/DMS/DocumentCreator/Document_Details.aspx?DocumentID=' + data.VersionID + '&tab=1&PID=' + data.ProcessTypeID + '>' + '' + '</a>';
                    }
                    else if (data.RequestType == "Revision Of Existing Document") {
                        return '<a  class="Edit" title="Modify" href=/DMS/DocumentCreator/Document_Details.aspx?DocumentID=' + data.VersionID + '&tab=1&PID=' + data.ProcessTypeID + '>' + '' + '</a>';
                    }
                    return "N/A";
                }
            },],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("/DMS/WebServices/DMSService.asmx/GetNewDocCreatorList" )%>',
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "ReqTypeID", "value": <%=hdnRequestType.Value%> });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                            $("#dtDates1").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblNewDocsClass").css({ "width": "100%" });
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
        });

        function CloseAllPopupNew() {
            if (NewDocuments != null) {
                NewDocuments.ajax.reload();
            }
        }
    </script>
    <script>    
        $('#dtDates2 thead tr').clone(true).appendTo('#dtDates2 thead');
        $('#dtDates2 thead tr:eq(1) th').each(function (i) {
            if (i < 8) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (RevertedDocuments.column(i).search() !== this.value) {
                        RevertedDocuments
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        var RevertedDocuments;
        function loadRevertedDocumentsDT() {
            if (RevertedDocuments != null) {
                RevertedDocuments.destroy();
            }
            RevertedDocuments = $('#dtDates2').DataTable({
                columns: [
                    { 'data': 'RowNumber' },
                    { 'data': 'VersionID' },
                    { 'data': 'DocumentNumber' },
                    { 'data': 'DocumentName' },
                    { 'data': 'Department' },
                    { 'data': 'DocumentType' },
                    { 'data': 'Initiator' },
                    { 'data': 'Request Type' },
                    { 'data': 'ProcessTypeID' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) { return '<a  class="Edit" title="Modify" href=/DMS/DocumentCreator/Document_Details.aspx?DocumentID=' + o.VersionID + '&tab=2&PID=' + o.ProcessTypeID + '>' + '' + '</a>'; }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) { return '<a  class="summary_latest" href="#" title="History" onclick=ViewCommentHistory(' + o.VersionID + ')></a>'; }
                    }
                ],
                "aoColumnDefs": [{ "targets": [0,1, 6, 8], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 1] }, { "className": "dt-body-left", "targets": [2, 3, 4, 5, 7] },
                {
                    targets: [7], render: function (a, b, data, d) {
                        if (data.RequestType == "New Document") {
                            return 'New';
                        } else if (data.RequestType == "Revision Of Existing Document") {
                            return 'Revision';
                        }
                        else if (data.RequestType == "Withdrawal Document") {
                            return 'Withdraw';
                        }
                        return "N/A";
                    }
                    },],
                "orderCellsTop": true,
                "order": [[1, "desc"]],
                'bAutoWidth': true,
                sServerMethod: 'Post',
                "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetPendingDocCreatorList" )%>',
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "ReqTypeID", "value": <%=hdnRequestType.Value%> });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                            $("#dtDates2").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblRevertedDocsClass").css({ "width": "100%" });
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
        function CloseAllPopupReverted() {
            loadRevertedDocumentsDT();
        }
    </script>
    <script>     
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfPKID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }

    </script>
    <!--To Reload the Jquery based on the Active Tab-->
    <script>
        function ReloadSessionDatatables() {
            if ($('#tab1').hasClass('active')) {
                if (NewDocuments != null) {
                    NewDocuments.ajax.reload();
                }
            }
            if ($('#tab2').hasClass('active')) {
                if (RevertedDocuments != null) {
                    RevertedDocuments.ajax.reload();
                }
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            $('.rtxtcontent').richText({
                // text formatting
                bold: true,
                italic: true,
                underline: true,
                // text alignment
                leftAlign: true,
                centerAlign: true,
                rightAlign: false,
                // lists
                ol: false,
                ul: false,
                // title
                heading: true,
                // colors
                fontColor: false,
                // uploads
                imageUpload: false,
                fileUpload: false,
                videoUpload: false,
                // link
                urls: true,
                // tables
                table: false,
                // code
                removeStyles: true,
                code: false,
                // colors
                colors: [],
                // dropdowns
                fileHTML: '',
                imageHTML: '',
                videoHTML: '',
            });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(document).ready(function () {
                $('.rtxtcontent').richText({
                    // text formatting
                    bold: true,
                    italic: true,
                    underline: true,
                    // text alignment
                    leftAlign: true,
                    centerAlign: true,
                    rightAlign: false,
                    // lists
                    ol: false,
                    ul: false,
                    // title
                    heading: true,
                    // colors
                    fontColor: false,
                    // uploads
                    imageUpload: false,
                    fileUpload: false,
                    videoUpload: false,
                    // link
                    urls: true,
                    // tables
                    table: false,
                    // code
                    removeStyles: true,
                    code: false,
                    // colors
                    colors: [],
                    // dropdowns
                    fileHTML: '',
                    imageHTML: '',
                    videoHTML: '',
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txt_DocAutoSearch").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Create_Document.aspx/GetDocListByNameorNumber",
                        data: "{'namePrefix':'" + $("#txt_DocAutoSearch").val() + "'}",
                        dataType: "json",
                        minLength: 3,
                        success: function (data) {
                            response(data.d)
                        },
                        error: function (response) {
                            alert("Error" + res.responseText);
                        }
                    });
                }
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            MainTabState();
            TabClick();
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            MainTabState();
            TabClick();
        });
        function MainTabState() {
            if ($('#<%=hdnNewDocTab.ClientID%>').val() == 'NewTab') {
                $('.RevertedTab').removeClass('active');
                $('.NewDocumentTab').addClass('active');
                $('.RevertedTabBody').removeClass('active');
                $('.RevertedTabBody').removeClass('show');
                $('.NewDocumentTabBody').addClass('active');
                $('.NewDocumentTabBody').addClass('show');
            }
            if ($('#<%=hdnNewDocTab.ClientID%>').val() == 'RevertedTab') {
                $('.NewDocumentTab').removeClass('active');
                $('.RevertedTab').addClass('active');
                $('.RevertedTabBody').addClass('active');
                $('.RevertedTabBody').addClass('show');
                $('.NewDocumentTabBody').removeClass('active');
                $('.NewDocumentTabBody').removeClass('show');
                loadRevertedDocumentsDT();
            }
        }
    </script>
    <script>
        function TabClick() {
            $(".RevertedTab").click(function () {
                $('#<%=hdnNewDocTab.ClientID%>').val('RevertedTab');
            });
            $(".NewDocumentTab").click(function () {
                $('#<%=hdnNewDocTab.ClientID%>').val('NewTab');
            });
        }
        $(document).ready(function () {
            $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
                var anchor = $(e.target).attr('href');
                if (anchor == "#TabPanel1") {
                    CloseAllPopupNew();
                }
                else if (anchor == "#TabPanel2") {
                    loadRevertedDocumentsDT();
                }
            })
        });
    </script>
    <script>
        function showImg() {
            ShowPleaseWait('show');
        }
        function hideImg() {
            ShowPleaseWait('hide');
        }
    </script>
</asp:Content>
