﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReadSopInIFrame.aspx.cs" EnableEventValidation="false" Inherits="AizantIT_PharmaApp.TMS.Training.ReadSopInIFrame" %>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="border: groove; border-width: 1px;">
            <div id="divPdfViewer" runat="server" visible="false">
                <asp:Literal ID="literalSop" runat="server"></asp:Literal>
            </div>
            <div id="divDX_Viewer" runat="server" visible="false">
                <dx:ASPxRichEdit ID="ASPxRichEdit1" runat="server" WorkDirectory="~\App_Data\WorkDirectory"
                    ReadOnly="true" Settings-Behavior-Copy="Disabled" Settings-Bookmarks-Visibility="Hidden"
                    ShowConfirmOnLosingChanges="false" Settings-DocumentCapabilities-Bookmarks="Hidden" Width="100%"
                    StylesRibbon-GroupPopup-Wrap="False" StylesPopupMenu-Item-Cursor="wait" StylesPopupMenu-SubMenuItem-Wrap="False"
                    RibbonMode="None" Settings-HorizontalRuler-Visibility="Hidden">
                    <Settings>
                        <Behavior Copy="Disabled" SaveAs="Disabled" Printing="Disabled" Download="Disabled" CreateNew="Disabled" />
                    </Settings>
                    <SettingsDocumentSelector EditingSettings-AllowCopy="false"></SettingsDocumentSelector>
                </dx:ASPxRichEdit>
            </div>
        </div>       
    </form>
</body>
</html>
