﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="TS_Approval.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TS.TS_Approval1" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<%@ Register Src="~/UserControls/SessionTimeOut.ascx" TagPrefix="uc1" TagName="SessionTimeOut" %>
<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <style>
        /*.modal-backdrop {
           background-color: gray;
        }*/
    </style>
    <style>
        .ui-dialog-title {
            font-size: 110% !important;
            color: #FFFFFF !important;
        }

        .gvArea {
            max-height: 550px;
            margin: 0px;
            overflow: auto;
            width: 100%;
        }
    </style>
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 dms_outer_border">
        <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:HiddenField ID="hdnJR_ID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hdnAction" runat="server" Value="0" />
        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>

        <div class="panel panel-default" id="mainContainer" runat="server">
                        <div class="grid_header col-lg-12  col-xs-12 col-md-12 col-sm-12 "><asp:Label ID="Label1" runat="server" Font-Bold="true">Approve TS</asp:Label></div>
            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 top">
                <asp:UpdatePanel ID="upGvTS_Approval" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="gvTS_Approval" runat="server" AutoGenerateColumns="False"
                            CssClass="table table-fixed fixHead AspGrid" OnRowCommand="gvTS_Approval_RowCommand"
                            EmptyDataText="No Records in TS Approve" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle"
                            DataKeyNames="JR_ID">
                            <AlternatingRowStyle BackColor="White" />
                             <RowStyle CssClass="col-xs-12 padding-none" />
                            <Columns>
                                <asp:TemplateField HeaderText="JR ID" Visible="false">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEditJR_ID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblJR_ID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="JR Name" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEditJR_Name" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblJR_Name" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Version" SortExpression="JR_Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Emp Code" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEditEmpCode" runat="server" Text='<%# Bind("EmpCode") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmpCode" runat="server" Text='<%# Bind("EmpCode") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trainee" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEditTrainee" runat="server" Text='<%# Bind("TraineeName") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTrainee" runat="server" Text='<%# Bind("TraineeName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- <asp:TemplateField HeaderText="Department">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblEditDeptName" runat="server" Text='<%# Bind("DeptName") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDeptName" runat="server" Text='<%# Bind("DeptName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Dept Code" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEditDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Designation" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEditDesignation" runat="server" Text='<%# Bind("DesignationName") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDesignation" runat="server" Text='<%# Bind("DesignationName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TS Created By" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEditTS_CreatedBy" runat="server" Text='<%# Bind("TS_Author") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblTS_CreatedBy" runat="server" Text='<%# Bind("TS_Author") %>'></asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TS Created Date" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTS_CreatedDate" runat="server" Text='<%# Bind("TS_CreatedDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblTS_CreatedDate" runat="server" Text='<%# Bind("TS_CreatedDate") %>'></asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTS_StatusName" runat="server" Text='<%# Bind("StatusName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblTS_StatusName" runat="server" Text='<%# Bind("StatusName") %>'></asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="View TS" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1 text-center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkViewTS" runat="server" Font-Underline="true" CommandName="ViewTS"
                                            CommandArgument="<%# Container.DataItemIndex %>">
                                            <%--<asp:Image ID="Image1" runat="server" ImageUrl="/Images/TMS_Images/DocSearch.png" Style="width: 22px" />--%>
                                            <asp:Image ID="Image1" runat="server" ImageUrl="/Images/TMS_Images/view_tms.png" Style="width: 22px" />
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modelTS_View" role="dialog">
            <div class="modal-dialog modal-lg" style="width: 90%">
                <!-- Modal content-->
                <div class="modal-content" style="border-radius: 0px">
                    <div class="modal-header tmsModalHeader">
                        <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title Panel_Title">Training Schedule Details</h4>
                    </div>
                    <div class="modal-body tmsModalBody">
                        <div class="row tmsModalContent">
                            <asp:UpdatePanel ID="upGvModalGrids" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#Regular_TS">Regular<sup><asp:Label ID="lblRegularRecCount" runat="server"></asp:Label></sup></a></li>
                                        <li><a data-toggle="tab" href="#Retrospective_TS">Retrospective<sup><asp:Label ID="lblRetRecCount" runat="server"></asp:Label></sup></a></li>
                                        <li><a data-toggle="tab" href="#Comments"><i class="fa fa-comments-o" style="font-size: 18px"></i>&nbsp;Comments History</a></li>
                                    </ul>
                                    <div class="tab-content" style="padding-top: 5px">
                                        <div id="Regular_TS" class="tab-pane fade in active">
                                            <div id="row" style="margin: 0px; padding: 3px 10px; overflow: auto; max-height: 400px; min-height: 400px;">
                                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                                    <%-- <asp:UpdatePanel ID="upGvViewTS" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>--%>
                                                    <asp:GridView ID="gvViewTS" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table table-fixed fixview AspGrid"
                                                        EmptyDataText="No Training Schedule for Approval" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <RowStyle CssClass="col-xs-12 padding-none" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S.No." HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- <asp:TemplateField HeaderText="Dept Name" HeaderStyle-Width="250px">
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblEditDeptName" runat="server" Text='<%# Bind("DeptName") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDeptName" runat="server" Text='<%# Bind("DeptName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="Dept Code" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SOP Numbers" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditSopNums" runat="server" Text='<%# Bind("DocumentNums") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSopNums" runat="server" Text='<%# Bind("DocumentNums") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Trainers" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTrainers" runat="server" Text='<%# Bind("Trainers") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrainers" runat="server" Text='<%# Bind("Trainers") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="No.of Days" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditDays" runat="server" Text='<%# Bind("Days") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDays" runat="server" Text='<%# Bind("Days") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <%-- </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="Retrospective_TS" class="tab-pane fade">
                                            <div class="row" style="margin: 0px; padding: 3px 10px; overflow: auto; max-height: 400px; min-height: 400px;">
                                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                                    <%-- <asp:UpdatePanel ID="upGvViewTS" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>--%>
                                                    <asp:GridView ID="gvRet_TS" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table table-fixed fixRetrospective AspGrid"
                                                        EmptyDataText="No Training Schedule for Approval" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <RowStyle CssClass="col-xs-12 padding-none" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S.No." HeaderStyle-CssClass="col-xs-4" ItemStyle-CssClass="col-xs-4">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- <asp:TemplateField HeaderText="Dept Name" HeaderStyle-Width="250px">
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblEditDeptName" runat="server" Text='<%# Bind("DeptName") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDeptName" runat="server" Text='<%# Bind("DeptName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="Dept Code" HeaderStyle-CssClass="col-xs-4" ItemStyle-CssClass="col-xs-4">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SOP Numbers" HeaderStyle-CssClass="col-xs-4" ItemStyle-CssClass="col-xs-4">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditRQ_Sops" runat="server" Text='<%# Bind("RQ_Documents") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRQ_Sops" runat="server" Text='<%# Bind("RQ_Documents") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <%-- </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="Comments" class="tab-pane fade">
                                            <div class="row" style="margin: 0px; padding: 3px 10px; overflow: auto; max-height: 400px; min-height: 400px;">
                                                <%--<asp:UpdatePanel ID="upgvTS_Commentry" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>--%>
                                             
                                                <%-- </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                <asp:UpdatePanel ID="upmodaltxtComments" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <div class="form-horizontal col-sm-4" id="divHodRemarks">
                                            <div class="form-group">
                                                <label class="control-label col-sm-4">HOD Remarks</label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtHOD_Remarks" runat="server" Visible="true" Enabled="false" CssClass="form-control" MaxLength="250" placeholder="HOD Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-horizontal col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label col-sm-4">QA Remarks</label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtRevertReason" runat="server" Visible="true" CssClass="form-control" MaxLength="250" placeholder="Enter QA Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer tmsModalContent">
                        <div class="row" style="text-align: center">

                            <asp:LinkButton ID="lnkAcceptTS" CssClass="btn btn-approve_popup" runat="server" OnClick="lnkAcceptTS_Click">
                                     <table>
                                        <tr>
                                            <td><span class="glyphicon glyphicon-ok InBtn_Icon"></span></td>
                                            <td><span>Accept</span> </td>
                                        </tr>
                                     </table> 
                            </asp:LinkButton>
                            <asp:LinkButton ID="lnkRevertTS" CssClass="btn btn-revert_popup" runat="server" OnClick="lnkRevertTS_Click" OnClientClick="return RevertRequired();"> 
                                    <table>
                                        <tr>
                                            <td> <span class="fa fa-reply InBtn_Icon"></span> </td>
                                            <td><span>Revert</span> </td>
                                        </tr>
                                    </table>
                            </asp:LinkButton>

                            <asp:LinkButton ID="lnkCloseModel" href="#" CssClass="btn btn-canceldms_popup" data-dismiss="modal" runat="server"> 
                                    <table>
                                        <tr>
                                            <td> <span class="glyphicon glyphicon-remove InBtn_Icon"></span> </td>
                                            <td><span>Cancel</span> </td>
                                        </tr>
                                    </table>
                            </asp:LinkButton>
                            <%--<button type="button" data-dismiss="modal" runat="server" id="btnModelClose" class="btn TMS_Button">Close</button>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <%--  <div class="modal fade" id="modelTS_ConfirmRevert" role="dialog">
            <div class="modal-dialog modal-sm">
                <!-- Modal content-->
                <div class="modal-content" style="border-radius: 0px">
                    <div class="modal-header tmsModalHeader">
                        <h4 class="modal-title Panel_Title">Revert Reason</h4>
                    </div>
                    <div class="modal-body tmsModalBody">
                        <div class="row tmsModalContent">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                              <asp:TextBox ID="txtRevertReason" TextMode="MultiLine" Rows="3" CssClass="form-control" runat="server" Wrap="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer tmsModalContent">
                        <div class="row" style="text-align: center">

                            <asp:LinkButton ID="lnkSubmitRevert" CssClass="btn TMS_Button" runat="server" OnClick="lnkSubmitRevert_Click" OnClientClick="javascript:return RevertRequired();">
                                     <table>
                                        <tr>
                                            <td></td>
                                            <td><span>Submit</span> </td>
                                        </tr>
                                     </table> 
                            </asp:LinkButton>

                            <asp:LinkButton ID="lnkCancelRevert" href="#" CssClass="btn TMS_Button" data-dismiss="modal" runat="server"> 
                                    <table>
                                        <tr>
                                            <td></td>
                                            <td><span>Cancel</span> </td>
                                        </tr>
                                    </table>
                            </asp:LinkButton>
                            <button type="button" data-dismiss="modal" runat="server" id="btnModelClose" class="btn TMS_Button">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>
    </div>
    <%--added for electronic Sign--%>
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
    <uc1:SessionTimeOut runat="server" ID="SessionTimeOut" />
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <script>
        function openElectronicSignModal() {
            $('#usES_Modal').modal({ backdrop: 'static', keyboard: false });
        }
        function closeES_ModalWithFailMsg() {
            custAlertMsg("Action already Performed by another user.", "info");
            $('#usES_Modal').modal('hide');
        }
        function closeES_ModalWithRevertMsg() {
            custAlertMsg("Training Schedule Reverted.", "success");
            $('#usES_Modal').modal('hide');
        }
        function closeES_ModalWithApproveMsg() {
            custAlertMsg("Training Schedule Approved.", "success");
            $('#usES_Modal').modal('hide');
        }
    </script>
    <script>
        function OpenModelViewTS() {
            $('#modelTS_View').modal({ backdrop: 'static', keyboard: false });
        }
        //function ConfirmRevert() {
        //    if (confirm("Are you sure, Revert this Training Schedule ?")) {
        //        return true;
        //    } else {
        //        return false;
        //    }
        //}

       <%-- function OpenRevertCommentsModel() {
            document.getElementById("<%=txtRevertReason.ClientID%>").value = "";
            $('#modelTS_ConfirmRevert').modal({ backdrop: 'static', keyboard: false });
        }--%>
        function RevertRequired() {
            if (document.getElementById("<%=txtRevertReason.ClientID%>").value.trim() == "") {
                custAlertMsg("Enter Revert Reason in QA Remarks.", "error","CursorInRevert();");
               
                return false;
            }
        }
        function CursorInRevert() {
            document.getElementById("<%=txtRevertReason.ClientID%>").focus();
        }
        </script>
    <script>
        function ConfirmRevert()  {
            custAlertMsg('Do you want to Revert this Training Schedule ?', 'confirm', true);
                }
    </script>

     <script type="text/javascript">
         $(document).ready(function () {
             // Fix up GridView to support THEAD tags            
             fixGvHeader();
             addThead();
             addTheader();
             addTheadHistory();
             // $("#gvFeedbackHistory").tablesorter({ sortList: [[1, 0]] });
         });
         var prm = Sys.WebForms.PageRequestManager.getInstance();
         prm.add_endRequest(function () {
             fixGvHeader();
             addThead();
             addTheader();
             addTheadHistory();
         });

         function fixGvHeader() {
             var tbl = document.getElementsByClassName("fixHead");  
             if ($(".fixHead").find("thead").length == 0) {
                 $(".fixHead tbody").before("<thead><tr></tr></thead>");
                 $(".fixHead thead tr").append($(".fixHead th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHead tbody tr:first").remove();
                 }               
             }
             
         }
          // Fix up GridView to support THEAD tags 
         function addThead() {
             var tbl = document.getElementsByClassName("fixview"); 
             if ($(".fixview").find("thead").length == 0) {
                 $(".fixview tbody").before("<thead><tr></tr></thead>");
                 $(".fixview thead tr").append($(".fixview th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixview tbody tr:first").remove();
                 }          
             }
         }
          // Fix up GridView to support THEAD tags  
         function addTheader() {
             var tbl = document.getElementsByClassName("fixRetrospective"); 
             if ($(".fixRetrospective").find("thead").length == 0) {
                 $(".fixRetrospective tbody").before("<thead><tr></tr></thead>");
                 $(".fixRetrospective thead tr").append($(".fixRetrospective th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixRetrospective tbody tr:first").remove();
                 }          
             }
         }
          // Fix up GridView to support THEAD tags
         function addTheadHistory() {
             var tbl = document.getElementsByClassName("fixHistory"); 
             if ($(".fixHistory").find("thead").length == 0) {
                 $(".fixHistory tbody").before("<thead><tr></tr></thead>");
                 $(".fixHistory thead tr").append($(".fixHistory th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHistory tbody tr:first").remove();
                 }          
             }
         }
</script>

    <%--    <script>
        //for clearing the Revert Reason modal popup
        $('#<%=lnkCancelRevert.ClientID%>').click(function () {
            $('#<%=txtRevertReason.ClientID%>').val("");
        });
    </script>--%>
</asp:Content>
