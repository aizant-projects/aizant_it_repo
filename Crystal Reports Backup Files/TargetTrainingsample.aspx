﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="TargetTrainingsample.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.SampleforTesting.TargetTrainingsample" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <link href="../../Scripts/bootstrap-multiselect.css" rel="stylesheet" />--%>
    <%-- <script src="../../Scripts/bootstrap-multiselect.js"></script>--%>
    <script type="text/javascript">
        $(function () {
            $('[id*=lstFruits]').multiselect({
                includeSelectAllOption: true
            });
            $("#Button1").click(function () {
                alert($(".multiselect-selected-text").html());
            });
        });
    </script>
    <script src="../../Scripts/select2-4.0.3/bootstrap-select.js"></script>
    <link href="../../Scripts/select2-4.0.3/bootstrap-select.css" rel="stylesheet" />
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class=" col-xs-10 grid_area pull-right" id="paragraph2">
        <div class="col-lg-12 padding-none">
            <div class="col-lg-12 padding-none ">
                <div class="col-lg-12 padding-none">
                    <div class="form-group col-lg-3 padding-none">
                        <div class="col-lg-10 padding-left_div p">
                            <label>Department</label>
                           
                            <asp:DropDownList ID="ddlDepartment" CssClass="selectpicker show-tick form-control" data-live-search="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged1"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group pull-right col-lg-2 padding-none">
                      <%--  <a href="calender.html">
                            <button type="button" class="btn btn-sub pull-right" data-dismiss="modal">Submit</button></a>--%>
                         <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    </div>
                    <div class="form-group col-lg-5 pull-right ">
                        <label>Select SOP</label>
                        <asp:UpdatePanel ID="uplstSops" runat="server">
                            <ContentTemplate>
                                <asp:ListBox ID="lstSops" CssClass="col-lg-12 selectpicker padding-none" multiple data-live-search="true" data-live-search-placeholder="Search" runat="server" SelectionMode="Multiple">
                                </asp:ListBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                       
                    </div>

                    <div class="form-group col-lg-2 padding-right_div pull-right">
                        <div class="col-lg-12 padding-none">
                            <label>Year</label>
                            <select id="basic" class="selectpicker show-tick form-control" data-live-search="true">
                                <option>Year</option>
                                <option>2007-2008</option>
                                <option>2009-2010</option>
                                <option>2012-20013</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 grid_panel_full padding-none">

                <div class="col-lg-12 grid_container">
                    <div class="col-lg-12 grid_panel_full padding-none">

                        <div class="grid_header col-lg-12">Calendar Dashboard</div>
                        <div class="col-lg-12 grid_container">
                           <%-- <table id="example" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>


                                        <th>S NO</th>
                                        <th>SOP</th>
                                        <th>JAN</th>
                                        <th>FEB</th>
                                        <th>MAR</th>
                                        <th>APR</th>
                                        <th>MAY</th>
                                        <th>JUN</th>
                                        <th>JUL</th>
                                        <th>AUG</th>
                                        <th>SEP</th>
                                        <th>OCT</th>
                                        <th>NOV</th>
                                        <th>DEC</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>12</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>

                                        <td>9</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>

                                        <td>8</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>

                                        <td>7</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>

                                        <td>6</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>

                                        <td>5</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>

                                        <td>4</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>

                                        <td>3</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>

                                        <td>1</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>

                                    </tr>
                                    <tr>

                                        <td>1</td>
                                        <td>Information techno</td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                        <td>
                                            <input type="checkbox" /></td>
                                    </tr>
                                </tbody>
                            </table>--%>

                            <asp:GridView ID="GridView1" runat="server">
                                 <Columns>
                                                    <asp:TemplateField HeaderText="SNo">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtRole" runat="server" Text='<%# Bind("Role") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRole" runat="server" Text='<%# Bind("Role") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="SOP" SortExpression="Document" ItemStyle-Width="8%">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtAction" runat="server" Text='<%# Bind("Document") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAction" runat="server" Text='<%# Bind("Document") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="JAN" SortExpression="">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtJAN" runat="server" Text='<%# Bind("JAN") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblJAN" runat="server" Text='<%# Bind("JAN") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="FEB" SortExpression="CommentedDate">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtCommentedDate" runat="server" Text='<%# Bind("CommentedDate") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCommentedDate" runat="server" Text='<%# Bind("CommentedDate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="MAR" SortExpression="Comment">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtComment" runat="server" Text='<%# Bind("Comment") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblComment" runat="server" Text='<%# Bind("Comment") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="APR" SortExpression="">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtJAN" runat="server" Text='<%# Bind("JAN") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblJAN" runat="server" Text='<%# Bind("JAN") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="MAY" SortExpression="CommentedDate">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtCommentedDate" runat="server" Text='<%# Bind("CommentedDate") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCommentedDate" runat="server" Text='<%# Bind("CommentedDate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="MAR" SortExpression="Comment">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtComment" runat="server" Text='<%# Bind("Comment") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblComment" runat="server" Text='<%# Bind("Comment") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                            </asp:GridView>
                        </div>
                        <a href="tms_grid.html">
                            <button type="button" class="pull-right btn btn-cancel_popup" data-dismiss="modal">Cancel</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
	<script>

        $(document).ready(function () {

            $('.closebtn').on('click', function () {

                $('#paragraph2').addClass('col-xs-12');
                $('#paragraph2').removeClass('col-xs-10');
                $('#paragraph1').hide();
                $('.closebtn1').show();
                $('.closebtn').hide();


            });
            $('.closebtn1').on('click', function () {

                $('#paragraph2').addClass('col-xs-10');
                $('#paragraph2').removeClass('col-xs-12');
                $('#paragraph1').show();
                $('.closebtn').show();
                $('.closebtn1').hide();


            });

            $('.btn_assign').on('click', function () {
                $(input, this).removeAttr('checked');
                $(this).removeClass('active');
            });
            var table = $('#example').DataTable({
                fixedHeader: {
                    header: true
                }
            });
            var selector = '.landing_links ul li a';

            $(selector).on('click', function () {
                $(selector).removeClass('active');
                $(this).addClass('active');
            });
        });
</script>
</asp:Content>
