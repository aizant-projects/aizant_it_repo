﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="FormActiveList.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentsList.FormActiveList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
        <div class=" col-md-12 col-lg-12 col-12 col-sm-12  float-left padding-none">
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left" id="divMainContainer" runat="server" visible="true">
                <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-right ">
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-md-12 col-lg-12 col-12 col-sm-12 float-left">Active Forms List</div>
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                            <div class="col-12 float-left padding-none top bottom">
                            <table id="dtDocList" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>VersionID</th>
                                        <th>Form Number</th>
                                        <th>Form Name</th>
                                        <th>Form Description</th>
                                        <th>Version</th>
                                        <th>Department</th>
                                        <th>RefDocNumber</th>
                                        <th>Creator</th>
                                        <th>Approver</th>
                                        <th>Action</th>
                                        <th>View Form</th>
                                    </tr>
                                </thead>
                            </table>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-------- Form VIEW-------------->
    <div id="myFormView" class="modal department fade" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 90%; height: 500px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                   
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpHeading">
                        <ContentTemplate>
                            <span id="span1" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divPDF_Viewer" runat="server">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class=" btn-cancel_popup">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--------End Form VIEW-------------->

    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:UpdatePanel ID="UpGridButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnFormView" runat="server" Text="Submit" Style="display: none" OnClick="btnFormView_Click" OnClientClick="showImg();"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        $('#dtDocList thead tr').clone(true).appendTo('#dtDocList thead');
        $('#dtDocList thead tr:eq(1) th').each(function (i) {
            if (i < 11) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (oTable.column(i).search() !== this.value) {
                        oTable
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDocList').wrap('<div class="dataTables_scroll" />');
        var oTable = $('#dtDocList').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'FormVersionID' },
                { 'data': 'FormNumber' },
                { 'data': 'FormName' },
                { 'data': 'Purpose' },
                { 'data': 'VersionNumber' },
                { 'data': 'dept' },
                { 'data': 'documentNUmber' },
                { 'data': 'CreatedBy' },
                { 'data': 'oldApprovedBY' },
                { 'data': 'ActionName' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="view" title="View" data-toggle="modal" data-target="#myFormView" onclick=ViewForm(' + o.FormVersionID + ')></a>'; }
                },
            ],
            "responsive": true,
            "paging": true,
            "search": {
                "regex": true
            },
            "aoColumnDefs": [{ "targets": [0,1, 4, 7, 10], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0] }, { "className": "dt-body-left", "targets": [2, 3, 4, 6, 8, 9] },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetFormActiveList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    </script>
    <script>
        function ViewForm(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnRV = document.getElementById("<%=btnFormView.ClientID %>");
            btnRV.click();
        }
        function CloseBrowser() {
            window.close();
        }
    </script>
     <script>
         function showImg() {
             ShowPleaseWait('show');
         }
         function hideImg() {
             ShowPleaseWait('hide');
         }
    </script>
</asp:Content>
