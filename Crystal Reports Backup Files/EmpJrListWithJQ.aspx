﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="EmpJrListWithJQ.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Reports.EmpJrListWithJQ" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table id="tblData" class="display" style="width: 100%">
         <thead style="width: 100%">
            <tr>
                <th>JR ID</th>
                <th>JR Name</th>
                <th>JR Version</th>
                <th>Trainee Name</th>
                <th>Current Status</th>
                <th>Designation</th>
                <th>Department</th>
                <th>Current Status</th>
                <th>Edit</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    <script>
        $.ajaxSetup({
            cache: false
        });

        var table = $('#tblData').DataTable({
            columns: [
                { 'data': 'JR_ID' },
                { 'data': 'JR_Name' },
                { 'data': 'JR_Version' },
                { 'data': 'TraineeName' },
                { 'data': 'StatusName' },
                { 'data': 'DesignationName' },
                { 'data': 'DepartmentName' },
                { 'data': 'CurrentStatus' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="Edit" href=<%=ResolveUrl("~/JR/AssignJRtoEmp.aspx")%>?JR_ID=' + o.JR_ID + '>' + '' + '</a>'; }
                }
            ],            
            "pagingType": "simple_numbers",
            "orderClasses": false,
            "order": [[0, "desc"]],
            "info": false,           
            "scrollY": "450px",
            "scrollCollapse": true,  
            "aoColumnDefs": [{ "targets": [3], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [1, 3, 4, 5, 6, 7] }],        
            "bServerSide": true,
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/EmpJrListService.asmx/GetTableData")%>",            
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": "17" });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#tblData").show();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
            //fnDrawCallback: function () {
            //    $('.image-details').bind("click", showDetails);
            //}
        });

        function showDetails() {
            //so something funky with the data
        }
    </script>
</asp:Content>
