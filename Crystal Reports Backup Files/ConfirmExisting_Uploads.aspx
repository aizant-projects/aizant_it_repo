﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="ConfirmExisting_Uploads.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.SetUP.ConfirmExisting_Uploads" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager> 
    <div class=" col-lg-6">
        <div class="closebtn">
            <span>&#9776;
            </span>
        </div>

        <div style="display: none;" class="closebtn1">
            <span>&#9776;</span>
        </div>
    </div> <div class="col-sm-12" style="padding-left: 20%; padding-right: 20%">
        <div class="row Panel_Frame">
            <div id="header">
                <div class="col-lg-12 header_profile">
                    <div class="col-sm-6">Confirm Existing Uploads</div>
                    <div class="col-sm-6">
                        <asp:HiddenField ID="HFDesgID" runat="server" />
                    </div>
                </div>
            </div>

            <div class="modal-body" style="margin-top: 40px">
              <div class="row" style="margin-left: 10px; margin-right: 10px">
                    <div class="form-group">
                                        <%--<label for="inputEmail3" class=" padding-none col-sm-4 control-label custom_label_answer pull-left"><span class="smallred_label">*</span></label>--%>
                        <label for="name" class="col-sm-4 control-label">Department<span class="smallred_label">*</span></label>
                                 
                        <div class="col-sm-7">
                                            <asp:UpdatePanel ID="UPddldepartment" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddldepartment" CssClass="form-control SearchDropDown" runat="server" Width="100%" TabIndex="15">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                </div>
                <div class="row" style="margin-left: 10px; margin-right: 10px; margin-top: 10px">
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">ExistUploads Completed<span class="smallred_label">*</span></label>
                        <div class="col-sm-7">
                           <asp:DropDownList ID="DropDownList2" CssClass="form-control SearchDropDown" Width="100%" runat="server">
                               <asp:ListItem>Select</asp:ListItem>
                               <asp:ListItem Value="1">YES</asp:ListItem>
                               <asp:ListItem Value="2">NA</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left: 10px; margin-right: 10px; margin-top: 10px">
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">Maximum Number<span class="smallred_label">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="name" id="Text1" maxlength="50" runat="server" placeholder="Enter Employee Code" onkeypress="return ValidateAlphaNumericSpace(event)" onpaste="return false" tabindex="1" title="Designation Name" />
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-12 modal-footer Panel_Footer" id="divFooter" runat="server">
                <div class="Panel_Footer_Buttons">
                    <div style="margin-right: 35%; margin-top: 9px">

                        <button type="button" id="btnEmployeeClear" runat="server" class="pull-right btn btn-cancel_popup" tabindex="3" title="click to Clear" onclientclick="javascript:return ResetValues();">Clear</button>
                        <asp:Button ID="btnSubmit" class="pull-right btn-signup_popup" runat="server" Text="Submit" OnClientClick="javascript:return EmployeeRequired();" TabIndex="2" />
                        <%--<asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Update" OnClick="btnUpdate_Click"/>--%>
                    </div>
                </div>
                <div class="Panel_Footer_Message_Label">
                    <asp:Label ID="lblEmployeeError" runat="server" Text="" ForeColor="Red"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
