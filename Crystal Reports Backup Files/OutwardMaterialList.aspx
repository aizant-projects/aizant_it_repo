﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="OutwardMaterialList.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.MaterialRegister.OutwardMaterialList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>

<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">
     <style>
        .select2-dropdown {
            z-index: 100002;
        }

        .modal-dialog modal-sm {
            z-index: 19999 !important;
        }
    </style>
    <script type="text/javascript">
        function Openpopup(popurl) {
            winpops = window.open(popurl, "", "height=600,width=1000,status=yes,location=no,toolbar=no,menubar=no,scrollbars=yes,target=_parent")
        }
    </script>


    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 90%; margin-left: 6%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title" id="ttloutwardCheckin" runat="server">Outward Materials List</span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
            <div id="body">               
                <div runat="server" id="divSearch">
                <div class="Row">
                    <div class="col-sm-6">

                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div5">
                                        <label class="control-label col-sm-4" id="lblDepartment" style="text-align: left; font-weight: lighter">Department</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlDepartment4" CssClass="form-control SearchDropDown" runat="server" data-size="5" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div3">
                                <label class="control-label col-sm-4" id="lblMaterialName" style="text-align: left; font-weight: lighter">Material Name</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtSearchMaterialName" runat="server" CssClass="form-control" placeholder="Enter Material Name"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div2">
                                <label class="control-label col-sm-4" id="lblGP/DC/Invoice No" style="text-align: left; font-weight: lighter">GP/DC/Invoice No</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtSearchMaterialGPDCInvoiceNO" runat="server" CssClass="form-control" placeholder="Enter DC/Invoice No"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div8">
                                <label class="control-label col-sm-4" id="lblPartyName" style="text-align: left; font-weight: lighter">Name of the Party</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtSearchMaterialNameoftheParty" runat="server" CssClass="form-control" placeholder="Enter Name of the Party"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">

                        <asp:UpdatePanel ID="UpdatePanel26" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div1">
                                        <label class="control-label col-sm-4" id="lblMaterialType" style="text-align: left; font-weight: lighter">Material Type</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlSearchMaterialType" CssClass="form-control SearchDropDown" data-size="5" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div7">
                                <label class="control-label col-sm-4" id="lblFromDate" style="text-align: left; font-weight: lighter">From Date</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtSearchMaterialDateFrom" runat="server" CssClass="form-control" placeholder="Select From Date"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div9">
                                <label class="control-label col-sm-4" id="lblToDate" style="text-align: left; font-weight: lighter">To Date</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtSearchMaterialDateTo" runat="server" CssClass="form-control" placeholder="Select To Date"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                    <asp:Button ID="btnMaterialSearchFind" Text="Find" CssClass="button" runat="server" OnClick="btnMaterialSearchFind_Click" ValidationGroup="ab" />
                    <asp:Button ID="btnMaterialSearchReset" Text="Reset" CssClass="button" runat="server" OnClick="btnMaterialSearchReset_Click" CausesValidation="false" />
                </div>
                 </div>
            </div>


            <div class="clearfix">
            </div>
            <div id="divGridViewOM" runat="server">
            <asp:GridView ID="GridViewOutwardMaterial" runat="server" CssClass="table table-bordered table-inverse" PagerStyle-CssClass="GridPager" Style="text-align: center" text-align="Center" OnRowDataBound="GridViewOutwardMaterial_RowDataBound" HorizontalAlign="Center" DataKeyNames="Sno" Font-Size="11px" HeaderStyle-Wrap="false" AutoGenerateColumns="False" HeaderStyle-Width="5%" EmptyDataText="Currently No Records Found" CellPadding="3" HeaderStyle-HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" AllowPaging="True" ForeColor="#66CCFF" OnPageIndexChanging="GridViewOutwardMaterial_PageIndexChanging">
                <EmptyDataRowStyle ForeColor="#FF3300" HorizontalAlign="Center" />
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#748CB2" Font-Bold="True" ForeColor="White" Height="30px" />
                <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="White" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#0c99f0" />
                <SortedAscendingHeaderStyle BackColor="#0c99f0" />
                <SortedDescendingCellStyle BackColor="#0c99f0" />
                <SortedDescendingHeaderStyle BackColor="#0c99f0" />
                <Columns>
                    <asp:TemplateField HeaderText="SNO" SortExpression="Sno">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVMSno" runat="server" Text='<%# Bind("Sno") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGOMSno" runat="server" Text='<%# Bind("Sno") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="MATERIAL TYPE" SortExpression="OutwardMaterialType">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVMaterialType" runat="server" Text='<%# Bind("OutwardMaterialType") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGMaterialType" runat="server" Text='<%# Bind("OutwardMaterialType") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MATERIAL NAME" SortExpression="MaterialName">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVMaterialName" runat="server" Text='<%# Bind("MaterialName") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGMaterialName" runat="server" Text='<%# Bind("MaterialName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Quantityu" HeaderText="QUANTITY" SortExpression="Quantityu" />
                    <asp:BoundField DataField="Department" HeaderText="DEPARTMENT" SortExpression="Department" />
                    <asp:BoundField DataField="GivenBy" HeaderText="GIVEN BY" SortExpression="GivenBy" />
                    <asp:BoundField DataField="VechileNo" HeaderText="VECHILE NUMBER" SortExpression="VechileNo" />
                    <asp:BoundField DataField="GPorDCPNo" HeaderText="GP/DC:NO" SortExpression="GPorDCPNo" />
                    <asp:BoundField DataField="NameOfParty" HeaderText="NAME OF THE PARTY" SortExpression="NameOfParty" />
                    <asp:BoundField DataField="OutDateTime" HeaderText="PREPARED ON" SortExpression="OutDateTime" />
                    <asp:BoundField DataField="Return_Quantityu" HeaderText="RETURN QUANTITY" SortExpression="Return_Quantityu" />
                    <asp:BoundField DataField="StatusName" HeaderText="HOD STATUS" SortExpression="StatusName" />
                    <asp:BoundField DataField="StoreStatusName" HeaderText="STORE STATUS" SortExpression="StoreStatusName" />
                                        <asp:TemplateField HeaderText="RETURN INDATE" SortExpression="Material_ReturnDateAndTime">
                        <ItemTemplate>
                            <asp:Label ID="lblGVReturnDateTime" runat="server" Text='<%# Bind("Material_ReturnDateAndTime") %>'></asp:Label>
                            <asp:LinkButton ID="lnkCheckOut" runat="server" OnClick="lnkCheckOut_Click" ForeColor="#ff0000" Visible="false">Check-In</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PRINT">
                        <ItemTemplate>
                            <a href='javascript:Openpopup("<%= ResolveUrl("~/VMS/Reports/Outward.aspx") +"?OutwardID=" %>"+ "<%# Eval("Sno") %>")'>
                                <asp:Label ID="Label13" runat="server" Style="font-size: 95%" CssClass="glyphicon glyphicon-print"></asp:Label></a>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EDIT">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkOutWard" runat="server" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" OnClick="lnkOutWard_Click"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            <!--Panel to add new record-->
            <asp:ModalPopupExtender ID="ModalPopupOutward" runat="server" TargetControlID="lbtPop" PopupControlID="panelOutward" RepositionMode="RepositionOnWindowResizeAndScroll" DropShadow="true" PopupDragHandleControlID="panelAddNewTitle" BackgroundCssClass="modalBackground"></asp:ModalPopupExtender>
            <asp:Panel ID="panelOutward" runat="server" Style="display: none; background-color: white;" ForeColor="Black" Width="960px" Height="610px">
                <asp:Panel ID="panel2" runat="server" Style="cursor: move; font-family: Tahoma; padding: 9px;" HorizontalAlign="Center" BackColor="#748CB2" BorderColor="#000000" Font-Bold="true" ForeColor="White" Height="35px">
                    <%--<b>Outward Material List </b>--%>
                    <span runat="server" id="ttlpopup">Modify Outward Material Check In</span>
                </asp:Panel>
                <asp:LinkButton ID="lbtPop" runat="server"></asp:LinkButton>
                <div class="Row">
                    <div class="col-sm-6">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div19">
                                        <label class="control-label col-sm-4" id="lblSno" style="text-align: left; font-weight: lighter">SNo</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtOutwardEditSno" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div4">
                                        <label class="control-label col-sm-4" id="lblMaterialTypeEdit" style="text-align: left; font-weight: lighter">Material Type<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlOutwardEditMaterialType" CssClass="form-control" data-size="5" AutoPostBack="true" runat="server"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Select Material Type" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlOutwardEditMaterialType" InitialValue="0" ValidationGroup="Outward"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div6">
                                <label class="control-label col-sm-4" id="lblMaterialNameEdit" style="text-align: left; font-weight: lighter">Material Name<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtOutwardEditMaterialName" runat="server" CssClass="form-control" placeholder="Enter Material Name"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Enter Material Name" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtOutwardEditMaterialName" ValidationGroup="Outward"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div10">
                                <label class="control-label col-sm-4" id="lblDes" style="text-align: left; font-weight: lighter">Description of the Material</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtDescriptionofMat" runat="server" CssClass="form-control" placeholder="Enter Material Descrption" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div11">
                                        <label class="control-label col-sm-4" id="lblQuantity" style="text-align: left; font-weight: lighter">Quantity<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtOutwardEditQuantity" runat="server" CssClass="form-control" placeholder="Quantity" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter Quantity" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtOutwardEditQuantity" ValidationGroup="Outward"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="col-sm-1" style="padding-left: 0px; padding-top: 5px">
                                            <label id="lblUnits" style="text-align: left; font-weight: lighter">Units<span class="mandatoryStar">*</span></label>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlUnits" runat="server" CssClass="form-control" data-size="5" AutoPostBack="true"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Select Units" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlUnits" InitialValue="0" ValidationGroup="Outward"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div12">
                                <label class="control-label col-sm-4" id="lblGP" style="text-align: left; font-weight: lighter">GP/DC No</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtOutwardEditGPDCPNo" runat="server" CssClass="form-control" placeholder="Enter GP/DC No"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div13">
                                <label class="control-label col-sm-4" id="lblVechileNo" style="text-align: left; font-weight: lighter">Vechile No</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtOutwardEditVechileNo" runat="server" CssClass="form-control" placeholder="Enter Vechile No"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanelOutward" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div14">
                                        <label class="control-label col-sm-4" id="lblDepartmentEdit" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlOutwardEditDepartment" runat="server" ClientIDMode="Static" CssClass="form-control" data-size="5" AutoPostBack="true" OnSelectedIndexChanged="ddlOutwardEditDepartment_SelectedIndexChanged1"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Select Department" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlOutwardEditDepartment" InitialValue="0" ValidationGroup="Outward"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlOutwardEditDepartment" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div15">
                                        <label class="control-label col-sm-4" id="lblGivenby" style="text-align: left; font-weight: lighter">GivenBy<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlGivenBy" runat="server" ClientIDMode="Static" CssClass="form-control" data-size="5" AutoPostBack="true"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Select Given By" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlGivenBy" InitialValue="0" ValidationGroup="Outward"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlGivenBy" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div16">
                                <label class="control-label col-sm-4" id="lblPartyNameEdit" style="text-align: left; font-weight: lighter">Name of the Party<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtOutwardEditNameoftheParty" runat="server" CssClass="form-control" placeholder="Enter Name of the Party" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Enter Name of the Party" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtOutwardEditNameoftheParty" ValidationGroup="Outward"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div17">
                                <label class="control-label col-sm-4" id="lblOutDateTime" style="text-align: left; font-weight: lighter">Created On</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtOutDate" runat="server" CssClass="form-control" BackColor="White" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div22">
                                <label class="control-label col-sm-4" id="lblExDateTime" style="text-align: left; font-weight: lighter">Expected ReturnDate</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtExpReturnDate" runat="server" Height="32px" CssClass="form-control" Style="margin-top: 3%" placeholder="Select ExpectedDate"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div23">
                                        <label class="control-label col-sm-4" id="lblRecDepartment" style="text-align: left; font-weight: lighter">ReceivedBy Department<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlReceivedByDept" runat="server" ClientIDMode="Static" Height="32px" CssClass="form-control" data-size="5" AutoPostBack="true" OnSelectedIndexChanged="ddlReceivedByDept_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Select Received Department" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlReceivedByDept" InitialValue="0" ValidationGroup="Outward"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlReceivedByDept" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div24">
                                        <label class="control-label col-sm-4" id="lblReceivedBy" style="text-align: left; font-weight: lighter">ReceivedBy<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlReceivedBy" runat="server" Height="32px" ClientIDMode="Static" CssClass="form-control" data-size="5" AutoPostBack="true"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Select Recevied By" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlReceivedBy" InitialValue="0" ValidationGroup="Outward"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlReceivedBy" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div21">
                                        <label class="control-label col-sm-4" id="lblReturnQuantity" style="text-align: left; font-weight: lighter">Return Quantity<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtOutwardEditReturnQuantity" runat="server" CssClass="form-control" placeholder="Quantity" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Enter Quantity" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtOutwardEditReturnQuantity" ValidationGroup="Outward"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="CVReturnQuantity" runat="server" ErrorMessage="Invalid Quantity" Display="Dynamic" Font-Size="x-Small" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtOutwardEditReturnQuantity" ControlToCompare="txtOutwardEditQuantity" ValidationGroup="Outward" Operator="LessThanEqual" Type="Double"></asp:CompareValidator>
                                        </div>
                                        <div class="col-sm-1" style="padding-left: 0px; padding-top: 5px">
                                            <label id="lblReturnUnits" style="text-align: left; font-weight: lighter">Units<span class="mandatoryStar">*</span></label>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlUnitsReturn" runat="server" CssClass="form-control" data-size="5" AutoPostBack="true"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Select Units" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlUnitsReturn" InitialValue="0" ValidationGroup="Outward"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div18">
                                <label class="control-label col-sm-4" id="lblRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtRemarks_MO" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div20">
                                <label class="control-label col-sm-4" id="lblComments" style="text-align: left; font-weight: lighter">Comments<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtOutwardEditedComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Enter Comments" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtOutwardEditedComments" ValidationGroup="Outward"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                </div>
                <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                    <asp:Button ID="btnOutwardUpdate" runat="server" CssClass="button" Text="Update" OnClick="btnOutwardUpdate_Click" UseSubmitBehavior="false" ValidationGroup="Outward" />
                    <asp:Button ID="btnApprove" Text="Approve" CssClass="button" runat="server" OnClick="btnApprove_Click" Visible="false"/>
                    <asp:Button ID="btnReject" Text="Revert" CssClass="button" runat="server" OnClick="btnReject_Click" Visible="false"/>
                    <asp:Button ID="btnStoreApprove" Text="Approve" CssClass="button" runat="server" OnClick="btnStoreApprove_Click" Visible="false"/>
                    <asp:Button ID="btnStoreReject" Text="Revert" CssClass="button" runat="server" OnClick="btnStoreReject_Click" Visible="false"/>
                    <asp:Button ID="btnOutwardCancel" runat="server" CssClass="button" Text="Close" CausesValidation="false" />
                </div>
            </asp:Panel>
           
            <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
            </div>
            <script>
                //for start date and end date validations startdate should be < enddate
                $(function () {
                    $('#<%=txtSearchMaterialDateFrom.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                    });
                    $('#<%=txtSearchMaterialDateTo.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        minDate: startdate,
                        useCurrent: false,
                    });
                    $('#<%=txtSearchMaterialDateFrom.ClientID%>').on("dp.change", function (e) {
                        $('#<%=txtSearchMaterialDateTo.ClientID%>').data("DateTimePicker").minDate(e.date);
                        $('#<%=txtSearchMaterialDateTo.ClientID%>').val("");
                    });
                    $('#<%=txtSearchMaterialDateTo.ClientID%>').on("dp.change", function (e) {
                    });
                });
            </script>

            <script type="text/javascript">
                //for datepickers
                var startdate2 = document.getElementById('<%=txtSearchMaterialDateFrom.ClientID%>').value
                if (startdate2 == "") {
                    $('#<%=txtSearchMaterialDateFrom.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                    });
                    $('#<%=txtSearchMaterialDateTo.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                    });
                }
            </script>
            
            <script>
                $("#NavLnkMaterial").attr("class", "active");
                $("#OutwardMaterialList").attr("class", "active");
    </script>

            <script type="text/javascript">
                function Submitvalidate() {
                    var MType = document.getElementById("<%=ddlOutwardEditMaterialType.ClientID%>").value;
                    var MName = document.getElementById("<%=txtOutwardEditMaterialName.ClientID%>").value;
                    var MDept = document.getElementById("<%=ddlOutwardEditDepartment.ClientID%>").value;
                    var GivenBy = document.getElementById("<%=ddlGivenBy.ClientID%>").value;
                    var Quantity = document.getElementById("<%=txtOutwardEditQuantity.ClientID%>").value;
                     var PartyName = document.getElementById("<%=txtOutwardEditNameoftheParty.ClientID%>").value;
                     var MUnits = document.getElementById("<%=ddlUnits.ClientID%>").value;
                      var ReturnQuantity = document.getElementById("<%=txtOutwardEditReturnQuantity.ClientID%>").value;
                      var MReturnUnits = document.getElementById("<%=ddlUnitsReturn.ClientID%>").value;
                    errors = [];
                    if (MType == 0) {
                        errors.push("Please Select Material Type.");
                    }
                    if (MName == "") {
                        errors.push("Please Enter Material Name.");
                    }
                    if (GivenBy == "0") {
                        errors.push("Please Select Given By.");
                    }
                    if (Quantity == "") {
                        errors.push("Please Enter Material Quantity.");
                    }
                    if (PartyName == "") {
                        errors.push("Please Enter Name of the Party.");
                    }
                    if (MDept == 0) {
                        errors.push("Please Select Department.");
                    }
                    if (MUnits == 0) {
                        errors.push("Please Select Units of Material.");
                    }
                    if (ReturnQuantity == "") {
                        errors.push("Please Enter Material Return Quantity.");
                    }
                    if (MReturnUnits == 0) {
                        errors.push("Please Select Return Quantity Units of Material.");
                    }
                    if (errors.length > 0) {
                        custAlertMsg(errors.join("<br/>"), "error");
                        return false;
                    }
                }
            </script>
            <script>
                $(function () {
                    $('#<%=txtExpReturnDate.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        minDate: new Date('<%=txtExpReturnDate.Text%>'),
                        useCurrent: false,
                    });
                });
            </script>

            <script>
                function openElectronicSignModal() {
                    $('#usES_Modal').modal({ backdrop: 'static', keyboard: false });
                }
            </script>

        </div>
    </div>
</asp:Content>
