﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="DocumentDetails.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentCreator.DocumentDetails" EnableEventValidation="false" ValidateRequest="false" %>
<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>
<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<%@ Register Src="~/UserControls/SessionTimeOut.ascx" TagPrefix="uc1" TagName="SessionTimeOut" %>
<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%=ResolveUrl("~/AppCSS/DMS/DMS_StyleSheet1.css")%>" rel="stylesheet" />
    <style>
        table tbody tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            padding: 4px;
            border-right: 1px solid #fff;
        }
        

        .dxreView dxreInFocus {
            height: 500px !important;
        }

        .dxrePage {
            width: 901px !important;
            height: 1123px !important;
        }

        .dxreControl .dxreView {
            background-color: #E0E0E0;
            height: 436px !important;
        }

        .dxreView {
            position: relative;
            overflow: auto;
            padding: 0 10px;
            -webkit-overflow-scrolling: touch;
            -webkit-appearance: none;
            -ms-touch-action: pan-y pinch-zoom;
            touch-action: pan-y pinch-zoom;
            height: 455px !important;
        }

        .CreatorGrid thead tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            padding: 4px;
            border-right: 1px solid #fff;
        }

        table tbody tr td {
            text-align: center;
            height: 30px;
            
        }

        .CreatorGrid tbody tr td {
            text-align: center;
            height: 30px;
            width: 20px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            background-color: #07889a !important;
        }

        .richText .richText-help {
            display: none !important;
        }

        .datalistcss {
            overflow-x: hidden;
            height: 180px;
            border-color: #CCCCCC;
            border-width: 1px;
            border-style: Solid;
            border-collapse: collapse;
            padding: 5px;
            border-radius: 5px;
        }

        .header_profile {
            background: #07889a;
            padding: 10px;
            color: #fff;
            font-size: 12pt;
            font-family: seguisb;
        }
    </style>
    <script>
        function Required() {
            var file = document.getElementById("<%=file_word.ClientID%>").value;
            errors = [];
            if (file == "") {
                errors.push("Please Upload file");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <script>
        function Required1() {
            var file = document.getElementById("<%=FileUpload1.ClientID%>").value;
            errors = [];
            if (file == "") {
                errors.push("Please Upload file");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            $('.rtxtcontent').richText({
                // text formatting
                bold: true,
                italic: true,
                underline: true,
                // text alignment
                leftAlign: true,
                centerAlign: true,
                rightAlign: false,
                // lists
                ol: false,
                ul: false,
                // title
                heading: true,
                // colors
                fontColor: false,
                // uploads
                imageUpload: false,
                fileUpload: false,
                //videoUpload: false,
                // media
                videoEmbed: false,
                // link
                urls: true,
                // tables
                table: false,
                // code
                removeStyles: true,
                code: false,
                // colors
                colors: [],
                // dropdowns
                fileHTML: '',
                imageHTML: '',
                //videoHTML: '',
                // translations
                translations: {
                    'title': 'Title',
                    'white': 'White',
                    'black': 'Black',
                    'brown': 'Brown',
                    'beige': 'Beige',
                    'darkBlue': 'Dark Blue',
                    'blue': 'Blue',
                    'lightBlue': 'Light Blue',
                    'darkRed': 'Dark Red',
                    'red': 'Red',
                    'darkGreen': 'Dark Green',
                    'green': 'Green',
                    'purple': 'Purple',
                    'darkTurquois': 'Dark Turquois',
                    'turquois': 'Turquois',
                    'darkOrange': 'Dark Orange',
                    'orange': 'Orange',
                    'yellow': 'Yellow',
                    'imageURL': 'Image URL',
                    'fileURL': 'File URL',
                    'linkText': 'Link text',
                    'url': 'URL',
                    'size': 'Size',
                    'responsive': '<a href="https://www.jqueryscript.net/tags.php?/Responsive/">Responsive</a>',
                    'text': 'Text',
                    'openIn': 'Open in',
                    'sameTab': 'Same tab',
                    'newTab': 'New tab',
                    'align': 'Align',
                    'left': 'Left',
                    'center': 'Center',
                    'right': 'Right',
                    'rows': 'Rows',
                    'columns': 'Columns',
                    'add': 'Add',
                    'pleaseEnterURL': 'Please enter an URL',
                    'videoURLnotSupported': 'Video URL not supported',
                    'pleaseSelectImage': 'Please select an image',
                    'pleaseSelectFile': 'Please select a file',
                    'bold': 'Bold',
                    'italic': 'Italic',
                    'underline': 'Underline',
                    'alignLeft': 'Align left',
                    'alignCenter': 'Align centered',
                    'alignRight': 'Align right',
                    'addOrderedList': 'Add ordered list',
                    'addUnorderedList': 'Add unordered list',
                    'addHeading': 'Add Heading/title',
                    'addFont': 'Add font',
                    'addFontColor': 'Add font color',
                    'addFontSize': 'Add font size',
                    'addImage': 'Add image',
                    'addVideo': 'Add video',
                    'addFile': 'Add file',
                    'addURL': 'Add URL',
                    'addTable': 'Add table',
                    'removeStyles': 'Remove styles',
                    'code': 'Show HTML code',
                    'undo': 'Undo',
                    'redo': 'Redo',
                    'close': 'Close'
                },
                // developer settings
                useSingleQuotes: false,
                height: 0,
                heightPercentage: 0,
                id: "",
                class: "",
                useParagraph: false
            });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(document).ready(function () {
                $('.rtxtcontent').richText({
                    // text formatting
                    bold: true,
                    italic: true,
                    underline: true,
                    // text alignment
                    leftAlign: true,
                    centerAlign: true,
                    rightAlign: false,
                    // lists
                    ol: false,
                    ul: false,
                    // title
                    heading: true,
                    // colors
                    fontColor: false,
                    // uploads
                    imageUpload: false,
                    fileUpload: false,
                    //videoUpload: false,
                    // media
                    videoEmbed: false,
                    // link
                    urls: true,
                    // tables
                    table: false,
                    // code
                    removeStyles: true,
                    code: false,
                    // colors
                    colors: [],
                    // dropdowns
                    fileHTML: '',
                    imageHTML: '',
                    //videoHTML: '',                        
                    // translations
                    translations: {
                        'title': 'Title',
                        'white': 'White',
                        'black': 'Black',
                        'brown': 'Brown',
                        'beige': 'Beige',
                        'darkBlue': 'Dark Blue',
                        'blue': 'Blue',
                        'lightBlue': 'Light Blue',
                        'darkRed': 'Dark Red',
                        'red': 'Red',
                        'darkGreen': 'Dark Green',
                        'green': 'Green',
                        'purple': 'Purple',
                        'darkTurquois': 'Dark Turquois',
                        'turquois': 'Turquois',
                        'darkOrange': 'Dark Orange',
                        'orange': 'Orange',
                        'yellow': 'Yellow',
                        'imageURL': 'Image URL',
                        'fileURL': 'File URL',
                        'linkText': 'Link text',
                        'url': 'URL',
                        'size': 'Size',
                        'responsive': '<a href="https://www.jqueryscript.net/tags.php?/Responsive/">Responsive</a>',
                        'text': 'Text',
                        'openIn': 'Open in',
                        'sameTab': 'Same tab',
                        'newTab': 'New tab',
                        'align': 'Align',
                        'left': 'Left',
                        'center': 'Center',
                        'right': 'Right',
                        'rows': 'Rows',
                        'columns': 'Columns',
                        'add': 'Add',
                        'pleaseEnterURL': 'Please enter an URL',
                        'videoURLnotSupported': 'Video URL not supported',
                        'pleaseSelectImage': 'Please select an image',
                        'pleaseSelectFile': 'Please select a file',
                        'bold': 'Bold',
                        'italic': 'Italic',
                        'underline': 'Underline',
                        'alignLeft': 'Align left',
                        'alignCenter': 'Align centered',
                        'alignRight': 'Align right',
                        'addOrderedList': 'Add ordered list',
                        'addUnorderedList': 'Add unordered list',
                        'addHeading': 'Add Heading/title',
                        'addFont': 'Add font',
                        'addFontColor': 'Add font color',
                        'addFontSize': 'Add font size',
                        'addImage': 'Add image',
                        'addVideo': 'Add video',
                        'addFile': 'Add file',
                        'addURL': 'Add URL',
                        'addTable': 'Add table',
                        'removeStyles': 'Remove styles',
                        'code': 'Show HTML code',
                        'undo': 'Undo',
                        'redo': 'Redo',
                        'close': 'Close'
                    },
                    // developer settings
                    useSingleQuotes: false,
                    height: 0,
                    heightPercentage: 0,
                    id: "",
                    class: "",
                    useParagraph: false
                });
            });
        });
    </script>
    <script>
            function closeReffers() {
                $("#myModal_Refferal").modal('hide');
            }
            function ShowRefferal() {
                $('#myModal_Refferal').modal({ show: 'true', backdrop: 'static', keyboard: false });
                TabRefClick();
            }
            function ShowDocCreate() {
                $('#myDocCreate').modal({ show: true, backdrop: 'static', keyboard: false });
            }
            function ShowFileUpload() {
                $('#myFileUpload').modal({ show: 'true', backdrop: 'static', keyboard: false });
            }
            function ShowExternalFile() {
                $('#mpop_FileViewer').modal({ show: 'true', backdrop: 'static', keyboard: false });
            }
            function ShowDocDetails1() {
                $('#myFileUpload').modal({ show: 'true', backdrop: 'static', keyboard: false });
            }
            function ShowRefferal1() {
                $('#myModal_Refferal').modal({ show: 'true', backdrop: 'static', keyboard: false });
            }
            function HidePopup() {
                $("#myDocCreate").modal('hide');
                $('#usES_Modal').modal('hide');
            }
            function ReloadCreatorPage() {
                var Tvalue = document.getElementById("<%=hdnFieldFrom.ClientID%>").value;
                if (Tvalue == "1") {
                    window.open("<%=ResolveUrl("~/DMS/DocumentCreator/Create_Document.aspx?tab=1")%>", "_self");
                }
                if (Tvalue == "2") {
                    window.open("<%=ResolveUrl("~/DMS/DocumentCreator/Create_Document.aspx?tab=2")%>", "_self");
                }
            }
    </script>
    <script>
            function ConfirmDelete(_commandArg) {
                custAlertMsg('Do You Want Delete This Document', 'confirm', true);
            }
            function ConfirmAlert() {
                document.getElementById("<%=hdfConfirm.ClientID%>").value = 'Save';
                custAlertMsg('Do You Want Save', 'confirm', true);
            }
            function ConfirmAlert1() {
                document.getElementById("<%=hdfConfirm.ClientID%>").value = 'Submit';
                custAlertMsg('Have you completed the Spell and Grammer check for the document', 'confirm', true);
            }
            function SaveYes() {
                $("#myDocDetails").modal({ show: 'true', backdrop: 'static', keyboard: false });
                $("#myDocCreate").modal('hide');
            }
    </script>
    <script>      
            function GridViewSearch(phrase, _id) {
                var words = phrase.value.toLowerCase().split(" ");
                var table = document.getElementById(_id);
                var ele;
                for (var r = 1; r < table.rows.length; r++) {
                    ele = table.rows[r].innerHTML.replace(/<[^>]+>/g, "");
                    var displayStyle = 'none';
                    for (var i = 0; i < words.length; i++) {
                        if (ele.toLowerCase().indexOf(words[i]) >= 0)
                            displayStyle = '';
                        else {
                            displayStyle = 'none';
                            break;
                        }
                    }
                    table.rows[r].style.display = displayStyle;
                }
            }
    </script>
    <script>
            $(document).ready(function () {
                ReferalsTabState();
                TabRefClick();
            });
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                ReferalsTabState();
                TabRefClick();
            });
            function ReferalsTabState() {
                if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == 'Link') {
                    $('.internaltab').removeClass('active');
                    $('.Externaltab').removeClass('active');
                    $('.Referralstab').addClass('active');
                    $('.Externaltabbody').removeClass('active');
                    $('.Externaltabbody').removeClass('in');
                    $('.internaltabbody').removeClass('active');
                    $('.internaltabbody').removeClass('in');
                    $('.Referralstabbody').addClass('active');
                    $('.Referralstabbody').addClass('in');
                }
                if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == 'Internal') {
                    $('.Externaltab').removeClass('active');
                    $('.Referralstab').removeClass('active');
                    $('.internaltab').addClass('active');
                    $('.Externaltabbody').removeClass('active');
                    $('.Externaltabbody').removeClass('in');
                    $('.Referralstabbody').removeClass('active');
                    $('.Referralstabbody').removeClass('in');
                    $('.internaltabbody').addClass('active');
                    $('.internaltabbody').addClass('in');
                }
                if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == 'External') {
                    $('.internaltab').removeClass('active');
                    $('.Referralstab').removeClass('active');
                    $('.Externaltab').addClass('active');
                    $('.internaltabbody').removeClass('active');
                    $('.internaltabbody').removeClass('in');
                    $('.Referralstabbody').removeClass('active');
                    $('.Referralstabbody').removeClass('in');
                    $('.Externaltabbody').addClass('active');
                    $('.Externaltabbody').addClass('in');
                }
            }
    </script>
    <script>
            function TabRefClick() {
                $(".Referralstab").click(function () {
                    $('#<%=hdnGrdiReferesh.ClientID%>').val('Link');
                });
                $(".internaltab").click(function () {
                    $('#<%=hdnGrdiReferesh.ClientID%>').val('Internal');
                });
                $(".Externaltab").click(function () {
                    $('#<%=hdnGrdiReferesh.ClientID%>').val('External');
                });
            }
    </script>
    <script>
            function openElectronicSignModal() {
                $('#usES_Modal').modal({ backdrop: 'static', keyboard: false });
                $('#myDocCreate').modal({ show: 'true', backdrop: 'static', keyboard: false });
            }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnFieldFrom" runat="server" Value="1" />
            <asp:HiddenField ID="hdnNewDocTab" runat="server" Value="NEWTab" />
            <asp:HiddenField ID="hdnGrdiReferesh" runat="server" Value="Link" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class=" col-lg-6 padding-none">
        <div class="closebtn">
            <span>&#9776;
            </span>
        </div>
        <div style="display: none;" class="closebtn1">
            <span>&#9776;</span>
        </div>
    </div>
    <div class=" col-lg-6">
        <asp:Button ID="btnAdd" runat="server" class="pull-right btn btn-dashboard" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 dms_outer_border padding-none" id="divMainContainer" runat="server" visible="true">
        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none top" style="background-color: #fff; overflow-y: auto">
            <asp:UpdatePanel ID="UpMyDocFiles" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">
                        <div class="form-group  col-xs-3 ">
                            <asp:Label ID="lbl_requestType" runat="server" Text="Request Type"></asp:Label>
                            <asp:TextBox ID="txt_requestType" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div id="type1" runat="server" class="form-group  col-xs-3">
                            <asp:Label ID="Label2" runat="server" Text="Document Type"></asp:Label>
                            <asp:TextBox ID="txt_documentType" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="form-group  col-xs-3 " id="div_dept1" runat="server">
                            <asp:Label ID="Label5" runat="server" Text="Department"></asp:Label>
                            <asp:TextBox ID="txt_Department" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="form-group col-xs-3 " id="DocumentNumber" runat="server">
                            <asp:Label ID="Label3" runat="server" Text="Document Number"></asp:Label>
                            <asp:TextBox ID="txt_DocumentNumber" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-4 " id="div3" runat="server" visible="false">
                        </div>
                        <div id="div_Namenew1" class="col-lg-6 form-group" runat="server">
                            <asp:Label ID="Label4" runat="server" Text="Document Name"></asp:Label>
                            <asp:TextBox ID="txt_DocumentName" runat="server" TextMode="MultiLine" CssClass="form-control" Height="80px" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div id="div_Purpose" runat="server" class="col-lg-6 form-group">
                            <asp:Label ID="lblComments" runat="server" Text=""></asp:Label>
                            <asp:TextBox ID="txt_Comments" runat="server" TextMode="MultiLine" CssClass="form-control" Width="100%" Height="80px" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="form-group  col-lg-3 pull-left" id="div_Version" runat="server">
                            <asp:Label ID="Label9" runat="server" Text="Version"></asp:Label>
                            <asp:TextBox ID="txt_VersionID" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div id="div_Userroles11" runat="server" class="col-lg-3 form-group">
                            <asp:Label ID="Label7" runat="server" Text="Author"></asp:Label>
                            <asp:TextBox ID="txt_Author" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div id="div1" runat="server" class="col-lg-3 form-group">
                            <asp:Label ID="Label8" runat="server" Text="Authorizer"></asp:Label>
                            <asp:TextBox ID="txt_Authorizer" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="form-group col-lg-3" id="div_template">
                            <asp:Label ID="lbl_Template" runat="server" Text="Add Template" Font-Bold="true" Visible="false"></asp:Label>
                            <asp:DropDownList ID="ddl_Template" runat="server" CssClass="col-md-12 col-lg-12 col-xs-12 col-sm-12 selectpicker drop_down padding-none " data-live-search="true" Visible="false"></asp:DropDownList>
                        </div>
                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 form-group">
                            <asp:UpdatePanel ID="UPgvApprover" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="panel-heading_title col-md-12 col-lg-12 col-xs-12 col-sm-12 pull-left">
                                        <h4 class="panel-title"><a>Selected Employees</a></h4>
                                    </div>
                                    <asp:GridView ID="gvApprover" CssClass="padding-right_div col-md-12 col-lg-12 col-xs-12 col-sm-12 top bottom cal" runat="server" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundField DataField="Role" HeaderText="Role" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                            <asp:BoundField DataField="EmpName" HeaderText="Approver" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                            <asp:BoundField DataField="DeptCode" HeaderText="DeptCode" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="form-group input-group col-lg-10 col-lg-offset-2" style="padding-top: 30px; color: darkslateblue;" id="WordUpload" visible="false" runat="server">
                        <div class="col-lg-8">
                            To Upload Original Word File Of This Document :
                                    <asp:LinkButton ID="lnk_Word" runat="server" CssClass="btn btn-link" Font-Underline="true" Text="Click Here" OnClick="lnk_Word_Click" Font-Bold="true"></asp:LinkButton>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="lnk_Word" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 pull-left top" id="divgvcomments" runat="server" style="">
                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 bottom padding-none" style="border: 1px solid #07889a; margin-top: -10px; max-height: 100px; min-height: 100px; overflow-y: auto">
                            <asp:GridView ID="gv_rejectComments" AllowPaging="true" Width="100%" OnPageIndexChanging="gv_rejectComments_PageIndexChanging" PageSize="1" Border="0" runat="server" EmptyDataText="No Records Found" AutoGenerateColumns="false" PagerSettings-Mode="NumericFirstLast">
                                <Columns>
                                    <asp:TemplateField HeaderText="Comments">
                                        <ItemTemplate>
                                            <div style="text-align: left !important;">
                                                <asp:Literal ID="lt1_Reject" runat="server" Text='<%# Eval("Comments") %>'></asp:Literal>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 bottom">
                <asp:UpdatePanel ID="asdqwe" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <button type="button" data-dismiss="modal" runat="server" id="Button1" class="pull-right btn  btn-canceldms_popup" style="margin-left: 5px !important">Cancel</button>
                        <asp:Button ID="btn_Submit" runat="server" CssClass="pull-right btn btn-approve_popup" Text="Create" OnClick="btn_Submit_Click" />
                        <asp:Button ID="btn_Addreferrals" runat="server" CssClass="pull-left btn btn-revert_popup" Text="Referrals" OnClick="btn_Addreferrals_Click" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btn_Submit" />
                        <asp:PostBackTrigger ControlID="btn_Addreferrals" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
       
    </div>
    <!--------Document Create View-------------->
    <div id="myDocCreate" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg " style="width: 70%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span id="span1" runat="server">Create Document</span>
                </div>
                <div class="modal-body" style="max-height: 900px; overflow: auto;">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 pull-left" style="height: 611px; overflow: auto;" runat="server" id="divRichEdit">
                        <dx:ASPxRichEdit ID="ASPxRichEdit2" runat="server" WorkDirectory="~\App_Data\WorkDirectory" Settings-Behavior-Save="Disabled" Settings-Behavior-SaveAs="Disabled" Settings-Behavior-Printing="Disabled" Settings-Behavior-FullScreen="Disabled" Settings-DocumentCapabilities-HeadersFooters="Enabled" Settings-Behavior-CreateNew="Disabled" Settings-Behavior-Open="Disabled" OnCallback="ASPxRichEdit2_Callback">
                            <Settings>
                                <SpellChecker Enabled="true" Culture="en-US">
                                    <OptionsSpelling IgnoreEmails="true" IgnoreUri="true" />
                                </SpellChecker>
                            </Settings>
                            <SettingsDocumentSelector UploadSettings-ValidationSettings-DisableHttpHandlerValidation="true" UploadSettings-Enabled="true" UploadSettings-AdvancedModeSettings-TemporaryFolder="~\App_Data\WorkDirectory"></SettingsDocumentSelector>
                        </dx:ASPxRichEdit>
                    </div>
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12  pull-left padding-none top">
                        <asp:UpdatePanel ID="upbns" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:CheckBox ID="cbDeclaration" runat="server" Text="click here to submit document for review" Style="display: none" AutoPostBack="true" />
                                <button type="button" data-dismiss="modal" runat="server" id="Button2" class="pull-right btn btn-canceldms_popup">Cancel</button>
                                <asp:Button ID="btn_SubmittoReviewer" runat="server" Text="Submit" CssClass="pull-right btn btn-approve_popup" OnClientClick="return ConfirmAlert1();" ValidationGroup="edit" />
                                <asp:Button ID="Btn_Save" runat="server" Text="Document Save" CssClass="pull-left btn btn-revert_popup" OnClientClick="return ConfirmAlert();" ValidationGroup="edit" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------Document Create View-------------->

    <!--------File Upload View-------------->
    <div id="myFileUpload" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg ">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span id="span2" runat="server">Upload Document</span>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="col-lg-5"><b>Upload your word File :</b></div>
                        <div class="col-lg-4">
                            <asp:FileUpload ID="file_word" runat="server" />
                        </div>
                    </div>
                    <br />
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel ID="www" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Button ID="Btn_Upload" runat="server" Text="Upload" CssClass="pull-right btn btn-approve_popup" OnClick="Btn_Upload_Click" ValidationGroup="edit" OnClientClick="return Required();" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="Btn_Upload" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!--------File Upload View-------------->

    <!-------- Refferal VIEW-------------->
    <div id="myModal_Refferal" class="modal testt department fade" role="dialog" style="background: #1a1a1a">
        <div class=" modal-dialog modal-lg " style="width: 90%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span id="span3" runat="server">Referrals</span>
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="background: #fff">
                    <ul class="nav nav-pills tab_grid">
                        <li class="Referralstab active"><a data-toggle="pill" href="#TabPanel01">Link Referrals</a></li>
                        <li class="internaltab"><a data-toggle="pill" href="#TabPanel02">Internal Document</a></li>
                        <li class="Externaltab"><a data-toggle="pill" href="#TabPanel03">External Documents</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="TabPanel01" class="Referralstabbody tab-pane fade in active">
                            <div class="row">
                                <div class="col-sm-12">
                                    <asp:UpdatePanel ID="uplinkDoc" runat="server">
                                        <ContentTemplate>
                                            <textarea id="txtLinkReferrences" runat="server" class="rtxtcontent"></textarea>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                        <div id="TabPanel02" class="internaltabbody tab-pane fade">
                            <asp:UpdatePanel ID="upInternalDoc" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="row">
                                        <br />
                                        <div class="col-sm-12">
                                            <div class="col-sm-2">Departments : </div>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddl_Dept" runat="server" CssClass="col-md-12 col-lg-12 col-xs-12 col-sm-12 selectpicker drop_down padding-none " data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddl_Dept_SelectedIndexChanged"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                        <div class="col-lg-5 top pull-left  " id="divAvailable" runat="server">
                                            <div class="panel-heading_title">
                                                <h4 class="panel-title"><a>Available Document</a></h4>
                                                <input name="txtTerm" class="gridSearchBox1" type="search" oninput="GridViewSearch(this, '<%=gvAvailableDocuments.ClientID %>')" placeholder="Search Assigned Documents" />
                                            </div>
                                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="border: 1px solid #07889a; margin-top: -10px; max-height: 500px; min-height: 500px; overflow-y: auto">
                                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 top padding-none">
                                                    <asp:GridView ID="gvAvailableDocuments" CssClass="CreatorGrid" runat="server" AutoGenerateColumns="false" Width="100%" AllowPaging="True" PageSize="15" EmptyDataText="Documents Not Available" OnPageIndexChanging="gvAvailableDocuments_PageIndexChanging">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="cbxSelect" runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Number" HeaderStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_DocumentNumber" runat="server" Text='<%# Eval("DocumentNumber") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_DocumentID" runat="server" Text='<%# Eval("VersionID") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lbl_DeptID" runat="server" Text='<%# Eval("DeptID") %>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_DocumentName" runat="server" Text='<%# Eval("DocumentName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2" id="divAddRemove" runat="server" style="margin-top: 13em">
                                            <div class="btn-group-vertical" style="padding-right: 20px;">
                                                <asp:Button ID="btn_AddDocuments" runat="server" class="col-lg-6 col-lg-offset-5 btn_selection_boxDms" Text=">" ToolTip="Add Documents" Style="margin-bottom: 10px; font-weight: bold" OnClick="btn_AddDocuments_Click" />
                                                <asp:Button ID="btn_RemoveDocuments" runat="server" class=" col-lg-offset-5 btn_selection_boxDms" Text="<" ToolTip="Remove Documents" Style="font-weight: bold" OnClick="btn_RemoveDocuments_Click" />
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 top pull-left  " id="divAssigned" runat="server">
                                                <div class="panel-heading_title">
                                                    <h4 class="panel-title"><a>Assigned Document</a></h4>
                                                </div>
                                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="border: 1px solid #07889a; margin-top: -10px; max-height: 500px; min-height: 500px; overflow-y: auto">
                                                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 top padding-none">
                                                        <asp:GridView ID="gvAssignedDocuments" runat="server" AutoGenerateColumns="false" Width="100%" AllowPaging="true" PageSize="15" OnPageIndexChanging="gvAssignedDocuments_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="cbxSelect" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Number" HeaderStyle-Width="20%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_DocumentNumber" runat="server" Text='<%# Eval("DocumentNumber") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_DocumentID" runat="server" Text='<%# Eval("VersionID") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lbl_DeptID" runat="server" Text='<%# Eval("DeptID") %>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_DocumentName" runat="server" Text='<%# Eval("DocumentName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div id="TabPanel03" class="Externaltabbody tab-pane fade">
                            <div>
                                <div class="col-sm-12 top">
                                    <div class="col-sm-1">Files : </div>
                                    <div class="col-sm-4">
                                        <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="true" Width="100%" />
                                    </div>
                                    <div class="col-sm-7 bottom padding-none">
                                        <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Button ID="btnFileUpload" runat="server" Text="Upload " CssClass="pull-right  btn-signup_popup" OnClick="btnFileUpload_Click" OnClientClick="return Required1();" />
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnFileUpload" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-sm-12">
                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvExternalFiles" runat="server" AutoGenerateColumns="false" EmptyDataText="No files uploaded" Width="100%" OnRowDataBound="gvExternalFiles_RowDataBound" BorderColor="#07889a">
                                                        <Columns>
                                                            <asp:BoundField DataField="FileName" HeaderText="File Name" />
                                                            <asp:TemplateField HeaderText="View Doc">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel ID="upgvbutton" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton ID="lnkDownload" class="summary_latest" CommandArgument='<%# Eval("FileName") +"@$"+Eval("Ref_FileID")+"@$"+Eval("pkid")%>' runat="server" OnClick="DownloadFile"></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="lnkDownload" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                    <asp:Label ID="lbl_isSavedFile" runat="server" Text='<%# Eval("isSavedFile") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lbl_Ref_FileID" runat="server" Text='<%# Eval("Ref_FileID") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lbl_pkid" runat="server" Text='<%# Eval("pkid") %>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkDelete" Text="" CommandArgument='<%# Eval("FileName")+"@$"+Eval("Ref_FileID")+"@$"+Eval("pkid") %>' runat="server" OnClick="DeleteFile"><img src="../../Images/GridActionImages/del_grid.png"  style="width:25px;height:25px"/></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 top">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                <ContentTemplate>
                                    <asp:Button ID="btn_Refferals_Cancel" runat="server" Text="Cancel" CssClass="pull-right btn btn-canceldms_popup" OnClick="btn_Refferals_Cancel_Click" />
                                    <asp:Button ID="btn_Add_Referrences" runat="server" Text="Save" CssClass="pull-right btn btn-approve_popup" OnClick="btn_Add_Referrences_Click" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btn_Add_Referrences" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------- End Refferal VIEW-------------->

    <!--------External Refferal VIEW-------------->
    <div id="mpop_FileViewer" class="modal department fade" role="dialog">
        <div class=" modal-dialog modal-lg" style="width: 86%">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="ShowRefferal();">&times;</button>
                    <span id="span5" runat="server">External Refferal</span>
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="background-color: #fff">
                    <div class="col-sm-12" style="pointer-events: none;">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="dviframe" runat="server">
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------End External Refferal VIEW-------------->
    
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <asp:HiddenField ID="hdfPKID" runat="server" />
    <asp:HiddenField ID="hdfConfirm" runat="server" />
    
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
</asp:Content>
