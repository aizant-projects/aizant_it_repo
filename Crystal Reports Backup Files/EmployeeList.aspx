﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.Master" AutoEventWireup="true" CodeBehind="EmployeeList.aspx.cs" Inherits="AizantIT_PharmaApp.UserManagement.EmployeeList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .container {
            width: 1390px;
        }

        .table-text-center th, .table-text-center td {
            text-align: center;
        }

        .dataTables_scroll {
            overflow: auto;
        }

        .gridpager tr td {
            padding: 5px 13px;
            background: #38b8ba;
            border: 1px solid #fff;
            color: #000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div class=" col-lg-3 padding-none float-left">
        <div class="closebtn">
            <span>&#9776;
            </span>
        </div>

        <div style="display: none;" class="closebtn1">
            <span>&#9776;</span>
        </div>
    </div>
    <asp:HiddenField ID="hdModuleId" runat="server" />
    <asp:HiddenField ID="hdnDeptCode" runat="server" Value="" />
    <asp:HiddenField ID="hdnRoleID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnShowType" runat="server" Value="0" />
    <div style="margin-right: 0px; margin-top: 4px">
        <asp:UpdatePanel ID="upserver" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:Button ID="btnEmployee" runat="server" class=" float-right  btn-signup_popup" PostBackUrl="~/UserManagement/Employee.aspx" Text="Add Employee" />
                <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" />
                <asp:Button ID="btnUserLoginHistory" runat="server" Text="ShowLog" Style="display: none" OnClick="btnUserLoginHistory_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
        <%-- <div class=" col-lg-9 float-right padding-none">       
        </div>--%>
        <div class=" col-lg-12 col-sm-12 col-md-12 col-12 padding-none float-left">
            <div class=" col-lg-12 col-sm-12 col-md-12 col-12 grid_area padding-none float-left">
                <div class="col-lg-12 col-sm-12 col-md-12 col-12 padding-none float-left">

                    <div id="header">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-12 dashboard_panel_header float-left">
                            Employee List
                              
                                
                        </div>
                    </div>
                    <div id="body" class="col-lg-12 col-sm-12 col-md-12 col-12 float-left " style="overflow: auto; margin-top: 10px">

                        <table id="dtEmployeeList" class="display datatable_cust" cellspacing="0" style="width: 100%">
                            <thead style="width: 100%">
                                <tr>
                                    <th>S.No</th>
                                    <th>Photo</th>
                                    <th>EmpID</th>
                                    <th>Emp. code</th>
                                    <th>Name</th>
                                    <th>Department</th>
                                    <th>Designation</th>
                                    <th>OfficialEmailID</th>
                                    <th>MobileNo</th>
                                    <th>Start date</th>
                                    <th>Status</th>
                                    <th>Login ID</th>
                                    <th>Locked</th>
                                    <th>Roles</th>
                                    <th>Edit</th>
                                    <th>History</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class=" col-lg-12 col-sm-12 col-md-12 col-12 padding-none float-left" style="margin-top: 4px; margin-bottom: 10px">
        <img src="../Images/GridActionImages/assgined_roles.png" />
        <b class="legends_grid">Assigned Roles</b>&nbsp;
        <img src="../Images/GridActionImages/assign_roles.png" />&nbsp;<b class="legends_grid">UnAssigned Roles</b>&nbsp;
        <img src="../Images/GridActionImages/locked.png" />&nbsp;<b class="legends_grid">Locked</b>&nbsp;
        <img src="../Images/GridActionImages/unclock.png" />&nbsp;<b class="legends_grid">UnLocked</b>&nbsp;
       <%-- <img src="../Images/GridActionImages/password.png" />&nbsp;<b>Password Locked</b>--%>
    </div>
    <div id='OpenDilog'></div>
    <div id="myModal" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <asp:Label ID="lblHeader" runat="server"></asp:Label></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>

                <div class="form-horizontal col-12 padding-none">
                    <div class="form-group padding-none" hidden="hidden">
                        <input type="text" class="form-control" name="name" id="txtEmpID" runat="server" placeholder=" Code" readonly="true" />
                    </div>
                    <div class="col-12 form-group float-left padding-none top">
                        <label for="username" class="col-sm-12 control-label float-left ">Reason<span class="smallred_label">*</span></label>
                        <div class="col-sm-12 float-left">
                            <textarea type="text" class="form-control col-12" name="name" id="txtComments" runat="server" placeholder="Reason" rows="5" onkeypress="return NoSpaceAllowFirst(event);" />
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <div>
                        <asp:UpdatePanel ID="upsd" runat="server" UpdateMode="Always">
                            <ContentTemplate>

                                <asp:Button ID="btnSubmit" Class=" btn btn-signup_popup" runat="server" Text="Submit" OnClick="btnSubmit_Click" OnClientClick="return Check();" />
                                <asp:Button class=" btn btn-cancel_popup" data-dismiss="modal" runat="server" ID="btnCancel" Text="Close"></asp:Button>
                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 75%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            <span id="span4" class="col-10" runat="server"></span>

                            <button type="button" id="btnUlogH" class="btn btn-sm innerbtn" onclick="ShowUserLoginHistory()" title="Employee/Login History"><span class="panel_list_refresh"><span style="padding-left: 20px;">History</span></span></button>

                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover table-responsive" ClientIDMode="Static"
                                    EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White"
                                    BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" AllowPaging="true" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Modified By" ItemStyle-Width="140px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("GivenBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-Width="140px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("CommentedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Submitted Data" ItemStyle-Width="300px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_SubmittedData" runat="server" Text='<%# Eval("SubmittedData") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-Width="300px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Comments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="gridpager" HorizontalAlign="Right" />
                                </asp:GridView>
                                <asp:GridView ID="gvUserLoginHistory" runat="server" CssClass="table table-hover table-responsive" ClientIDMode="Static"
                                    EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White"
                                    BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" AllowPaging="true" OnPageIndexChanging="gvUserLoginHistory_PageIndexChanging" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Employee" ItemStyle-Width="300px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Employee" runat="server" Text='<%# Eval("Employee") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LoginID" ItemStyle-Width="140px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_LoginID" runat="server" Text='<%# Eval("LoginID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Login_Time" ItemStyle-Width="180px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Login_Time" runat="server" Text='<%# Eval("Login_Time") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Logout_Time" ItemStyle-Width="180px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Logout_Time" runat="server" Text='<%# Eval("Logout_Time") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ServerAddress" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_UserHostAddress" runat="server" Text='<%# Eval("UserHostAddress") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ClientAddress" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_UserClientAddress" runat="server" Text='<%# Eval("UserClientAddress") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="gridpager" HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->

    <asp:HiddenField ID="hdfVID" runat="server" />
    <style>
        .avatar {
            vertical-align: middle;
            width: 26px;
            height: 26px;
            border-radius: 50%;
        }

        .gridpager table {
            float: right;
        }

        .gridpager, .gridpager td {
            text-align: right;
            color: #38b8ba;
            background: #efffff;
            font-weight: bold;
            text-decoration: none;
        }

            .gridpager a {
                color: White;
                font-weight: normal;
            }
    </style>

    <script>
        function NoSpaceAllowFirst(event) {
            var que = document.getElementById('<%=txtComments.ClientID%>').value.trim();
            if (event.keyCode == 32 && que.length == 0) {
                return false;
            }
            else {
                return true;
            }
        }

        $('#dtEmployeeList thead tr').clone(true).appendTo('#dtEmployeeList thead');
        $('#dtEmployeeList thead tr:eq(1) th').each(function (i) {
            if (i < 11) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search " />');

                $('input', this).on('keyup change', function () {
                    if (tblEmployeeList.column(i).search() !== this.value) {
                        tblEmployeeList
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 0 || i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });

        $('#dtEmployeeList').wrap('<div class="dataTables_scroll" />');
        var tblEmployeeList = $('#dtEmployeeList').DataTable({
            columns: [

                { 'data': 'RowNumber' },//0
                {
                    "data": "Photo",
                    "render": function (data) {
                        if (data != null) {
                            return '<img width="70px" class="avatar" height="55px" src="data:image/gif;base64,' + data + '" />';
                        } else {
                            return '<img width="70px" class="avatar" height="55px"  src="../Images/UserLogin/Ima123.png" />';
                        }

                    }
                }, //1
                { 'data': 'EmpID' },//2
                { 'data': 'EmpCode' },//3
                { 'data': 'Name' },//4
                { 'data': 'DepartmentName' }, //5
                { 'data': 'DesignationName' }, //6
                { 'data': 'OfficialEmailID' },//7
                { 'data': 'MobileNo' },//8
                { 'data': 'StartDate' },//9
                { 'data': 'Status' },//10
                { 'data': 'LoginID' },//11
                { 'data': 'Locked' },//12
                { 'data': 'RoleCount' },//13
                { //14
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="Edit" title="Edit" href=/UserManagement/Employee.aspx?EmpID=' + o.EmpID + '&S=' + o.Status + '>' + '' + '</a>'; } //13
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History" data-toggle="modal" data-target="#myCommentHistory" onclick=ViewCommentHistory(' + o.EmpID + ')></a>'; }
                }
            ],
            "paging": true,
            "search": {
                "regex": true
            },

            "lengthMenu": [[10], [10]],
            "aoColumnDefs": [{ "targets": [0, 2, 7, 8], "visible": false, "searchable": false }, { "width": "15%", "targets": 4 }, { "bSortable": false, "aTargets": [1, 8, 11, 13, 14] },
            {
                "className": "dt-body-left", "targets": [2, 3, 4, 5, 6, 7, 8, 11]
            },
            {
                targets: [12], render: function (a, b, data, d) {
                    if (data.Locked == "No") {
                        return data.Status == "Active" ? '<a class="unlock" title="Unlock" href="#"  onclick=EditEmployeeunlock(' + data.EmpID + ')>' + '' + '</a>' : 'NA';
                    } else if (data.Locked == "Yes") {
                        return data.Status == "Active" ? '<a class="lock" title="Lock"  href="#" onclick=EditEmployeelock(' + data.EmpID + ')></a>' : 'NA';
                    }
                    return "";
                }
            },
            {
                targets: [13], render: function (a, b, data, d) {
                    //if (data.Status == "Active") {
                        if (data.RoleCount == "0") {
                            return '<a class="Assign_Role" title="Role" href=/UserManagement/EmployeeAssignRoles.aspx?E=' + data.EmpID + '&D=' + data.DeptID + '&S=' + data.Status + '>' + '' + '</a>';
                        } else if (data.RoleCount != "0") {
                            return '<a class="Assigned_Role" title="Role" href=/UserManagement/EmployeeAssignRoles.aspx?E=' + data.EmpID + '&D=' + data.DeptID + '&S=' + data.Status + '>' + '' + '</a>';
                        }
                    //} else {
                       
                    //    var Name = data.EmpCode + '-' + data.Name;
                    //  //  console.log(data.EmpCode + '-' + data.Name);
                    //    return '<a class="Assign_Role" title="Role" href="#" onclick="InActiveEmployeeAssignRoles( \'' + Name.replace(/'/g, "\\'") + '\');" ></a>';
                    //}
                    return "";
                }
                //targets: [13], render: function (a, b, data, d) {
                //    if (data.RoleCount == "0") {
                //        if (data.status == "Active") {
                //            return '<a class="Assign_Role" href=/UserManagement/EmployeeAssignRoles.aspx?E=' + data.EmpID + '&D=' + data.DeptID + '>' + '' + '</a>';
                //        }
                //        else {
                //            return '<a class="Assign_Role" href="#" onclick="EmployeeInActive();"></a>';
                //        }
                //    } else if (data.RoleCount != "0") {
                //        if (data.Status == "Active") {
                //            return '<a class="Assigned_Role" href=/UserManagement/EmployeeAssignRoles.aspx?E=' + data.EmpID + '&D=' + data.DeptID + '>' + '' + '</a>';
                //        }
                //        else {
                //            return '<a class="Assigned_Role" href="#" onclick="EmployeeInActive();"></a>';
                //        }
                //    }
                //    return "";
                //} 
            },
            ],
            "orderCellsTop": true,
            "responsive": true,
            "bAutoWidth": true,
            "sServerMethod": 'post',
            "sAjaxSource": '/UserManagement/WebServicesList.asmx/GetEmployeesList1',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "DeptCode", "value": $('#<%=hdnDeptCode.ClientID%>').val() }, { "name": "RoleID", "value": $('#<%=hdnRoleID.ClientID%>').val() }, { "name": "ShowType", "value": $('#<%=hdnShowType.ClientID%>').val() });

                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtEmployeeList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        //$(".tblTrainingSessionsClass").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        //if (typeof console == "object") {
                        //    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        //}

                        //errors = [];
                        //errors.push("No Records Found");
                        //if (errors.length > 0) {
                        custAlertMsg(xhr.status + "," + xhr.responseText + "," + textStatus + error, "error");
                        //}
                    }
                });
            }
        });
        function Reloadtable() {
            tblEmployeeList.ajax.reload();
            $('#myModal').modal('hide');
        }

        //function InActiveEmployeeAssignRoles(EmpName) {
        //    custAlertMsg('<b>'+EmpName + '</b> Employee is In-Active, Roles cannot be Assigned', 'info');
        //}
        function EditEmployeeunlock(E_ID) {

            var a = E_ID;
            document.getElementById('<%=txtComments.ClientID%>').value = "";
            document.getElementById('<%=lblHeader.ClientID%>').innerText = "Lock User";
            document.getElementById('<%=btnSubmit.ClientID%>').value = "Lock";
            document.getElementById("<%=txtEmpID.ClientID%>").value = a;
            $('#myModal').modal('show');
        }
        function EditEmployeelock(E_ID) {

            //var ph = $("#OpenDilog");
            var a = E_ID;
            document.getElementById('<%=txtComments.ClientID%>').value = "";
                document.getElementById('<%=lblHeader.ClientID%>').innerText = "UnLock User";
                document.getElementById('<%=btnSubmit.ClientID%>').value = "UnLock";
                document.getElementById("<%=txtEmpID.ClientID%>").value = a;
            $('#myModal').modal('show');
        }
        function ShowPopup() {

            custAlertMsgSucess('Updated Successfully', 'confirm', true);

        }
        function Reload() {
            location.reload();
        }
        function Check() {
            var a = document.getElementById('<%=txtComments.ClientID%>');
            errors = [];
            if (a.value == "") {
                errors.push("Enter Reason");

            }

            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
        function ShowUserLoginHistory() {

            var btnSUL = document.getElementById("<%=btnUserLoginHistory.ClientID %>");
            btnSUL.click();
        }

    </script>
    <script>
        $(document).keydown(function (event) {
            if (event.keyCode == 27) {
                var clickButton = document.getElementById("<%=btnCancel.ClientID %>");
                clickButton.click();
            }
        });
    </script>
    <script>
        function ReloadCurrentPage() {
            window.open("<%=ResolveUrl("~/UserManagement/EmployeeList.aspx")%>", "_self");
        }
    </script>
    <script>
        function EmployeeInActive() {
            custAlertMsg("Employee is In-Active ,Roles cannot be Assigned", 'error', 'ReloadCurrentPage();');
            return false;
        }
    </script>

</asp:Content>
