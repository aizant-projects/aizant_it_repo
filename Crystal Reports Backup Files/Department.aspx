﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.Master" AutoEventWireup="true" CodeBehind="Department.aspx.cs" Inherits="AizantIT_PharmaApp.Department.Department" %>

<%@ Register Src="~/UserControls/SessionTimeOut.ascx" TagPrefix="uc1" TagName="SessionTimeOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
          <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <link href="<%=ResolveUrl("~/AppCSS/CreateUserStyles.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/AppCSS/UserLogin/UserLoginStyles.css")%>" rel="stylesheet" />
     <div class=" col-lg-3 padding-none">
				<div class="closebtn"><span>&#9776;</span></div>
				<div style="display:none;" class="closebtn1"><span>&#9776;</span>

				</div>										
			</div>
    <div class="col-sm-12" style="padding-left: 20%; padding-right: 20%">
        <div class="row Panel_Frame">
            <div id="header">
                <div class="col-sm-12 Panel_Header" >
                    <div class="col-sm-6">
                        <span class="Panel_Title">Department</span>
                    </div>
                    <div class="col-sm-6">
                        <asp:HiddenField ID="HFDeptID" runat="server" />
                    </div>
                </div>
            </div>
            <div>
                <div class="row" style="margin-left: 10px; margin-right: 10px">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="username" class="col-sm-4 control-label">Department Code<span class="mandatoryStar">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="name" id="txtDeptCode" maxlength="5" runat="server" placeholder="Department Code" onkeypress="return ValidateAlphaNumeric(event)" onpaste="return false" tabindex="1" title="Dept Code" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-4 control-label">Department Name<span class="mandatoryStar">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="name" id="txtDeptName" maxlength="50" runat="server" placeholder="Department Name" onkeypress="return ValidateAlphaNumericSpace(event)" onpaste="return false" tabindex="2" title="Dept Name" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-12 modal-footer Panel_Footer" >
                <div class="Panel_Footer_Buttons">
                    <div style="margin-right:35%;margin-top:8px">
            <asp:Button ID="btnCancel" Class="pull-right btn btn-cancel_popup"   runat="server" Text="Cancel" OnClientClick="Clear();" OnClick="btnCancel_Click" TabIndex="4" />    <asp:Button ID="btnSubmit" Class="pull-right btn btn-signup_popup" runat="server" Text="Submit" OnClientClick="javascript:return Requiredvalidate();" OnClick="btnSubmit_Click" TabIndex="3"/> 
               
                        </div>
                    </div>
                  <div class="Panel_Footer_Message_Label">
                        <asp:Label ID="lblDepartmentError" runat="server" Text="" ForeColor="Red"></asp:Label>
                      </div>
            </div>             
        </div>
    </div>

       <%-- <!--To Show Custom Alert Messages-->
        <script>
            function custAlertMsg(message, alertType, url = '') {
                if (alertType == 'error') {
                    document.getElementById("msgError").innerHTML = message;
                    $("#ModalError").modal();
                }
                else if (alertType == 'success') {
                    document.getElementById("msgSuccess").innerHTML = message;
                    $("#ModalSuccess").modal('show');
                }
                else if (alertType == 'warning') {
                    document.getElementById("msgWarning").innerHTML = message;
                    $("#ModalWarning").modal();
                }
                else if (alertType == 'info') {
                    document.getElementById("msgInfo").innerHTML = message;
                    $("#ModalInfo").modal();
                }
                else if (alertType == 'confirm') {
                    document.getElementById("msgInfo").innerHTML = message;
                    $("#ModalInfo").modal();
                }
            }
        </script>--%>
    <script type="text/javascript">

        //for required fields
        function Requiredvalidate() {
            var DeptName = document.getElementById("<%=txtDeptName.ClientID%>").value.trim();
            var DeptCode = document.getElementById("<%=txtDeptCode.ClientID%>").value.trim();
            errors = [];
            if (DeptName == "" && DeptCode == "") {
                errors.push("Enter Department Code and Name");
                document.getElementById("<%=txtDeptCode.ClientID%>").focus();
                  
            }
            if (DeptCode == "") {
                errors.push("Enter Department Code");
                document.getElementById("<%=txtDeptCode.ClientID%>").focus();
                 
              }
             if (DeptName == "") {
                  errror.push("Enter Department Name");
                  document.getElementById("<%=txtDeptName.ClientID%>").focus();
                 
             }
             if (errors.length > 0) {
                 document.getElementById("msgError").innerHTML = errors.join("<br/>");
                $("#ModalError").modal('show');

                return false;
            }
              else {
                  return true;
              }
        }
    </script>

    <script>
        //for clearing the Fields
        function Clear() {
            document.getElementById("<%=txtDeptName.ClientID%>").value = "";
            document.getElementById("<%=txtDeptCode.ClientID%>").value = "";
            document.getElementById("<%=lblDepartmentError.ClientID%>").innerHTML = "";
        }
    </script>

    <script>
        $("#NavLnkDepartment").attr("class", "active");
        $("#NavLnkCreateDept").attr("class", "active");
    </script>

</asp:Content>
