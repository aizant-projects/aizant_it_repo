﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="Existing_DocumentList.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentCreator.Existing_DocumentList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .container {
            width: 1390px;
        }

        .table-text-center th, .table-text-center td {
            text-align: center;
        }

        .dxreView {
            height: 584px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DocumentCreator/Existing_Documents.aspx" Text="Add Document" />
    </div>
    <div class="col-md-12 col-lg-12 col-12 col-sm-12  float-left padding-none">
        <div class=" col-md-12 col-lg-12 col-12 dms_outer_border float-left padding-none">
            <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div id="divMainContainer" runat="server" visible="true">
                <div class=" col-md-12 col-lg-12 col-12 padding-none float-right">
                    <div class="col-md-12 col-lg-12 col-12  padding-none float-left">
                        <div class="grid_header col-md-12 col-lg-12 col-12 float-left">Existing Documents List</div>
                        <div class="col-md-12 col-lg-12 col-12 grid_panel_full border_top_none bottom float-left">
                            <div class="col-12 float-left top padding-none">
                            <table id="dtDocList" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>VersionID</th>
                                        <th>Document Number</th>
                                        <th>Document Name</th>
                                        <th>Version</th>
                                        <th>Document Type</th>
                                        <th>Department</th>
                                        <th>Effective Date</th>
                                        <th>Review Date</th>
                                        <th>View Doc</th>
                                        <th>View Details</th>
                                        <th>History</th>
                                    </tr>
                                </thead>
                            </table>
                                </div>
                            <div class="col-12 float-left bottom">
                            <img src="<%=ResolveUrl("~/Images/GridActionImages/view_tms.png")%>" /><b class="grid_icon_legend">Effective Document</b>
                            <img src="<%=ResolveUrl("~/Images/GridActionImages/revert_tms.png")%>" /><b class="grid_icon_legend">Review Date Exceeded</b>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------- SOP VIEW-------------->
    <div id="myModal_lock" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="min-width: 90%">
            <!-- Modal content-->
            <div class="modal-content col-lg-12 col-md-12 padding-none">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                          
                            <span id="span_FileTitle" runat="server"></span>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div id="divPDF_Viewer">
                                <noscript>
                                    Please Enable JavaScript.
                                </noscript>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!-------- End SOP VIEW-------------->
    <script>
        $('#dtDocList thead tr').clone(true).appendTo('#dtDocList thead');
        $('#dtDocList thead tr:eq(1) th').each(function (i) {
            if (i < 9) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (table2.column(i).search() !== this.value) {
                        table2
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDocList').wrap('<div class="dataTables_scroll" />');
        var table2 = $('#dtDocList').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'VersionID' },
                { 'data': 'DocumentNumber' },
                { 'data': 'DocumentName' },
                { 'data': 'VersionNumber' },
                { 'data': 'DocumentType' },
                { 'data': 'Department' },
                { 'data': 'EffectiveDate' },
                { 'data': 'ExpirationDate' },
                { 'data': 'expStatus' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a   class="view_req" title="View Details" href=/DMS/DocumentCreator/Existing_Documents.aspx?DocID=' + o.VersionID + ' > </a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History" data-toggle="modal" data-target="#myCommentHistory" onclick=ViewCommentHistory(' + o.VersionID + ')></a>'; }
                },
            ],
            "responsive": true,
            "paging": true,
            "search": {
                "regex": true
            },
            "aoColumnDefs": [{ "targets": [0,1], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 1, 9] }, { "className": "dt-body-left", "targets": [2, 3, 5, 6] },
            {
                targets: [9], render: function (a, b, data, d) {
                    if (data.expStatus > 0) {
                        return '<a class="view_Training" title="View Doc" href="#"  onclick=ViewDocument1(' + data.VersionID + ')></a>';
                    } else {
                        return '<a class="view" href="#" title="View Doc"  onclick=ViewDocument(' + data.VersionID + ')></a>';
                    }
                    return "N/A";
                }
            },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetDocumentList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push();
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    </script>
    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdfFileName" runat="server" />
    <asp:UpdatePanel ID="upBtns" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnFileView" runat="server" Text="Submit" OnClick="btnFileView_Click" Style="display: none" OnClientClick="showImg();"/>
            <asp:Button ID="btnFileView1" runat="server" Text="Submit" OnClick="btnFileView1_Click" Style="display: none" OnClientClick="showImg();"/>
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" OnClientClick="showImg();"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="max-width:85%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            
                            <span id="span4" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("GivenBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("CommentedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Time Spent">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Time" runat="server" Text='<%# Eval("Time") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="22%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="26%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                <asp:TextBox ID="txt_Comments" runat="server" ReadOnly="true" Rows="4" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM">Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL">Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned To">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script>
        function ViewDocument(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnFV = document.getElementById("<%=btnFileView.ClientID %>");
            btnFV.click();
        }
        function ViewDocument1(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnFV1 = document.getElementById("<%=btnFileView1.ClientID %>");
            btnFV1.click();
        }
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
        function CloseBrowser() {
            window.close();
        }
        function ViewExistingDoc() {
            PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', "0", $('#<%=hdfVID.ClientID%>').val(), "0", "0", "#divPDF_Viewer");
        }
    </script>
    <script type="text/javascript">
        if (document.layers) {
            //Capture the MouseDown event.
            document.captureEvents(Event.MOUSEDOWN);
            //Disable the OnMouseDown event handler.
            document.onmousedown = function () {
                return false;
            };
        }
        else {
            //Disable the OnMouseUp event handler.
            document.onmouseup = function (e) {
                if (e != null && e.type == "mouseup") {
                    //Check the Mouse Button which is clicked.
                    if (e.which == 2 || e.which == 3) {
                        //If the Button is middle or right then disable.
                        return false;
                    }
                }
            };
        }
        //Disable the Context Menu event.
        document.oncontextmenu = function () {
            return false;
        };
    </script>
    <script>
        function showImg() {
            ShowPleaseWait('show');
        }
        function hideImg() {
            ShowPleaseWait('hide');
        }
    </script>
</asp:Content>
