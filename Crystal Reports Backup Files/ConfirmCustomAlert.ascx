﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfirmCustomAlert.ascx.cs" Inherits="AizantIT_PharmaApp.UserControls.ConfirmCustomAlert" %>

<!-- confirmation content-->
<div id="ModalConfirm" class="modal ModalConfirm confirmation_popup" role="dialog">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content modalMainContents">
                    <div class="modal-header ModalHeaderPart">
                        <div class="success_icon float-left">
                                
                            <h4 class="modal-title float-left popup_title">Confirmation</h4>
                            </div>                        
                    </div>
                    <div class="modal-body" style="padding: 0px">
                        <div class="col-lg-12 padding-none">
                            <p id="msgConfirm" class="col-lg-12 capitalize" style="padding: 8px; padding: 12px;  max-height: 128px; overflow-x: auto;"></p>
                        </div>
                    </div>
                    <div class="modal-footer ModalFooterPart" >
                        <div class="confirmDialog" id="cnf" >
                            <asp:UpdatePanel ID="upconfirm" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button ID="btnCnfYes" runat="server" class=" btn-signup_popup btnUcCnfYes" style="margin-top:10px !important" Text="Yes" OnClientClick="closepopup();" OnClick="btnYes_Click" />
                                     <input type="button" value="No" style="margin-left: 1px;margin-top:10px !important" class=" btn-cancel_popup" data-dismiss="modal" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<script>
    function closepopup() {
        $('#ModalConfirm').modal('hide');
    }
</script>
<!-- End confirmation content-->

