﻿<%@ Page Title="Target TS List" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="TargetTS_List.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Reports.TargetTS_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
       <asp:HiddenField ID="hdnEmpID" runat="server" Value="0"/>
     <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnTTS_ID" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
     <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
       <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="mainContainer" runat="server" visible="true">
            <div class="col-12 padding-none" id="Div3" runat="server">     
                  <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 bottom"><asp:Label ID="Label2" runat="server" Font-Bold="true" Text="Target Training List"></asp:Label></div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none">
                    <table id="tblTTS_ApprovalList" class="datatable_cust display breakword" style="width: 100%">
                        <thead>
                            <tr>
                                <th>TTS_ID</th>
                                <th>S.NO</th>
                                <th>Document</th>
                                <th>Dept Code</th>
                                <th>Type of Training</th>
                                <th>Target Type</th>
                                <th>Author</th>
                                <th>Reviewer</th>
                                <th>Approved Date</th>
                                <th>Status</th>
                                <th>View</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnViewTTS_ApprovalList" OnClick="btnViewTTS_ApprovalList_Click" runat="server" Text="Button" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script>
        function ViewTTS_ApprovalList(TTS_ID) {
            $("#<%=hdnTTS_ID.ClientID%>").val(TTS_ID);
            $("#<%=btnViewTTS_ApprovalList.ClientID%>").click();
        }
    </script>

    <!--TTS_ApprovalList jQuery DataTable-->
    <script>
        function getUrlVars() {
            var vars = [], hash;
            var url = window.location.href;
            var hashes = url.slice(url.indexOf('?') + 1).split('&');
            if (url.indexOf("#") > 0) {
                var hashes = url.slice(url.indexOf('?') + 1, url.indexOf('#')).split('&');
            }

            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
        $('#tblTTS_ApprovalList thead tr').clone(true).appendTo('#tblTTS_ApprovalList thead');
        $('#tblTTS_ApprovalList thead tr:eq(1) th').each(function (i) {
            if (i < 9) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search " />');

                $('input', this).on('keyup change', function () {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });

        var table = $('#tblTTS_ApprovalList').DataTable({
            columns: [
                { 'data': 'TTS_ID' },
                { 'data': 'RowNumber' },
                { 'data': 'DocumentName' },
                { 'data': 'DeptCode' },             
                { 'data': 'TypeOfTraining' },
                { 'data': 'TargetType' },
                { 'data': 'Author' },
                { 'data': 'Reviewer' },
                { 'data': 'ApprovedDate' },
                { 'data': 'StatusName' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="view" title="View"  href="#" onclick="ViewTTS_ApprovalList(' + o.TTS_ID + ');"></a>'; }
                }
            ],
            "aoColumnDefs": [{ "targets": [0, 4], "visible": false },
                { className: "textAlignLeft", "targets": [2, 5, 6,7, 8, 9] },
                { "bSortable": false, "aTargets": [ 1 ] }
            ],
            "orderCellsTop": true,
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/TargetTrainingList")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "Year", "value": getUrlVars()["Year"] }, { "name": "EMP_ID", "value": <%=hdnEmpID.Value%> }, { "name": "DeptID", "value": getUrlVars()["DeptID"] }, { "name": "FilterType", "value": getUrlVars()["FilterType"] }, { "name": "Month", "value": getUrlVars()["Month"] }, { "name": "TargetType", "value": getUrlVars()["TargetType"] }, { "name": "Status", "value": getUrlVars()["Status"] } );
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblTTS_ApprovalList").show();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });

        function showDetails() {
            //so something funky with the data
        }
    </script>
</asp:Content>
