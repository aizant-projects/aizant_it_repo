﻿<%@ Page Title="Target Training Master" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" ValidateRequest="false"
    EnableEventValidation="false" AutoEventWireup="true" CodeBehind="TTS_Master.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TargetTrainingSchedule.TTS_Master" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<%@ Register Src="~/UserControls/TMS_UserControls/Exam/ucExamResultSheet.ascx" TagPrefix="uc1" TagName="ucExamResultSheet" %>
<%@ Register Src="~/UserControls/TMS_UserControls/ucFeedback.ascx" TagPrefix="uc1" TagName="ucFeedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .btnAddTrainers {
            margin-bottom: 10px;
        } 
        .tblFixHeight tbody {
            max-height: 210px !important;
        }
        .leftAlign {
            text-align: left !important;
        }

        .paddingOverwrite {
            padding: 5px 12px !important
        }

        .Traineechckbx thead input {
            width: initial !important;
        }

          .rowred {
        background-color: #f7d2d8 !important;
        width: 20px;
        height: 20px;
        float: left;
        border: 2px solid #e67e7e;
        border-radius: 4px;
    }
          .cancel_table tbody tr:nth-of-type(odd){
            background-color:#f4feff;
        }
          
          .tablerowred {
        background-color: #f7d2d8 !important;
    }
        

    </style>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script src="<%= ResolveUrl("~/AppScripts/TableCheckBoxSelection.min.js")%>"></script>
    <asp:HiddenField ID="hdnTTS_ID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTrainingSessionID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnPreviousPage" runat="server" />
    <asp:HiddenField ID="hdnTTS_CurrentStatus" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRecentSessionID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTrainee" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTargetExamID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnEmpIsHodOrQA" runat="server" Value="N" />
    <asp:HiddenField ID="hdnIsAssignedTrainer" runat="server" Value="false" />
    <asp:HiddenField ID="hdnModeOfTraining" runat="server" Value="1" />

    <asp:UpdatePanel UpdateMode="Always" runat="server" ID="uphdnRecentSessionStatus">
        <ContentTemplate>
            <asp:HiddenField ID="hdnRecentSessionStatus" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTTS_OnlineSessionID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnDocID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnEmpFeedbackID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnExamType" runat="server" Value="0" />
            <asp:HiddenField ID="hdnIs_Selfcomplte" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdnMinDate" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDueDate" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTraineeIds" runat="server" Value="" />
    <asp:HiddenField ID="hdnTTS_Status" runat="server" />

    <div class="col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div class="form-group col-lg-6 float-right padding-none">
        <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
            <%--   <asp:LinkButton ID="lbTTSList" class="float-right btn-signup_popup" OnClick="lbTTSList_Click" runat="server">
                    Back To List</asp:LinkButton>--%>
            <asp:Button Text="Back To List" class="float-right btn-signup_popup" runat="server" ID="btnBackToList" OnClick="btnBackToList_Click" />
        </div>
    </div>
    <div id="divMainContainer" class="col-lg-12 float-left padding-none" runat="server">
        <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 col-12 col-md-12 padding-none">
            <div id="divHearderCollapse" class="accordion ">
                <div class="card mb-0">
                    <div class="card-header grid_header header_col" data-toggle="collapse" href="#collapseOne123">
                        <a class="card-title">Target Training Schedule
                        </a>
                    </div>
                    <div id="collapseOne123" class="collapse show  grid_panel_full" data-parent="#divHearderCollapse">
                        <div class="card-body acc_boby float-left ">
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none top">
                                <asp:UpdatePanel ID="upDeptAndDocType" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-group float-left col-lg-3  col-12 col-md-3 col-sm-12 ">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Select Department<span class="mandatoryStar">*</span></label>
                                                <asp:DropDownList ID="ddlDepartmentTTS" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control"
                                                    data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlDepartmentTTS_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group float-left col-lg-2 col-12 col-md-2 col-sm-12">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Document Type</label>
                                                <asp:DropDownList ID="ddlDocumentType" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control"
                                                    OnSelectedIndexChanged="ddlDocumentType_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdatePanel ID="upTargetType" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-group float-left col-lg-3  col-12 col-md-3 col-sm-12 TargetTypeDiv" id="divTargetType">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Target Training Type<span class="mandatoryStar">*</span></label>
                                                <asp:DropDownList ID="ddlTargetType" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlTargetType_SelectedIndexChanged">
                                                    <asp:ListItem Text="- Select Target Training Type -" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Refresher" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="New Document" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Revision" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="General" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdatePanel ID="upMonth" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-group float-left col-lg-4 col-12 col-md-4 col-sm-12 MonthDiv" runat="server" id="divMonth" visible="false">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Month<span class="mandatoryStar">*</span></label>
                                                <asp:DropDownList ID="ddlRefresherMonth" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control"
                                                    OnSelectedIndexChanged="ddlRefresherMonth_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div id="divIncident" runat="server" visible="false" class="form-group float-left col-lg-4  col-12 col-md-4 col-sm-12">
                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                        <label>Sub Type</label>
                                        <input type='text' runat="server" id="txtIncident" class="login_input_sign_up form-control" placeholder="Author" readonly="readonly" style="cursor: not-allowed;" />
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="upTrainingDoc" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-group float-left col-lg-7 col-12 col-md-7 col-sm-12 float-left" id="divDocuments">
                                            <label>Select Document<span class="mandatoryStar">*</span></label>
                                            <asp:DropDownList ID="ddlTrainingDocument" runat="server" CssClass="float-left col-lg-12 col-sm-12 col-12 col-md-12 selectpicker regulatory_dropdown_style show-tick padding-none" data-live-search="true" data-size="5" OnSelectedIndexChanged="ddlTrainingDocument_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdatePanel ID="upTypeOfTrainingAndStatus" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-group float-left col-lg-3  col-12 col-md-3 col-sm-12  float-left" id="divTypeOfTraining">
                                            <label>Type of Training</label>
                                            <asp:DropDownList ID="ddlTypeOfTraining" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control">
                                                <asp:ListItem Text="Internal" Value="I"></asp:ListItem>
                                                <asp:ListItem Text="External" Value="E"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group float-left col-lg-2 col-12 col-md-2 col-sm-12 float-left">
                                            <label>Status</label>
                                            <div class="padding-none">
                                                <input type="text" runat="server" class="login_input_sign_up form-control" disabled="disabled" id="txtStatusTTS" value="Not Approved" />
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdatePanel ID="upActionUsers" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-group float-left col-lg-3 col-md-3 col-sm-12 col-12 float-left ">
                                            <label>Select Trainer<span class="mandatoryStar">*</span></label>
                                            <asp:ListBox ID="lbxTrainersTTS" runat="server"
                                                CssClass="float-left col-lg-12 col-sm-12 col-12 col-md-12 selectpicker regulatory_dropdown_style show-tick form-control  testLbx"
                                                title="Select Trainer" SelectionMode="Multiple" data-live-search="true" data-size="5"></asp:ListBox>
                                        </div>
                                        <div class="form-group float-left col-lg-3  col-12 col-md-3 col-sm-12" runat="server" id="divAuthorName" visible="false">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Author</label>
                                                <input type='text' runat="server" id="txtAuthor" class="login_input_sign_up form-control" placeholder="Author" readonly="readonly" style="cursor: not-allowed;" />
                                            </div>
                                        </div>
                                        <div class="form-group float-left col-lg-3  col-12 col-md-3 col-sm-12" runat="server" id="divApproverName">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Approver<span class="mandatoryStar">*</span></label>
                                                <asp:DropDownList ID="ddlReviewerTTS" Visible="false" CssClass="selectpicker regulatory_dropdown_style form-control" data-live-search="true" data-size="6" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <input type='text' runat="server" id="txtReviewer" class="login_input_sign_up form-control" placeholder="Reviewer" visible="false" readonly="readonly" style="cursor: not-allowed;" />
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-3 float-left">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label for="lblDueDate">Due Date<span class="mandatoryStar">*</span></label>
                                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                    <input type='text' autocomplete="off" runat="server" id="txtTTSDueDate" value="" class="form-control login_input_sign_up" placeholder="DD MMM YYYY" onpaste="return false" />
                                                </div>
                                            </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                <ul class="nav nav-tabs tab_grid top">
                    <li class="nav-item "><a data-toggle="tab" class="active nav-link" href="#TraineeList">Trainee List</a></li>
                    <li class="nav-item "><a data-toggle="tab" class="nav-link" href="#Schedule" runat="server" id="Tab_TrainingSession" visible="false">Training Session</a></li>
                    <li class="nav-item "><a data-toggle="tab" class="nav-link" href="#History" runat="server" id="Tab_TargetHistory">History</a></li>
                </ul>
                <div class="tab-content">
                    <div id="TraineeList" class="tab-pane fade active show">
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none grid_panel_hod" id="gridSubmit">
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 ">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <div class="form-group  col-lg-4 col-12 col-md-4 col-sm-12 padding-none top float-left ">
                                            <label>Trainee Department</label>
                                            <asp:DropDownList ID="ddlTraineeDepartment" runat="server" data-live-search="true" Enabled="false" data-size="7" AutoPostBack="true"
                                                CssClass="float-left col-lg-12 col-sm-12 col-12 col-md-12 selectpicker regulatory_dropdown_style padding-none" OnSelectedIndexChanged="ddlTraineeDepartment_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none " id="divTraineeSelection">
                                            <div class="float-left col-lg-5 col-5 col-md-5 col-sm-5 padding-none panel_selection grid_panel_full">
                                                <div class="grid_header gridSearchHeadingTile col-12 float-left" style="padding: 3px;">
                                                    <h6 class="card-title float-left gridSearchTile">Select Trainee</h6>
                                                    <span data-toggle="tooltip" data-placement="top" style="color: #15afc2; border: 1px solid #fff; background: #fff; padding: 3px; border-radius: 4px; margin-left: 2px;" class="float-right glyphicon glyphicon-info-sign"
                                                        title="When Target Type is 'General', if the Trainee has already been Assigned with the same Training, the same would not be shown in the below list."></span>
                                                    <input name="txtTerm" class="col-6 gridSearchBox float-right" readonly="readonly" onfocus="this.removeAttribute('readonly');" autocomplete="off" type="search" oninput="GridViewSearch(this, '<%=gvTraineeSelector.ClientID %>')" placeholder="Search Select Trainees" />
                                                </div>
                                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 " style="min-height: 365px">
                                                    <asp:GridView ID="gvTraineeSelector" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table tableBody_Scrollheight  col-12 float-left datatable_cust fixGridHead top AspGrid Traineechckbx"
                                                        EmptyDataText="No Records" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                        <RowStyle CssClass="col-12 float-left gvRowStyle padding-none" />
                                                        <Columns>
                                                            <asp:TemplateField Visible="false">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox runat="server" onclick="chkHeaderSelect(this);" ID="chkHeaderElement" CssClass="chkHeaderElement"></asp:CheckBox>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" onclick="chkRowSelect(this);" ID="cbTraineeGv" CssClass="chkRowElement"></asp:CheckBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="EmpID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("EmpID") %>' ID="lblEmpIDGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="DeptID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("DeptID") %>' ID="lblEmpDeptIDGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Emp. Code">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox runat="server" Text='<%# Bind("EmpCode") %>' ID="TextBox2"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("EmpCode") %>' ID="lblEmpCodeGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Dept. Code">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox runat="server" Text='<%# Bind("DeptCode") %>' ID="txtDeptCode"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("DeptCode") %>' ID="lblDeptCodeGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Trainee Name">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox runat="server" Text='<%# Bind("EmpName") %>' ID="TextBox3"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("EmpName") %>' ID="lblTraineeGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>

                                            </div>

                                            <div class="float-left col-lg-2 col-2 col-md-2 col-sm-2" style="margin-top: 8em;">
                                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                                    <asp:Button ID="btnAddTrainees" CssClass="col-lg-4 float-left offset-lg-4 btn btn-signup_popup btnAddTrainers" Style="cursor: not-allowed;" runat="server" Text=">" OnClick="btnAddTrainees_Click" />
                                                    <asp:Button ID="btnRemoveTrainees" CssClass="col-lg-4 float-left offset-lg-4 btn btn-signup_popup" runat="server" Style="cursor: not-allowed;" Text="<" OnClick="btnRemoveTrainees_Click" />
                                                </div>
                                            </div>
                                            <div class="float-left col-lg-5 col-5 col-md-5 col-sm-5 panel_selection padding-none grid_panel_full ">
                                                <div class="grid_header col-12 float-left" style="padding: 3px;">
                                                    <h6 class="card-title float-left gridSearchTile">Selected Trainee</h6>
                                                    <input name="txtTerm" readonly="readonly" autocomplete="off" onfocus="this.removeAttribute('readonly');" type="search" class="col-6 float-right gridSearchBox" oninput="GridViewSearch(this, '<%=gvTraineeSelected.ClientID %>')" placeholder="Search Selected Trainees">
                                                </div>
                                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 " style="min-height: 331px">
                                                    <asp:GridView ID="gvTraineeSelected" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table tableBody_Scrollheight datatable_cust fixHead top AspGrid col-12 float-left Traineechckbx"
                                                        EmptyDataText="No Records" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                        <RowStyle CssClass="float-left col-12 gvRowStyle padding-none" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Select" Visible="false">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox runat="server" onclick="chkHeaderSelect(this);" ID="chkHeaderElement" CssClass="chkHeaderElement"></asp:CheckBox>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" onclick="chkRowSelect(this);" ID="cbTraineeGv" CssClass="chkRowElement"></asp:CheckBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="EmpID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("EmpID") %>' ID="lblEmpIDGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="DeptID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("DeptID") %>' ID="lblEmpDeptIDGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Emp. Code">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox runat="server" Text='<%# Bind("EmpCode") %>' ID="TextBox2"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("EmpCode") %>' ID="lblEmpCodeGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Dept. Code">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox runat="server" Text='<%# Bind("DeptCode") %>' ID="txtDeptCode"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("DeptCode") %>' ID="lblDeptCodeGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Trainee Name">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox runat="server" Text='<%# Bind("EmpName") %>' ID="TextBox3"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("EmpName") %>' ID="lblTraineeGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>

                                                </div>
                                                <div class="float-left" style="padding: 1px; height: 34px;">
                                                    <div class="top float-left padding-none" id="divLegend" style="display: none">
                                                        <div class="legend_images float-left" style="margin-left: 15px;">
                                                            <span class="reminder rowred"></span><span class="legends_grid" style="margin-left: 4px;"><b>Represent Cancelled Trainee(s)</b>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div id="Schedule" class="tab-pane fade">
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none grid_panel_hod">
                            <asp:UpdatePanel ID="upClassRoomSessionBtns" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-group float-left col-lg-12 col-sm-12 col-12 col-md-12 text-right" style="margin-top: 10px;" id="divClassroomActionBtns" runat="server" visible="false">
                                        <button type="button" class=" btn-signup_popup " id="btnReScheduleTrainingSession" onclick="OpenModelReschedule('Reschedule');" runat="server" visible="false">Re-Schedule Training Session</button>
                                        <asp:Button ID="btnNewTrainingSession" runat="server" class=" btn-signup_popup " OnClientClick="OpenModelSchedule('New');" OnClick="btnNewTrainingSession_Click" Text="Schedule Training Session" Visible="false" />
                                        <a class=" btn-signup_popup" href="#" onclick="bindTraineesBasedOnAttendance(1);" visible="false" runat="server" id="btnAttendees">Attendees</a>
                                        <a class=" btn-signup_popup " href="#" onclick="bindTraineesBasedOnAttendance(2);" id="btnNonAttendeesSchedule">Non Attendees</a>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="upTrainingSession" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div id="divClassRoomGrid" runat="server" visible="false">
                                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none">
                                            <table id="tblTrainingSessions" class="datatable_cust tblTrainingSessionsClass display breakword" style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th>TTS_TrainingSessionID</th>
                                                        <th>Trainer</th>
                                                        <th>Training Date</th>
                                                        <th>Start</th>
                                                        <th>End</th>
                                                        <th>Status</th>
                                                        <th>Exam Type</th>
                                                        <th>StatusID</th>
                                                        <th>Attendees</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="divOnlineGrid" runat="server" class="col-12" visible="false" style="width: 100%;">
                                        <div class="grid_header float-left col-lg-12 col-12 col-md-12 col-sm-12 bottom">
                                            <asp:Label ID="Label1" runat="server" Font-Bold="true">Online Session Details</asp:Label>
                                        </div>
                                        <div style="width: 100%;">

                                            <asp:GridView ID="gvOnlineTraining" runat="server" AutoGenerateColumns="False" OnRowCommand="gvOnlineTraining_RowCommand"
                                                DataKeyNames="TTS_ID,TTS_SessionID" CssClass="table table-fixed datatable fixgvHeadOnlineSession AspGrid"
                                                EmptyDataText="No Records" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" Width="100%">
                                                <AlternatingRowStyle BackColor="White" />
                                                <RowStyle CssClass="col-12 float-left col-lg-12 col-md-12 col-sm-12 float-left padding-none" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="TTS ID" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTTS_ID" runat="server" Text='<%# Bind("TTS_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="TTS SessionID" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTTS_SessionID" runat="server" Text='<%# Bind("TTS_SessionID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Due Date" SortExpression="DueDate" HeaderStyle-CssClass="col-2" ItemStyle-CssClass="col-2">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtDueDate" runat="server" Text='<%# Bind("DueDate") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDueDate" runat="server" Text='<%# Bind("DueDate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Trainer" SortExpression="Trainer" HeaderStyle-CssClass="col-2" ItemStyle-CssClass="col-2 textAlignLeft">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtTrainer" runat="server" Text='<%# Bind("Trainer") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTrainer" runat="server" Text='<%# Bind("Trainer") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Creation Date" SortExpression="CreationDate" HeaderStyle-CssClass="col-2" ItemStyle-CssClass="col-2">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtCreationDate" runat="server" Text='<%# Bind("CreationDate") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCreationDate" runat="server" Text='<%# Bind("CreationDate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status" SortExpression="Status" HeaderStyle-CssClass="col-2" ItemStyle-CssClass="col-2 textAlignLeft">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtStatus" runat="server" Text='<%# Bind("Status") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Remarks" SortExpression="Remarks" HeaderStyle-CssClass="col-3" ItemStyle-CssClass="col-3 textAlignLeft">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="View" HeaderStyle-CssClass="col-1" ItemStyle-CssClass="col-1">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkViewOnlineTraining" CssClass="view paddingOverwrite" runat="server" CommandName="ViewOnlineTraining" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' ToolTip="Click To View"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div id="History" class="tab-pane fade grid_panel_hod float-left float-left col-lg-12 col-sm-12 col-12 col-md-12 ">
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                            <table id="tblTTS_ActionHistory" class="datatable_cust tblTTS_ActionHistoryClass display breakword" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>Action</th>
                                        <th>Role</th>
                                        <th>Action By</th>
                                        <th>Action Date</th>
                                        <th>Remarks</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <div class="form-group float-left col-6 padding-right_div" runat="server" visible="false" id="divAuthorComments">
                            <label for="inputPassword3" class=" col-sm-12 control-label padding-none custom_label_answer">Author Comments</label>
                            <div class="col-sm-12 col-12 col-md-12 float-left col-lg-12 padding-none">
                                <textarea class="form-control" maxlength="250" runat="server" rows="5" id="txtAuthorComments"></textarea>
                            </div>
                        </div>
                        <div class="form-group float-left col-6 padding-none" runat="server" visible="false" id="divReviewerComments">
                            <label for="inputPassword3" class=" col-sm-12 control-label padding-none custom_label_answer">Reviewer Comments</label>
                            <div class="col-sm-12 col-12 col-md-12 float-left col-lg-12 padding-none">
                                <textarea class="form-control" runat="server" maxlength="250" rows="5" id="txtReviewerComments" placeholder="Reviewer Comments"></textarea>
                            </div>
                        </div>
                    </div>
                    <div id="divTTS_ActionBtns">
                        <div id="divHodActionBtns" runat="server" class="form-group text-right float-right col-lg-12 col-sm-12 col-12 col-md-12 padding-none" visible="false">

                            <asp:Button ID="btnHodSubmit" runat="server" Text="Create" OnClick="btnHodSubmit_Click" title="Create" OnClientClick="return TargetTrainingValidations();" CssClass="btn-signup_popup" />

                            <%--<a href="#" id="btnReset" class=" btn-revert_popup" onclick="TTS_Created_Success();">Reset</a>--%>
                            <a href="#" runat="server" id="divHodReset" class=" btn-revert_popup" title="Reset" onclick="TTS_Created_Success();">Reset</a>

                            <input type="button" class="btn-cancel_popup" visible="false" runat="server" id="btnHodCancel" title="Cancel" onclick="gotoPendingList();" value="Cancel" />
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">

                            <ContentTemplate>
                                <div runat="server" id="divCloseTargetTraining" visible="false">
                                    <label>Comments<span class="mandatoryStar">*</span></label>
                                    <textarea class="form-control" runat="server" maxlength="250" rows="3" id="txtCloseComments" placeholder="Close Comments"></textarea>
                                    <asp:Button ID="btnTargetTrainingCancel" runat="server" Text="Close Target Training Schedule" ToolTip="Click To Close Target Training Schedule" OnClientClick="CloseTargetTraining();" CssClass="btn-signup_popup float-right " />
                                </div>
                            </ContentTemplate>

                        </asp:UpdatePanel>
                        <div id="divQaActions" runat="server" class=" text-right padding-none" visible="false">
                            <asp:UpdatePanel ID="upQA_Approval" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnQaApprove" runat="server" Text="Approve" OnClick="btnQaApprove_Click" CssClass="btn-signup_popup" />
                                    <asp:Button ID="btnQaRevert" runat="server" Text="Revert" CssClass=" btn-cancel_popup" OnClick="btnQaRevert_Click" />
                                    <asp:Button ID="btnQaCancel" runat="server" Visible="false" Text="Cancel" CssClass=" btn-cancel_popup" OnClientClick="gotoApprovalList();" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal popup  for Getting TTS Trainees Online List-->
    <div id="modalOnlineTraineeDetails" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <asp:Label ID="lblExamDetails" CssClass="modal-title" runat="server" Text="Trainee Details"></asp:Label>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <table id="tblTTS_OnlineTraineeDetails" class="display datatable_cust tblTTS_OnlineTraineeDetailsClass breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>TargetExamID</th>
                                    <th>EmpID</th>
                                    <th>Emp.Code</th>
                                    <th>Emp.Name</th>
                                    <th>Attempts</th>
                                    <th>Status</th>
                                    <th>StatusID</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-------- End Online Trainees List-------------->

    <!-------- Attendees Log-------------->
    <div id="modelAttendanceLog" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Trainee Attendance</h4>
                    <button type="button" class="close TabFocus" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body float-left col-lg-12 col-sm-12 col-12 col-md-12 ">
                    <div id="HideNote" class="" style="padding-left: 16px; color: #049aaf; font-weight: bold;">
                        Note: Click on the Rows to Select Attendees
                    </div>
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <table id="tblTraineeAttendance" class="test datatable_cust  display tblTraineeAttendanceClass breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>TraineeID</th>
                                    <th>Emp Code</th>
                                    <th>Trainee Name</th>
                                    <th>Department</th>
                                    <th>Exam Type</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="form-group col-lg-3  col-12 col-md-3 col-sm-12  float-left">
                        <div id="HideExamType">
                            <label>Exam Type</label>
                            <asp:UpdatePanel runat="server" ID="upExamType" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlExamType" CssClass="col-lg-12 col-sm-12 col-12 coTraining Session l-md-12 selectpicker regulatory_dropdown_style padding-none" runat="server">
                                        <asp:ListItem Text="--Select Exam Type--" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Written" Value="W"></asp:ListItem>
                                        <asp:ListItem Text="Oral" Value="O"></asp:ListItem>
                                        <asp:ListItem Text="Practical" Value="P"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-12 text-right float-left" id="AttendeesButtons">
                        <button type="button" class=" btn-cancel_popup float-right" style="margin-left: 5px;" data-dismiss="modal">Cancel</button>
                        <asp:UpdatePanel runat="server" ID="upAttandenceSubmit" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:Button ID="btnAttandenceSubmit" runat="server" Text="Submit" CssClass="float-right btn-signup_popup" OnClick="btnAttandenceSubmit_Click" OnClientClick="return AttandenceValidate();" />
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---- End of Attendees Log ---->

    <!-------- Attendees and Non-Attendees------------->
    <div id="modelAttendees" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="min-width: 54%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">
                        <asp:Label ID="lblScheduleAttendees" runat="server" Text=""></asp:Label>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class=" col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <table id="tblTTS_TraineesByAttendence" class="datatable_cust display tblTTS_TraineesByAttendenceClass breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Emp Code</th>
                                    <th>Emp Name</th>
                                    <th>Department</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-------- End Attendees-------------->

    <!--------Training Session Trainees Model---------->
    <div id="modelSessionTraineesForOral" role="dialog" class="modal department fade">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Trainees List</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body col-lg-12 col-sm-12 col-12 col-md-12">
                    <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <table id="tblSessionTraineesListForOralandPractical" class="datatable_cust display tblSessionTraineesListForOralandPracticalClass breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Training Session ID</th>
                                    <th>Trainee ID</th>
                                    <th>EvaluationStatus</th>
                                    <th>TargetExam_ID</th>
                                    <th>Emp.Code</th>
                                    <th>Trainee Name</th>
                                    <th>Department</th>
                                    <th>Attempts</th>
                                    <th>StatusText</th>
                                    <th>EmpFeedbackID</th>
                                    <th>TrainerSatisfactory</th>
                                    <th>EvaluationRemarks</th>
                                    <th>Status</th>
                                    <th>Feedback</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!---- End---->

    <!--------Training Session Trainees Model---------->
    <div id="modelSessionTraineesForWritten" role="dialog" class="modal department fade">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Trainees List</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body col-lg-12 col-sm-12 col-12 col-md-12">
                    <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <table id="tblSessionTraineesListForWritten" class="datatable_cust display tblSessionTraineesListForWrittenClass breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Training Session ID</th>
                                    <th>Trainee ID</th>
                                    <th>EvaluationStatus</th>
                                    <th>TargetExam_ID</th>
                                    <th>Emp.Code</th>
                                    <th>Trainee Name</th>
                                    <th>Department</th>
                                    <th>Attempts</th>
                                    <th>StatusText</th>
                                    <th>EmpFeedbackID</th>
                                    <th>TrainerSatisfactory</th>
                                    <th>EvaluationRemarks</th>
                                    <th>Status</th>
                                    <th>Feedback</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!---- End---->

    <!--------Re Schedule Training Session Model-------------->
    <div id="modelReSchedule" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">
                        <asp:Label ID="lblReScheduleTitle" runat="server">Re-Schedule Training Session</asp:Label>
                    </h4>
                    <button type="button" class="close TabFocus" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class=" col-lg-12 col-sm-12 col-12 col-md-12 float-left padding-none">
                        <asp:UpdatePanel runat="server" ID="upReSchedule" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-group float-left col-sm-4  padding-none">
                                    <label for="lblScheduleDate" class=" padding-none float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">
                                        Date<span style="color: red; font: x-large"></span></label>
                                    <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                        <input type='text' autocomplete="off" runat="server" id="txtReSchedulDate" class="login_input_sign_up form-control" placeholder="DD MMM YYYY" onpaste="return false" />
                                    </div>
                                </div>
                                <div class="col-sm-4 float-left input-group float-left form-group bootstrap-timepicker timepicker">
                                    <label for="txtScheduleStartTime" class=" col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">
                                        Start Time<span style="color: red; font: x-large"></span></label>
                                    <div class="col-lg-12 col-sm-12 col-12 col-md-12 ">
                                        <asp:TextBox ID="txtReScheduleStartTime" autocomplete="off" CssClass="login_input_sign_up form-control input-small TimePicker" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-4  float-left input-group float-left form-group bootstrap-timepicker timepicker">
                                    <label for="txtScheduleEndTime" class=" padding-none col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">
                                        End Time<span style="color: red; font: x-large"></span></label>
                                    <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-right_div">
                                        <asp:TextBox ID="txtReScheduleEndTime" autocomplete="off" CssClass="login_input_sign_up form-control input-small TimePicker" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class=" col-lg-12 col-sm-12 col-12 col-md-12 float-left padding-none">
                        <div class="form-group col-lg-12 col-sm-12 col-12 col-md-12 padding-right_div ">
                            <label for="inputEmail3" class=" padding-none col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">Comments<span class="mandatoryStar">*</span></label>
                            <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                <textarea class="form-control textarea_height" runat="server" rows="5" maxlength="250" id="txtCommentsReSchedule"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel runat="server" ID="upbtnUpdateReSchedule" UpdateMode="Conditional" style="display: inline-block;">
                        <ContentTemplate>
                            <asp:Button ID="btnUpdateReSchedule" CssClass="btn btn-signup_popup" runat="server" Text="Update" OnClick="btnUpdateReSchedule_Click" OnClientClick="return ReScheduleValidation();" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="btn btn-cancel_popup" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--End of Re Schedule Training Session model-->

    <!-------- Schedule Training Session Model-------------->
    <div id="modelScheduleNew" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">
                        <asp:Label ID="lblNewTraingSession" runat="server" Text="New Training Session"></asp:Label>
                    </h4>
                    <button type="button" class="close TabFocus" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class=" col-lg-12 col-sm-12 col-12 col-md-12 padding-none float-left">
                        <div class="form-group col-sm-4  padding-none float-left">
                            <label class=" float-left padding-none col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">Mode of Training</label>
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                <asp:DropDownList ID="ddlModeOfTraining" CssClass="selectpicker regulatory_dropdown_style show-tick form-control" onchange="HideShowSessionsDiv($('option:selected', this).val());" runat="server">
                                    <asp:ListItem Text="Classroom" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Online" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none" id="divClassRoom">
                        <div class="form-group col-sm-4 float-left padding-none">
                            <label for="lblScheduleDate" class=" padding-none float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">Date<span class="mandatoryStar">*</span></label>
                            <div class="col-lg-12 col-sm-12 float-left  col-12 col-md-12 padding-none">
                                <input type='text' runat="server" autocomplete="off" id="txtNewScheduleDate" class="form-control login_input_sign_up" placeholder="DD MMM YYYY" onpaste="return false" />
                            </div>
                        </div>
                        <div class="col-sm-4  float-left input-group form-group bootstrap-timepicker timepicker">
                            <label for="txtScheduleStartTime" class="  col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">Start Time<span class="mandatoryStar">*</span></label>
                            <div class="col-lg-12 col-sm-12 col-12 col-md-12 float-left ">
                                <asp:TextBox ID="txtScheduleNewStart" autocomplete="off" CssClass="form-control login_input_sign_up input-small TimePicker" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-4  float-left input-group form-group bootstrap-timepicker timepicker">
                            <label for="txtScheduleEndTime" class=" padding-none col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">End Time<span class="mandatoryStar">*</span></label>
                            <div class="col-lg-12 float-left col-sm-12 col-12 col-md-12 padding-right_div">
                                <asp:TextBox ID="txtScheduleNewEnd" autocomplete="off" CssClass="form-control login_input_sign_up input-small TimePicker" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 float-left col-sm-12 col-12 col-md-12 padding-right_div ">
                            <label for="inputEmail3" class=" padding-none col-lg-12 float-left col-sm-12 col-12 col-md-12 control-label custom_label_answer">Comments</label>
                            <div class="col-lg-12 col-sm-12 col-12 float-left col-md-12 padding-none">
                                <textarea class="form-control" runat="server" maxlength="250" id="txtScheduleNewComments"></textarea>
                            </div>
                        </div>
                    </div>

                    <!--Online Div-->
                    <div class=" col-lg-12 col-sm-12 col-12 col-md-12 float-left padding-none" id="divOnline" style="display: none">
                        <div class="form-group col-sm-4 float-left padding-none">
                            <label for="lblScheduleDueDate" class=" padding-none col-lg-12 col-sm-12 col-12 col-md-12 float-left control-label custom_label_answer">Due Date<span class="mandatoryStar">*</span></label>
                            <div class="col-lg-12 col-sm-12 col-12 col-md-12 float-left padding-none">
                                <input type='text' runat="server" id="txtOnlineDueDate" onpaste="return false;" autocomplete="off" class="form-control" placeholder="DD MMM YYYY" />
                                <input type='text' runat="server" autocomplete="off" style="display: none" class="form-control" placeholder="DD MMM YYYY" onpaste="return false" />
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-sm-12 col-12 col-md-12 float-left padding-right_div">
                            <label for="inputEmail3" class=" padding-none col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">Comments<%--<span visible="false" id="spanMandatoryOnlineComments" class="smallred">*</span>--%></label>
                            <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                <textarea class="form-control" runat="server" maxlength="250" id="txtCommentsOnlineTraining"></textarea>
                            </div>
                            <div id="divCheckbox" style="display: none" class="padding-none col-lg-4 col-sm-12 col-12 col-md-4 float-left">
                                <%--<asp:CheckBox runat="server" ID="chkbxOnline" CssClass="chkOnline"></asp:CheckBox>--%>
                                <label class=" form-check-label">
                                    <input type="checkbox" id="chkbxOnline" runat="server" class="chkOnline form-check-input">
                                    <span style="padding-left: 21px;">Trainee can Self-Complete </span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div id="divWarningMsg" style="display: none" class="padding-none col-lg-4 col-sm-12 col-12 col-md-4">
                        <span class="float-left" style="color: orangered; font-weight: bold; margin-right: 6px;">Warning :</span>
                        <label id="lblWarningMsg" style="color: #2e2e2e;" class="control-label float-left"></label>
                    </div>
                </div>

                <div class="modal-footer">
                    <asp:UpdatePanel runat="server" ID="upbtnSubmitSchedule" UpdateMode="Conditional" style="display: inline-block;">
                        <ContentTemplate>
                            <asp:Button ID="btnNewSession" CssClass=" btn-signup_popup"
                                OnClientClick="return NewScheduleValidation();"
                                runat="server" Text="Submit" OnClick="btnSubmitSchedule_Click" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnNewSession" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <button type="button" class=" btn-cancel_popup" data-dismiss="modal">Cancel</button>
                </div>

            </div>
        </div>
    </div>
    <!--End of Schedule Training Session model-->

    <!--------View Trainer Evaluation Opinion---------->
    <div id="modelTrainerEvaluationOpinion" role="dialog" class="modal department fade">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content"> 
                <div class="modal-header">
                    <h4 class="modal-title">Trainer Opinion</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body col-lg-12 col-sm-12 col-12 col-md-12">
                    <div id="divOpinion">
                        <div class=" padding-none col-sm-12 col-12 col-lg-12 col-md-12 float-left" style="cursor: not-allowed" data-toggle="buttons">
                            <label for="inputPassword3" class=" col-lg-12 col-sm-12 col-12 col-md-12  padding-none control-label">Opinion</label>
                            <label class="btn btn-primary_radio col-6 col-lg-6 col-md-6 col-sm-6 float-left active radio_check" id="lblRadioSatisfactory" runat="server" disabled="disabled" style="pointer-events: none">
                                <asp:RadioButton ID="rbtnSatisfactory" runat="server" GroupName="A" Text="Satisfactory" Enabled="false" />
                            </label>
                            <label class="btn btn-primary_radio col-6 col-lg-6 col-md-6 col-sm-6 float-left radio_check" id="lblRadioNotSatisfactory" runat="server" disabled="disabled" style="pointer-events: none">
                                <asp:RadioButton ID="rbtnNotSatisfactory" runat="server" GroupName="A" Text="Not Satisfactory" Enabled="false" />
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <label for="inputPassword3" class=" col-lg-12 col-sm-12 col-12 col-md-12 control-label padding-none custom_label_answer">Comments</label>
                        <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                            <asp:TextBox ID="txtEvaluationComments" runat="server" TextMode="MultiLine" Rows="5" MaxLength="250" placeholder="Enter Comments" Enabled="false" CssClass="form-control feed_text"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!---- End Trainer Opinion---->

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upServerButtons" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:Button ID="btnViewExamAnswerSheet" runat="server" OnClick="btnViewExamAnswerSheet_Click" Text="" />
                <asp:Button ID="btnAskFeedBack" runat="server" Text="Button" OnClick="btnAskFeedBack_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
    <uc1:ucExamResultSheet runat="server" ID="ucExamResultSheet" />
    <uc1:ucFeedback runat="server" ID="ucFeedback" />

    <script>
        function closemodalSchedule() {
            $('#modelScheduleNew').modal('hide');
        }

        function lblWarningText() {
            $('#lblWarningMsg').text("Questionnaire Doesn't Exist");
            $('#divWarningMsg').show();
            $('#divCheckbox').show();
        }

    </script>

    <!--script of Due date-->
    <script>
        $(function () {
                     loadDatePickers();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            loadDatePickers();
        });

        function loadDatePickers() {
            var tts_Status = $('#<%=hdnTTS_CurrentStatus.ClientID%>').val();
            if (tts_Status == "0" || tts_Status == "2") {
                $('#<%=txtTTSDueDate.ClientID%>').datetimepicker({
                    format: 'DD MMM YYYY',
                    minDate: new Date('<%=hdnMinDate.Value%>'),
                    useCurrent: true,
                });
            }
        }

    </script>

    <!---To fix Header-->
    <script type="text/javascript">
        function fixOnlineSessionHeader() {
            var tbl = document.getElementsByClassName("fixgvHeadOnlineSession");
            // Fix up GridView to support THEAD tags     
            if ($(".fixgvHeadOnlineSession").find("thead").length == 0) {
                $(".fixgvHeadOnlineSession tbody").before("<thead><tr></tr></thead>");
                $(".fixgvHeadOnlineSession thead tr").append($(".fixgvHeadOnlineSession th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixgvHeadOnlineSession tbody tr:first").remove();
                }
            }
        }
    </script>

    <!--TTS_Action History jQuery DataTable-->
    <script>
        var oTableTTS_ActionHistory;
        function loadTTS_ActionHistory() {
            if (oTableTTS_ActionHistory != null) {
                oTableTTS_ActionHistory.destroy();
            }
            oTableTTS_ActionHistory = $('#tblTTS_ActionHistory').DataTable({
                columns: [
                    { 'data': 'ActionStatus' },
                    { 'data': 'ActionRole' },
                    { 'data': 'ActionBy' },
                    { 'data': 'ActionDate' },
                    { 'data': 'Remarks' }
                ],
                "order": [[0]],
                "scrollY": "250px",
                "aoColumnDefs": [{ className: "textAlignLeft", "targets": [0, 1, 2, 4] },{"orderable": false, "targets": [0,1,2,3,4]}],
                "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
                <%--"sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_ActionHistoryService.asmx/GetTTS_ActionHistory")%>",--%>
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TMS_ActionHistoryService.asmx/GetTMS_ActionHistory")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                   <%--aoData.push({ "name": "TTS_ID", "value": <%=hdnTTS_ID.Value%> });--%>
                    aoData.push({ "name": "Base_ID", "value": <%=hdnTTS_ID.Value%> },{ "name": "Table_ID", "value": "1"});
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }

    </script>

    <!--Training Mode Selection-->
    <script>    
        function HideShowSessionsDiv(ModeofTraining) {
            if (ModeofTraining == 1) {
                $('#divClassRoom').show();
                $('#divOnline').hide();
            }
            else {
                $('#divOnline').show();
                $('#divClassRoom').hide();
                $('.chkOnline input').prop('checked', false);
            }
        }
        function ShowHideOnlineClassroomGrids(TrainingMode) {
            if (TrainingMode == 'ClassRoom') {
                $('#divClassRoomGrid').show();
                $('#divOnlineGrid').hide();
            }
            else {
                $('#divClassRoomGrid').hide();
                $('#divOnlineGrid').show();
            }
        }
    </script>

    <!--Trainees Attendence Non Attendence-->
    <script>      
        var oTableTrainees; var AttendanceStatus = 0;
        function loadTrainees() {
            if (oTableTrainees != null) {
                oTableTrainees.destroy();
            }
            oTableTrainees = $('#tblTTS_TraineesByAttendence').DataTable({
                columns: [
                    { 'data': 'EmpCode' },
                    { 'data': 'EmpName' },
                    { 'data': 'DepartmentName' }
                ],
                "scrollY": "371px",
                "order": [1, "desc"],
                "aoColumnDefs": [{ className: "textAlignLeft", "targets": [1, 2] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TraineesAttendenceService.asmx/TraineesAttendence")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "TTS_ID", "value": <%=hdnTTS_ID.Value%> },
                        { "name": "AttendenceStatus", "value": AttendanceStatus });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }

        function bindTraineesBasedOnAttendance(AttendanceStatusAt) {
            AttendanceStatus = AttendanceStatusAt;
            if (AttendanceStatusAt == 1) {
                $('#<%=lblScheduleAttendees.ClientID%>').text("Attended Trainees");
                $('#modelAttendees').modal({ backdrop: 'static', keyboard: false });

            }
            else {
                $('#<%=lblScheduleAttendees.ClientID%>').text("Non Attended Trainees");
                $('#modelAttendees').modal({ backdrop: 'static', keyboard: false });

            }
        }
        $('#modelAttendees').on('shown.bs.modal', function (e) {
            loadTrainees();
        });

        function showDetails() {
            //so something funky with the data
        }
    </script>

    <!--Maintain Date in Training Session-->
    <script>
        $(function () {
            $('#<%=txtNewScheduleDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                minDate: new Date('<%=hdnMinDate.Value%>'),
                useCurrent: false,
            });
        });
    </script>

    <!--Online Training Session Date Validation-->
    <script>
        $(function () {
            $('#<%=txtOnlineDueDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                minDate: new Date('<%=hdnMinDate.Value%>'),
                useCurrent: false,
            });
        });

        $(function () {
            bindDatePickerReScheduleDate();
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            bindDatePickerReScheduleDate();
        });

        function bindDatePickerReScheduleDate() {
            $('#<%=txtReSchedulDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                minDate: new Date('<%=hdnMinDate.Value%>'),
                useCurrent: false,
            });
        }

    </script>

    <!--View UI based on Document Type Selection-->
    <script>      
        function showHideDivsByDocType(optionValue, targetType) {
            if (optionValue == '1') { //Training Process 1
                $(".TargetTypeDiv").show();
                if (targetType == '1') {
                    $(".MonthDiv").show();
                }
                else {
                    $(".MonthDiv").hide();
                }
            }
            else { //Training Process 2
                $(".TargetTypeDiv").hide();
                $(".MonthDiv").hide();
            }
        }
        function showHideDivsByTargetType(optionValue) {
            if (optionValue == '1') { //Training Process        
                $(".MonthDiv").show();
            }
            else {
                $(".MonthDiv").hide();
            }
        }
    </script>

    <!--Open Training Session Models-->
    <script>
        function OpenModelSchedule(Type) {
            document.getElementById("<%=txtNewScheduleDate.ClientID%>").value = '';
            document.getElementById("<%=txtScheduleNewStart.ClientID%>").value = '';
            document.getElementById("<%=txtScheduleNewEnd.ClientID%>").value = '';
            document.getElementById("<%=txtScheduleNewComments.ClientID%>").value = '';
            document.getElementById("<%=txtOnlineDueDate.ClientID%>").value = '';
            document.getElementById("<%=txtCommentsOnlineTraining.ClientID%>").value = '';
            if (Type == "New") {
                $('#modelScheduleNew').modal({ backdrop: 'static', keyboard: false });
            }
        }

        function OpenModelReschedule(Type) {
            document.getElementById("<%=txtCommentsReSchedule.ClientID%>").value = '';
            if (Type == "Reschedule") {
                $('#modelReSchedule').modal({ backdrop: 'static', keyboard: false });
            }
        }
    </script>

    <!--Validating TTS-->
    <script>
        function TargetTrainingValidations() {
            var TodayDate = document.getElementById("<%=hdnMinDate.ClientID%>").value;
            var Department = document.getElementById("<%=ddlDepartmentTTS.ClientID%>");
            var DocumentType = document.getElementById("<%=ddlDocumentType.ClientID%>");
            var Document = document.getElementById("<%=ddlTrainingDocument.ClientID%>");
            var Trainer = document.getElementById("<%=lbxTrainersTTS.ClientID%>");
            var Reviewer = document.getElementById("<%=ddlReviewerTTS.ClientID%>");
            var Month = document.getElementById("<%=ddlRefresherMonth.ClientID%>");
            var TargetType = document.getElementById("<%=ddlTargetType.ClientID%>");
            var DueDate = document.getElementById("<%=txtTTSDueDate.ClientID%>");
            errors = [];
            if (Department.value == 0) {
                errors.push("Select Department");
            }
            //if (DocumentType.value == 0) {
            //    errors.push("Select DocumentType");
            //}
            if (TargetType.value == 0) {
                errors.push("Select Target Training Type");
            }
            if (TargetType.value == 1) {
                if (Month.value == 0) {
                    errors.push("Select Month");
                }
            }
            if (Document.value == 0) {
                errors.push("Select Document");
            }
            if (Trainer.value == 0) {
                errors.push("Select Trainer");
            }
            if (Reviewer.value == 0) {
                errors.push("Select Approver");
            }
            if (DueDate.value == "") {
                errors.push("Select Due Date");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ShowPleaseWait('show');
                return true;
            }
        }
    </script>
    <!--New Training Session Validation-->
    <script>
        function NewScheduleValidation() {
            if (document.getElementById("<%=ddlModeOfTraining.ClientID%>").value == 1) {
                var txtScheduleDate = document.getElementById("<%=txtNewScheduleDate.ClientID%>").value;
                var txtScheduleStartTime = document.getElementById("<%=txtScheduleNewStart.ClientID%>").value;
                var txtScheduleEndTime = document.getElementById("<%=txtScheduleNewEnd.ClientID%>").value;
                var txtCommentsSchedule = document.getElementById("<%=txtScheduleNewComments.ClientID%>").value;
                errors = [];
                if (txtScheduleDate == "") {
                    errors.push("Choose Date");
                }
                if (txtScheduleStartTime == "") {
                    errors.push("Select Start Time");
                }
                if (txtScheduleEndTime == "") {
                    errors.push("Select End Time");
                }
                //if (txtScheduleEndTime != 0) {
                //    if (txtScheduleStartTime >= txtScheduleEndTime) {
                //        errors.push("End Time should be greater than start Time");
                //    }
                //}
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                    return false;
                }
                else {
                     ShowPleaseWait('show');
                    return true;
                }
            }
            else {
                //code for validating the Online training
                var txtDueDate = document.getElementById("<%=txtOnlineDueDate.ClientID%>").value;
               // var txtOnlineComments = document.getElementById("<%=txtCommentsOnlineTraining.ClientID%>").value;
                errors = [];
                if (txtDueDate == "") {
                    errors.push("Select Due Date");
                }
                //if (txtOnlineComments == "") {
                //    errors.push("Enter Notes in Comments");
                //}
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                    return false;
                }
                else {
                   ShowPleaseWait('show');
                    return true;
                }
            }
        }
    </script>

    <!--Re-schedule Training Session Validation-->
    <script>
        function ReScheduleValidation() {
            var txtReScheduleDate = document.getElementById("<%=txtReSchedulDate.ClientID%>").value;
            var txtReScheduleStartTime = document.getElementById("<%=txtReScheduleStartTime.ClientID%>").value;
            var txtReScheduleEndTime = document.getElementById("<%=txtReScheduleEndTime.ClientID%>").value;
            var txtReScheduleComments = document.getElementById("<%=txtCommentsReSchedule.ClientID%>").value;
            errors = [];
            if (txtReScheduleDate == "") {
                errors.push("Choose Date");
            }
            if (txtReScheduleStartTime == "") {
                errors.push("Select Start Time");
            }
            if (txtReScheduleEndTime == "") {
                errors.push("Select End Time");
            }
            if (txtReScheduleComments == "") {
                errors.push("Enter Reason for Modification in Comments");
            }
            if (txtReScheduleEndTime != 0) {
                if (txtReScheduleStartTime >= txtReScheduleEndTime) {
                    errors.push("End Time should be greater than start Time");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ShowPleaseWait('show');
                return true;
            }
        }
    </script>

    <!--for gridview check box selection when we click on row-->
    <script>
        $(document).ready(function () {
            HideShowSessionsDiv(1);
            $('.gvRowStyle').click(function (event) {
                if (event.target.type !== 'checkbox') {
                    $(':checkbox', this).trigger('click');
                }
            });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.gvRowStyle').click(function (event) {
                if (event.target.type !== 'checkbox') {
                    $(':checkbox', this).trigger('click');
                }
            });
        });
    </script>


    <!--for gridview check box selection when we click on row-->
    <script>
        $(document).ready(function () {
            if ($(<%=hdnModeOfTraining.ClientID%>).val() == "1") {
                loadClassRoomSession();
            }
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            if ($(<%=hdnModeOfTraining.ClientID%>).val() == "1") {
                loadClassRoomSession();
            }
        });
    </script>

    <!--Redirecting the Page-->
    <script>
        function TTS_Created_Success() {
            ShowPleaseWait('hide');
            window.open("<%=ResolveUrl("~/TMS/TargetTrainingSchedule/TTS_Master.aspx?TTS_Type=Create")%>", "_self");
        }
        function gotoPendingList() {
            ShowPleaseWait('hide');
            window.open('<%=ResolveUrl("~/TMS/TargetTrainingSchedule/TTS_Pending.aspx?FilterType=2")%>', '_self');
        }
        function gotoApprovalList() {
            window.open("<%=ResolveUrl("~/TMS/TargetTrainingSchedule/TTS_ApprovalList.aspx?FilterType=2")%>", "_self");
        }
        function TTS_QA_RevertReason() {
            var CommentsTxt = document.getElementById("<%=txtReviewerComments.ClientID%>");
            CommentsTxt.focus();
        }
    </script>

    <!--Response Messages on Actions-->
    <script>
        function TraineeAttendanceresult(ResultID) {
            if (ResultID == "1") {
                custAlertMsg("Attendance Submitted Successfully ! On Selected Trainees", "success", "ReloadSessionDetails();");
                $('#<%=txtStatusTTS.ClientID%>').val('In Progress');
            }
            else if (ResultID == "2") {
                custAlertMsg("There is no Questionnaire on this Document.", "error");
            }
            else if (ResultID == "3") {
                custAlertMsg("Attendance Submitted and Target Training is Completed !", "success", "ReloadSessionDetails();");
                $('#<%=txtStatusTTS.ClientID%>').val('In Progress');
            }
            else if (ResultID == "4") {
                custAlertMsg("Attendance Submitted Successfully!", "success", "ReloadSessionDetails();");
                $('#<%=txtStatusTTS.ClientID%>').val('Training Completed');
            }
            else if (ResultID == "5") {
                custAlertMsg("Session already Conducted", "warning", "ReloadSessionDetails();");
            }
            else if (ResultID == "6") {
                custAlertMsg("Session already Conducted", "warning", "ReloadSessionDetails();");
            }
        }
    </script>

    <!--For Trainee ASP Grid Search-->
    <script>      
        function GridViewSearch(phrase, _id) {
            var words = phrase.value.toLowerCase().split(" ");
            var table = document.getElementById(_id);
            var ele;
            for (var r = 1; r < table.rows.length; r++) {
                ele = table.rows[r].innerHTML.replace(/<[^>]+>/g, "");
                var displayStyle = 'none';
                for (var i = 0; i < words.length; i++) {
                    if (ele.toLowerCase().indexOf(words[i]) >= 0)
                        displayStyle = '';
                    else {
                        displayStyle = 'none';
                        break;
                    }
                }
                table.rows[r].style.display = displayStyle;
            }
        }
    </script>

    <!--Training Session jQuery DataTable-->
    <script>
        var TodayDate = document.getElementById("<%=hdnMinDate.ClientID%>").value;
        var oTableClassroomSession;
        function loadClassRoomSession() {
            if (oTableClassroomSession != null) {
                oTableClassroomSession.destroy();
            }
            oTableClassroomSession = $('#tblTrainingSessions').DataTable({
                columns: [
                    { 'data': 'TTS_TrainingSessionsID' },
                    { 'data': 'Trainer' },
                    { 'data': 'TrainingDate' },
                    { 'data': 'StartTime' },
                    { 'data': 'EndTime' },
                    { 'data': 'StatusName' },
                    { 'data': 'ExamType' },
                    { 'data': 'StatusID' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            if ((o.StatusID == "1" || o.StatusID == "2") && o.TrainingDate == TodayDate && $(<%=hdnIsAssignedTrainer.ClientID%>).val() == "true") {
                                return '<a class="attendence_LogPending_tms"  href="#" onclick="BindToTrainingSessionGrid(' + o.TTS_TrainingSessionsID + ',' + o.StatusID + ');"></a>';
                            }
                            else if (o.StatusID >= "3") {
                                if (o.ExamType == "Oral" || o.ExamType == "Practical") {
                                    return '<a class="view"  href="#" onclick="BindTraineeListOfOralOrPractical(' + o.TTS_TrainingSessionsID + ',\'' + o.ExamType + '\');"></a>';
                                }
                                else if (o.ExamType == "Written") {
                                    return '<a class="view"  href="#" onclick="BindTraineeListOfWritten(' + o.TTS_TrainingSessionsID + ');"></a>';
                                }
                                else {
                                    return '';
                                }
                            }
                            else {
                                return '';
                            }
                        }
                    }
                ],
                "scrollY": "360px",
                "order": [6, "asc"],
                "aoColumnDefs": [{ "targets": [0, 7], "visible": false }, { className: "textAlignLeft", "targets": [1, 5, 6] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/GetTTS_TraingSessionList")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "TTS_ID", "value": <%=hdnTTS_ID.Value%> });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }

        function ReloadSessionDetails() {
            loadClassRoomSession();
            $('#modelScheduleNew').modal('hide');
            $('#modelReSchedule').modal('hide');
            $('#modelAttendanceLog').modal('hide');
            ShowPleaseWait('hide');
        }
    </script>

    <!--TO load Columns for self complete--->
    <script>
        var oTableOnlineTraineeSelfDetails
        function loadOnlineTraineeSelfDetails() {
            if (oTableOnlineTraineeSelfDetails != null) {
                oTableOnlineTraineeSelfDetails.destroy();
            }
            oTableOnlineTraineeSelfDetails = $('#tblTTS_OnlineTraineeDetails').DataTable({
                columns: [
                    { 'data': 'TargetExamID' },
                    { 'data': 'EmpID' },
                    { 'data': 'EmpCode' },
                    { 'data': 'EmpName' },
                    { 'data': 'Attempts' },
                    { 'data': 'Status' },
                    { 'data': 'StatusID' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            if (o.TargetExamID == 0) {
                                return ' ';
                            }
                            else if (o.StatusID != 2) {
                                return ' ';
                            }
                            else {
                                return '<a class="AnswerSheet_tms"  href="#" onclick="ViewAnswerSheet(' + o.TargetExamID + ',' + o.EmpID + ');"></a>';
                            }
                        }
                    }
                ],
                "scrollY": "250px",
                "aoColumnDefs": [{ "targets": [0, 1, 4, 6, 7], "visible": false }, { className: "textAlignLeft", "targets": [3, 5] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_OnlineTraineeDetailsService.asmx/TTS_OnlineTraineeDetails")%>",
                 "fnServerData": function (sSource, aoData, fnCallback) {
                     aoData.push({ "name": "TTS_ID", "value": <%=hdnTTS_ID.Value%> });
                     $.ajax({
                         "dataType": 'json',
                         "contentType": "application/json; charset=utf-8",
                         "type": "GET",
                         "url": sSource,
                         "data": aoData,
                         "success": function (msg) {
                             var json = jQuery.parseJSON(msg.d);
                             fnCallback(json);
                             UserSessionCheck();
                         },
                         error: function (xhr, textStatus, error) {
                             if (typeof console == "object") {
                                 console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                             }
                         }
                     });
                 }
             });
         }
    </script>

    <!--OnlineTraineeDetails jQuery DataTable-->
    <script>    
         var oTableOnlineTraineeDetails
         function loadOnlineTraineeDetails() {
             if (oTableOnlineTraineeDetails != null) {
                 oTableOnlineTraineeDetails.destroy();
             }
             oTableOnlineTraineeDetails = $('#tblTTS_OnlineTraineeDetails').DataTable({
                 columns: [
                     { 'data': 'TargetExamID' },
                     { 'data': 'EmpID' },
                     { 'data': 'EmpCode' },
                     { 'data': 'EmpName' },
                     { 'data': 'Attempts' },
                     { 'data': 'Status' },
                     { 'data': 'StatusID' },
                     {
                         "mData": null,
                         "bSortable": false,
                         "mRender": function (o) {
                             if (o.TargetExamID == 0) {
                                 return ' ';
                             }
                             else if (o.StatusID != 2) {
                                 return ' ';
                             }
                             else {
                                 return '<a class="AnswerSheet_tms"  href="#" onclick="ViewAnswerSheet(' + o.TargetExamID + ',' + o.EmpID + ');"></a>';
                             }
                         }
                     }
                 ],
                 "scrollY": "374px",
                 "aoColumnDefs": [{ "targets": [0, 1, 6], "visible": false }, { className: "textAlignLeft", "targets": [3, 5] }],
                 "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_OnlineTraineeDetailsService.asmx/TTS_OnlineTraineeDetails")%>",
                 "fnServerData": function (sSource, aoData, fnCallback) {
                     aoData.push({ "name": "TTS_ID", "value": <%=hdnTTS_ID.Value%> });
                     $.ajax({
                         "dataType": 'json',
                         "contentType": "application/json; charset=utf-8",
                         "type": "GET",
                         "url": sSource,
                         "data": aoData,
                         "success": function (msg) {
                             var json = jQuery.parseJSON(msg.d);
                             fnCallback(json);
                             UserSessionCheck();
                         },
                         error: function (xhr, textStatus, error) {
                             if (typeof console == "object") {
                                 console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                             }
                         }
                     });
                 }
             });
         }
         function showOnlineDetails() {
             $('#modalOnlineTraineeDetails').modal('show');
             if ($(<%=hdnIs_Selfcomplte.ClientID%>).val() == "1") {
                $('#modalOnlineTraineeDetails').on('shown.bs.modal', function (e) {
                    loadOnlineTraineeSelfDetails();
                });
            }
            else {
                $('#modalOnlineTraineeDetails').on('shown.bs.modal', function (e) {
                    loadOnlineTraineeDetails();
                });
            }
         }
    </script>

    <!---show Trainee Session Details of Oral and Practical-->
    <script>
        var oTraineeSessionsForOralAndPractical;
        function loadTraineeSessionsListForOralAndPractical() {
            if (oTraineeSessionsForOralAndPractical != null) {
                oTraineeSessionsForOralAndPractical.destroy();
            }
            var colHideOn_tblSessionTraineesListForOralandPractical = [];
            colHideOn_tblSessionTraineesListForOralandPractical = [0, 1, 2, 3, 7, 8, 9, 10, 11];
            oTraineeSessionsForOralAndPractical = $('#tblSessionTraineesListForOralandPractical').DataTable({
                columns: [
                    { 'data': 'TTS_TrainingSessionID' },
                    { 'data': 'TraineeID' },
                    { 'data': 'EvaluationStatus' },
                    { 'data': 'TargetExamID' },
                    { 'data': 'EmpCode' },
                    { 'data': 'Trainee' },
                    { 'data': 'DepartmentName' },
                    { 'data': 'Attempts' },
                    { 'data': 'StatusText' },
                    { 'data': 'EmpFeedbackID' },
                    { 'data': 'TrainerSatisfactory' },
                    { 'data': 'EvaluationRemarks' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            if (o.EvaluationStatus == 5) {
                                return '<a class="OralEvaluation_tms"  href="#" onclick="ViewTrainerOpinion(\'' + o.EvaluationRemarks + '\',\'' + o.TrainerSatisfactory + '\');"></a>';
                            }
                            else {
                                return o.StatusText;
                            }
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            if (o.EmpFeedbackID != 0) {
                                return '<a class="feedback_tms"  href="#" onclick="ViewEmpJR_Feedback(' + o.EmpFeedbackID + ');"></a>';
                            }
                            else {
                                return '';
                            }
                        }
                    }
                ],
                "scrollY": "374px",
                "aoColumnDefs": [{ "targets": colHideOn_tblSessionTraineesListForOralandPractical, "visible": false }, { className: "textAlignLeft", "targets": [5, 6] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/ViewTrainingSessionDetails")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "TrainingSessionID", "value": $("#<%=hdnTrainingSessionID.ClientID%>").val() },
                        { "name": "ExamType", "value": $("#<%=hdnExamType.ClientID%>").val() },
                        { "name": "TTS_ID", "value": $("#<%=hdnTTS_ID.ClientID%>").val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });

        }
    </script>

    <!---show Trainee Session Details of Written-->
    <script>
        var oTraineeSessionsForWritten;
        function loadTraineeSessionsListOfWritten() {
            if (oTraineeSessionsForWritten != null) {
                oTraineeSessionsForWritten.destroy();
            }
            var colHideOn_tblSessionTraineesListForWritten = [];
            colHideOn_tblSessionTraineesListForWritten = [0, 1, 2, 3, 8, 9, 10, 11];
            oTraineeSessionsForWritten = $('#tblSessionTraineesListForWritten').DataTable({
                columns: [
                    { 'data': 'TTS_TrainingSessionID' },
                    { 'data': 'TraineeID' },
                    { 'data': 'EvaluationStatus' },
                    { 'data': 'TargetExamID' },
                    { 'data': 'EmpCode' },
                    { 'data': 'Trainee' },
                    { 'data': 'DepartmentName' },
                    { 'data': 'Attempts' },
                    { 'data': 'StatusText' },
                    { 'data': 'EmpFeedbackID' },
                    { 'data': 'TrainerSatisfactory' },
                    { 'data': 'EvaluationRemarks' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            if (o.EvaluationStatus == 5) {
                                return '<a class="AnswerSheet_tms"  href="#" onclick="ViewAnswerSheet(' + o.TargetExamID + ', ' + o.TraineeID + ');"></a>';
                            }
                            else {
                                return o.StatusText;
                            }
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            if (o.EvaluationStatus == 5 && o.EmpFeedbackID != 0) {
                                return '<a class="feedback_tms"  href="#" onclick="ViewEmpJR_Feedback(' + o.EmpFeedbackID + ');"></a>';
                            }
                            else {
                                return '';
                            }
                        }
                    }

                ],
                "scrollY": "374px",
                "aoColumnDefs": [{ "targets": colHideOn_tblSessionTraineesListForWritten, "visible": false }, { className: "textAlignLeft", "targets": [4, 5, 6] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/ViewTrainingSessionDetails")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "TrainingSessionID", "value": $("#<%=hdnTrainingSessionID.ClientID%>").val() },
                        { "name": "ExamType", "value": $("#<%=hdnExamType.ClientID%>").val() },
                        { "name": "TTS_ID", "value": $("#<%=hdnTTS_ID.ClientID%>").val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                            $("#tblSessionTraineesListForWritten").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblSessionTraineesListForWrittenClass").css({ "width": "100%" });
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
    </script>

    <!---Jquery Datatable functions to view AnsSheet and Feedback--->
    <script>
        function ViewAnswerSheet(TargetExamID, TraineeID) {
            $("#<%=hdnTargetExamID.ClientID%>").val(TargetExamID);
            $("#<%=hdnTrainee.ClientID%>").val(TraineeID);
            $("#<%=hdnDocID.ClientID%>").val($('<%=ddlTrainingDocument.ClientID%>').val());
            $("#<%=btnViewExamAnswerSheet.ClientID%>").click();
        }
        function ViewEmpJR_Feedback(EmpFeedbackID) {
            $('#<%=hdnEmpFeedbackID.ClientID%>').val(EmpFeedbackID);
            $("#<%=btnAskFeedBack.ClientID%>").click();
        }

        function ViewTrainerOpinion(TrainerRemarks, Opinion) {
            document.getElementById("<%=txtEvaluationComments.ClientID %>").value = TrainerRemarks;

            if (Opinion == 1) {
                $("#<%=lblRadioSatisfactory.ClientID%>").addClass("active");
                $("#<%=lblRadioNotSatisfactory.ClientID%>").removeClass("active");
            }
            else {
                $("#<%=lblRadioSatisfactory.ClientID%>").removeClass("active");
                $("#<%=lblRadioNotSatisfactory.ClientID%>").addClass("active");
            }
            //loadTraineeSessionsListForOralAndPractical();
            $('#modelTrainerEvaluationOpinion').modal('show');
        }
    </script>

    <!--Update Page After Both Status is Close-->
    <script>
        function RefreshPageAfterStatusClose() {
            document.getElementById('<%=btnNewTrainingSession.ClientID %>').style.visibility = "hidden";
            document.getElementById('<%=btnReScheduleTrainingSession.ClientID %>').style.visibility = "hidden";
            document.getElementById('txtStatusTTS').value = 'Closed'
            window.open("<%=ResolveUrl("~/TMS/TargetTrainingSchedule/TTS_Master.aspx")%>", "_self");
        }
    </script>

    <!--TraineeAttendence Log jQuery DataTable-->
    <script>
        var TTS_SessionID = 0; var oTraineeAttendenceLog = $('#tblTraineeAttendance').DataTable(
            { "bServerSide": false }
        );
        function loadTraineeAttendence() {
            if (oTraineeAttendenceLog != null) {
                oTraineeAttendenceLog.destroy();
            }
            oTraineeAttendenceLog = $('#tblTraineeAttendance').DataTable({
                columns: [
                    { 'data': 'Trainee_ID' },
                    { 'data': 'Trainee_Code' },
                    { 'data': 'Trainee_Name' },
                    { 'data': 'Dept_Code' },
                    { 'data': 'ExamType' }
                ],
                "scrollY": "374px",
                "bServerSide": true,
                "aoColumnDefs": [{ "targets": [0, 4], "visible": false }, { className: "textAlignLeft", "targets": [2, 3] }],
                "createdRow": function (row, data, dataIndex) {
                    var SelectedTrainees = $("#<%=hdnTraineeIds.ClientID%>");
                    if (SelectedTrainees != null) {
                        TraineeArray = [];
                        if (SelectedTrainees.val() != "") {
                            var TraSelected = SelectedTrainees.val();
                            TraineeArray = TraSelected.split(',');
                            var status = "";
                            for (var i = 0; i < TraineeArray.length; i++) {
                                var name = TraineeArray[i];
                                if (name == data.Trainee_ID) {
                                    status = "Exist";
                                    break;
                                }
                            }
                            if (status == "Exist") {
                                $(row).toggleClass('selected');
                            }
                        }
                    }
                },
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/TargetTrainingAssignedTraineeList")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "TTS_ID", "value": <%=hdnTTS_ID.Value%> }, { "name": "TTS_TrainingSessionsID", "value": TTS_SessionID });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }

        function ReloadTraineeTable(TTS_TrainingSessionsID) {
            TTS_SessionID = TTS_TrainingSessionsID;
            $('#modelAttendanceLog').modal({ backdrop: 'static', keyboard: false });
        }
        $('#modelAttendanceLog').on('shown.bs.modal', function (e) {
            loadTraineeAttendence();
        });
    </script>

    <!--Open Attendance Data table Based On Training Session Status-->
    <script>
        var AttendedTraineeIDs = [];
        var TrainingSessionStatus; // 1 Open 2 Close        
        function BindToTrainingSessionGrid(TTS_TrainingSessionsID, SessionStatus) {
            $("#<%=hdnTrainingSessionID.ClientID%>").val(TTS_TrainingSessionsID);
            if (SessionStatus == "3" || SessionStatus == "5") {
                TrainingSessionStatus = 2;
                //oTraineeAttendenceLog.columns([4]).visible(true).draw();
                $("#AttendeesButtons").hide();
                $("#HideExamType").hide();
                $("#HideNote").hide();
                ReloadTraineeTable(TTS_TrainingSessionsID);
            }
            else {
                TrainingSessionStatus = 1;
                $("#AttendeesButtons").show();
                $("#HideExamType").show();
                $("#HideNote").show();
                ReloadTraineeTable(TTS_TrainingSessionsID);
            }
            $("#<%=hdnTraineeIds.ClientID%>").val('');
            if (AttendedTraineeIDs.length > 0) {
                AttendedTraineeIDs = [];
            }
        }

        $('#tblTraineeAttendance tbody').on('click', 'tr', function () {
            if (TrainingSessionStatus == "1") {
                $(this).toggleClass('selected');
                var pos = oTraineeAttendenceLog.row(this).index();
                var row = oTraineeAttendenceLog.row(pos).data();
                if (!$(this).hasClass('selected')) {
                    if (($.inArray(row.Trainee_ID, AttendedTraineeIDs)) != -1) {
                        AttendedTraineeIDs.splice(($.inArray(row.Trainee_ID, AttendedTraineeIDs)), 1);
                    }
                }
                var ids = $.map(oTraineeAttendenceLog.rows('.selected').data(), function (item) {
                    if (($.inArray(item.Trainee_ID, AttendedTraineeIDs)) == -1) {
                        AttendedTraineeIDs.push(item.Trainee_ID);
                    }
                    return item.Trainee_ID;
                });
                $("#<%=hdnTraineeIds.ClientID%>").val(AttendedTraineeIDs);
            }
        });

        function BindTraineeListOfOralOrPractical(TrainingSessionID, ExamType) {
            $("#<%=hdnTrainingSessionID.ClientID%>").val(TrainingSessionID);
            $("#<%=hdnExamType.ClientID%>").val(ExamType);
            $('#modelSessionTraineesForOral').modal({ backdrop: 'static', keyboard: false });
        }
        $('#modelSessionTraineesForOral').on('shown.bs.modal', function (e) {
            loadTraineeSessionsListForOralAndPractical();
        });
        function BindTraineeListOfWritten(TrainingSessionID) {
            $("#<%=hdnTrainingSessionID.ClientID%>").val(TrainingSessionID);
            $("#<%=hdnExamType.ClientID%>").val("Written");
            $('#modelSessionTraineesForWritten').modal({ backdrop: 'static', keyboard: false });
        }
        $('#modelSessionTraineesForWritten').on('shown.bs.modal', function (e) {
            loadTraineeSessionsListOfWritten();
        });
    </script>

    <!--Fix up GridView to support THEAD tags -->
    <script type="text/javascript">        
        $(function () {
            fixGvHeader();
            addGridThead();
            fixOnlineSessionHeader();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixGvHeader();
            addGridThead();
            fixOnlineSessionHeader();
        });

        function fixGvHeader() {
            var tbl = document.getElementsByClassName("fixGridHead");
            // Fix up GridView to support THEAD tags 
            if ($(".fixGridHead").find("thead").length == 0) {
                $(".fixGridHead tbody").before("<thead><tr></tr></thead>");
                $(".fixGridHead thead tr").append($(".fixGridHead th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixGridHead tbody tr:first").remove();
                }
            }
        }
        function addGridThead() {
            var tbl = document.getElementsByClassName("fixHead");
            // Fix up GridView to support THEAD tags
            if ($(".fixHead").find("thead").length == 0) {
                $(".fixHead tbody").before("<thead><tr></tr></thead>");
                $(".fixHead thead tr").append($(".fixHead th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHead tbody tr:first").remove();
                }
            }
        }
    </script>

    <!--Attendance Validation-->
    <script>
        function AttandenceValidate() {
            var hdnTraineeValues = document.getElementById("<%=hdnTraineeIds.ClientID%>");
            var ExamType = document.getElementById("<%=ddlExamType.ClientID%>");
            errors = [];
            if (hdnTraineeValues.value == "") {
                errors.push("Select Attendees");
            }
            if (ExamType.value == 0) {
                errors.push("Select Exam Type");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
            }
            else {
                ShowPleaseWait('show');
                return true;
            }
            return false;
        }
    </script>

    <!--For Electronic Sign Model-->
    <script>      
        function openElectronicSignModal() {
            openUC_ElectronicSign();
        }

        // To Hide Electronic Sign Before Success/Error Message 
        function HideElectronicSignModal() {
            $('#usES_Modal').modal('hide');
        }
    </script>

    <script>
        function CloseTargetTraining() {
            if ($("#<%=txtCloseComments.ClientID%>").val().trim() == '' || $("#<%=txtCloseComments.ClientID%>").val().trim() == null) {
                    custAlertMsg("Enter Comments", "error");
                }
            else {
                ShowPleaseWait('show');
                    UserSessionCheck();
                    $.ajax({
                        type: "GET",
                       // url: AizantIT_WebAPI_URL + "/api/TmsService/CloseTheTargetTraining?TTS_ID=" + $("#<%=hdnTTS_ID.ClientID%>").val()+"&Comments=" + $("#<%=txtCloseComments.ClientID%>").val(),
                        url: AizantIT_WebAPI_URL + "/api/TmsService/CloseTheTargetTraining",
                        data:{
                            TTS_ID:$("#<%=hdnTTS_ID.ClientID%>").val(),
                            Comments:$("#<%=txtCloseComments.ClientID%>").val()
                            },
                    contentType: "application/json; charset=utf-8",
                    headers: WebAPIRequestHeaderJson,
                    dataType: "json",
                    success: function (response) {
                        ShowPleaseWait('hide');
                        switch (response) {
                            case 1:
                                custAlertMsg("You are not authorised to make this action", "warning", "getBackToList();");
                                break;
                            case 2:
                                custAlertMsg("not in a status to make this action", "info", "getBackToList();");
                                break;
                            case 3:
                                custAlertMsg("Target Training schedule Closed", "success", "getBackToList();");
                                break;
                            default:
                                custAlertMsg("action not performed well contact admin", "error", "getBackToList();");
                        }
                    },
                    failure: function (response) {
                        custAlertMsg("Failure : unable to perform this action", "error");
                    },
                    error: function (response) {
                        custAlertMsg(response.statusText, "error");
                    }
                });
                }
            }
            function getBackToList() {
                $("#<%=btnBackToList.ClientID%>").click();
            }
    </script>

    <script>
            $(document).ready(function () {
                $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
                    var anchor = $(e.target).attr('href');
                    if (anchor == "#History") {
                        loadTTS_ActionHistory();
                    }
                    else if (anchor == "#Schedule") {
                        loadClassRoomSession();
                    }
                })
            });
    </script>

    <script>
        function hideImg() {
            ShowPleaseWait('hide');
        }
    </script>

    <script>
        function OpenTTSCloseValidation() {
            custAlertMsg('You are not able to perform this action due to Target Training schedule has been closed', 'error', "HideSessionBtn();");
        }
        function HideSessionBtn(){
            $("#<%=btnNewTrainingSession.ClientID%>").hide();
            $("#<%=btnReScheduleTrainingSession.ClientID%>").hide();
        }
    </script>

    <script>
        //Get  cancelled Trainee's  List 
       // if ($('#<%=hdnTTS_CurrentStatus.ClientID%>').val() == "12"){
        CancelledTraineesList();
       // }
            function  CancelledTraineesList(){
                    $.ajax({
                            type: "GET",
                            url: AizantIT_WebAPI_URL + "/api/TmsService/GetCancelledTraineesList?TTS_ID=" + $("#<%=hdnTTS_ID.ClientID%>").val(),
                            contentType: "application/json; charset=utf-8",
                            headers: WebAPIRequestHeaderJson,
                            dataType: "json",
                            success: function (response) {
                                $.each(response, function (i, v) {
                                    $("td:contains('"+v.EmpCode+"')").parent('tr').addClass("tablerowred");
                                });
                                if(response.length>0)
                                {
                                    $("#divLegend").show();
                                }                                    
                            },
                            error: function (response) {
                                     custAlertMsg(response.statusText, "error");
                            }
                    });
    }
    </script>
</asp:Content>
