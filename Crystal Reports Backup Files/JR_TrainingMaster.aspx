﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="JR_TrainingMaster.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Training.JR_TrainingMaster" %>

<%@ Register Src="~/UserControls/TMS_UserControls/Exam/ucExamination.ascx" TagPrefix="uc1" TagName="ucExamination" %>
<%@ Register Src="~/UserControls/TMS_UserControls/Exam/ucExamResultSheet.ascx" TagPrefix="uc1" TagName="ucExamResultSheet" %>
<%@ Register Src="~/UserControls/TMS_UserControls/ucFeedback.ascx" TagPrefix="uc1" TagName="ucFeedback" %>
<%@ Register Src="~/UserControls/TMS_UserControls/ucViewDocFAQs.ascx" TagPrefix="uc1" TagName="ucViewDocFAQs" %>
<%@ Register Src="~/UserControls/TMS_UserControls/ucViewQuestions.ascx" TagPrefix="uc1" TagName="ucViewQuestions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <!--ML Enable and Disable script starts -->
   <script src='<%=ResolveUrl("~/AppScripts/ML_FeaturesChecking.js")%>'></script>
        <script>
            GetMLFeatureChecking(3);//Passing Paramenter Module ID
        </script>
    <!--ML Enable and Disable script ends-->
    
    <style>
        #dvViewRegDociframe iframe {
            height: 500px !important;
        }
        #dvViewRetDociframe iframe {
            height: 550px !important;
        }
    </style>
    <asp:HiddenField ID="hdnDeptID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnJR_ID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDocID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDocVersionID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIsRetrospective" runat="server" Value="No" />
    <asp:UpdatePanel ID="upHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnIsFeedbacktoAsk" runat="server" Value="N" />
            <asp:HiddenField ID="hdnHours" runat="server" Value="0" />
            <asp:HiddenField ID="hdnMinutes" runat="server" Value="0" />
            <asp:HiddenField ID="hdnSeconds" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div id="divMainContainer" runat="server" visible="true">
        <div class="form-group col-lg-6 float-right padding-none">
            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                <asp:LinkButton ID="lblJR_TrainingList" class="panel-tile float-right btn btn-dashboard" OnClick="lblJR_TrainingList_Click" runat="server">Back To List</asp:LinkButton>
            </div>
        </div>
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
            <div class="panel panel-default" id="mainContainer" runat="server">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Text="JR Training" oncontextmenu="return false;"></asp:Label>
                </div>
                <div class="float-left col-lg-12  col-12 col-md-12 col-sm-12 padding-none bottom">
                    <div class="form-horizontal ">
                        <div class="float-left col-lg-3 col-12 col-sm-12 col-md-6 padding-none">
                            <label class="control-label col-sm-12">JR Name</label>
                            <div class="col-sm-12">
                                <asp:TextBox ID="txtJR_List" Enabled="false" CssClass="login_input_sign_up form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>

                        <div class="float-left col-lg-3 col-12 col-sm-12  col-md-6 padding-none">
                            <label class="control-label col-sm-12">JR Assigned By</label>
                            <div class="col-sm-12">
                                <asp:TextBox ID="txtJR_AssignedBy" Enabled="false" CssClass="login_input_sign_up form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="float-left col-lg-3 col-12 col-sm-12  col-md-6 padding-none">
                            <label class="control-label col-sm-12">JR Assigned Date</label>
                            <div class="col-sm-12">
                                <asp:TextBox ID="txtJR_AssignedDate" Enabled="false" CssClass="login_input_sign_up form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="float-left col-lg-3 col-12 col-sm-12  col-md-6 padding-none">
                            <label class="control-label col-sm-12">Training Schedule Prepared By</label>
                            <div class="col-sm-12">
                                <asp:TextBox ID="txtTS_PreparedBy" Enabled="false" CssClass="login_input_sign_up form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="float-left col-lg-3 col-12 col-sm-12  col-md-6 padding-none">
                            <label class="control-label col-sm-12">Training Schedule Prepared Date</label>
                            <div class="col-sm-12">
                                <asp:TextBox ID="txtTS_PreparedDate" Enabled="false" CssClass="login_input_sign_up form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="float-left col-lg-3 col-12 col-sm-12  col-md-6 padding-none">
                            <label class="control-label col-sm-12">JR Status</label>
                            <div class="col-sm-12">
                                <asp:UpdatePanel ID="uptxtJR_Status" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtJR_Status" Enabled="false" CssClass="login_input_sign_up form-control" runat="server"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="float-left col-lg-3 col-12 col-sm-12  col-md-6 padding-none">
                            <label class="control-label col-sm-12">JR Version</label>
                            <div class="col-sm-12">
                                <asp:TextBox ID="txtJR_Version" Enabled="false" CssClass="login_input_sign_up form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TabArea">
                <ul class="nav nav-tabs tab_grid">
                    <li class=" nav-item "><a data-toggle="tab" class="nav-link active" href="#RegularTab">Regular</a></li>
                    <li class=" nav-item "><a data-toggle="tab" href="#RetrospectiveTab" class="nav-link" runat="server" id="liRetrospectiveTab">Retrospective</a></li>
                </ul>
            </div>
            <div class="tab-content">
                <%-- Regular Tab--%>
                <div id="RegularTab" class="tab-pane fade show active">
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                        <table id="tblRegularDocDetails" class="datatable_cust tblRegularDocDetailsClass display breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Dept_ID</th>
                                    <th>Department</th>
                                    <th>Document</th>
                                    <th>Trainer</th>
                                    <th>Days</th>
                                    <th>Remains</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <%-- Retrospective Tab--%>
                <div id="RetrospectiveTab" class="tab-pane fade">
                    <div>
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                            <table id="tblRestrospective" class="datatable_cust tblRestrospectiveClass display breakword" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>Dept_ID</th>
                                        <th>Department</th>
                                        <th>Document</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div id="divJustification" class="float-left col-lg-12 col-sm-12 col-12 col-md-12" runat="server">
                        <div class="form-group float-left col-lg-12 col-sm-12 col-12 col-md-12">
                            <label class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">Justification Note<span class="mandatoryStar">*</span></label>
                            <asp:UpdatePanel runat="server" ID="upRetSubmit" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <textarea runat="server" id="txtJustificationNote" class="float-left col-lg-12 col-sm-12 col-12 col-md-12 txt_Just" rows="3" maxlength="250" placeholder="Enter Justification"></textarea>
                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none  bottom">
                                        <a href="#" class="btn btn-signup_popup float-right" id="btnRetJustNoteSubmit" runat="server" onclick="ConfirmJustificationAlert();">Submit Justification</a>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="divSubmitJustificationNote" style="display: none">
        <asp:UpdatePanel ID="upSubmitJustificationNote" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnSubmitJustificationNote" runat="server" Text="" OnClick="btnSubmitJustificationNote_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="divOralSubmit" style="display: none">
        <asp:UpdatePanel ID="upOralSubmit" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnOralSubmit" runat="server" Text="" OnClick="btnOralSubmit_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <!-------- Document Details Modal---------->
    <div id="modelDocDetails" role="dialog" class="modal department fade">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Document Details</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body float-left col-lg-12 col-sm-12 col-12 col-md-12">
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <table id="tblDeptDocuments" class="datatable_cust display tblDeptDocumentsClass breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>DocID</th>
                                    <th>DocVersionID</th>
                                    <th>Document</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Read</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!---- End---->

    <!-------- Trainer Details Modal---------->
    <div id="modelDeptTrainerDetails" role="dialog" class="modal department fade">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Trainer Details</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body float-left col-lg-12 col-sm-12 col-12 col-md-12">
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <table id="tblDeptTrainerDetails" class="datatable_cust display tblDeptTrainerDetailsClass breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Emp.Code</th>
                                    <th>Trainer</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!---- End---->

    <%--modal popup to load Regular document Content--%>
    <div class="modal fade department" id="modalRegularDocument" role="dialog">
        <div class="modal-dialog" style="min-width: 90%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header tmsModalHeader">

                    <h4 class="modal-title" style="color: aliceblue">Document</h4>
                    <button type="button" class="close " onclick="ClosemodalRegularDocument();" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body tmsModalBody" style="max-height: 665px; overflow-y: auto">
                    <div class="row tmsModalContent">
                        <div class="text-right col-lg-12 col-md-12 col-12 col-sm-12">
                            <button type="button" id="btnAskQuestion" class="btn-signup_popup" data-toggle="modal" onclick="ShowAskQuestionDiv();">Ask a Question</button>
                            <button type="button" class="btn-signup_popup" data-toggle="modal" data-target="#modalViewQuestions" onclick="openViewQuestionPopUp();">View Questions</button>
                            <button type="button" class="btn-revert_popup" data-toggle="modal" data-target="#modalDocFAQs" onclick="openFaqPopUp();">FAQs</button>
                            <div class=" float-left" style="font-size: 21px; padding-top: 2px; padding-right: 20px; text-align: left"><span class="glyphicon glyphicon-time"></span>&nbsp;<span id="Timer"></span></div>
                           <!--Start TMS_ML (1/4) HTML Code-->
                            <button type="button" id="btnRelatedObservations" style="display:none"  class="btn-signup_popup" data-toggle="modal">Related Observation<span class="relatedobs_count">0</span></button>
                            <div class=" col-lg-6 col-sm-6 col-6 col-md-6 related_obs_panel padding-none text-left float-left" id="draggablelist">
                                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">
                                    <span class="col-3 padding-none" style="font-weight: bold;">Related Observations</span> <a href="#" id="relatedobsclose" class="close">&times;</a>
                                </div>
                                <div id="divRelatedDragList">
                                    <ul class="col-12 top float-left related_draglist">
                                        Loading...
                                </ul>
                                </div>
                                   <div class="footr_ml float-left col-lg-12  col-12 col-md-12 col-sm-12 ">
                                    <span class="internal_legend_ml"></span><span class="">Internal</span><span class="external_legend_ml"></span><span class="">External</span>
                                </div>
                            </div>
                           <!--End TMS_ML (1/4) HTML Code-->

                            <div id="divCreateQuestion" class="float-left col-lg-12" style="display: none">
                                <div class="form-group padding-none">
                                    <label for="inputEmail3" class=" padding-none float-left control-label custom_label_answer">Question</label>
                                    <div class=" padding-none">
                                        <textarea class="form-control" rows="5" runat="server" maxlength="350" id="txtQuestion"></textarea>
                                    </div>
                                </div>
                                <div class="float-left col-lg-12 padding-none">
                                    <div class="float-right ">
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upCreateQuestion">
                                            <ContentTemplate>
                                                <asp:Button ID="btnCreateQuestion" type="button" class="btn btn-signup_popup" OnClientClick="return CreateQuestionValidation();" runat="server" Text="Create" OnClick="btnCreateQuestion_Click" />
                                                <button type="button" class="btn btn-cancel_popup" onclick="CloseAsk_aQuestion();">Cancel</button>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                            <div id="dvViewRegDociframe" class="col-12 traning_dept_select text-center float-left" style="width: 100%">
                                <noscript>
                                    Please Enable Javascript.
                                </noscript>
                            </div>
                        </div>
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12" style="text-align: center;">
                            <asp:UpdatePanel ID="upBtnTakeExam" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <div runat="server" id="divOralExamSubmittedMsg" visible="false">
                                        <span style="color: green">You submitted this document, ready for oral exam.</span>
                                    </div>
                                    <div runat="server" id="divTakeExamBtn">
                                        <asp:Button ID="btnTakeExam" runat="server" ToolTip="You would be able to take an exam after 10 minutes" CssClass="btn-signup_popup" Text="Take Exam" OnClick="btnTakeExam_Click" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--modal popup to load Retrospective document Content--%>
    <div class="modal fade department" id="modalRetrospectiveDocument" role="dialog">
        <div class="modal-dialog modal-lg" style="min-width: 90%; height: 600px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header tmsModalHeader">

                    <h4 class="modal-title Panel_Title" style="color: aliceblue">Document</h4>
                    <button type="button" class="close " data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body tmsModalBody">
                    <div class="row tmsModalContent">
                        <div class="float-left col-lg-12 col-md-12 col-12 col-sm-12">
                            <button type="button" class="btn btn-signup_popup float-right bottom" onclick="openFaqPopUp();">FAQs</button>
                            <div id="dvViewRetDociframe" style="width: 100%">
                                <noscript>
                                    Please Enable Javascript.
                                </noscript>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-------- Trainee Ask Question-------------->
    <div id="modalAskQuestion" class="modal department fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">New Question</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="form-group padding-none">
                        <label for="inputEmail3" class=" padding-none float-left control-label custom_label_answer">Question</label>
                        <div class=" padding-none">
                            <textarea class="form-control" maxlength="350" rows="5" id=""></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class=" btn-signup_popup" data-dismiss="modal">Create</button>
                    <button type="button" class=" btn-cancel_popup" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>
    <!-------- End new question-------------->

    <%-- <!-- Modal Confirm content-->
    <div id="ModalConfirm_JrTM" class="modal confirmation_popup" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="success_icon float-left">
                        <h4 class="modal-title float-left popup_title">Confirmation</h4>
                        <img src="<%=ResolveUrl("~/Images/CustomAlerts/confirm.png")%>" class="float-left" style="width: 35px" alt="Smiley face" />
                        <h4 class="modal-title float-left popup_title">Confirmation</h4>
                    </div>
                </div>
                <div class="modal-body" style="padding: 0px">
                    <div class="float-left col-lg-12 padding-none " style="padding: 10px; border-bottom: 1px solid #57a5f6;">
                        <p id="JrTM_ConfirmMsg" class="float-left col-lg-12 " style="padding: 8px; padding: 12px; max-height: 128px; overflow-x: auto;"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="confirmDialog" id="cnf">
                        <button type="button" id="btnJrTM_ConfirmYes" class="btn continue-btn" data-dismiss="modal" style="margin-top: 10px !important">Yes</button>
                        <input type="button" value="No" style="margin-left: 1px; margin-top: 10px !important" class="btn btn-cancel_popup" data-dismiss="modal" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Confirm content-->--%>

    <!-- Modal Confirm content-->
    <div id="ModalConfirm_JrTM" class="modal confirmation_popup ModalConfirm" role="dialog" style="z-index: 9999">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content modalMainContent">
                <div class="modal-header ModalHeaderPart">
                    <div class="success_icon float-left">

                        <h4 class="modal-title float-left popup_title">Confirmation</h4>
                    </div>
                </div>
                <div class="modal-body" style="padding: 0px">
                    <div class="col-lg-12 padding-none ">
                        <p id="JrTM_ConfirmMsg" class="col-lg-12 " style="padding: 8px; padding: 12px; max-height: 128px; overflow-x: auto;"></p>
                    </div>
                </div>
                <div class="modal-footer ModalFooterPart">
                    <div class="confirmDialog" id="cnf">
                        <button type="button" id="btnJrTM_ConfirmYes" class="btn-signup_popup" data-dismiss="modal" style="margin-top: 10px !important">Yes</button>
                        <input type="button" value="No" style="margin-left: 1px; margin-top: 10px !important" class=" btn-cancel_popup" data-dismiss="modal" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <uc1:ucViewQuestions runat="server" ID="ucViewQuestions" />
    <uc1:ucViewDocFAQs runat="server" ID="ucViewDocFAQs" />
    <uc1:ucExamination runat="server" ID="ucExamination" />
    <uc1:ucFeedback runat="server" ID="ucFeedback" />

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnOpenDocumentOnRegular" runat="server" Text="Button" OnClick="btnOpenDocumentOnRegular_Click" />
                <asp:Button ID="btnAskFeedBack" runat="server" Text="Button" OnClick="btnAskFeedBack_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script>      
        var TrainingOn;
        function viewDocument(DocID, DocVersionID) {
//Start TMS_ML (2/4) Code
            makeRelatedObservationsForDocumentToDefault();
//End TMS_ML (2/4) Code
            $("#<%=hdnDocID.ClientID%>").val(DocID);
            $("#<%=hdnDocVersionID.ClientID%>").val(DocVersionID);
            $("#<%=btnOpenDocumentOnRegular.ClientID%>").click();
        }

        function OpenPdfViewerContent() {
            if (TrainingOn == "Regular") {
                $.ajax({
                    type: "POST",
                    url: "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/CheckTraineeActiveDocRead")%>",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "DocID": $("#<%=hdnDocID.ClientID%>").val() }),
                    success: function (result) {
                        if (result.d != "0") {
                            custAlertMsg("You can read only one document at a time, <br/> if not kindly re-login.", "warning");
                        }
                        else {
                            UpdateReadDocID($("#<%=hdnDocID.ClientID%>").val());
                            openModalRegularDocs();
                            $('#<%=btnTakeExam.ClientID %>').prop('disabled', true);
                            $('#<%=btnTakeExam.ClientID %>').css('cursor', 'not-allowed');
                            document.getElementById("Timer").innerHTML = "";
                            PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', 0, $('#<%=hdnDocVersionID.ClientID%>').val(), 0, 0, "#dvViewRegDociframe", 'OnFileLoadingSuccess');
                        }
                    }
                });
            }
            else {
                openModalRetrospectiveDocs();
                PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', 0, $('#<%=hdnDocVersionID.ClientID%>').val(), 0, 0, "#dvViewRetDociframe", 'GetRelatedObservationsForDocument($("#<%=hdnDocID.ClientID%>")[0].value);');
            }
        }

        var deptID;
        function viewDocDetailsByDepartment(DeptID, trainingOn) {
            TrainingOn = trainingOn;
            $("#<%=hdnDeptID.ClientID%>").val(DeptID);
            $('#modelDocDetails').modal({ backdrop: 'static', keyboard: false });
        }
        $('#modelDocDetails').on('shown.bs.modal', function (e) {
            bindDeptDocumentList($("#<%=hdnDeptID.ClientID%>").val());
        });
        function viewDeptTrainerDetials(DeptID) {
            $("#<%=hdnDeptID.ClientID%>").val(DeptID);
            $('#modelDeptTrainerDetails').modal({ backdrop: 'static', keyboard: false });
        }
        $('#modelDeptTrainerDetails').on('shown.bs.modal', function (e) {
            bindDeptTrainerDetails($("#<%=hdnDeptID.ClientID%>").val());
        });

    </script>

    <!--Modal handling-->
    <script>
        function openModalRegularDocs() {
            CloseAsk_aQuestion();
            $('#modalRegularDocument').modal({ backdrop: 'static', keyboard: false });
        }
        function openModalRetrospectiveDocs() {
            CloseAsk_aQuestion();
            $('#modalRetrospectiveDocument').modal({ backdrop: 'static', keyboard: false });
        }
        function AskFeedBackOnTraining(AskFeedback) {
            $('#<%=hdnIsFeedbacktoAsk.ClientID%>').val(AskFeedback);
            $("#<%=btnAskFeedBack.ClientID%>").click();
            ReloadRegularDocDataTable();
            ShowPleaseWait('hide');
            //ML feature checking 
            if (ML_FeatureIDs.includes(7) == true) {
                $.get(AGA_ML_URL + "/Emp", { "Empid": $("#<%=hdnEmpID.ClientID%>").val() });
            }
        }
        function AskFeedBackOnJR_Training() {
            $("#<%=btnAskFeedBack.ClientID%>").click();
            ShowPleaseWait('hide');
        }
        function CloseReadDocModelAndCloseDocDetailsModel() {
            DocReadDurationTimer('stop');
            $('#modelDocDetails').modal('hide');
            UpdateReadDocID(0);
        }
        function CloseReadDocModel_OpenDocExamModel() {
            DocReadDurationTimer('stop');
        }
        function openFaqPopUp() {
            var DocID = $('#<%=hdnDocID.ClientID%>').val();
            OpenDocFAQPopup(DocID);
        }
        function openViewQuestionPopUp() {
            var DocID = $('#<%=hdnDocID.ClientID%>').val();
            var JR_ID = $('#<%=hdnJR_ID.ClientID%>').val();
            ViewQuestionsByTrainee('1', JR_ID, DocID, '0');
        }
        function ShowAskQuestionDiv() {
            $('#divCreateQuestion').show();
            $('#btnAskQuestion').hide();
        }
        function CloseAsk_aQuestion() {
            $('#divCreateQuestion').hide();
            $('#btnAskQuestion').show();
            clearQuestionText();
        }
        //here FeedbackForm Status 1-Active,it opens when it is in Active Mode
        function CheckJR_StatusClick() {
            $('#modelDocDetails').modal('hide');
            var CurrentStatus = document.getElementById('<%=txtJR_Status.ClientID%>').value;
            //code for feedback status loading.
        }
        //confirm modal popup for Oral Submission
        function ConfirmOralSubmission() {
            JrTrainingMasterConfirm('You Need to Attend Oral Evaluation for this Document, Do you want to continue ?', "SubmitOral");
            CloseReadDocModelAndCloseDocDetailsModel();
        }
    </script>

    <script>
        function UpdateTrainingStatus() {
            var formData = {
                DocID: $("#<%=hdnDocID.ClientID%>").val(),
                DocVersionID: $("#<%=hdnDocVersionID.ClientID%>").val(),
                JrID: $("#<%=hdnJR_ID.ClientID%>").val()
              };
              console.log(formData);
              $.ajax({
                  "dataType": 'json',
                  "contentType": "application/json; charset=utf-8",
                  "type": "GET",
                  "url": '<%=ResolveUrl("~/TMS/WebServices/JrTraining/JR_TrainingMasterService.asmx/UpdateJrTrainingStatus")%>',
                "data": formData,
                "success": function (msg) {
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }
    </script>

    <!--For JR Training Regular jQuery Datatable-->
    <script>
        $(function () {
            LoadRegularDataTable();
        });
        var oTblRegularDeptDocsDetails;
        function LoadRegularDataTable() {
            if (oTblRegularDeptDocsDetails != null) {
                oTblRegularDeptDocsDetails.destroy();
            }
            oTblRegularDeptDocsDetails = $('#tblRegularDocDetails').DataTable({
                columns: [
                    { 'data': 'Dept_ID' },
                    { 'data': 'DepartmentName' },
                    {
                        "mData": null,
                        // "bSortable": false,
                        "mRender": function (o) { return '<a  href="#" class="grid_button" onclick="viewDocDetailsByDepartment(' + o.Dept_ID + ',\'Regular\');">' + o.DocumentCount + '</a>'; }
                    },
                    {
                        "mData": null,
                        // "bSortable": false,
                        "mRender": function (o) { return '<a  href="#" class="grid_button" onclick="viewDeptTrainerDetials(' + o.Dept_ID + ');">' + o.TrainerCount + '</a>'; }
                    },
                    { 'data': 'AssignedDays' },
                    { 'data': 'RemainingDays' },
                    { 'data': 'StatusName' }
                ],
                "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
                "aoColumnDefs": [{ "targets": [0], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [1,6] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTraining/JR_TrainingMasterService.asmx/GetJR_TrainingRegular")%>",
                  "fnServerData": function (sSource, aoData, fnCallback) {
                      aoData.push({ "name": "JR_ID", "value": <%=hdnJR_ID.Value%> });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                            $("#tblRegularDocDetails").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblRegularDocDetailsClass").css({ "width": "100%" });
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }

        function ReloadRegularDocDataTable() {
            LoadRegularDataTable();
            ShowPleaseWait('hide');
            //ML feature checking 
            if (ML_FeatureIDs.includes(7) == true) {
                $.get(AGA_ML_URL + "/Emp", { "Empid": $("#<%=hdnEmpID.ClientID%>").val() });
            }
        }
    </script>

    <!--For JR Training Retrospective jQuery Datatable-->
    <script>
        $.ajaxSetup({
            cache: false
        });

        var oTableRetrospective = $('#tblRestrospective').DataTable({
            columns: [
                { 'data': 'Dept_ID' },
                { 'data': 'DepartmentName' },
                {
                    "mData": null,
                    // "bSortable": false,
                    "mRender": function (o) { return '<a href="#" class="grid_button" onclick="viewDocDetailsByDepartment(' + o.Dept_ID + ',\'Retrospective\');">' + o.Document + '</a>'; }
                }
            ],
            "aoColumnDefs": [{ "targets": [0], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [1] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTraining/JR_TrainingMasterService.asmx/GetJR_TrainingRetrospective")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "JR_ID", "value": <%=hdnJR_ID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblRestrospective").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        $(".tblRestrospectiveClass").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });


    </script>

    <!--For JR Training Dept wise Document List Regular jQuery Datatable-->
    <script>       
        var deptID = 0;

        var otblDeptDocuments;
        $(function () {
            loadDeptDocuments();
        });

        function loadDeptDocuments() {
            if (otblDeptDocuments != null) {
                otblDeptDocuments.destroy();
            }
            otblDeptDocuments = $('#tblDeptDocuments').DataTable({
                columns: [
                    { 'data': 'DocumentID' },
                    { 'data': 'DocumentVesrsionID' },
                    { 'data': 'Document' },
                    { 'data': 'DocumentType' },
                    { 'data': 'TrainingStatus' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            if (o.TrainingStatus == "Pending") {
                                return '<a class="read_tms" href="#" onclick="viewDocument(' + o.DocumentID + ',' + o.DocumentVesrsionID + ');"></a>';
                            }
                            else {
                                return ''
                            }
                        }
                    }
                ],
                "aoColumnDefs": [{ "targets": [0, 1], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [2, 3, 4] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTraining/JR_TrainingMasterService.asmx/GetJR_TrainingDocDetails")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "JR_ID", "value": <%=hdnJR_ID.Value%> }, { "name": "Dept_ID", "value": deptID },
                        { "name": "TrainingOn", "value": TrainingOn });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                            $("#tblDeptDocuments").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblDeptDocumentsClass").css({ "width": "100%" });
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
        //To Reload Datatable

        function bindDeptDocumentList(Deptid) {
            deptID = Deptid;
            otblDeptDocuments.ajax.reload();
        }
    </script>

    <!--For JR Dept wise Trainer Details-->
    <script>
        var otblDeptTrainerDetails;
        function loadDeptTrainerDetails(DeptID) {
            if (otblDeptTrainerDetails != null) {
                otblDeptTrainerDetails.destroy();
            }
            otblDeptTrainerDetails = $('#tblDeptTrainerDetails').DataTable({
                columns: [
                    { 'data': 'EmpCode' },
                    { 'data': 'TrainerName' }
                ],
                "aoColumnDefs": [{ "targets": [], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [1] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTraining/JR_TrainingMasterService.asmx/GetJR_DeptTrainerDetails")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "JR_ID", "value": <%=hdnJR_ID.Value%> }, { "name": "DeptID", "value": DeptID });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                            $("#tblDeptTrainerDetails").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblDeptTrainerDetailsClass").css({ "width": "100%" });
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
        //To Reload Datatable

        function bindDeptTrainerDetails(DeptID) {
            loadDeptTrainerDetails(DeptID);
        }
    </script>

    <script>
        var readingTimer = 0;
        function DocReadDurationTimer(type) {
            if (type == "start") {
                var sec = parseInt(document.getElementById("<%=hdnSeconds.ClientID%>").value);
                var h = parseInt(document.getElementById("<%=hdnHours.ClientID%>").value);
                var min = parseInt(document.getElementById("<%=hdnMinutes.ClientID%>").value);
                readingTimer = setInterval(function () {
                    var seconds = sec + 1;
                    var minutes = min;
                    var hours = h;
                    SaveCurrentDuration(seconds, minutes, hours);
                    if (seconds >= 60) {
                        var minutes = min + 1;
                        seconds = 0;
                    }
                    if (minutes >= 60) {
                        var hours = h + 1;
                        minutes = 0;
                    }
                    sec = seconds;
                    min = minutes;
                    h = hours;

                    document.getElementById("Timer").innerHTML = hours + "h "
                        + minutes + "m " + seconds + "s ";
                }, 1000);
                $('#modalRegularDocument').modal({ backdrop: 'static', keyboard: false });
            }
            else {
                $('#modalRegularDocument').modal('hide');
                clearInterval(readingTimer);
            }
        }

        function OpenModalRegularDocumentModal() {
            $('#<%=btnTakeExam.ClientID %>').prop('disabled', true);
            $('#<%=btnTakeExam.ClientID %>').css('cursor', 'not-allowed');
            ReloadRegularDocDataTable();
        }

        function OnFileLoadingSuccess() {
            UpdateTrainingStatus();
            DocReadDurationTimer('start');
            //ML feature checking 
            if (ML_FeatureIDs.includes(7) == true) {
                GetRelatedObservationsForDocument($("#<%=hdnDocID.ClientID%>")[0].value);//TMS_ML
            }
        }


        function ClosemodalRegularDocument() {
            DocReadDurationTimer('stop');
            UpdateReadDocID(0);
        }

        function UpdateReadDocID(DocumentID) {
             $.ajax({
                type: "POST",
                url: "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/UpdateTraineeDocRead")%>",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ "TraineeReadingDocID": DocumentID }),
                dataType: "json",
                success: function (result) {
                }
            });  
        }
    </script>

    <script type="text/javascript">
        function SaveCurrentDuration(Seconds, Minutes, Hours) {
            UserSessionCheck();
            var JR_ID = document.getElementById('<%=hdnJR_ID.ClientID%>').value;
            var docVersionID = $("#<%=hdnDocVersionID.ClientID%>")[0].value;
            var docID = $("#<%=hdnDocID.ClientID%>")[0].value;
            var hours = Hours;
            var min = Minutes;
            var sec = Seconds;
            var objStr = { "JR_ID": JR_ID, "docVersionID": docVersionID, "docID": docID, "Hours": hours, "Minutes": min, "Seconds": sec };
            var jsonData = JSON.stringify(objStr);
            $.ajax({
                type: "POST",
                url: "<%=ResolveUrl("~/TMS/WebServices/captureTime/captureSpentDuarationOnDoc.asmx/RecordSpentDuarationOnDoc")%>",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                    var btnIsDisabled = $('#<%=btnTakeExam.ClientID %>').prop('disabled');
                    if (!btnIsDisabled) {
                        //modified duration
                        if (hours >= 1 || Minutes >= 10) {
                            $('#<%=btnTakeExam.ClientID %>').prop('disabled', false);
                            $('#<%=btnTakeExam.ClientID %>').css('cursor', 'pointer');
                        }
                        else {
                            $('#<%=btnTakeExam.ClientID %>').prop('disabled', true);
                            $('#<%=btnTakeExam.ClientID %>').css('cursor', 'not-allowed');
                        }
                    }
                    else {
                        //modified duration
                        if (hours == 0 && Minutes < 10) {
                            $('#<%=btnTakeExam.ClientID %>').prop('disabled', true);
                            $('#<%=btnTakeExam.ClientID %>').css('cursor', 'not-allowed');
                        }
                        else {
                            $('#<%=btnTakeExam.ClientID %>').prop('disabled', false);
                        }
                    }
                },

                failure: function () {
                    custAlertMsg("Internal Server Error Occured. Document Read Duration not Recorded.", "error");
                }
            });
        }
    </script>

    <script>
        function focusOnJustificationNote() {
            $('#<%=txtJustificationNote.ClientID %>').focus();
        }
        function ConfirmJustificationAlert() {
            var Justification = document.getElementById("<%=txtJustificationNote.ClientID%>").value.trim();
            errors = [];
            if (Justification == "") {
                errors.push("Enter Justification Note");

                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error", "focusOnJustificationNote()");
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                JrTrainingMasterConfirm('Do You Want To Submit Justification Note', "SubmitJustificationNote");
            }
        }
    </script>

    <!---Create Question Validation--->
    <script>
        function CreateQuestionValidation() {
            var Question = document.getElementById("<%= txtQuestion.ClientID%>").value.trim();
            errors = [];

            if (Question == "") {
                errors.push("Enter Question");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ShowPleaseWait('show');
                return true;
            }
        }

        function clearQuestionText() {
           document.getElementById("<%= txtQuestion.ClientID%>").value = '';
        }

    </script>

    <!--To Reload Datatable--->
    <script>
        $(document).ready(function () {
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href"); // activated tab
                $($.fn.dataTable.tables(true)).css('width', '100%');
                $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
            });
        });
    </script>

    <!--Jr Training Master Confirm-->
    <script>
        var _CBF;
        function JrTrainingMasterConfirm(Message, callbackFunction) {
            $("#JrTM_ConfirmMsg").text(Message);
            _CBF = callbackFunction;
            $('#btnJrTM_ConfirmYes').attr('onclick', _CBF + '();');
            $("#ModalConfirm_JrTM").modal({
                backdrop: 'static',
                keyboard: false
            });
        }
        
        function SubmitOral() {
            $("#<%=btnOralSubmit.ClientID%>").click();
            ShowPleaseWait('show');
        }

        function SubmitJustificationNote() {
            $("#<%=btnSubmitJustificationNote.ClientID%>").click();
            ShowPleaseWait('show');
        }
        function UpdateJrTrainingStatus() {
            $("#<%=txtJR_Status.ClientID%>").val("JR at Trainer Evaluation");
        }
    </script>
    <script>
        function HideImg() {
            ShowPleaseWait('hide');
        }
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <!--Start TMS_ML (4/4) Code-->
    <script src='<%= Page.ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>'></script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/TMS/TMS_ML_RelatedObservationsonDoc.js")%>'></script>
    <!--End TMS_ML (4/4) Code-->
</asp:Content>
