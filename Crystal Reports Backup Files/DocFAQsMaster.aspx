﻿<%@ Page Title="FAQ Master" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="DocFAQsMaster.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Administration.DocFAQs.DocFAQsMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:UpdatePanel ID="upHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnDocID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnDocFAQQuestionID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnNewOrEditFAQ" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div id="divMainContainer" runat="server">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 col-12 col-md-12 padding-none ">
            <div class="card" id="divHearderCollapse">
                <div class="card-header grid_header accordion-toggle" href="#divHearderBody" data-toggle="collapse" data-parent="#divHearderCollapse">
                    <h6 class="mb-0 ">Document FAQ(s) </h6>
                </div>
                <div id="divHearderBody" class="collapse show" aria-labelledby="divHearderCollapse" data-parent="#divHearderCollapse">
                    <div class="card-body ">
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none grid_panel_full">
                            <asp:UpdatePanel ID="upDropDowns" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="form-group float-left col-lg-3  col-12 col-md-3 col-sm-12 top">
                                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                            <label>Select Department<span class="smallred_label">*</span></label>
                                            <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="selectpicker form-control regulatory_dropdown_style" onChange="ShowHideTheJQDataTable(0);"
                                                data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group float-left col-lg-2 col-12 col-md-2 col-sm-12 top">
                                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                            <label>Document Type</label>
                                            <asp:DropDownList ID="ddlDocumentType" runat="server" CssClass="selectpicker form-control regulatory_dropdown_style" onChange="ShowHideTheJQDataTable(0);"
                                                data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlDocumentType_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group float-left col-lg-7 col-12 col-md-7 col-sm-12  padding-right_div top" id="divDocuments">
                                        <label>Select Document<span class="smallred_label">*</span></label>
                                        <asp:DropDownList ID="ddlDocument" runat="server" CssClass="float-left col-lg-12 col-sm-12 col-12 col-md-12 regulatory_dropdown_style selectpicker show-tick padding-none" onChange="ShowHideTheJQDataTable($('option:selected', this).val());" data-live-search="true" data-size="5">
                                        </asp:DropDownList>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 float-left padding-none grid_panel_full"  id="divtblDocFaqList" style="display: none">
            <div class="form-group  text-right col-lg-12 col-sm-12 col-12 col-md-12 top">
                <button type="button" class="btn-signup_popup " id="btnNewFAQ" onclick="OpenModelFAQ('NewFAQ');" visible="false" title="Create New Question">Add New FAQ</button>
            </div>
            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none  ">
                <table id="tblDocFAQs_List" class="datatable_cust tblDocFAQs_ListClass display breakword" style="width: 100%">
                    <thead>
                        <tr>
                            <th>DocQuestionID</th>
                            <th>Document Question</th>
                            <th>Created By</th>
                            <th>Created Date</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>History</th>
                        </tr>
                    </thead>
                </table>
            </div>
                </div>
        </div>
    </div>
    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnEditDocFAQ" runat="server" Text="Button" OnClick="btnEditDocFAQ_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <!-------- Modal For Create and Edit FAQ-------------->
    <div id="modelFAQ" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <label id="lblFAQTitle" class="modal-title"></label>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" title="Close">&times;</button>
                </div>
                <div class="modal-body">
                    <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <asp:UpdatePanel ID="upFaqModal" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="divQuestionStatus" class="padding-none col-sm-12 col-12 float-left col-lg-12 col-12 col-md-12">
                                    <div class="form-group padding-none col-sm-12 col-12 float-left col-lg-12 col-12 col-md-12  " data-toggle="buttons" id="divRadioBtns" style="display: none">
                                        <label for="inputPassword3" class=" col-12 float-left padding-none control-label">Question Status</label>
                                        <div class="col-12 float-left padding-none">
                                            <label class=" btn btn-primary_radio radio_check col-3  col-lg-6 col-md-6 col-sm-6 float-left active" id="lblRadioActiveFQ" runat="server" title="Make Active">
                                                <asp:RadioButton ID="rbtnActive" runat="server" GroupName="A" Text="Active" />
                                            </label>
                                            <label class=" btn btn-primary_radio radio_check col-3 col-lg-6 col-md-6 col-sm-6 float-left" id="lblRadioInActiveFQ" runat="server" title="Make InActive">
                                                <asp:RadioButton ID="rbtnInActive" runat="server" GroupName="A" Text="In-Active" />
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-right_div">
                                    <label for="inputEmail3" class=" padding-none float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">Question<span class="smallred_label">*</span></label>
                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                        <textarea class="form-control" runat="server" id="txtDocFAQ_Question" placeholder="Enter Question" maxlength="350" title="Question"></textarea>
                                    </div>
                                </div>
                                <div class="form-group float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-right_div">
                                    <label for="inputEmail3" class=" padding-none float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">Answer<span class="smallred_label">*</span></label>
                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                        <textarea class="form-control " runat="server" id="txtDocFAQ_Answer" placeholder="Enter Answer" title="Answer" style="min-height: 150px; max-height: 350px;"></textarea>
                                    </div>
                                </div>
                                <div class="form-group float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-right_div" id="divFaqComments" style="display: none">
                                    <label for="inputEmail3" class=" padding-none float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">Remarks<span class="smallred_label">*</span></label>
                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                        <textarea class="form-control" runat="server" id="txtDocFAQ_Comments" maxlength="250" placeholder="Enter Modified Reason" title="Remarks"></textarea>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel runat="server" ID="upbtn" UpdateMode="Conditional" style="display: inline-block;">
                        <ContentTemplate>
                            <asp:Button ID="btnSubmitDocFAQ" CssClass=" btn-signup_popup" OnClick="btnSubmitDocFAQ_Click"
                                OnClientClick="return DocFAQValidation();"
                                runat="server" Text="Submit" ToolTip="Click to Submit" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class=" btn-cancel_popup" data-dismiss="modal" title="Click to Cancel">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--End of Create and Edit FAQ model-->

    <!-------- Modal For Viewing the History of FAQuestion------------->
    <div id="modelFAQ_History" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">
                        <asp:Label ID="lblmodelFAQ_HistoryTitle" runat="server" Text="FAQ History"></asp:Label>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" title="Close">&times;</button>
                </div>
                <div class="modal-body">
                    <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <table id="tblFAQ_History" class="datatable_cust tblFAQ_HistoryClass display breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>DocFaqID</th>
                                    <th>Modified By</th>
                                    <th>Modified Date</th>
                                    <th>Modified Reason</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-------- End FAQ History-------------->

    <!--DocFAQ List jQuery DataTable-->
    <script>
        $.ajaxSetup({
            cache: false
        });

        var tblDocFAQ;
        function loadDocFAQ_Master() {
            ClearText();
            if ($('#<%=hdnDocID.ClientID%>').val() != "0") {
                if (tblDocFAQ != null) {
                    tblDocFAQ.destroy();
                }
                tblDocFAQ = $('#tblDocFAQs_List').DataTable({
                    columns: [
                        { 'data': 'DocQuestionID' },
                        { 'data': 'DocQuestion' },
                        { 'data': 'CreatedBy' },
                        { 'data': 'CreatedDate' },
                        { 'data': 'CurrentStatusName' },
                        {
                            "mData": null,
                            "bSortable": false,
                            "mRender": function (o) {
                                return '<a class="Edit"  href="#" onclick="UpdateDocFAQ(' + o.DocQuestionID + ')" title="Click to Modify"></a>';
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "mRender": function (o) {
                                return '<a class="summary_latest"  href="#" onclick="ViewDocFAQ_History_List(' + o.DocQuestionID + ')" title="Click to View"></a>';
                            }
                        }
                    ],
                    "order": [[4, "desc"]],
                    "aoColumnDefs": [{ "targets": [0], "visible": false }, { className: "textAlignLeft", "targets": [1, 2, 4] }],
                    "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/DocumentFAQ/DocumentFAQ_Service.asmx/DocFAQ_List")%>",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({ "name": "DocID", "value": $('#<%=hdnDocID.ClientID%>').val() });
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                UserSessionCheck();
                                $("#tblDocFAQs_List").show();
                                $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                                $(".tblDocFAQs_ListClass").css({ "width": "100%" });
                            },
                            error: function (xhr, textStatus, error) {
                                if (typeof console == "object") {
                                    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                }
                            }
                        });
                    }
                });
            }
        }

        function ShowHideTheJQDataTable(DocumentID) {
           // var DocumentID = $('#<%=ddlDocument.ClientID%> option:selected').val();
            $("#<%=hdnDocID.ClientID%>").val(DocumentID);
            if (DocumentID > 0) {
                //showing the Buttion Create new
                $("#btnNewFAQ").show();
                $("#divtblDocFaqList").show();
                loadDocFAQ_Master();
            }
            else {
                //hiding the create new button
                $("#btnNewFAQ").hide();
                $("#divtblDocFaqList").hide();
            }
        }
    </script>
    <!-- End of DOCFAQ JQDT-->

    <!--DocFAQ History jQuery DataTable-->
    <script>
        $.ajaxSetup({
            cache: false
        });

        var tblFAQ_History;

        function loadDocFAQ_History() {
            if ($('#<%=hdnDocFAQQuestionID.ClientID%>').val() != "0") {
                if (tblFAQ_History != null) {
                    tblFAQ_History.destroy();
                }
                tblFAQ_History = $('#tblFAQ_History').DataTable({
                    columns: [
                        { 'data': 'DocFAQ_ID' },
                        { 'data': 'ModifiedBy' },
                        { 'data': 'ModifiedDate' },
                        { 'data': 'ModifiedReason' }
                    ],
                    "order": [[2, "desc"]],
                    "scrollY": "374px",
                    "aoColumnDefs": [{ "targets": [0], "visible": false }, { className: "textAlignLeft", "targets": [1, 3,] }],
                    "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/DocumentFAQ/DocumentFAQ_Service.asmx/DocFAQ_History_List")%>",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({ "name": "DocFaqID", "value": $('#<%=hdnDocFAQQuestionID.ClientID%>').val() });
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                UserSessionCheck();
                                $("#tblFAQ_History").show();
                                $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                                $(".tblFAQ_HistoryClass").css({ "width": "100%" });
                            },
                            error: function (xhr, textStatus, error) {
                                if (typeof console == "object") {
                                    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                }
                            }
                        });
                    }
                });
            }
            else {
                tblFAQ_History = $('#tblFAQ_History').DataTable();
            }
        }

        function ReloadtblFAQ_History() {
            loadDocFAQ_History();
        }
    </script>
    <!--End of FAQ HIStory JQDT-->

    <script>
        $(function () {
            $("#btnNewFAQ").hide();
        });
    </script>

    <!--End of DocFAQ JQ List-->
    <script>
        //for updating the Question.
        function UpdateDocFAQ(DocQuestionID) {
            $("#<%=hdnDocFAQQuestionID.ClientID%>").val(DocQuestionID);
            $("#<%=btnEditDocFAQ.ClientID%>").click();
        }
        //for viewing the history.
        function ViewDocFAQ_History_List(DocQuestionID) {
            $("#<%=hdnDocFAQQuestionID.ClientID%>").val(DocQuestionID);
            $('#modelFAQ_History').modal({ backdrop: 'static', keyboard: false });
        }

        $('#modelFAQ_History').on('shown.bs.modal', function (e) {
            ReloadtblFAQ_History();
        });
        //For Openiong the DocFaqModalpopup
        function OpenModelFAQ(Type) {
            if (Type == "NewFAQ") {
                document.getElementById("<%=txtDocFAQ_Question.ClientID%>").value = '';
                document.getElementById("<%=txtDocFAQ_Answer.ClientID%>").value = '';
                document.getElementById("<%=txtDocFAQ_Comments.ClientID%>").value = '';
                $('#lblFAQTitle').text("New FAQ");
                $("#<%=hdnNewOrEditFAQ.ClientID%>").val("NewFAQ");
                $("#<%=btnSubmitDocFAQ.ClientID%>").val("Submit");
                $("#<%=btnSubmitDocFAQ.ClientID%>").attr('title', 'Click to Submit');
                $('#divFaqComments').hide();
                $('#divRadioBtns').hide();
            }
            if (Type == "EditFAQ") {
                $('#lblFAQTitle').text("Edit FAQ");
                $("#<%=hdnNewOrEditFAQ.ClientID%>").val("EditFAQ");
                $("#<%=btnSubmitDocFAQ.ClientID%>").val("Update");
                $("#<%=btnSubmitDocFAQ.ClientID%>").attr('title','Click to Update');
                $('#divFaqComments').show();
                $('#divRadioBtns').show();
            }
            $('#modelFAQ').modal({ backdrop: 'static', keyboard: false });
        }
        function CheckWethereNewOrEditFAQ() {
            var FAQType = document.getElementById("<%=hdnNewOrEditFAQ.ClientID%>").value;
            if (FAQType == "EditFAQ") {
                $("#<%=btnSubmitDocFAQ.ClientID%>").val("Update");
                $('#lblFAQTitle').text("Edit FAQ");
                $('#divFaqComments').show();
                $('#divRadioBtns').show();
            }
        }
        function DocFAQValidation() {
            var Question = document.getElementById("<%=txtDocFAQ_Question.ClientID%>").value.trim();
            var Answer = document.getElementById("<%=txtDocFAQ_Answer.ClientID%>").value.trim();
            var Comments = document.getElementById("<%=txtDocFAQ_Comments.ClientID%>").value.trim();
            var FAQType = document.getElementById("<%=hdnNewOrEditFAQ.ClientID%>").value;
            errors = [];
            if (Question == "") {
                errors.push("Enter Question");
            }
            if (Answer == "") {
                errors.push("Enter Answer");
            }
            if (FAQType == "EditFAQ") {
                if (Comments == "") {
                    errors.push("Enter Modified Reason In Remarks");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join(",<br/>").toString() + '.', "error", "LoadTheCursor()");
                return false;
            }
            else {
                ShowPleaseWait('Show');
                return true;
            }
        }
        function LoadTheCursor() {
            if (document.getElementById("<%=txtDocFAQ_Question.ClientID%>").value == "") {
                document.getElementById("<%=txtDocFAQ_Question.ClientID%>").focus();
            }
            else if (document.getElementById("<%=txtDocFAQ_Answer.ClientID%>").value == "") {
                document.getElementById("<%=txtDocFAQ_Answer.ClientID%>").focus();
            }
            else if (document.getElementById("<%=hdnNewOrEditFAQ.ClientID%>").value == "EditFAQ") {
                if (document.getElementById("<%=txtDocFAQ_Comments.ClientID%>").value == "") {
                    document.getElementById("<%=txtDocFAQ_Comments.ClientID%>").focus();
                }
            }
        }
        $('#modelFAQ').on('shown.bs.modal', function () {
            document.getElementById("<%=txtDocFAQ_Question.ClientID%>").focus();
        })

        function CloseFAQModal() {
            $('#modelFAQ').modal('hide');
            loadDocFAQ_Master();
        }
        function changebtnText() {
            $("#<%=btnSubmitDocFAQ.ClientID%>").val("Submit");
        }
    </script>

    <script>
        function ClearText() {
            $("#<%=btnSubmitDocFAQ.ClientID%>").val("Submit");
        document.getElementById("<%=txtDocFAQ_Question.ClientID%>").value = '';
        document.getElementById("<%=txtDocFAQ_Answer.ClientID%>").value = '';
        document.getElementById("<%=txtDocFAQ_Comments.ClientID%>").value = '';
    }
    </script>
</asp:Content>
