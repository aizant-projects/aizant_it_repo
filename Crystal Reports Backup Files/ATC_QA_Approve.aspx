﻿<%@ Page Title="ATC QA Approval" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="ATC_QA_Approve.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.ATC.list.ATC_QA_Approve" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      <asp:HiddenField ID="hdnEmpID" runat="server" Value="0"/>
    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnATC_QA_Approval" runat="server" />
            <asp:HiddenField ID="hdnPrint" runat="server" Value="false" />
            <asp:HiddenField runat="server" ID="hdnNotificationID" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="col-12 padding-none" id="Div3" runat="server">
                
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">Annual Training Calendar QA Approval</div>

                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none" style="overflow: auto; margin-top: 10px">
                    <table id="tblATC_QA_Approval" class="datatable_cust tblATC_QA_ApprovalClass display breakword" style="width: 100%">
                        <thead>
                            <tr>
                                <th>ATC_ID</th>
                                <th>Department</th>
                                <th>Calendar Year</th>
                                <th>Author</th>
                                <th>Reviewer</th>
                                <th>Status</th>
                                <th>StatusID</th>
                                <th>View</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnViewATC_QA_Approval" runat="server" OnClick="btnViewATC_QA_Approval_Click" Text="Button"/>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script>
        function ViewATC_QA_Approval(ATC_ID) {
            $("#<%=hdnATC_QA_Approval.ClientID%>").val(ATC_ID);
            $("#<%=btnViewATC_QA_Approval.ClientID%>").click();
        }
    </script>

    <!--ATC_Pending List jQuery DataTable-->
    <script>
        function getUrlVars() {
            var vars = [], hash;
            var url = window.location.href;
            var hashes = url.slice(url.indexOf('?') + 1).split('&');
            if (url.indexOf("#") > 0) {
                var hashes = url.slice(url.indexOf('?') + 1, url.indexOf('#')).split('&');
            }

            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        var oTable = $('#tblATC_QA_Approval').DataTable({
            columns: [
                { 'data': 'ATC_ID' },
                { 'data': 'DepartmentName' },
                { 'data': 'CalendarYear' },
                { 'data': 'Author' }, 
                { 'data': 'Reviewer' },
                { 'data': 'CurrentStatus' },
                { 'data': 'StatusID' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="view"  href="#" onclick="ViewATC_QA_Approval(' + o.ATC_ID + ');"></a>'; }
                }
            ],
            "scrollY": "47vh",
            "aoColumnDefs": [{ "targets": [0, 6], "visible": false }, { className: "textAlignLeft", "targets": [1, 3, 4, 5] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/ATC_ListService.asmx/ATC_QA_Approval")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "ApproverID", "value": <%=hdnEmpID.Value%> }, { "name": "FilterType", "value": getUrlVars()["FilterType"] }, { "name": "Notification_ID", "value": <%=hdnNotificationID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblATC_QA_Approval").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        $(".tblATC_QA_ApprovalClass").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
            
        });

    </script>
</asp:Content>
