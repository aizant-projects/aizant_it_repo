﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="Document_Details.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentCreator.Document_Details" EnableEventValidation="false" ValidateRequest="false" %>
<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<link href="<%=ResolveUrl("~/AppCSS/DMS/DMS_StyleSheet1.css")%>" rel="stylesheet" />--%>
    <style>
        .CreatorGrid thead tr th {
            background-color: #07889a;
            color: #fff;
            text-align: left !important;
            padding: 4px 16px;
            border-right: 1px solid #000;
        }
     
        .CreatorGrid tbody tr th {
            background-color: #07889a;
            color: #fff;
            text-align: left !important;
            padding: 4px 16px;
            border-right: 1px solid #000;
        }

        table tbody tr td {
            text-align: center;
            height: 30px;
        }
        th {
            text-align: center !important;
        }

        .CreatorGrid tbody tr td {
            text-align: left;
            border: none;
            border-bottom: 1px solid #3e3e3e;
            padding: 4px 16px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            background-color: #07889a !important;
        }

        .richText .richText-help {
            display: none !important;
        }

        .datalistcss {
            overflow-x: hidden;
            height: 180px;
            border-color: #CCCCCC;
            border-width: 1px;
            border-style: Solid;
            border-collapse: collapse;
            padding: 5px;
            border-radius: 5px;
        }

        .header_profile {
            background: #07889a;
            padding: 10px;
            color: #fff;
            font-size: 12pt;
            font-family: seguisb;
        }

        iframe {
            overflow-y: hidden;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnFieldFrom" runat="server" Value="1" />
            <asp:HiddenField ID="hdnNewDocTab" runat="server" Value="Create" />
            <asp:HiddenField ID="hdnGrdiReferesh" runat="server" Value="Link" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="padding-none float-right">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class="col-md-12 col-lg-12 col-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div class="col-md-12 col-lg-12 col-12 col-sm-12 dms_outer_border bottom padding-none float-left" id="divMainContainer" runat="server" visible="true">
       <div id="accordion_author" class="accordion qms_accordion">
           <div class="card mb-0">
               <div class="card-header header_col  accordion-toggle" data-toggle="collapse" href="#collapse1">
                      
                                <a class="card-title">Document Details
                                </a>
                          </div>
           
                 <div id="collapse1" class=" show grid_panel_full border_top_none" data-parent="#accordion_author1">
                    <div class="card-body acc_boby float-left ">
                        <asp:UpdatePanel ID="UpMyDocFiles" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-lg-12 content_bg float-left ">
                                    <div class="col-lg-6 top bottom float-left padding-none" style="border-right: 1px solid #d4d4d4;">
                                        <div class="col-sm-12 form-group float-left padding-right_div">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Request Type</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_requestType" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 form-group float-left top padding-right_div">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Document Type</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_documentType" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class=" col-sm-12 form-group float-left top padding-right_div">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Department</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_Department" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class=" col-sm-12 form-group float-left top padding-right_div">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Document Number</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_DocumentNumber" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class=" col-sm-12 form-group float-left top padding-right_div">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Document Name</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_DocumentName" runat="server" TextMode="MultiLine" CssClass="form-control" Height="80px" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 top float-left padding-none">
                                        <div class="col-sm-12 form-group top float-left ">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Version</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_VersionID" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 form-group float-left top">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Privacy</label>
                                            <div class="col-sm-8 profile_defined float-left" style="margin-top:-9px !important">
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList1" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Public" Value="yes"></asp:ListItem>
                                                    <asp:ListItem Text="Private" Value="no"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class=" col-sm-12 form-group float-left top">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Author</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_Author" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 form-group float-left top" runat="server" id="div1">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Authorizer</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_Authorizer" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class=" col-sm-12 form-group float-left top">
                                            <label for="inputEmail3" runat="server" id="lblComments" class=" padding-none profile_label col-sm-4 float-left"></label>
                                            <%--<asp:Label ID="lblComments" runat="server" class="profile_label padding-none  col-sm-4 float-left" Text=""></asp:Label>--%>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_Comments" runat="server" TextMode="MultiLine" CssClass="form-control" Width="100%" Height="80px" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-12 col-sm-12 grid_border top padding-none float-left">
                                        <asp:UpdatePanel ID="UPgvApprover" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <div class="panel-heading_title col-md-12 col-lg-12 col-12 col-sm-12 float-left">
                                                   Selected Employees
                                                </div>
                                                <div class="col-md-12 col-lg-12 col-12 col-sm-12 float-left grid_panel_full bottom">
                                                    <asp:GridView ID="gvApprover" CssClass=" col-md-12 col-lg-12 col-12 col-sm-12 bottom  top " runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:BoundField DataField="Role" HeaderText="Role" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                                            <asp:BoundField DataField="EmpName" HeaderText="Employee" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                                            <asp:BoundField DataField="DeptCode" HeaderText="DeptCode" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            <div class="card-header header_col  accordion-toggle" data-toggle="collapse" href="#collapse2">
                      
                                <a class="card-title">Create Document
                                </a>
                          </div>
           
                <%--<div class="panel-heading_title_dms accordion-toggle" data-toggle="collapse" data-parent="#accordion_author_in" href="#collapse2">
                    <h4 class="panel-title">
                        <a>Create Document</a>
                    </h4>
                </div>--%>
             
                <div id="collapse2" class=" show grid_panel_full border_top_none" data-parent="#accordion_author2">
                    <div class="card-body acc_boby float-left ">
                
                        <div class=" col-lg-12  padding-none float-left">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-12 float-left">
                                <ul class="nav nav-tabs tab_grid top">
                                    
                                    <li class="   nav-item" runat="server" id="liCreate" onclick="TabCreateClick();"><a class="Createtab nav-link active" data-toggle="tab" href="#home">Create</a></li>
                                    <li class="   nav-item" runat="server" id="liModify" onclick="TabModifyClick();"><a class="Modifytab nav-link active" data-toggle="tab" href="#home">Modify</a></li>
                                    <li class="nav-item" onclick="TabReferralClick();"><a class="Refferaltab nav-link " data-toggle="tab" href="#menu1">Referrals</a></li>
                                    <li class="float-right nav-item  ml-auto">
                                        <div class="edit_botton_reviewer float-right ">
                                            <asp:Button ID="Btn_ShowOriginal" runat="server" Text="Show Original" CssClass=" btn-track_edit" OnClick="Btn_ShowOriginal_Click" OnClientClick="showImg();"/>
                                            <asp:DropDownList ID="ddl_RevertedBy" runat="server" CssClass="selectpicker drop_down regulatory_dropdown_style" data-live-search="true" OnSelectedIndexChanged="ddl_RevertedBy_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            <asp:Button ID="Btn_ShowModified" runat="server" Text="Show Modified" CssClass=" btn-track_edit" OnClick="Btn_ShowModified_Click" OnClientClick="showImg();"/>
                                        </div>
                                    </li>
                                    </ul>
                                <div class="tab-content">
                                    <div id="home" class="Createtabbody tab-pane fade  active ">
                                        <div class="top col-lg-12 float-left padding-none" id="WordUpload">
                                            <div class="padding-none float-left" id="lbl_Upload" runat="server"><b class="grid_icon_legend padding-none">Upload your word File:</b></div>
                                            <div class="col-lg-9 padding-left_div float-left">
                                                <asp:FileUpload ID="file_word" CssClass="btn-file-upload" value="Choose File" runat="server" />
                                            </div>
                                            <asp:UpdatePanel ID="upUpload" runat="server" UpdateMode="Conditional" style="text-align: right">
                                                <ContentTemplate>
                                                    <asp:Button ID="Btn_Upload" runat="server" Text="Upload" CssClass=" btn-signup_popup" ValidationGroup="edit" OnClientClick="return Required();" OnClick="Btn_Upload_Click" />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="Btn_Upload" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div class="col-lg-12 float-left padding-none">
                                            <div class="bottom col-lg-6 float-left" id="divinfotext" style="display: none">
                                                <span class="note_lbl">NOTE:</span>
                                                <asp:Label ID="lbl_InfoText" class="dms_info_text" runat="server" Text="Enable track changes option in word document before uploading"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="listPDF_Viewer" id="divPDF_Viewer">
                                            <noscript>
                                                Please Enable JavaScript.
                                            </noscript>
                                        </div>
                                        <label for="inputEmail3" class="form-group  padding-none col-sm-12">Remarks :<span class="smallred_label">*</span></label>
                                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 form-group padding-none">
                                            <textarea ID="txt_RevertComments" runat="server" class="form-control" rows="5" placeholder="Enter Remarks" maxlength="3000"></textarea>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-12 col-sm-12  float-left padding-none top bottom">
                                            <input type="button" data-dismiss="modal" runat="server" id="btn_Cancel" class="float-right btn-cancel_popup" onclick="return ReloadCreatorPage();" value="Cancel" style="margin-left:2px;"/>
                                            <asp:UpdatePanel ID="upbns" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Button ID="btn_SubmittoReviewer" runat="server" Text="Submit" CssClass="float-right btn-signup_popup" OnClientClick="openElectronicSignModal();" ValidationGroup="edit" />
                                                    <asp:Button ID="btn_Download" runat="server" Text="Download" CssClass="float-left btn-signup_popup" OnClick="btn_Download_Click" Visible="false"/>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btn_Download" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                            <input type="button" class="float-left btn-revert_popup" onclick="return ConfirmAlert();" value="Save Draft" style="display: none" />
                                        </div>
                                    </div>
                                    <div id="menu1" class="Refferaltabbody tab-pane fade ">
                                        <div class="col-lg-12">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-12 top padding-none">
                                                <ul class="nav nav-tabs tab_grid">
                                                    <li class=" nav-item "><a class="Linktab nav-link active" data-toggle="tab" href="#home1">Link Referrals</a></li>
                                                    <li class=" nav-item"><a class="internaltab nav-link " data-toggle="tab" href="#menu12">Internal Documents</a></li>
                                                    <li class=" nav-item"><a class="Externaltab nav-link " data-toggle="tab" href="#menu23">External Documents</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-12 padding-none tab-content">
                                                <div id="home1" class="Linktabbody tab-pane fade  top ">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <asp:UpdatePanel ID="uplinkDoc" runat="server">
                                                                <ContentTemplate>
                                                                    <textarea id="txtLinkReferrences" runat="server" class="rtxtcontent" maxlength="300"></textarea>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="menu12" class="internaltabbody tab-pane fade ">
                                                    <asp:UpdatePanel ID="upInternalDoc" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="row">
                                                                <br />
                                                                <div class="col-sm-12 float-left top">
                                                                    <div class="float-left profile_label" style="background: none !important">Departments:</div>
                                                                    <div class="col-sm-4 float-left">
                                                                        <asp:DropDownList ID="ddl_Dept" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 form-control selectpicker drop_down showtick regulatory_dropdown_style" data-live-search="true" data-size="7" AutoPostBack="true" OnSelectedIndexChanged="ddl_Dept_SelectedIndexChanged"></asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
                                                                <div class="col-lg-5 col-md-5 top float-left padding-none " id="divAvailable" runat="server">
                                                                    <div class="col-12 panel-heading_title float-left">
                                                                        Available Document
                                                                        <div class=" float-right padding-none"><input name="txtTerm" class="gridSearchBox1" type="search" oninput="GridViewSearch(this, '<%=gvAvailableDocuments.ClientID %>')" placeholder="Search Available Documents" readonly onfocus="this.removeAttribute('readonly');" />
                                                                    </div></div>
                                                                    <div class="col-md-12 col-lg-12 col-12 col-sm-12" style="border: 1px solid #07889a; margin-top: -10px; max-height: 500px; min-height: 500px; overflow-y: auto">
                                                                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 top padding-none">
                                                                            <asp:GridView ID="gvAvailableDocuments" BorderWidth="1" CssClass="CreatorGrid" runat="server" AutoGenerateColumns="false" Width="100%" EmptyDataText="Documents Not Available">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="">
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="cbxSelect" runat="server" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Document Number">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lbl_DocumentNumber" runat="server" Text='<%# Eval("DocumentNumber") %>'></asp:Label>
                                                                                            <asp:Label ID="lbl_DocumentID" runat="server" Text='<%# Eval("VersionID") %>' Visible="false"></asp:Label>
                                                                                            <asp:Label ID="lbl_DeptID" runat="server" Text='<%# Eval("DeptID") %>' Visible="false"></asp:Label>
                                                                                            <asp:Label ID="lbl_DocID" runat="server" Text='<%# Eval("DocumentID") %>' Visible="false"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Document Name">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lbl_DocumentName" runat="server" Text='<%# Eval("DocumentName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2 float-left" id="divAddRemove" runat="server" style="margin-top: 13em; text-align: center;">
                                                                    <div class="btn-group-vertical" style="padding-right: 20px;">
                                                                        <asp:Button ID="btn_AddDocuments" runat="server" class="col-lg-6 col-lg-offset-5 btn_selection_boxDms" Text=">" ToolTip="Add Documents" Style="margin-bottom: 10px; font-weight: bold" OnClick="btn_AddDocuments_Click" OnClientClick="showImg();"/>
                                                                        <asp:Button ID="btn_RemoveDocuments" runat="server" class="col-lg-6  col-lg-offset-5 btn_selection_boxDms" Text="<" ToolTip="Remove Documents" Style="font-weight: bold" OnClick="btn_RemoveDocuments_Click" OnClientClick="showImg();"/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 col-md-5 padding-none float-left">
                                                                    <div class="col-md-12 col-lg-12 col-12 col-sm-12 top float-left padding-none  " id="divAssigned" runat="server">
                                                                        <div class="col-12 panel-heading_title ">
                                                                            Assigned Document
                                                                        </div>
                                                                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 float-left" style="border: 1px solid #07889a; max-height: 500px; min-height: 500px; overflow-y: auto">
                                                                            <div class="col-md-12 col-lg-12 col-12 col-sm-12 top padding-none float-left">
                                                                                <asp:GridView ID="gvAssignedDocuments" BorderWidth="1" CssClass="CreatorGrid" runat="server" AutoGenerateColumns="false" Width="100%">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="">
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="cbxSelect" runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Document Number">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbl_DocumentNumber" runat="server" Text='<%# Eval("DocumentNumber") %>'></asp:Label>
                                                                                                <asp:Label ID="lbl_DocumentID" runat="server" Text='<%# Eval("VersionID") %>' Visible="false"></asp:Label>
                                                                                                <asp:Label ID="lbl_DeptID" runat="server" Text='<%# Eval("DeptID") %>' Visible="false"></asp:Label>
                                                                                                <asp:Label ID="lbl_DocID" runat="server" Text='<%# Eval("DocumentID") %>' Visible="false"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Document Name">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbl_DocumentName" runat="server" Text='<%# Eval("DocumentName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <div class="Externaltabbody tab-pane fade top" id="menu23">
                                                    <div>
                                                        <div class="col-sm-12 padding-none top">
                                                            <div class="float-left profile_label padding-right_div" style="background: none !important">Files:</div>
                                                            <div class="col-sm-9 float-left padding-none ">
                                                                <asp:FileUpload ID="FileUpload1" CssClass="btn-file-upload-multiple" value="Choose Files" runat="server" AllowMultiple="true" />

                                                            </div>
                                                            <div class="bottom padding-none float-right">
                                                                <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Button ID="btnFileUpload" runat="server" Text="Upload " CssClass="float-right  btn-signup_popup " OnClientClick="return Required1();" OnClick="btnFileUpload_Click" />
                                                                        </div>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnFileUpload" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                        <div class="col-12  padding-none">
                                                            <div class="col-sm-12 padding-none ">
                                                                <div class="col-sm-12 float-left padding-none">
                                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:GridView ID="gvExternalFiles" runat="server" AutoGenerateColumns="false" CssClass="col-12 CreatorGrid" EmptyDataText="No files uploaded" Width="100%" BorderColor="#07889a" OnRowDataBound="gvExternalFiles_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="FileName" HeaderText="File Name" />
                                                                                    <asp:TemplateField HeaderText="View Doc">
                                                                                        <ItemTemplate>
                                                                                            <asp:UpdatePanel ID="upgvbutton" runat="server" UpdateMode="Conditional">
                                                                                                <ContentTemplate>
                                                                                                    <asp:LinkButton ID="lnkDownload" class="view" CommandArgument='<%# Eval("FileName") +"@$"+Eval("Ref_FileID")+"@$"+Eval("pkid")%>' runat="server" OnClick="DownloadFile" OnClientClick="showImg();"></asp:LinkButton>
                                                                                                </ContentTemplate>
                                                                                                <Triggers>
                                                                                                    <asp:PostBackTrigger ControlID="lnkDownload" />
                                                                                                </Triggers>
                                                                                            </asp:UpdatePanel>
                                                                                            <asp:Label ID="lbl_isSavedFile" runat="server" Text='<%# Eval("isSavedFile") %>' Visible="false"></asp:Label>
                                                                                            <asp:Label ID="lbl_Ref_FileID" runat="server" Text='<%# Eval("Ref_FileID") %>' Visible="false"></asp:Label>
                                                                                            <asp:Label ID="lbl_pkid" runat="server" Text='<%# Eval("pkid") %>' Visible="false"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Remove">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="lnkDelete" Text="" CssClass="text-center" CommandArgument='<%# Eval("FileName")+"@$"+Eval("Ref_FileID")+"@$"+Eval("pkid") %>' runat="server" OnClick="DeleteFile" OnClientClick="showImg();"><i class="glyphicon glyphicon-trash" title="Remove" style="color:red"></i></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-lg-12 col-12 col-sm-12 top padding-none float-left bottom">
                                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Button ID="btn_Refferals_Cancel" runat="server" Text="Cancel" CssClass="top float-right  btn-cancel_popup bottom" OnClick="btn_Refferals_Cancel_Click" style="margin-left:2px;"/>
                                                            <asp:Button ID="btn_Add_Referrences" runat="server" Text="Save" CssClass="top float-right  btn-signup_popup bottom" OnClick="btn_Add_Referrences_Click" OnClientClick="showImg();" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
       <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="sticky-popup open" id="divcomments" runat="server" visible="false">
                            <div class="popup-wrap">
                                <div class="popup-header">
                                    <span class="popup-title">Comments<div class="popup-image"></div>
                                    </span>
                                </div>
                                <div class="popup-content">
                                    <div class="popup-content-pad">
                                        <div class="popup-form" id="popup-1560">
                                            <div class="popup-form--part-title-font-weight-normal" id="popup-form-1560">
                                                <div class="popup-form__part popup-part popup-part--multi_line_text popup-part--width-full popup-part--label-above"
                                                    id="popup-1560_multi_line_text_4-part" data-popup-type="multi_line_text">
                                                    <div class="popup-part-wrap">
                                                        <div class="popup-part__el">
                                                            <div class="col-md-12 col-lg-12 col-12 col-sm-12 form-group top bottom">
                                                                <div class="col-md-12 col-lg-12 col-12 col-sm-12 bottom padding-none top" style="border: 1px solid #2e2e2e; max-height: 338px; min-height: 250px; overflow-y: auto;" id="divgvcomments">
                                                                    loading..
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
    <!--------Progress Bar VIEW-------------->
    <div id="modalProgressbar" data-backdrop="static" class="modal department fade" role="dialog">
        <div class=" modal-dialog modal-md" style="min-width: 47%">
            <!-- Modal content-->
            <div class="modal-content ">
               <div class="modal-header">
                    <span id="span1" runat="server">Progress Bar</span>
                </div>
             <div class="modal-body">
                <div class="col-md-12 col-lg-12 col-12 col-sm-12 float-left">
                    
                                <img alt="" src="<%=ResolveUrl("~/Images/TMS_Images/please_wait.gif")%>" />
                        
                </div>
                    </div>
            </div>
        </div>
    </div>
    <!--------End Progress Bar VIEW-------------->

    <!--------External Refferal VIEW-------------->
    <div id="mpop_FileViewer" class="modal department fade" role="dialog">
        <div class=" modal-dialog modal-lg" style="min-width: 86%">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    
                    <span id="span5" runat="server">External Referral</span>
                    <button type="button" class="close" data-dismiss="modal" onclick="ShowRefferal();">&times;</button>
                </div>
                <div class="col-md-12 col-lg-12 col-12 col-sm-12" style="background-color: #fff">
                    <div class="col-sm-12">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="ExternalRefDiv" runat="server">
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------End External Refferal VIEW-------------->

    <asp:HiddenField ID="hdfPKID" runat="server" Value="123" />
    <asp:HiddenField ID="hdfPID" runat="server" />
    <asp:HiddenField ID="hdfConfirm" runat="server" />
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdfCrChange" runat="server" Value="0" />
    <asp:HiddenField ID="hdfEmpID" runat="server" />
    <asp:HiddenField ID="hdfFromRoleID" runat="server" />
    <asp:HiddenField ID="hdfDocStatus" runat="server" />
    <asp:HiddenField ID="hdfFromEmpID" runat="server" />
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <asp:HiddenField ID="hdfViewDivID" runat="server" />
    <asp:HiddenField ID="hdfIsFirst" runat="server" Value="0" />
    <asp:HiddenField ID="hdfFileName" runat="server" />
    <asp:HiddenField ID="hdfRUpload" runat="server" Value="0" />
    <script>
        function Required() {
            var fullPath = document.getElementById("<%=file_word.ClientID%>").value;
            var filename = "";
            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }
            }
            errors = [];
            if (filename == "") {
                errors.push("Please Upload file.");
            }
            if (filename.length > 95) {
                errors.push("Uploaded file length should not be greater than 95 characters");
            }
            if (filename.includes(",") || filename.includes("/") || filename.includes(":") || filename.includes("*") || filename.includes("?") ||
                filename.includes("<") || filename.includes(">") || filename.includes("|") || filename.includes("\"") || filename.includes("\\") ||
                filename.includes(";")) {
                errors.push("Uploaded file name can contain only the following special characters !@#()-_'+=$^%&~` ");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                $('#modalProgressbar').modal({ show: true });
                
                return true;
            }
        }
    </script>
    <script>
        function Required1() {
            var file = document.getElementById("<%=FileUpload1.ClientID%>").value;
            errors = [];
            if (file == "") {
                errors.push("Please Upload file.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            $('.rtxtcontent').richText({
                // text formatting
                bold: true,
                italic: true,
                underline: true,
                // text alignment
                leftAlign: true,
                centerAlign: true,
                rightAlign: false,
                // lists
                ol: false,
                ul: false,
                // title
                heading: true,
                // colors
                fontColor: false,
                // uploads
                imageUpload: false,
                fileUpload: false,
                //videoUpload: false,
                // media
                videoEmbed: false,
                // link
                urls: true,
                // tables
                table: false,
                // code
                removeStyles: true,
                code: false,
                // colors
                colors: [],
                // dropdowns
                fileHTML: '',
                imageHTML: '',
                //videoHTML: '',
                // translations
                translations: {
                    'title': 'Title',
                    'white': 'White',
                    'black': 'Black',
                    'brown': 'Brown',
                    'beige': 'Beige',
                    'darkBlue': 'Dark Blue',
                    'blue': 'Blue',
                    'lightBlue': 'Light Blue',
                    'darkRed': 'Dark Red',
                    'red': 'Red',
                    'darkGreen': 'Dark Green',
                    'green': 'Green',
                    'purple': 'Purple',
                    'darkTurquois': 'Dark Turquois',
                    'turquois': 'Turquois',
                    'darkOrange': 'Dark Orange',
                    'orange': 'Orange',
                    'yellow': 'Yellow',
                    'imageURL': 'Image URL',
                    'fileURL': 'File URL',
                    'linkText': 'Link text',
                    'url': 'URL',
                    'size': 'Size',
                    'responsive': '<a href="https://www.jqueryscript.net/tags.php?/Responsive/">Responsive</a>',
                    'text': 'Text',
                    'openIn': 'Open in',
                    'sameTab': 'Same tab',
                    'newTab': 'New tab',
                    'align': 'Align',
                    'left': 'Left',
                    'center': 'Center',
                    'right': 'Right',
                    'rows': 'Rows',
                    'columns': 'Columns',
                    'add': 'Add',
                    'pleaseEnterURL': 'Please enter an URL',
                    'videoURLnotSupported': 'Video URL not supported',
                    'pleaseSelectImage': 'Please select an image',
                    'pleaseSelectFile': 'Please select a file',
                    'bold': 'Bold',
                    'italic': 'Italic',
                    'underline': 'Underline',
                    'alignLeft': 'Align left',
                    'alignCenter': 'Align centered',
                    'alignRight': 'Align right',
                    'addOrderedList': 'Add ordered list',
                    'addUnorderedList': 'Add unordered list',
                    'addHeading': 'Add Heading/title',
                    'addFont': 'Add font',
                    'addFontColor': 'Add font color',
                    'addFontSize': 'Add font size',
                    'addImage': 'Add image',
                    'addVideo': 'Add video',
                    'addFile': 'Add file',
                    'addURL': 'Add URL',
                    'addTable': 'Add table',
                    'removeStyles': 'Remove styles',
                    'code': 'Show HTML code',
                    'undo': 'Undo',
                    'redo': 'Redo',
                    'close': 'Close'
                },
                // developer settings
                useSingleQuotes: false,
                height: 0,
                heightPercentage: 0,
                id: "",
                class: "",
                useParagraph: false,
                hasFocus: true

            });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(document).ready(function () {
                $('.rtxtcontent').richText({
                    // text formatting
                    bold: true,
                    italic: true,
                    underline: true,
                    // text alignment
                    leftAlign: true,
                    centerAlign: true,
                    rightAlign: false,
                    // lists
                    ol: false,
                    ul: false,
                    // title
                    heading: true,
                    // colors
                    fontColor: false,
                    // uploads
                    imageUpload: false,
                    fileUpload: false,
                    //videoUpload: false,
                    // media
                    videoEmbed: false,
                    // link
                    urls: true,
                    // tables
                    table: false,
                    // code
                    removeStyles: true,
                    code: false,
                    // colors
                    colors: [],
                    // dropdowns
                    fileHTML: '',
                    imageHTML: '',
                    //videoHTML: '',                        
                    // translations
                    translations: {
                        'title': 'Title',
                        'white': 'White',
                        'black': 'Black',
                        'brown': 'Brown',
                        'beige': 'Beige',
                        'darkBlue': 'Dark Blue',
                        'blue': 'Blue',
                        'lightBlue': 'Light Blue',
                        'darkRed': 'Dark Red',
                        'red': 'Red',
                        'darkGreen': 'Dark Green',
                        'green': 'Green',
                        'purple': 'Purple',
                        'darkTurquois': 'Dark Turquois',
                        'turquois': 'Turquois',
                        'darkOrange': 'Dark Orange',
                        'orange': 'Orange',
                        'yellow': 'Yellow',
                        'imageURL': 'Image URL',
                        'fileURL': 'File URL',
                        'linkText': 'Link text',
                        'url': 'URL',
                        'size': 'Size',
                        'responsive': '<a href="https://www.jqueryscript.net/tags.php?/Responsive/">Responsive</a>',
                        'text': 'Text',
                        'openIn': 'Open in',
                        'sameTab': 'Same tab',
                        'newTab': 'New tab',
                        'align': 'Align',
                        'left': 'Left',
                        'center': 'Center',
                        'right': 'Right',
                        'rows': 'Rows',
                        'columns': 'Columns',
                        'add': 'Add',
                        'pleaseEnterURL': 'Please enter an URL',
                        'videoURLnotSupported': 'Video URL not supported',
                        'pleaseSelectImage': 'Please select an image',
                        'pleaseSelectFile': 'Please select a file',
                        'bold': 'Bold',
                        'italic': 'Italic',
                        'underline': 'Underline',
                        'alignLeft': 'Align left',
                        'alignCenter': 'Align centered',
                        'alignRight': 'Align right',
                        'addOrderedList': 'Add ordered list',
                        'addUnorderedList': 'Add unordered list',
                        'addHeading': 'Add Heading/title',
                        'addFont': 'Add font',
                        'addFontColor': 'Add font color',
                        'addFontSize': 'Add font size',
                        'addImage': 'Add image',
                        'addVideo': 'Add video',
                        'addFile': 'Add file',
                        'addURL': 'Add URL',
                        'addTable': 'Add table',
                        'removeStyles': 'Remove styles',
                        'code': 'Show HTML code',
                        'undo': 'Undo',
                        'redo': 'Redo',
                        'close': 'Close'
                    },
                    // developer settings
                    useSingleQuotes: false,
                    height: 0,
                    heightPercentage: 0,
                    id: "",
                    class: "",
                    useParagraph: false,
                    hasFocus: true
                });
            });
        });
    </script>
    <script>
        function ShowRefferal() {
            TabRefClick();
        }
        function ShowExternalFile() {
            $('#mpop_FileViewer').modal({ show: true, backdrop: 'static', keyboard: false });
            $('.edit_botton_reviewer').hide();
        }
        function HidePopup() {
            $('#usES_Modal').modal('hide');
        }
        function ReloadCreatorPage() {
            var Tvalue = document.getElementById("<%=hdnFieldFrom.ClientID%>").value;
            if (Tvalue == "1") {
                window.open("<%=ResolveUrl("~/DMS/DocumentCreator/Create_Document.aspx?tab=1")%>", "_self");
            }
            if (Tvalue == "2") {
                window.open("<%=ResolveUrl("~/DMS/DocumentCreator/Create_Document.aspx?tab=2")%>", "_self");
            }
        }
        function accordion_tab() {
            $("#accordion_author").accordion("option", "active", 1);
        }
    </script>
    <script>
        function ConfirmDelete(_commandArg) {
            custAlertMsg('Do You Want To Remove This Document?', 'confirm', true);
        }
        function ConfirmAlert() {
            document.getElementById("<%=hdfConfirm.ClientID%>").value = 'Save';
            custAlertMsg('Do You Want Save?', 'confirm', true);
        }
        function ConfirmAlert1() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Approve";
            openElectronicSignModal();
        }
    </script>
    <script>      
        function GridViewSearch(phrase, _id) {
            var words = phrase.value.toLowerCase().split(" ");
            var table = document.getElementById(_id);
            var ele;
            for (var r = 1; r < table.rows.length; r++) {
                ele = table.rows[r].innerHTML.replace(/<[^>]+>/g, "");
                var displayStyle = 'none';
                for (var i = 0; i < words.length; i++) {
                    if (ele.toLowerCase().indexOf(words[i]) >= 0)
                        displayStyle = '';
                    else {
                        displayStyle = 'none';
                        break;
                    }
                }
                table.rows[r].style.display = displayStyle;
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            ReferalsTabState();
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            ReferalsTabState();
        });
        function ReferalsTabState() {
            if ($('#<%=hdnNewDocTab.ClientID%>').val() == 'Create') {
                $('.internaltab').removeClass('active');
                $('.Externaltab').removeClass('active');
                $('.Createtab').addClass('active');
                $('.Linktab').removeClass('active');
                $('.Refferaltab').removeClass('active');
                $('.Externaltabbody').removeClass('active');
                $('.Externaltabbody').removeClass('show');
                $('.internaltabbody').removeClass('active');
                $('.internaltabbody').removeClass('show');
                $('.Createtabbody').addClass('active');
                $('.Createtabbody').addClass('show');
                $('.Linktabbody').removeClass('active');
                $('.Linktabbody').removeClass('show');
                $('.Refferaltabbody').removeClass('active');
                $('.Refferaltabbody').removeClass('show');
                $('.edit_botton_reviewer').show();
            }
            if ($('#<%=hdnNewDocTab.ClientID%>').val() == 'Modify') {
                $('.internaltab').removeClass('active');
                $('.Externaltab').removeClass('active');
                $('.Modifytab').addClass('active');
                $('.Linktab').removeClass('active');
                $('.Refferaltab').removeClass('active');
                $('.Externaltabbody').removeClass('active');
                $('.Externaltabbody').removeClass('show');
                $('.internaltabbody').removeClass('active');
                $('.internaltabbody').removeClass('show');
                $('.Createtabbody').addClass('active');
                $('.Createtabbody').addClass('show');
                $('.Linktabbody').removeClass('active');
                $('.Linktabbody').removeClass('show');
                $('.Refferaltabbody').removeClass('active');
                $('.Refferaltabbody').removeClass('show');
                $('.edit_botton_reviewer').show();
            }
            if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == '' && $('#<%=hdnNewDocTab.ClientID%>').val() == 'Refferal') {
                $('.internaltab').removeClass('active');
                $('.Externaltab').removeClass('active');
                $('.Createtab').removeClass('active');
                $('.Modifytab').removeClass('active');
                $('.Linktab').addClass('active');
                $('.Refferaltab').addClass('active');
                $('.Externaltabbody').removeClass('active');
                $('.Externaltabbody').removeClass('show');
                $('.internaltabbody').removeClass('active');
                $('.internaltabbody').removeClass('show');
                $('.Createtabbody').removeClass('active');
                $('.Createtabbody').removeClass('show');
                $('.Linktabbody').addClass('active');
                $('.Linktabbody').addClass('show');
                $('.Refferaltabbody').addClass('active');
                $('.Refferaltabbody').addClass('show');
            }
            if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == 'Link' && $('#<%=hdnNewDocTab.ClientID%>').val() == 'Refferal') {
                $('.internaltab').removeClass('active');
                $('.Externaltab').removeClass('active');
                $('.Createtab').removeClass('active');
                $('.Modifytab').removeClass('active');
                $('.Linktab').addClass('active');
                $('.Refferaltab').addClass('active');
                $('.Externaltabbody').removeClass('active');
                $('.Externaltabbody').removeClass('show');
                $('.internaltabbody').removeClass('active');
                $('.internaltabbody').removeClass('show');
                $('.Createtabbody').removeClass('active');
                $('.Createtabbody').removeClass('show');
                $('.Linktabbody').addClass('active');
                $('.Linktabbody').addClass('show');
                $('.Refferaltabbody').addClass('active');
                $('.Refferaltabbody').addClass('show');
            }
            if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == 'Internal' && $('#<%=hdnNewDocTab.ClientID%>').val() == 'Refferal') {
                $('.Externaltab').removeClass('active');
                $('.Linktab').removeClass('active');
                $('.Createtab').removeClass('active');
                $('.Modifytab').removeClass('active');
                $('.internaltab').addClass('active');
                $('.Refferaltab').addClass('active');
                $('.Externaltabbody').removeClass('active');
                $('.Externaltabbody').removeClass('show');
                $('.Linktabbody').removeClass('active');
                $('.Linktabbody').removeClass('show');
                $('.Createtabbody').removeClass('active');
                $('.Createtabbody').removeClass('show');
                $('.internaltabbody').addClass('active');
                $('.internaltabbody').addClass('show');
                $('.Refferaltabbody').addClass('active');
                $('.Refferaltabbody').addClass('show');
            }
            if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == 'External' && $('#<%=hdnNewDocTab.ClientID%>').val() == 'Refferal') {
                $('.internaltab').removeClass('active');
                $('.Linktab').removeClass('active');
                $('.Createtab').removeClass('active');
                $('.Modifytab').removeClass('active');
                $('.Externaltab').addClass('active');
                $('.Refferaltab').addClass('active');
                $('.internaltabbody').removeClass('active');
                $('.internaltabbody').removeClass('show');
                $('.Linktabbody').removeClass('active');
                $('.Linktabbody').removeClass('show');
                $('.Createtabbody').removeClass('active');
                $('.Createtabbody').removeClass('show');
                $('.Externaltabbody').addClass('active');
                $('.Externaltabbody').addClass('show');
                $('.Refferaltabbody').addClass('active');
                $('.Refferaltabbody').addClass('show');
            }
        }
    </script>
    <script>
        function TabRefClick() {
            $(".Linktab").click(function () {
                $('#<%=hdnGrdiReferesh.ClientID%>').val('Link');
            });
            $(".internaltab").click(function () {
                $('#<%=hdnGrdiReferesh.ClientID%>').val('Internal');
            });
            $(".Externaltab").click(function () {
                $('#<%=hdnGrdiReferesh.ClientID%>').val('External');
            });
            $(".Createtab").click(function () {
                $('#<%=hdnNewDocTab.ClientID%>').val('Create');
            });
            $(".Modifytab").click(function () {
                $('#<%=hdnNewDocTab.ClientID%>').val('Modify');
            });
            $(".Refferaltab").click(function () {
                $('#<%=hdnNewDocTab.ClientID%>').val('Refferal');
            });
        }
        function TabReferralClick() {
            $('#<%=hdnNewDocTab.ClientID%>').val('Refferal');
            $('.edit_botton_reviewer').hide();
            ReferalsTabState();
        }
        function TabCreateClick() {
            $('#<%=hdnNewDocTab.ClientID%>').val('Create');
            ReferalsTabState();
            var filename = document.getElementById("<%=hdfFileName.ClientID%>").value;
            ViewTempDocument('#divPDF_Viewer', filename);
        }
        function TabModifyClick() {
            $('#<%=hdnNewDocTab.ClientID%>').val('Modify');
            ReferalsTabState();
            var filename = document.getElementById("<%=hdfFileName.ClientID%>").value;
            if (filename == "") {
                ViewDocument('#divPDF_Viewer');
            }
            else {
                ViewTempDocument('#divPDF_Viewer', filename);
            }
        }
    </script>
    <script>
        function openElectronicSignModal() {
            var tab = '<%=this.Request.QueryString["tab"]%>';
            errors = [];
            if ($('#<%=txt_RevertComments.ClientID %>').val().trim() == "") {
                errors.push("Enter Remarks.");
            }
            if ($('#<%=hdfFileName.ClientID %>').val() == "") {
                if (tab == "1") {
                    errors.push("Upload Document.");
                }
                else if (tab == "2") {
                    if ($('#<%=hdfRUpload.ClientID %>').val() != "0") {
                        errors.push("Upload Document.");
                    }
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                if (tab == "1") {
                    document.getElementById("<%=hdnAction.ClientID%>").value = "Submit";
                    openUC_ElectronicSign();
                }
                else if (tab == "2") {
                    if ($('#<%=hdfFileName.ClientID %>').val() == "") {
                        document.getElementById("<%=hdfConfirm.ClientID%>").value = 'Submit';
                        custAlertMsg('Do you want to update without uploading document?', 'confirm', true);
                    }
                    else {
                        document.getElementById("<%=hdnAction.ClientID%>").value = "Submit";
                        openUC_ElectronicSign();
                    }
                }
            }
        }
        function reload() {
            $('#collapse1').collapse();
        }
    </script>
    <script>
        function Onkeydown() {
            var Crchange = document.getElementById("<%=hdfCrChange.ClientID%>").value;
            if (Crchange == "1") {
                var selection = cr.core.selection.getLastSelectedInterval();
                cr.commands.changeFontForeColor.execute("#3371FF");
            }
        }
    </script>
    <script>
        $(function () {
            if (/*@cc_on!@*/true) {
                var ieclass = 'ie' + document.documentMode;
                $(".popup-wrap").addClass(ieclass);
            }
            $(".sticky-popup").addClass('sticky-popup-right');
            var contwidth = $(".popup-content").outerWidth() + 2;
            $(".sticky-popup").css("right", 0);
            $(window).scroll(function () {
                var height = $(window).scrollTop();
                if (height > 50) {
                    $(".sticky-popup").css("visibility", "visible");
                } else {
                    $(".sticky-popup").css("visibility", "hidden");
                }
            });
            $(".popup-header").click(function () {
                if ($('.sticky-popup').hasClass("open")) {
                    $('.sticky-popup').removeClass("open");
                    $(".sticky-popup").css("right", "-" + contwidth + "px");
                }
                else {
                    $('.sticky-popup').addClass("open");
                    $(".sticky-popup").css("right", 0);
                }
            });
            $("#closeButton").click(function () {
                $('.sticky-popup').removeClass("open");
                $(".sticky-popup").css("right", 0);
            });
        });
        function func1() {

        }
    </script>
    <script>
        function showtxtComments() {
            $('#divtextcomments').show();
            $('#divgvcomments').hide();
        }
        function showgvComments() {
            $('#divtextcomments').hide();
            $('#divgvcomments').show();
        }
    </script>
    <script>
            $(document).ready(function () {
                function GetParameterValues(param) {
                    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                    for (var i = 0; i < url.length; i++) {
                        var urlparam = url[i].split('=');
                        if (urlparam[0] == param) {
                            return urlparam[1];
                        }
                    }
                }
                var tab = GetParameterValues("tab");
                var ProcessID1 = GetParameterValues("PID");
                var DocumentID1 = GetParameterValues("DocumentID");
                if (tab == "2") {
                    var jsPath = '<%=ResolveUrl("~/AppScripts/ReviewCommentsDiv.js")%>';
                    var security = document.createElement("script");
                    security.type = "text/javascript";
                    security.src = jsPath;
                    $("#divgvcomments").append(security);
                    getEveryoneCommentsData(ProcessID1, DocumentID1);
                }
                if (tab == "2") {
                    var isFirst = document.getElementById("<%=hdfIsFirst.ClientID%>").value;
                    //if (ProcessID1 == "1#no-back-button") {
                        if (isFirst == "0") {
                            ViewDocument1('#divPDF_Viewer', DocumentID1, '11');
                        }
                    //}
                    //else {

                    //    if (isFirst == "0") {
                    //        ViewDocument1('#divPDF_Viewer', DocumentID1, '0');
                    //    }
                    //}
                }
            });
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script>
            function getEveryoneCommentsData(ProcessID, DocumentID) {
                var PID = 0;
                if (ProcessID == "1#no-back-button")
                    PID = 1;
                if (ProcessID == "2#no-back-button")
                    PID = 2;
                if (ProcessID == "3#no-back-button")
                    PID = 3;
                //if (PID == 1) {
                    getEveryoneComments('<%= Page.ResolveUrl("~//DMS//WebServices//DMSService.asmx//GetEveryoneComments" )%>', DocumentID);
               <%-- }
                else {
                    getReviewComments('<%= Page.ResolveUrl("~//DMS//WebServices//DMSService.asmx//GetRevertedComments" )%>', DocumentID, PID);
                }--%>
            }
            function CloseBrowser() {
                window.close();
            }
            function ViewDocument(DivID) {
                PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', $('#<%=hdfViewType.ClientID%>').val(), $('#<%=hdfPKID.ClientID%>').val(), $('#<%=hdfViewEmpID.ClientID%>').val(), $('#<%=hdfViewRoleID.ClientID%>').val(), DivID, "func1");
            }
            function ViewDocument1(DivID, PKID1, RoleID1) {
                PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', '0', PKID1, '0', RoleID1, DivID, "func1");
            }
            function ViewTempDocument(DivID1, FileName1) {
                PdfFormViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_Of_File")%>', FileName1, DivID1);
            }
    </script>
    <script>
            function showImg() {
                ShowPleaseWait('show');
            }
            function hideImg() {
                ShowPleaseWait('hide');
            }
    </script>
</asp:Content>
