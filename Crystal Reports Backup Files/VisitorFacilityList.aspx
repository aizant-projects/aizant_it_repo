﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VisitorFacilityList.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VisitorFacilities.VisitorFacilityList" %>

<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>


<asp:Content ID="Content1" ContentPlaceHolderID="VMSBody" runat="server">

    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upbtnNavFacilityCreate">
        <ContentTemplate>
            <div class=" float-right">
                <asp:Button ID="btnNavFacilityCreate" runat="server" CssClass="float-right  btn-signup_popup" Text="Create" OnClick="btnNavFacilityCreate_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnPkVisitorFacilityEditID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnVisitorFacilityCustom" runat="server" Value="" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="HFDeptID" runat="server" />
    <asp:HiddenField ID="hdfUserRole" runat="server" />

    <style>
        table tbody tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            padding: 4px;
            border-right: 1px solid #fff;
        }
    </style>

    <div class="col-lg-12 col-sm-12 col-12 col-md-12  float-left padding-none">
        <div class="col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div class=" col-md-12 col-lg-12 col-sm-12 col-12 padding-none grid_panel_full float-left">
            
                
                    <div class="col-md-12 col-lg-12 col-sm-12 col-12  padding-none float-left">
                        <div class=" grid_header col-md-12 col-lg-12 col-sm-12 col-12 float-left ">
                            Visitor Facility Request
                        </div>

                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-12 grid_container top float-left padding-none">
                    <table id="tblVisitorFacility" class="tblvisitorFacility_ListClass display datatable_cust" style="width: 100%">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>S.No</th>
                                <th>Client Name</th>
                                <th>No. of Visitors</th>
                                <th>Date of Visit</th>
                                <th>Initiated By</th>
                                <th>Department</th>
                                <th>Total Estimated Cost</th>
                                <th>Total Actual Cost</th>
                                <th>Status</th>
                                <th>Edit</th>
                                <th>History</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    
    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnEditVisitorFacility" OnClick="btnEditVisitorFacility_Click" runat="server" Text="Button" />
                <asp:Button ID="btnVisitorFacilityHistory" runat="server" Text="Button" OnClick="btnVisitorFacilityHistory_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <style>
        .lbl {
            text-align: left;
            font-weight: lighter;
        }
    </style>

    <!-- Modal Popup for Visitor Facility start-->
    <div id="ModalVisitorFacility" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 80%">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <h4 class="modal-title col-sm-8">Visitor Facility Request</h4>
                    <button type="button" class="close col-sm-4" style="text-align: right;" data-dismiss="modal">&times;</button>
                </div>
                <asp:UpdatePanel runat="server" ID="upVisitorFacilityModel" UpdateMode="Always">
                    <ContentTemplate>
                        <div class="col-sm-12 top bottom float-left padding-none" style="border-bottom: 1px solid #d4d4d4;">
                            <div class="form-group col-md-6 col-sm-6 col-lg-3  col-6 float-left" runat="server" id="div5">
                                <label for="inputEmail3" class=" padding-none profile_label float-left col-xl-4 col-md-4 col-sm-6 col-lg-4  col-6" id="lblEmpName12">Initiated By  : </label>
                                <div class="col-xl-8 col-md-8 col-sm-6 col-lg-8 float-left col-6 padding-none profile_defined ">
                                    <span id="lblEmpName" runat="server"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-sm-6 col-lg-3  col-6 float-left" runat="server" id="div12">
                                <label class="padding-none profile_label col-md-4 col-sm-6 col-lg-4 col-6 float-left" id="lblDepartment12">Department  : </label>
                                <div class=" col-md-8 col-sm-6 col-6  col-lg-8 padding-none profile_defined float-left">
                                    <span id="lblDepartment" runat="server"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-sm-6 col-lg-2 col-6 float-left" runat="server" id="div1">
                                <label class="padding-none   col-md-4 col-sm-6 col-lg-8  col-6 float-left" id="lblEmpID12">Employee ID :   </label>
                                <div class="col-xl-4 col-md-8 col-sm-6 col-lg-4  col-6 padding-none profile_defined float-right">
                                    <span id="lblEmpID" runat="server"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-sm-6 col-lg-4  col-6 float-left" runat="server" id="div2">
                                <label class="padding-none  float-left col-xl-4 col-md-4 col-sm-6 col-lg-4 float-left col-6" id="lblDate12">Date of Request : </label>
                                <div class=" col-md-6 col-sm-6 col-lg-8  col-6 float-left col-6 padding-none  float-left">
                                    <span id="lblDateofRequest" runat="server"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 padding-none float-left">
                            <asp:UpdatePanel ID="upNameOfClient" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div4">
                                        <label class="control-label col-sm-10 lbl" id="lblFirstName">Name of the Visitor/Client<span class="mandatoryStar">*</span></label>
                                        <asp:Button ID="btnAddVisitorClientName" runat="server" CssClass="btnnVisit float-right" Text="+" Font-Bold="true" OnClick="btnAddVisitorClientName_Click1" />
                                        <div class="col-sm-12 ">
                                            <asp:TextBox ID="txtVisitorName" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Visitor/Client Name" Visible="false" AutoPostBack="true" OnTextChanged="txtVisitorName_TextChanged" MaxLength="100"></asp:TextBox>
                                            <asp:DropDownList ID="ddlClientName" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" TabIndex="1" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div11">
                                <label class="control-label col-sm-12 lbl" id="lblNoofVisitors">No. of Visitors<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-12">
                                    <asp:TextBox ID="txtNoofVisitors" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter No of Visitors" TabIndex="11" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;" MaxLength="3"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div14">
                                <label class="control-label col-sm-12  lbl" id="lblDateofVisit">Date of Visit<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-12">
                                    <asp:TextBox ID="txtDateofVisit" runat="server" CssClass="form-control login_input_sign_up" placeholder="Select Date"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-12 col-lg-12 col-sm-12 col-12  float-left ">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-12  padding-none  ">
                                <div class=" grid_header col-md-12 col-lg-12 col-sm-12 col-12 float-left ">
                                    Travel Arrangements
                                </div>
                                <div class="col-12 float-left padding-none grid_panel_full">
                                <div class="form-group col-lg-12 padding-none float-left" runat="server" id="div6">
                                    <label class="control-label col-sm-12 lbl" id="lblTravel">Travel Arrangements Details If Any</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtTravelArrangements" runat="server" CssClass="form-control " placeholder="Enter Travel Arrangements" TextMode="MultiLine" Rows="3" MaxLength="200"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div19">
                                    <label class="control-label col-sm-12 lbl" id="lblTrvlECost">Estimated Cost (Rs.)</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtTrvlECost" runat="server" CssClass="form-control login_input_sign_up" onkeypress="return onlyDotsAndNumbers(this,event);" MaxLength="18"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div20">
                                    <label class="control-label col-sm-12 lbl" id="lblTrvlACost">Actual Cost (Rs.)</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtTrvlACost" runat="server" CssClass="form-control login_input_sign_up" onkeypress="return onlyDotsAndNumbers(this,event);" MaxLength="18"></asp:TextBox>
                                    </div>
                                </div>
                                    </div>
                            </div>
                        </div>
                            </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-12 top float-left">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-12 grid_border padding-none bottom float-left">
                                <div class=" grid_header col-md-12 col-lg-12 col-sm-12 col-12 float-left ">
                                    Food Arrangements
                                </div>
                                <div class="col-12 float-left padding-none grid_panel_full">
                                <div class="form-group bottom top col-lg-4 padding-none float-left" runat="server" id="div8" style="border-right: 1px solid #07889a;">
                                    <label class="control-label col-sm-12 lbl" id="lblMorningRD">Morning Refreshment Details</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtMorningRef" runat="server" CssClass="form-control " placeholder="Enter Morning Refreshment Details" TextMode="MultiLine" Rows="3" MaxLength="200"></asp:TextBox>
                                    </div>
                                    <div class=" col-lg-6 padding-none bottom form-group float-left" runat="server" id="div13">
                                        <label class="control-label col-sm-12 lbl" id="lblMorningECost">Estimated Cost (Rs.)</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtMorningECost" runat="server" CssClass="form-control login_input_sign_up" onkeypress="return onlyDotsAndNumbers(this,event);" MaxLength="18"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="  col-lg-6 padding-none bottom form-group float-left" runat="server" id="div15">
                                        <label class="control-label col-sm-12 lbl" id="lblMorningACost">Actual Cost (Rs.)</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtMorningACost" runat="server" CssClass="form-control login_input_sign_up" onkeypress="return onlyDotsAndNumbers(this,event);" MaxLength="18"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group bottom top float-left col-lg-4 padding-none" runat="server" id="div9" style="border-right: 1px solid #07889a;">
                                    <label class="control-label col-sm-12 lbl" id="lblAfterNoon">AfterNoon Lunch Details</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtAfterLD" runat="server" CssClass="form-control " placeholder="Enter AfterNoon Lunch Details" TextMode="MultiLine" Rows="3" MaxLength="200"></asp:TextBox>
                                    </div>
                                    <div class="form-group col-lg-6 padding-none bottom float-left" runat="server" id="div16">
                                        <label class="control-label col-sm-12 lbl" id="lblAfterECost">Estimated Cost (Rs.)</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtAfterECost" runat="server" CssClass="form-control login_input_sign_up" onkeypress="return onlyDotsAndNumbers(this,event);" MaxLength="18"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6 padding-none bottom float-left" runat="server" id="div17">
                                        <label class="control-label col-sm-12 lbl" id="lblAfterACost">Actual Cost (Rs.)</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtAfterACost" runat="server" CssClass="form-control login_input_sign_up" onkeypress="return onlyDotsAndNumbers(this,event);" MaxLength="18"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="form-group bottom top float-left col-lg-4 padding-none" runat="server" id="div10">
                                    <label class="control-label col-sm-12 lbl" id="lblEvening">Evening Refreshment Details</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtEveningRef" runat="server" CssClass="form-control " placeholder="Enter Evening Refreshment Details" TextMode="MultiLine" Rows="3" MaxLength="200"></asp:TextBox>
                                    </div>
                                    <div class="form-group col-lg-6 padding-none bottom float-left" runat="server" id="div18">
                                        <label class="control-label col-sm-12 lbl" id="lblEveningECost">Estimated Cost (Rs.)</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtEveningECost" runat="server" CssClass="form-control login_input_sign_up" onkeypress="return onlyDotsAndNumbers(this,event);" MaxLength="18"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6 col-sm-6 col-6 padding-none float-left bottom" runat="server" id="div21">
                                        <label class="control-label col-sm-12 lbl" id="lblEveningACost">Actual Cost (Rs.)</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtEveningACost" runat="server" CssClass="form-control login_input_sign_up" onkeypress="return onlyDotsAndNumbers(this,event);" MaxLength="18"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                 </div>
                            <div class="col-12 float-left padding-none">
                            <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div7">
                                <label class="control-label col-sm-12 lbl padding-none" id="lblRemarks">Remarks</label>
                                <div class="col-sm-12 padding-none">
                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control " placeholder="Enter Remarks" TextMode="MultiLine" Rows="3" MaxLength="200"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div_FacilitiesComments">
                                <label class="control-label col-sm-12 lbl " id="lblComments">Comments<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-12" style="padding-right:0px;">
                                    <asp:TextBox ID="txtFacilitycomments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="3" MaxLength="200"></asp:TextBox>
                                </div>
                            </div>
                                </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>


                <div class="modal-footer " style="border: none;">
                    <div class="col-sm-12  top text-right padding-none">
                        <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Button ID="btnSubmit" Text="Submit" CssClass=" btn-signup_popup" runat="server" OnClientClick="javascript:return Submitvalidate();"  Visible="false" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnReset" Text="Reset" CssClass=" btn-revert_popup" runat="server" CausesValidation="false" OnClick="btnReset_Click" Visible="false" />
                                <asp:Button ID="btnUpdate" Text="Update" CssClass=" btn-signup_popup"  runat="server" OnClientClick="javascript:return UpdateValidate();" Visible="false" />
                                <asp:Button ID="btnApprove" Text="Approve" CssClass=" btn-signup_popup" runat="server" OnClick="btnApprove_Click" Visible="false" />
                                <asp:Button ID="btnRevert" Text="Revert" CssClass=" btn-revert_popup" runat="server" OnClick="btnRevert_Click" Visible="false" />
                                <asp:Button ID="btnReject" Text="Reject" CssClass=" btn-revert_popup" runat="server" OnClick="btnReject_Click" Visible="false" />
                                <input type="button" value="Cancel" class=" btn-cancel_popup" data-dismiss="modal" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Popup for Visitor Facility end-->

    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="min-width:44%;">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            
                            <span id="span1" runat="server">Comment History</span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover table-responsive" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="118px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_EmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="88px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_RoleName" runat="server" Text='<%# Eval("RoleName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action Date" ItemStyle-Width="158px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ActionDate" runat="server" Text='<%# Eval("ActionDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="218px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Actions" runat="server" Text='<%# Eval("Actions") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Comments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                       
                    </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------Comment History View End-------------->

    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />

    <script>
        function ConfirmAlertSubmit() {
            custAlertMsg('Do you want to submit ?', 'confirm', true);
            document.getElementById("<%=hdnVisitorFacilityCustom.ClientID%>").value = "Submit";
        }
        function ConfirmAlertUpdate() {
            custAlertMsg('Do you want to update ?', 'confirm', true);
            document.getElementById("<%=hdnVisitorFacilityCustom.ClientID%>").value = "Update";
        }
    </script>


    <!--Validation Visitor Facilities-->
    <script type="text/javascript">
        function Submitvalidate() {
             var NoofVisitors = document.getElementById("<%=txtNoofVisitors.ClientID%>").value;
             var DateofVisit = document.getElementById("<%=txtDateofVisit.ClientID%>").value;
             errors = [];
             var ddlClientName = $('#<%=ddlClientName.ClientID%>');
             var isVisible5 = ddlClientName.is(':visible');
             if (isVisible5 == true) {
                 var ClientName1 = document.getElementById("<%=ddlClientName.ClientID%>").value;
                 if (ClientName1 == 0) {
                     errors.push("Please select client name.");
                }
            }
             var txtClientName = $('#<%=txtVisitorName.ClientID%>');
             var isVisible6 = txtClientName.is(':visible');
            if (isVisible6 == true) {
                var ClientName2 = document.getElementById("<%=txtVisitorName.ClientID%>").value;
                if (ClientName2.trim() == "") {
                    errors.push("Please enter client name.");
                    }
                }
            
             if (NoofVisitors.trim() == "" || NoofVisitors.trim() == 0) {
                 errors.push("Please enter no of visitors.");
             }
             if (DateofVisit.trim() == "") {
                 errors.push("Please select date of visit.");
             }
             if (errors.length > 0) {
                 custAlertMsg(errors.join("<br/>"), "error");
                 return false;
             }
             ConfirmAlertSubmit();
        }
    </script>

    <script type="text/javascript">
         function UpdateValidate() {
             var ClientName = document.getElementById("<%=ddlClientName.ClientID%>").value;
             var NoofVisitors = document.getElementById("<%=txtNoofVisitors.ClientID%>").value;
            var DateofVisit = document.getElementById("<%=txtDateofVisit.ClientID%>").value;
            var Comments = document.getElementById("<%=txtFacilitycomments.ClientID%>").value;
            errors = [];
            if (ClientName == 0) {
                errors.push("Please select client name.");
            }
            if (NoofVisitors.trim() == "" || NoofVisitors.trim() == 0) {
                errors.push("Please enter no of visitors.");
            }
            if (DateofVisit.trim() == "") {
                errors.push("Please select date of visit.");
            }
            if (Comments.trim() == "") {
                errors.push("Please enter modification reason in comments.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            ConfirmAlertUpdate();
         }
    </script>

    <script>
         $("#NavLnkVFacilities").attr("class", "active");
         $("#VisitorFacilities").attr("class", "active");
    </script>

    <script>
          $(document).ready(function () {
              //Binding Code
              $(function () {
                  $('#<%=txtDateofVisit.ClientID%>').datetimepicker({
                      format: 'DD MMM YYYY',
                 
                  });
              });
              Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
              function EndRequestHandler(sender, args) {
                  //Binding Code Again
                  $(function () {
                      $('#<%=txtDateofVisit.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        
                    });
                });
            }
          });
    </script>

    <!--Visitor Facilities Request Jquery Datatable-->
    <script>
          $.ajaxSetup({
              cache: false
          });

          var oTable = $('#tblVisitorFacility').DataTable({
              columns: [

                  { 'data': 'FacilityRequestID' },
                  { 'data': 'RowNumber' },
                  { 'data': 'ClientName' },
                  { 'data': 'NoofVisitors' },
                  { 'data': 'DateofVisit' },
                  { 'data': 'Initiatedby' },
                  { 'data': 'DeptName' },
                  { 'data': 'TotalEstimatedCost' },
                  { 'data': 'TotalActualCost' },
                  { 'data': 'ApproveStatus' },
                  {
                      "mData": null,
                      "bSortable": false,
                      "mRender": function (o) { return '<a class="Edit" title="Edit"  data-target="#ModalVisitorFacility" data-toggle="modal" onclick="ViewVisitorFacilities_List(' + o.FacilityRequestID + ');"></a>'; }
                  },
                  {
                      "mData": null,
                      "bSortable": false,
                      "mRender": function (o) { return '<a  class="summary_latest" title="History" data-target="#myCommentHistory" data-toggle="modal" onclick="GetVisitorFacilities_History(' + o.FacilityRequestID + ');"></a>'; }
                  }
              ],
              "pagingType": "simple_numbers",
              "language": {
                  "infoFiltered": ""
              },
              "processing": true,
              "orderClasses": false,
              "order": [[1, "desc"]],
             
              "info": true,

              "scrollCollapse": true,
              "aoColumnDefs": [{ "targets": [0,1], "visible": false }, { "className": "dt-body-left", "targets": [2, 5, 6] }, { "className": "dt-body-right", "targets": [3, 7, 8] }],
             
              "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
              "sAjaxSource": "<%=ResolveUrl("~/VMS/WebService/VMS_Service.asmx/GetVisitorFacilitiesRequestList")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
               
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#tblVisitorFacility").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        $(".tblvisitorFacility_ListClass").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
            
        });

        function Reloadtable(isupdate) {
                       
            $('#ModalVisitorFacility').modal('hide');
            var role = document.getElementById("<%=hdfUserRole.ClientID%>").value;
                    if (isupdate == 0)
                        window.open("<%=ResolveUrl("~/VMS/VisitorFacilities/VisitorFacilityList.aspx")%>", "_self");
                    if (isupdate == 1)
                        window.open("<%=ResolveUrl("~/VMS/VisitorFacilities/VisitorFacilityList.aspx?c=")%>" + role, "_self");
        }

       
    </script>
    <!--End Visitor Facilities Request Jquery Datatable-->

    <script>
                function ViewVisitorFacilities_List(FacilityRequestID) {
                    $("#<%=hdnPkVisitorFacilityEditID.ClientID%>").val(FacilityRequestID);
                    $("#<%=btnEditVisitorFacility.ClientID%>").click();
                }
                function GetVisitorFacilities_History(FacilityRequestID) {
                    $("#<%=hdnPkVisitorFacilityEditID.ClientID%>").val(FacilityRequestID);
                    $("#<%=btnVisitorFacilityHistory.ClientID%>").click();
                }

    </script>

    <script>
                function OpenModelVisitorFacility() {
                    document.getElementById("<%=txtNoofVisitors.ClientID%>").value = '';
            document.getElementById("<%=txtDateofVisit.ClientID%>").value = '';
            document.getElementById("<%=txtTravelArrangements.ClientID%>").value = '';
            document.getElementById("<%=txtTrvlECost.ClientID%>").value = '';
            document.getElementById("<%=txtTrvlACost.ClientID%>").value = '';
            document.getElementById("<%=txtRemarks.ClientID%>").value = '';
            document.getElementById("<%=txtMorningRef.ClientID%>").value = '';
            document.getElementById("<%=txtAfterLD.ClientID%>").value = '';
            document.getElementById("<%=txtEveningRef.ClientID%>").value = '';
            document.getElementById("<%=txtMorningECost.ClientID%>").value = '';
            document.getElementById("<%=txtMorningACost.ClientID%>").value = '';
            document.getElementById("<%=txtAfterECost.ClientID%>").value = '';
            document.getElementById("<%=txtAfterACost.ClientID%>").value = '';
            document.getElementById("<%=txtEveningECost.ClientID%>").value = '';
            document.getElementById("<%=txtEveningACost.ClientID%>").value = '';
            $('#ModalVisitorFacility').modal('show');
                }
    </script>

    <!--Visitor Facility Request Edit Popup-->
    <script>     
        function OpenModalVisitorFacilityEdit() {
            $('#<%=btnSubmit.ClientID %>').hide();
            $('#<%=btnReset.ClientID %>').hide();
            $('#ModalVisitorFacility').modal('show');
        }
    </script>

</asp:Content>






