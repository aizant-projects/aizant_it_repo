﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VisitorReportPage.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.Reports.VMSReportPage" %>

<%@ Register Assembly="DevExpress.XtraReports.v18.1.Web.WebForms, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:Content ID="cnt1" ContentPlaceHolderID="VMSBody" runat="server">
       <script src="<%=ResolveUrl("~/DevexScripts/jquery-3.2.1.min.js")%>"></script>
        <link href="<%=ResolveUrl("~/DevexScripts/jquery-ui.css")%>" rel="stylesheet" type="text/css"/>
        <script src="<%=ResolveUrl("~/DevexScripts/jquery-ui.js")%>"></script>
        <script src="<%=ResolveUrl("~/DevexScripts/cldr.js")%>"></script>
        <script src="<%=ResolveUrl("~/DevexScripts/cldr/event.js")%>"></script>
        <script src="<%=ResolveUrl("~/DevexScripts/cldr/supplemental.js")%>"></script>
        <script src="<%=ResolveUrl("~/DevexScripts/globalize.js")%>"></script>
        <script src="<%=ResolveUrl("~/DevexScripts/globalize/currency.js")%>"></script>
        <script src="<%=ResolveUrl("~/DevexScripts/globalize/date.js")%>"></script>
        <script src="<%=ResolveUrl("~/DevexScripts/globalize/message.js")%>"></script>
        <script src="<%=ResolveUrl("~/DevexScripts/globalize/number.js")%>"></script>
        <script src="<%=ResolveUrl("~/DevexScripts/knockout.js")%>"></script>
        <script src="<%=ResolveUrl("~/DevexScripts/ace.js")%>"></script>
        <script src="<%=ResolveUrl("~/DevexScripts/ext-language_tools.js")%>"></script>
      <%--  <script src="<%=ResolveUrl("~/DevexScripts/bootstrap.min.js")%>"></script>    --%>        
        <script src="<%=ResolveUrl("~/DevexScripts/moment.min.js")%>"></script>
        <script src="<%=ResolveUrl("~/DevexScripts/bootstrap-datetimepicker.min.js")%>"></script>       
             <%-- <script src="<%=ResolveUrl("~/DevexScripts/bootstrap-select.min.js")%>"></script>--%>
     
            <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none grid_panel_full float-left">
                <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none float-left" >
                    <div class=" grid_header col-md-12 col-lg-12 col-sm-12 col-12 float-left  ">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-6 ">Visitor Report</div>
                    </div>
                   
                     <div class="col-lg-12 col-sm-12 col-12 col-md-12 top padding-none float-left" >
                            <div class="col-sm-3 padding-none float-left">
                                <div class="form-group" >
                                    <label class="control-label col-sm-12 lbl" id="lblReportType">Report Type</label>
                                    <div class="col-sm-12 float-left">
                                        <asp:DropDownList ID="ddlReportType" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged">
                                            <asp:ListItem Value="1">Daily</asp:ListItem>
                                            <asp:ListItem Value="2">Monthly</asp:ListItem>
                                            <asp:ListItem Value="3">Yearly</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                     
                            <div class="col-sm-3  float-left" id="div_MonthYear" runat="server" visible="false">
                                <div class="form-group" runat="server">
                                    <label class="control-label col-sm-12 lbl" id="lblyear">Enter Year<span class="mandatoryStar">*</span></label>
                                    <div class="col-lg-12 col-sm-12 col-12 col-md-12">
                                        <asp:TextBox ID="txtMonthYear" CssClass="form-control login_input_sign_up" runat="server" placeholder="Enter Year"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 padding-none float-left">
                                <div class="col-12 form-group float-left" runat="server" id="div_FromDate" visible="false">
                                    <label class="control-label col-sm-12 lbl" id="lblFromDate">From Date<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-12">                                     
                                       <asp:TextBox ID="txtSearchVisitorsFromDate"  CssClass="form-control login_input_sign_up" runat="server" placeholder="Select From Date" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-12 form-group float-left" runat="server" id="div_FromMonths" visible="false">
                                    <label class="control-label col-sm-12 lbl" id="lblFromMonths">From Months<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-12 float-left">
                                        <asp:DropDownList ID="ddlfromMonths" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddlfromMonths_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-12 form-group float-left" runat="server" id="div_FromYear" visible="false">
                                    <label class="control-label col-sm-12 lbl" id="lblFromYear">From Year<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtFromYear" CssClass="form-control login_input_sign_up" runat="server" placeholder="Enter Year" MaxLength="4" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 padding-none bottom float-left">
                                <div class="col-12 form-group float-left" runat="server" id="div_ToDate" visible="false">
                                    <label class="control-label col-sm-12  lbl" id="lblToDate">To Date<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtSearchVisitorsToDate" runat="server" CssClass="form-control login_input_sign_up" placeholder="Select To Date" autocomplete="off"></asp:TextBox>
                                      
                                    </div>
                                </div>
                                <div class="col-12 form-group float-left" runat="server" id="div_ToMonth" visible="false">
                                    <label class="control-label col-sm-12  lbl" id="lblToMonths">To Month<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-12 float-left">
                                        <asp:DropDownList ID="ddlToMonth" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-12 form-group float-left" runat="server" id="div_ToYear" visible="false">
                                    <label class="control-label col-sm-12  lbl" id="lblToYears">To year<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-12">
                                         <asp:TextBox ID="txtToYear" CssClass="form-control login_input_sign_up" runat="server" placeholder="Enter Year" MaxLength="4" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                    
                   
                </div>
                  
                     </div>
                 <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none float-left" >
                       <div class="col-sm-3 padding-none float-left ">
                                <div class="form-group" >
                                    <label class="control-label col-sm-12 lbl" id="">Purpose of Visit</label>
                                    <div class="col-sm-12">
                                        <asp:DropDownList ID="ddlPurposeofVisit" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" runat="server" >                                          
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                      <div class="col-sm-3  bottom float-left">
                                <div class="form-group float-left" runat="server" id="div1" >
                                    <label class="control-label col-sm-12  lbl" >Department</label>
                                    <div class="col-sm-12 float-left">
                                        <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Department" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>                              
                            </div>
                      <div class="col-sm-3 bottom float-left">
                                <div class="form-group float-left" runat="server" id="div2" >
                                    <label class="control-label col-sm-12  lbl" >Organization</label>
                                    <div class="col-sm-12 float-left">
                                        <asp:TextBox ID="txtOrganization" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Organization" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                              
                            </div>
                      <div class="col-lg-3 text-right top float-left" style="margin-top: 20px;">                      
                                   <asp:LinkButton ID="btnFind1" runat="server" CssClass=" btn-signup_popup  " OnClick="btnFind1_Click">Submit</asp:LinkButton>                            
                                <asp:Button ID="btnReset" class=" btn-revert_popup  " Text="Reset" runat="server" OnClick="btnReset_Click" />
                             
                    </div>
                     </div>
             <div class="form-group col-lg-12 float-left">
                 <dx:ASPxWebDocumentViewer ID="ASPxWebDocumentViewer1" runat="server" OnCacheReportDocument="ASPxDocumentViewer1_CacheReportDocument" OnRestoreReportDocumentFromCache="ASPxDocumentViewer1_RestoreReportDocumentFromCache">
                     <ClientSideEvents Init="function(s, e) {
                                s.previewModel.reportPreview.zoom(1);
                            }" />
                 </dx:ASPxWebDocumentViewer>
        </div>
            </div>
        
   
    <!----End To Load Calendar for Appointment Date---->

     <!----To Load Calendar ---->

    <script>
        $(document).ready(function () {
            //Binding Code
            myfunction();
            dashb();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                //Binding Code Again
                myfunction();
                dashb();
            }
        });

    </script>
    <!----End To Load Calendar for Appointment Date---->
  <script>
      function myfunction() {
          //for start date and end date validations startdate should be < enddate
          $(function () {
              $('#<%=txtSearchVisitorsFromDate.ClientID%>').datetimepicker({
                    format: 'DD MMM YYYY',
                    useCurrent: false,
                });

                $('#<%=txtSearchVisitorsToDate.ClientID%>').datetimepicker({
                    format: 'DD MMM YYYY',
                    useCurrent: false,
                });
                $('#<%=txtSearchVisitorsFromDate.ClientID%>').on("dp.change", function (e) {
                  $('#<%=txtSearchVisitorsToDate.ClientID%>').data("DateTimePicker").minDate(e.date);
                  var _mindate = new Date(e.date);
                  _mindate.setDate(_mindate.getDate() + 6)
                  $('#<%=txtSearchVisitorsToDate.ClientID%>').val(_mindate.format('dd MMM yyyy'));
                        $('#<%=txtSearchVisitorsToDate.ClientID%>').data("DateTimePicker").maxDate(moment().add(7, 'days'));
              });
          });
      }
    </script>

    
    
    <script>
        function Clearfromtodate() {
            document.getElementById('<%=txtSearchVisitorsFromDate.ClientID%>').value = '';
            document.getElementById('<%=txtSearchVisitorsToDate.ClientID%>').value = '';
        }
    </script>

    <script type="text/javascript">

        window.onload = function () {
            getDate();
        };

        function getDate() {
            try {
                var m_names = new Array("Jan", "Feb", "Mar",
                    "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                    "Oct", "Nov", "Dec");
                var dtTO = new Date();
                var curr_date = dtTO.getDate();
                var curr_month = dtTO.getMonth();
                var curr_year = dtTO.getFullYear();
                var dtFrom = new Date(dtTO.getTime() - (6 * 24 * 60 * 60 * 1000));
                var Todate = document.getElementById("<%=txtSearchVisitorsToDate.ClientID%>");
            Todate.value = curr_date + " " + m_names[curr_month] + " " + curr_year;

            var Fromdate = document.getElementById("<%=txtSearchVisitorsFromDate.ClientID%>");
            var curr_date1 = dtFrom.getDate();
            var curr_month1 = dtFrom.getMonth();
            var curr_year1 = dtFrom.getFullYear();
            Fromdate.value = curr_date1 + " " + m_names[curr_month1] + " " + curr_year1;
            }
            catch{ }
        }
    </script>
   <script>
        $(function () {
            $('.selectpicker1').selectpicker();
        });
    </script>
</asp:Content>
