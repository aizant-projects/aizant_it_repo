﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucExamination.ascx.cs" Inherits="AizantIT_PharmaApp.UserControls.TMS_UserControls.Exam.ucExamination" %>
<%@ Register Src="~/UserControls/TMS_UserControls/Exam/ucExamResultSheet.ascx" TagPrefix="uc1" TagName="ucExamResultSheet" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <asp:HiddenField ID="hdnBaseType" runat="server" Value="0" />
        <asp:HiddenField ID="hdnDocID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnBaseID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnMarksGained" runat="server" Value="0" />
        <asp:HiddenField ID="hdnExamAttempts" runat="server" Value="1" />
        <asp:HiddenField ID="hdnIsAskFeedback" runat="server" Value="N" />
        <asp:HiddenField ID="hdnTraineeID" runat="server" Value="0" />
    </ContentTemplate>
</asp:UpdatePanel>

 <!--ML Enable and Disable script starts -->
   <script src='<%=ResolveUrl("~/AppScripts/ML_FeaturesChecking.js")%>'></script>
        <script>
            GetMLFeatureChecking(3);//Passing Paramenter Module ID
        </script>
    <!--ML Enable and Disable script ends-->

<!-- Modal popup for exam paper-->
<div class="modal fade department" id="modelUcExamination" role="dialog" style="z-index: 1040 !important;">
    <div class="modal-dialog modal-lg" style="width: 90%; height: 600px">
        <div class="modal-content">
            <div class="modal-header tmsModalHeader">
                <h4 class="modal-title Panel_Title" style="color: aliceblue">Examination</h4>
                <button type="button" class="close " data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body tmsModalBody">
                <div class="tmsModalContent col-12 float-left padding-none" style="overflow: auto; padding: 0px;">
                    <asp:UpdatePanel ID="upExamQuestionnaire" runat="server" style="width: 100%" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="gvExamQuestionnaire" OnRowDataBound="gvExamQuestionnaire_RowDataBound" OnPageIndexChanging="gvExamQuestionnaire_PageIndexChanging"
                                DataKeyNames="QuestionID" runat="server" AutoGenerateColumns="False" CssClass="table table-striped AspGrid col-12 float-left question_list padding-none"
                                EmptyDataText="No Questionnaire's" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle col-12 padding-none" ShowHeader="false"
                                PageSize="1" AllowPaging="true">
                                <PagerSettings Mode="NextPreviousFirstLast" PageButtonCount="4" FirstPageText="First" PreviousPageText="Previous" NextPageText="Next" LastPageText="Last" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Question ID" SortExpression="QueID" Visible="false">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <table class="table col-12 float-left">
                                                <tr>
                                                    <td style="color: #fff; background: #38b8ba; width: 100%; font-size: 12pt; font-weight: bold;">
                                                        <asp:Label ID="lblGVQuestion" runat="server" Text='<%# Bind("QuestionTitle") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="QuestionOptions">
                                                        <asp:RadioButtonList ID="rblOptions" Font-Bold="false" AutoPostBack="true" runat="server" OnSelectedIndexChanged="rblOptions_SelectedIndexChanged"></asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Left" CssClass="GridPager col-12  float-left" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="modal-footer tmsModalContent">
                <div class="row" style="text-align: center">
                    <asp:UpdatePanel ID="upSubmitExamClick" runat="server" style="margin: 10px" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Button ID="btnSubmitExam" runat="server" CssClass="btn btn-signup_popup" Text="Submit Exam" title="Click To Submit the Exam" OnClientClick="ShowPleaseWait('show');" OnClick="btnSubmitExam_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Exam Passed-->

<div id="modalUcExamPass" class="modal department" role="dialog">
    <div class=" modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
            <div class="modal-header tmsModalHeader">
                <h4 class="modal-title ">You have been Qualified !</h4>
            </div>
            <div class="modal-body" style="padding: 0px">
                <div class="success_icon col-lg-12 col-sm-12 col-xs-12 col-md-12">
                    <img src="<%=ResolveUrl("~/Images/TMS_Images/ExamPass.gif")%>" alt="Smiley face" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnInfoOk" class="btn-signup_popup" data-dismiss="modal" runat="server" onclick="OpenDocExamResultModal();">Ok</button>
            </div>
        </div>
    </div>
</div>
<!-- Exam Fail-->

<div id="modalUcExamFail" class="modal" role="dialog">
    <div class=" modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
            <div class="modal-header tmsModalHeader">
                <h4 class="modal-title">Not Qualified !</h4>
            </div>
            <div class="modal-body" style="padding: 0px">
                <div class="padding-none" style="padding: 10px">
                    <div class="success_icon col-lg-12 col-sm-12 col-xs-12 col-md-12">
                        <img src="<%=ResolveUrl("~/Images/TMS_Images/ExamFail.gif")%>" alt="Smiley face" style="margin-left: 1pc;" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="Button1" class="btn-signup_popup" data-dismiss="modal" runat="server">Ok</button>
            </div>
        </div>
    </div>
</div>

<uc1:ucExamResultSheet runat="server" ID="ucExamResultSheet" />

<div id="divServerBtn" style="display: none">
    <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnViewExamResult" runat="server" Text="Button" OnClick="btnViewExamResult_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<script>
    function OpenSopExamModel() {
        // CloseReadDocModelAndCloseDocDetailsModel();
        $('#modelUcExamination').modal({ show: true, backdrop: 'static', keyboard: false });
    }
    function OpenTTSCloseValidation() {
        custAlertMsg('You are not able to perform this action due to Target Training schedule has been closed', 'error', true);
    }
    <%--function CloseExamModelAndOpenCongratulationsPopup() {
        ShowPleaseWait('hide');
        if ($("#<%=hdnBaseType.ClientID%>").val() == "1") {
               ReloadRegularDocDataTable();
               UpdateJrTrainingStatus();
           }
           $('#modelUcExamination').modal('hide');
           $('#modalUcExamPass').modal({ backdrop: 'static', keyboard: false });
    }--%>
   <%-- function CloseExamModelAndOpenFailurePopup() {
        ShowPleaseWait('hide');
        if ($("#<%=hdnBaseType.ClientID%>").val() == "1") {
        }
        $('#modelUcExamination').modal('hide');
        $('#modalUcExamFail').modal({ backdrop: 'static', keyboard: false });
    }--%>
    function OpenDocExamResultModal() {

        $("#<%=btnViewExamResult.ClientID%>").click();
    }
    function showNoQuestionsmessage() {
        custAlertMsg('Currently there is no Questionnaire on this Document to take exam, Contact Trainer', 'info', true);
    }
    function messageinCustomPopups(msgDescription, msgType) {
        custAlertMsg(msgDescription, msgType, true);
        ShowPleaseWait('hide');
    }

    <%--$('#modelUcExamination').on('shown.bs.modal', function () {
        if ($("#<%=hdnBaseType.ClientID%>").val() == "1") {
            $('.modal-backdrop').remove();
            $('body').append('<div class="modal-backdrop show modal-stack" style="z-index: 1029;"></div>');
            $('#modelUcExamination').css('z-index', '1030');         
        }
    });  

    $('#modelUcExamination').on('hidden.bs.modal', function () {
        if ($("#<%=hdnBaseType.ClientID%>").val() == "1") {
            $('.modal-backdrop').remove();
        }
     });  --%>

    function CloseExamModel(typeOfMsg) {
        ShowPleaseWait('hide');
        switch (typeOfMsg) {
            case 'Fail':
                $('#modelUcExamination').modal('hide');
                $('#modalUcExamFail').modal({ backdrop: 'static', keyboard: false });
                //ML feature Checking
                if (ML_FeatureIDs.includes(7) == true) {
                    $.get(AGA_ML_URL + "/Emp", { "Empid": $("#<%=hdnTraineeID.ClientID%>").val() });
                }
                break;
            case 'Success':
                if ($("#<%=hdnBaseType.ClientID%>").val() == "1") {
                    ReloadRegularDocDataTable();
                    UpdateJrTrainingStatus();
                }
                //ML feature Checking
                if (ML_FeatureIDs.includes(7) == true) {
                    $.get(AGA_ML_URL + "/Emp", { "Empid": $("#<%=hdnTraineeID.ClientID%>").val() });
                }
                $('#modelUcExamination').modal('hide');
                $('#modalUcExamPass').modal({ backdrop: 'static', keyboard: false });
                break;
            case 'Error':
                custAlertMsg('Not Allowed to Re-Attempt Examination on this Document, You are Already Qualified.', 'error', true);
                $('#modelUcExamination').modal('hide');
                break;
            default:
        }
    }
</script>
