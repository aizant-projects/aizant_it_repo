﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.master" AutoEventWireup="true" CodeBehind="UMSDashBoardPage.aspx.cs" Inherits="AizantIT_PharmaApp.UMSDashBoardPage" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="<%=ResolveUrl("~/AppCSS/dashboardstyle.css")%>" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class=" col-xs-3 ">
        <div class="closebtn"><span>&#9776;</span></div>
        <div style="display: none;" class="closebtn1">
            <span>&#9776;</span>

        </div>
    </div>
     <div class=" col-xs-12 ">
        <div class=" col-xs-12  outer_border_profile <%--outer_border--%>">
            <div class=" col-xs-12 padding-none">

                 

          <div class="col-xs-3 pull-left padding-right_div fade-in one" id="Div2" runat="server">
                <div class="col-xs-12 padding-none  outer_dashboard pull-left">
                    <div class="hidden-md hidden-xs hidden-sm" style="position: absolute; right: 10px; opacity: 0.1; top: 20px;">
                        <img src="<%=ResolveUrl("~/Images/UMSDashBoard/user_big.png")%>" class="" alt="Approve" />
                    </div>
                    <div class="col-xs-12 header_text_dshboard">Total Employees</div>
                    <div class="col-xs-12 padding-none">
                        <div class="col-xs-6 padding-none hidden-md hidden-xs hidden-sm">
                            <img src="<%=ResolveUrl("~/Images/UMSDashBoard/user_bsmall.png")%>" class="dashboard-image" alt="Approve" />
                        </div>
                        <div class="col-lg-6  col-xs-12">
                            <span class="link_dashboard">
                                <a href="UserManagement/EmployeeList.aspx?L=T">
                                <asp:Label ID="lblTotalUSers" CssClass="link_dashboard" runat="server" Text="0"></asp:Label></a></span>
                        </div>
                    
                    </div>
                </div>
            </div>      




             <div class="col-xs-3 pull-left padding-right_div fade-in one" id="div1" runat="server">
                <div class="col-xs-12 padding-none outer_dashboard1 pull-left">
                    <div class="hidden-md hidden-xs hidden-sm" style="position: absolute; right: 10px; opacity: 0.1; top: 20px;">
                        <img src="<%=ResolveUrl("~/Images/UMSDashBoard/locked_big.png")%>" class="" alt="Approve" />
                    </div>
                    <div class="col-xs-12 header_text_dshboard">Locked Employees</div>
                    <div class="col-xs-12 padding-none">
                        <div class="col-xs-6 padding-none hidden-md hidden-xs hidden-sm">
                            <img src="<%=ResolveUrl("~/Images/UMSDashBoard/locked_small.png")%>" class="dashboard-image" alt="Approve" />
                        </div>
                        <div class="col-lg-6  col-xs-12 ">
                            <span class="link_dashboard">
                                <a href="UserManagement/EmployeeList.aspx?L=Y">
                                <asp:Label ID="lblLockedUsers" CssClass="link_dashboard" runat="server" Text="0"></asp:Label></a></span>
                        </div>
                     
                    </div>
                </div>
            </div>




              <div class="col-xs-3 pull-left padding-right_div fade-in one" id="dvEfective" runat="server">
                <div class="col-xs-12 padding-none  outer_dashboard3  pull-left">
                    <div class="hidden-md hidden-xs hidden-sm" style="position: absolute; right: 10px; opacity: 0.1; top: 20px;">
                        <img src="<%=ResolveUrl("~/Images/UMSDashBoard/depart_big.png")%>" class="" alt="Approve" />
                    </div>
                    <div class="col-xs-12 header_text_dshboard">Total Departments</div>
                    <div class="col-xs-12 padding-none">
                        <div class="col-xs-6 padding-none hidden-md hidden-xs hidden-sm">
                            <img src="<%=ResolveUrl("~/Images/UMSDashBoard/depart_small.png")%>" class="dashboard-image" alt="Approve" />
                        </div>
                        <div class="col-lg-6  col-xs-12 ">
                            <span class="link_dashboard">
                                <a href="UserManagement/Department/DepartmentList.aspx?L=T">
                                <asp:Label ID="lblTotalDepartment" CssClass="link_dashboard" runat="server" Text="0"></asp:Label></a></span>
                        </div>
                   
                    </div>
                </div>
            </div>



                     
                <div class="col-xs-3 pull-left padding-right_div fade-in one" id="divCreatorRevert" runat="server" >
                <div class="col-xs-12 padding-none  outer_dashboard2  pull-left">
                    <div class="hidden-md hidden-xs hidden-sm" style="position: absolute; right: 10px; top: 20px;opacity:0.1">
                        <img src="<%=ResolveUrl("~/Images/UMSDashBoard/no_roles_big.png")%>" class="" alt="Approve" />
                    </div>
                    <div class="col-xs-12 header_text_dshboard">Employee with No Role</div>
                    <div class="col-xs-12 padding-none">
                        <div class="col-xs-6 hidden-md hidden-xs hidden-sm padding-none ">
                            <img src="<%=ResolveUrl("~/Images/UMSDashBoard/no_roles_small.png")%>" class="dashboard-image" alt="Approve" />
                        </div>
                        <div class="col-lg-6  col-xs-12 ">
                            <span class="link_dashboard">
                                 <a href="UserManagement/EmployeeList.aspx?A=U">
                                <asp:Label ID="lblUnAssignRoles" CssClass="link_dashboard" runat="server" Text="0"></asp:Label></a></span>
                        </div>
                   
                    </div>
                </div>
            </div>

             
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <div class="col-sm-12 padding-none dashboard_panel_full">

             
                 <div class="col-xs-12" style="text-align:left; padding:10px; border-bottom:1px solid #07889a; color:#07889a; background:#cde8f6;  font-weight:bold;">Total No of Employees</div>

             <%--   <div style="text-align:center; font-weight:bold;">Total No of Employees</div>--%>

                <div class="col-xs-12 pull-left dashboard_ums_chart" >
                    <asp:Chart ID="Chart1" runat="server"  Height="400px" Width="901px" >
                        <Series>
                            <asp:Series Name="Series1" ChartType="Column" IsValueShownAsLabel="true"  LabelAngle="-90" Font="Microsoft Sans Serif, 12pt"  ChartArea="ChartArea1"  LabelToolTip="#VALX #VAL" CustomProperties="DrawSideBySide=False" ToolTip="#VALX #VAL" YValuesPerPoint="1">
                            </asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1">
                                <AxisX Title="DepartmentNames" TitleFont="Microsoft Sans Serif, 10pt,style=Bold" TextOrientation="Auto"  IsLabelAutoFit="False" Interval="1">
                                    <LabelStyle Angle="-45" Font=" 5pt"  />
                                </AxisX>
                                <AxisY Title="No of Employees" TitleFont="Microsoft Sans Serif, 10pt,style=Bold"  >
                                    <LabelStyle Angle="-45" Font="Microsoft Sans Serif, 5pt, style=Bold" />
                                </AxisY>
                            </asp:ChartArea>
                        </ChartAreas>
                      <%--  <Legends>
                            <asp:Legend Docking="Bottom" Name="LegendClass"></asp:Legend>
                        </Legends>--%>
                    </asp:Chart>
                </div>
              
            </div>
             <div class="col-xs-12 padding-none dashboard_panel_full">
                 <div class="col-xs-12" style="text-align:left; padding:10px; border-bottom:1px solid #07889a; color:#07889a; background:#cde8f6;  font-weight:bold;">Employees by Role</div>
              <div class="form-group padding-none col-xs-12 top pull-left">
                    <div class="col-xs-6">
                        <label class="padding-none col-xs-6">Select Module</label>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                               
                               <asp:DropDownList ID="Moduelddl" CssClass="form-control SearchDropDown" AutoPostBack="true" Width="100%" title="Click to Select Department" OnSelectedIndexChanged="Moduleddl_SelectedIndexChanged" runat="server"></asp:DropDownList>
                               
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="col-xs-6">
                        <label class="padding-none col-xs-6">Select Roles</label>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                               
                             <asp:DropDownList ID="ddlRoles" CssClass="form-control SearchDropDown" AutoPostBack="true" Width="100%" title="Click to Select Department" OnSelectedIndexChanged="ddlRoles_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            <div class="col-xs-12 dashboard_ums_chart">
                <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                        <asp:Chart ID="Chart2" runat="server" Height="400px" Width="901px">
                            <Series>
                                <asp:Series Name="Series2" ChartType="Column" IsValueShownAsLabel="true" IsVisibleInLegend="true" LabelToolTip="#VALX #VAL" CustomProperties="DrawSideBySide=False" ToolTip="#VALX #VAL" YValuesPerPoint="2">
                                </asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea2">
                                    <AxisX Title="DepartmentNames" TitleFont="Microsoft Sans Serif, 10pt, style=Bold" IsLabelAutoFit="false" Interval="1">
                                        <LabelStyle Angle="-45" Font="Microsoft Sans Serif, 10pt, style=Bold" />
                                    </AxisX>
                                    <AxisY Title="No of Employees" TitleFont="Microsoft Sans Serif, 10pt, style=Bold" interval="Auto" >
                                        <LabelStyle Angle="-45" Font="Microsoft Sans Serif, 10pt, style=Bold"  />
                                    </AxisY>
                                </asp:ChartArea>
                            </ChartAreas>
                            <%--    <Titles>
                            <asp:Title Font="Microsoft Sans Serif, 14.25pt, style=Bold, Underline" Name="Title1" Text="No Of Department List">
                            </asp:Title>
                        </Titles>--%>
                          <%--  <Legends>
                                <asp:Legend Docking="Bottom" Name="LegendClass"></asp:Legend>
                            </Legends>--%>
                        </asp:Chart>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

        </div>
        </div>

    </div>
     </div>
</asp:Content>
