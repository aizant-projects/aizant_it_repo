﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="Effective_Documents.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentApprover.Effective_Documents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        iframe {
            overflow-y: hidden;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div id="divEffective" class=" col-lg-12 padding-none float-left">
        <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 dms_outer_border float-left padding-none">
            <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div id="divMainContainer" runat="server" visible="true">
                <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 padding-none float-right ">
                    <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 float-left">Documents To Be Effective</div>
                        <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                            <div class="col-12 float-left padding-none top">
                            <table id="dtDates" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Version ID</th>
                                        <th>Document Number</th>
                                        <th>Document Name</th>
                                        <th>Version</th>
                                        <th>Document Type</th>
                                        <th>Department</th>
                                        <th>Effective Date</th>
                                        <th>Review Date</th>
                                        <th>vDocument ID</th>
                                        <th>Training</th>
                                        <th>Edit</th>
                                        <th>PendingDocRequestCount</th>
                                        <th>RequestType</th>
                                        <th>View Doc</th>
                                    </tr>
                                </thead>
                            </table>
                                </div>
                            <div class=" col-lg-12 float-left bottom">
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/edit_red.png")%>" />
                                <b class="grid_icon_legend">Pending SOP's for Target Training/Pending Document Distribution Request's</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------Add Dates View-------------->
    <div id="myModal_lock" class="modal department fade " role="dialog"  data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog ">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    
                    <span id="span_FileTitle" runat="server">Effective Date & Review Date</span>
                    <button type="button" class="close" data-dismiss="modal" style="width: 50px;">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="col-lg-12 padding-none float-left">
                                <div class="col-lg-12 padding-none float-left">
                                    <asp:Label ID="Label1" runat="server" Style="color: #07889a" Text="Effective Date"></asp:Label><span class="smallred_label">*</span>
                                </div>
                                <div class="col-lg-12 padding-none float-left" id="datetimepicker1">
                                    <asp:TextBox ID="txt_EffectiveDate" runat="server" AutoPostBack="true" CssClass="form-control DatePicker login_input_sign_up" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-12 padding-none top float-left">
                                <div class="col-lg-12 padding-none float-left">
                                    <asp:Label ID="lblreview" Style="color: #07889a" runat="server" Text=" Review Date"></asp:Label><span class="smallred_label">*</span>
                                </div>
                                <div class="col-lg-12 padding-none float-left" id="datetimepicker2">
                                    <asp:TextBox ID="txt_expirationdate" runat="server" AutoPostBack="true" CssClass="form-control DatePicker login_input_sign_up" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group padding-none top float-left">
                                <asp:Label runat="server" Text="Comments" CssClass="label-style" Font-Bold="true"></asp:Label><span class="smallred_label">*</span>
                            </div>
                            <div style="padding-bottom: 5px;">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <textarea ID="txt_Reject" Text='<%# Eval("Comments") %>' runat="server" class="form-control" MaxLength="300"></textarea>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer text-right">
                    <asp:Button ID="btn_Submit" runat="server" OnClick="btn_Submit_Click" OnClientClick="return RequiredDate();" Text="Submit" CssClass="float-right  btn-signup_popup top" />
                    <button type="button" data-dismiss="modal" runat="server" id="btnclose1" class="float-right top btn-cancel_popup">Cancel</button>
                    </div>
            </div>
        </div>
    </div>
    <!--------End Add Dates VIEW-------------->

    <!-------- File VIEW-------------->
    <div id="myModal_ViewDoc" class="modal department fade" role="dialog" tabindex="-1"  data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog modal-lg" style="min-width: 90%; height: 700px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    
                    <asp:UpdatePanel ID="UpHeading" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <span id="span1" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpViewDL" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divPDF_Viewer">
                                <noscript>
                                    Please Enable JavaScript.
                                </noscript>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!-------- End File VIEW-------------->

    <asp:HiddenField ID="hdfPKID" runat="server" />
    <asp:HiddenField ID="hdfDocID" runat="server" />
    <asp:HiddenField ID="hdfStatus" runat="server" />
    <asp:HiddenField ID="hdfDocNum" runat="server" />
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRequestType" runat="server" Value="0" />
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <asp:HiddenField ID="hdfViewDivID" runat="server" />
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Button ID="btnDatesEdit" runat="server" type="button" Text="Submit" Style="display: none" OnClick="btnDatesEdit_Click" OnClientClick="showImg();"/>
            <asp:Button ID="btnDatesEdit1" runat="server" type="button" Text="Submit" Style="display: none" OnClick="btnDatesEdit1_Click" OnClientClick="showImg();"/>
            <asp:Button ID="btnFileView" runat="server" Text="Submit" OnClick="btnFileView_Click" Style="display: none" OnClientClick="showImg();"/>
        </ContentTemplate>
    </asp:UpdatePanel>
     <!--Ml Feature Checking-->
    <script src='<%=ResolveUrl("~/AppScripts/ML_FeaturesChecking.js")%>'></script>
    <script>
        GetMLFeatureChecking(2);//Passing Paramenter Module ID
    </script>
    <script>
        $('#dtDates thead tr').clone(true).appendTo('#dtDates thead');
        $('#dtDates thead tr:eq(1) th').each(function (i) {
            if (i < 9) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDates').wrap('<div class="dataTables_scroll" />');
        var table = $('#dtDates').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'VersionID' },
                { 'data': 'DocumentNumber' },
                { 'data': 'DocumentName' },
                { 'data': 'VersionNumber' },
                { 'data': 'DocumentType' },
                { 'data': 'Department' },
                { 'data': 'EffectiveDate' },
                { 'data': 'ExpirationDate' },
                { 'data': 'vDocumentID' },
                { 'data': 'IsTraining' },
                { 'data': 'TrainingCount' },
                { 'data': 'PendingDocRequestCount' },
                { 'data': 'RequestTypeID' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="view" title="View Doc" data-toggle="modal" data-target="#myModal_ViewDoc" onclick=ViewDocument(' + o.VersionID + ')></a>'; }
                }
            ],
            "responsive": true,
            "paging": true,
            "search": {
                "regex": true
            },
            "aoColumnDefs": [{ "targets": [0,1, 9, 10, 12,13], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 1, 11] }, { "className": "dt-body-left", "targets": [2, 3, 5, 6] },
            {
                targets: [11], render: function (a, b, data, d) {
                    var DocType = data.DocumentType.toUpperCase();
                    if ((data.TrainingCount == false && data.IsTraining == true) && (data.PendingDocRequestCount != 0)) {
                        return '<a class="edit_Training" href="#" title="No Target Training Completed/Pending Document Distribution Requests" onclick=EditDocument1(' + data.VersionID + ',' + data.vDocumentID + ')></a>';
                    }
                    else if (data.PendingDocRequestCount != 0) {
                        return '<a class="edit_Training" href="#" title="Pending Document Distribution Requests" onclick=EditDocument1(' + data.VersionID + ',' + data.vDocumentID + ')></a>';
                    }
                    else if (data.TrainingCount == false && data.IsTraining == true && data.RequestTypeID == 1) {
                        return '<a class="edit_Training" href="#" title="Not able to effective New document" onclick=EditDocument1(' + data.VersionID + ',' + data.vDocumentID + ')></a>';
                    }
                    else if (data.TrainingCount == false && data.IsTraining == true && data.RequestTypeID == 2) {
                        return '<a class="edit_Training" href="#" title="Not able to effective Revision Doc" onclick=EditDocument1(' + data.VersionID + ',' + data.vDocumentID + ')></a>';
                    }
                    else {
                        return '<a class="Edit" data-toggle="modal" data-target="#myModal_lock" title="Edit" onclick=EditDocument(' + data.VersionID + ',' + data.vDocumentID + ')></a>';
                    }
                    return "N/A";
                }
            },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetDates" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "ReqTypeID", "value": <%=hdnRequestType.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDates").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    </script>
    <script>
        function EditDocument(PK_ID, DOC_ID) {
            document.getElementById("<%=txt_EffectiveDate.ClientID%>").value = '';
            document.getElementById("<%=txt_expirationdate.ClientID%>").value = '';
            document.getElementById("<%=hdfPKID.ClientID%>").value = PK_ID;
            document.getElementById("<%=hdfDocID.ClientID%>").value = DOC_ID;
            var btnFV = document.getElementById("<%=btnDatesEdit.ClientID%>");
            btnFV.click();
        }
        function EditDocument1(PK_ID, DOC_ID) {
            document.getElementById("<%=txt_EffectiveDate.ClientID%>").value = '';
            document.getElementById("<%=txt_expirationdate.ClientID%>").value = '';
            document.getElementById("<%=hdfPKID.ClientID%>").value = PK_ID;
            document.getElementById("<%=hdfDocID.ClientID%>").value = DOC_ID;
            var btnFV1 = document.getElementById("<%=btnDatesEdit1.ClientID%>");
            btnFV1.click();
        }
        function ViewDocument(V_ID) {
            document.getElementById("<%=hdfPKID.ClientID%>").value = V_ID;
            var btnFV2 = document.getElementById("<%=btnFileView.ClientID %>");
            btnFV2.click();
        }
    </script>
    <script>       
        $('#<%=txt_EffectiveDate.ClientID%>').datetimepicker({
            format: 'DD MMM YYYY',
            minDate: new Date('<%=DateTime.Now.Date.ToString("dd MMM yyyy")%>'),
            useCurrent: false,
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('#<%=txt_EffectiveDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                minDate: new Date('<%=DateTime.Now.Date.ToString("dd MMM yyyy")%>'),
                useCurrent: false,
            });
        });
    </script>
    <script>
        $('#<%=txt_expirationdate.ClientID%>').datetimepicker({
            format: 'DD MMM YYYY',
            useCurrent: false,
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('#<%=txt_expirationdate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
        });
    </script>
    <script>
        $(function () {
            $('#<%=txt_EffectiveDate.ClientID%>').on("dp.change", function (e) {
                $('#<%=txt_expirationdate.ClientID%>').data("DateTimePicker").minDate(e.date);
                var _mindate = new Date(e.date);
                _mindate.setDate(_mindate.getDate() - 1)// subtract one day
                _mindate.setFullYear(_mindate.getFullYear() + 2) //to add years
                $('#<%=txt_expirationdate.ClientID%>').val(_mindate.format('dd MMM yyyy'))
            });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('#<%=txt_EffectiveDate.ClientID%>').on("dp.change", function (e) {
                if (e.date != false) {
                    $('#<%=txt_expirationdate.ClientID%>').data("DateTimePicker").minDate(e.date);
                    var _mindate = new Date(e.date);
                    _mindate.setDate(_mindate.getDate() - 1)// subtract one day
                    _mindate.setFullYear(_mindate.getFullYear() + 2) //to add years
                    $('#<%=txt_expirationdate.ClientID%>').val(_mindate.format('dd MMM yyyy'))
                }
                else {
                    $('#<%=txt_expirationdate.ClientID%>').val("");
                }
            });
        });

    </script>
    <script>
        $(function () {
            $('#<%=txt_expirationdate.ClientID%>').on("dp.change", function (e) {
                var mindate = document.getElementById("<%=txt_EffectiveDate.ClientID%>").value;
                $('#<%=txt_expirationdate.ClientID%>').data("DateTimePicker").minDate(mindate);
                var _mindate = new Date(mindate);
                _mindate.setDate(_mindate.getDate() - 1)// subtract one day
                _mindate.setFullYear(_mindate.getFullYear() + 2) //to add years
            });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('#<%=txt_expirationdate.ClientID%>').on("dp.change", function (e) {
                var mindate = document.getElementById("<%=txt_EffectiveDate.ClientID%>").value;
                $('#<%=txt_expirationdate.ClientID%>').data("DateTimePicker").minDate(mindate);
                var _mindate = new Date(mindate);
                _mindate.setDate(_mindate.getDate() - 1)// subtract one day
                _mindate.setFullYear(_mindate.getFullYear() + 2) //to add years
            });
        });
    </script>
    <script type="text/javascript">
        function RequiredDate() {
            var EffectiveDate = document.getElementById("<%=txt_EffectiveDate.ClientID%>").value;
            var ReviewDate = document.getElementById("<%=txt_expirationdate.ClientID%>").value;
            var Purpose = document.getElementById("<%=txt_Reject.ClientID%>").value;
            var DateCount = 0;
            errors = [];
            if (EffectiveDate.trim() == "") {
                errors.push("Select Effective Date.");
                DateCount = DateCount + 1;
            }
            if (ReviewDate.trim() == "") {
                errors.push("Select Review Date.");
                DateCount = DateCount + 1;
            }
            if (DateCount == 0) {
                if (Date.parse(EffectiveDate) == Date.parse(ReviewDate)) {
                    errors.push("Effective Date and Review Date must not be same.");
                }
                else if (Date.parse(EffectiveDate) > Date.parse(ReviewDate)) {
                    errors.push("Effective Date should not be greater than Review Date.");
                }
            }
            if (Purpose.trim() == "") {
                errors.push("Enter Comments.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script>           
        function ResetDate() {
            document.getElementById("<%=txt_EffectiveDate.ClientID%>").value = "";
            document.getElementById("<%=txt_expirationdate.ClientID%>").value = "";
        }
        function ClosePopUp() {
            var clickButton = document.getElementById("<%=btnclose1.ClientID %>");
            clickButton.click();
        }
        function ReloadEffetive() {
            window.open("<%=ResolveUrl("~/DMS/DocumentApprover/Effective_Documents.aspx")%>", "_self");
        }
        function ReloadEffectiveWithML() {
            window.open("<%=ResolveUrl("~/DMS/DocumentApprover/Effective_Documents.aspx")%>", "_self");
            //Start DMS_ML
            if (ML_FeatureIDs.indexOf(6) != -1) {
                var DOCVID = $('#<%=hdfPKID.ClientID%>').val();
                $.get(AGA_ML_URL + "/Version", { "id": DOCVID });
            }
            //End DMS_ML
        }
        function CloseBrowser() {
            window.close();
        }
        function ViewPDFDocument(DivID) {
            PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', $('#<%=hdfViewType.ClientID%>').val(), $('#<%=hdfPKID.ClientID%>').val(), $('#<%=hdfViewEmpID.ClientID%>').val(), $('#<%=hdfViewRoleID.ClientID%>').val(), DivID);
         }
    </script>
    <script>
        function showImg() {
            ShowPleaseWait('show');
        }
        function hideImg() {
            ShowPleaseWait('hide');
        }
    </script>
</asp:Content>
