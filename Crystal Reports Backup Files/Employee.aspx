﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.Master" AutoEventWireup="true" CodeBehind="Employee.aspx.cs" Inherits="AizantIT_PharmaApp.UserManagement.WebForm2" %>

<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .hidden {
            display: none; 
        } 
        /*.cusorNotAllowed {
             cursor:not-allowed!important;
        }*/
         .cusorNotAllowed * {
             cursor:not-allowed!important;
        }
        
    </style>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <script type="text/javascript">
       function showimagepreview(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.readAsDataURL(input.files[0]);
                filerdr.onload = function (e) {
                    var image = new Image();
                    //Set the Base64 string return from FileReader as source.
                    image.src = e.target.result;
                    //Validate the File Height and Width.
                    image.onload = function () {
                        var height = this.height;
                        var width = this.width;
                       // var SizeofImage = 0;
                        errors = [];
                        if (height < 130 || width < 95)
                        {
                            errors.push("Image Height and Width must not be less than 130px and 95px.");
                        }
                        //if (e.target.result.length >= 1024)
                        //{
                        //  SizeofImage = (e.target.result.length / 1024).toFixed(2);
                        //}
                        //if (parseInt(SizeofImage) > 300)
                        //{
                        //    errors.push("Image Should  be less than 300 KB of Size.");
                        //}
                        if (errors.length > 0)
                        {
                            custAlertMsg(errors.join("<br/>"), "error");
                            document.getElementById("<%=FileUpload1.ClientID %>").value = "";
                            return false;
                        }
                        else
                        {
                            if (width > height)
                            {
                                errors.push("Image width shouldn't be more than the Height of the Image");
                                document.getElementById("<%=FileUpload1.ClientID %>").value = "";
                            }
                            if (errors.length > 0)
                            {
                                custAlertMsg(errors.join("<br/>"), "error");
                                return false;
                            }
                            else {                                 
                                $('#<%=imgEmpImage.ClientID%>').attr('src', e.target.result);
                                return true;
                            }
                        }
                    }

                }
            }
        }

    </script>

    <asp:UpdatePanel ID="uphidden" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnImage" runat="server" />
            <asp:HiddenField ID="hdnDeleteRow" runat="server" />
            <asp:HiddenField ID="hfTab" runat="server" />
            <asp:HiddenField ID="HFDOB" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="col-lg-3 padding-none float-left" style="margin-top: 4px;">
        <div class="closebtn"><span>&#9776;</span></div>
        <div style="display: none;" class="closebtn1">
            <span>&#9776;</span>
        </div>
    </div>
    <div style="margin-top: 4px">
        <a class="float-right btn btn-signup_popup" href="EmployeeList.aspx">Employee List</a>
    </div>

    <div class=" col-lg-12 padding-none float-left">

        <div class=" col-lg-12 padding-none  float-left">
            <div class="col-lg-12 padding-none  float-left">


             
                <div class="col-12 grid_scrollbar padding-none  float-left">
                    <div id="header">
                        <div class="col-sm-12 dashboard_panel_header " id="txtTitle">
                            Create Employee 
                        </div>
                    </div>
                    
                    <div class="col-lg-10 padding-none label_panel  float-left top">
                        <div class="col-sm-12 padding-none  float-left">
                            <div class="form-group col-sm-4  float-left">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_ums">Employee Code<span class="smallred_label">*</span></label>
                                <div class="col-sm-12 padding-none">
                                    <input type="text" class="form-control login_input_sign_up" id="txtEmpCode" placeholder="" runat="server" tabindex="1" maxlength="20">
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group col-sm-4  float-left">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer  ">First Name<span class="smallred_label">*</span></label>
                                <div class="col-sm-12 padding-none">
                                    <input type="text" class="form-control login_input_sign_up ViewMode" data-error="Bruh, that email address is invalid" id="txtEmpFirstName" placeholder="" runat="server" tabindex="2" onkeypress="return ValidateAlpha(event);" maxlength="50" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group col-sm-4  float-left">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Last Name<span class="smallred_label">*</span></label>
                                <div class="col-sm-12 padding-none">
                                    <input type="text" class="form-control login_input_sign_up ViewMode" id="txtEmpLastName" placeholder="" runat="server" tabindex="3" onkeypress="return ValidateAlpha(event);" maxlength="50" autocomplete="off">
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12 padding-none  float-left">

                            <div class="form-group col-sm-4  float-left">

                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Date Of Birth<span class="smallred_label">*</span></label>
                               
                                <div class="col-sm-12 padding-none">
                                     <asp:TextBox class="form-control login_input_sign_up ViewMode" ID="txtEmpDob" placeholder="" runat="server" TabIndex="4" OnTextChanged="txtEmpDob_TextChanged" AutoPostBack="true" AutoCompleteType="Disabled" autocomplete="off"></asp:TextBox>
                                </div>
                               
                            </div>

                            <div class="form-group col-sm-4  float-left">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Blood Group</label>
                                <div class="col-sm-12 padding-none ">
                                    <asp:UpdatePanel ID="upBloodGroup" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlBloodGroup" CssClass="form-control selectpicker regulatory_dropdown_style ViewMode" data-live-search="true" runat="server" TabIndex="5" ToolTip="-- Select Blood Group --">
                                                <asp:ListItem Text="-- Select Blood Group --" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="A+" Value="1">A+</asp:ListItem>
                                                <asp:ListItem Text="A-" Value="2">A-</asp:ListItem>
                                                <asp:ListItem Text="AB+" Value="3">AB+</asp:ListItem>
                                                <asp:ListItem Text="AB-" Value="4">AB-</asp:ListItem>
                                                <asp:ListItem Text="B+" Value="5">B+</asp:ListItem>
                                                <asp:ListItem Text="B-" Value="6">B-</asp:ListItem>
                                                <asp:ListItem Text="O+" Value="7">O+</asp:ListItem>
                                                <asp:ListItem Text="O-" Value="8">O-</asp:ListItem>
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlBloodGroup" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="btn-group form-group col-sm-12 col-12 col-lg-4 col-md-12  float-left ViewMode" data-toggle="buttons" runat="server" id="divgenderStatus">
                                <div class="col-12 float-left padding-none">
                                    <label for="inputEmail3" class=" padding-none col-12 col-lg-12 col-md-12 col-sm-12 control-label custom_label_answer ">Gender<span class="smallred_label">*</span></label>

                                    <asp:Label class="btn btn-primary_radio active col-sm-6 col-6 col-lg-6 col-md-6 active float-left radio_check" ID="lblrbMale" runat="server">
                                        <asp:RadioButton ID="rbMale" runat="server" TabIndex="6" GroupName="rbGender" onkeydown="return RestrictEnterKey(event);" />
                                        Male
                                    </asp:Label>
                                    <asp:Label class="btn btn-primary_radio  col-sm-6 col-6 col-lg-6 col-md-6 float-left radio_check" ID="lblrbFemale" runat="server">
                                        <asp:RadioButton ID="rbbtnFemale" runat="server" TabIndex="7" GroupName="rbGender" onkeydown="return RestrictEnterKey(event);" />
                                        Female
                                    </asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 padding-none  float-left">
                            <div class="form-group col-sm-4  float-left">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Email ID<span class="smallred_label">*</span></label>
                                <div class="col-sm-12 padding-none">
                                    <input type="text" class="form-control login_input_sign_up ViewMode" id="txtEmpEmailID" placeholder="" runat="server" tabindex="8" onkeypress="return nospaces(event);" onkeydown="return RestrictEnterKey(event);" maxlength="200" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group col-sm-4  float-left">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Mobile Number<span class="smallred_label">*</span> </label>
                                <div class="col-sm-12 padding-none">
                                    <asp:TextBox class="form-control login_input_sign_up ViewMode" ID="txtEmpMobileNo" placeholder="" runat="server" TabIndex="9" onkeypress="return ValidatePhoneNo(event);" onpaste="return false" title="Mobile No." MaxLength="10" autocomplete="off" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-sm-4  float-left">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Alternate Number</label>
                                <div class="col-sm-12 padding-none">
                                    <asp:TextBox ID="txtEmpTelNo" runat="server" class="form-control login_input_sign_up ViewMode" placeholder="" TabIndex="10" onkeypress="return ValidatePhoneNo(event);" MaxLength="10" autocomplete="off" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 padding-none  float-left">
                            <div class="form-group col-sm-4  float-left">
                                <label for="ContentPlaceHolder1_ContentPlaceHolder1_txtEmpEmgNo" class=" padding-none col-sm-12 control-label custom_label_answer">Emergency Number</label>
                                <div class="col-sm-12 padding-none">
                                    <asp:TextBox class="form-control login_input_sign_up ViewMode" name="txtEmpEmgNo" ID="txtEmpEmgNo" placeholder="" runat="server" TabIndex="11" onkeypress="return ValidatePhoneNo(event);" MaxLength="10" autocomplete="off" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="upcheck" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="form-group col-sm-4  float-left">
                                        <label for="inputEmail3" class=" padding-none col-sm-9 control-label custom_label_answer float-left">Login ID<span class="smallred_label">*</span></label>
                                        <asp:UpdatePanel ID="sdsd" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <div class="check_status text-right">

                                                    <asp:Button ID="btnLogincheck" CssClass="check_button" runat="server" Text="Check!" OnClick="btnLogincheck_Click" TabIndex="13" ToolTip="Login ID Availability" />

                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div class="col-sm-12 padding-none  float-left">
                                            <input type="text" class="form-control login_input_sign_up" id="txtEmpLoginID" placeholder="" runat="server" data-toggle="tooltip" data-placement="bottom" title="LoginID Should be unique and minimum of 4 characters" onkeypress="return ValidateAlphaNumeric(event)" onpaste="return false" maxlength="10" tabindex="12" autocomplete="off">
                                            <asp:Label ID="lblLoginCheck" runat="server" CssClass="TestBoxUnderDescription"></asp:Label>
                                        </div>

                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="form-group col-sm-4  float-left">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Start Date<span class="smallred_label">*</span></label>
                                <div class="col-sm-12 padding-none">
                                    <input type="text" class="form-control login_input_sign_up ViewMode" id="txtStartDate" placeholder="" runat="server" tabindex="14" onkeypress="return ValidateAlphaNumeric(evt);" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 padding-none  float-left">
                            <div class="form-group col-sm-4  float-left">
                                <label for="inputEmail3" class=" padding-none col-sm-10 control-label custom_label_answer float-left">Designation<span class="smallred_label">*</span></label>
                                <a href="#" title="Add" data-toggle="modal" data-target="#myModalDesignation" class="add_depart1">
                                    <div class="add_depart col-sm-1 float-right" id="add_depart1"></div>
                                </a>
                               
                                <div class="col-sm-12 padding-none  float-left">
                                    <asp:UpdatePanel ID="UPddldesignation1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlDesignation" CssClass="form-control col-md-12 col-lg-12 col-12 col-sm-12 drop_down selectpicker regulatory_dropdown_style ViewMode" data-size="9" data-live-search="true" runat="server" Width="100%" TabIndex="15">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="form-group col-sm-4  float-left">
                             
                                <label for="inputEmail3" class=" padding-none col-sm-10 control-label custom_label_answer float-left">Department<span class="smallred_label">*</span></label>
                                <a href="#" title="Add" data-toggle="modal" data-target="#myModalDepartment" class="add_depart1">
                                    <div class="add_depart  col-sm-1 float-right"></div>
                                </a>
                                <div class="col-sm-12 padding-none  float-left">
                                    <asp:UpdatePanel ID="UPddlDepartments1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlDepartments" CssClass="form-control SearchDropDown selectpicker regulatory_dropdown_style ViewMode" data-size="9" data-live-search="true" runat="server" Width="100%" TabIndex="16" title="Click to Select Department">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlDepartments" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="form-group col-sm-4  float-left">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">End Date</label>
                                <div class="col-sm-12 padding-none">
                                    <input type="text" class="form-control login_input_sign_up ViewMode" id="txtEndDate" runat="server" tabindex="17" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-12  padding-none  float-left">
                            <asp:UpdatePanel ID="UPCountries" runat="server">
                                <ContentTemplate>
                                    <div class="form-group col-sm-4  float-left">
                                        <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Country<span class="smallred_label">*</span></label>
                                        <div class="col-sm-12 padding-none">
                                            <asp:DropDownList ID="ddlCountry" CssClass="form-control SearchDropDown selectpicker regulatory_dropdown_style ViewMode" data-size="9" data-live-search="true" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" Width="100%" TabIndex="18">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlCountry" EventName="SelectedIndexChanged" />
                                </Triggers>

                            </asp:UpdatePanel>
                            <div class="form-group col-sm-4  float-left">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">State<span class="smallred_label">*</span></label>
                                <div class="col-sm-12 padding-none">
                                    <asp:UpdatePanel ID="upstates" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlState" CssClass="form-control SearchDropDown selectpicker regulatory_dropdown_style ViewMode" data-size="9" data-live-search="true" runat="server" Width="100%" TabIndex="19" ToolTip="select State" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlState" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                </div>
                            </div>
                             <div class="form-group col-sm-4  padding-none  float-left ">
                            <div class="form-group col-sm-12  float-left">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">City<span class="smallred_label">*</span></label>
                                <div class="col-sm-12 padding-none">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlCity" CssClass="form-control SearchDropDown selectpicker regulatory_dropdown_style ViewMode" data-size="9" data-live-search="true" runat="server" Width="100%" TabIndex="20" ToolTip="select State">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlCity" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                            </div>
                              <div class="form-group col-sm-12  float-left">
                                <label for="inputEmail3" class=" padding-none col-sm-10 control-label custom_label_answer float-left">Address<span class="smallred_label">*</span></label>

                                <div class="col-sm-12 padding-none">

                                    <textarea type="text" class="form-control ViewMode" id="txtEmpAddress" placeholder="" runat="server" tabindex="21" rows="0" maxlength="500"></textarea>
                                </div>
                            </div>
                            <div class="btn-group form-group col-sm-12 col-12 col-lg-6 col-md-12 float-left top ViewMode" data-toggle="buttons" runat="server" id="divrblockstatus">
                                <div class="col-12 float-left padding-none">
                                    <label for="inputEmail3" class=" padding-none col-12 col-lg-12 col-md-12 col-sm-12 control-label custom_label_answer">Access Status</label>
                                    <label class="btn btn-primary_radio  col-6 col-lg-6 col-md-6 col-sm-6 active  float-left radio_check" runat="server" id="lblrblocked">
                                        <asp:RadioButton ID="rbbtnLocked" runat="server" GroupName="rbLocked" TabIndex="22" onkeydown="return RestrictEnterKey(event);" />
                                        Locked
                                    </label>
                                    <label class="btn btn-primary_radio  col-6 col-lg-6 col-md-6 col-sm-6  float-left radio_check" runat="server" id="lblrbunlocked">
                                        <asp:RadioButton ID="rbbtnUnlocked" runat="server" GroupName="rbLocked" TabIndex="23" onkeydown="return RestrictEnterKey(event);" />
                                        Unlocked
                                    </label>
                                </div>
                            </div>
                            <div class="btn-group  form-group col-lg-6 col-12  float-left top ViewMode" data-toggle="buttons" runat="server" id="divrbActiveStatus">
                                <div class="col-12 float-left padding-none">
                                    <label for="options" class=" padding-none col-12 col-lg-12 col-md-12 col-sm-12 control-label custom_label_answer">Employee Status</label>
                                    <asp:Label class="btn btn-primary_radio  col-6 col-lg-6 col-md-6 col-sm-6 active  float-left radio_check" ID="lblrbActive" runat="server">
                                        <asp:RadioButton name="options" ID="rbbtnActive" runat="server" GroupName="rbActive" TabIndex="24" onkeydown="return RestrictEnterKey(event);"></asp:RadioButton>
                                        Active
                                    </asp:Label>
                                    <asp:Label class="btn btn-primary_radio col-6 col-lg-6 col-md-6 col-sm-6  float-left radio_check" ID="lblrbInActive" runat="server">
                                        <asp:RadioButton name="options" ID="rbbtnInActive" runat="server" GroupName="rbActive" TabIndex="25" onkeydown="return RestrictEnterKey(event);"></asp:RadioButton>
                                        In Active
                                    </asp:Label>
                                </div>
                            </div>
                        
                       
                    </div>

                    <div class="col-lg-2 col-12 col-sm-12 col-md-12  float-left top">

                        <div class="profile_bg col-lg-12 col-12 col-sm-12 col-md-12">

                            <asp:Image ID="imgEmpImage" runat="server" class="img-fluid" align="center" Width="200px" Height="200px" ToolTip="Employee Image" /><%--<img src="../Images/UserLogin/profile_bg.png"  style="margin-top: 8px;"alt="Smiley face"/>--%>
                        </div>
                        <div class="file upload-button btn  btn-primary col-sm-12 col-12 col-lg-12 col-md-12">Upload  Photo<asp:FileUpload class="upload-button_input ViewMode" ID="FileUpload1" runat="server" onchange="showimagepreview(this)" accept=".png,.jpg,.jpeg,.gif" ToolTip="Click to Upload Photo" /></div>

                        <div>
                            <a href="#" title="Reset Password" data-toggle="modal" data-target="#ChangePasswordModal">
                                <input type="button" id="btnResetPwd" class="btn-revert_popup ViewMode" data-target="#ChangePasswordModal" runat="server" visible="false" style="width: 100%; margin-top: 20px; margin-left: 0px" value="Reset Password" /></a>
                        </div>
                    </div>


                    <div class="col-lg-10 padding-none label_panel  float-left" id="divDoc" runat="server">
                        <div id="divFileUpload">
                        <div class="form-group col-sm-4  float-left">
                            <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Document Type</label>
                            <div class="col-sm-12 padding-none">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlDocumenType" CssClass="form-control selectpicker regulatory_dropdown_style ViewMode" data-live-search="true" runat="server" ToolTip="-- Select DocumentType Group --" AutoPostBack="true" OnSelectedIndexChanged="ddlDocumenType_SelectedIndexChanged" TabIndex="26">
                                            <asp:ListItem Text="-- Select Document Type --" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Resume" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Academic certificate" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Other Document" Value="3"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlDocumenType" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <%--  <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div class="modal">
                                            <div class="center">
                                                <img alt="" src="~/Images/loader.gif" />
                                            </div>
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>--%>
                        <asp:UpdatePanel ID="upSelectDoc" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-group col-sm-4  float-left">
                                    <div class="file upload-button btn  btn-primary col-sm-12 col-12 col-lg-12 col-md-12">
                                        <input type="file" name="file" class="col-12 padding-none" id="FileUploadDoc" runat="server" tabindex="27" />
                                    </div>
                                </div>
                                <div class="form-group col-sm-2  float-left ">
                                    <asp:Button ID="btnUpload" Class="file upload-button btn btn-secondary col-sm-12 col-12 col-lg-12 col-md-12" runat="server" Text="Upload" OnClick="UploadFile" OnClientClick="return ValidatEmployeeCode();" TabIndex="28" />
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnUpload" />

                            </Triggers>
                        </asp:UpdatePanel>
                        </div>
                        <br />
                        <div class="col-lg-12  float-left">
                            <asp:UpdatePanel ID="upgv1" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvDocumentUpload" runat="server" CssClass="table doc_grid table-frdered " HeaderStyle-BackColor="#07889a" HeaderStyle-ForeColor="White" AutoGenerateColumns="false" EmptyDataText="No files uploaded">
                                        <Columns>
                                            <asp:BoundField DataField="ID" HeaderText="S.No" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                           
                                            <asp:BoundField DataField="DocID" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden">
                                                <ItemStyle CssClass="hidden" />
                                                <HeaderStyle CssClass="hidden" />
                                            </asp:BoundField>
                                           
                                            <asp:TemplateField Visible="false" HeaderText="File Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocTypeID1" Text='<%#Eval("Bytes")%>' runat="server" Font-Bold="true"></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Document" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFileName" Text='<%#Eval("FileName")%>' runat="server" Font-Bold="true"></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="File Name" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocTypeID" Text='<%#Eval("DocTypeID")%>' runat="server" Font-Bold="true"></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="View" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel ID="upgv" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:LinkButton ID="lnkDownload" Text="" CommandArgument='<%# Eval("DocID") %>' Font-Size="Large" runat="server" title="View" OnClick="DownloadFile" Font-Bold="true"><img src="../Images/TMS_Images/DocSearch.png"  style="width:25px;height:25px"/></asp:LinkButton>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <%--<asp:AsyncPostBackTrigger ControlID="lnkDownload" EventName="click" />--%>
                                                            <asp:PostBackTrigger ControlID="lnkDownload" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkDeleterow" Text="" CommandArgument='<%# Eval("ID") %>' Font-Size="Large" runat="server" Font-Bold="true" OnClientClick="return GetSelectedRow(this)"><i class="delete" ></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkDelete" Text="" CommandArgument='<%# Eval("DocID") %>' Font-Size="Large" runat="server" title="Delete" Font-Bold="true" OnClientClick="return GetSelectedRowDelete(this)"><i class="delete"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                </div>
                <%-- </div>
                </div>--%>
                <div class="col-lg-12 padding-none float-left">
                    <div class="form-group">
                        <div class="col-sm-12 text-right padding-none">
                            <a href="landing.html">
                                <asp:UpdatePanel ID="UpdatePanelBtns" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <a href="#">
                                            <asp:Button type="button" class="btn btn-signup_popup" runat="server" ID="btnSubmit" OnClick="btnSubmit_Click" Text="Submit" TabIndex="29" OnClientClick="javascript:return Submitvalidate('Submit');"></asp:Button></a>

                                        <asp:Button class=" btn btn-revert_popup" ID="btnReset" runat="server" Text="Reset" TabIndex="30" OnClick="btnReset_Click"></asp:Button>
                                        <asp:Button class="btn btn-cancel_popup" ID="btnCancelSubmit" runat="server" Text="Cancel" TabIndex="31" PostBackUrl="~/UserManagement/EmployeeList.aspx"></asp:Button></a>
                                   
                                       
                                       
                                        <asp:Button ID="btnUpdate" CssClass=" btn btn-signup_popup" runat="server" Text="Update" OnClientClick="javascript:return Submitvalidate('Update');" OnClick="btnUpdate_Click" ToolTip="click to Update Employee" />
                                        <asp:Button ID="btnCancel" CssClass=" btn btn-cancel_popup" runat="server" Text="Cancel" OnClick="btnCancel_Click" ToolTip="click to Cancel" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnUpdate" />
                                    </Triggers>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnSubmit" />
                                    </Triggers>
                                </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="Panel_Footer_Message_Label">
            <asp:UpdatePanel ID="UPLblMsgMessage" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Label ID="lblMsgMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div style="display:none">
        <asp:Button ID="btnUpdateWithOutValidation" CssClass=" btn btn-signup_popup" runat="server" Text="Update" OnClick="btnUpdate_Click" ToolTip="click to Update Employee" />
    </div>

    <div class="modal fade department" id="ViewTrainingRecord" role="dialog">
        <div class="modal-dialog modal-lg" style="min-width: 90%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title Panel_Title" style="color: aliceblue">Document</h4>
                    <button type="button" class="close " data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body " style="max-height: 720px;">
                    <div class=" ">
                        <div>
                            <div style="border: groove; border-width: 1px;">
                                <div id="div1" runat="server">
                                    <asp:Literal ID="literalViewDocment" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12" style="text-align: center; padding-top: 10px">
                            <button type="button" data-dismiss="modal" runat="server" id="btnCancelDoc" class="float-right btn btn-cancel_popup" tabindex="15" visible="false">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal popup For Change Password-->
    <div class="modal department fade" id="ChangePasswordModal" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header Panel_Header" style="padding: 8px">

                    <h4 class="modal-title" style="color: aliceblue">Reset Password</h4>
                    <button type="button" class="close" data-dismiss="modal" onclick="ModelPopupClear();">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-left: 10px; margin-right: 10px">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="div2" runat="server" visible="true">
                                    <div class="col-12 form-group float-left padding-none">
                                        <label class="control-label col-sm-4 float-left top">New Password<span class="smallred_label">*</span></label>
                                        <div class="col-sm-8 float-left" style="margin-bottom: 5px">
                                            <input type="password" class="form-control login_input_sign_up" id="txtNewPassword" runat="server" placeholder="Enter New Password" data-toggle="tooltip" data-placement="bottom" title=" Password must contain atleast 8 characters,one uppercase,one lowercase,one special character and one number." autocomplete="off" onkeypress="return NoSpace(event)" maxlength="20" />
                                        </div>
                                    </div>
                                    <div class="col-12 form-group float-left padding-none">
                                        <label class="control-label col-sm-4 float-left top">Confirm Password<span class="smallred_label">*</span></label>
                                        <div class="col-sm-8 float-left" style="margin-bottom: 5px">
                                            <input type="password" class="form-control login_input_sign_up" id="txtConfirmPassword" runat="server" placeholder="Enter Confirm Password" data-toggle="tooltip" data-placement="bottom" title="Password and Confirm Password should be same" autocomplete="off" maxlength="20" onkeypress="return NoSpace(event);" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="modal-footer" style="padding-bottom: 2px; padding-top: 2px; margin-top: 5px; text-align: right">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional" style="display: inline-block">
                        <ContentTemplate>
                            <asp:Button ID="btnsubmitResetPwd" type="button" Class="btn-signup_popup top" runat="server" Text="Submit" OnClientClick=" return  ValidateChangePassword();" OnClick="btnsubmitResetPwd_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" data-dismiss="modal" id="btnCanclePwd" runat="server" class="btn-cancel_popup top" onclick="ModelPopupClear();">Cancel</button>
                </div>
                <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Always" style="padding: 5px">
                    <ContentTemplate>
                        <asp:Label ID="Label1" runat="server" Text="" ForeColor="Red"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <!-- Modal popup For Designation-->

    <%-- designation start--%>
    <div id="myModalDesignation" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Designation</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="form-group col-sm-12" style="float: none !important">
                        <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer" style="float: none !important">Designation Name <span class="smallred_label">*</span></label>
                        <div class="col-sm-12 padding-none" style="float: none !important">
                            <asp:UpdatePanel ID="UPtxtDesignationName" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <input type="text" class="form-control login_input_sign_up" maxlength="27" id="txtDesignationName" runat="server" placeholder="Designation Name" onkeypress="return ValidateAlphaNumericSpace(event)" onpaste="return false" tabindex="13" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                </div>
                <div class=" col-12 modal-footer">
                    <div class="col-12 text-right">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Button ID="btnCreateDesignation" Class=" btn btn-signup_popup" runat="server" Text="Create" OnClientClick="javascript:return DesignationRequired();" TabIndex="29" OnClick="btnCreateDesignation_Click" />
                                 <button type="button" data-dismiss="modal" runat="server" id="btnDesignationCancel" class=" btn btn-cancel_popup" tabindex="28">Cancel</button>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                   
                </div>
                <div class="Panel_Footer_Message_Label Model_Msg">
                    <asp:UpdatePanel ID="UPdesigMessage" runat="server" UpdateMode="Always" style="padding: 5px">
                        <ContentTemplate>
                            <asp:Label ID="lblDesignationError" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

            </div>

        </div>
    </div>

    <%-- End designation--%>
    <%-- dept--%>
    <div id="myModalDepartment" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Department</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">

                    <asp:UpdatePanel ID="UPDepartmentCreationModal" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-group col-sm-12" style="float: none !important">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Department Code<span class="smallred_label">*</span></label>
                                <div class="col-sm-12 padding-none" style="float: none !important">

                                    <input type="text" class="form-control login_input_sign_up" name="name" id="txtDeptCode" maxlength="5" runat="server" placeholder="New Department Code" onkeypress="return ValidateAlphaNumericSpace(event)" onpaste="return false" tabindex="30" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12" style="float: none !important">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer" style="float: none !important">Department Name<span class="smallred_label">*</span></label>
                                <div class="col-sm-12 padding-none" style="float: none !important">
                                    <input type="text" class="form-control login_input_sign_up" name="name" id="txtDeptName" maxlength="50" runat="server" placeholder="New Department Name" onkeypress="return ValidateAlphaNumericSpace(event)" onpaste="return false" tabindex="31" />

                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-12 modal-footer">
                    <div class="col-12  text-right ">
                        <asp:UpdatePanel ID="UPbtndeptCreate" runat="server" UpdateMode="Conditional" style="display: inline-block">
                            <ContentTemplate>
                                <asp:Button ID="btnDeptCreate" CssClass="f btn btn-signup_popup" runat="server" Text="Create" OnClientClick="return DepartmentRequired();" TabIndex="33" OnClick="btnDeptCreate_Click" />
                                 <button type="button" data-dismiss="modal" id="btnDeptCancel" runat="server" class=" btn btn-cancel_popup" tabindex="32">Cancel</button>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                </div>
                <div class="Panel_Footer_Message_Label Model_Msg">
                    <asp:UpdatePanel ID="UPDepartmentError" runat="server" UpdateMode="Always" style="padding: 5px">
                        <ContentTemplate>
                            <asp:Label ID="lblDepartmentError" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
     <asp:HiddenField ID="HFStatus" runat="server" />
    <asp:HiddenField ID="HFEmpID" runat="server" />
    <asp:HiddenField ID="HFImage" runat="server" />
    <asp:HiddenField ID="hdfMaxDateForDOB" runat="server" />
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Always" style="padding: 5px">
        <ContentTemplate>
            <asp:Button ID="btnResetDepartment" runat="server" Text="Button" Style="display: none;" OnClick="btnResetDepartment_Click"></asp:Button>
            <asp:Button ID="btnResetDesignation" runat="server" Text="Button" Style="display: none;" OnClick="btnResetDesignation_Click"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>

        $(function () {
              var Status = '<%=this.Request.QueryString["S"]%>';
            if (Status == "In-Active") {
                $('#<%=gvDocumentUpload.ClientID%> tr').each(function () {
                    $(this).find('th:eq(4)').hide();
                    $(this).find('td:eq(4)').hide();
                });
            }
         });
        $(function () {
               var Status = '<%=this.Request.QueryString["S"]%>';
            if (Status == "In-Active") {
                $('#divFileUpload').hide();
                $('.add_depart1').hide();
                $('#<%=btnUpdate.ClientID%>').hide();
                $('.ViewMode').css('cursor', 'not-allowed');         
                $('.ViewMode').children().css('cursor', 'not-allowed');   
                $('.ViewMode').addClass('cusorNotAllowed');   
                $('.ViewMode').attr("disabled", "disabled");     
                $('.ViewMode').prop('disabled', true); 
                $('.ViewMode .btn').addClass('disabled');
            }
        });
        $(function(){
         var empID = '<%=this.Request.QueryString["EmpID"]%>';
            if(empID!="")
            {
                $('#txtTitle').text("Update Employee");
            }
        });
        $(function () {
            $(document).on("hidden.bs.modal", "#myModal", function () {
                document.getElementById('<%=txtDeptName.ClientID%>').value = ''; // Remove from DOM.
                document.getElementById('<%=txtDeptCode.ClientID%>').value = '';
            });
        });
        function HideModDepartment() {
            $("#myModalDepartment").modal('hide');
            var btnDept = document.getElementById('<%=btnResetDepartment.ClientID%>');
            btnDept.click();

        }
        function HideModDesignation() {

            $("#myModalDesignation").modal('hide');
            var btnDesig = document.getElementById('<%=btnResetDesignation.ClientID%>');
            btnDesig.click();
        }

        function HideModDepartmentErrorMsg() {
            document.getElementById("txtDesignationName").value = "";
        }
    </script>


    <script type="text/javascript">
        function GetSelectedRow(lnk) {
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var lblDocID = row.cells[0].innerHTML;
            var hdnDeleteRow = document.getElementById('<%=hdnDeleteRow.ClientID%>');
            hdnDeleteRow.value = lblDocID;
            ConformAlert();
        }
        function GetSelectedRowDelete(lnk) {

            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var lblDocID = row.cells[0].innerHTML;
            var hdnDeleteRow = document.getElementById('<%=hdnDeleteRow.ClientID%>');
            hdnDeleteRow.value = lblDocID;
            ConformAlert();
        }
    </script>
    <script>
        function ConformAlert() {
            custAlertMsg('Are you sure you want to Delete this Document ?', 'confirm', true);
        }
    </script>
    <script>
        //This is for Validate Change password( Current and New and Confirm Password)
        function ValidateChangePassword() {
            var txtNewPwd = document.getElementById("<%=txtNewPassword.ClientID%>").value;
            var txtConfirmPwd = document.getElementById("<%=txtConfirmPassword.ClientID%>").value;
            errors = [];
            if (txtNewPwd != "")
            {
                if (txtNewPwd.length < 8)
                {
                    errors.push("Password must contain Minimum 8 characters");
                }
                if (txtNewPwd.length > 20)
                {
                    errors.push("Password must contain Maximum 20 characters");
                }
                if (txtNewPwd.search(/[a-z]/) < 0) {
                    errors.push("Password must contain atleast one Lower Case");
                }
                if (txtNewPwd.search(/[A-Z]/) < 0) {
                    errors.push("Password must contain atleast one Upper Case");
                }
                if (txtNewPwd.search(/[0-9]/) < 0) {
                    errors.push("Password must contain atleast one Number");
                }
                if (txtNewPwd.search(/[@#&*]/) < 0) {
                    errors.push("Password must contain atleast one @,#,&,* character");
                }
            }
            if (txtNewPwd == '') {
                errors.push("Enter New Password");

            }
            if (txtConfirmPwd != "") {
                if (txtNewPwd != txtConfirmPwd) {
                    errors.push(" New Password and Confirm Password should match");
                }
            }
            if (txtConfirmPwd == '') {
                errors.push("Enter Confirm Password");
            }

            if (errors.length > 0) {
                document.getElementById('msgError').innerHTML = errors.join("<br/>");
                $('#ModalError').modal();
                $("#btnErrorOk").focus();
                return false;
            }
            else
            {
                ShowPleaseWait('show');
               //  setTimeout(function () { $("#btnsubmitResetPwd").attr("disabled", true); }, 3000);//Reset PWD
                return true;
            }
        }
    </script>

    <script>
        function Reset() {
            document.getElementById("<%=txtNewPassword.ClientID%>").value = "";
            document.getElementById("<%=txtConfirmPassword.ClientID%>").value = "";
        }
        //for clearing the signup page
        function ClearChangePwd() {
            Reset();
            $("#ChangePasswordModal").modal('hide');
        }
        function ErrorClearChangePwd() {
            Reset();

            $("#ChangePasswordModal").modal('show');
        }
    </script>

    <script>

        function ValidatEmployeeCode() {
            var ddldocType = document.getElementById('<%=ddlDocumenType.ClientID%>');
            errors = [];
            if (ddldocType.value == "0") {
                errors.push("Please Select  Document Type");
            }
            var array = ['pdf'];
            var xyz = document.getElementById('<%=FileUploadDoc.ClientID%>');
            var filename = "";
            var fullPath = document.getElementById('<%=FileUploadDoc.ClientID%>').value;
            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }
            }

            var EmpCode = document.getElementById('<%=txtEmpCode.ClientID%>');
            var Extension = xyz.value.substring(xyz.value.lastIndexOf('.') + 1).toLowerCase();
            errors = [];
            if (ddldocType.value == "0") {
                errors.push("Please Select  Document Type");
            }
            if (xyz.value == '') {
                errors.push("Please Upload file.");
            }
            else {
                if (array.indexOf(Extension) <= -1) {

                    errors.push("Please Upload PDF files only.");
                }
            }
            if (EmpCode.value == "") {
                errors.push('Please Enter Employee Code');

            }
            if (filename.length > 190) {
                var strMsg = 'Max Characters In File name allowed is 190 characters' + "<br/>" + "Current File Name Consist " + filename.length + " Characters";
                errors.push(strMsg);
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>

    <script>      
        $(function () {
            $('#<%=txtStartDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: true,
            });
            var startdate = document.getElementById('<%=txtStartDate.ClientID%>').value
            $('#<%=txtEndDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                minDate: startdate,
                useCurrent: false,
            });
            $('#<%=txtStartDate.ClientID%>').on("dp.change", function (e) {
                $('#<%=txtEndDate.ClientID%>').data("DateTimePicker").minDate(e.date);

                   $('#<%=txtEndDate.ClientID%>').val("");
            });
            $('#<%=txtEndDate.ClientID%>').on("dp.change", function (e) {
            });
        });
    </script>

    <script type="text/javascript">
        //for datepickers
        var startdate2 = document.getElementById('<%=txtStartDate.ClientID%>').value
        if (startdate2 == "") {
            $('#<%=txtStartDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
            $('#<%=txtEndDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
        }
    </script>

    <script type="text/javascript">
        var todayYear = new Date().getFullYear();
        $('#<%=txtEmpDob.ClientID%>').datetimepicker({
            format: 'DD MMM YYYY',
            maxDate: new Date(new Date().setFullYear(todayYear - 18)),
        });

</script>

    <script>
        //to stop loading the default date in DOB
        function func1() {
          
            var dob = document.getElementById('<%=HFDOB.ClientID%>').value;
             
            if (dob != "") {
                document.getElementById('<%=txtEmpDob.ClientID%>').value = dob;
            }
            else {
                document.getElementById('<%=txtEmpDob.ClientID%>').value = "";
            }
        }
        window.onload = func1;
    </script>

    <script>
        $('#<%=btnDesignationCancel.ClientID%>').click(function () {
            $('#<%=txtDesignationName.ClientID%>').val("");
            $('#<%=lblDesignationError.ClientID%>').text("");

        });

        function DesignationRequired() {

            var NewDesig = document.getElementById("<%=txtDesignationName.ClientID%>").value.trim();
            errors = [];
            if (NewDesig == "") {
                errors.push("Enter new Designation Name");
                document.getElementById("<%=txtDesignationName.ClientID%>").focus();
            }
            if (errors.length > 0) { 
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                $("myModalDesignation").hide();
                return true;

            }
        }
    </script>

    <script>
        function DesignationPopupLoad() {
            document.getElementById("<%=txtDesignationName.ClientID%>").focus();

        }
        function ReloadListPage() {
            window.open("<%=ResolveUrl("~/UserManagement/EmployeeList.aspx")%>", "_self");
        }
        function ReloadCurrentPage() {

            window.open("<%=ResolveUrl("~/UserManagement/Employee.aspx")%>", "_self");
        }
        function ReloadEmployeeCodePage() {
            window.open("<%=ResolveUrl("~/UserManagement/EmployeeCode.aspx")%>", "_self");
        }
    </script>

    <script>
        function DepartmentRequired() {
            var DeptName = document.getElementById("<%=txtDeptName.ClientID%>").value.trim();
            var DeptCode = document.getElementById("<%=txtDeptCode.ClientID%>").value.trim();
            errors = [];
            if (DeptCode == "") {
                errors.push("Enter Department Code");
                document.getElementById("<%=txtDeptCode.ClientID%>").focus();

            }
            if (DeptName == "") {
                errors.push("Enter Department Name");
                document.getElementById("<%=txtDeptName.ClientID%>").focus();

            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }

            return true;

        }
    </script>

    <script type="text/javascript">
        //Reset the Fields
        function Resetvalidate() {
            document.getElementById("<%=txtEmpFirstName.ClientID%>").value = '';
            document.getElementById("<%=txtEmpLastName.ClientID%>").value = '';
            document.getElementById("<%=txtEmpEmailID.ClientID%>").value = '';
            document.getElementById("<%=txtEmpLoginID.ClientID%>").value = '';
            document.getElementById("<%=txtStartDate.ClientID%>").value = '';
            document.getElementById("<%=txtEndDate.ClientID%>").value = '';
            document.getElementById("<%=ddlCountry.ClientID%>").value = 0;
            document.getElementById("<%=ddlState.ClientID%>").value = 0;
            document.getElementById("<%=ddlCity.ClientID%>").value = 0;
            document.getElementById("<%=ddlDepartments.ClientID%>").value = 0;
            document.getElementById("<%=ddlDesignation.ClientID%>").value = 0;
            document.getElementById("<%=txtEmpDob.ClientID%>").value = '';
            document.getElementById("<%=imgEmpImage.ClientID%>").src ="<%=ResolveUrl("~/Images/UserLogin/DefaultImage.jpg")%>";
        }
    </script>

    <script>
        function ShowPleaseWaitGifDisableImage() {
            ShowPleaseWaitImg('hide');
        }
         function ShowPleaseWaitGifDisable() {
            ShowPleaseWait('hide');
        }
        var checkDate;
        function GetCurrentDate() {
            var date = new Date();
            var strArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var d = date.getDate();
            var m = strArray[date.getMonth()];
            var y = date.getFullYear();
            checkDate= '' + (d <= 9 ? '0' + d : d) + ' ' + m + ' ' + y;
        }

        // For mandatory fields in main Page Employee.aspx
        function Submitvalidate(btnType) {
             GetCurrentDate();
            var EmpCode = document.getElementById("<%=txtEmpCode.ClientID%>").value.trim();
            var EmpFirstName = document.getElementById("<%=txtEmpFirstName.ClientID%>").value.trim();
            var EmpLastName = document.getElementById("<%=txtEmpLastName.ClientID%>").value.trim();
            var EmpDob = document.getElementById("<%=txtEmpDob.ClientID%>").value.trim();
            var EmpEmailID = document.getElementById("<%=txtEmpEmailID.ClientID%>").value.trim();
            var EmpLoginID = document.getElementById("<%=txtEmpLoginID.ClientID%>").value.trim();
            var EmpMobileNo = document.getElementById("<%=txtEmpMobileNo.ClientID%>").value.trim();
            var EmpAddress = document.getElementById("<%=txtEmpAddress.ClientID%>").value.trim();
            var TelNo = document.getElementById('<%=txtEmpTelNo.ClientID%>').value.trim();
            var txtEmpEmgNo = document.getElementById('<%=txtEmpEmgNo.ClientID%>').value.trim();
            var endDate = document.getElementById('<%=txtEndDate.ClientID%>').value.trim();
            var EmpActiveStatus = document.getElementById("<%=rbbtnInActive.ClientID %>").checked;
             var Name = EmpFirstName + EmpLastName;
            
            errors = [];
            if (TelNo != "") {
                if (TelNo.length != 10) {
                    errors.push("Alternate Number Should be 10 Digits");
                }
            }
            if (txtEmpEmgNo != "") {
                if (txtEmpEmgNo.length != 10) {
                    errors.push("Emergency Number Should be 10 Digits");
                }
            }
            if (EmpFirstName == '') {
                errors.push("Enter First Name");
            }
            if (EmpLastName == '') {
                errors.push("Enter Last Name");
            }
            if (EmpDob == "") {
                errors.push("Enter Date Of Birth");
            }
            if (EmpEmailID == "") {
                errors.push("Enter Email ID");
            } else if (!IsValidEmail(EmpEmailID)) {
                errors.push("Invalid Email ID");
            }
            if (EmpMobileNo == "") {
                errors.push("Enter Mobile Number");
            } else
                if (EmpMobileNo.length != 10) {
                    errors.push("Mobile Number Should be 10 Digits");
                }
            if (EmpLoginID == "") {
                errors.push("Enter Employee Login ID");
            } else
                if (EmpLoginID.length <= 3) {
                    errors.push("Login Id Should be atleast 4 Characters");
                }


            if (document.getElementById('<%=txtStartDate.ClientID%>').value == "") {
                errors.push("Enter Start Date");
            }
            if (document.getElementById('<%=ddlDesignation.ClientID%>').value == 0) {
                errors.push("Select Designation");
            }
            if (document.getElementById('<%=ddlDepartments.ClientID%>').value == 0) {
                errors.push("Select Department");
            }


            if (document.getElementById('<%=ddlCountry.ClientID%>').value == 0) {
                errors.push("Select Country");
            }

            if (document.getElementById('<%=ddlState.ClientID%>').value == 0) {
                errors.push("Select State");
            } if (document.getElementById('<%=ddlCity.ClientID%>').value == 0) {
                errors.push("Select City");
            }
            if (EmpAddress == "") {
                errors.push("Enter Address");
            }
            if (btnType == "Update")
            {
                if (EmpActiveStatus)
                {
                    if (endDate == "") {
                        errors.push("Enter End Date");
                    }
                    else if (checkDate <=endDate)
                    {
                        custAlertMsg('You have set Future Inactive Date  for the Employee <b>' + EmpCode + '-' + Name + '</b>, <br>Do you want to continue?', "confirm", "");
                        $('.btnUcCnfYes').attr("onclick", "$('#<%=btnUpdateWithOutValidation.ClientID%>').click()");
                        return false;
                    }
                    else
                    {
                        custAlertMsg('You\'re InActivating the Employee  <b>' + EmpCode + '-' + Name + '</b> whose details will not be available for future Modifications, <br>Do you want to continue?', "confirm", "");
                        $('.btnUcCnfYes').attr("onclick", "$('#<%=btnUpdateWithOutValidation.ClientID%>').click()");
                        return false;
                    }
                } 
            }
            if (errors.length > 0)
            {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else
            {
                ShowPleaseWaitImg('show');//Create or Update Button
                return true;
            }
        }
        
    </script>

    <script>
        function ConfirmAlert() {
            custAlertMsg('Do You Want To Submit Later You Are Unable To Modify', 'confirm', true);
        }
    </script>

    <script>
        $(document).keydown(function (event) {
            if (event.keyCode == 27) {
                var clickButton2 = document.getElementById("<%=btnCancelDoc.ClientID %>");
                var clickButton1 = document.getElementById("<%=btnDeptCancel.ClientID %>");
                var clickButton = document.getElementById("<%=btnDesignationCancel.ClientID %>");
                var Clickbtnerror = document.getElementById('btnErrorOk');
                var Clickbtnwarning = document.getElementById('btnWarningOk');
                var Clickbtnsuccess = document.getElementById('btnSuccessOk');
                clickButton1.click();
                clickButton.click();
                clickButton2.click();
                Clickbtnerror.click();
                Clickbtnwarning.click();
            }
        });
    </script>

    <script>
        $("#NavLnkEmployee").attr("class", "active");
        $("#NavLnkCreateEmployee").attr("class", "active");
    </script>


    <!--Attachments-->
    <script>
        $('#fuDeficiencyReport').on('filepreupload', function (event, data, previewId, index, jqXHR) {
            AllDeficiencyFilesUploaded = 0;
            $('#hdnDeficiencyFilesUploadCount').val(data.filescount);
            var fileNamesExceedCount = 0;
           
            $('.file-preview-thumbnails>.file-preview-frame.explorer-frame.kv-preview-thumb').each(function () {
                if ($(this).find('.explorer-caption').attr('title').length > 200) {
                    fileNamesExceedCount++;
                    $(this).find('.explorer-caption').css("color", "red");
                }
            });
            if (fileNamesExceedCount > 0) {
                var strMsg = fileNamesExceedCount + ' file name exceed' + (fileNamesExceedCount > 1 ? 's' : '') + ' 200 characters';
                return
                {
                    message: strMsg
                };
            }
        });
    </script>
</asp:Content>
