﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="QA_RevertedTS.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TS.QA_RevertedTS1" %>

<%@ Register Src="~/UserControls/SessionTimeOut.ascx" TagPrefix="uc1" TagName="SessionTimeOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 inner_border">
    <div>
        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="panel panel-default" id="mainContainer" runat="server" >
                        <div class="grid_header col-lg-12  col-xs-12 col-md-12 col-sm-12 ">QA Reverted TS</div>
        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 top">
            <asp:GridView ID="gvQA_RevertedTS"
                runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixHead AspGrid"
                EmptyDataText="No Records in QA Reverted TS " EmptyDataRowStyle-CssClass="GV_EmptyDataStyle"
                Width="100%">
                <RowStyle CssClass="padding-none" />
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <%-- <asp:TemplateField HeaderText="Employee ID" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("EmpID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="JR ID" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblEJRID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="JR Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                        <ItemTemplate>
                            <asp:Label ID="lblJRName" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="JR Version" SortExpression="JR_Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Emp Code" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                        <ItemTemplate>
                            <asp:Label ID="lblEmpCode" runat="server" Text='<%# Bind("EmpCode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Trainee Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                        <ItemTemplate>
                            <asp:Label ID="lblTraineeName" runat="server" Text='<%# Bind("TraineeName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:TemplateField HeaderText="Department Name">
                            <ItemTemplate>
                                <asp:Label ID="lblDeptName" runat="server" Text='<%# Bind("DeptName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Dept Code" SortExpression="DeptCode" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Designation" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                        <ItemTemplate>
                            <asp:Label ID="lblDesignationName" runat="server" Text='<%# Bind("DesignationName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:TemplateField HeaderText="Assigned By">
                                <EditItemTemplate>
                                    <asp:Label ID="lblEditTS_AssignedBy" runat="server" Text='<%# Bind("TS_AssignedBy") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblTS_AssignedBy" runat="server" Text='<%# Bind("TS_AssignedBy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Assigned Date" SortExpression="TS_AssignedDate" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTS_AssignedDate" runat="server" Text='<%# Bind("TS_AssignedDate") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTS_AssignedDate" runat="server" Text='<%# Bind("TS_AssignedDate") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reverted By" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                        <ItemTemplate>
                            <asp:Label ID="lblRevertedName" runat="server" Text='<%# Bind("TS_RevertedBy") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reverted Date" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                        <ItemTemplate>
                            <asp:Label ID="lblRevertedDate" runat="server" Text='<%# Bind("TS_RevertedDate") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1 text-center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEditTS" title="Click to Edit" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" runat="server" OnClick="lnkEditTS_Click" CommandArgument='<%# Eval("JR_ID") %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
        </div>
    </div>
         </div>
    <uc1:SessionTimeOut runat="server" ID="SessionTimeOut" />

    <script type="text/javascript">
        $(document).ready(function () {
            // Fix up GridView to support THEAD tags            
            fixGvHeader();
            // $("#gvFeedbackHistory").tablesorter({ sortList: [[1, 0]] });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixGvHeader();
        });

        function fixGvHeader()
        {
            var tbl = document.getElementsByClassName("fixHead");          
            // Fix up GridView to support THEAD tags 
            if ($(".fixHead").find("thead").length == 0) {
                $(".fixHead tbody").before("<thead><tr></tr></thead>");
                $(".fixHead thead tr").append($(".fixHead th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length==0) {
                    $(".fixHead tbody tr:first").remove();
                }               
            }
        }
</script>

</asp:Content>
