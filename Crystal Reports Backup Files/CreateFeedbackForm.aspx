﻿<%@ Page Title="Feedback" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="CreateFeedbackForm.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Administration.Feedback.CreateFeedbackForm" %>

<%@ Register Src="~/UserControls/SessionTimeOut.ascx" TagPrefix="uc1" TagName="SessionTimeOut" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnFeedback_ID" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <script> 
        $(document).ready(function () {
            $("#ContentPlaceHolder1_rptPager_lnkPage_<%=rptPager.Items.Count-1%>").css("background-color", "white");
            $("#ContentPlaceHolder1_rptPager_lnkPage_<%=rptPager.Items.Count-1%>").css("color", "black");
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $("#ContentPlaceHolder1_rptPager_lnkPage_<%=rptPager.Items.Count-1%>").css("background-color", "white");
            $("#ContentPlaceHolder1_rptPager_lnkPage_<%=rptPager.Items.Count-1%>").css("color", "black");
        });
    </script>

    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 dms_outer_border">
        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none" id="divMainContainer" runat="server">
            <div class="panel-group padding-none" id="accordion1">
                <div class="panel panel-default">
                    <div class=" grid_header" data-toggle="collapse">
                        Feedback Master
                    </div>
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upFeedback">
                        <ContentTemplate>
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                                <div class="form-group col-lg-4 col-sm-12 col-xs-12 col-md-4">
                                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                                        <label>Feedback Type</label>
                                        <asp:DropDownList ID="ddlFeedbackType" CssClass="show-tick form-control" data-live-search="true" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlFeedbackType_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="JR Training" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Target Training" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-lg-4 col-sm-12 col-xs-12 col-md-4">
                                    <label for="inputEmail3" class=" padding-none col-lg-12 col-sm-12 col-xs-12 col-md-12 control-label custom_label_answer">Feedback Title</label>
                                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                                        <asp:TextBox ID="txtTitle" CssClass="form-control login_input_sign_up" placeholder="Title" runat="server" onkeypress="return ValidateAlpha(event);"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-lg-4 col-sm-12 col-xs-12 col-md-4 ">
                                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                                        <label>Status</label>
                                        <asp:DropDownList ID="ddlStatus" CssClass="show-tick form-control" data-live-search="true" runat="server" AutoPostBack="true">
                                            <asp:ListItem Text="In-Active" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 ">
                                        <label>Comments</label>
                                        <asp:TextBox ID="txtFeedbackComments" TextMode="MultiLine" Rows="2" CssClass="form-control login_input_sign_up" placeholder="Comments" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                    <div class="pull-right">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass=" btn btn-approve_popup" Text="Submit" OnClick="btnSubmit_Click" OnClientClick="return SubmitValidate();" />
                                        <asp:Button ID="btnUpdate" runat="server" CssClass=" btn btn-revert_popup" Text="Update" OnClick="btnUpdate_Click" Visible="false" OnClientClick="return UpdateValidate();" />
                                        <asp:Button ID="btnReset" runat="server" CssClass=" btn btn-revert_popup" Text="Reset" OnClick="btnReset_Click" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlStatus" />
                            <asp:AsyncPostBackTrigger ControlID="ddlFeedbackType" />
                            <asp:AsyncPostBackTrigger ControlID="txtTitle" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <asp:UpdatePanel ID="upFeedbackQuestions" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div id="divFeedbackQuestion" runat="server" visible="false">
                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none grid_panel_hod">
                                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12  padding-none">
                                        <div class="grid_header col-lg-12 col-sm-12 col-xs-12 col-md-12">Feedback Questions List</div>
                                        <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6  pull-left" style="margin-top: 5px;" runat="server" id="PageDiv" visible="false">
                                            <div>
                                                <table>
                                                    <tr>
                                                        <td>Page:&nbsp;</td>
                                                        <td>
                                                            <asp:UpdatePanel ID="upddlPageSize" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddlPageSize" CssClass="form-control" runat="server" AutoPostBack="true" ToolTip="Select No.of Records/page" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                                        <asp:ListItem Text="5" Value="5" />
                                                                        <asp:ListItem Text="10" Value="10" />
                                                                        <asp:ListItem Text="15" Value="15" />
                                                                        <asp:ListItem Text="20" Value="20" />
                                                                        <asp:ListItem Text="25" Value="25" />
                                                                        <asp:ListItem Text="50" Value="50" />
                                                                    </asp:DropDownList>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlPageSize" EventName="SelectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 bottom">
                                            <div class="pull-right  padding-none" style="margin-top: 10px; padding-right: 5px">
                                                <asp:Button ID="btnHistory" CssClass=" btn btn-dashboard" runat="server" Style="margin-bottom: 0px;" Text="History" OnClick="btnHistory_Click" />
                                                <button type="button" class=" btn btn-dashboard" runat="server" id="btnCreate" onclick="openQuestionModelWhenCreate('Yes');" style="margin-bottom: 0px;">Create Question</button>
                                            </div>
                                        </div>
                                        <asp:UpdatePanel UpdateMode="Conditional" ID="upgvFeedback" runat="server">
                                            <ContentTemplate>
                                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 section_table ">
                                                    <asp:GridView ID="gvFeedback" EmptyDataText="No Record's"
                                                        runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixFeedbackHeader AspGrid"
                                                        EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" OnRowCommand="gvFeedback_RowCommand">
                                                        <RowStyle CssClass="col-xs-12 padding-none" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S.No" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1 ">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditRowNumber" runat="server" Text='<%# Bind("RowNumber") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRowNumber" runat="server" Text='<%# Bind("RowNumber") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Questions" HeaderStyle-CssClass="col-xs-8" ItemStyle-CssClass="col-xs-8" ItemStyle-HorizontalAlign="Left">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditQuestion" runat="server" Text='<%# Bind("Question") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblQuestion" runat="server" Text='<%# Bind("Question") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtCurrentStatus" runat="server" Text='<%# Bind("QStatus") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCurrentStatus" runat="server" Text='<%# Bind("QStatus") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Comments" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblComments" runat="server" Text='<%# Bind("QRemarks") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="QStatus" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblQStatus" runat="server" Text='<%# Bind("QuestionStatus") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Edit" HeaderStyle-CssClass="text-center col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CssClass="btngridedit_tms" runat="server" CommandName="GetFeedback" CommandArgument='<%# Eval("QuestionID") %>' ToolTip="Click To Edit"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12" style="text-align: center">
                                        <div class="pagination pull-right" style="margin: 0px;">
                                            <asp:UpdatePanel runat="server" ID="uplnkPage" style="display: inline-block;">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptPager" runat="server">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>' Enabled='<%# Eval("Enabled") %>' OnClick="lnkPage_Click"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div class="Panel_Footer_Message_Label">
                                        <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%-- Start Question Model Popup--%>
    <div id="myModal_lock" class="modal  fade feedbackpop" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close  close_feed" data-dismiss="modal">&times;</button>
                    <h4>
                        <asp:Label ID="lblScheduleTitle" runat="server" Text="Create Questions"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel runat="server" ID="upFeedbackCE" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divQuestionStatus">
                                <div class="btn-group padding-none col-sm-12 col-xs-12 col-lg-12 col-sm-12 col-xs-12 col-md-12 col-md-12 pull-left" data-toggle="buttons">
                                    <label for="inputPassword3" class=" col-sm-12 padding-none control-label">Access Status</label>
                                    <label class="btn btn-primary_radio col-xs-6 col-lg-6 col-md-6 col-sm-6 active" id="lblRadioActiveFQ" runat="server">
                                        <%--<input type="radio" name="options" id="rbtnActive" autocomplete="off" runat="server">--%>
                                        <asp:RadioButton ID="rbtnActive" runat="server" GroupName="A" Text="Active" />
                                        <%-- Active	--%>
                                    </label>
                                    <label class="btn btn-primary_radio col-xs-6 col-lg-6 col-md-6 col-sm-6" id="lblRadioInActiveFQ" runat="server">
                                        <%--<input type="radio" name="options" id="rbtnInActive" autocomplete="off" runat="server">--%>
                                        <asp:RadioButton ID="rbtnInActive" runat="server" GroupName="A" Text="In-Active" />
                                        <%-- In Active		--%>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class=" col-sm-12 padding-none control-label">Create Question</label>
                                <div class="col-sm-12 padding-none">
                                    <asp:TextBox ID="txtQuestion" runat="server" TextMode="MultiLine" Rows="5" placeholder="Enter Question" CssClass="form-control feed_text"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                                <label for="inputPassword3" class=" col-sm-12 control-label padding-none custom_label_answer">Comments</label>
                                <div class="col-sm-12 padding-none">
                                    <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Rows="5" placeholder="Enter Comments" CssClass="form-control feed_text"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-sm-12 padding-none">
                                <button type="button" class="btn btn-canceldms_popup btn_ pull-right" style="margin-bottom: 0px !important" data-dismiss="modal">Cancel</button>
                                <div id="divBtnAddQuestion">
                                    <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-approve_popup pull-right" Text="Add" OnClick="btnAdd_Click" OnClientClick="return PopUpAddValidate();" />
                                </div>
                                <div id="divBtnUpdateQuestion">
                                    <asp:Button ID="btnUpdateQuestion" runat="server" CssClass="btn btn-approve_popup pull-right" Text="Update" OnClick="btnUpdateQuestion_Click" OnClientClick="return PopUpUpdateValidate();" />
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <%-- End Question Model Popup--%>

    <%-- Start Feedback History Grid--%>
    <div id="FeedbackHistoryModel" class="modal feedbackpop fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close close_feed" data-dismiss="modal">&times;</button>
                    <h4>
                        <asp:Label ID="Label1" runat="server" Text="Feedback History"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel runat="server" ID="upgvHistory" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divHistoryGrid" runat="server">
                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none history_Section">
                                    <div class="col-sm-12" style="overflow: auto">
                                        <asp:GridView ID="gvFeedbackHistory"
                                            EmptyDataText="No Record's"
                                            runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixHead AspGrid"
                                            EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                            <AlternatingRowStyle BackColor="White" />
                                            <RowStyle CssClass="col-xs-12 padding-none" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblEditActionBy" runat="server" Text='<%# Bind("FStatus") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblActionBy" runat="server" Text='<%# Bind("FStatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action By" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2 ">
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblEditRemarks" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>l
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action Date" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblEditActionDate" runat="server" Text='<%# Bind("ActionDate") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblActionDate" runat="server" Text='<%# Bind("ActionDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Remarks" HeaderStyle-CssClass="col-xs-5" ItemStyle-CssClass="col-xs-5">
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblEditAction" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>l
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAction" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <%-- End Feedback History Grid--%>
    <uc1:SessionTimeOut runat="server" ID="SessionTimeOut" />

    <script>
        function openQuestionModelWhenUpdate(ClearText) {
            if (ClearText == "Yes") {
                document.getElementById("<%=txtComments.ClientID %>").value = "";
            }
            $('#<%=lblScheduleTitle.ClientID%>').text("Update Feedback Form");
            $("#divQuestionStatus").show();
            $("#divBtnAddQuestion").hide();
            $("#divBtnUpdateQuestion").show();
            $('#myModal_lock').modal({ backdrop: 'static', keyboard: false });
        }

        function openQuestionModelWhenCreate(ClearText) {
            if (ClearText == "Yes") {
                document.getElementById("<%=txtQuestion.ClientID %>").value = "";
                document.getElementById("<%=txtComments.ClientID %>").value = "";
            }
            $('#<%=lblScheduleTitle.ClientID%>').text("Create Feedback Form");
            $("#divQuestionStatus").hide();
            $("#divBtnAddQuestion").show();
            $("#divBtnUpdateQuestion").hide();
            $('#myModal_lock').modal({ backdrop: 'static', keyboard: false });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            // Fix up GridView to support THEAD tags            
            fixGvHeader();
            addThead();
            // $("#gvFeedbackHistory").tablesorter({ sortList: [[1, 0]] });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixGvHeader();
            addThead();
        });

        function fixGvHeader() {
            var tbl = document.getElementsByClassName("fixHead");
            // Fix up GridView to support THEAD tags     
            if ($(".fixHead").find("thead").length == 0) {
                $(".fixHead tbody").before("<thead><tr></tr></thead>");
                $(".fixHead thead tr").append($(".fixHead th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHead tbody tr:first").remove();
                }
            }
        }

        function addThead() {
            var tbl = document.getElementsByClassName("fixFeedbackHeader");
            // Fix up GridView to support THEAD tags 
            if ($(".fixFeedbackHeader").find("thead").length == 0) {
                $(".fixFeedbackHeader tbody").before("<thead><tr></tr></thead>");
                $(".fixFeedbackHeader thead tr").append($(".fixFeedbackHeader th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixFeedbackHeader tbody tr:first").remove();
                }
            }
        }
    </script>

    <script>
        $(document).ready(function () {
            // Change the selector if needed
            //var $table = $('table.cal'),
            //    $bodyCells = $table.find('tbody tr:first').children(),
            //    colWidth;

            //// Adjust the width of thead cells when window resizes
            //$(window).resize(function () {
            //    // Get the tbody columns width array
            //    colWidth = $bodyCells.map(function () {
            //        return $(this).width();
            //    }).get();

            //    // Set the width of thead columns
            //    $table.find('thead tr').children().each(function (i, v) {
            //        $(v).width(colWidth[i]);
            //    });
            //}).resize(); // Trigger resize handler

            $('#notificationFooter').click(function () {
                $(".notify_more").fadeOut(function () {
                    $(".notify_more").text(($(".notify_more").text() == 'Show Less') ? 'Show More' : 'Show Less').fadeIn();
                })
                var h = parseInt($('.notifications_alerts ul').css('height'), 10);
                if (h != 600) {
                    $('.notifications_alerts ul').css('height', '600px');
                }
                else {
                    $('.notifications_alerts ul').css('height', '');
                }
            });

            $('.closebtn').on('click', function () {

                $('#paragraph2').addClass('col-xs-12');
                $('#paragraph2').removeClass('col-xs-10');
                $('#paragraph1').hide();
                $('.closebtn1').show();
                $('.closebtn').hide();

            });
            $('.closebtn1').on('click', function () {

                $('#paragraph2').addClass('col-xs-10');
                $('#paragraph2').removeClass('col-xs-12');
                $('#paragraph1').show();
                $('.closebtn').show();
                $('.closebtn1').hide();

            });

            $('.btn_assign').on('click', function () {
                $(input, this).removeAttr('checked');
                $(this).removeClass('active');
            });
            var table = $('#example').DataTable({
                fixedHeader: {
                    header: true
                }
            });

            var selector = '.landing_links ul li a';

            $(selector).on('click', function () {
                $(selector).removeClass('active');
                $(this).addClass('active');
            });
            $("#notificationLink").click(function () {
                $("#notificationContainer").fadeToggle(300);
                $("#notification_count").fadeOut("slow");
                return false;
            });
            $(document).click(function () {
                $("#notificationContainer").hide();
            });

            $("#notificationContainer").click(function () {
                return false;
            });
        });
    </script>
    <script>
        function RedirecttoPage() {
            window.open("<%=ResolveUrl("~/TMS/Administration/Feedback/CreateFeedbackForm.aspx")%>", "_self");
        }
    </script>
    <script type="text/javascript">
        function SubmitValidate() {
            var FeedbackType = document.getElementById('<%=ddlFeedbackType.ClientID%>').value;
            var Tille = document.getElementById('<%=txtTitle.ClientID%>').value;
            errors = [];
            if (FeedbackType == 0) {
                errors.push("Select FeedbackType");
            }
            if (Tille == "") {
                errors.push("Enter Feedback Title");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <script type="text/javascript">
        function PopUpUpdateValidate() {
            var QComments = document.getElementById('<%=txtComments.ClientID%>').value;
            var Question = document.getElementById('<%=txtQuestion.ClientID%>').value;
            errors = [];
            if (Question == "") {
                errors.push("Enter Question");
            }
            if (QComments == "") {
                errors.push("Enter Comments");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <script type="text/javascript">
        function PopUpAddValidate() {
            var Question = document.getElementById('<%=txtQuestion.ClientID%>').value;
            errors = [];
            if (Question == "") {
                errors.push("Enter Question");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>

    <script type="text/javascript">
        function UpdateValidate() {
            var FeedbackType = document.getElementById('<%=ddlFeedbackType.ClientID%>').value;
            var Tille = document.getElementById('<%=txtTitle.ClientID%>').value;
            var FeedbackComments = document.getElementById('<%=txtFeedbackComments.ClientID%>').value;
            errors = [];
            if (FeedbackType == 0) {
                errors.push("Select FeedbackType");
            }
            if (Tille == "") {
                errors.push("Enter Feedback Title");
            }
            if (FeedbackComments == "") {
                errors.push("Enter Comments");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>

    <script>
        function OpenModelwithValue() {
            var hdnValue = document.getElementById("<%=hdnFeedback_ID.ClientID %>").value;
            if (hdnValue > 0) {
                $('#myModal_lock').modal('show');
            }
        }
    </script>

    <script>
        function OpenHistoryModel() {
            $('#FeedbackHistoryModel').modal({ backdrop: 'static', keyboard: false });
        }
    </script>
</asp:Content>
