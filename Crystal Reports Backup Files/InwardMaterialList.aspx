﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="InwardMaterialList.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.MaterialRegister.InwardMaterialList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">
     <style>
        .select2-dropdown {
            z-index: 100002;
        }
    </style>
    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 1100px; margin-left: 10%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Inward Materials List</span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>

           

            <div class="Row">
                <div class="col-sm-6">
                    <asp:UpdatePanel ID="UpdatePanel26" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div1">
                                    <label class="control-label col-sm-4" id="lblMaterialType" style="text-align: left; font-weight: lighter">Material Type</label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlInwardMaterialType" CssClass="form-control SearchDropDown" data-size="5" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div3">
                            <label class="control-label col-sm-4" id="lblMaterialName" style="text-align: left; font-weight: lighter">Material Name</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtInWardSearchMaterialName" runat="server" CssClass="form-control" placeholder="Enter Material Name"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div2">
                            <label class="control-label col-sm-4" id="lblGP/DC/Invoice No" style="text-align: left; font-weight: lighter">GP/DC/Invoice No</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtInWardGPDCSearch" runat="server" CssClass="form-control" placeholder="Enter DC/Invoice No"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div8">
                            <label class="control-label col-sm-4" id="lblPartyName" style="text-align: left; font-weight: lighter">Name of the Supplier</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtInWardSupplierSearch" runat="server" CssClass="form-control" placeholder="Enter Name of the Supplier"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div5">
                                    <label class="control-label col-sm-4" id="lblDepartment" style="text-align: left; font-weight: lighter">Department</label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlInWardDepartmentSearch"  CssClass="form-control SearchDropDown" runat="server" AutoPostBack="true" data-size="5" OnSelectedIndexChanged="ddlInWardDepartmentSearch_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:TextBox ID="txtSearchDepartment" runat="server" CssClass="form-control" placeholder="Enter Department" maxlength="50" Visible="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlInWardDepartmentSearch" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>

                    
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div7">
                            <label class="control-label col-sm-4" id="lblFromDate" style="text-align: left; font-weight: lighter">From Date</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtInWardDateFromSearch" runat="server" CssClass="form-control" placeholder="Select From Date"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div9">
                            <label class="control-label col-sm-4" id="lblToDate" style="text-align: left; font-weight: lighter">To Date</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtInWardDateToSearch" runat="server" CssClass="form-control" placeholder="Select To Date"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                <asp:Button ID="btnInWardSearchFind" Text="Find" CssClass="button" runat="server" OnClick="btnInWardSearchFind_Click" ValidationGroup="ab" />
                <asp:Button ID="btnInWardSearchReset" Text="Reset" CssClass="button" runat="server" OnClick="btnInWardSearchReset_Click" CausesValidation="false" />
            </div>
            <div class="clearfix">
            </div>

            <asp:GridView ID="GridViewSearchInWard" runat="server" CssClass="table table-bordered table-inverse" PagerStyle-CssClass="GridPager" Style="text-align: center" HorizontalAlign="Center" text-align="Center" DataKeyNames="Sno" Font-Size="11px" HeaderStyle-Wrap="false" AutoGenerateColumns="False" HeaderStyle-Width="5%" EmptyDataText="Currently No Records Found" CellPadding="3" HeaderStyle-HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" AllowPaging="True" ForeColor="#66CCFF" OnPageIndexChanging="GridViewSearchInWard_PageIndexChanging">
                <EmptyDataRowStyle ForeColor="#FF3300" HorizontalAlign="Center" />
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#748CB2" Font-Bold="True" ForeColor="White" Height="30px" />
                <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="White" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#0c99f0" />
                <SortedAscendingHeaderStyle BackColor="#0c99f0" />
                <SortedDescendingCellStyle BackColor="#0c99f0" />
                <SortedDescendingHeaderStyle BackColor="#0c99f0" />
                <Columns>
                    <asp:TemplateField HeaderText="SNO" SortExpression="Sno">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVSno" runat="server" Text='<%# Bind("Sno") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSno" runat="server" Text='<%# Bind("Sno") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MATERIAL TYPE" SortExpression="InwardMaterialType">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVMaterialType" runat="server" Text='<%# Bind("InwardMaterialType") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGMaterialType" runat="server" Text='<%# Bind("InwardMaterialType") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MATERIAL NAME" SortExpression="MaterialName">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVMaterialName" runat="server" Text='<%# Bind("MaterialName") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGMaterialName" runat="server" Text='<%# Bind("MaterialName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="QUANTITY" SortExpression="Quantity1">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVQuantity" runat="server" Text='<%# Bind("Quantity1") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGQuantity" runat="server" Text='<%# Bind("Quantity1") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DEPARTMENT" SortExpression="DeptID">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="RECEVIED BY" SortExpression="ReceivedBy">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVMaterialINWD_ReceivedBy" runat="server" Text='<%# Bind("ReceivedBy") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVMaterialINWD_ReceivedBy" runat="server" Text='<%# Bind("ReceivedBy") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="VECHILE NUMBER" SortExpression="VechileNo">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVVechileNo" runat="server" Text='<%# Bind("VechileNo") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVechileNo" runat="server" Text='<%# Bind("VechileNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DC/INVOICE:NO" SortExpression="DCP_InvoiceNo">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVDCP_InvoiceNo" runat="server" Text='<%# Bind("DCP_InvoiceNo") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVDCP_InvoiceNo" runat="server" Text='<%# Bind("DCP_InvoiceNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NAME OF THE SUPPLIER" SortExpression="SupplierName">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVSupplierName" runat="server" Text='<%# Bind("SupplierName") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVSupplierName" runat="server" Text='<%# Bind("SupplierName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="IN DATETIME" SortExpression="InDateTime">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVInDateTime" runat="server" Text='<%# Bind("InDateTime") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVInDateTime" runat="server" Text='<%# Bind("InDateTime") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EDIT">
                        <ItemTemplate>
                            <asp:LinkButton ID="LnkEdit" runat="server" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" OnClick="LnkEdit_Click"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            <!--Panel to add new record-->
            <asp:ModalPopupExtender ID="modelpopup" runat="server" TargetControlID="btnInwardUpdate" PopupControlID="panelAddNew" RepositionMode="RepositionOnWindowResizeAndScroll" DropShadow="true" PopupDragHandleControlID="panelAddNewTitle" BackgroundCssClass="modalBackground"></asp:ModalPopupExtender>
            <asp:Panel ID="panelAddNew" runat="server" Style="display: none; background-color: white;" ForeColor="Black" Width="900" Height="450px">
                <asp:Panel ID="panelAddNewTitle" runat="server" Style="cursor: move; font-family: Tahoma; padding: 10px;" HorizontalAlign="Center" BackColor="#748CB2" BorderColor="#000000" Font-Bold="true" ForeColor="White" Height="35px">
                    <b>Modify Inward Material CheckOut </b>
                </asp:Panel>
                <div class="Row">
                    <div class="col-sm-6">
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div20">
                                <label class="control-label col-sm-4" id="lblSno" style="text-align: left; font-weight: lighter">SNo</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtEditSno" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel28" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div6">
                                        <label class="control-label col-sm-4" id="lblMaterialTypeEdit" style="text-align: left; font-weight: lighter">Material Type<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlInWardTypePO" CssClass="form-control" data-size="5" AutoPostBack="true" runat="server"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Select Material Type" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlInWardTypePO" InitialValue="0" ValidationGroup="Inward"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div10">
                                <label class="control-label col-sm-4" id="lblMaterialNameEdit" style="text-align: left; font-weight: lighter">Material Name<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtINWDMaterialName" runat="server" CssClass="form-control" placeholder="Enter Material Name"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Material Name" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtINWDMaterialName" ValidationGroup="Inward"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div11">
                                <label class="control-label col-sm-4" id="lblDes" style="text-align: left; font-weight: lighter">Description of the Material</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtINWDDescriptionofMat" runat="server" CssClass="form-control" placeholder="Enter Material Descrption" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div12">
                                        <label class="control-label col-sm-4" id="lblQuantity" style="text-align: left; font-weight: lighter">Quantity<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtINWDQuantity" runat="server" CssClass="form-control" placeholder="Quantity" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Quantity" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtINWDQuantity" ValidationGroup="Inward"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="col-sm-1" style="padding-left: 0px; padding-top: 5px">
                                            <label id="lblUnits" style="text-align: left; font-weight: lighter">Units<span class="mandatoryStar">*</span></label>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlUnits" runat="server" CssClass="form-control" data-size="5" AutoPostBack="true"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Select Units" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlUnits" InitialValue="0" ValidationGroup="Inward"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger  ControlID="ddlUnits" EventName="SelectedIndexChanged"/>
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div13">
                            <label class="control-label col-sm-4" id="lblGP" style="text-align: left; font-weight: lighter">DC/Invoice No</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtINWDDcNo" runat="server" CssClass="form-control" placeholder="Enter DC/Invoice No"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div14">
                            <label class="control-label col-sm-4" id="lblVechileNo" style="text-align: left; font-weight: lighter">Vechile No</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtINWDVechileNo" runat="server" CssClass="form-control" placeholder="Enter Vechile No"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <asp:UpdatePanel ID="UpdatePanel29" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div15">
                                    <label class="control-label col-sm-4" id="lblDepartmentEdit" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddlDepartment3" runat="server" ClientIDMode="Static" CssClass="form-control" AutoPostBack="true" data-size="5" OnSelectedIndexChanged="ddlDepartment3_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control" placeholder="Enter Department" maxlength="50"  Visible="false"></asp:TextBox>
                                       
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Select Department" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlDepartment3" InitialValue="0" ValidationGroup="Inward"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlDepartment3" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div16">
                                    <label class="control-label col-sm-4" id="lblReceivedby" style="text-align: left; font-weight: lighter">Received By<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddlReceivedBy" runat="server" ClientIDMode="Static" CssClass="form-control" data-size="5" AutoPostBack="true"></asp:DropDownList>
                                         <asp:TextBox ID="txtReceivedBy" runat="server" CssClass="form-control" placeholder="Enter ReceivedBy" onkeypress="return ValidateAlpha(event)" Visible="false"></asp:TextBox>
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Select Received By" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlReceivedBy" InitialValue="0" ValidationGroup="Inward"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlReceivedBy" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div17">
                            <label class="control-label col-sm-4" id="lblPartyNameEdit" style="text-align: left; font-weight: lighter">Name of the Supplier<span class="mandatoryStar">*</span></label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtINWDNameoftheSupplier" runat="server" CssClass="form-control" placeholder="Enter Supplier Name" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Enter Supplier Name" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtINWDNameoftheSupplier" ValidationGroup="Inward"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="DIVINWDINDate">
                            <label class="control-label col-sm-4" id="lblInDateTime" style="text-align: left; font-weight: lighter">InDateTime</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtINWDInDate" runat="server" BackColor="White" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div18">
                            <label class="control-label col-sm-4" id="lblAssessableValue" style="text-align: left; font-weight: lighter">Assessable Value</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtINWDAssessable" runat="server" CssClass="form-control" placeholder="Enter Assessable value"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div19">
                            <label class="control-label col-sm-4" id="lblRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtINWDRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div21">
                            <label class="control-label col-sm-4" id="lblComments" style="text-align: left; font-weight: lighter">Comments<span class="mandatoryStar">*</span></label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Enter Comments" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtComments" ValidationGroup="Inward"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                </div>
     
        <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
            <asp:Button ID="btnInwardUpdate" runat="server" CssClass="button" Text="Update" OnClick="btnInwardUpdate_Click" UseSubmitBehavior="false" ValidationGroup="Inward"/> <%--OnClientClick="javascript:return Submitvalidate();"--%>
            <asp:Button ID="btnCancel1" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"/>
        </div>
        </asp:Panel>
     
              <script>
                  //for start date and end date validations startdate should be < enddate
                  $(function () {
                      $('#<%=txtInWardDateFromSearch.ClientID%>').datetimepicker({
                  format: 'DD MMM YYYY',
                  useCurrent: false,
              });
              var startdate = document.getElementById('<%=txtInWardDateFromSearch.ClientID%>').value
                  $('#<%=txtInWardDateToSearch.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        minDate: startdate,
                        useCurrent: false,
                    });
                    $('#<%=txtInWardDateFromSearch.ClientID%>').on("dp.change", function (e) {
                        $('#<%=txtInWardDateToSearch.ClientID%>').data("DateTimePicker").minDate(e.date);
               <%--  $('#<%=txtEndDate.ClientID%>').value = $('#<%=txtStartDate.ClientID%>').value; --%>
                    $('#<%=txtInWardDateToSearch.ClientID%>').val("");
                    });
                    $('#<%=txtInWardDateToSearch.ClientID%>').on("dp.change", function (e) {
                  });
                  });
    </script>

           

      <script type="text/javascript">
          //for datepickers
          var startdate2 = document.getElementById('<%=txtInWardDateFromSearch.ClientID%>').value
          if (startdate2 == "") {
              $('#<%=txtInWardDateFromSearch.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                    });
                    $('#<%=txtInWardDateToSearch.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                    });
                }
    </script>
        <script>
            $("#NavLnkMaterial").attr("class", "active");
            $("#InwardMaterialList").attr("class", "active");
    </script>        

        </div>
    </div>
</asp:Content>
