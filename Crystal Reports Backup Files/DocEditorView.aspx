﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocEditorView.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DMSFileView.DocEditorView" %>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  <%--  <style>
         .dxreControl .dxreView {
            background-color: #E0E0E0;
            height: 1150px !important;
        }

        .dxreView {
            position: relative;
            overflow: auto;
            padding: 0 10px;
            -webkit-overflow-scrolling: touch;
            -webkit-appearance: none;
            -ms-touch-action: pan-y pinch-zoom;
            touch-action: pan-y pinch-zoom;
            height: 1150px !important;
        }
    </style>--%>
</head>
<body>
    <form id="form1" runat="server">
        <link href="<%=ResolveUrl("~/Content/bootstrap.css")%>" rel="stylesheet" />
        <script src="<%=ResolveUrl("~/Scripts/jquery-3.2.1.js")%>"></script>
        <script src="<%=ResolveUrl("~/Scripts/bootstrap.min.js")%>"></script>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
         <div class="bottom col-lg-9" id="WordUpload" runat="server" style="padding-top: 32px !important; padding-left: 10px;">
                                            <div class="col-lg-3 padding-none" id="lbl_Upload" runat="server"><b class="grid_icon_legend">Upload your word File:</b></div>
                                            <div class="col-lg-6 padding-none">
                                                <asp:FileUpload ID="file_word" runat="server" />
                                            </div>
                                            <asp:UpdatePanel ID="www" runat="server" UpdateMode="Conditional" style="text-align: right">
                                                <ContentTemplate>
                                                    <asp:Button ID="Btn_Upload" runat="server" Text="FileUpload" CssClass="btn btn-approve_popup" ValidationGroup="edit" OnClientClick="return Required();" OnClick="Btn_Upload_Click" />
<%--                                                    <asp:Button ID="Btn_TempUpload" runat="server" Text="DBUpload" CssClass="btn btn-approve_popup" ValidationGroup="edit" OnClientClick="return Required();" OnClick="Btn_TempUpload_Click" />
                                                    <asp:Button ID="Btn_HFUpload" runat="server" Text="HeadFootUpload" CssClass="btn btn-approve_popup" ValidationGroup="edit" OnClientClick="return Required();" OnClick="Btn_HFUpload_Click" />--%>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="Btn_Upload" />
<%--                                                    <asp:PostBackTrigger ControlID="Btn_TempUpload" />
                                                    <asp:PostBackTrigger ControlID="Btn_HFUpload" />--%>
                                                </Triggers>
                                                </asp:UpdatePanel>
             </div>
        <div>
             <dx:ASPxRichEdit ID="ASPxRichEdit2" runat="server" Width="100%" WorkDirectory="~\App_Data\WorkDirectory" ClientInstanceName="cr">
                                                <Settings>
                                                    <SpellChecker Enabled="true" Culture="en-US">
                                                        <OptionsSpelling IgnoreEmails="true" IgnoreUri="true" />
                                                    </SpellChecker>
                                                    <Behavior CreateNew="Hidden" Drag="Hidden" Drop="Hidden" Printing="Hidden" SaveAs="Hidden" />
                                                    <HorizontalRuler Visibility="Hidden" />
                                                    <AutoCorrect DetectUrls="True" ReplaceTextAsYouType="True"></AutoCorrect>
                                                    <RangePermissions Visibility="Auto"></RangePermissions>
                                                </Settings>
                                                <SettingsDocumentSelector UploadSettings-ValidationSettings-DisableHttpHandlerValidation="true" UploadSettings-Enabled="true" UploadSettings-AdvancedModeSettings-TemporaryFolder="~\App_Data\WorkDirectory">
                                                    <UploadSettings Enabled="True">
                                                        <AdvancedModeSettings TemporaryFolder="~\App_Data\WorkDirectory"></AdvancedModeSettings>
                                                    </UploadSettings>
                                                </SettingsDocumentSelector>
                                            </dx:ASPxRichEdit>
        </div>
         <div class="col-md-12 col-lg-6 col-xs-12 col-sm-12  pull-left padding-none top">
             <input type="button" class="pull-left btn btn-revert_popup" onclick="return ConfirmAlert();" value="Save Draft" />
             </div>
        <div class="col-md-12 col-lg-6 col-xs-12 col-sm-12  pull-left padding-none top"> 
            <input type="button" data-dismiss="modal" runat="server" id="btn_Cancel" class="pull-right btn btn-canceldms_popup" onclick="return ReloadCreatorPage();" value="Cancel" />
                                            <asp:UpdatePanel ID="upbns" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Button ID="btn_SubmittoReviewer" runat="server" Text="Submit" CssClass="pull-right btn btn-approve_popup" OnClientClick="return ConfirmAlert1();" ValidationGroup="edit" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>          
            </div>                                                                                 
        <asp:HiddenField ID="hdfPKID" runat="server" />
    </form>
</body>
</html>
