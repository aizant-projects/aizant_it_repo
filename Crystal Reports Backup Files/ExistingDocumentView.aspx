﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExistingDocumentView.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentCreator.ExistingDocumentView" %>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .dxreView {
            /*width: 989px !important;*/
            height: 604px !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <dx:ASPxRichEdit ID="ASPxRichEdit1" runat="server" Settings-Behavior-Copy="Disabled" Settings-DocumentCapabilities-Bookmarks="Disabled" Settings-Bookmarks-Visibility="Hidden" WorkDirectory="~/Files/" RibbonMode="None" ReadOnly="true" Width="100%">
                <Settings Behavior-Copy="Disabled" Behavior-Cut="Disabled" Behavior-Open="Hidden" Behavior-Printing="Hidden" Behavior-Download="Hidden" HorizontalRuler-Visibility="Hidden"
                    Behavior-CreateNew="Hidden" Behavior-SaveAs="Hidden" Behavior-AcceptsTab="false" HorizontalRuler-ShowTabs="false" DocumentCapabilities-Sections="Hidden"
                    Bookmarks-Visibility="Hidden">
                </Settings>
            </dx:ASPxRichEdit>
            <asp:Label ID="lblError" runat="server" Visible="false"></asp:Label>
        </div>
    </form>
</body>
</html>
