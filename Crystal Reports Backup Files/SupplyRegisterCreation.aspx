﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/VMS_Master.Master" AutoEventWireup="true" CodeBehind="SupplyRegisterCreation.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.SupplyRegister.SupplyRegisterCreation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 900px; margin-left: 17%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Supply Register Check In</span>
                    </div>
                    <div class="col-sm-7">
                    </div>
                </div>
            </div>
          
            <asp:UpdatePanel ID="UpdatePanel13" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="Row">
                        <div class="col-sm-6">

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div5">
                                    <label class="control-label col-sm-4" id="lblSupplyType" style="text-align: left; font-weight: lighter">Supply Type<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddlSupplyType" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" OnSelectedIndexChanged="ddlSupplyType_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div28">
                                            <label class="control-label col-sm-4" id="lblDepartment" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlDepartment"  runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlDepartment" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>

                            <asp:UpdatePanel runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div2">
                                            <label class="control-label col-sm-4" id="labelEmployeeName" style="text-align: left; font-weight: lighter">Employee Name<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlEmp" runat="server" CssClass="form-control SearchDropDown"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlEmp" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server">
                                    <label class="control-label col-sm-4" id="lblIssuedDateTime" style="text-align: left; font-weight: lighter">Issued DateTime</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtSnacksInDateTime" runat="server" BackColor="White" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="DIVIssuedItems">
                                    <label class="control-label col-sm-4" id="lblIssuedItems" style="text-align: left; font-weight: lighter">Issued Items<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtSnacksIssuedItems" runat="server" CssClass="form-control" placeholder="Enter Issued Items"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="DIVItemCost">
                                    <label class="control-label col-sm-4" id="lblItemsCost" style="text-align: left; font-weight: lighter">Item Cost</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtItemCost" runat="server" CssClass="form-control" placeholder="Enter Items Cost" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="DIVAreaoftheDept" visible="false">
                                    <label class="control-label col-sm-4" id="lblDepartmentArea" style="text-align: left; font-weight: lighter">Department Area<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtKeyIssueDepartmentArea" runat="server" CssClass="form-control" placeholder="Enter DepartmentArea"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="DIVAccessCardNo" visible="false">
                                    <label class="control-label col-sm-4" id="lbl" style="text-align: left; font-weight: lighter">AccessCardNo<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtAccessCardNo" runat="server" CssClass="form-control" placeholder="Enter AccessCard No"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="DIVNoofkeys" visible="false">
                                    <label class="control-label col-sm-4" id="lblNoofKeys" style="text-align: left; font-weight: lighter">No of Keys<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtKeyIssueNoofKeys" runat="server" CssClass="form-control" placeholder="Enter No of Keys" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;"></asp:TextBox>
                                                                           </div>
                                </div>
                            </div>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="DIVMobile" visible="false">
                                    <label class="control-label col-sm-4" id="lblMobileNo" style="text-align: left; font-weight: lighter">Mobile No</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control" placeholder="Enter Mobile No" MaxLength="10" onkeypress="return ValidatePhoneNo(event);"></asp:TextBox>
                                                                           </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div3">
                                    <label class="control-label col-sm-4" id="lblRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtSnacksRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   

                    <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                        <asp:Button ID="btnSubmit" Text="Submit" CssClass="button" runat="server" OnClientClick="javascript:return Submitvaidate();" OnClick="btnSubmit_Click" ValidationGroup="Supply"/>
                        <asp:Button ID="btnReset" Text="Reset" CssClass="button" runat="server" CausesValidation="false" OnClick="btnReset_Click" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>
   
    <script>
        function Submitvaidate() {
            var SupplyType = document.getElementById("<%=ddlSupplyType.ClientID%>");
            var IssuedItems = document.getElementById("<%=txtSnacksIssuedItems.ClientID%>");
            var Dept = document.getElementById("<%=ddlDepartment.ClientID%>");
            var EmpName = document.getElementById("<%=ddlEmp.ClientID%>");

            errors = [];
            if (SupplyType.value == "1") {

                
                if (Dept.value == "0") {
                    errors.push("Please Select Department.");
                }
                if (EmpName.value == 0) {
                    errors.push("Please Select Employee.");
                }
                if (IssuedItems.value.trim() == "") {
                    errors.push("Please Enter Issued Items.");
                }
            }
            if (SupplyType.value == "2") {

                if (Dept.value == "0") {
                    errors.push("Please Select Department.");
                }
                if (EmpName.value == 0) {
                    errors.push("Please Select Employee.");
                }
                var DeptArea = document.getElementById("<%=txtKeyIssueDepartmentArea.ClientID%>");
                if (DeptArea.value.trim() == "") {
                    errors.push("Please Enter Department Area.");
                }
                var NoofKeys = document.getElementById("<%=txtKeyIssueNoofKeys.ClientID%>");
                if (NoofKeys.value.trim() == "" || NoofKeys.value.trim() == 0) {
                     errors.push("Please Enter Issued No of Keys.");
                 }
            }

            if (SupplyType.value == "3") {
                var AccessCardNo = document.getElementById("<%=txtAccessCardNo.ClientID%>");
                if (Dept.value == "0") {
                    errors.push("Please Select Department.");
                }
                if (EmpName.value == 0) {
                    errors.push("Please Select Employee.");
                }
                if (AccessCardNo.value.trim() == "") {
                    errors.push("Please Enter AccessCard Number.");
                }
            }
            if (errors.length > 0) {
                alert(errors.join("\n"));
                return false;
            }
        }
    </script>
    <script>
        $("#NavLnkSupply").attr("class", "active");
        $("#SupplyRegister").attr("class", "active");
    </script>


</asp:Content>
