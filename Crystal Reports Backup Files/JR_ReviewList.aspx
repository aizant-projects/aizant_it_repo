﻿<%@ Page Title="JR Review" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="JR_ReviewList.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.JR.JR_ReviewList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hdnJR_ID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnEmpID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnNotificationID" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="col-12 padding-none" id="Div3" runat="server">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">JR Review List</div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top padding-none">
                    <table id="tblJR_ReviewList" class="datatable_cust tblJR_ReviewListClass display breakword" style="width: 100%">
                        <thead>
                            <tr>
                                <th>JR_ID</th>
                                <th>JR Name</th>
                                <th>Version</th>
                                <th>Emp.Code</th>
                                <th>Trainee Name</th>
                                <th>Dept.Code</th>
                                <th>Designation</th>
                                <th>Assigned Date</th>
                                <th>TAD</th>
                                <th>RevertedBy</th>
                                <th>Reverted Date</th>
                                <th>Author</th>
                                <th>Status</th>
                                <th>StatusID</th>
                                <th>View</th>
                            </tr>
                        </thead>
                    </table>
                    <div class="col-12 grid_legend_tms">TAD: Trainee Accepted Date</div>
                </div>
            </div>
        </div>
    </div>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnViewJR" runat="server" Text="ViewJR" OnClick="btnViewJR_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script>
        function ViewJR_ReviewList(JR_ID) {
            $("#<%=hdnJR_ID.ClientID%>").val(JR_ID);
            $("#<%=btnViewJR.ClientID%>").click();
        }
    </script>

    <!--JR_Pending List jQuery DataTable-->
    <script>
        function getUrlVars() {
            var vars = [], hash;
            var url = window.location.href;
            var hashes = url.slice(url.indexOf('?') + 1).split('&');
            if (url.indexOf("#") > 0) {
                var hashes = url.slice(url.indexOf('?') + 1, url.indexOf('#')).split('&');
            }

            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        var oTabletblJR_ReviewList = $('#tblJR_ReviewList').DataTable({
            columns: [
                { 'data': 'JR_ID' },
                { 'data': 'JR_Name' },
                { 'data': 'JR_Version' },
                { 'data': 'EmpCode' },
                { 'data': 'EmpName' },
                { 'data': 'DeptCode' },
                { 'data': 'DesignationName' },
                { 'data': 'HOD_AssignedDate' },
                { 'data': 'TraineeAcceptedDate' },
                { 'data': 'DeclinedBy' },
                { 'data': 'QARevertedDate' },
                { 'data': 'Approver' },
                { 'data': 'StatusName' },
                { 'data': 'StatusID' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="view"  href="#" onclick="ViewJR_ReviewList(' + o.JR_ID + ');"></a>'; }
                }
            ],
            "scrollY": "47vh",
            "autoWidth": false,
            "aoColumnDefs": [{ "targets": [0, 9, 10, 13], "visible": false }, { className: "textAlignLeft", "targets": [1, 4, 6, 9, 11, 12] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JR_ReviewListService.asmx/GetJR_ReviewList")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "FilterType", "value": getUrlVars()["FilterType"] }, { "name": "NotificationID", "value": <%=hdnNotificationID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblJR_ReviewList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        $(".tblJR_ReviewListClass").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });

    </script>

</asp:Content>
