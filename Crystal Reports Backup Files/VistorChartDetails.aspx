﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VistorChartDetails.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VisitorRegister.VistorChartDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="VMSBody" runat="server">    
    <style>
        table tbody tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            padding: 4px;
            border-right: 1px solid #fff;
        }

        /*.dxreControl .dxreView {
            background-color: #E0E0E0;
            height: 392px !important;
        }

        .dxreView {
            position: relative;
            overflow: auto;
            padding: 0 10px;
            -webkit-overflow-scrolling: touch;
            -webkit-appearance: none;
            -ms-touch-action: pan-y pinch-zoom;
            touch-action: pan-y pinch-zoom;
            height: 455px !important;
        }*/

        .CreatorGrid thead tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            padding: 4px;
            border-right: 1px solid #fff;
        }

        table tbody tr td {
            text-align: center;
            height: 30px;
        }

        .CreatorGrid tbody tr td {
            text-align: center;
            height: 30px;
            width: 20px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            background-color: #07889a !important;
        }
    </style>

    <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup top" PostBackUrl="~/VMS/VMS_HomePage.aspx" Text="Dashboard" />

    <div class="col-md-12 col-lg-12 col-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div class="col-md-12 col-lg-12 col-12 padding-none  float-left" id="divMainContainer" runat="server" visible="true">                       
                    <div class=" col-md-12 col-lg-12 col-sm-12 col-12 vms_outer_border float-left top padding-none">                     
                            <div class="col-md-12 col-lg-12 col-sm-12 col-12 grid_panel_full padding-none float-left">
                                <div class="  col-md-12 col-lg-12 col-sm-12 col-12 float-left padding-none">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 visitor_lists grid_header ">Visitor List</div>
                                </div>
                                <div class="col-md-12 col-lg-12 col-sm-12 col-12 grid_container top bottom float-left padding-none">
                                    <table id="dtVisitorList" class="tblVisitorRegister datatable_cust display" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>S.No</th><%--0--%>
                                                <th>S.No</th><%--1--%>
                                                <th>Visitor ID</th><%--2--%>
                                                <th>Visitor Name</th><%--3--%>
                                                <th>Mobile No.</th><%--4--%>
                                                <th>Department</th><%--5--%>
                                                <th>Whom To Visit</th><%--6--%>
                                                <th>Purpose of Visit</th><%--7--%>
                                                <th>Organization</th><%--8--%>
                                                <th>Check In Time</th><%--9--%>
                                        </thead>
                                    </table>
                                </div>
                        </div>
              </div>      
    </div>  
    <!--Visitor Jquery Datatable-->
    <script>
          //function visitordt() {
              $('#dtVisitorList').wrap('<div class="dataTables_scroll" />');
              var otable = $('#dtVisitorList').DataTable({
                  columns: [
                      { 'data': 'SNo' },//0
                      { 'data': 'RowNumber' },//1
                      { 'data': 'VisitorID' },//2
                      { 'data': 'VisitorName' },//3
                      { 'data': 'MobileNo' },//4
                      { 'data': 'DeptName' },//5
                      { 'data': 'EmpName' },//6
                      { 'data': 'PurposeofVisitTxt' },//7
                      { 'data': 'Organization' },//8
                      { 'data': 'InDateTime' },//9
                  ],
                 
                  "lengthMenu": [[10, 15, 25, 100], [10, 15, 25, 100]],
                  "aoColumnDefs": [{ "targets": [0, 1], "visible": false, "searchable": false }, { className: "dt-body-left", "targets": [2, 3, 5, 6, 7, 8] },],
                  "order": [[1, "desc"]],
                  'bAutoWidth': false,
                  "sServerMethod": 'Post',
                  "sAjaxSource": '<%= ResolveUrl("/VMS/WebService/VMS_Service.asmx/GetVisitorChartList" )%>',
                  "fnServerData": function (sSource, aoData, fnCallback) {
                      aoData.push({ "name": "FromDate", "value": getUrlVars()["FromDate"] }, { "name": "ToDate", "value": getUrlVars()["ToDate"] }, { "name": "Mode", "value": getUrlVars()["Mode"] }, { "name": "FromYear", "value": getUrlVars()["FromYear"] }, { "name": "ToYear", "value": getUrlVars()["ToYear"] }, { "name": "FromMonth", "value": getUrlVars()["FromMonth"] }, { "name": "ToMonth", "value": getUrlVars()["ToMonth"] }, { "name": "TimePeriod", "value": getUrlVars()["TimePeriod"] });
                      $.ajax({
                          "dataType": 'json',
                          "contentType": "application/json; charset=utf-8",
                          "type": "GET",
                          "url": sSource,
                          "data": aoData,
                          "success": function (msg) {
                              var json = jQuery.parseJSON(msg.d);
                              fnCallback(json);
                              $("#dtVisitorList").show();
                              $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                              $(".tblVisitorRegister").css({ "width": "100%" });
                          },
                          error: function (xhr, textStatus, error) {
                              if (typeof console == "object") {
                                  console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                              }
                          }
                      });
                  }
              });
          //}

        function ReloadVisitortable() {
            otable.ajax.reload();
        }
    </script>
    <!--End Visitor Jquery Datatable-->
</asp:Content>
