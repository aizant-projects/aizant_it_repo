﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewFile.aspx.cs" Inherits="AizantIT_PharmaApp.Common.ViewFile" %>

<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxSpreadsheet.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpreadsheet" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <dx:PanelContent runat="server" >
                   
<dx:ASPxSpreadsheet ID="Spreadsheet" runat="server" Height="550" ReadOnly="true" WorkDirectory="~/App_Data/WorkDirectory">
  
</dx:ASPxSpreadsheet>
                    <dx:ASPxRichEdit ID="RichEdit" runat="server" Height="550" ClientInstanceName="rich" Visible="false" ReadOnly="true" WorkDirectory="~\App_Data\WorkDirectory">
                        <Settings>
                   <Behavior Copy="Disabled" Download="Disabled" Cut="Disabled" Paste="Disabled" Printing="Disabled" AcceptsTab="false" Save="Disabled" FullScreen="Enabled" />
               </Settings>
                    </dx:ASPxRichEdit>
                    <dx:ASPxImage runat="server" ID="Image" Height="520"  Visible="false" ReadOnly="true" >
                         
                    </dx:ASPxImage>                    
                    <dx:ASPxSplitter ID="Splitter" Visible="false"  runat="server"   Height="550" ClientInstanceName="splitter" ReadOnly="true" >
                        <Panes>
                            <dx:SplitterPane>
                                <ContentCollection>
                                    <dx:SplitterContentControl ID="SplitterContentControl1" runat="server" SupportsDisabledAttribute="True"></dx:SplitterContentControl>
                                </ContentCollection>
                            </dx:SplitterPane>
                        </Panes>
                       
                    </dx:ASPxSplitter>
                </dx:PanelContent>
        </div>
    </form>
</body>
</html>
