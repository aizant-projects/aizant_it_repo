﻿<%@ Page Title="JR TS Master" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="AssignTS.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TS.AssignTS1" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script src="<%= ResolveUrl("~/AppScripts/TableCheckBoxSelection.min.js")%>"></script>
    <style>
        [type="checkbox"], [type="radio"] {
            height: 15px;
            width: 20px;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            padding: 0;
        }

        .table-fixed_custom tbody {
            max-height: 189px !important;
        }

        .gvTextAlignCenter {
            text-align: center;
        }

        .gvRowStyle {
            text-align: left;
        }

        .gvHeaderStyle {
            /*background-color: #efefef !important;*/
        }

        .gvItemStyleCenter {
            text-align: center;
        }

        .InfoText {
            margin: 2px 0px;
            padding: 3px;
            font-weight: bold;
            background-color: #098293;
            color: white;
            font-family: 'Segoe UI';
        }

        .tableArea {
            overflow: auto;
            height: 244px;
            border: 1px solid #d4d4d4;
        }

        .mainFormButtons {
            text-align: center;
            margin: 5px auto;
        }

        .arrowButtons {
            height: 300px;
            padding-top: 100px;
            padding-left: 25px;
        }

        .rightArrowButton {
            font-weight: bold;
            border-radius: 0px;
            border-top-right-radius: 8px;
            border-bottom-right-radius: 8px;
        }

        .leftArrowButton {
            font-weight: bold;
            border-radius: 0px;
            border-top-left-radius: 8px;
            border-bottom-left-radius: 8px;
        }

        .custColSm5 {
            width: 45%;
            float: left;
        }

        .custColSm5_right {
            width: 45%;
            float: right;
        }

        .day_selcetion {
            width: 7%;
            float: left;
            margin-left: 26px;
            margin-top: 16px;
        }

    </style>
    <asp:HiddenField ID="hdnJR_ID" runat="server" />
    <asp:HiddenField ID="hdnTS_ID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTraineeID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnPrevipusPageURL" runat="server" Value="0" />
    <asp:HiddenField ID="hdnActionPurpose" runat="server" Value="0" />
    <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnAction" runat="server" Value="0" />
            <asp:HiddenField ID="hdnDocumentsCountForValidation" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>

    <asp:Label ID="lblError" runat="server" Text="" EnableViewState="false" ForeColor="Red" Font-Bold="true"></asp:Label>
    <asp:HyperLink ID="hlBackToList" Visible="false" CssClass="btn btn-dashboard pull-right" runat="server">Back To List</asp:HyperLink>
    <div class="float-left col-lg-12 col-md-12 col-sm-12 col-12 grid_panel_full padding-none">
        <div id="divMainContainer" runat="server" visible="true">

            <div class="float-left col-lg-12 padding-none" id="mainContainer" runat="server">
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_header">
                    <asp:UpdatePanel ID="upTitle" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Label ID="lblTitle" runat="server" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="">
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none bottom">
                        <div class="form-horizontal col-sm-12 padding-none">
                            <div class=" float-left col-lg-4 col-sm-12 col-12 col-md-4 padding-none">
                                <label class="control-label float-left col-lg-12 col-sm-12 col-12 col-md-12">Trainee Department <span style="color: red; font: x-large"></span></label>
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                    <asp:UpdatePanel ID="upEmpDeptDdl" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlEmpDepartments" Enabled="false" runat="server"
                                                ClientIDMode="static" CssClass="form-control login_input_sign_up  SearchDropDown"
                                                Width="100%">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class=" float-left col-lg-4 col-sm-12 col-12 col-md-4 padding-none">
                                <label class="control-label float-left col-lg-12 col-sm-12 col-12 col-md-12"><span style="font: x-large">Trainee </span></label>
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                    <asp:TextBox ID="txtTraineeName" runat="server" class="form-control login_input_sign_up" ReadOnly="true" Style="cursor: not-allowed;"></asp:TextBox>
                                </div>
                            </div>
                            <div class=" float-left col-lg-4 col-sm-12 col-12 col-md-4 padding-none">
                                <label class="control-label float-left col-lg-12 col-sm-12 col-12 col-md-12">Designation</label>
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                    <asp:UpdatePanel ID="upDesignationDdl" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtDesignation" class="form-control login_input_sign_up" runat="server" ReadOnly="true" Style="cursor: not-allowed;"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="upDepartmentsDdl" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class=" float-left col-lg-4 col-sm-12 col-12 col-md-4 padding-none">
                                        <label class="control-label float-left col-lg-12 col-sm-12 col-12 col-md-12">Select Training Department<span style="color: red; font: x-large">*</span></label>
                                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                            <asp:DropDownList ID="ddlTrainingDept" runat="server" ClientIDMode="static"
                                                CssClass="selectpicker regulatory_dropdown_style 
                                                 form-control"
                                                data-live-search="true" data-size="7"
                                                OnSelectedIndexChanged="ddlTrainingDept_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class=" float-left col-lg-4 col-sm-12 col-12 col-md-4 padding-none" runat="server" id="divReviewer">
                                        <label class="control-label float-left col-lg-12 col-sm-12 col-12 col-md-12">Select Reviewer<span style="color: red; font: x-large">*</span></label>
                                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                            <asp:DropDownList ID="ddlReviewer" runat="server"
                                                AutoPostBack="true" data-live-search="true" data-size="7"
                                                CssClass="selectpicker regulatory_dropdown_style form-control" TabIndex="2">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class=" float-left col-lg-4 col-sm-12 col-12 col-md-4 padding-none" runat="server" id="divAuthor" visible="false">
                                        <label class="control-label float-left col-lg-12 col-sm-12 col-12 col-md-12">Author</label>
                                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                            <asp:TextBox ID="txtAuthor" class="form-control login_input_sign_up" runat="server" placeholder="" Width="100%" Style="cursor: not-allowed;" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlTrainingDept" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>

                            <div class=" float-left col-lg-4 col-sm-12 col-12 col-md-4 padding-none">
                                <label class="control-label float-left col-lg-12 col-sm-12 col-12 col-md-12">JR Name</label>
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                    <asp:UpdatePanel ID="upJrName" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtJrName" class="form-control login_input_sign_up" runat="server" placeholder="" Width="100%" ReadOnly="true" Style="cursor: not-allowed;"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div id="divTrainingScheduleAssign" style="display: none">
                <div>
                    <div>
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TabArea">
                            <ul class="nav nav-tabs tab_grid">
                                <li class=" nav-item "><a data-toggle="tab" class="nav-link active" href="#Regular">Regular</a></li>
                                <li class=" nav-item "><a data-toggle="tab" class="nav-link " href="#Retrospective">Retrospective</a></li>
                                <li class=" nav-item "><a data-toggle="tab" class="nav-link " href="#Comments_History">History</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="Regular" class="tab-pane fade in show active">
                                    <div>
                                        <div class=" custColSm5 padding-none">
                                            <div id="divAvailableTrainers">
                                                <div class="table-responsive">
                                                    <div class="row" style="margin: 0px; padding: 0px">
                                                        <div class="grid_header top col-12  ">
                                                            <span>Available Trainer(s)</span>
                                                            <input name="txtTerm" autocomplete="off" type="search" class="col-6 gridSearchBox float-right" readonly="readonly" onfocus="this.removeAttribute('readonly');" style="margin-top: -3px" oninput="GridViewSearch(this, '<%=gvAvailableTrainers.ClientID %>')" placeholder="Search Available Trainers" />
                                                        </div>
                                                    </div>
                                                    <asp:UpdatePanel ID="upGvAvailableTrainers" runat="server" UpdateMode="Always" class="tableArea">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gvAvailableTrainers" runat="server"
                                                                CssClass="table table-fixed table-fixed_custom col-12 float-left fixHeadTrainer   AspGrid" ShowHeaderWhenEmpty="false"
                                                                EmptyDataText=" No Available Trainers " AutoGenerateColumns="false"
                                                                EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                                <RowStyle CssClass="col-12 float-left gvRowStyle padding-none" />
                                                                <HeaderStyle CssClass="gvHeaderStyle col-12 float-left padding-none" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-CssClass="col-1 float-left" ItemStyle-CssClass="col-1 float-left padding-none">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox runat="server" onclick="chkHeaderSelect(this);" ID="chkHeaderElement" CssClass="chkHeaderElement"></asp:CheckBox>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox runat="server" onclick="chkRowSelect(this);" ID="chkRow" CssClass="chkRowElement"></asp:CheckBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Employee ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("EmpID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Emp. Code" HeaderStyle-CssClass="col-3 float-left" ItemStyle-CssClass="col-3 float-left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEMPCode" runat="server" Text='<%# Bind("EmpCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Trainer Name" HeaderStyle-CssClass="col-8 float-left" ItemStyle-CssClass="col-8 float-left textAlignLeft">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEMPName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Department ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDeptID" runat="server" Text='<%# Bind("DeptID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                            <div id="divAvailableSOPs">
                                                <div class="table-responsive">
                                                    <div class="row" style="margin: 0px; padding: 0px">
                                                        <div class="grid_header top col-12">
                                                            <span>Available Document(s)</span>
                                                            <input name="txtTerm" type="search" autocomplete="off" class="col-6 gridSearchBox float-right" readonly="readonly" onfocus="this.removeAttribute('readonly');" style="margin-top: -3px" oninput="GridViewSearch(this, '<%=gvAvailableDocs.ClientID %>')" placeholder="Search Available Docs" />
                                                        </div>
                                                    </div>
                                                    <asp:UpdatePanel ID="upGvAvailableSOPs" runat="server" UpdateMode="Always" class="tableArea">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gvAvailableDocs" runat="server"
                                                                CssClass="table table-fixed table-fixed_custom col-12 float-left fixHeadSOPs AspGrid" ShowHeaderWhenEmpty="false"
                                                                EmptyDataText="No Available Doc(s) " AutoGenerateColumns="False"
                                                                EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                                <RowStyle CssClass="col-12 float-left gvRowStyle padding-none" />
                                                                <HeaderStyle CssClass="col-12 float-left gvHeaderStyle" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-CssClass="col-1 float-left" ItemStyle-CssClass="col-1 float-left">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox runat="server" onclick="chkHeaderSelect(this);" ID="chkHeaderElement" CssClass="chkHeaderElement"></asp:CheckBox>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox runat="server" onclick="chkRowSelect(this);" ID="chkRow" CssClass="chkRowElement"></asp:CheckBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="SOP ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSopID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="SOP Version ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSopVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Document Name" HeaderStyle-CssClass="col-6 float-left" ItemStyle-CssClass="col-6 float-left textAlignLeft">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSOP_Description" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Questions" HeaderStyle-CssClass="col-2 float-left" ItemStyle-CssClass="col-2 float-left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblQuestions" runat="server" Text='<%# Bind("Questions") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Doc.Type" HeaderStyle-CssClass="col-3 float-left" ItemStyle-CssClass="col-3 float-left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocumentType" runat="server" Text='<%# Bind("DocumentType") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Department ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDeptID" runat="server" Text='<%# Bind("DeptID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="day_selcetion" style="text-align: center">
                                            <div class="form-group">
                                                <label>Days<span style="color: red; font: x-large">*</span></label>
                                                <asp:UpdatePanel ID="upTxtDuration" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtDuration" class="form-control" runat="server" placeholder="No of days"
                                                            onkeypress="return AllowNonDecimalNumbers(event);" MaxLength="6" onpaste="return false" autocomplete="off"></asp:TextBox>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="arrowButtons">
                                                <asp:UpdatePanel ID="upBtnAddRemove" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <table>
                                                            <tr>
                                                                <td style="padding-bottom: 12px;">
                                                                    <asp:LinkButton ID="lnkBtnAdd" OnClick="lnkBtnAdd_Click"
                                                                        CssClass="btn btn-signup_popup rightArrowButton" runat="server"
                                                                        OnClientClick="return validateAssign();">
                                                            <span class="glyphicon glyphicon-chevron-right"></span></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkBtnRemove" OnClick="lnkBtnRemove_Click"
                                                                        CssClass="btn btn-signup_popup leftArrowButton" runat="server"
                                                                        OnClientClick="return validateRemoveAssigned();">
                                                        <span class="glyphicon glyphicon-chevron-left"></span></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </div>
                                        </div>
                                        <div class="custColSm5_right padding-none">
                                            <div id="divAssignedTrainers">
                                                <div class="table-responsive">
                                                    <div>
                                                        <div class="grid_header top col-12 ">
                                                            <span>Assigned Trainer(s)</span>
                                                            <input name="txtTerm" autocomplete="off" type="search" class="col-6 gridSearchBox float-right" readonly="readonly" onfocus="this.removeAttribute('readonly');" style="margin-top: -3px" oninput="GridViewSearch(this, '<%=gvAssignedTrainers.ClientID %>')" placeholder="Search Assigned Trainers" />
                                                        </div>
                                                    </div>
                                                    <asp:UpdatePanel ID="upGvAssignedTrainers" runat="server" UpdateMode="Always" class="tableArea">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gvAssignedTrainers" runat="server"
                                                                EmptyDataText="No Assigned Trainers " AutoGenerateColumns="false"
                                                                CssClass="table table-fixed table-fixed_custom col-12 float-left fixHeadAssignedTrainers AspGrid" ShowHeaderWhenEmpty="false"
                                                                EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                                <RowStyle CssClass="col-12 gvRowStyle float-left padding-none" />
                                                                <HeaderStyle CssClass="gvHeaderStyle col-12 float-left" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-CssClass="col-1 float-left" ItemStyle-CssClass="col-1 float-left">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox runat="server" onclick="chkHeaderSelect(this);" ID="chkHeaderElement" CssClass="chkHeaderElement"></asp:CheckBox>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox runat="server" onclick="chkRowSelect(this);" ID="chkRow" CssClass="chkRowElement"></asp:CheckBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Employee ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("EmpID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Emp. Code" HeaderStyle-CssClass="col-3 float-left" ItemStyle-CssClass="col-3 float-left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEMPCode" runat="server" Text='<%# Bind("EmpCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Trainer Name" HeaderStyle-CssClass="col-8 float-left" ItemStyle-CssClass="col-8 float-left textAlignLeft">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEMPName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Department ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDeptID" runat="server" Text='<%# Bind("DeptID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>

                                            <div id="divAssignedSOPs">
                                                <div class="table-responsive">
                                                    <div class="row" style="margin: 0px; padding: 0px">
                                                        <div class="grid_header top col-12">
                                                            <span>Assigned Document(s)</span>
                                                            <input name="txtTerm" autocomplete="off" type="search" class="col-6 gridSearchBox float-right" readonly="readonly" onfocus="this.removeAttribute('readonly');" style="margin-top: -3px" oninput="GridViewSearch(this, '<%=gvAssignedDocs.ClientID %>')" placeholder="Search Assigned Docs" />
                                                        </div>
                                                    </div>
                                                    <asp:UpdatePanel ID="upGvAssignedSOPs" runat="server" UpdateMode="Always" class="tableArea">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gvAssignedDocs" runat="server"
                                                                EmptyDataText="No Assigned Doc(s)" AutoGenerateColumns="false"
                                                                CssClass="table table-fixed table-fixed_custom fixHeadAssignedSOPs AspGrid col-12 float-left" ShowHeaderWhenEmpty="false"
                                                                EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                                <RowStyle CssClass="col-12 gvRowStyle padding-none float-left" />
                                                                <HeaderStyle CssClass="gvHeaderStyle col-12 float-left" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-CssClass="col-1 float-left" ItemStyle-CssClass="col-1 float-left">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox runat="server" onclick="chkHeaderSelect(this);" ID="chkHeaderElement" CssClass="chkHeaderElement"></asp:CheckBox>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox runat="server" onclick="chkRowSelect(this);" ID="chkRow" CssClass="chkRowElement"></asp:CheckBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="SOP ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSopID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="SOP Version ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSopVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Document Name" HeaderStyle-CssClass="col-6 float-left" ItemStyle-CssClass="col-6 textAlignLeft float-left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSOP_Description" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Questions" HeaderStyle-CssClass="col-2 float-left" ItemStyle-CssClass="col-2 float-left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblQuestions" runat="server" Text='<%# Bind("Questions") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Doc.Type" HeaderStyle-CssClass="col-3" ItemStyle-CssClass="col-3">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocumentType" runat="server" Text='<%# Bind("DocumentType") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Department ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDeptID" runat="server" Text='<%# Bind("DeptID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="Retrospective" class="tab-pane fade">
                                    <div style="overflow: auto;">
                                        <div class=" custColSm5">
                                            <div id="divRetAvailableSOPs">
                                                <div class="table-responsive">
                                                    <div class="row" style="margin: 0px; padding: 0px">
                                                        <div class="grid_header top col-12">
                                                            <span>Available Document(s)</span>
                                                            <input name="txtTerm" autocomplete="off" type="search" class="col-6 gridSearchBox float-right" readonly="readonly" onfocus="this.removeAttribute('readonly');" style="margin-top: -3px" oninput="GridViewSearch(this, '<%=gvRetAvailableDocs.ClientID %>')" placeholder="Search Available Docs" />
                                                        </div>
                                                    </div>
                                                    <asp:UpdatePanel ID="upgvRetAvailableSOPs" runat="server" UpdateMode="Always" class="tableArea">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gvRetAvailableDocs" runat="server"
                                                                CssClass="table table-fixed table-fixed_custom fixHeadAvailableSOPs col-12 float-left AspGrid" ShowHeaderWhenEmpty="false"
                                                                EmptyDataText="No Available Doc(s) " AutoGenerateColumns="False"
                                                                EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                                <RowStyle CssClass="col-12 float-left gvRowStyle padding-none" />
                                                                <HeaderStyle CssClass="col-12 float-left gvHeaderStyle" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-CssClass="col-1 float-left" ItemStyle-CssClass="col-1 float-left">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox runat="server" onclick="chkHeaderSelect(this);" ID="chkHeaderElement" CssClass="chkHeaderElement"></asp:CheckBox>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox runat="server" onclick="chkRowSelect(this);" ID="chkRow" CssClass="chkRowElement"></asp:CheckBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="SOP ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSopID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="SOP Version ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSopVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Document Name" HeaderStyle-CssClass="col-8 float-left" ItemStyle-CssClass="col-8 float-left textAlignLeft">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSOP_Description" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Doc.Type" HeaderStyle-CssClass="col-3 float-left" ItemStyle-CssClass="col-3 float-left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocumentType" runat="server" Text='<%# Bind("DocumentType") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Department ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDeptID" runat="server" Text='<%# Bind("DeptID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=" day_selcetion arrowButtons">
                                            <asp:UpdatePanel ID="upRetBtnAddRemove" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table>
                                                        <tr>
                                                            <td style="padding-bottom: 10px">
                                                                <asp:LinkButton ID="lnkRetBtnAdd" OnClick="lnkRetBtnAdd_Click"
                                                                    CssClass="btn-signup_popup rightArrowButton" runat="server"
                                                                    OnClientClick="return validateRetAssign();">
                                                        <span class="glyphicon glyphicon-chevron-right"></span></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:LinkButton ID="lnkRetBtnRemove" OnClick="lnkRetBtnRemove_Click"
                                                                    CssClass="btn-signup_popup leftArrowButton" runat="server"
                                                                    OnClientClick="return validateRetRemoveAssigned();">
                                                        <span class="glyphicon glyphicon-chevron-left"></span></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div class=" custColSm5_right">
                                            <div id="divRetAssignedSOPs">
                                                <div class="table-responsive">
                                                    <div class="row" style="margin: 0px; padding: 0px">
                                                        <div class="grid_header top col-12">
                                                            <span>Assigned Document(s)</span>
                                                            <input name="txtTerm" autocomplete="off" type="search" class="col-6 gridSearchBox float-right" readonly="readonly" onfocus="this.removeAttribute('readonly');" style="margin-top: -3px" oninput="GridViewSearch(this, '<%=gvRetAssignedDocs.ClientID %>')" placeholder="Search Assigned Docs">
                                                        </div>
                                                    </div>
                                                    <asp:UpdatePanel ID="upgvRetAssignedSOPs" runat="server" UpdateMode="Always" class="tableArea">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gvRetAssignedDocs" runat="server"
                                                                EmptyDataText="No Assigned Doc(s)" AutoGenerateColumns="false"
                                                                CssClass="table table-fixed table-fixed_custom fixHeadAssignSOPs AspGrid col-12 float-left" ShowHeaderWhenEmpty="false"
                                                                EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                                <RowStyle CssClass="col-12 float-left gvRowStyle padding-none" />
                                                                <HeaderStyle CssClass="col-12 float-left gvHeaderStyle" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-CssClass="col-1 float-left" ItemStyle-CssClass="col-1 float-left">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox runat="server" onclick="chkHeaderSelect(this);" ID="chkHeaderElement" CssClass="chkHeaderElement"></asp:CheckBox>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox runat="server" onclick="chkRowSelect(this);" ID="chkRow" CssClass="chkRowElement"></asp:CheckBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="SOP ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSopID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="SOP Version ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSopVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Document Name" HeaderStyle-CssClass="col-8 float-left" ItemStyle-CssClass="col-8 float-left textAlignLeft">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSOP_Description" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Doc.Type" HeaderStyle-CssClass="col-3 float-left " ItemStyle-CssClass="col-3 float-left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocumentType" runat="server" Text='<%# Bind("DocumentType") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Department ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDeptID" runat="server" Text='<%# Bind("DeptID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="Comments_History" class="tab-pane fade">
                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top padding-none">
                                        <table id="tblJrTS_ActionHistory" class="datatable_cust tblJrTS_ActionHistoryClass display breakword" style="width: 100%">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>Action By</th>
                                                    <th>Role</th>
                                                    <th>Action Date</th>
                                                    <th>Remarks</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none top bottom" style="border-top: 1px solid #d4d4d4; border-bottom: 1px solid #d4d4d4;">
                            <div class="form-horizontal col-sm-6 padding-none float-left" id="divRevertReason" visible="false" runat="server">
                                <div class="form-group">
                                    <label class="control-label float-left col-lg-12 col-sm-12 col-12 col-md-12">QA Remarks</label>
                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                        <asp:TextBox ID="txtRevertReason" runat="server" Enabled="false" CssClass="form-control" MaxLength="250" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal col-sm-6 padding-none float-left" id="divHOD_Remarks" visible="false" runat="server">
                                <div class="form-group">
                                    <label class="control-label float-left col-lg-12 col-sm-12 col-12 col-md-12">Training Coordinator Remarks</label>
                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                        <asp:TextBox ID="txtHOD_Remarks" runat="server" CssClass="form-control" TextMode="MultiLine" MaxLength="250" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom">
                        <div class="pull-right">
                            <div id="divButtons" runat="server">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnSubmitTS" runat="server"
                                            Text="Submit" CssClass=" btn-signup_popup " OnClick="btnSubmitTS_Click"
                                            OnClientClick="return validateSubmitTS();" />
                                        <asp:Button ID="btnCancelTS" runat="server"
                                            Text="Cancel" CssClass=" btn-revert_popup" OnClientClick="return validateCancelTS();" OnClick="btnCancelTS_Click" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="divSelectDeptNote" class="float-left col-lg-12 traning_dept_select">
                Select Training Department
            </div>
        </div>
    </div>

    <%--added for electronic Sign--%>
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />


    <script>
        function openElectronicSignModal() {
            onDepartmentChange();
            openUC_ElectronicSign();
        }
        function focusOnRevertreason() {
            onDepartmentChange();
            document.getElementById('<%=txtRevertReason.ClientID%>').focus();
        }
    </script>

    <script>
        function CloseESign() {
            $('#usES_Modal').modal('hide');
        }
    </script>

    <!--JrTS_Action  History jQuery DataTable-->
    <script>
        var oTableJrTS_ActionHistory;
        function loadJrTS_ActionHistory() {
            if (oTableJrTS_ActionHistory != null) {
                oTableJrTS_ActionHistory.destroy();
            }
            oTableJrTS_ActionHistory = $('#tblJrTS_ActionHistory').DataTable({
                columns: [
                    { 'data': 'ActionStatus' },
                    { 'data': 'ActionBy' },
                    { 'data': 'ActionRole' },
                    { 'data': 'ActionDate' },
                    { 'data': 'Remarks' }
                ],
                 "order": [[0]],
                "scrollY": "250px",
                "aoColumnDefs": [{ className: "textAlignLeft", "targets": [0, 1, 2, 4] }, { "orderable": false,"targets": [0, 1, 2,3, 4] }],
                <%--"sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTsActionHistoryService.asmx/GetJrTS_ActionHistory")%>",--%>
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TMS_ActionHistoryService.asmx/GetTMS_ActionHistory")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                  aoData.push({ "name": "Base_ID", "value": $('#<%=hdnTS_ID.ClientID%>').val() }, { "name": "Table_ID", "value": "3" });
                    <%-- aoData.push({ "name": "TS_ID", "value": <%=hdnTS_ID.Value%> });--%>
                     $.ajax({
                         "dataType": 'json',
                         "contentType": "application/json; charset=utf-8",
                         "type": "GET",
                         "url": sSource,
                         "data": aoData,
                         "success": function (msg) {
                             var json = jQuery.parseJSON(msg.d);
                             fnCallback(json);
                             UserSessionCheck();
                             $("#tblJrTS_ActionHistory").show();
                             $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                             $(".tblJrTS_ActionHistoryClass").css({ "width": "100%" });
                         },
                         error: function (xhr, textStatus, error) {
                             if (typeof console == "object") {
                                 console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                             }
                         }
                     });
                 }
             });
         }
    </script>

    <script type="text/javascript">
         $(document).ready(function () {
             // Fix up GridView to support THEAD tags            
             fixGvHeader();
             fixHeader();
             fixHeaderCommentry();
             fixHeaderAssign();
             addThead();
             AssignedHead();
             addTheadSops();

         });
         var prm = Sys.WebForms.PageRequestManager.getInstance();
         prm.add_endRequest(function () {
             fixGvHeader();
             fixHeader();
             fixHeaderCommentry();
             fixHeaderAssign();
             addThead();
             AssignedHead();
             addTheadSops();
         });

         function fixGvHeader() {
             var tbl = document.getElementsByClassName("fixHeadTrainer");
             // Fix up GridView to support THEAD tags     
             if ($(".fixHeadTrainer").find("thead").length == 0) {
                 $(".fixHeadTrainer tbody").before("<thead><tr></tr></thead>");
                 $(".fixHeadTrainer thead tr").append($(".fixHeadTrainer th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHeadTrainer tbody tr:first").remove();
                 }
             }
         }
         function fixHeader() {
             var tbl = document.getElementsByClassName("fixHeadAssignedTrainers");
             // Fix up GridView to support THEAD tags   
             if ($(".fixHeadAssignedTrainers").find("thead").length == 0) {
                 $(".fixHeadAssignedTrainers tbody").before("<thead><tr></tr></thead>");
                 $(".fixHeadAssignedTrainers thead tr").append($(".fixHeadAssignedTrainers th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHeadAssignedTrainers tbody tr:first").remove();
                 }
             }
         }
         function fixHeaderCommentry() {
             var tbl = document.getElementsByClassName("fixHeadCommentry");
             // Fix up GridView to support THEAD tags 
             if ($(".fixHeadCommentry").find("thead").length == 0) {
                 $(".fixHeadCommentry tbody").before("<thead><tr></tr></thead>");
                 $(".fixHeadCommentry thead tr").append($(".fixHeadCommentry th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHeadCommentry tbody tr:first").remove();
                 }
             }
         }
         function fixHeaderAssign() {
             var tbl = document.getElementsByClassName("fixHeadAssignSOPs");
             // Fix up GridView to support THEAD tags   
             if ($(".fixHeadAssignSOPs").find("thead").length == 0) {
                 $(".fixHeadAssignSOPs tbody").before("<thead><tr></tr></thead>");
                 $(".fixHeadAssignSOPs thead tr").append($(".fixHeadAssignSOPs th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHeadAssignSOPs tbody tr:first").remove();
                 }
             }
         }
         function addThead() {
             var tbl = document.getElementsByClassName("fixHeadAvailableSOPs");
             // Fix up GridView to support THEAD tags 
             if ($(".fixHeadAvailableSOPs").find("thead").length == 0) {
                 $(".fixHeadAvailableSOPs tbody").before("<thead><tr></tr></thead>");
                 $(".fixHeadAvailableSOPs thead tr").append($(".fixHeadAvailableSOPs th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHeadAvailableSOPs tbody tr:first").remove();
                 }
             }
         }
         function AssignedHead() {
             var tbl = document.getElementsByClassName("fixHeadAssignedSOPs");
             // Fix up GridView to support THEAD tags       
             if ($(".fixHeadAssignedSOPs").find("thead").length == 0) {
                 $(".fixHeadAssignedSOPs tbody").before("<thead><tr></tr></thead>");
                 $(".fixHeadAssignedSOPs thead tr").append($(".fixHeadAssignedSOPs th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHeadAssignedSOPs tbody tr:first").remove();
                 }
             }
         }
         function addTheadSops() {
             var tbl = document.getElementsByClassName("fixHeadSOPs");
             // Fix up GridView to support THEAD tags
             if ($(".fixHeadSOPs").find("thead").length == 0) {
                 $(".fixHeadSOPs tbody").before("<thead><tr></tr></thead>");
                 $(".fixHeadSOPs thead tr").append($(".fixHeadSOPs th"));
                 var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                 if (rows.length == 0) {
                     $(".fixHeadSOPs tbody tr:first").remove();
                 }
             }
         }
    </script>

    <script>
         function validateRemoveAssigned() {
             errors = [];
             if (document.getElementById("<%=ddlTrainingDept.ClientID%>").value == 0) {
                errors.push("Select Training Department");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
         }
    </script>

    <script>
        function validateAssign() {
            errors = [];

            if (document.getElementById('<%=ddlEmpDepartments.ClientID%>').value == 0) {
                errors.push("Select Department");
            }
            if (document.getElementById('<%=txtJrName.ClientID%>').value == "") {
                errors.push("Enter JR Name");
            }
            if (document.getElementById("<%=ddlTrainingDept.ClientID%>").value == 0) {
                errors.push("Select Training Department");
            }
            if (document.getElementById('<%=txtDesignation.ClientID%>').value == "") {
                errors.push("Enter Designation");
            }
            if (document.getElementById('<%=txtDuration.ClientID%>').value == "") {
                errors.push("Enter No of Days");
            }
            else {
                if (parseFloat(document.getElementById('<%=txtDuration.ClientID%>').value) == 0) {
                    errors.push("No of Days Shouldn\'t be zero.");
                }
            }

            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
            return true;
        }
    </script>

    <script>
        function validateRetAssign() {
            errors = [];
            if (document.getElementById('<%=ddlEmpDepartments.ClientID%>').value == 0) {
                errors.push("Select Department");
            }
            if (document.getElementById('<%=txtJrName.ClientID%>').value == "") {
                errors.push("Enter JR Name");
            }
            if (document.getElementById("<%=ddlTrainingDept.ClientID%>").value == 0) {
                errors.push("Select Training Department");
            }
            if (document.getElementById('<%=txtDesignation.ClientID%>').value == "") {
                errors.push("Enter Designation");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
            return true;
        }
        function validateRetRemoveAssigned() {
            errors = [];

            if (document.getElementById("<%=ddlTrainingDept.ClientID%>").value == 0) {
                errors.push("Select Training Department");
            }

            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>

    <script>
        function validateSubmitTS() {
            errors = [];
            if (document.getElementById('<%=ddlEmpDepartments.ClientID%>').value == 0) {
                errors.push("Select Department");
            }
            if (document.getElementById('<%=txtJrName.ClientID%>').value == "") {
                errors.push("Enter JR Name");
            }
            if (document.getElementById('<%=txtDesignation.ClientID%>').value == "") {
                errors.push("Enter Designation");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
        function validateCancelTS() {
            if (document.getElementById('<%=hdnAction.ClientID%>').value == "Approval") {
                if (document.getElementById('<%=txtRevertReason.ClientID%>').value == "") {
                    custAlertMsg("Enter Revert Reason in Reviewer Comments.", "error", "focusOnRevertreason()");
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    </script>

    <script>
        function TS_Created_Sucess() {
            window.open(document.getElementById("<%=hdnPrevipusPageURL.ClientID%>").value, "_self");
        }
        function JrTS_ReviewList() {
            window.open("<%=ResolveUrl("~/TMS/TS/JrTS_ReviewList.aspx?FilterType=2")%>", "_self");
        }
        function JrTS_PendingList() {
            window.open("<%=ResolveUrl("~/TMS/TS/JrTS_PendingList.aspx?FilterType=2")%>", "_self");
        }
    </script>

    <script>
        $(document).ready(function () {
            $('.gvRowStyle').click(function (event) {
                if (event.target.type !== 'checkbox') {
                    $(':checkbox', this).trigger('click');
                }
            });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.gvRowStyle').click(function (event) {
                if (event.target.type !== 'checkbox') {
                    $(':checkbox', this).trigger('click');
                }
            });
        });

    </script>

    <script>
        //for gridview search
        function GridViewSearch(phrase, _id) {
            var words = phrase.value.toLowerCase().split(" ");
            var table = document.getElementById(_id);
            var ele;
            for (var r = 1; r < table.rows.length; r++) {
                ele = table.rows[r].innerHTML.replace(/<[^>]+>/g, "");
                var displayStyle = 'none';
                for (var i = 0; i < words.length; i++) {
                    if (ele.toLowerCase().indexOf(words[i]) >= 0)
                        displayStyle = '';
                    else {
                        displayStyle = 'none';
                        break;
                    }
                }
                table.rows[r].style.display = displayStyle;
            }
        }
    </script>

    <script>        
        function onDepartmentChange() {
            if ($('#<%=ddlTrainingDept.ClientID%>').val() != 0) {
                $('#divTrainingScheduleAssign').show();
                $('#divSelectDeptNote').hide();
            }
            else {
                $('#divTrainingScheduleAssign').hide();
                $('#divSelectDeptNote').show();
            }
        }
    </script>
   
    <script>
        $(document).ready(function () {
            $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
                var anchor = $(e.target).attr('href');
                if (anchor == "#Comments_History") {
                    loadJrTS_ActionHistory();
                }
            })
        });
    </script>

</asp:Content>
