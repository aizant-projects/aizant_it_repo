﻿<%@ Page Title="Approve Questionnaire" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="ApproveQuestionnaire.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Administration.Questionnaire.ApproveQuestionnaire" %>

<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

      <style>
        .hr_Line {
            margin-bottom: 10px;
            margin-top: 5px;
        }
    </style>

    <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnRevertQueID" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
     <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  col-12 col-md-12 grid_panel_full padding-none">
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div id="divMainContainer" runat="server" visible="true">
            <div class="panel panel-default" id="mainContainer" runat="server" style="padding-bottom: 10px">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">Approve Questionnaire</div>
                <div>
                    <div class="float-left col-lg-12  col-12 col-md-12 col-sm-12 padding-none top">
                        <div class="form-horizontal">
                            <asp:UpdatePanel ID="upDropdowns" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="float-left col-lg-3  col-12 col-md-3 col-sm-3 ">
                                        <label class="float-left col-lg-12  col-12 col-md-12 col-sm-12 control-label padding-none">Department<span class="mandatoryStar">*</span></label>
                                        <div class="float-left col-lg-12  col-12 col-md-12 col-sm-12 padding-none">
                                             <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control"
                                                    data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="float-left col-lg-7 col-12 col-md-6 col-sm-7">
                                        <label class="control-label float-left col-lg-12  col-12 col-md-12 col-sm-12 padding-none">Document<span class="mandatoryStar">*</span></label>
                                        <div class="float-left col-lg-12  col-12 col-md-12 col-sm-12 padding-none">
                                              <asp:DropDownList ID="ddlDocuments" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control"
                                                    data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlDocuments_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="float-left col-lg-2 col-12 col-md-3 col-sm-2">
                                        <label class="float-left col-lg-12  col-12 col-md-12 col-sm-12 control-label padding-none">Document Type</label>
                                        <div class="float-left col-lg-12  col-12 col-md-12 col-sm-12 padding-none">
                                             <input type="text" class="form-control login_input_sign_up" name="DocumentType" runat="server" id="txtDocumentType" placeholder="Document Type" title="Document Type" readonly="readonly" style="cursor:not-allowed;" />
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    
                    <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 top" style="overflow: auto;max-height: 530px; ">
                        <asp:UpdatePanel ID="upgvApproveQuestionnaire" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:GridView ID="gvApproveQuestionnaire" OnRowCommand="gvApproveQuestionnaire_RowCommand" OnRowDataBound="gvApproveQuestionnaire_RowDataBound"
                                    DataKeyNames="QuestionID" runat="server" AutoGenerateColumns="False" CssClass="col-12 padding-none question_list table-striped table-bordered AspGrid"
                                    EmptyDataText="No Records" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle question_none">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Question ID" SortExpression="QueID" Visible="false">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Questionnaire" SortExpression="Question">
                                            <ItemTemplate>
                                                <table class=" table table-bordered">
                                                    <tr>
                                                        <td style="color:#2e2e2e; background:#c0d4d7; font-size:12pt; font-weight:bold;">
                                                            <asp:Label ID="lblGVQuestion"  runat="server" Text='<%# Bind("QuestionTitle") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding: 0px;">
                                                            <asp:GridView ID="gvOptions" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered" Style="margin-bottom: 0px;" ShowHeader="false">
                                                                <Columns>
                                                                    <asp:BoundField DataField="Option" HeaderText="Option" ItemStyle-BackColor="#07889a" ItemStyle-ForeColor="#ffffff" ItemStyle-Width="1px" />
                                                                    <asp:BoundField DataField="OptionTitle" HeaderText="Value" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="color:#2e2e2e; background:#c0d4d7; font-size:15pt; font-weight:bold;">
                                                            <label style="background-color:#07889a;color:#ffffff;font-size:12pt; font-weight:bold;">Answer : </label>
                                                            <asp:Label ID="lblGVAnswer" runat="server" Text='<%#Eval("Answer")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" SortExpression="Description" ItemStyle-Width="23%">
                                            <ItemTemplate>
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblGVCreatedBy" runat="server" Text='<%#"Created By : " + Eval("CreatorName")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblGVCreatedDate" runat="server" Text='<%#"Created Date : " + Eval("CreatedDate")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%-- <th>Revert  : <asp:LinkButton ID="lnkRevert"  class="fa fa-reply GV_Edit_Icon" runat="server" CommandName="RevertButton" CommandArgument='<%# Eval("QuestionID") %>' OnClientClick="javascript:return OnConfirmRevert();" ToolTip="Click To Revert" Style="top:5px;"></asp:LinkButton>
                                                    </th>--%>
                                                      <th style="background:transparent !important" >
                                                            <asp:LinkButton ID="lnkRevert" CssClass="btn btn-revert_popup" Text="Revert" runat="server" CommandName="RevertButton" CommandArgument='<%# Eval("QuestionID") %>'  ToolTip="Click To Revert">
                                                            </asp:LinkButton>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    
                    <asp:UpdatePanel ID="upBtnSubmit" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom" id="divBtnSubmit" runat="server" visible="false">
                                <asp:Button ID="btnSubmit" CssClass="float-right btn-signup_popup" runat="server" OnClick="btnSubmit_Click" ToolTip="Click to Approve" OnClientClick="javascript:return SubmitRequired();" Text="Approve" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                        <asp:UpdatePanel ID="uplblMsg" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="Panel_Footer_Message_Label" style="padding-left: 2%; padding-right: 2%;">
                                    <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                </div>
            </div>
    </div>
    <%--modal popup to load Revert Remarks--%>
    <div class="modal department fade" id="RevertRemarks" runat="server" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header tmsModalHeader">
                    
                    <h4 class="modal-title Panel_Title" style="color: aliceblue">Revert Reason</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body tmsModalBody">
                    <asp:UpdatePanel ID="upModalBody" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="row tmsModalContent">
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                    <textarea class="form-control" id="txtRevertReason" style="height: 100px" cols="12" runat="server" maxlength="500" placeholder="Enter Revert Reason" aria-multiline="true" title="Revert Reason"></textarea>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer tmsModalContent">
                    <div class="" style="text-align: center">
                        <%-- <asp:UpdatePanel ID="UpBtnRevert" runat="server" UpdateMode="Conditional" style="display: inline-block">
                        <ContentTemplate>--%>
                        <asp:Button ID="btnRevert" CssClass=" btn-signup_popup" runat="server" Text="Submit" OnClientClick="javascript:return RevertRequired();" OnClick="btnRevert_Click" />
                        <asp:Button ID="btnCancel" CssClass=" btn-cancel_popup" runat="server" OnClick="btnCancel_Click" Text="Cancel" />
                        <%--   </ContentTemplate>
                    </asp:UpdatePanel>--%>
                        <asp:UpdatePanel ID="UPdesigMessage" runat="server" UpdateMode="Always" style="padding: 3px">
                            <ContentTemplate>
                                <asp:Label ID="lblmodalMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
         </div>
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    
    <script>
        //for clearing the RevertMessage
        $('#<%=btnCancel.ClientID%>').click(function () {
            $('#<%=txtRevertReason.ClientID%>').val("");
            document.getElementById("<%=lblmodalMsg.ClientID%>").innerHTML = "";
        });
    </script>
    <script>
        function OpenModelViewRevertRemarks() {
            $('#RevertRemarks').modal({ backdrop: 'static', keyboard: false });
        }
    </script>
    <script type='text/javascript'>
        function openModal() {
            $('[id*=RevertRemarks]').modal('show');
        }
        //for confirm message before Reverting the record
        function OnConfirmRevert() {
            custAlertMsg('Do You Want To Revert This Question ?', 'confirm', true);
        }

        function RevertRequired() {
            if (document.getElementById("<%=txtRevertReason.ClientID%>").value == "") {
                //alert("Enter Revert Reason");
                custAlertMsg("Enter Revert Reason", "error");
                document.getElementById("<%=txtRevertReason.ClientID%>").focus();
                return false;
            }
            else {
                return true;
            }
        }
        function SubmitRequired() {
            errors = [];
            if (document.getElementById('<%=ddlDepartment.ClientID%>').value == 0) {
                errors.push("Select Department");
            }
            if (document.getElementById('<%=ddlDocuments.ClientID%>').value == 0) {
                errors.push("Select Document");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</asp:Content>
