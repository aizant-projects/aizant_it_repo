﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="testcheckbox.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.Settings.testcheckbox" %>

<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <dx:ASPxCheckBoxList ID="ASPxCheckBoxList1" runat="server" RepeatColumns="8"
                RepeatDirection="Vertical" RepeatLayout="Table" AutoPostBack="true" OnSelectedIndexChanged="ASPxCheckBoxList1_SelectedIndexChanged">
                <Items>
                    <dx:ListEditItem Text="Item 0" Value="0" />
                    <dx:ListEditItem Text="Item 1" Value="1"/>
                    <dx:ListEditItem Text="Item 2" Value="2"/>
                </Items>
            </dx:ASPxCheckBoxList>
        </div>
    </form>
</body>
</html>
