﻿<%@ Page Title="TMS Dash Board" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="TmsDashboard.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TmsDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:HiddenField ID="hdnEmpID" runat="server" />
    <asp:HiddenField ID="hdnLoadCharts" runat="server" Value="false" />
    <asp:HiddenField ID="hdnToAccessRoles" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTMS_TrainingCoordinator" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTMS_TrainingTrainee" runat="server" Value="0" />
    <asp:HiddenField ID="hdnMLTrainingPerformace_Enable" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTMS_DocumentTrainingRecommendations" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTMS_DocumentRecommendedCount" runat="server" Value="0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link href='<%=ResolveUrl("~/DevExtremeContent/dx.common.css")%>' rel="stylesheet" />
    <link href='<%=ResolveUrl("~/DevExtremeContent/dx.light.css")%>' rel="stylesheet" />
    <script src='<%=ResolveUrl("~/DevExtremeScripts/dx.all.js")%>'></script>
    <!--- Trainee Performance Script Starts--->
    <script src='<%=ResolveUrl("~/AppScripts/jquery.circlechart.js")%>'></script>
    <!--- Trainee Performance Script ends--->

    <!--ML Enable and Disable script starts -->
    <script src='<%=ResolveUrl("~/AppScripts/ML_FeaturesChecking.js")%>'></script>
    <script>
        GetMLFeatureChecking(3);//Passing Paramenter Module ID
    </script>
    <!--ML Enable and Disable script ends-->

    <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
        <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none panel_list_bg bottom float-left" id="content" style="position: relative; z-index: 9;">
            <div id="divCards">
                <!--Evaluate Card-->
                <div class="col-lg-3 col-md-3 col-sm-6 col-6 float-left padding-right_div f1_container" id="divEvaluationCard" runat="server" visible="false" style="position: relative; z-index: 99;">
                    <div class=" f1_card col-lg-12  float-left">
                        <div class="front face outer_dashboard float-left" style="height: 120px !important;">
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none   float-left">
                                <div style="position: absolute; right: 0; opacity: 0.2; top: 20px;">
                                    <img src="<%=ResolveUrl("~/Images/common_dashboard/approve_big.png")%>" class="" alt="Approve" />
                                </div>
                                <div class="col-lg-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Evaluate</div>
                                <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                    <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none float-left">
                                        <img src='<%=ResolveUrl("~/Images/common_dashboard/approve_small.png")%>' class="dashboard-image" alt="Approve" />
                                    </div>
                                    <div class="col-lg-6 col-6 col-sm-6 col-md-6 float-right">
                                        <span class="link_dashboard">
                                            <div class="dashboard_frontpanel_value">
                                                <asp:Label ID="lblEvaluationCount" runat="server" Text="0"></asp:Label>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="back face center outer_dashboard padding-none float-left" style="height: 120px !important;">
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                <div class="col-lg-12 padding-none float-left">
                                    <div class="col-lg-12 padding-none float-left">
                                        <div class=" col-lg-12 padding-none  float-right ">
                                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none view_text_dshboard" runat="server" id="divEvaluationNoDetails" visible="false">
                                                <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none card_view_box_1 float-left">No Records</div>
                                            </div>
                                            <ul class="col-lg-12 col-12 col-sm-12 col-md-12 flip_list style-2 padding-none" runat="server" id="divEvaluationViewDetails" visible="true">
                                                <li>
                                                    <asp:HyperLink ID="hlJR_HodEvaluation" NavigateUrl="~/TMS/Training/JR_Training/Evaluation/HodEvaluation.aspx" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlJR_TrainerEvaluation" NavigateUrl="~/TMS/Training/JR_Training/Evaluation/JR_TrainerEvaluationDetails.aspx" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlTargetTrainerEvaluation" NavigateUrl="~/TMS/Training/TargetTrainerEvaluation.aspx?Evaluated=Y" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!--Reverted Card-->
                <div class="col-lg-3 col-md-3 col-sm-6 col-6 float-left padding-right_div f1_container float-left" id="divRevertedCard" runat="server" visible="false" style="position: relative; z-index: 99;">
                    <div class=" f1_card col-lg-12  float-left">
                        <div class="front face outer_dashboard1 float-left">
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none  outer_dashboard1  float-left">

                                <div style="position: absolute; right: 0; top: 20px;">
                                    <img src="<%=ResolveUrl("~/Images/common_dashboard/reverted.png")%>" class="" alt="Reverted" />
                                </div>
                                <div class="col-lg-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Reverted </div>
                                <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                    <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none float-left">
                                        <img src="<%=ResolveUrl("~/Images/common_dashboard/revetred_small.png")%>" class="dashboard-image" alt="Reverted" />
                                    </div>
                                    <div class="col-lg-6 col-6 col-sm-6 col-md-6 float-right">
                                        <span class="link_dashboard">
                                            <div class="dashboard_frontpanel_value">
                                                <asp:Label ID="lblRevertedCount" runat="server" Text="0"></asp:Label>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="back face center outer_dashboard1 float-left" style="height: 121px !important;">
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 float-left padding-none">
                                <div class="col-lg-12 padding-none float-left">
                                    <div class="col-lg-12 padding-none float-left">
                                        <div class=" col-lg-12 padding-none  float-right ">
                                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 view_text_dshboard" runat="server" id="divRevertedNoDetails" visible="false">
                                                <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none card_view_box_1 float-left">No Records</div>
                                            </div>
                                            <ul class="col-lg-12 col-12 col-sm-12 col-md-12 flip_list style-2 padding-none" runat="server" id="divRevertedViewDetails" visible="true">
                                                <li>
                                                    <asp:HyperLink ID="hlDocQueMaster" NavigateUrl="~/TMS/Administration/Questionnaire/Questionnaire.aspx?Reverted='Yes'" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlTrainer_ATC_QAorHODReverted" NavigateUrl="~/TMS/ATC/List/ATC_PendingList.aspx?FilterType=1" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlJR_TS_Reverted" NavigateUrl="~/TMS/TS/JrTS_PendingList.aspx?FilterType=3" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlJR_PendingList" NavigateUrl="~/TMS/JR/JR_PendingList.aspx?FilterType=1" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlQA_RevertedTTS" NavigateUrl="~/TMS/TargetTrainingSchedule/TTS_Pending.aspx?FilterType=1" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <!--Pending Card-->
                <div class="col-lg-3 col-md-3 col-sm-6 col-6 float-left padding-right_div f1_container" id="divPendingCard" runat="server" visible="false" style="position: relative; z-index: 99;">
                    <div class=" f1_card col-lg-12  float-left">
                        <div class="front face outer_dashboard2 float-left">
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none  outer_dashboard2  float-left">

                                <div style="position: absolute; right: 0; opacity: 0.2; top: 20px;">
                                    <img src="<%=ResolveUrl("~/Images/common_dashboard/pending_big.png")%>" class="" alt="Approve" />
                                </div>
                                <div class="col-lg-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Pending </div>
                                <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                    <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none float-left">
                                        <img src="<%=ResolveUrl("~/Images/common_dashboard/pending_small.png")%>" class="dashboard-image" alt="Approve" />
                                    </div>
                                    <div class="col-lg-6 col-6 col-sm-6 col-md-6 float-right">
                                        <span class="link_dashboard">
                                            <div class="dashboard_frontpanel_value">
                                                <asp:Label ID="lblPendingCount" runat="server" Text="0"></asp:Label>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="back face center outer_dashboard2 float-left" style="height: 121px !important;">
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 float-left padding-none">
                                <div class="col-lg-12 padding-none float-left">
                                    <div class="col-lg-12 padding-none float-left">
                                        <div class=" col-lg-12 padding-none  float-right ">
                                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 view_text_dshboard" runat="server" id="divPendingNoDetails" visible="false">
                                                <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none card_view_box_1 float-left">No Records</div>
                                            </div>
                                            <ul class="col-lg-12 col-12 col-sm-12 col-md-12 flip_list style-2 padding-none" runat="server" id="divPendingViewDetails" visible="true">
                                                <li>
                                                    <asp:HyperLink ID="hlPendingTrainingCalendar" NavigateUrl="~/TMS/ATC/List/CreateATC_List.aspx" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlHOD_ATCApproval" NavigateUrl="~/TMS/ATC/List/ATC_Hod_Approval.aspx?FilterType=1" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlQA_ATC_Approval" NavigateUrl="~/TMS/ATC/List/ATC_QA_Approve.aspx?FilterType=1" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlJR_TsReviewList" NavigateUrl="~/TMS/TS/JrTS_ReviewList.aspx?FilterType=1" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlJR_TS_PendingList" NavigateUrl="~/TMS/TS/JrTS_PendingList.aspx?FilterType=1" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlHOD_Que_ToApproval" NavigateUrl="~/TMS/Administration/Questionnaire/ApproveQuestionnaire.aspx" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlAcceptJR" NavigateUrl="~/TMS/JR/AcceptMyJR.aspx" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlJR_TrainingList" NavigateUrl="~/TMS/Training/JR_Training/JR_TrainingList.aspx?FilterType=1" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlTrainerAnswersToTrainee" NavigateUrl="~/TMS/TrainerAnswersOnDocs/TrainerAnswerDocList.aspx" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlTraineeQuestionsToTrainer" NavigateUrl="~/TMS/TraineeQuestionsOnDocs/TraineeQuestionDocList.aspx" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlJR_ReviewList" NavigateUrl="~/TMS/JR/JR_ReviewList.aspx?FilterType=1" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlHOD_TTS_NewSOP_Pending" NavigateUrl="~/TMS/TargetTrainingSchedule/TTS_CreationList.aspx?FilterType=2" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlHOD_TTS_RevisionSOP_Pending" NavigateUrl="~/TMS/TargetTrainingSchedule/TTS_CreationList.aspx?FilterType=3" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlQA_TTS_Approval" NavigateUrl="~/TMS/TargetTrainingSchedule/TTS_ApprovalList.aspx?FilterType=1" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlTrainer_TTS_SessionCreationOrModification" NavigateUrl="~/TMS/TargetTrainingSchedule/TTS_ApprovedList.aspx" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlHOD_TTS_RefresherDocForThisMonth" NavigateUrl="~/TMS/TargetTrainingSchedule/TTS_CreationList.aspx?FilterType=1" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlTTS_ExamClassroom" NavigateUrl="~/TMS/Training/TargetTraining.aspx?Tab=ClassRoom&FilterType=2" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlTTS_SessionClassroom" NavigateUrl="~/TMS/Training/TargetTraining.aspx?Tab=ClassRoom" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlOnlineExam" NavigateUrl="~/TMS/Training/TargetTraining.aspx?Tab=Online" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                                <li>
                                                    <asp:HyperLink ID="hlScheduledJRandTT_PendingListForDeptHead" NavigateUrl="~/TMS/Reports/ScheduledJRandTT_PendingList.aspx" runat="server" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" Visible="false">
                                                    </asp:HyperLink>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--ATC Card-->
                <div class="col-lg-3 col-md-3 col-sm-6 col-6 float-left padding-right_div f1_container" id="divATC_Card" runat="server" visible="false" style="position: relative; z-index: 9;">
                    <div class=" f1_card col-lg-12  float-left">
                        <div class="front face outer_dashboard3 float-left">
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none  outer_dashboard3  float-left">
                                <div style="position: absolute; right: 0; opacity: 0.4; top: 20px;">
                                    <img src="<%=ResolveUrl("~/Images/common_dashboard/calender_big.png")%>" class="" alt="Approve" />
                                </div>
                                <div class="col-lg-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Annual Training Calendar </div>
                                <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                    <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none float-left">
                                        <img src="<%=ResolveUrl("~/Images/common_dashboard/calender_small.png")%>" class="dashboard-image" alt="Approve" />
                                    </div>
                                    <div class="col-lg-6 col-6 col-sm-6 col-md-6 float-right">
                                        <span class="link_dashboard">
                                            <%=DateTime.Now.Year %>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="back face center outer_dashboard3 float-left" style="height: 121px !important;">
                            <div id="Atc_Main" class="col-lg-12 float-left ">
                                <div class="form-group drop col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                    <label for="filter" class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none top" style="color: #fff; text-align: left;">Select Department</label>
                                    <asp:DropDownList ID="ddlATC_Departments" runat="server" CssClass="selectpicker dropdown_atc col-lg-12 col-12 col-sm-12 col-md-12 padding-none" data-size="7" data-live-search="true">
                                    </asp:DropDownList>
                                </div>
                                <ul class="calender_atc dropdown_dashboard  col-lg-12 col-12 col-sm-12 col-md-12 " id="ulCalendar" style="display: none">
                                    <li>
                                        <a href="#" onclick="getDocsOn(1,this);" class="active">JAN<span class="atc_month_count" id="janCount"></span></a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="getDocsOn(2,this);">FEB<span class="atc_month_count" id="febCount"></span></a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="getDocsOn(3,this);">MAR<span class="atc_month_count" id="marCount"></span></a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="getDocsOn(4,this);">APR<span class="atc_month_count" id="aprCount"></span></a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="getDocsOn(5,this);">MAY<span class="atc_month_count" id="mayCount"></span></a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="getDocsOn(6,this);">JUN<span class="atc_month_count" id="junCount"></span></a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="getDocsOn(7,this);">JUL<span class="atc_month_count" id="julCount"></span></a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="getDocsOn(8,this);">AUG<span class="atc_month_count" id="augCount"></span></a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="getDocsOn(9,this);">SEP<span class="atc_month_count" id="sepCount"></span></a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="getDocsOn(10,this);">OCT<span class="atc_month_count" id="octCount"></span></a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="getDocsOn(11,this);">NOV<span class="atc_month_count" id="novCount"></span></a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="getDocsOn(12,this);">DEC<span class="atc_month_count" id="decCount"></span></a>
                                    </li>
                                </ul>
                                <div class="atcCardDocsList col-lg-12 col-12 col-sm-12 col-md-12 top padding-none float-left" id="divAtcDocsList" style="display: none;">
                                    <div class="grid_header_dashboard col-lg-12 col-12 col-sm-12 col-md-12 float-left">
                                        <div class="col-lg-6 col-6 col-sm-6 col-md-6 float-left text-left padding-none" id="AtcDocsListTitle">FEB</div>
                                        <div class="col-lg-6 col-6 col-sm-6 col-md-6 float-left padding-none">
                                            <div class="float-right close_Cal" id="divCloseAtcDocsList">X</div>
                                        </div>
                                    </div>
                                    <table id="tblAtcDocsList" class="display" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>Document</th>
                                                <th>Doc.Type</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Loading Document(s)....</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Chart Section Start-->
        <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left" style="display: none;" id="divTmsDashboard">
            <!-- ATC Chart-->
            <div class="col-12 bottom padding-none float-left" id="divAtc_Chart">
                <div class="grid_header  col-lg-12 float-left ">
                    <div class="col-lg-6 padding-none float-left">Refresher Training's</div>
                    <div class="col-lg-6 padding-none float-left">
                        <div class="panel_list float-right">
                            <ul>
                                <li>
                                    <a class="refresh" title="Refresh" id="RefreshATC"></a>
                                </li>
                                <li>
                                    <a class="filter_icon" title="Filter" id="lifilterATC" onclick="OpenCharFilter('FilterAtc')"></a>
                                    <div class="filter_dropdown" id="FilterAtc" style="display: none">
                                        <ul class="top">
                                            <li>
                                                <div class="form-group  col-lg-12 padding-none">
                                                    <asp:DropDownList ID="ddlDepartmentATC" runat="server" CssClass="selectpicker form-control regulatory_dropdown_style"
                                                        data-live-search="true" data-size="5" AutoPostBack="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group  col-lg-12 padding-none">
                                                    <asp:DropDownList ID="ddlATCYear" runat="server" CssClass="selectpicker form-control regulatory_dropdown_style"
                                                        data-live-search="true" data-size="5" AutoPostBack="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </li>
                                            <li class="float-right">
                                                <div class="text-right">

                                                    <input type="button" id="btnAtcSubmit" class="  btn-signup_popup  float-left" value="Submit" />
                                                    <input type="button" onclick="CloseFilterDivs()" class=" btn-cancel_popup  float-left" value="Close" style="margin-left: 2px;" />

                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 grid_panel_full  bottom float-left dx-viewport float-left">
                    <div id="divAtcDetails">
                        <div class="demo-container">
                            <div id="ATCBarchart"></div>
                        </div>
                        <label id="lblAtcChartTitle" class="col-md-12 chart_title_bottom" style="text-align: center">Refresher Training in <%=DateTime.Now.Year%></label>
                    </div>
                    <div style="display: none" id="divNoAtcDetails" class="norecords">No Refresher Training's</div>
                </div>
            </div>
            <!--End ATC Chart-->

            <div class="col-12 padding-none float-left">
                <!---Training Pending/Completed chart--->
                <div class="col-6 padding-none float-left">
                    <div class="grid_header  col-lg-12 col-md-12 float-left">
                        <div class="col-lg-6 col-md-8 padding-none float-left">Training Details</div>
                        <div class="col-lg-6 col-md-4 padding-none float-left">
                            <div class="panel_list float-right">
                                <ul>
                                    <%--<li>
                                        <a href="#" class="fav"></a>
                                    </li>--%>
                                    <li>
                                        <a class="refresh" title="Refresh" id="RefreshCompletedandPending"></a>
                                    </li>
                                    <li>
                                        <a class="filter_icon" title="Filter" id="lifilterCompleted" onclick="OpenCharFilter('FilterForCompleted')"></a>
                                        <div class="filter_dropdown " id="FilterForCompleted" style="display: none">
                                            <ul class="top">
                                                <li>
                                                    <div class="form-group col-lg-12 padding-none">
                                                        <asp:DropDownList ID="ddlDepartmentCompleted" runat="server" CssClass="selectpicker form-control regulatory_dropdown_style"
                                                            data-live-search="true" data-size="5" AutoPostBack="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-group col-lg-12 padding-none">
                                                        <asp:DropDownList ID="ddlCompletedYear" runat="server" CssClass="selectpicker form-control regulatory_dropdown_style"
                                                            data-live-search="true" data-size="5" AutoPostBack="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </li>
                                                <li id="liTraineePending" style="display: none">
                                                    <div class=" btn-group padding-none col-sm-12 col-12 col-lg-12 col-md-12 float-left top bottom" data-toggle="buttons">
                                                        <label class="btn btn-primary_radio col-6 col-lg-6 col-md-6 col-sm-6 active radio_check" id="lblOverallTraining" onclick="RefreshTraineeAssignedDepartments('OverAllDepartments');">
                                                            <asp:RadioButton ID="rbtnOverallTrainee" value="Yes" GroupName="rbnPending" runat="server" Text="Overall Training" />
                                                        </label>
                                                        <label class="btn btn-primary_radio col-6 col-lg-6 col-md-6 col-sm-6 radio_check" id="lblPendingTraining" onclick="RefreshTraineeAssignedDepartments('MyTraining');">
                                                            <asp:RadioButton ID="rbtnPendingTrainee" value="No" GroupName="rbnPending" runat="server" Text="My Training" />
                                                        </label>
                                                    </div>
                                                </li>
                                                <li class="float-right">
                                                    <div class="float-right">
                                                        <input type="button" onclick="CloseFilterDivs()" class="   btn-cancel_popup  float-left" value="Close" style="margin-left: 2px;" />
                                                    </div>
                                                    <div class="float-right">
                                                        <input type="button" id="btnSubmitCompletedandPending" class="  btn-signup_popup  float-left" value="Submit" />
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 grid_panel_full chart_height  bottom float-left dx-viewport">
                        <div id="divCompletedrecords">
                            <div class="demo-container">
                                <div id="Barchart"></div>
                            </div>
                            <label id="lblTrainingStatus" class="col-md-12 chart_title_bottom" style="text-align: center">Training Status in <%=DateTime.Now.Year%></label>
                        </div>
                        <div style="display: none" id="NoCompletedrecords" class="norecords">No Training Data</div>
                    </div>
                </div>
                <!---End Training Pending/Completed chart--->

                <!---Training OverDue chart--->
                <div class="col-6 padding-left_div float-left">
                    <div class="grid_header  col-lg-12 col-md-12 float-left">
                        <div class="col-lg-6 col-md-6 padding-none float-left">Over Due Training's</div>
                        <div class="col-lg-6 col-md-6 padding-none float-left">
                            <div class="panel_list float-right">
                                <ul>
                                    <%-- <li>
                                        <a href="#" class="fav"></a>
                                    </li>--%>
                                    <li>
                                        <a class="refresh" title="Refresh" id="RefreshOverDue"></a>
                                    </li>
                                    <li>
                                        <a class="filter_icon" title="Filter" id="lifilterOverDue" onclick="OpenCharFilter('FilterForOverDue')"></a>
                                        <div class=" filter_dropdown " id="FilterForOverDue" style="display: none">
                                            <ul class="top">
                                                <li>
                                                    <div class="form-group  col-lg-12 padding-none">
                                                        <asp:DropDownList ID="ddlDepartmentOverDue" runat="server" CssClass="selectpicker form-control regulatory_dropdown_style"
                                                            data-live-search="true" data-size="5"
                                                            AutoPostBack="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </li>
                                                <li id="liOverDueradiobuttons" style="display: none;">
                                                    <div class="btn-group padding-none col-sm-12 col-12 col-lg-12 col-md-12 float-left top bottom" data-toggle="buttons">
                                                        <label class="btn btn-primary_radio col-6 col-lg-6 col-md-6 col-sm-6 active radio_check" id="lblOverall" onclick="RefreshTraineeAssignedDepartments('MyOverdueAllDepartments');">
                                                            <asp:RadioButton ID="rbtnOverall" value="Yes" GroupName="rbnOverdue" runat="server" Text="Overall Training" Checked="true" />
                                                        </label>
                                                        <label class="btn btn-primary_radio col-6 col-lg-6 col-md-6 col-sm-6 radio_check" id="lblTrainee" onclick="RefreshTraineeAssignedDepartments('MyOverdueTraining');">
                                                            <asp:RadioButton ID="rbtnTrainee" value="No" GroupName="rbnOverdue" runat="server" Text="My Training" />
                                                        </label>
                                                    </div>
                                                </li>
                                                <li class="float-right">
                                                    <div class="float-right">
                                                        <input type="button" onclick="CloseFilterDivs()" class="  btn-cancel_popup  float-left" value="Close" style="margin-left: 2px;" />
                                                    </div>
                                                    <div class="float-right">
                                                        <input type="button" id="btnOverDueSubmit" class="  btn-signup_popup  float-left" value="Submit" />
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 grid_panel_full bottom float-left dx-viewport">
                        <div id="divOverDueDetails">
                            <div class="demo-container">
                                <div id="OverDueChart" style="margin: 10px 0px;"></div>
                            </div>
                            <label id="lblOverDueTrainingChartTitle" class="col-md-12 chart_title_bottom" style="text-align: center">Over Due Trainings on All Accessible Departments</label>
                        </div>
                        <div style="display: none" id="divNo_OverDuedetails" class="noOverDuerecords">No Overdue Training's</div>
                    </div>
                </div>
                <!---End Training OverDue chart--->
            </div>
            <!--- Trainee Performance Div starts--->
            <div id="divTrainingPerformanceDetails" class="col-12 float-left padding-none" style="display: none;">

                <div class="col-md-12 col-lg-12 col-xl-12  top float-left padding-none ">
                    <div class="grid_header float-left col-lg-12">
                        <div class="col-lg-6 float-left padding-none">Training Performance Details</div>
                        <div class="col-lg-6 float-left padding-none">
                            <div class="panel_list float-right">
                                <ul>
                                    <%-- <li>
                                        <a class="fav"></a>
                                    </li>--%>
                                    <li>
                                        <a class="refresh" title="Refresh" id="RefreshTrainingPerformanceList"></a>
                                    </li>
                                    <li>
                                        <a class="filter_icon" id="filter_icon_tp" title="Filter" onclick="OpenCharFilter('ddlTraineeOverallPerformanceDept')"></a>
                                        <div class="col-lg-12 filter_dropdown " id="ddlTraineeOverallPerformanceDept" style="display: none;">
                                            <ul class="top">
                                                <li>
                                                    <div class="form-group col-lg-12 padding-none">
                                                        <asp:DropDownList ID="ddlTrainingPerformanceDept" runat="server" CssClass="selectpicker form-control regulatory_dropdown_style"
                                                            data-live-search="true" data-size="5" AutoPostBack="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </li>
                                                <li class="float-right">
                                                    <div class="float-right">
                                                        <input type="button" onclick="CloseFilterDivs()" class="  btn-cancel_popup  float-left" value="Close" style="margin-left: 2px;" />
                                                    </div>
                                                    <div class="float-right">
                                                        <input type="button" id="DeptWiseTrainingPerformanceSubmit" onclick="GetDeptWiseTrainingPerformance();" class="btn-signup_popup  float-left" value="Submit" />
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12  grid_panel_full bottom float-left padding-none">
                        <div md="main" class="chart" style="height: 320px">
                            <div class="col-12 float-left top">
                                <table id="tblTrainingPerformaceDetails" class="tblTrainingPerformaceDetailsClass display breakword datatable_cust" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>Emp. Name</th>
                                            <th>Avg Duration</th>
                                            <th>Avg Marks</th>
                                            <th>No.Of Times Failed</th>
                                            <th>No.Of Documents</th>
                                            <th>Performance</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/col-md--->
            </div>
            <!--- Trainee Performance Div Ends--->
        </div>
    </div>

    <!--Chart Titles By Selection-->
    <!--New trainee performance sticky pop up-->
    <div class="performance-popup " id="divTraineePerformanceDetails" style="display: none">
        <div class="performancepopup-wrap">
            <div class="performancepopup-header">
                <span title="My Training Performance" class="performancepopup-title">
                    <img src="<%=ResolveUrl("~/Images/pie-chart.png")%>" width="32" height="32" />
                    <div class="performancepopup-image"></div>
                </span>
            </div>
            <div class="performancepopup-content">
                <div class="col-12 float-left grid_header_performance">My Training Performance</div>
                <div class="performancepopup-content-pad">
                    <div class="performancepopup-form">

                        <div class="performancepopup-form--part-title-font-weight-normal">
                            <div class="performancepopup-flex">
                                <div class="performancepopup-form__part popup-part performancepopup-part--multi_line_text performancepopup-part--width-full performancepopup-part--label-above" style="text-align: center;"
                                    data-popup-type="multi_line_text">
                                    <div class="performancepopup-part-wrap">
                                        <div class="col-12 float-left padding-none top text-center" id="divTraineePercentage">
                                            <div id="TraineeOverallPercentage" class="demo percentile_chart" data-percent="65"></div>
                                        </div>
                                        <div class="col-12 float-left padding-none top" id="carddiv" style="display: none;">
                                            <div class="col-lg-12 padding-none  float-left averagelistdiv">
                                                <ul class="averagelist float-left  col-lg-12 text-left padding-none">
                                                    <li>
                                                        <a href="#" class="float-left col-lg-12">
                                                            <div class="float-left padding-none col-lg-7">Average Duration</div>
                                                            <div id="TraineeAvgDuration" class="float-right padding-none col-lg-5"></div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="float-left col-lg-12">
                                                            <div class="float-left padding-none col-lg-7">Average Marks</div>
                                                            <div id="TraineeAvgMarks" class="float-right padding-none col-lg-5"></div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="float-left col-lg-12">
                                                            <div class="float-left padding-none col-lg-7">No. of Times Failed </div>
                                                            <div id="TraineeNo_OfAttempts" class="float-right padding-none col-lg-5"></div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="float-left col-lg-12">
                                                            <div class="float-left padding-none col-lg-7">No. of Trainings </div>
                                                            <div id="TraineeNo_OfDocs" class="float-right padding-none col-lg-5"></div>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="col-12 float-left text-right padding-none">
                                            <img src="<%=ResolveUrl("~/Images/list.png")%>" class="list_clas" id="ImgTraineePercentageList" alt="Approve" />
                                            <img src="<%=ResolveUrl("~/Images/chart.png")%>" class="list_clas top" id="OverallPercentageCard" alt="Approve" style="display: none;" />

                                        </div>

                                        <label class="performancepopup-part__label" style="text-align: left;">
                                            <span class="label ">
                                                <span class="performancepopupHeading">

                                                    <div class="performancepopupNavigate" id="divOverDueDataBind">
                                                    </div>
                                                </span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--trainee performance sticky end-->

    <!-- Document Recommendations -->
    <div class="sticky-popup" id="divDocumentRecommendation" style="display: none">
        <span class="popup_notification">
            <strong class="divDocumentRecommendationCount">0</strong>
        </span>
        <div class="popup-wrap">
            <div class="popup-header">
                <span class="popup-title">Training Recommendation(s)<div class="popup-image"></div>
                </span>
            </div>
            <div class="popup-content">
                <div class="popup-content-pad">
                    <div class="popup-form">
                        <div class="popup-form--part-title-font-weight-normal">
                            <div class="popup-flex">
                                <div class="popup-form__part popup-part popup-part--multi_line_text popup-part--width-full popup-part--label-above" style="text-align: center;"
                                    data-popup-type="multi_line_text">
                                    <div class="popup-part-wrap">
                                        <label class="popup-part__label" style="text-align: left;">
                                            <span class="label ">
                                                <span class="popupHeading">Hi
                                                                <span class="popheadingName" style="text-decoration: none; text-transform: capitalize" id="spnRecommendedEmpName">Your Name</span>,<br />
                                                    <div class="popupNavigate" id="">Training Recommendations<a href="#" data-target="#TrainingDocumentRecommendedModal" data-toggle="modal"><strong class="divDocumentRecommendationCount">0</strong></a></div>
                                                </span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Recommendations stickypopup-->

    <!--- Performance  modal--->
    <div id="TraineePerformancemodal" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog modal-dialog-centered" style="min-width: 40%">
            <!-- Modal content-->
            <div class="modal-content">
                <div id="TraineeName" class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-12 float-left padding-none top">
                        <div class="col-5 float-left average_durdiv">
                            <h5 class="col-12 text-center averagedur_text head top">Average Duration</h5>
                            <div id="divTraineeAvgDuration" class="col-12 text-center bottom averagedur_val averagedur_text padding-none"></div>
                        </div>
                        <div class="col-5 float-left average_atmpdiv">
                            <h5 class="col-12 text-center averagedatmp_text head top">No. of Times Failed</h5>
                            <div id="divNo_OfTimesFailed" class="col-12 text-center bottom averagedur_val averagedatmp_text padding-none"></div>
                        </div>
                    </div>
                    <div class="col-12 float-left padding-none">
                        <div class="col-5 float-left average_mardiv">
                            <h5 class="col-12 text-center averagemar_text head top">Average Marks</h5>
                            <div id="divAvgMarks" class="col-12 text-center bottom averagedur_val averagemar_text padding-none"></div>
                        </div>
                        <div class="col-5 float-left average_docdiv">
                            <h5 class="col-12 text-center averagedoc_text head top">No. of Trainings</h5>
                            <div id="divNo_OfDocs" class="col-12 text-center bottom averagedur_val averagedoc_text padding-none"></div>
                        </div>
                    </div>
                    <div class="col-12 float-left text-right padding-none top">
                        <button type="button" data-dismiss="modal" class="btn-cancel_popup">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--- Performance  modal--->

    <!--- Recommendation documents modal--->
   <%-- <div id="LoadDocumentRecommendedModal" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    Recommended on your Trainings            
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <table id="tblDocumentRecommended" class="datatable_cust display tblDeptDocumentsClass breakword" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Doc ID</th>
                                <th>S.NO</th>
                                <th>Training</th>
                                <th>No of Recommended Trainings</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>--%>
    <!---Recommendation documents  modal--->
    <!--- Training  Recommendation documents modal--->
    <div id="TrainingDocumentRecommendedModal" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    Trainings Recommended             
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <table id="tblTrainingDocumentRecommendedList" class="datatable_cust display tblDeptDocumentsClass breakword" style="width: 100%">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Your Trainings</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!---Training  Recommendation documents  modal--->
    <script>
        $("#btnAtcSubmit").click(function () {
            var AtcYear = $("#<%=ddlATCYear.ClientID%> :selected").text().trim();
            var DepartmentATC = $("#<%=ddlDepartmentATC.ClientID%> :selected").text().trim();
            var tiltle = "";
            if (DepartmentATC == "All Departments") {
                tiltle = "Refresher Training in " + AtcYear;
            }
            else {
                tiltle = "Refresher Training of " + DepartmentATC + " in " + AtcYear;
            }

            $("#lblAtcChartTitle").text(tiltle);

        });

        $("#btnSubmitCompletedandPending").click(function () {
            var CompletedYear = $("#<%=ddlCompletedYear.ClientID%> :selected").text().trim();
            var DepartmentCompleted = $("#<%=ddlDepartmentCompleted.ClientID%> :selected").text().trim();
            var tiltle = "";
            if (DepartmentCompleted == "All Departments") {
                tiltle = "Training Status in " + CompletedYear;
            }
            else {
                tiltle = "Training Status of " + DepartmentCompleted + " in " + CompletedYear;
            }

            $("#lblTrainingStatus").text(tiltle);
        });

        $("#btnOverDueSubmit").click(function () {
            var DepartmentOverDue = $("#<%=ddlDepartmentOverDue.ClientID%> :selected").text().trim();
            var tiltle = "";
            if (DepartmentOverDue == "All Departments") {
                tiltle = "Over Due Trainings on All Accessible Departments";
            }
            else {
                tiltle = "Over Due Trainings on " + DepartmentOverDue;
            }

            $("#lblOverDueTrainingChartTitle").text(tiltle);
        });

    </script>

    <!--JR and Target Pending/Completed Bar chart--->
    <script>     
        var _dataSource;
        function BarchartData() {
            UserSessionCheck();
            var formData = {
                Year: document.getElementById("<%=ddlCompletedYear.ClientID%>").value,
                EmpID: document.getElementById("<%=hdnEmpID.ClientID%>").value,
                DeptID: document.getElementById("<%=ddlDepartmentCompleted.ClientID%>").value
            };

            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": '<%=ResolveUrl("~/TMS/WebServices/DashboardCharts/DashboardCharts_Service.asmx/GetCompletedandPendingCount")%>',
                "data": formData,
                "success": function (msg) {
                    _dataSource = msg.d;
                    loadBarChart();
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }
        $("#btnSubmitCompletedandPending").on("click", function () {
            var CompletedYear = document.getElementById("<%=ddlCompletedYear.ClientID%>").value;
            var completedDepartments = document.getElementById("<%=ddlDepartmentCompleted.ClientID%>").value;
            if (CompletedYear != "" && completedDepartments != "") {
                if ($("#<%=rbtnOverallTrainee.ClientID%>").prop('checked')) {
                    BarchartData();
                }
                else {
                    TraineePendingData();
                }
            }
            else {
                errors = [];
                if (CompletedYear == "") {
                    errors.push("Select Year");
                }
                if (completedDepartments == "") {
                    errors.push("Select Department");
                }
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                }
            }
            CloseFilterDivs();
        });

        function loadBarChart() {
            $("#Barchart").dxChart({
                dataSource: JSON.parse(_dataSource),
                commonSeriesSettings: {
                    argumentField: "EventName",
                    type: "stackedBar",
                    barWidth: 50
                },
                series: [
                    {
                        valueField: "Completed",
                        name: "Completed",
                        label: {
                            visible: true,
                            position: "inside",
                            backgroundColor: "none",
                            customizeText: function (e) {
                                //if (e.valueText > 0) {
                                //    return "<span style='margin-top:px;font-size: 11px'>" + e.valueText + "";
                                //}
                            }
                        }
                    },
                    {
                        valueField: "Pending",
                        name: "Pending",
                        label: {
                            visible: true,
                            position: "inside",
                            backgroundColor: "none",
                            customizeText: function (e) {
                                //if (e.valueText > 0) {
                                //    return "<span style='margin-top:px;font-size: 11px'>" + e.valueText + "";
                                //}
                            }
                        }
                    }
                ],
                legend: {
                    verticalAlignment: "bottom",
                    horizontalAlignment: "center",
                    itemTextPosition: 'top'
                },
                title: " ",
                tooltip: {
                    enabled: true,
                    location: "edge",
                    customizeTooltip: function (arg) {
                        return {
                            text: arg.seriesName + " : " + arg.valueText
                        };
                    }
                },
                onPointClick: function (e) {
                    var point = e.target;
                    var argument = point.argument;
                    var val = point.series.name;
                    NavigateLinkForCompletedandPending(argument, val);
                }
            });
        }

        function NavigateLinkForCompletedandPending(argument, val) {
            var Year = document.getElementById("<%=ddlCompletedYear.ClientID%>").value;
            var DeptID = document.getElementById("<%=ddlDepartmentCompleted.ClientID%>").value
            if (argument == "JR" && val == "Pending") {
                var url = '<%=ResolveUrl("~/TMS/Reports/EmpJR_List.aspx?FilterType=1")%>' + '&Year=' + Year + '&DeptID=' + DeptID;
                window.open(url, "_self");
            }
            if (argument == "JR" && val == "Completed") {
                var url = '<%=ResolveUrl("~/TMS/Reports/EmpJR_List.aspx?FilterType=2")%>' + '&Year=' + Year + '&DeptID=' + DeptID;
                window.open(url, "_self");
            }
            if (argument == "Refresher" && val == "Pending") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=1")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=' + argument;
                window.open(url, "_self");
            }
            if (argument == "Refresher" && val == "Completed") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=2")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=' + argument;
                window.open(url, "_self");
            }
            if (argument == "New Document" && val == "Pending") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=1")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=NewDocument';
                window.open(url, "_self");
            }
            if (argument == "New Document" && val == "Completed") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=2")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=NewDocument';
                window.open(url, "_self");
            }
            if (argument == "Revision" && val == "Pending") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=1")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=' + argument;
                window.open(url, "_self");
            }
            if (argument == "Revision" && val == "Completed") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=2")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=' + argument;
                window.open(url, "_self");
            }
            if (argument == "General" && val == "Pending") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=1")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=' + argument;
                window.open(url, "_self");
            }
            if (argument == "General" && val == "Completed") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=2")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=' + argument;
                window.open(url, "_self");
            }
        }
    </script>

    <!--OverDue Dashboard Funnel chart-->
    <script>
        function FunnelOverDuechartData() {
            UserSessionCheck();
            var formData = {
                EmpID: document.getElementById("<%=hdnEmpID.ClientID%>").value,
                DeptID: document.getElementById("<%=ddlDepartmentOverDue.ClientID%>").value
            };
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": '<%=ResolveUrl("~/TMS/WebServices/DashboardCharts/DashboardCharts_Service.asmx/GetOverDueCount")%>',
                "data": formData,
                "success": function (result) {
                    data = $.parseJSON(result.d);
                    for (var i = 0; i < data.length; i++) {
                        if (data[0].EventName == "General" && data[0].OverDue == 0 && data[1].EventName == "JR" && data[1].OverDue == 0 &&
                            data[2].EventName == "New Document" && data[2].OverDue == 0 && data[3].EventName == "Refresher" && data[3].OverDue == 0 &&
                            data[4].EventName == "Revision" && data[4].OverDue == 0
                        ) {
                            $("#divNo_OverDuedetails").show();
                            $("#divOverDueDetails").hide();
                        }
                        else {
                            $("#divOverDueDetails").show();
                            $("#divNo_OverDuedetails").hide();
                            loadOverDueChart();
                        }
                    }
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }
        $("#btnOverDueSubmit").on("click", function () {
            var OverDueDepartments = document.getElementById("<%=ddlDepartmentOverDue.ClientID%>").value;
            if (OverDueDepartments != "") {
                if ($("#<%=rbtnOverall.ClientID%>").prop('checked')) {
                    FunnelOverDuechartData();
                }
                else {
                    TraineeOverDuechartData();
                }
            }
            else {
                errors = [];
                if (OverDueDepartments == "") {
                    errors.push("Select Department");
                }
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                }
            }
            CloseFilterDivs();
        });

        function loadOverDueChart() {
            $("#OverDueChart").dxFunnel({
                dataSource: data,
                title: {
                    //text: "Website Conversions",
                    //margin: 30
                },
                commonSeriesSettings: {
                    barWidth: 100
                },
                argumentField: "EventName",
                valueField: "OverDue",
                palette: "Soft Pastel",
                "export": {
                    enabled: false
                },
                tooltip: {
                    enabled: true,
                    customizeTooltip: function (arg) {
                        return {
                            text: arg.item.argument + " : " + arg.value
                        };
                    }
                },
                item: {
                    border: {
                        visible: true
                    }
                },
                label: {
                    visible: true,
                    position: "inside",
                    backgroundColor: "none",
                    customizeText: function (e) {
                        return "<span style='font-size: 14px'>" +
                            //e.item.argument +
                            //" : " +
                            e.item.value;

                    }
                },
                onItemClick: function (e) {
                    var argument = e.item.argument;
                    NavigateLinkForOverDueRecords(argument);
                }
            });

        }

        function NavigateLinkForOverDueRecords(argument) {
            var DeptID = document.getElementById("<%=ddlDepartmentOverDue.ClientID%>").value;
            var Status = "JR";
            var OverDueStatus = "OverDue";
            if (argument == "JR") {
                var url = '<%=ResolveUrl("~/TMS/Reports/EmpJR_List.aspx?FilterType=1")%>' + '&DeptID=' + DeptID + '&Status=' + Status;
                window.open(url, "_self");
            }
            if (argument == "Refresher") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=1")%>' + '&DeptID=' + DeptID + '&Status=' + OverDueStatus + '&TargetType=' + argument;
                window.open(url, "_self");
            }
            if (argument == "New Document") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=1")%>' + '&DeptID=' + DeptID + '&Status=' + OverDueStatus + '&TargetType=NewDocument';
                window.open(url, "_self");
            }
            if (argument == "Revision") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=1")%>' + '&DeptID=' + DeptID + '&Status=' + OverDueStatus + '&TargetType=' + argument;
                window.open(url, "_self");
            }
            if (argument == "General") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=1")%>' + '&DeptID=' + DeptID + '&Status=' + OverDueStatus + '&TargetType=' + argument;
                window.open(url, "_self");
            }
        }
    </script>

    <!--OverDue Count for Trainee--->
    <script>
        function TraineeOverDuechartData() {
            UserSessionCheck();
            var formData = {
                EmpID: document.getElementById("<%=hdnEmpID.ClientID%>").value,
                DeptID: document.getElementById("<%=ddlDepartmentOverDue.ClientID%>").value
            };

            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": '<%=ResolveUrl("~/TMS/WebServices/DashboardCharts/DashboardCharts_Service.asmx/GetTraineeOverDueCount")%>',
                "data": formData,
                "success": function (result) {
                    data = $.parseJSON(result.d);
                    for (var i = 0; i < data.length; i++) {
                        if (data[0].EventName == "General" && data[0].OverDue == 0 && data[1].EventName == "JR" && data[1].OverDue == 0 &&
                            data[2].EventName == "New Document" && data[2].OverDue == 0 && data[3].EventName == "Refresher" && data[3].OverDue == 0 &&
                            data[4].EventName == "Revision" && data[4].OverDue == 0
                        ) {
                            $("#divNo_OverDuedetails").show();
                            $("#divOverDueDetails").hide();
                        }
                        else {
                            $("#divOverDueDetails").show();
                            $("#divNo_OverDuedetails").hide();
                            loadTraineeOverDueChart();
                        }
                    }
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }

        function loadTraineeOverDueChart() {
            $("#OverDueChart").dxFunnel({
                dataSource: data,
                title: {
                    //text: "Website Conversions",
                    //margin: 30
                },
                commonSeriesSettings: {
                    barWidth: 100
                },
                argumentField: "EventName",
                valueField: "OverDue",
                palette: "Soft Pastel",
                "export": {
                    enabled: false
                },
                tooltip: {
                    enabled: true,
                    customizeTooltip: function (arg) {
                        return {
                            text: arg.item.argument + " : " + arg.value
                        };
                    }
                },
                item: {
                    border: {
                        visible: true
                    }
                },
                label: {
                    visible: true,
                    position: "inside",
                    backgroundColor: "none",
                    customizeText: function (e) {
                        return "<span style='font-size: 14px'>" +
                            //e.item.argument +
                            //" : " +
                            e.item.value;

                    }
                },
                onItemClick: function (e) {
                    var argument = e.item.argument;
                    NavigateLinkForTraineeOverDueRecords(argument);
                }
            });

        }

        function NavigateLinkForTraineeOverDueRecords(argument) {
            var DeptID = document.getElementById("<%=ddlDepartmentOverDue.ClientID%>").value;
            if (argument == "JR") {
                var url = '<%=ResolveUrl("~/TMS/Training/JR_Training/JR_TrainingList.aspx?FilterType=1")%>';
                window.open(url, "_self");
            }
            else {
                GetOverDueIsClassroomTrainingCount(argument, DeptID)
            }
        }
    </script>

    <!--Trainee Pending/Completed Count--->
    <script>
        var _dataSource;
        function TraineePendingData() {
            UserSessionCheck();
            var formData = {
                Year: document.getElementById("<%=ddlCompletedYear.ClientID%>").value,
                EmpID: document.getElementById("<%=hdnEmpID.ClientID%>").value,
                DeptID: document.getElementById("<%=ddlDepartmentCompleted.ClientID%>").value
            };

            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": '<%=ResolveUrl("~/TMS/WebServices/DashboardCharts/DashboardCharts_Service.asmx/GetTraineePendingCount")%>',
                "data": formData,
                "success": function (msg) {
                    _dataSource = msg.d;
                    loadTraineePendingChart();
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }

        function loadTraineePendingChart() {
            $("#Barchart").dxChart({
                dataSource: JSON.parse(_dataSource),
                commonSeriesSettings: {
                    argumentField: "EventName",
                    type: "stackedBar",
                    barWidth: 50
                },
                series: [
                    {
                        valueField: "Completed",
                        name: "Completed",
                        label: {
                            visible: true,
                            position: "inside",
                            backgroundColor: "none",
                            customizeText: function (e) {
                                //if (e.valueText > 0) {
                                //    return "<span style='margin-top:px;font-size: 11px'>" + e.valueText + "";
                                //}
                            }
                        }
                    },
                    {
                        valueField: "Pending",
                        name: "Pending",
                        label: {
                            visible: true,
                            position: "inside",
                            backgroundColor: "none",
                            customizeText: function (e) {
                                //if (e.valueText > 0) {
                                //    return "<span style='margin-top:px;font-size: 11px'>" + e.valueText + "";
                                //}
                            }
                        }
                    }
                ],
                legend: {
                    verticalAlignment: "bottom",
                    horizontalAlignment: "center",
                    itemTextPosition: 'top'
                },
                title: " ",
                tooltip: {
                    enabled: true,
                    location: "edge",
                    customizeTooltip: function (arg) {
                        return {
                            text: arg.seriesName + " : " + arg.valueText
                        };
                    }
                },
                onPointClick: function (e) {
                    var point = e.target;
                    var argument = point.argument;
                    var val = point.series.name;
                    NavigateLinkForTraineeCompletedandPending(argument, val);
                }
            });
        }

        function NavigateLinkForTraineeCompletedandPending(argument, val) {
            var Year = document.getElementById("<%=ddlCompletedYear.ClientID%>").value;
            var DeptID = document.getElementById("<%=ddlDepartmentCompleted.ClientID%>").value
            var Status = "TraineePending";
            if (val == "Pending" && argument != "JR") {
                GetIsClassroomTrainingCount(argument, Year, DeptID);
            }
            else {
                var url = "";
                if (argument == "JR" && val == "Pending") {
                    url = '<%=ResolveUrl("~/TMS/Training/JR_Training/JR_TrainingList.aspx?FilterType=1")%>' + '&Year=' + Year + '&DeptID=' + DeptID;
                }
                if (argument == "JR" && val == "Completed") {
                    url = '<%=ResolveUrl("~/TMS/Training/JR_Training/JR_TrainingList.aspx?FilterType=3")%>' + '&Year=' + Year + '&DeptID=' + DeptID;
                }
                if (argument == "Refresher" && val == "Completed") {
                    url = '<%=ResolveUrl("~/TMS/Reports/MyTrainingRecords.aspx?FilterType=1")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=' + argument;
                }
                if (argument == "New Document" && val == "Completed") {
                    url = '<%=ResolveUrl("~/TMS/Reports/MyTrainingRecords.aspx?FilterType=1")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=NewDocument';
                }
                if (argument == "Revision" && val == "Completed") {
                    url = '<%=ResolveUrl("~/TMS/Reports/MyTrainingRecords.aspx?FilterType=1")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=' + argument;
                }
                if (argument == "General" && val == "Completed") {
                    url = '<%=ResolveUrl("~/TMS/Reports/MyTrainingRecords.aspx?FilterType=1")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=' + argument;
                }
                window.open(url, "_self");
            }
        }
    </script>

    <!--To get Is Classroom Training count-->
    <script>
        function GetIsClassroomTrainingCount(argument, Year, DeptID) {
            var formData = {
                EmpID: document.getElementById("<%=hdnEmpID.ClientID%>").value,
                TargetType: argument
            };
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": '<%=ResolveUrl("~/TMS/WebServices/DashboardCharts/DashboardCharts_Service.asmx/GetClassroomTrainingCount")%>',
                "data": formData,
                "success": function (msg) {
                    var json = jQuery.parseJSON(msg.d);
                    var TotalRecords = json["iTotalRecordsCount"];
                    var IsClassroomExist = false;
                    if (TotalRecords == 0) {
                        IsClassroomExist = true;
                    }
                    var url = "";
                    if (argument == "New Document") {
                        url = '<%=ResolveUrl("~/TMS/Training/TargetTraining.aspx?FilterType=1")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=NewDocument' + '&Status=TraineePending' + '&IsClassroomExist=' + IsClassroomExist;
                    }
                    else {
                        url = '<%=ResolveUrl("~/TMS/Training/TargetTraining.aspx?FilterType=1")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=' + argument + '&Status=TraineePending' + '&IsClassroomExist=' + IsClassroomExist;
                    }
                    window.open(url, "_self");
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }
    </script>

    <!--To get OverDue Is Classroom Training count-->
    <script>
        function GetOverDueIsClassroomTrainingCount(argument, DeptID) {
            var formData = {
                EmpID: document.getElementById("<%=hdnEmpID.ClientID%>").value,
                TargetType: argument
            };
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": '<%=ResolveUrl("~/TMS/WebServices/DashboardCharts/DashboardCharts_Service.asmx/GetClassroomTrainingCount")%>',
                "data": formData,
                "success": function (msg) {
                    var json = jQuery.parseJSON(msg.d);
                    var TotalRecords = json["iTotalRecordsCount"];
                    var IsClassroomExist = false;
                    if (TotalRecords == 0) {
                        IsClassroomExist = true;
                    }
                    var url = "";
                    if (argument == "New Document") {
                        url = '<%=ResolveUrl("~/TMS/Training/TargetTraining.aspx?FilterType=1")%>' + '&DeptID=' + DeptID + '&TargetType=NewDocument' + '&IsClassroomExist=' + IsClassroomExist;
                    }
                    else {
                        var url = '<%=ResolveUrl("~/TMS/Training/TargetTraining.aspx?FilterType=1")%>' + '&DeptID=' + DeptID + '&TargetType=' + argument + '&IsClassroomExist=' + IsClassroomExist;
                    }
                    window.open(url, "_self");
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }
    </script>

    <!--ATC Dashboard Barchart-->
    <script>
        var _ATCdataSource;
        function ATC_chartData() {
            UserSessionCheck();
            var Year = document.getElementById("<%=ddlATCYear.ClientID%>").value;
            var formData = {
                Year: document.getElementById("<%=ddlATCYear.ClientID%>").value,
                EmpID: document.getElementById("<%=hdnEmpID.ClientID%>").value,
                DeptID: document.getElementById("<%=ddlDepartmentATC.ClientID%>").value
            };

            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": '<%=ResolveUrl("~/TMS/WebServices/DashboardCharts/DashboardCharts_Service.asmx/GetCompletedandPendingCountForATC")%>',
                "data": formData,
                "success": function (msg) {
                    _ATCdataSource = msg.d;
                    loadATC_Chart();
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }
        $("#btnAtcSubmit").on("click", function () {
            var ATC_Year = document.getElementById("<%=ddlATCYear.ClientID%>").value;
            errors = [];
            if (ATC_Year == "") {
                errors.push("Select Year");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
            } else {
                ATC_chartData();
                CloseFilterDivs();
            }

        });

        function loadATC_Chart() {
            $(function () {
                $("#ATCBarchart").dxChart({
                    dataSource: JSON.parse(_ATCdataSource),
                    commonSeriesSettings: {
                        argumentField: "MonthName",
                        barWidth: 50
                    },
                    panes: [{
                        name: "topPane"
                    }, {
                        name: "bottomPane"
                    }],
                    defaultPane: "bottomPane",
                    series: [
                        {
                            pane: "topPane",
                            valueField: "Completed",
                            name: "Completed",
                            label: {
                                visible: true,
                                customizeText: function () {
                                    return this.valueText + "";
                                }
                            }
                        }, {
                            type: "bar",
                            valueField: "Pending",
                            name: "Pending",
                            label: {
                                visible: true,
                                customizeText: function () {
                                    return this.valueText + "";
                                }
                            }
                        }
                    ],
                    valueAxis: [
                        {
                            pane: "bottomPane",
                            grid: {
                                visible: true
                            },
                            title: {
                                text: "Pending"
                            }
                        },
                        {
                            pane: "topPane",
                            grid: {
                                visible: true
                            },
                            title: {
                                text: "Completed"
                            }
                        }],
                    tooltip: {
                        enabled: true,
                        customizeTooltip: function (arg) {
                            return {
                                text: arg.seriesName + " : " + arg.value
                            };
                        }
                    },
                    legend: {
                        verticalAlignment: "bottom",
                        horizontalAlignment: "center"
                    },
                    onPointClick: function (e) {
                        var point = e.target;
                        var argument = point.index + 1;
                        var val = point.series.name;
                        var CompletedValue = point.originalValue;
                        if (CompletedValue != 0) {
                            NavigateLinkForATCRefresher(argument, val);
                        }
                    }
                });
            });
        }

        function NavigateLinkForATCRefresher(argument, val) {
            var Year = document.getElementById("<%=ddlATCYear.ClientID%>").value;
            var DeptID = document.getElementById("<%=ddlDepartmentATC.ClientID%>").value;
            var TargetType = "ATC";
            if (val == "Completed") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=2")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=' + TargetType + '&Month=' + argument;
                window.open(url, "_self");
            }
            if (val == "Pending") {
                var url = '<%=ResolveUrl("~/TMS/Reports/TargetTS_List.aspx?FilterType=1")%>' + '&Year=' + Year + '&DeptID=' + DeptID + '&TargetType=' + TargetType + '&Month=' + argument;
                window.open(url, "_self");
            }
        }
    </script>

    <!--Chart Filter Dropdown-->
    <script>
        function CloseFilterDivs() {
            $(".filter_dropdown").hide();
        }

        function OpenCharFilter(OpenFilter) {
            $(".filter_dropdown:not(#" + OpenFilter + ")").hide();
            $("#" + OpenFilter).toggle();
        }
    </script>

    <!----To refresh charts--->
    <script>           
        $('#RefreshCompletedandPending').click(function () {
            if ($("#<%=rbtnOverallTrainee.ClientID%>").prop('checked')) {
                loadBarChart();
            } else {
                loadTraineePendingChart();
            }
        });
        $('#RefreshOverDue').click(function () {
            if ($("#<%=rbtnOverall.ClientID%>").prop('checked')) {
                loadOverDueChart();
            } else {
                loadTraineeOverDueChart();
            }
        });
        $('#RefreshATC').click(function () {
            loadATC_Chart();
        });
        $('#RefreshTrainingPerformanceList').click(function () {
            loadTrainingPerformanceList();
        });
    </script>

    <!--To Load Trainee Assigned Departments-->
    <script>
        function TraineeDepartments(DeptType) {
            var formData = {
                EmpID: document.getElementById("<%=hdnEmpID.ClientID%>").value
            };
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": '<%=ResolveUrl("~/TMS/WebServices/DashboardCharts/DashboardCharts_Service.asmx/GetAssignedDepartments")%>',
                "data": formData,
                "success": function (result) {
                    var Departmentsdata = JSON.parse(result.d);
                    if (DeptType == "DepartmentCompleted") {// For Training Departments
                        $("#<%=ddlDepartmentCompleted.ClientID%>").empty();
                        $("#<%=ddlDepartmentCompleted.ClientID%>").append("<option value='0'>All Departments</option>");
                        $(Departmentsdata).each(function () {
                            $("#<%=ddlDepartmentCompleted.ClientID%>").append($("<option></option>").val(this.DeptID).html(this.DepartmentName));
                        });
                        $("#<%=ddlDepartmentCompleted.ClientID%>").val('default');
                        $("#<%=ddlDepartmentCompleted.ClientID%>").selectpicker("refresh");
                    }
                    else if (DeptType == "DepartmentOverdue") {//For OverDue Departments
                        $("#<%=ddlDepartmentOverDue.ClientID%>").empty();
                        $("#<%=ddlDepartmentOverDue.ClientID%>").append("<option value='0'>All Departments</option>");
                        $(Departmentsdata).each(function () {
                            $("#<%=ddlDepartmentOverDue.ClientID%>").append($("<option></option>").val(this.DeptID).html(this.DepartmentName));
                        });
                        $("#<%=ddlDepartmentOverDue.ClientID%>").val('default');
                        $("#<%=ddlDepartmentOverDue.ClientID%>").selectpicker("refresh");
                    }
                    else {// For Both
                        $("#<%=ddlDepartmentOverDue.ClientID%>").empty();
                        $("#<%=ddlDepartmentCompleted.ClientID%>").empty();
                        $(Departmentsdata).each(function () {
                            $("#<%=ddlDepartmentOverDue.ClientID%>").append($("<option></option>").val(this.DeptID).html(this.DepartmentName));
                            $("#<%=ddlDepartmentCompleted.ClientID%>").append($("<option></option>").val(this.DeptID).html(this.DepartmentName));
                        });
                        $("#<%=ddlDepartmentCompleted.ClientID%>").val('default');
                        $("#<%=ddlDepartmentCompleted.ClientID%>").selectpicker("refresh");
                        $("#<%=ddlDepartmentOverDue.ClientID%>").val('default');
                        $("#<%=ddlDepartmentOverDue.ClientID%>").selectpicker("refresh");
                    }
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }
    </script>

    <!--To Load overall Departments-->
    <script>
        function OverallDepartments(DeptType) {
            var formData = {
                EmpID: document.getElementById("<%=hdnEmpID.ClientID%>").value
            };
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": '<%=ResolveUrl("~/TMS/WebServices/DashboardCharts/DashboardCharts_Service.asmx/GetOverallDepartments")%>',
                "data": formData,
                "success": function (result) {
                    var Departmentsdata = JSON.parse(result.d);
                    if (DeptType == "DepartmentCompleted") { // For Training Departments
                        $("#<%=ddlDepartmentCompleted.ClientID%>").empty();
                        $("#<%=ddlDepartmentCompleted.ClientID%>").append("<option value='0'>All Departments</option>");
                        $(Departmentsdata).each(function () {
                            $("#<%=ddlDepartmentCompleted.ClientID%>").append($("<option></option>").val(this.DeptID).html(this.DepartmentName));
                        });
                        $("#<%=ddlDepartmentCompleted.ClientID%>").val('default');
                        $("#<%=ddlDepartmentCompleted.ClientID%>").selectpicker("refresh");
                    }
                    else if (DeptType == "DepartmentOverdue") { //For OverDue Departments
                        $("#<%=ddlDepartmentOverDue.ClientID%>").empty();
                        $("#<%=ddlDepartmentOverDue.ClientID%>").append("<option value='0'>All Departments</option>");
                        $(Departmentsdata).each(function () {
                            $("#<%=ddlDepartmentOverDue.ClientID%>").append($("<option></option>").val(this.DeptID).html(this.DepartmentName));
                        });
                        $("#<%=ddlDepartmentOverDue.ClientID%>").val('default');
                        $("#<%=ddlDepartmentOverDue.ClientID%>").selectpicker("refresh");
                    }
                    else { //For Both
                        $("#<%=ddlDepartmentOverDue.ClientID%>").empty();
                        $("#<%=ddlDepartmentCompleted.ClientID%>").empty();
                        $(Departmentsdata).each(function () {
                            $("#<%=ddlDepartmentOverDue.ClientID%>").append($("<option></option>").val(this.DeptID).html(this.DepartmentName));
                            $("#<%=ddlDepartmentCompleted.ClientID%>").append($("<option></option>").val(this.DeptID).html(this.DepartmentName));
                        });
                        $("#<%=ddlDepartmentCompleted.ClientID%>").val('default');
                        $("#<%=ddlDepartmentCompleted.ClientID%>").selectpicker("refresh");
                        $("#<%=ddlDepartmentOverDue.ClientID%>").val('default');
                        $("#<%=ddlDepartmentOverDue.ClientID%>").selectpicker("refresh");
                    }
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }
    </script>

    <!--To load departments based on Radio button selection-->
    <script>
        function RefreshTraineeAssignedDepartments(TrainingType) {
            if (TrainingType == "MyTraining") {
                TraineeDepartments('DepartmentCompleted');
            }
            else if (TrainingType == "OverAllDepartments") {
                OverallDepartments('DepartmentCompleted')
            }
            else if (TrainingType == "MyOverdueTraining") {
                TraineeDepartments('DepartmentOverdue');
            }
            else if (TrainingType == "MyOverdueAllDepartments") {
                OverallDepartments('DepartmentOverdue')
            }
        }
    </script>

    <!--ATC Card-->
    <script>       
        $("#divCloseAtcDocsList").on("click", function () {
            $("#divAtcDocsList").hide();
            $(".calender_atc").show();
        });
        $('.cardNormal').click(function () {
            $(".calender_atc_main").hide();
        });

        $('#<%=ddlATC_Departments.ClientID%>').on("change", function () {
            $("#divAtcDocsList").hide();
            var atcDeptID = $('#<%=ddlATC_Departments.ClientID%>').val();
            if (atcDeptID == "0") {
                $("#ulCalendar").hide();
            }
            else {
                $("#ulCalendar").hide();
                loadDocCountByDept(atcDeptID);
            }
        });

        function getDocsOn(monthNum, control) {
            $("#divAtcDocsList").hide();
            if ($(control).hasClass('active')) {
                $(".calender_atc").hide();
                $("#Atc_Main").show();
                var vAtcDocsListTitle = "";
                var DeptID = $('#<%=ddlATC_Departments.ClientID%>').val();
                switch (monthNum) {
                    case 1:
                        vAtcDocsListTitle = "January";
                        break;
                    case 2:
                        vAtcDocsListTitle = "February";
                        break;
                    case 3:
                        vAtcDocsListTitle = "March";
                        break;
                    case 4:
                        vAtcDocsListTitle = "April";
                        break;
                    case 5:
                        vAtcDocsListTitle = "May";
                        break;
                    case 6:
                        vAtcDocsListTitle = "June";
                        break;
                    case 7:
                        vAtcDocsListTitle = "July";
                        break;
                    case 8:
                        vAtcDocsListTitle = "August";
                        break;
                    case 9:
                        vAtcDocsListTitle = "September";
                        break;
                    case 10:
                        vAtcDocsListTitle = "October";
                        break;
                    case 11:
                        vAtcDocsListTitle = "November";
                        break;
                    case 12:
                        vAtcDocsListTitle = "December";
                        break;
                }
                loadATC_DocsList(monthNum, DeptID);
                $("#AtcDocsListTitle").text(vAtcDocsListTitle);
                $("#divAtcDocsList").show();
            }
        }

        function loadDocCountByDept(atcDeptID) {
            UserSessionCheck();
            var objStr = { "DeptID": atcDeptID };
            var jsonData = JSON.stringify(objStr);
            $.ajax({
                type: "POST",
                url: '<%= Page.ResolveUrl("~/TMS/WebServices/TmsDashboard/TMS_Dashboard.asmx/GetATC_DocCountforEachMonthByDept")%>',
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var jsonResult = $.parseJSON(response.d);
                    if (jsonResult.length != 0) {
                        var JanCount = jsonResult["Jan"];
                        var FebCount = jsonResult["Feb"];
                        var MarCount = jsonResult["Mar"];
                        var AprCount = jsonResult["Apr"];
                        var MayCount = jsonResult["May"];
                        var JuneCount = jsonResult["June"];
                        var JulyCount = jsonResult["July"];
                        var AugCount = jsonResult["Aug"];
                        var SeptCount = jsonResult["Sept"];
                        var OctCount = jsonResult["Oct"];
                        var NovCount = jsonResult["Nov"];
                        var DecCount = jsonResult["Dec"];

                        $('#janCount').text(JanCount);
                        $('#febCount').text(FebCount);
                        $('#marCount').text(MarCount);
                        $('#aprCount').text(AprCount);
                        $('#mayCount').text(MayCount);
                        $('#junCount').text(JuneCount);
                        $('#julCount').text(JulyCount);
                        $('#augCount').text(AugCount);
                        $('#sepCount').text(SeptCount);
                        $('#octCount').text(OctCount);
                        $('#novCount').text(NovCount);
                        $('#decCount').text(DecCount);

                        parseInt(JanCount) > 0 ? $('#janCount').parent().addClass('active') : $('#janCount').parent().removeClass('active');
                        parseInt(FebCount) > 0 ? $('#febCount').parent().addClass('active') : $('#febCount').parent().removeClass('active');
                        parseInt(MarCount) > 0 ? $('#marCount').parent().addClass('active') : $('#marCount').parent().removeClass('active');
                        parseInt(AprCount) > 0 ? $('#aprCount').parent().addClass('active') : $('#aprCount').parent().removeClass('active');
                        parseInt(MayCount) > 0 ? $('#mayCount').parent().addClass('active') : $('#mayCount').parent().removeClass('active');
                        parseInt(JuneCount) > 0 ? $('#junCount').parent().addClass('active') : $('#junCount').parent().removeClass('active');
                        parseInt(JulyCount) > 0 ? $('#julCount').parent().addClass('active') : $('#julCount').parent().removeClass('active');
                        parseInt(AugCount) > 0 ? $('#augCount').parent().addClass('active') : $('#augCount').parent().removeClass('active');
                        parseInt(SeptCount) > 0 ? $('#sepCount').parent().addClass('active') : $('#sepCount').parent().removeClass('active');
                        parseInt(OctCount) > 0 ? $('#octCount').parent().addClass('active') : $('#octCount').parent().removeClass('active');
                        parseInt(NovCount) > 0 ? $('#novCount').parent().addClass('active') : $('#novCount').parent().removeClass('active');
                        parseInt(DecCount) > 0 ? $('#decCount').parent().addClass('active') : $('#decCount').parent().removeClass('active');

                        $("#ulCalendar").show();
                    }
                },
                failure: function (response) {
                    // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
                },
                error: function (response) {
                    custAlertMsg('Internal Error Occured on loading ATC Details', 'error');
                }
            });
        }

        function loadATC_DocsList(monthNum, DeptID) {
            UserSessionCheck();
            var objStr = { "DeptID": DeptID, "MonthNo": monthNum };
            var jsonData = JSON.stringify(objStr);
            $.ajax({
                type: "POST",
                url: '<%= Page.ResolveUrl("~/TMS/WebServices/TmsDashboard/TMS_Dashboard.asmx/GetATC_DocDetailsByDeptAndMonth")%>',
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    BindATC_DocDetails(response.d);
                },
                failure: function (response) {
                    // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
                },
                error: function (response) {
                    custAlertMsg('Internal Error Occured on loading ATC Document Details', 'error');
                }
            });
        }

        function BindATC_DocDetails(jsonResult) {
            var objJsonAtcDocList = $.parseJSON(jsonResult);
            var objATC_DocDetails = objJsonAtcDocList['DocumentDetails'];

            $("#tblAtcDocsList tbody").empty();
            if (objATC_DocDetails.length == 0) {
                $("#tblAtcDocsList tbody").prepend("<tr><td>No Document(s) to Show</td></tr>");
            }
            else {
                var trATC_DocList = "";
                $.each(objATC_DocDetails, function (index, itemObj) {
                    trATC_DocList += "<tr><td>" + itemObj.Document + "</td><td>" + itemObj.DocumentType + "</td></tr>";
                });
                $("#tblAtcDocsList tbody").append(trATC_DocList);
            }
        }
    </script>

    <!--To load Charts-->
    <script>
        if (document.getElementById("<%=hdnLoadCharts.ClientID%>").value == "true") {
            $('#divTmsDashboard').show();
            var ATC = "AtcDetails";
            var JRandTarget = "Completed";
            var CompletedYear = document.getElementById("<%=ddlCompletedYear.ClientID%>").value;
            var ATC_Year = document.getElementById("<%=ddlATCYear.ClientID%>").value;
            if (ATC = "AtcDetails") {
                if (ATC_Year == 0) {
                    $("#divAtcDetails").hide();
                    $("#divNoAtcDetails").show();

                }
                else {
                    $("#divAtcDetails").show();
                    $("#divNoAtcDetails").hide();
                    ATC_chartData();
                }
            }
            if (JRandTarget = "Completed") {
                if (CompletedYear == 0) {
                    $("#NoCompletedrecords").show();
                    $("#divCompletedrecords").hide();
                }
                else {
                    $("#divCompletedrecords").show();
                    $("#NoCompletedrecords").hide();
                    BarchartData();
                }
            }
            FunnelOverDuechartData();
        }
        else {
            $('#divTmsDashboard').hide();
        }

    </script>

    <!--To Hide/Show radio buttons based on Roles--->
    <script>
        var CompletedYear = document.getElementById("<%=ddlCompletedYear.ClientID%>").value;
        if (document.getElementById("<%=hdnToAccessRoles.ClientID%>").value == "1") {  //All Roles.
            $("#<%=rbtnOverall.ClientID%>").prop('checked', true);
            $("#<%=rbtnOverallTrainee.ClientID%>").prop('checked', true);
            $('#liOverDueradiobuttons').show();
            $('#liTraineePending').show();
        }
        else if (document.getElementById("<%=hdnToAccessRoles.ClientID%>").value == "2") { // Without Trainee Role.
            $("#<%=rbtnOverall.ClientID%>").prop('checked', true);
            $("#<%=rbtnOverallTrainee.ClientID%>").prop('checked', true);
            $('#liOverDueradiobuttons').hide();
            $('#liTraineePending').hide();
        }
        else if (document.getElementById("<%=hdnToAccessRoles.ClientID%>").value == "3") { //Only Trainee Role.
            $("#<%=rbtnTrainee.ClientID%>").prop('checked', true);
            $("#<%=rbtnPendingTrainee.ClientID%>").prop('checked', true);
            $('#divTmsDashboard').show();
            $('#divAtc_Chart').hide();
            $('#liOverDueradiobuttons').hide();
            $('#liTraineePending').hide();
            if (CompletedYear != "") {
                TraineePendingData();
            } else {
                $("#NoCompletedrecords").show();
                $("#divCompletedrecords").hide();
            }
            TraineeOverDuechartData();
        }
    </script>

    <script>
        $(function () {
            $('.demo').percentcircle({
                animate: true,
                diameter: 100,
                guage: 20,
                coverBg: '#fff',
                bgColor: '#efefef',
                fillColor: '#5c93c8',
                percentSize: '15px',
                percentWeight: 'normal'

            });


            $("#ImgTraineePercentageList").on("click", function () {
                $("#OverallPercentageCard,#carddiv").show();
                $("#ImgTraineePercentageList,#divTraineePercentage").hide();
            });
            $("#OverallPercentageCard").on("click", function () {
                $("#ImgTraineePercentageList,#divTraineePercentage").toggle();
                $("#OverallPercentageCard,#carddiv").toggle();
            });


            $(".performance-popup").addClass('performance-popup-right');
            var contwidth = $(".performancepopup-content").outerWidth() + 2;
            $(".performance-popup").css("right", "-" + 316 + "px");
            $(".performance-popup").css("visibility", "visible");
            $(".performancepopup-header").click(function () {
                $(".sticky-popup").addClass("sticky-placeholder");
                if ($('.performance-popup').hasClass("open")) {
                    $(".sticky-popup").removeClass("sticky-placeholder");
                    $('.performance-popup').removeClass("open");
                    $(".performance-popup").css("right", "-" + 316 + "px");

                }
                else {
                    $('.performance-popup').addClass("open");
                    $(".performance-popup").css("right", 0);
                }
                if ($('.sticky-popup').hasClass("open")) {
                    $('.sticky-popup').removeClass("open");
                    $(".sticky-popup").css("right", "-" + 316 + "px");
                }

            });
            $("#closeButton").click(function () {
                $('.performance-popup').removeClass("open");
                $(".performance-popup").css("right", "-" + 316 + "px");
            });
        });
    </script>
    <!--- Trainee Performance Script Ends--->

    <!--TMS ML Trainee Performance-->
    <script>
        $(function () {
            //ml feature checking 
            if (document.getElementById("<%=hdnMLTrainingPerformace_Enable.ClientID%>").value == "1") {
                TMS_ML_Performance();
            }
        });

        // TMS_ML_Performance();// whenever want to check the trainee perfomance uncomment this function 
        function TMS_ML_Performance() {
            if (document.getElementById("<%=hdnTMS_TrainingTrainee.ClientID%>").value == "6")//hdnTMS_TrainingTrainee alias Trainee  Role
            {
                ToGetTraineeOverallPerformance(document.getElementById("<%=hdnEmpID.ClientID%>").value);//parament Passing Employee ID
            }
            if (document.getElementById("<%=hdnTMS_TrainingCoordinator.ClientID%>").value == "3") // Training Coordinator Role
            {
                $("#divTrainingPerformanceDetails").show();
                loadTrainingPerformanceList();
            }
        }

        function ToGetTraineeOverallPerformance(EmpID) {
            $.ajax({
                type: "GET",
                url: AizantIT_WebAPI_URL + "/api/TMS_ML/GetTraineePerformance?EmpID=" + EmpID,
                contentType: "application/json; charset=utf-8",
                headers: WebAPIRequestHeaderJson,// localStorage.getItem('token') },
                dataType: "json",
                "success": function (data) {
                    if (data.length != 0) {
                        $("#divTraineePerformanceDetails").show();
                        var Overall_Performance = data[0].Overall_Performance;
                        var AvgDuration = " : " + data[0].Avg_Duration;
                        var AvgMarks = " : " + data[0].Avg_Marks;
                        var No_of_times_Failed = " : " + data[0].No_of_times_Failed;
                        var No_of_Documents = " : " + data[0].No_of_Documents;
                        GetTraineeOverallPerformance(Overall_Performance, AvgDuration, AvgMarks, No_of_times_Failed, No_of_Documents);
                    }
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        }
        function GetTraineeOverallPerformance(Overall_Performance, AvgDuration, AvgMarks, No_of_times_Failed, No_of_Documents) {
            $('#TraineeOverallPercentage  .perc').text(Overall_Performance);
            $('#TraineeAvgDuration').html(AvgDuration);
            $('#TraineeAvgMarks').html(AvgMarks);
            $('#TraineeNo_OfAttempts').html(No_of_times_Failed);
            $('#TraineeNo_OfDocs').html(No_of_Documents);
        }
    </script>

    <!--Recommendations STICKY POPUP-->
    <script>
        $(function () {
            $(".sticky-popup").addClass('sticky-popup-right');
            var contwidth = $(".popup-content").outerWidth() + 2;
            $(".sticky-popup").css("right", "-" + 316 + "px");
            $(".sticky-popup").css("visibility", "visible");
            $(".popup-header").click(function () {
                $(".sticky-popup").removeClass("sticky-placeholder");
                if ($('.sticky-popup').hasClass("open")) {
                    $('.sticky-popup').removeClass("open");
                    $(".sticky-popup").css("right", "-" + 316 + "px");
                }
                else {
                    $('.sticky-popup').addClass("open");
                    $(".sticky-popup").css("right", 0);
                }
                if ($('.performance-popup').hasClass("open")) {
                    $('.performance-popup').removeClass("open");
                    $(".performance-popup").css("right", "-" + 316 + "px");
                }
            });
            $("#closeButton").click(function () {
                $('.sticky-popup').removeClass("open");
                $(".sticky-popup").css("right", "-" + 316 + "px");
            });
        });

    </script>
    <!--Recommendations STICKY POPUP-->

    <!--ML Training Performance List--->
    <script>
        var DeptID = 0;
        function GetDeptWiseTrainingPerformance() {
            DeptID = $("#<%=ddlTrainingPerformanceDept.ClientID%>").val();
            loadTrainingPerformanceList();
        }
        function ToGetTraineePerformance(EmpName, AvgDuration, AvgMarks, No_of_times_Failed, No_of_Documents) {
            $('#TraineePerformancemodal').modal('show');
            $('#TraineeName').html(EmpName);
            $('#divTraineeAvgDuration').html(AvgDuration);
            $('#divAvgMarks').html(AvgMarks);
            $('#divNo_OfTimesFailed').html(No_of_times_Failed);
            $('#divNo_OfDocs').html(No_of_Documents);
        }
        var oTableTrainingPerformance;
        function loadTrainingPerformanceList() {
            if (oTableTrainingPerformance != null) {
                oTableTrainingPerformance.destroy();
            }
            oTableTrainingPerformance = $('#tblTrainingPerformaceDetails').DataTable({
                columns: [
                    { 'data': 'EmpName' },
                    { 'data': 'Avg_Duration' },
                    { 'data': 'Avg_Marks' },
                    { 'data': 'No_of_times_Failed' },
                    { 'data': 'No_of_Documents' },
                    {
                        "mData": null,
                        // "bSortable": false,
                        "mRender": function (o) {
                            return '<a class="grid_button traineeperformancedt" title="Trainee Percentage" onclick="ToGetTraineePerformance(\'' + o.EmpName.replace(/'/g, "\\'") + '\',\'' + o.Avg_Duration.replace(/'/g, "\\'") + '\',' + o.Avg_Marks + ',' + o.No_of_times_Failed + ',' + o.No_of_Documents + ');">' + o.Overall_Performance + '</a>';
                        }
                    }
                ],
                "scrollY": "285px",
                //"bServerSide": true,
                "orderCellsTop": true,
                "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
                "aoColumnDefs": [{ className: "textAlignLeft", "targets": [0] }, { "targets": [1, 2, 3, 4], "visible": false }],
                "sAjaxSource": AizantIT_WebAPI_URL + "/api/TMS_ML/GetTrainingPerformance",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "EmpID", "value": $("#<%=hdnEmpID.ClientID%>").val() }, { "name": "DeptID", "value": DeptID });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (result) {
                            fnCallback(result);
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
    </script>
    <!--Document Recommended Feature-->
    <script>
        <%--if (((document.getElementById("<%=hdnTMS_DocumentTrainingRecommendations.ClientID%>").value == "1") && (document.getElementById("<%=hdnTMS_TrainingTrainee.ClientID%>").value == "6"))) {
            //$('#divDocumentRecommendation').show();
            $('#spnRecommendedEmpName').html('<%= (Session["UserDetails"] as System.Data.DataSet).Tables[0].Rows[0]["EmpName"].ToString() %>');
            //var count = document.getElementById("<%=hdnTMS_DocumentRecommendedCount.ClientID%>").value;
            //$('.divDocumentRecommendationCount').html(6)
        }--%>
        //var oTblDocumentRecommended;
        //function LoadDocumentRecommended() {
        //    if (oTblDocumentRecommended != null) {
        //        oTblDocumentRecommended.destroy();
        //    }
        //    oTblDocumentRecommended = $('#tblDocumentRecommended').DataTable({
        //        columns: [
        //            { 'data': 'DocVersionID' },
        //            { 'data': 'RowNumber' },
        //            { 'data': 'DocumentName' },
        //            {
        //                "mData": null,
        //                "bSortable": false,
        //                "mRender": function (o) { return '<a  href="#" class="grid_button" onclick="ViewTrainingDocumentRecommendedList(' + o.DocVersionID + ');" >' + o.DocRecommendationCount + '</a>'; }
        //            }
        //        ],
        //        "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
        //        "aoColumnDefs": [{ "targets": [0, 1], "visible": false, "searchable": false },
        //        { className: "textAlignLeft", "targets": [2] },{ "bSortable": false, aTargets: [1] }],
        //        "sAjaxSource": AizantIT_WebAPI_URL + "/api/API_ML_Integration/GetDocumentRecomendedList",
        //        "fnServerData": function (sSource, aoData, fnCallback) {
        //            $.ajax({
        //                "dataType": 'json',
        //                "contentType": "application/json; charset=utf-8",
        //                "type": "GET",
        //                "url": sSource,
        //                "data": aoData,
        //                "success": function (result) {
        //                    fnCallback(result);
        //                },
        //                error: function (xhr, textStatus, error) {
        //                    if (typeof console == "object") {
        //                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
        //                    }
        //                }
        //            });
        //        }
        //    });
        //}
        //$('#LoadDocumentRecommendedModal').on('shown.bs.modal', function (e) {
        //    LoadDocumentRecommended();
        //});
//Training Document Recommended
        //var _DocVersionID = 0;
        //var ViewTrainingDocumentRecommendedList = function (DocVersionID) {
        //    _DocVersionID = DocVersionID;
        //    $('#TrainingDocumentRecommendedModal').modal('show');

        //}
        $('#TrainingDocumentRecommendedModal').on('shown.bs.modal', function (e) {
            LoadTrainingDocumentRecommendedList();
        });

        var oTrainingDocumentRecommendedList;
        function LoadTrainingDocumentRecommendedList() {
            if (oTrainingDocumentRecommendedList != null) {
                oTrainingDocumentRecommendedList.destroy();
            }
            oTrainingDocumentRecommendedList = $('#tblTrainingDocumentRecommendedList').DataTable({
                columns: [
                    { 'data': 'RowNumber' },
                    { 'data': 'DocumentName' }
                ],
                "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
                "order": [[ 1, "desc" ]],
                "aoColumnDefs": [
                    { className: "textAlignLeft", "targets": [1] },{ "bSortable": false, aTargets: [0] }],

                "sAjaxSource": AizantIT_WebAPI_URL + "/api/API_ML_Integration/GetTrainingDocumentRecommendedList?TraineeID="+document.getElementById("<%=hdnEmpID.ClientID%>").value,
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (result) {
                            fnCallback(result);
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }

    </script>
    <script>
        $(function () {
            var EmpID = document.getElementById("<%=hdnEmpID.ClientID%>").value;
            $.ajax({
                type: "GET",
                url: AizantIT_WebAPI_URL + "/api/API_ML_Integration/GetDocumentRecommendedCount?TraineeID=" + EmpID,
                contentType: "application/json; charset=utf-8",
                headers: WebAPIRequestHeaderJson,
                dataType: "json",
                "success": function (data) {
                    if (data > 0) {
                        if (((document.getElementById("<%=hdnTMS_DocumentTrainingRecommendations.ClientID%>").value == "1") && (document.getElementById("<%=hdnTMS_TrainingTrainee.ClientID%>").value == "6"))) {
                            $('#divDocumentRecommendation').show();
                            $('#spnRecommendedEmpName').html('<%= (Session["UserDetails"] as System.Data.DataSet).Tables[0].Rows[0]["EmpName"].ToString() %>');
                            $('.divDocumentRecommendationCount').html(data)
                        }
                    }
                }
            }); 
        });

    </script>
</asp:Content>
