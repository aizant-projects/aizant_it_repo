﻿<%@ Page Title="Trainer Evaluation" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="TrainerEvaluation.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Training.TrainerEvaluation1" %>

<%@ Register Src="~/UserControls/SessionTimeOut.ascx" TagPrefix="uc1" TagName="SessionTimeOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        /*styles are in AppStyles page*/
        .RemoveDefaultStyle tbody {
            display: inline !important;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnJR_ID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnSOP_ID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnSopVersion_ID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTrainingDept" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 dms_outer_border">
        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="panel panel-default" id="mainContainer" runat="server">
                        <div class="grid_header col-lg-12  col-xs-12 col-md-12 col-sm-12 "><asp:Label ID="lblTitle" runat="server" Font-Bold="true" Text="JR Trainer Evaluation"></asp:Label></div>
                <div>
                    <div class="col-sm-6 top">
                        <div class="form-horizontal">
                            <asp:UpdatePanel ID="upddlDepartment" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <label class="col-sm-12 col-lg-4 col-xs-12 col-md-4 control-label">Select Training Department</label>
                                        <div class="col-sm-12 col-lg-6 col-xs-12 col-md-6">
                                            <asp:DropDownList ID="ddlDepartment" CssClass="form-control SearchDropDown" runat="server" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true" Width="100%"></asp:DropDownList>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                  
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12" style="overflow: auto; max-height: 460px; min-height: 460px;">
                        <asp:UpdatePanel ID="upgvTrainerEvaluationList" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView ID="gvTrainerEvaluationList" OnRowCommand="gvTrainerEvaluationList_RowCommand" DataKeyNames="JR_ID,TrainingDeptID"
                                    EmptyDataText="No Record's"
                                    runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixHead AspGrid"
                                    EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                    <AlternatingRowStyle BackColor="White" />
                                    <RowStyle CssClass="col-xs-12 padding-none" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="JR ID" Visible="false">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditJrID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJrID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TrainingDeptID" Visible="false">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditTrainingDeptID" runat="server" Text='<%# Bind("TrainingDeptID") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTrainingDeptID" runat="server" Text='<%# Bind("TrainingDeptID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:TemplateField HeaderText="EmpID" Visible="false">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditEmpID" runat="server" Text='<%# Bind("TraineeEmpID") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpID" runat="server" Text='<%# Bind("TraineeEmpID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="JR Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditJrName" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJrName" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JR Version" SortExpression="JR_Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--   <asp:TemplateField HeaderText="Emp Code">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditEmpCode" runat="server" Text='<%# Bind("TraineeEmpCode") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpCode" runat="server" Text='<%# Bind("TraineeEmpCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Trainee Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditEmpName" runat="server" Text='<%# Bind("TraineeName") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("TraineeName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:TemplateField HeaderText="Trainee Department">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblEditTraineeDeptName" runat="server" Text='<%# Bind("TraineeDeptName") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTraineeDeptName" runat="server" Text='<%# Bind("TraineeDeptName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Trainee Dept" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditTraineeDeptCode" runat="server" Text='<%# Bind("TraineeDeptCode") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTraineeDeptCode" runat="server" Text='<%# Bind("TraineeDeptCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JR Assigned By" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditJR_AssignedBy" runat="server" Text='<%# Bind("JR_AssignedBy") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJR_AssignedBy" runat="server" Text='<%# Bind("JR_AssignedBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JR Assigned Date" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditJR_AssignedDate" runat="server" Text='<%# Bind("JR_AssignedDate") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJR_AssignedDate" runat="server" Text='<%# Bind("JR_AssignedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Training Department">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblEditTrainingDeptName" runat="server" Text='<%# Bind("TrainingDeptName") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTrainingDeptName" runat="server" Text='<%# Bind("TrainingDeptName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="DeptCode" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditTrainingDeptCode" runat="server" Text='<%# Bind("TrainingDeptCode") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTrainingDeptCode" runat="server" Text='<%# Bind("TrainingDeptCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="View Exam" HeaderStyle-CssClass="col-xs-2 text-center" ItemStyle-CssClass="col-xs-2 center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkViewTS" runat="server" Font-Underline="true" CommandName="ViewTS" CommandArgument="<%# Container.DataItemIndex %>" ToolTip="click to view Training Record">
                                                    <%--<asp:Image ID="Image1" runat="server" ImageUrl="/Images/TMS_Images/DocSearch.png" Style="width: 22px" />--%>
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="/Images/TMS_Images/view_tms.png" />
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="modelSops_View" role="dialog">
            <div class="modal-dialog modal-lg" style="width: 90%">
                <!-- Modal content-->
                <div class="modal-content" style="border-radius: 0px">
                    <div class="modal-header tmsModalHeader">
                        <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title Panel_Title">Trained Sop's</h4>
                    </div>
                    <div class="modal-body tmsModalBody">
                        <div class="row tmsModalContent">
                            <asp:UpdatePanel ID="upSopslist" runat="server" UpdateMode="conditional">
                                <ContentTemplate>
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a data-toggle="tab" href="#tabgvOralExam" class="tabstyle">Oral/Practical<sup><asp:Label ID="lblOralExamCount" runat="server"></asp:Label></sup></a>
                                        </li>
                                        <li><a data-toggle="tab" href="#tabgvWrittenExam" class="tabstyle">Written<sup><asp:Label ID="lblWrittenExamCount" runat="server"></asp:Label></sup></a></li>
                                    </ul>

                                    <div class="tab-content" style="padding-top: 3px">
                                        <div id="tabgvOralExam" class="tab-pane fade in active">
                                            <div class=" col-lg-12 col-sm-12 col-xs-12 col-md-12 divGV_Styles">
                                                <asp:UpdatePanel ID="upGvOralExamSops" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gvOralExamSops" runat="server" AutoGenerateColumns="False" OnRowCommand="gvOralExamSops_RowCommand" DataKeyNames="DocumentID, DocVersionID"
                                                            OnRowDataBound="gvOralExamSops_RowDataBound" CssClass="table table-fixed fixview AspGrid"
                                                            EmptyDataText="No Record's To Evaluate" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" Style="margin-bottom: 0px !important;">
                                                            <AlternatingRowStyle BackColor="White" />
                                                            <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Sop ID" Visible="false">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SOP NO." HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditDocumentNumber" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocumentNumber" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SOP Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditDocumentName" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocumentName" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Read Duration" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Remarks" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                    <EditItemTemplate>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <%--<asp:TextBox ID="txtRemarks" CssClass="form-control" MaxLength="250" placeholder="Trainer Remarks" TextMode="MultiLine" Rows="2" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:TextBox  >--%>
                                                                        <asp:Label ID="lblTraineeRemarks" runat="server" Visible="false" Text='<%# Bind("TrainerComments") %>'></asp:Label>
                                                                        <textarea typeof="text" id="txtTrainerRemarks2" cssclass="form-control" maxlength="250" placeholder="Trainer Remarks" runat="server" style="height: 40px;"></textarea>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Evaluation Type" SortExpression="Description" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                    <ItemTemplate>
                                                                        <%-- <table class="table" style="padding: 0px; margin-bottom: 0px; width: 100%">--%>
                                                                        <%--  <tr>--%>
                                                                        <%--<td class="QuestionOptions" style="padding: 0px; border-top: none;">--%>
                                                                        <asp:Label ID="lblEvalutionType" runat="server" Text='<%#Bind("ExamType") %>' Visible="false" />
                                                                        <asp:RadioButtonList ID="rblEvalutionType" CssClass="RemoveDefaultStyle" Style="padding: 0px; margin-bottom: 0px; width: 100%"
                                                                            Font-Bold="false" runat="server" RepeatDirection="Horizontal">
                                                                            <asp:ListItem Text="Oral" Value="2"></asp:ListItem>
                                                                            <asp:ListItem Text="Practical" Value="3"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                        <%--</td>--%>
                                                                        <%-- </tr>--%>
                                                                        <%-- </table>--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Opininon" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTrainer_Opinion" runat="server" Text='<%# Bind("Opinion") %>' Visible="false"></asp:Label>
                                                                        <asp:CheckBox ID="chOpinionSatisfactory" runat="server" Text="Satisfactory" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Evaluate" SortExpression="Description" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTrainerEvaluatedStatus" runat="server" Text='<%#Bind("Evaluated_Status") %>' Visible="false" />
                                                                        <asp:LinkButton ID="lnkAttendedOralExam" CssClass="btn TMS_Button btn-sm" runat="server" CommandName="EvaluateOralSop" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' ToolTip="Click To Submit Oral Result">
                                                                           <div>                                                                             
                                                                                   <div> <span class="glyphicon glyphicon-check InBtn_Icon"></span></div>
                                                                                <%--<div> <span class="Evaluation_tms"></span></div>--%>
                                                                                   <%--<div><span>Submit</span> </div>--%>                                                                              
                                                                           </div>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <%-- <asp:UpdatePanel ID="upTrainerRemarks" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div class="form-horizontal col-sm-4" id="divTrainerRemarks">
                                                        <asp:CheckBox ID="cbTrainerRemarks" runat="server" Text="Apply for All"/>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4">Trainer Remarks</label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtTrainerRemarks" runat="server" CssClass="form-control" MaxLength="250" placeholder="Trainer Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                                          </ContentTemplate>
                                                </asp:UpdatePanel>--%>
                                        </div>
                                        <div id="tabgvWrittenExam" class="tab-pane fade">
                                            <div class=" col-lg-12 col-sm-12 col-xs-12 col-md-12 divGV_Styles">
                                                <asp:UpdatePanel ID="upGvViewSops" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gvViewSopsList" runat="server" AutoGenerateColumns="False" DataKeyNames="DocumentID,DocVersionID,Evaluated_Status,ExplainationStatus"
                                                            CssClass="table table-fixed fixWritten AspGrid" OnRowCommand="gvViewSopsList_RowCommand"
                                                            EmptyDataText="No Record's To Evaluate" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                            <AlternatingRowStyle BackColor="White" />
                                                            <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Sop ID" Visible="false">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--  <asp:TemplateField HeaderText="S.No." HeaderStyle-Width="25px" ItemStyle-HorizontalAlign="Center">
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblEditRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="SOP No." HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditDocumentNumber" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocumentNumber" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SOP Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditDocumentName" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocumentName" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Read Duration" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="No of Attempts" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditAttemptedCount" runat="server" Text='<%# Bind("AttemptedCount") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAttemptedCount" runat="server" Text='<%# Bind("AttemptedCount") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Marks" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditMarks" runat="server" Text='<%# Bind("Marks") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblMarks" runat="server" Text='<%# Bind("Marks") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Qualified Date" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditExamPassedDate" runat="server" Text='<%# Bind("ExamPassedDate") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblExamPassedDate" runat="server" Text='<%# Bind("ExamPassedDate") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Explaination Status" Visible="false">
                                                                    <EditItemTemplate>
                                                                        <asp:Label ID="lblEditExplainationStatus" runat="server" Text='<%# Bind("ExplainationStatus") %>'></asp:Label>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblExplainationStatus" runat="server" Text='<%# Bind("ExplainationStatus") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Answer Sheet" HeaderStyle-CssClass="col-xs-2 text-center" ItemStyle-CssClass="col-xs-2 Center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTrainerEvaluatedStatus" runat="server" Text='<%#Bind("Evaluated_Status") %>' Visible="false" />
                                                                        <asp:LinkButton ID="lnkViewExamReport" runat="server" Font-Underline="true" CommandName="ViewExamResult" ToolTip="Click to view Exam Record"
                                                                            CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'>
                                                                            <%--<asp:Image ID="Image1" runat="server" ImageUrl="/Images/TMS_Images/DocSearch.png" Style="width: 25px" />--%>
                                                                            <asp:Image ID="Image1" runat="server" ImageUrl="/Images/TMS_Images/view_answer.png"/>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="modal-footer tmsModalContent">
                        <div class="row" style="text-align: center">
                            <%-- <asp:LinkButton ID="lnkAcceptTS" CssClass="btn TMS_Button" runat="server">
                                     <table>
                                        <tr>
                                            <td><span class="glyphicon glyphicon-ok InBtn_Icon"></span></td>
                                            <td><span>Training Completed</span> </td>
                                        </tr>
                                     </table> 
                                </asp:LinkButton>--%>
                            <%--<button type="button" data-dismiss="modal" runat="server" id="btnModelClose" class="btn TMS_Button">Close</button>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal popup for exam Result-->
        <div class="modal fade" id="SopExamResultModal" role="dialog">
            <div class="modal-dialog modal-lg" style="width: 90%; height: 600px">
                <div class="modal-content">
                    <div class="modal-header tmsModalHeader">
                        <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title Panel_Title" style="color: aliceblue">Exam Result</h4>
                    </div>
                    <div class="modal-body tmsModalBody">
                        <div class="row form-horizontal">
                            <asp:UpdatePanel ID="upSopExamResultBody" runat="server">
                                <ContentTemplate>
                                    <div style="margin: 0px 6px 0px 10px">
                                        <div class="col-sm-6">
                                            <span style="font-weight: bold">Exam Attempted Date:</span>
                                            <label class="control-label" style="font-weight: normal" runat="server" id="lblExamDate" title="Exam Date and Time"></label>
                                        </div>
                                        <div class="col-sm-6" style="text-align: right; padding-right: 32px">
                                            <span style="font-weight: bold">Marks :</span>
                                            <label class="control-label" style="font-weight: normal" runat="server" id="lblMarks" title="Marks Obtained In this Sop"></label>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="row tmsModalContent" style="overflow: auto; padding: 0px; max-height: 490px; min-height: 490px;">
                            <asp:UpdatePanel ID="upGvResultQuestionnaire" runat="server" UpdateMode="Conditional" style="margin: 10px">
                                <ContentTemplate>
                                    <asp:GridView ID="gvResultQuestionnaire" OnRowDataBound="gvResultQuestionnaire_RowDataBound"
                                        DataKeyNames="QuestionID" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered AspGrid"
                                        EmptyDataText="No Questionnaire's Result" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" ShowHeader="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Question ID" SortExpression="QueID" Visible="false">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <table class="table">
                                                        <tr>
                                                            <td style="color:#2e2e2e; background:#c0d4d7; font-size:12pt; font-weight:bold;">
                                                                <asp:Label ID="lblGVQuestion" runat="server" Text='<%# Bind("QuestionTitle") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="QuestionOptions">
                                                                <asp:RadioButtonList ID="rblOptions" Font-Bold="false" AutoPostBack="true" runat="server" Enabled="false"></asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="modal-footer tmsModalContent">
                        <div class="row" style="text-align: left">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                <div class="col-sm-6">
                                    <div class="col-sm-5" style="padding-left: 0px">
                                        <table>
                                            <tr>
                                                <td style="width: 10px">
                                                    <span class="label label-success" style="display: block; padding: 6px; background-color: green;"></span>
                                                </td>
                                                <td>&nbsp;Correct Answer
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-sm-5">
                                        <table>
                                            <tr>
                                                <td style="width: 10px">
                                                    <span class="label label-success" style="display: block; padding: 5px; background-color: red;"></span>
                                                </td>
                                                <td>&nbsp;Wrong Answer
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="upWrittenSoplistFooter" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12" style="padding-left: 30px">
                                        <div class="col-sm-5" style="width: 35%;">
                                            <span>Have You Explained the Wrongly Answered Questions to Trainee</span>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:RadioButtonList ID="rblWrongAnsExplained" Font-Bold="false" CssClass="rblOptions" CellSpacing="1" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                <asp:ListItem Text="N/A" Value="NA"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12" style="text-align: center">
                                        <asp:Button ID="btnEvaluateWritten" CssClass="btn TMS_Button" runat="server" OnClick="btnEvaluateWritten_Click" Text="Evaluate" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <uc1:SessionTimeOut runat="server" ID="SessionTimeOut" />

    <%-- <script type="text/javascript">
        function Validate() {
            var rb = document.getElementById("<%=rblWrongAnsExplained.ClientID%>");
            var radio = rb.getElementsByTagName("input");
            var isChecked = false;
            for (var i = 0; i < radio.length; i++) {
                if (radio[i].checked) {
                    isChecked = true;
                    break;
                }
            }
            if (!isChecked) {
                alert("Please select an option!");
            }

            return isChecked;
        }
</script>--%>

    <script type="text/javascript">
        $(document).ready(function () {
            // Fix up GridView to support THEAD tags            
            fixGvHeader();
            addHead();
            addHeader();
            // $("#gvFeedbackHistory").tablesorter({ sortList: [[1, 0]] });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixGvHeader();
            addHead();
            addHeader();
        });

        function fixGvHeader() {
            var tbl = document.getElementsByClassName("fixHead");
            if ($(".fixHead").find("thead").length == 0) {
                $(".fixHead>tbody").before("<thead><tr></tr></thead>");
                $(".fixHead>thead>tr").append($(".fixHead th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHead>tbody>tr:first").remove();
                }               
            }
        }
        function addHead() {
            var tbl = document.getElementsByClassName("fixview");
            if ($(".fixview").find("thead").length == 0) {
                $(".fixview>tbody").before("<thead><tr></tr></thead>");
                $(".fixview>thead>tr").append($(".fixview th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixview>tbody>tr:first").remove();
                }               
            }
        }

        function addHeader() {
            var tbl = document.getElementsByClassName("fixWritten");
            if ($(".fixWritten").find("thead").length == 0) {
                $(".fixWritten tbody").before("<thead><tr></tr></thead>");
                $(".fixWritten thead tr").append($(".fixWritten th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixWritten tbody tr:first").remove();
                }               
            }
        }

    </script>

    <script>
            function OpenModelViewTS() {
                $('#modelSops_View').modal({ backdrop: 'static', keyboard: false });
            }
            function OpenSopExamResultModal() {
                $('#SopExamResultModal').modal({ show: 'true', backdrop: 'static', keyboard: false });
            }
    </script>
</asp:Content>
