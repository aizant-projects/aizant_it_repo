﻿<%@ Page Title="ATC Creation" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="CreateATC_List.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.ATC.List.CreateATC_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      <asp:HiddenField ID="hdnEmpID" runat="server" Value="0"/>
    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnAssigned_ATC" runat="server" />
            <asp:HiddenField ID="hdnPrint" runat="server" Value="false" />
            <asp:HiddenField ID="hdnDeptID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnCalendarYear" runat="server" Value="0" />
            <asp:HiddenField runat="server" ID="hdnNotificationID" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="float-left col-lg-12 padding-none" id="Div3" runat="server">                
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">Create Annual Training Calendar</div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none" style="overflow: auto; margin-top: 10px">
                    <table id="tblAssigned_ATC" class="datatable_cust tblAssigned_ATC_Class display breakword" style="width: 100%">
                        <thead>
                            <tr>
                                <th>InitializeATC_ID</th>
                                <th>DeptID</th>
                                <th>Department</th>
                                <th>Calendar Year</th>
                                <th>Initiated By</th>
                                <th>Initiated Date</th>
                                <th>Create</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnViewAssigned_ATC" runat="server" OnClick="btnViewAssigned_ATC_Click" Text="Button"/>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script>
        function ViewAssigned_ATC(InitializeATC_ID) {           
            $("#<%=hdnAssigned_ATC.ClientID%>").val(InitializeATC_ID);
            $("#<%=btnViewAssigned_ATC.ClientID%>").click();
        }
    </script>

    <!--ATC_Pending List jQuery DataTable-->
    <script>
        $.ajaxSetup({
            cache: false
        });

        var oTable = $('#tblAssigned_ATC').DataTable({
            columns: [
                { 'data': 'InitializeATC_ID' }, 
                { 'data': 'DeptID' }, 
                { 'data': 'DepartmentName' },
                { 'data': 'CalendarYear' },
                { 'data': 'InitiatedBy' }, 
                { 'data': 'InitiatedDate' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="btngridassign_tms" title="Create"  href="#" onclick="ViewAssigned_ATC(' + o.InitializeATC_ID + ');"></a>'; }
                }
            ],
            "order": [[3, "desc"]],
            "scrollY": "47vh",
            "aoColumnDefs": [{ "targets": [0, 1], "visible": false }, { className: "textAlignLeft", "targets": [2, 4] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/AssignedATC_Service.asmx/AssignedATC")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "AuthorID", "value": <%=hdnEmpID.Value%> }, { "name": "Notification_ID", "value": <%=hdnNotificationID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblAssigned_ATC").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        $(".tblAssigned_ATC_Class").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
            
        });

    </script>
   
</asp:Content>
