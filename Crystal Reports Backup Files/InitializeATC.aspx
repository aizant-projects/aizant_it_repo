﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="InitializeATC.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Administration.InitializeATC" %>

<%@ Register Src="~/UserControls/SessionTimeOut.ascx" TagPrefix="uc1" TagName="SessionTimeOut" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnInitializedATC_ID" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 inner_border">
        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div class="col-lg-12 padding-none" id="divMainContainer" runat="server">
            <div class="panel-group padding-none" id="accordion1">
                <div class="panel panel-default">
                    <a href="#collapseFG" data-toggle="collapse" data-parent="#accordion1">
                        <div class="panel-heading header_acc accordion-toggle">
                            <h4 class="panel-title">Initiate Annual Training calendar</h4>
                        </div>
                    </a>
                    <div id="collapseFG" class="panel-collapse collapse in">
                        <div class="panel-body col-lg-12 work_accordion">
                            <asp:UpdatePanel ID="upAccordon1" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="form-group col-lg-3 ">
                                        <div class="col-lg-12 padding-none">
                                            <label>Department<span class="smallred">*</span></label>
                                            <asp:DropDownList ID="ddlDepartment" CssClass="form-control SearchDropDown col-lg-12" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" data-live-search="true" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-1 padding-none ">
                                        <div class="col-lg-12 padding-none">
                                            <label>Year<span class="smallred">*</span></label>
                                            <asp:DropDownList ID="ddlYear" CssClass="form-control SearchDropDown col-lg-12" data-live-search="true" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-3 ">
                                        <div class="col-lg-12 padding-none">
                                            <label>Training Calendar Author<span class="smallred">*</span></label>
                                            <asp:DropDownList ID="ddlATC_Author" CssClass="form-control SearchDropDown" OnSelectedIndexChanged="ddlATC_Author_SelectedIndexChanged" data-live-search="true" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-3 ">
                                        <div class="col-lg-12 padding-none">
                                            <label>Approval HOD<span class="smallred">*</span></label>
                                            <asp:DropDownList ID="ddlApprovalHod" CssClass="form-control SearchDropDown col-lg-12" OnSelectedIndexChanged="ddlApprovalHod_SelectedIndexChanged" data-live-search="true" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-2 padding-none">
                                        <div class="col-lg-12 padding-none">
                                            <label>Approval QA<span class="smallred">*</span></label>
                                            <asp:DropDownList ID="ddlApprovalQA" CssClass="form-control SearchDropDown" OnSelectedIndexChanged="ddlApprovalQA_SelectedIndexChanged" data-live-search="true" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-lg-12  padding-none">
                                        <div class="form-group col-lg-6 padding-none">
                                            <label for="inputPassword3" class=" col-lg-12 col-sm-12 col-xs-12 col-md-12 control-label  custom_label_answer">Remarks<span runat="server" visible="false" id="spanRemarks" class="smallred">*</span></label>
                                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group pull-right col-lg-6">
                                            <asp:Button ID="btnInitialize" runat="server" CssClass="btn btn-sub pull-right" Text="Initialize" OnClick="btnInitialize_Click" OnClientClick="javascript:return InitializeRequired();" />
                                            <asp:Button ID="btnUpdateATC" runat="server" CssClass="btn btn-sub pull-right" Text="Update" Visible="false" OnClick="btnUpdateATC_Click" OnClientClick="javascript:return UpdateRequired();" />
                                            <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-sub pull-right" Text="Reset" OnClick="btnCancel_Click" />

                                            <%--<asp:Button type="button" class=" btn-signup_popup" runat="server" ID="btnSubmit" Text="Submit" TabIndex="29"></asp:Button>
                                    <asp:Button ID="btnCancel" CssClass="btn btn-cancel_popup" runat="server" Text="Cancel" ToolTip="click to Cancel" />
                                    <asp:Button ID="btnUpdate" CssClass=" btn-signup_popup" runat="server" Text="Update"  ToolTip="click to Update Employee" />--%>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <%--<div class="panel panel-default">
                        <div class="panel-heading header_acc accordion-toggle" data-toggle="collapse" href="#collapseFG1" data-parent="#accordion1">
                            <h4 class="panel-title">
                                <a class="">Annual Training Calendar List
						        </a>
                            </h4>
                        </div>
                        <div id="collapseFG1" class="panel-collapse collapse in">
                            <div class="panel-body col-lg-12 work_accordion">--%>
                    <div class="col-lg-12 padding-none grid_panel_hod" id="gridSubmit">
                        <div class="grid_header col-lg-12">Initialized Calendar List</div>
                        <div class="col-lg-12">
                            <asp:UpdatePanel ID="upGV_ATCList" runat="server" UpdateMode="Conditional" style="overflow:auto; max-height:250px">
                                <ContentTemplate>
                                    <asp:GridView ID="gvATCList" runat="server" Width="100%" AutoGenerateColumns="false" OnRowCommand="gvATCList_RowCommand"
                                        OnRowDataBound="gvATCList_RowDataBound" CssClass="table table-striped table-bordered"
                                        DataKeyNames="InitializeATC_ID,DeptID,CalendarYear,ATC_Author_ID,ATC_HOD_Approval_ID,ATC_QA_Approval_ID">
                                        <Columns>
                                            <asp:TemplateField HeaderText="JR ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblJR_ID" runat="server" Text='<%# Bind("InitializeATC_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DeptID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDeptID" runat="server" Text='<%# Bind("DeptID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Department" SortExpression="DeptName">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtDeptName" runat="server" Text='<%# Bind("DeptName") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDeptName" runat="server" Text='<%# Bind("DeptName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Calendar Year" SortExpression="CalendarYear">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtCalendarYear" runat="server" Text='<%# Bind("CalendarYear") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCalendarYear" runat="server" Text='<%# Bind("CalendarYear") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="InitiatedBy_ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInitiatedBy_ID" runat="server" Text='<%# Bind("InitiatedBy_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Initiated By" SortExpression="InitiatedBy">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtInitiatedBy" runat="server" Text='<%# Bind("InitiatedBy") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInitiatedBy" runat="server" Text='<%# Bind("InitiatedBy") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Initiated Date" SortExpression="InitiatedDate">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtInitiatedDate" runat="server" Text='<%# Bind("InitiatedDate") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInitiatedDate" runat="server" Text='<%# Bind("InitiatedDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ATC_Author_ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblATC_Author_ID" runat="server" Text='<%# Bind("ATC_Author_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Author" SortExpression="ATC_Author">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtATC_Author" runat="server" Text='<%# Bind("ATC_Author") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblATC_Author" runat="server" Text='<%# Bind("ATC_Author") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ATC_HOD_Approval_ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblATC_HOD_Approval_ID" runat="server" Text='<%# Bind("ATC_HOD_Approval_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Reviewer" SortExpression="ATC_HOD_Approval">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtATC_HOD_Approval" runat="server" Text='<%# Bind("ATC_HOD_Approval") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblATC_HOD_Approval" runat="server" Text='<%# Bind("ATC_HOD_Approval") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ATC_QA_Approval_ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblATC_QA_Approval_ID" runat="server" Text='<%# Bind("ATC_QA_Approval_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Approver" SortExpression="ATC_QA_Approval">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtATC_QA_Approval" runat="server" Text='<%# Bind("ATC_QA_Approval") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblATC_QA_Approval" runat="server" Text='<%# Bind("ATC_QA_Approval") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ATC_Status" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblATC_Status" runat="server" Text='<%# Bind("ATC_Status") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="txtCenterIcon" HeaderStyle-Width="8%">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkEdit" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" runat="server" CommandName="EditATC" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' ToolTip="Click To Edit"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <%--<button type="button" class="pull-right btn btn-cancel_popup" data-dismiss="modal">Cancel</button>--%>
                <%--</div>
                        </div>
                    </div>--%>
            </div>
        </div>
    </div>
    <uc1:SessionTimeOut runat="server" ID="SessionTimeOut" />
    <script>
        function UpdateRequired() {
            var Remarks = document.getElementById("<%=txtRemarks.ClientID%>").value;
            errors = [];
            if (Remarks == "") {
                errors.push("Specify Purpose of Modification in Remarks");
            }
            if (document.getElementById('<%=ddlDepartment.ClientID%>').value == 0) {
               errors.push("Select Department");
           }
           if (document.getElementById('<%=ddlApprovalHod.ClientID%>').value == 0) {
               errors.push("Select Approval HOD");
           }
           if (document.getElementById('<%=ddlApprovalQA.ClientID%>').value == 0) {
               errors.push("Select Approval QA");
           }

           if (errors.length > 0) {
               custAlertMsg(errors.join("<br/>"), "error");
               return false;
           }
           else {
               return true;
           }
        }

        function InitializeRequired() {
            errors = [];
            if (document.getElementById('<%=ddlDepartment.ClientID%>').value == 0) {
                errors.push("Select Department");
            }
            if (document.getElementById('<%=ddlATC_Author.ClientID%>').value == 0) {
                errors.push("Select Author");
            }
            if (document.getElementById('<%=ddlApprovalHod.ClientID%>').value == 0) {
                errors.push("Select Approval HOD");
            }
            if (document.getElementById('<%=ddlApprovalQA.ClientID%>').value == 0) {
                errors.push("Select Approval QA");
            }

            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
        function ExpandDiv() {
            $('#collapseFG').addClass('panel-collapse collapse in').removeClass('panel-collapse collapse');
        }
    </script>
</asp:Content>
