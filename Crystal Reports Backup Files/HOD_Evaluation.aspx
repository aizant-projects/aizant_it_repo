﻿<%@ Page Title="HOD Evaluation" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="HOD_Evaluation.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Training.HOD_Evaluation" %>
<%@ Register Src="~/UserControls/SessionTimeOut.ascx" TagPrefix="uc1" TagName="SessionTimeOut" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnJR_ID" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 dms_outer_border">
        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="panel panel-default" id="mainContainer" runat="server">
                    <div class="grid_header col-lg-12  col-xs-12 col-md-12 col-sm-12 "><asp:Label ID="lblTitle" runat="server" Font-Bold="true" Text="HOD Evaluation"></asp:Label></div>
                <div>
                    <div class="col-sm-6 padding-none top bottom">
                        <div class="form-horizontal">
                            <asp:UpdatePanel ID="upddlDepartment" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12  padding-none">
                                        <label class="col-sm-12 col-lg-3 col-xs-12 col-md-4 control-label">Department</label>
                                        <div class="col-sm-12 col-lg-6 col-xs-12 col-md-6">
                                            <asp:DropDownList ID="ddlDepartment" CssClass="form-control SearchDropDown" runat="server" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true" Width="100%"></asp:DropDownList>
                                        </div>
                                    </div>                                    
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12" style="overflow: auto; max-height: 460px;">
                        <asp:UpdatePanel ID="upgvHODEvaluation_JR_List" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView ID="gvHODEvaluation_JR_List" OnRowCommand="gvHODEvaluation_JR_List_RowCommand" DataKeyNames="JR_ID"
                                    EmptyDataText="No Record's"
                                    runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixHeadHodEvaluationJrList AspGrid"
                                    EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                    <AlternatingRowStyle BackColor="White" />
                                    <RowStyle CssClass="col-xs-12 padding-none" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="JR ID" Visible="false">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditJrID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJrID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:TemplateField HeaderText="EmpID" Visible="false">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditEmpID" runat="server" Text='<%# Bind("TraineeEmpID") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpID" runat="server" Text='<%# Bind("TraineeEmpID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="JR Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditJrName" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJrName" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JR Version" SortExpression="JR_Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--   <asp:TemplateField HeaderText="Emp Code">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditEmpCode" runat="server" Text='<%# Bind("TraineeEmpCode") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpCode" runat="server" Text='<%# Bind("TraineeEmpCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Trainee Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditEmpName" runat="server" Text='<%# Bind("TraineeName") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("TraineeName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Trainee Department">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditTraineeDeptName" runat="server" Text='<%# Bind("TraineeDeptName") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTraineeDeptName" runat="server" Text='<%# Bind("TraineeDeptName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Dept Code" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditTraineeDeptCode" runat="server" Text='<%# Bind("TraineeDeptCode") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTraineeDeptCode" runat="server" Text='<%# Bind("TraineeDeptCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JR Assigned By" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditJR_AssignedBy" runat="server" Text='<%# Bind("JR_AssignedBy") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJR_AssignedBy" runat="server" Text='<%# Bind("JR_AssignedBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JR Assigned Date" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditJR_AssignedDate" runat="server" Text='<%# Bind("JR_AssignedDate") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJR_AssignedDate" runat="server" Text='<%# Bind("JR_AssignedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Training Record" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkViewTS" runat="server" Font-Underline="true" CommandName="ViewTS" CommandArgument="<%# Container.DataItemIndex %>" ToolTip="click to view Training Record">
                                                    <%--<asp:Image ID="Image1" runat="server" ImageUrl="~/Images/TMS_Images/DocSearch.png" Style="width: 25px" />--%>
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/TMS_Images/view_tms.png" />
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Evaluate" SortExpression="Description" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkAttendedOralExam" runat="server" CommandName="SubmitEvalution" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' ToolTip="Click To Submit Evaluation">                                                    
                                                    <span class="glyphicon glyphicon-check" style="font-size: 22px;color: #07889a;"></span>
                                                    <%--<span class="Evaluation_tms" style="font-size: 22px;color: #07889a;"></span>--%>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="gvHODEvaluation_JR_List" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal popup  for Sops list based on selected department-->
        <div class="modal fade" id="SopsModalView" role="dialog">
            <div class="modal-dialog modal-lg" style="width: 90%">
                <!-- Modal content-->
                <div class="modal-content" style="border-radius: 0px">
                    <div class="modal-header tmsModalHeader">
                        <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title Panel_Title">Trained Sop's</h4>
                    </div>
                    <div class="modal-body tmsModalBody">
                        <div class="row tmsModalContent">
                            <div class="col-sm-6">
                                <div class="form-horizontal">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Select Trained Department</label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddlJR_Trained_Depts" CssClass="form-control SearchDropDown" runat="server" OnSelectedIndexChanged="ddlJR_Trained_Depts_SelectedIndexChanged" AutoPostBack="true" Width="100%"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <ul class="nav nav-tabs pull-left">
                                    <li class="active"><a data-toggle="tab" href="#tabgvOralExam" class="tabstyle">Oral/Practical</a></li>
                                    <li><a data-toggle="tab" href="#tabgvWrittenExam" class="tabstyle">Written</a></li>
                                    <li><a data-toggle="tab" href="#tabgvRetrospective" runat="server" id="tabRetrospective" class="tabstyle">Retrospective</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div id="tabgvOralExam" class="tab-pane fade in active">
                                    <div class=" col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                        <asp:UpdatePanel ID="upGvOralExamSops" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:GridView ID="gvOralExamSops" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvOralExamSops_RowDataBound" OnRowCommand="gvOralExamSops_RowCommand"
                                                    CssClass="table table-fixed fixHeadExamSOPs"
                                                    EmptyDataText="No Oral Exam Record's" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" Style="margin-bottom: 0px !important;">
                                                    <AlternatingRowStyle BackColor="White" />
                                                    <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Sop ID" Visible="false">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--    <asp:TemplateField HeaderText="Trained Department" HeaderStyle-Width="100px">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderText="SOP No." HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDocumentNumber" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocumentNumber" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="SOP Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDocumentName" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocumentName" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <%--  <asp:TemplateField HeaderText="Trained Department" HeaderStyle-Width="120px">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderText="Trained Dept" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditTrained_DeptCode" runat="server" Text='<%# Bind("Trained_DeptCode") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTrained_DeptCode" runat="server" Text='<%# Bind("Trained_DeptCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Read Duration" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Evaluated Trainer" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditEvaluatedBY" runat="server" Text='<%# Bind("EvaluatedTrainerBY") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEvaluatedBY" runat="server" Text='<%# Bind("EvaluatedTrainerBY") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Evaluated Date" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditEvaluatedDate" runat="server" Text='<%# Bind("EvaluatedDate") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEvaluatedDate" runat="server" Text='<%# Bind("EvaluatedDate") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Remarks" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2" ControlStyle-ForeColor="#9a9a9c">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTrainerComments" runat="server" Text='<%# Bind("TrainerComments") %>' Visible="false"></asp:Label>
                                                                <asp:LinkButton ID="lnkViewComments" CssClass="fa fa-commenting-o GV_Edit_Icon" runat="server" CommandName="ViewTraineeComments" CommandArgument='<%# Eval("TrainerComments") %>' ToolTip="Click To View"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <%--<i class="fa fa-commenting-o"></i>--%>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Trainer Opinion" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditTrainer_Opinion" runat="server" Text='<%# Bind("Trainer_Opinion") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTrainer_Opinion" runat="server" Text='<%# Bind("Trainer_Opinion") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Exam Type" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditExamType" runat="server" Text='<%# Bind("ExamType") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExamType" runat="server" Text='<%# Bind("ExamType") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div id="tabgvWrittenExam" class="tab-pane fade">
                                    <div class=" col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                        <asp:UpdatePanel ID="upGvViewSops" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:GridView ID="gvWrittenExamSops" runat="server" AutoGenerateColumns="False" DataKeyNames="DocumentID,Trainer_ExplanationStatus"
                                                    CssClass="table table-fixed fixHeadWrittenExamSOPs" OnRowCommand="gvWrittenExamSops_RowCommand"
                                                    EmptyDataText="No Written Exam Record's" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                    <AlternatingRowStyle BackColor="White" />
                                                    <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Sop ID" Visible="false">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSOP_ID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%-- <asp:TemplateField HeaderText="Trained Department" HeaderStyle-Width="100px">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderText="SOP No." HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDocumentNumber" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocumentNumber" runat="server" Text='<%# Bind("DocumentNumber") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocVersionID" runat="server" Text='<%# Bind("DocVersionID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="SOP Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDocumentName" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocumentName" runat="server" Text='<%# Bind("DocumentName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField HeaderText="Trained Department" HeaderStyle-Width="200px">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrained_Department" runat="server" Text='<%# Bind("Trained_Department") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderText="Trained Dept" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditTrained_DeptCode" runat="server" Text='<%# Bind("Trained_DeptCode") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTrained_DeptCode" runat="server" Text='<%# Bind("Trained_DeptCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Read Duration" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocDuration" runat="server" Text='<%# Bind("DocDuration") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No of Attempts" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditAttemptedCount" runat="server" Text='<%# Bind("AttemptedCount") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAttemptedCount" runat="server" Text='<%# Bind("AttemptedCount") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Evaluated Trainer" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditEvaluatedBY" runat="server" Text='<%# Bind("EvaluatedTrainerBY") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEvaluatedBY" runat="server" Text='<%# Bind("EvaluatedTrainerBY") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Evaluated Date" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditEvaluatedDate" runat="server" Text='<%# Bind("EvaluatedDate") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEvaluatedDate" runat="server" Text='<%# Bind("EvaluatedDate") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField HeaderText="Marks" HeaderStyle-Width="60px">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditMarks" runat="server" Text='<%# Bind("Marks") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMarks" runat="server" Text='<%# Bind("Marks") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Qualified Date" HeaderStyle-Width="190px">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditExamPassedDate" runat="server" Text='<%# Bind("ExamPassedDate") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExamPassedDate" runat="server" Text='<%# Bind("ExamPassedDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Explanation Status">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditExplainationStatus" runat="server" Text='<%# Bind("Trainer_ExplanationStatus") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExplainationStatus" runat="server" Text='<%# Bind("Trainer_ExplanationStatus") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderText="Answer Sheet" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkViewExamReport" runat="server" Font-Underline="true" CommandName="ViewExamResult" ToolTip="Click to view Exam Record"
                                                                    CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'>
                                                                    <%--<asp:Image ID="Image1" runat="server" ImageUrl="~/Images/TMS_Images/DocSearch.png" Style="width: 25px" />--%>
                                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/TMS_Images/view_answer.png" />
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div id="tabgvRetrospective"  class="tab-pane fade">
                                    <div class=" col-lg-12 col-sm-12 col-xs-12 col-md-12 divGV_Styles" style="min-height:75px; max-height:450px;">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:GridView ID="gvRetAvailableSOPs" runat="server"
                                                    CssClass="table table-fixed fixHeadAvailableSOPs" ShowHeaderWhenEmpty="false"
                                                    EmptyDataText="No Retrospective SOPs " AutoGenerateColumns="False"
                                                    EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                    <RowStyle CssClass="col-xs-12 padding-none" />
                                                    <HeaderStyle CssClass="gvHeaderStyle" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="SNo" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocumentID" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Dept Code" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocVersionID" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Dept Name" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocumentNumber" runat="server" Text='<%# Bind("DeptName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Retrospective Sops" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSOP_Description" runat="server" Text='<%# Bind("RQ_Documents") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div id="divJustification" class="col-lg-12 col-sm-12 col-xs-12 col-md-12" runat="server">
                                        <div class="form-group">
                                            <label class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">Justification Note</label>
                                            <textarea runat="server" id="txtJustification" class="col-lg-12 col-sm-12 col-xs-12 col-md-12 txt_Just" style="height: 100px;" disabled="disabled"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal popup for exam Result-->
        <div class="modal fade" id="SopExamResultModal" role="dialog">
            <div class="modal-dialog modal-lg" style="width: 90%; height: 600px">
                <div class="modal-content">
                    <div class="modal-header tmsModalHeader">
                        <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title Panel_Title" style="color: aliceblue">Exam Result</h4>
                    </div>
                    <div class="modal-body tmsModalBody">
                        <div class="row form-horizontal">
                            <asp:UpdatePanel ID="upSopExamResultBody" runat="server">
                                <ContentTemplate>
                                    <div style="margin: 0px 6px 0px 10px">
                                        <div class="col-sm-6">
                                            <span style="font-weight: bold">Exam Attempted Date:</span>
                                            <label class="control-label" style="font-weight: normal" runat="server" id="lblExamDate" title="Exam Date and Time"></label>
                                        </div>
                                        <div class="col-sm-6" style="text-align: right; padding-right: 32px">
                                            <span style="font-weight: bold">Marks :</span>
                                            <label class="control-label" style="font-weight: normal" runat="server" id="lblMarks" title="Marks Obtained In this Sop"></label>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="row tmsModalContent" style="overflow: auto; padding: 0px; max-height: 490px; min-height: 490px;">
                            <asp:UpdatePanel ID="upGvResultQuestionnaire" runat="server" UpdateMode="Conditional" style="margin: 10px">
                                <ContentTemplate>
                                    <asp:GridView ID="gvResultQuestionnaire" OnRowDataBound="gvResultQuestionnaire_RowDataBound"
                                        DataKeyNames="QuestionID" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered"
                                        EmptyDataText="No Questionnaire's Result" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" ShowHeader="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Question ID" SortExpression="QueID" Visible="false">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <table class="table">
                                                        <tr>
                                                            <td style="color:#2e2e2e; background:#c0d4d7; font-size:12pt; font-weight:bold;">
                                                                <asp:Label ID="lblGVQuestion" runat="server" Text='<%# Bind("QuestionTitle") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="QuestionOptions">
                                                                <asp:RadioButtonList ID="rblOptions" Font-Bold="false" AutoPostBack="true" runat="server" Enabled="false"></asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="modal-footer tmsModalContent">
                        <div class="row" style="text-align: left">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                <div class="col-sm-6">
                                    <div class="col-sm-5" style="padding-left: 0px">
                                        <table>
                                            <tr>
                                                <td style="width: 10px">
                                                    <span class="label label-success" style="display: block; padding: 6px; background-color: green;"></span>
                                                </td>
                                                <td>&nbsp;Correct Answer
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-sm-5">
                                        <table>
                                            <tr>
                                                <td style="width: 10px">
                                                    <span class="label label-success" style="display: block; padding: 5px; background-color: red;"></span>
                                                </td>
                                                <td>&nbsp;Wrong Answer
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="upWrittenSoplistFooter" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12" style="padding-left: 30px">
                                        <div class="col-sm-5" style="width: 32%;padding:10px">
                                            <span style="font-weight:bold;">Trainer explained wrongly answered questions to the Trainee</span>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:RadioButtonList ID="rblWrongAnsExplained" Font-Bold="false" CssClass="rblOptions" CellSpacing="1" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                <asp:ListItem Text="N/A" Value="NA"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--modal popup to load Trainer Remarks--%>
        <div class="modal fade" id="ModalTrainerRemarks" role="dialog">
            <div class="modal-dialog modal-sm">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header tmsModalHeader">
                        <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title Panel_Title" style="color: aliceblue">Trainer Remarks</h4>
                    </div>
                    <div class="modal-body tmsModalBody">
                        <asp:UpdatePanel ID="upModalBody" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="row tmsModalContent">
                                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                        <textarea class="form-control" id="txtTrainerRemarks" disabled="disabled" style="height: 100px" cols="12" runat="server" placeholder="Enter Revert Reason" aria-multiline="true" title="Trainer Remarks"></textarea>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="modal-footer tmsModalContent">
                        <div class="row" style="text-align: center">
                            <%-- <asp:UpdatePanel ID="UpBtnRevert" runat="server" UpdateMode="Conditional" style="display: inline-block">
                        <ContentTemplate>--%>
                            <asp:Button ID="btnClose" CssClass="btn TMS_Button" data-dismiss="modal" runat="server" Text="Close" />
                            <%--   </ContentTemplate>
                    </asp:UpdatePanel>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <uc1:SessionTimeOut runat="server" ID="SessionTimeOut" />

    <script type="text/javascript">
        $(document).ready(function () {
            // Fix up GridView to support THEAD tags            
            fixGvHeader();
            fixHeader();
            fixHeaderSOP();
            fixGvHead();

        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixGvHeader();
            fixHeader();
            fixHeaderSOP();
            fixGvHead();
        });

        function fixGvHeader() {
            var tbl = document.getElementsByClassName("fixHeadHodEvaluationJrList");
            // Fix up GridView to support THEAD tags     
            if ($(".fixHeadHodEvaluationJrList").find("thead").length == 0) {
                $(".fixHeadHodEvaluationJrList tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadHodEvaluationJrList thead tr").append($(".fixHeadHodEvaluationJrList th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadHodEvaluationJrList tbody tr:first").remove();
                }
            }
        }

        function fixGvHead() {
            var tbl = document.getElementsByClassName("fixHeadAvailableSOPs");
            // Fix up GridView to support THEAD tags     
            if ($(".fixHeadAvailableSOPs").find("thead").length == 0) {
                $(".fixHeadAvailableSOPs tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadAvailableSOPs thead tr").append($(".fixHeadAvailableSOPs th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadAvailableSOPs tbody tr:first").remove();
                }
            }
        }

        function fixHeader() {
            var tbl = document.getElementsByClassName("fixHeadExamSOPs");
            // Fix up GridView to support THEAD tags   
            if ($(".fixHeadExamSOPs").find("thead").length == 0) {
                $(".fixHeadExamSOPs tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadExamSOPs thead tr").append($(".fixHeadExamSOPs th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadExamSOPs tbody tr:first").remove();
                }
            }
        }
        function fixHeaderSOP() {
            var tbl = document.getElementsByClassName("fixHeadWrittenExamSOPs");
            // Fix up GridView to support THEAD tags 
            if ($(".fixHeadWrittenExamSOPs").find("thead").length == 0) {
                $(".fixHeadWrittenExamSOPs tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadWrittenExamSOPs thead tr").append($(".fixHeadWrittenExamSOPs th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadWrittenExamSOPs tbody tr:first").remove();
                }
            }
        }
    </script>

</asp:Content>
