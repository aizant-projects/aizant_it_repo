﻿<%@ Page Title="JR List" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeBehind="EmployeeJRList.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Reports.EmployeeJRList" %>

<%--<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<%@ Register Src="~/UserControls/SessionTimeOut.ascx" TagPrefix="uc1" TagName="SessionTimeOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script> 
        $(document).ready(function () {
            $("#ContentPlaceHolder1_rptPager_lnkPage_<%=rptPager.Items.Count-1%>").css("background-color", "white");
            $("#ContentPlaceHolder1_rptPager_lnkPage_<%=rptPager.Items.Count-1%>").css("color", "black");
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $("#ContentPlaceHolder1_rptPager_lnkPage_<%=rptPager.Items.Count-1%>").css("background-color", "white");
            $("#ContentPlaceHolder1_rptPager_lnkPage_<%=rptPager.Items.Count-1%>").css("color", "black");
        });
    </script>
    <%-- <style>
        .container {
            width:1390px;
        }       
    </style> --%>
    <asp:UpdatePanel ID="upHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnJR_ID" runat="server" />
            <asp:HiddenField ID="hdnPrint" runat="server" Value="false" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 inner_border">
        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="panel panel-default" id="mainContainer" runat="server">
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 Panel_Header_TMS">
                    <div class="col-sm-6">
                        <span class="Panel_Title">
                            <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Text="Employee JR List"></asp:Label>
                        </span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
                <div class="col-sm-6" style="padding-right: 0px; margin-left: -41%; margin-bottom: 1%;">
                    <div style="float: right">
                        <table>
                            <tr>
                                <td>Page:&nbsp;</td>
                                <td>
                                    <asp:UpdatePanel ID="upddlPageSize" UpdateMode="Always" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlPageSize" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged1" ToolTip="Select No.of Records/page">
                                                <asp:ListItem Text="5" Value="5" />
                                                <asp:ListItem Text="10" Value="10" />
                                                <asp:ListItem Text="15" Value="15" />
                                                <asp:ListItem Text="20" Value="20" />
                                                <asp:ListItem Text="25" Value="25" />
                                                <asp:ListItem Text="50" Value="50" />
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlPageSize" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12" style="overflow: auto;">
                    <asp:UpdatePanel ID="upgvEmployeeJRList" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:GridView ID="gvEmployeeJRList" DataKeyNames="JR_ID"
                                EmptyDataText="No Record's"
                                runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixHeadEmployeeJRList AspGrid"
                                EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" AllowSorting="true" OnSorting="gvEmployeeJRList_Sorting" OnRowCommand="gvEmployeeJRList_RowCommand" OnRowDataBound="gvEmployeeJRList_RowDataBound">
                                <AlternatingRowStyle BackColor="White" />
                                <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                <Columns>
                                    <asp:TemplateField HeaderText="JR Id" Visible="false">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblEditJR_ID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblJR_ID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="JR Name" SortExpression="JR_Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblEditJrName" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblJrName" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="JR Version" SortExpression="JR_Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblEditJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trainee Name" SortExpression="TraineeName" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblEditTraineeName" runat="server" Text='<%# Bind("TraineeName") %>'></asp:Label>l
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTraineeName" runat="server" Text='<%# Bind("TraineeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Current Status" SortExpression="StatusName" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtStatusName" runat="server" Text='<%# Bind("StatusName") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatusName" runat="server" Text='<%# Bind("StatusName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Designation" SortExpression="DesignationName" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblEditDesignation" runat="server" Text='<%# Bind("DesignationName") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Bind("DesignationName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Department" SortExpression="DepartmentName" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblEditDepartment" runat="server" Text='<%# Bind("DepartmentName") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Bind("DepartmentName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCurrentStatus" runat="server" Text='<%# Bind("CurrentStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkViewEmpList" runat="server" Font-Underline="true" CssClass="fa fa-eye" ToolTip="Click to view" CommandName="ViewJR" CommandArgument='<%# Eval("JR_ID") %>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--   <asp:TemplateField HeaderText="Training File" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8%">
                                  <ItemTemplate>
                                 <asp:LinkButton ID="lnkTrngFile" runat="server" CommandArgument='<%# Eval("JR_ID") %>' Font-Underline="true" Font-Bold="true">
                                    <asp:Image ID="ImageTrngFile" Width="20px" ImageUrl="~/Images/TMS_Images/AssignTS.png" runat="server" Visible="false" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Training File" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkTrngFile" runat="server" CommandName="ViewTF" CommandArgument='<%# Eval("JR_ID") %>'>
                                                <asp:Image ID="ImageTrngFile" Width="20px" runat="server" ImageUrl="~/Images/TMS_Images/DocSearch.png" />
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12" style="text-align: center">
                    <div class="pagination pull-right" style="margin: 0px;">
                        <asp:UpdatePanel runat="server" ID="uplnkPage" style="display: inline-block;">
                            <ContentTemplate>
                                <asp:Repeater ID="rptPager" runat="server">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>' Enabled='<%# Eval("Enabled") %>' OnClick="lnkPage_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="Panel_Footer_Message_Label">
                    <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal EmpJrList -->
    <div class="modal fade" id="modelEmpJrList_View" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 0px">
                <div class="modal-header tmsModalHeader">
                    <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title Panel_Title">JR Details</h4>
                </div>
                <div class="modal-body tmsModalBody">
                    <div class="row tmsModalContent">
                        <asp:UpdatePanel ID="upJrDetails" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none ">
                                    <div class="form-group col-sm-4 padding-none">
                                        <label class="control-label col-sm-5 padding-none">JR Name</label>
                                        <div class="col-sm-7">
                                            <span class="semiColStyle">:</span><asp:Label ID="lblJrName" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-3 padding-none">
                                        <label class="control-label col-sm-6">JR Version</label>
                                        <div class="col-sm-6">
                                            <span class="semiColStyle">:</span><asp:Label ID="lblJrVersion" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-5 padding-none">
                                        <label class="control-label col-sm-5">Trainee Name</label>
                                        <div class="col-sm-7">
                                            <span class="semiColStyle">:</span><asp:Label ID="lblTraineeName" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                                    <div class="form-group">
                                        <label class="control-label col-sm-5 padding-none">JR Description</label>
                                        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none " style="overflow: auto; max-height: 350px;">
                                            <%--<asp:TextBox ID="txtJrDescription" runat="server" CssClass="col-lg-12 col-sm-12 col-xs-12 col-md-12 txt_JRDES" TextMode="MultiLine" Rows="4" Enabled="false"></asp:TextBox>--%>
                                            <div style="border: groove; height: 285px; overflow: auto; border-width: 1px; padding-left: 20px">
                                                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="nav nav-tabs col-lg-12 col-sm-12 col-xs-12 col-md-12" style="margin-top: 10px;">
                                    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
                                    <li><a data-toggle="tab" href="#Comments">Comments</a></li>
                                    <%--<li><a data-toggle="tab" href="#Comments"><i class="fa fa-comments-o" style="font-size: 18px"></i>&nbsp;Comments History</a></li>--%>
                                </ul>
                                <div class="tab-content col-lg-12 col-sm-12 col-xs-12 col-md-12" style="max-height: 192px; overflow-y: scroll;">
                                    <div id="General" class="tab-pane fade in active">
                                        <div class="">
                                            <div class="col-sm-6 ">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-5">Prepared By</label>
                                                        <div class="col-sm-7">
                                                            <span class="semiColStyle">:</span><asp:Label ID="lblPreparedBy" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-5">Prepared Date</label>
                                                        <div class="col-sm-7">
                                                            <span class="semiColStyle">:</span><asp:Label ID="lblPreparedDate" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-5">TS Prepared By</label>
                                                        <div class="col-sm-7">
                                                            <span class="semiColStyle">:</span><asp:Label ID="lblTS_PreparedBy" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-5">TS Prepared Date</label>
                                                        <div class="col-sm-7">
                                                            <span class="semiColStyle">:</span><asp:Label ID="lblTS_PreparedDate" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-5">Approved By</label>
                                                        <div class="col-sm-7">
                                                            <span class="semiColStyle">:</span><asp:Label ID="lblApprovedBy" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-5">Approved Date</label>
                                                        <div class="col-sm-7">
                                                            <span class="semiColStyle">:</span><asp:Label ID="lblApprovedDate" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-5">TS Approved By</label>
                                                        <div class="col-sm-7">
                                                            <span class="semiColStyle">:</span><asp:Label ID="lblTS_ApprovedBy" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-5">TS Approved Date</label>
                                                        <div class="col-sm-7">
                                                            <span class="semiColStyle">:</span><asp:Label ID="lblTS_ApprovedDate" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="Comments" class="tab-pane fade">
                                        <div class="form-group">
                                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                                                <div class="col-lg-12 col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xs-12 col-md-12 padding-none">
                                                    <asp:UpdatePanel ID="upgvJR_Commentry" runat="server" UpdateMode="Always">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gvJR_Commentry" runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixHeadJR_Commentry AspGrid"
                                                                EmptyDataText="No Comments" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                                <RowStyle CssClass="col-xs-12 grid_border padding-none" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Role" SortExpression="Role" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtRole" runat="server" Text='<%# Bind("Role") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblRole" runat="server" Text='<%# Bind("Role") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Action" SortExpression="Action" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtAction" runat="server" Text='<%# Bind("Action") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAction" runat="server" Text='<%# Bind("Action") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Commented By" SortExpression="EmpName" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Commented Date" SortExpression="CommentedDate" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtCommentedDate" runat="server" Text='<%# Bind("CommentedDate") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCommentedDate" runat="server" Text='<%# Bind("CommentedDate") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Comment" SortExpression="Comment" HeaderStyle-CssClass="col-xs-3" ItemStyle-CssClass="col-xs-3">
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtComment" runat="server" Text='<%# Bind("Comment") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblComment" runat="server" Text='<%# Bind("Comment") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal GetTraineeDetailsForTF -->
    <div class="modal fade" id="modelTrainingFile" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 98%;">
            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 0px">
                <div class="modal-header tmsModalHeader">
                    <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title Panel_Title">Training File</h4>
                </div>
                <div class="modal-body tmsModalBody">
                    <div class="row tmsModalContent">
                        <asp:UpdatePanel ID="upTrainingFile" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4">Select Training File</label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlTrainingFileView" OnSelectedIndexChanged="ddlTrainingFileView_SelectedIndexChanged" runat="server" CssClass="form-control SearchDropDown" Width="300px" AutoPostBack="true">
                                                    <asp:ListItem Value="0">Complete Training File</asp:ListItem>
                                                    <asp:ListItem Value="1">Job Description</asp:ListItem>
                                                    <asp:ListItem Value="2">Training Schedule</asp:ListItem>
                                                    <asp:ListItem Value="3">Retrospective Qualification Form</asp:ListItem>
                                                    <asp:ListItem Value="4">Training Record</asp:ListItem>
                                                    <asp:ListItem Value="5">Training Questionnaire</asp:ListItem>
                                                    <asp:ListItem Value="6">Training Certificate</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-2" runat="server" id="divPrint">
                                                <asp:Button ID="btnPrint" runat="server" CssClass="btn TMS_Button" OnClick="btnPrint_Click" Text="Print" Visible="false" />
                                            </div>
                                        </div>
                                        <div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12" style="overflow: auto; max-height: 640px;">
                                    <%--<rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="100%" ShowBackButton="false" ShowExportButton="false" ShowFindControls="false"
                                        ShowCredentialPrompts="False" ShowDocumentMapButton="False" ShowExportControls="False" ShowPageNavigationControls="False" ShowParameterPrompts="False"
                                        ShowPrintButton="False" ShowPromptAreaButton="False" ShowRefreshButton="False" ShowToolBar="False" ShowWaitControlCancelLink="False" ShowZoomControl="False">
                                    </rsweb:ReportViewer>--%>
                                    <asp:Literal ID="Literal2" runat="server" />
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <uc1:SessionTimeOut runat="server" ID="SessionTimeOut" />

    <script type="text/javascript">
        $(document).ready(function () {
            // Fix up GridView to support THEAD tags            
            fixGvHeader();
            fixHeader();

        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixGvHeader();
            fixHeader();
        });

        function fixGvHeader() {
            var tbl = document.getElementsByClassName("fixHeadEmployeeJRList");
            // Fix up GridView to support THEAD tags     
            if ($(".fixHeadEmployeeJRList").find("thead").length == 0) {
                $(".fixHeadEmployeeJRList tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadEmployeeJRList thead tr").append($(".fixHeadEmployeeJRList th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadEmployeeJRList tbody tr:first").remove();
                }
            }
        }
        function fixHeader() {
            var tbl = document.getElementsByClassName("fixHeadJR_Commentry");
            // Fix up GridView to support THEAD tags   
            if ($(".fixHeadJR_Commentry").find("thead").length == 0) {
                $(".fixHeadJR_Commentry tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadJR_Commentry thead tr").append($(".fixHeadJR_Commentry th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadJR_Commentry tbody tr:first").remove();
                }
            }
        }
    </script>

    <script>
        function OpenModelViewJR() {
            $('#modelEmpJrList_View').modal({ backdrop: 'static', keyboard: false });
        }
        function OpenModelTrainingFile() {
            $('#modelTrainingFile').modal({ backdrop: 'static', keyboard: false });
        }
    </script>
</asp:Content>
