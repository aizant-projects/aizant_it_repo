﻿using Aizant_API_Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_ML_BusinessLayer
{
    public class TMS_ML_BAL
    {
        public List<TMS_ML_BusinessObjects.AizantIT_AuditObservation> GetDocumentRelatedObservations(int DocID)
        {
            try
            {
                using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
                {
                    List<TMS_ML_BusinessObjects.AizantIT_AuditObservation> ObservationsList = new List<TMS_ML_BusinessObjects.AizantIT_AuditObservation>();
                    var ObjObservationsList = from T1 in dbContext.AizantIT_TOP_10_FDA_Observations_based_on_DocID
                                              join T2 in dbContext.AizantIT_FDA_Drugs_Data on new { f1 = T1.Obv_ID, f2 = T1.Type_of_obv} equals new { f1 = T2.Drug_Data_Obv_ID, f2 = 1} into temp1
                                              from x in temp1.DefaultIfEmpty()
                                              join T3 in dbContext.AizantIT_AuditObservation on new {f1=T1.Obv_ID,f2=T1.Type_of_obv} equals new {f1=T3.ObservationID,f2=0} into temp2
                                              from y in temp2.DefaultIfEmpty()
                                              where T1.DocumentID==DocID
                            select new {
                                ObservationID = T1.Obv_ID,//T1.Type_of_obv == 0 ? y.ObservationID : x.Drug_Data_Obv_ID, 
                                ObservationDescription = T1.Type_of_obv==1 ? x.Long_Description:y.ObservationDescription,
                                 ObservationNumber= T1.Type_of_obv == 0? y.ObservationNum:""
                            };
                    foreach (var item in ObjObservationsList)
                    {
                        if (item.ObservationDescription!=null && item.ObservationDescription!="")
                        {
                            ObservationsList.Add(new TMS_ML_BusinessObjects.AizantIT_AuditObservation
                            {
                                ObservationID = item.ObservationID,
                                ObservationDescription = item.ObservationDescription,
                                ObservationNum = item.ObservationNumber
                            });
                        }
                    }
                    return ObservationsList;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}


