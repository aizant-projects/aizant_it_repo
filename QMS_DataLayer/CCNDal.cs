﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using QMS_BO;
using System.Data.SqlClient;
using System.Data;
using QMS_BO.QMS_ChangeControl_BO;
using QMS_BO.QMS_CAPABO;

namespace QMS_DataLayer
{
   public class CCNDal
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());


        public DataSet GetCCNListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch,
            int Role,/* int CCN_ID,*/string QualityEvent, int RoleIDCharts, int DeptidDb, string FromDateDb, 
            string ToDateDb,int EmployeeId,string sSearch_CCN_Number,string sSearch_Department,string sSearch_Block,string sSearch_Initiatedby
            ,string sSearch_Classification,string sSearch_CCN_Date, string sSearch_CurrentStatus)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetCCNList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter EmpLoginID = new SqlParameter("@EmployeeID", SqlDbType.Int);
                cmd.Parameters.Add(EmpLoginID).Value = Convert.ToInt32(EmployeeId);
                SqlParameter Roel1 = new SqlParameter("@Role", SqlDbType.Int);
                cmd.Parameters.Add(Roel1).Value = Role;
                //SqlParameter vaCCNID = new SqlParameter("@CCN_ID", SqlDbType.VarChar);
                //cmd.Parameters.Add(vaCCNID).Value = CCN_ID;
                SqlParameter parmRoleIdDb = new SqlParameter("@RoleIdCht", SqlDbType.Int);
                cmd.Parameters.Add(parmRoleIdDb).Value = RoleIDCharts;
                SqlParameter parmDeptIdDb = new SqlParameter("@DeptidCht", SqlDbType.Int);
                cmd.Parameters.Add(parmDeptIdDb).Value = DeptidDb;
                SqlParameter parmFromDate = new SqlParameter("@FromDateCht", SqlDbType.VarChar);
                cmd.Parameters.Add(parmFromDate).Value = FromDateDb;
                SqlParameter parmToDate = new SqlParameter("@ToDateCht", SqlDbType.VarChar);
                cmd.Parameters.Add(parmToDate).Value = ToDateDb;
                SqlParameter parmQualityEvent = new SqlParameter("@QualityEventCht", SqlDbType.VarChar);
                cmd.Parameters.Add(parmQualityEvent).Value = QualityEvent;

                SqlParameter parmssSearch_CCN_Number = new SqlParameter("@sSearch_CCN_Number", SqlDbType.VarChar);
                cmd.Parameters.Add(parmssSearch_CCN_Number).Value = sSearch_CCN_Number;
                SqlParameter paramsSearch_Department = new SqlParameter("@sSearch_Department", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Department).Value = sSearch_Department;
                SqlParameter paramsSearch_Block = new SqlParameter("@sSearch_Block", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Block).Value = sSearch_Block;
                SqlParameter paramsSearch_Initiatedby = new SqlParameter("@sSearch_Initiatedby", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Initiatedby).Value = sSearch_Initiatedby;
                SqlParameter paramsSearch_Classification = new SqlParameter("@sSearch_Classification", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Classification).Value = sSearch_Classification;
                SqlParameter paramsSearch_CCN_Date = new SqlParameter("@sSearch_CCN_Date", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_CCN_Date).Value = sSearch_CCN_Date;
                SqlParameter paramsSearchCurrentStatus = new SqlParameter("@sSearch_CurrentStatus", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearchCurrentStatus).Value = sSearch_CurrentStatus;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable CCNInsertDal(CCNMaster cCNMaster,string Status,int IsTempRefID)
        {
            string query = "QMS.AizantIT_SP_CCNInsertandUpdate";
                string capadate = Convert.ToDateTime(cCNMaster.CCN_Date).ToString("yyyy/MM/dd");
                // string targetdate12 = Convert.ToDateTime(cCNMaster.in).ToString("yyyy/MM/dd");
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ccno = new SqlParameter("@CCN_No", SqlDbType.VarChar);
                cmd.Parameters.Add(ccno).Value = cCNMaster.CCN_No;
            SqlParameter CCNID = new SqlParameter("@CCNID", SqlDbType.Int);
            cmd.Parameters.Add(CCNID).Value = cCNMaster.CCN_ID;
            SqlParameter CCNStatus = new SqlParameter("@CCNStatus", SqlDbType.Int);
                cmd.Parameters.Add(CCNStatus).Value = cCNMaster.CurrentStatus;
                SqlParameter deptid = new SqlParameter("@Department", SqlDbType.Int);
                cmd.Parameters.Add(deptid).Value = cCNMaster.DeptID;
                SqlParameter capdate = new SqlParameter("@CCN_Date", SqlDbType.VarChar);
                cmd.Parameters.Add(capdate).Value = capadate;
                SqlParameter TCID = new SqlParameter("@TC_ID", SqlDbType.Int);
                cmd.Parameters.Add(TCID).Value = cCNMaster.TC_ID;
                SqlParameter marketRegion = new SqlParameter("@Market_ID", SqlDbType.Int);
                cmd.Parameters.Add(marketRegion).Value = cCNMaster.marketID;
                SqlParameter itemID = new SqlParameter("@ItemID", SqlDbType.Int);
                cmd.Parameters.Add(itemID).Value = cCNMaster.ItemID;
                SqlParameter ProjectID = new SqlParameter("@ProjectID", SqlDbType.Int);
                cmd.Parameters.Add(ProjectID).Value = cCNMaster.ProjectID;
                SqlParameter DocType = new SqlParameter("@DocumentTypeID", SqlDbType.Int);
                cmd.Parameters.Add(DocType).Value = cCNMaster.DocumentType;
               
                
                if (cCNMaster.DocumentType == 0)//Others
                {
                    SqlParameter docname = new SqlParameter("@DocName", SqlDbType.VarChar);
                    cmd.Parameters.Add(docname).Value = cCNMaster.DocName.Trim();
                    SqlParameter versionid = new SqlParameter("@ReferenceNumberOthers", SqlDbType.VarChar);
                    cmd.Parameters.Add(versionid).Value = cCNMaster.VersionID.Trim();
                }
                else if(cCNMaster.DocumentType!=0 && cCNMaster.DocumentType!=-1)//Not others and Not Select
                {
                    SqlParameter versionid = new SqlParameter("@versionID", SqlDbType.Int);
                    cmd.Parameters.Add(versionid).Value = cCNMaster.VersionID;
                }
                
                SqlParameter CChange = new SqlParameter("@CategoryofChange", SqlDbType.Int);
                cmd.Parameters.Add(CChange).Value = cCNMaster.CategoryofChange;
                SqlParameter DPC = new SqlParameter("@DescriptionofProposedChanges", SqlDbType.VarChar);
                cmd.Parameters.Add(DPC).Value = cCNMaster.DescriptionofProposedChanges.Trim();
                SqlParameter RFC = new SqlParameter("@ReasonforChange", SqlDbType.VarChar);
                cmd.Parameters.Add(RFC).Value = cCNMaster.ReasonforChange.Trim();
                SqlParameter JFC = new SqlParameter("@JustificationforChange", SqlDbType.VarChar);
                cmd.Parameters.Add(JFC).Value = cCNMaster.JustificationforChange.Trim();
                SqlParameter EXP = new SqlParameter("@ExistingProcedure", SqlDbType.VarChar);
                cmd.Parameters.Add(EXP).Value = cCNMaster.ExistingProcedure.Trim();
                SqlParameter IMPID = new SqlParameter("@Initiator_EmpID", SqlDbType.Int);
                cmd.Parameters.Add(IMPID).Value = cCNMaster.Initiator_EmpID;
                SqlParameter HOD1 = new SqlParameter("@HOD_EmpID", SqlDbType.Int);
                cmd.Parameters.Add(HOD1).Value = cCNMaster.HOD_EmpID;
                SqlParameter Imp_Comments = new SqlParameter("@Initiator_Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(Imp_Comments).Value = cCNMaster.Initiator_Comments.Trim();
                SqlParameter Opration = new SqlParameter("@Operation", SqlDbType.VarChar);
                cmd.Parameters.Add(Opration).Value = Status.Trim();
            SqlParameter Batch = new SqlParameter("@ApplicableBatch", SqlDbType.VarChar);
            cmd.Parameters.Add(Batch).Value = cCNMaster.ApplicableBatch;
            SqlParameter Equipment = new SqlParameter("@ApplicableEquipment", SqlDbType.VarChar);
            cmd.Parameters.Add(Equipment).Value = cCNMaster.ApplicableEquipment;
            SqlParameter AppTimeLine = new SqlParameter("@ApplicableTimeline", SqlDbType.VarChar);
            cmd.Parameters.Add(AppTimeLine).Value = cCNMaster.ApplicableTimeline;
            SqlParameter TempRefID = new SqlParameter("@IsTempRefID", SqlDbType.Int);
            cmd.Parameters.Add(TempRefID).Value = IsTempRefID;
            //con.Open();
            //  cmd.ExecuteNonQuery();
            //    con.Close();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
             DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
            
        }
        
        public DataTable CCNSplitDeptStringDal(string deptIDS)
        {
            try
            {
                string query = "QMS.AizantIT_SP_CCNSplitStringDeptWise";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter Qualityidd = new SqlParameter("@DeptIDS", SqlDbType.VarChar);
                cmd.Parameters.Add(Qualityidd).Value = deptIDS;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int HODActionCCNDal(CCNMaster cCN)
        {
            try
            {
                string query = "QMS.AizantIT_SP_CCNHOD_Action";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                if (cCN.HOD_Comments == null)
                {
                    cCN.HOD_Comments = "";
                }
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter CCNID = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(CCNID).Value = cCN.CCN_ID;
                SqlParameter EMpID = new SqlParameter("@EMpID", SqlDbType.Int);
                cmd.Parameters.Add(EMpID).Value = cCN.Initiator_EmpID;
                SqlParameter Comments = new SqlParameter("@HOD_Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(Comments).Value = cCN.HOD_Comments.Trim(); 
                if (cCN.QA_EmpID.ToString() == null)
                {
                    SqlParameter QAEMpID = new SqlParameter("@QAEMpID", SqlDbType.Int);
                    cmd.Parameters.Add(QAEMpID).Value = 0;
                }
                else
                {
                    SqlParameter QAEMpID = new SqlParameter("@QAEMpID", SqlDbType.Int);
                    cmd.Parameters.Add(QAEMpID).Value = cCN.QA_EmpID;
                }
                SqlParameter QualityNameID = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                cmd.Parameters.Add(QualityNameID).Value = Convert.ToInt32(cCN.CurrentStatus);
                SqlParameter CCN_Number = new SqlParameter("@CCN_No", SqlDbType.VarChar);
                cmd.Parameters.Add(CCN_Number).Value = cCN.CCN_No;
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int QAAction_CCNCloserDal(CCNClose objCCN)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_CCN_QA_Closer]";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter CCNID = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(CCNID).Value = objCCN.CCNID;
                SqlParameter EmployeeID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(EmployeeID).Value = objCCN.QAEMpID;
                SqlParameter CloserComments = new SqlParameter("@CloserComments", SqlDbType.VarChar);
                cmd.Parameters.Add(CloserComments).Value = (objCCN.Comments.Trim());
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteCapaTemp(int AssessmentId,string Status,int QualityEventTypeID)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_CCN_DeleteCapaTemp]";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter AssessID = new SqlParameter("@assessmentId", SqlDbType.Int);
                cmd.Parameters.Add(AssessID).Value = AssessmentId;
                SqlParameter StatusQuality = new SqlParameter("@QualityEvent", SqlDbType.VarChar);
                cmd.Parameters.Add(StatusQuality).Value = Status;
                SqlParameter NoteToFileID = new SqlParameter("@QualityEventTypeID", SqlDbType.Int);
                cmd.Parameters.Add(NoteToFileID).Value = QualityEventTypeID;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int AssessmenT_ActionsCCNDal(CCNMaster cCN)
        {
            try
            {
                string query = "QMS.AizantIT_SP_CCN_AsessessmentAprval_Revert_Action";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                if (cCN.Comments == null)
                {
                    cCN.Comments = "";
                }
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter CCNID = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(CCNID).Value = cCN.CCN_ID;
                SqlParameter UID = new SqlParameter("@AssmentID", SqlDbType.Int);
                cmd.Parameters.Add(UID).Value = cCN.AssessmentId;
                SqlParameter Comments = new SqlParameter("@HeadQA_EmpID", SqlDbType.Int);
                cmd.Parameters.Add(Comments).Value = cCN.EMPID;
                SqlParameter QualityNameID1 = new SqlParameter("@Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(QualityNameID1).Value = (cCN.Comments.Trim());
                SqlParameter QualityNameID = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                cmd.Parameters.Add(QualityNameID).Value = Convert.ToInt32(cCN.CurrentStatus);
                
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int HeadQAActionCCNDal(CCNMaster cCN)
        {
            try
            {
                string query = "QMS.AizantIT_SP_CCN_Head_QA_Action";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                if (cCN.HEADQA_Comments == null)
                {
                    cCN.HEADQA_Comments = "";
                }
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter CCNID = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(CCNID).Value = cCN.CCN_ID;
                SqlParameter Comments = new SqlParameter("@HeadQA_EmpID", SqlDbType.Int);
                cmd.Parameters.Add(Comments).Value = cCN.HeadQA_EmpID;
                SqlParameter QualityNameID1 = new SqlParameter("@Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(QualityNameID1).Value = (cCN.HEADQA_Comments.Trim());
                SqlParameter QualityNameID = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                cmd.Parameters.Add(QualityNameID).Value = Convert.ToInt32(cCN.CurrentStatus);
                SqlParameter WhomeToRevert = new SqlParameter("@WhomeToRevert", SqlDbType.Int);
                cmd.Parameters.Add(WhomeToRevert).Value = Convert.ToInt32(cCN.WhomeToRevert);
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int AssessmentRevertHeadQADAL(CCNMaster cCN)
        {
            try
            {
                string query = "QMS.AizantIT_SP_AssessmentRevertHeadQA";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                if (cCN.HEADQA_Comments == null)
                {
                    cCN.HEADQA_Comments = "";
                }
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter CCNID = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(CCNID).Value = cCN.CCN_ID;
                SqlParameter Comments = new SqlParameter("@HeadQA_EmpID", SqlDbType.Int);
                cmd.Parameters.Add(Comments).Value = cCN.HeadQA_EmpID;
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int RefershAssessmentHeadQAPAgeDAL(CCNMaster cCN)
        {
            try
            {
                string query = "QMS.AizantIT_SP_RefershHeadAssessmentPage";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter CCNID = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(CCNID).Value = cCN.CCN_ID;
                SqlParameter Comments = new SqlParameter("@HeadQA_EmpID", SqlDbType.Int);
                cmd.Parameters.Add(Comments).Value = cCN.HeadQA_EmpID;
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int QAActionCCNDal(CCNMaster cCN)
        {
            try
            {
                if (cCN.DatabaseSubmitStatus == "QAApprove")
                {
                    string query = "QMS.AizantIT_SP_CCNQA_Action";
                    SqlCommand cmd = new SqlCommand(query, con);
                    con.Open();
                    if (cCN.QA_Comments == null)
                    {
                        cCN.QA_Comments = "";
                    }
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter CCNID = new SqlParameter("@CCNID", SqlDbType.Int);
                    cmd.Parameters.Add(CCNID).Value = cCN.CCN_ID;
                    SqlParameter EMpID = new SqlParameter("@EMpID", SqlDbType.Int);
                    cmd.Parameters.Add(EMpID).Value = cCN.QA_EmpID;
                    SqlParameter Comments = new SqlParameter("@QA_Comments", SqlDbType.VarChar);
                    cmd.Parameters.Add(Comments).Value = cCN.QA_Comments.Trim();
                    SqlParameter QualityNameID = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                    cmd.Parameters.Add(QualityNameID).Value = Convert.ToInt32(cCN.CurrentStatus);
                    int i = cmd.ExecuteNonQuery();
                    con.Close();
                    return i;
                }
                else
                {
                    string query1 = "QMS.AizantIT_SP_CCNQA_Asses_Summary_Action";
                    SqlCommand cmd1 = new SqlCommand(query1, con);
                    con.Open();
                    if (cCN.QA_Comments == null)
                    {
                        cCN.QA_Comments = "";
                    }
                    cmd1.CommandType = CommandType.StoredProcedure;
                    SqlParameter CCNID = new SqlParameter("@CCNID", SqlDbType.Int);
                    cmd1.Parameters.Add(CCNID).Value = cCN.CCN_ID;
                    SqlParameter EMpID = new SqlParameter("@EMpID", SqlDbType.Int);
                    cmd1.Parameters.Add(EMpID).Value = cCN.QA_EmpID;
                    SqlParameter EMpID1 = new SqlParameter("@HeadQA_EmpID", SqlDbType.Int);
                    cmd1.Parameters.Add(EMpID1).Value = cCN.HeadQA_EmpID;
                    SqlParameter Comments = new SqlParameter("@Assessment_Summary", SqlDbType.VarChar);
                    cmd1.Parameters.Add(Comments).Value = cCN.QA_Comments.Trim();
                    SqlParameter QualityNameID = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                    cmd1.Parameters.Add(QualityNameID).Value = Convert.ToInt32(cCN.CurrentStatus);
                    SqlParameter QualityNameID1 = new SqlParameter("@changeClassification", SqlDbType.Int);
                    cmd1.Parameters.Add(QualityNameID1).Value = Convert.ToInt32(cCN.ChangeClassification);
                    int i = cmd1.ExecuteNonQuery();
                    con.Close();
                    return i;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DeleteCCN_ActionPlanAndCCCRecordsDal(int id,string operation, int ActionBy,string ActionFileNo)
        {
            try
            {
                string query = "QMS.AizantIT_SP_DeleteCCN_ActionPlanAndCCCRecords";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter CCNID = new SqlParameter("@id", SqlDbType.Int);
                cmd.Parameters.Add(CCNID).Value = id;
                SqlParameter spoperation = new SqlParameter("@operation", SqlDbType.VarChar);
                cmd.Parameters.Add(spoperation).Value = operation;
                SqlParameter SPActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(SPActionBy).Value = ActionBy;
                SqlParameter SPActionFileNo = new SqlParameter("@ActionFileNO", SqlDbType.VarChar);
                cmd.Parameters.Add(SPActionFileNo).Value = ActionFileNo;
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable AddCCN_CommitteDal(int AssignedById,int uniqueid,int cCNid,int BlockId,int AssignedToID,int deptid,string opeation,int AssStatus)
        {
            try
            {
                string query = "QMS.AizantIT_SP_CCN_InsertAndUpdate_CCNCommitte";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter CCNID = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(CCNID).Value = cCNid;
                SqlParameter EMpID11 = new SqlParameter("@EmployeeiD", SqlDbType.Int);
                cmd.Parameters.Add(EMpID11).Value = AssignedById;//AssignedByID
                SqlParameter EMpID1 = new SqlParameter("@uniquid", SqlDbType.Int);
                cmd.Parameters.Add(EMpID1).Value = uniqueid;
                SqlParameter EMpID = new SqlParameter("@EMpID", SqlDbType.Int);
                cmd.Parameters.Add(EMpID).Value = AssignedToID;//AssignedToID
                SqlParameter Comments = new SqlParameter("@deptid", SqlDbType.Int);
                cmd.Parameters.Add(Comments).Value = deptid;
                SqlParameter Comments1 = new SqlParameter("@blockid", SqlDbType.Int);
                cmd.Parameters.Add(Comments1).Value = BlockId;
                SqlParameter opeation1 = new SqlParameter("@opeation", SqlDbType.VarChar);
                cmd.Parameters.Add(opeation1).Value = opeation;
                SqlParameter AssessmentStatus = new SqlParameter("@AssessmentStatus", SqlDbType.Int);
                cmd.Parameters.Add(AssessmentStatus).Value = AssStatus;
                DataTable dt = new DataTable();
                if (opeation == "CCN_count")
                {
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);

                    sda.Fill(dt);
                }
                else if (opeation == "CCN_Verifycount")
                {
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);

                    sda.Fill(dt);

                }
                else
                {
                    int i = cmd.ExecuteNonQuery();
                }

                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddCCN_DepartmentBlockCommiteeDal(DataTable dtDeptImpactlst)
        {
            try
            {
                string query = "QMS.[AizantIT_SP_InsertChangeCommiteeDepartBlock]";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AssessmentDeptList", dtDeptImpactlst);
                    int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
             }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //int cCNid, int ImpactPointid, int isyesorno, string Refno, string impactcomments,
        public int AddCCN_ImpactPointDal(int EmpID, DataTable dtImpactlst,int CCNID, string comments, int DeptidEmpty,int blockEmpty,int Status, int AssessmentID)
        {
            try
            {
                string query = "QMS.AizantIT_SP_CCN_ImpactPointInsert";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                if (comments == null) { comments = "NA"; }
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter CCNID1 = new SqlParameter("@CCN_ID", SqlDbType.Int);
                cmd.Parameters.Add(CCNID1).Value = CCNID;
                SqlParameter EMpID1 = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(EMpID1).Value = EmpID;
                cmd.Parameters.AddWithValue("@AssessmentList", dtImpactlst);
                SqlParameter opeation21 = new SqlParameter("@comments", SqlDbType.VarChar);
                cmd.Parameters.Add(opeation21).Value = comments.Trim();
                SqlParameter CommitteStatus = new SqlParameter("@CommitteStatus", SqlDbType.Int);
                cmd.Parameters.Add(CommitteStatus).Value = Status;
                SqlParameter AssessmentID1 = new SqlParameter("@AssessmentID", SqlDbType.Int);
                cmd.Parameters.Add(AssessmentID1).Value = AssessmentID;
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }
        public DataSet GetCCN_DeptAssListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch,  int CCNID)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetCCN_DeptAssessmentList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter vaHOD = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(vaHOD).Value = CCNID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCCN_VerificationListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int CCNID)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetCCN_VerificationList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter vaHOD = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(vaHOD).Value = CCNID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetHodAttachmentsDAL(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int CCNID,int AssessID,string Status)
        {
            try
            {
                string query = "QMS.[AizantIT_SP_GetCCNHodFileUploadList]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter vaCCnid = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(vaCCnid).Value = CCNID;
                SqlParameter vaAssid = new SqlParameter("@AssessmentID", SqlDbType.Int);
                cmd.Parameters.Add(vaAssid).Value = AssessID;
                SqlParameter vaStatus = new SqlParameter("@Status", SqlDbType.VarChar);
                cmd.Parameters.Add(vaStatus).Value = Status;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCCNAssessmentDAL(int CCNID)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetAssessmentIDForReport";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter vaHOD = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(vaHOD).Value = CCNID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetDropdownbindingCCNCreateDAL(int empid )
        {
            try
            {
                string query = "QMS.[AizantIT_SP_CCNGetDropdownBinding]";
                SqlCommand cmd = new SqlCommand(query,con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramEMpid = new SqlParameter("@EmpdID", SqlDbType.Int);
                cmd.Parameters.Add(paramEMpid).Value = empid;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateAssessmentHODsDAL(int HodEmpId,int AssessmentID,int CCNID,int ActionBy, string AssessmentComments, out int Result)
        {
            try
            {
                string query = "QMS.AizantIT_SP_ModifiedAssessmentHOds";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramAssID = new SqlParameter("@AssessmentID", SqlDbType.Int);
                cmd.Parameters.Add(paramAssID).Value = AssessmentID;
                SqlParameter paramHodID = new SqlParameter("@Modified_HodEmpId", SqlDbType.Int);
                cmd.Parameters.Add(paramHodID).Value = HodEmpId;
                SqlParameter paramCCNID = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(paramCCNID).Value = CCNID;
                SqlParameter paramActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(paramActionBy).Value = ActionBy;
                SqlParameter paramAssessComments = new SqlParameter("@AssessmentComments", SqlDbType.VarChar);
                cmd.Parameters.Add(paramAssessComments).Value = AssessmentComments;
                SqlParameter GetResult = cmd.Parameters.Add("@ResultOutParm", SqlDbType.Int);
                GetResult.Direction = ParameterDirection.Output;
                int i = cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@ResultOutParm"].Value);
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int ModifiedAssignedEmployeeInAdminDAL(int EMPID, int CCNID,int Status,int ActionBy, string ReassignComments, out int Result)
        {
            try
            {
                string query = "QMS.[AizantIT_SP_ModifiedCCNEmployessInAdminRole]";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramAssID = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(paramAssID).Value = CCNID;
                SqlParameter paramHodID = new SqlParameter("@Modified_EmpId", SqlDbType.Int);
                cmd.Parameters.Add(paramHodID).Value = EMPID;
                SqlParameter paramRoleStatus = new SqlParameter("@RoleStatus", SqlDbType.Int);
                cmd.Parameters.Add(paramRoleStatus).Value = Status;
                SqlParameter paramReAssignComments = new SqlParameter("@ReassignComments", SqlDbType.VarChar);
                cmd.Parameters.Add(paramReAssignComments).Value = ReassignComments;
                SqlParameter paramActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(paramActionBy).Value = ActionBy;
                SqlParameter GetResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                GetResult.Direction = ParameterDirection.Output;
                int i = cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int ModifiedCAPA_EmployeesInAdminManageDAL(int EMPID, int capaid, int Status,int ActionBy, string GetReAssignComments, out int Result)
        {
            try
            {
                string query = "QMS.[AizantIT_SP_ModifiedCAPAEmployessInAdminRole]";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramAssID = new SqlParameter("@CapaID", SqlDbType.Int);
                cmd.Parameters.Add(paramAssID).Value = capaid;
                SqlParameter paramHodID = new SqlParameter("@Modified_EmpId", SqlDbType.Int);
                cmd.Parameters.Add(paramHodID).Value = EMPID;
                SqlParameter paramRoleStatus = new SqlParameter("@RoleStatus", SqlDbType.Int);
                cmd.Parameters.Add(paramRoleStatus).Value = Status;
                SqlParameter paramActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(paramActionBy).Value = ActionBy;
                SqlParameter paramComments = new SqlParameter("@ReassignComments", SqlDbType.VarChar);
                cmd.Parameters.Add(paramComments).Value = GetReAssignComments;
                SqlParameter GetResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                GetResult.Direction = ParameterDirection.Output;
                int i = cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int QAActionSaveDraft(CCNMaster cCN)
        {
            try
            {
               
                    string query = "QMS.AizantIT_SP_QAActionSaveDraft";
                    SqlCommand cmd = new SqlCommand(query, con);
                    con.Open();
                    if (cCN.QA_Comments == null)
                    {
                        cCN.QA_Comments = "";
                    }
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter CCNID = new SqlParameter("@CCNID", SqlDbType.Int);
                    cmd.Parameters.Add(CCNID).Value = cCN.CCN_ID;
                    SqlParameter EMpID = new SqlParameter("@EMpID", SqlDbType.Int);
                    cmd.Parameters.Add(EMpID).Value = cCN.QA_EmpID;
                    SqlParameter Comments = new SqlParameter("@QA_Comments", SqlDbType.VarChar);
                    cmd.Parameters.Add(Comments).Value = cCN.QA_Comments.Trim();
                    SqlParameter QualityNameID = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                    cmd.Parameters.Add(QualityNameID).Value = Convert.ToInt32(cCN.CurrentStatus);
                    int i = cmd.ExecuteNonQuery();
                    con.Close();
                    return i;
                

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
