﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using QMS_BO.QMS_Errata_BO;
using QMS_BO.QMS_CAPABO;
using QMS_BO.QMS_NoteTofile_BO;
using QMS_BO.QMS_Incident_BO;
using QMS_BO.QMS_Dashboard_BO;

namespace QMS_DataLayer
{
    public class QMS_DAL
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
        public DataSet GetActionPlanLISTDal(int iDisplayLength, int iDisplayStart, int iSortCol_0,string sSortDir_0, string sSearch)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetActionPlanList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetBlockMasterLISTDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetBlockMasterList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetImpactPointLISTDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetImpactPointList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetTypeChangeMasterLISTDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetTypeofChangeMasterList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetImpactPointAndDeptLinkDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetImpactPointAndDeptLink_List";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCAPAListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch,string Role, int RoleIDCharts, int DeptidDb,
            string FromDateDb, string ToDateDb,int CapaQualityEventType,int OverDueCapaRoleID, int RptDeptID,
            string RptFrmDate, string RptToDate,int Employeeid,int RptQualityEvent, int isAll,
            string sSearch_CAPA_Number, string sSearch_Department, string sSearch_QualityEventName, string sSearch_QualityEvent_Number, string sSearch_EmployeeName
              , string sSearch_CAPA_Date, string sSearch_TargetDate, string sSearch_CurrentStatus)
        {
            try
            {
                RptFrmDate = RptFrmDate == "0" ? DateTime.Now.AddMonths(-1).ToString("dd MMM yyyy") : RptFrmDate;
                RptToDate = RptToDate == "0" ? DateTime.Now.ToString("dd MMM yyyy") : RptToDate;
                string query = "QMS.AizantIT_SP_GetCAPAList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter EmpLoginID = new SqlParameter("@EmployeeID", SqlDbType.Int);
                cmd.Parameters.Add(EmpLoginID).Value = Convert.ToInt32(Employeeid);
                SqlParameter RoleName = new SqlParameter("@Role", SqlDbType.VarChar);
                cmd.Parameters.Add(RoleName).Value = Role;
                SqlParameter parmRoleIdDb = new SqlParameter("@RoleIdCht", SqlDbType.Int);
                cmd.Parameters.Add(parmRoleIdDb).Value = RoleIDCharts;
                SqlParameter parmDeptIdDb = new SqlParameter("@DeptidCht", SqlDbType.Int);
                cmd.Parameters.Add(parmDeptIdDb).Value = Convert.ToInt32(DeptidDb);
                SqlParameter parmFromDate = new SqlParameter("@FromDateCht", SqlDbType.VarChar);
                cmd.Parameters.Add(parmFromDate).Value = FromDateDb;
                SqlParameter parmToDate = new SqlParameter("@ToDateCht", SqlDbType.VarChar);
                cmd.Parameters.Add(parmToDate).Value = ToDateDb;
                SqlParameter parmOverDueRoleID = new SqlParameter("@CapaOverDueRoleID", SqlDbType.VarChar);
                cmd.Parameters.Add(parmOverDueRoleID).Value = OverDueCapaRoleID;
                SqlParameter parmEvent = new SqlParameter("@CapaQualityEventCht", SqlDbType.Int);
                cmd.Parameters.Add(parmEvent).Value = CapaQualityEventType;
                //parameters for Capa Report
                SqlParameter paramRptDeptID = new SqlParameter("@DeptidRpt", SqlDbType.Int);
                cmd.Parameters.Add(paramRptDeptID).Value =RptDeptID;
                SqlParameter paramRptQuality = new SqlParameter("@RptQualityEvent", SqlDbType.Int);
                cmd.Parameters.Add(paramRptQuality).Value = RptQualityEvent;
                SqlParameter paramRptFrmDate = new SqlParameter("@FromDateRpt", SqlDbType.DateTime);
                cmd.Parameters.Add(paramRptFrmDate).Value = Convert.ToDateTime(RptFrmDate);
                SqlParameter paramRptToDate = new SqlParameter("@ToDateRpt", SqlDbType.DateTime);
                cmd.Parameters.Add(paramRptToDate).Value = Convert.ToDateTime(RptToDate);
                //isAll parameter for avoiding page display length
                SqlParameter paramisAll = new SqlParameter("@isAll", SqlDbType.Int);
                cmd.Parameters.Add(paramisAll).Value = isAll;

                SqlParameter paramsSearch_CAPA_Number = new SqlParameter("@sSearch_CAPA_Number", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_CAPA_Number).Value = sSearch_CAPA_Number;
                SqlParameter paramssSearch_Department = new SqlParameter("@sSearch_Department", SqlDbType.VarChar);
                cmd.Parameters.Add(paramssSearch_Department).Value = sSearch_Department;
                SqlParameter paramsSearch_QualityEventName = new SqlParameter("@sSearch_QualityEventName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_QualityEventName).Value = sSearch_QualityEventName;
                SqlParameter paramsSearch_QualityEvent_Number = new SqlParameter("@sSearch_QualityEvent_Number", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_QualityEvent_Number).Value = sSearch_QualityEvent_Number;
                SqlParameter paramsSearch_EmployeeName = new SqlParameter("@sSearch_EmployeeName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_EmployeeName).Value = sSearch_EmployeeName;
                SqlParameter paramsSearch_CAPA_Date = new SqlParameter("@sSearch_CAPA_Date", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_CAPA_Date).Value = sSearch_CAPA_Date;
                SqlParameter paramsSearch_TargetDate = new SqlParameter("@sSearch_TargetDate", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_TargetDate).Value = sSearch_TargetDate;
                SqlParameter paramsSearch_CurrentStatus = new SqlParameter("@sSearch_CurrentStatus", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_CurrentStatus).Value = sSearch_CurrentStatus;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        public DataSet GetCCNReportListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0,
            string sSortDir_0, string sSearch, string Role, int DeptidRT, string FromDateRt, string ToDateRt,int EmployeeID, int isAll,
            string sSearch_CCN_Number, string sSearch_Department, string sSearch_Initiatedby
            , string sSearch_Classification, string sSearch_CCN_Date, string sSearch_Due_Date, string sSearch_CurrentStatus)
        {
            try
            {
                FromDateRt = FromDateRt == "0" ? DateTime.Now.AddMonths(-1).ToString("dd MMM yyyy") : FromDateRt;
                ToDateRt = ToDateRt == "0" ? DateTime.Now.ToString("dd MMM yyyy") : ToDateRt;
                string query = "QMS.AizantIT_SP_GetCCNReportList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter Roel1 = new SqlParameter("@Role", SqlDbType.VarChar);
                cmd.Parameters.Add(Roel1).Value = Role;
                SqlParameter LoginEmployee = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
                cmd.Parameters.Add(LoginEmployee).Value = EmployeeID;
                SqlParameter parmDeptIdDb = new SqlParameter("@DeptRpt", SqlDbType.Int);
                cmd.Parameters.Add(parmDeptIdDb).Value = DeptidRT;
                SqlParameter parmFromDate = new SqlParameter("@FromDateRt", SqlDbType.DateTime);
                cmd.Parameters.Add(parmFromDate).Value = Convert.ToDateTime(FromDateRt);
                SqlParameter parmToDate = new SqlParameter("@ToDateRt", SqlDbType.DateTime);
                cmd.Parameters.Add(parmToDate).Value = Convert.ToDateTime(ToDateRt);
                SqlParameter paramisAll = new SqlParameter("@isAll", SqlDbType.Int);
                cmd.Parameters.Add(paramisAll).Value = isAll;

                SqlParameter parmssSearch_CCN_Number = new SqlParameter("@sSearch_CCN_Number", SqlDbType.VarChar);
                cmd.Parameters.Add(parmssSearch_CCN_Number).Value = sSearch_CCN_Number;
                SqlParameter paramsSearch_Department = new SqlParameter("@sSearch_Department", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Department).Value = sSearch_Department;
                SqlParameter paramsSearch_Due_Date = new SqlParameter("@sSearch_Due_Date", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Due_Date).Value = sSearch_Due_Date;
                SqlParameter paramsSearch_Initiatedby = new SqlParameter("@sSearch_Initiatedby", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Initiatedby).Value = sSearch_Initiatedby;
                SqlParameter paramsSearch_Classification = new SqlParameter("@sSearch_Classification", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_Classification).Value = sSearch_Classification;
                SqlParameter paramsSearch_CCN_Date = new SqlParameter("@sSearch_CCN_Date", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_CCN_Date).Value = sSearch_CCN_Date;
                SqlParameter paramsSearchCurrentStatus = new SqlParameter("@sSearch_CurrentStatus", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearchCurrentStatus).Value = sSearch_CurrentStatus;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCCNListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int EmpID, int HOD, int QA, int Head_QA,string Role,int CCN_ID,int QmsAdmin, string QualityEvent, int RoleIDCharts, int DeptidDb, string FromDateDb, string ToDateDb)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetCCNList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter varEMPID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(varEMPID).Value = Convert.ToInt32(EmpID);
                SqlParameter vaHOD = new SqlParameter("@HOD", SqlDbType.Int);
                cmd.Parameters.Add(vaHOD).Value = Convert.ToInt32(HOD);
                SqlParameter vaQA = new SqlParameter("@QA", SqlDbType.Int);
                cmd.Parameters.Add(vaQA).Value = Convert.ToInt32(QA);
                SqlParameter HodQA = new SqlParameter("@Head_QA", SqlDbType.Int);
                cmd.Parameters.Add(HodQA).Value = Convert.ToInt32(Head_QA);
                SqlParameter QAdmin = new SqlParameter("@Qadmin", SqlDbType.Int);
                cmd.Parameters.Add(QAdmin).Value = Convert.ToInt32(QmsAdmin);
                SqlParameter Roel1 = new SqlParameter("@Role", SqlDbType.VarChar);
                cmd.Parameters.Add(Roel1).Value = Role;
                SqlParameter vaCCNID = new SqlParameter("@CCN_ID", SqlDbType.VarChar);
                cmd.Parameters.Add(vaCCNID).Value = CCN_ID;
                SqlParameter parmRoleIdDb = new SqlParameter("@RoleIdCht", SqlDbType.Int);
                cmd.Parameters.Add(parmRoleIdDb).Value = RoleIDCharts;
                SqlParameter parmDeptIdDb = new SqlParameter("@DeptidCht", SqlDbType.Int);
                cmd.Parameters.Add(parmDeptIdDb).Value = DeptidDb;
                SqlParameter parmFromDate = new SqlParameter("@FromDateCht", SqlDbType.VarChar);
                cmd.Parameters.Add(parmFromDate).Value = FromDateDb;
                SqlParameter parmToDate = new SqlParameter("@ToDateCht", SqlDbType.VarChar);
                cmd.Parameters.Add(parmToDate).Value = ToDateDb;
                SqlParameter parmQualityEvent = new SqlParameter("@QualityEventCht", SqlDbType.VarChar);
                cmd.Parameters.Add(parmQualityEvent).Value = QualityEvent;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetAssignedImpactPointDal(int DeptID, int BlockID)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetAssignedImpactPoints";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = DeptID;
                SqlParameter paramDisplayStart = new SqlParameter("@BlockID", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = BlockID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet Getdashboarddal(int Empid)
        {
            try
            {
                string query = "QMS.AizantIT_SP_DashBoard";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = Empid;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
     public DataSet GetdashboardCCN(int Empid)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_DashBoardCCN]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = Empid;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    
        #region NoteFile
        public DataSet GetNoteToFileListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, 
            int ShowType, int UserEmpID,int RoleIDCharts, int DeptidCht, string _FromDate, string _ToDate,
            string sSearch_NoteToFileNo, string sSearch_DeptName, string sSearch_ReferenceNumber, string sSearch_DocTitle, string sSearch_PreparedBy
            , string sSearch_DocCreateDate, string sSearch_CurrentStatus)
        {
            try
            {
                _FromDate = _FromDate == "" ? DateTime.Now.AddDays(-365).ToString("dd MMM yyyy") : _FromDate;
                _ToDate = _ToDate == "" ? DateTime.Now.ToString("dd MMM yyyy") : _ToDate;
                string query = "[QMS].[AizantIT_SP_GetNoteToFile]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter parmRole = new SqlParameter("@ShowType", SqlDbType.Int);
                cmd.Parameters.Add(parmRole).Value = ShowType;
                SqlParameter parmUserid = new SqlParameter("@UserEmpID", SqlDbType.Int);
                cmd.Parameters.Add(parmUserid).Value = UserEmpID;
                SqlParameter parmRoleIdDb= new SqlParameter("@RoleIdCht", SqlDbType.Int);
                cmd.Parameters.Add(parmRoleIdDb).Value = RoleIDCharts;
                SqlParameter parmDeptIdDb = new SqlParameter("@DeptidCht", SqlDbType.Int);
                cmd.Parameters.Add(parmDeptIdDb).Value = DeptidCht;
                SqlParameter parmFromDate = new SqlParameter("@FromDateCht", SqlDbType.DateTime);
                cmd.Parameters.Add(parmFromDate).Value = _FromDate;
                SqlParameter parmToDate = new SqlParameter("@ToDateCht", SqlDbType.DateTime);
                cmd.Parameters.Add(parmToDate).Value = _ToDate;
                SqlParameter paramsSearch_NoteToFileNo = new SqlParameter("@sSearch_NoteToFileNo", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_NoteToFileNo).Value = sSearch_NoteToFileNo;
                SqlParameter paramsSearch_DeptName = new SqlParameter("@sSearch_DeptName", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_DeptName).Value = sSearch_DeptName;
                SqlParameter paramsSearch_ReferenceNumber = new SqlParameter("@sSearch_ReferenceNumber", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_ReferenceNumber).Value = sSearch_ReferenceNumber;
                SqlParameter paramsSearch_DocTitle = new SqlParameter("@sSearch_DocTitle", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_DocTitle).Value = sSearch_DocTitle;
                SqlParameter paramsSearch_PreparedBy = new SqlParameter("@sSearch_PreparedBy", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_PreparedBy).Value = sSearch_PreparedBy;
                SqlParameter paramsSearch_DocCreateDate = new SqlParameter("@sSearch_DocCreateDate", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_DocCreateDate).Value = sSearch_DocCreateDate;
                SqlParameter paramsSearch_CurrentStatus = new SqlParameter("@sSearch_CurrentStatus", SqlDbType.VarChar);
                cmd.Parameters.Add(paramsSearch_CurrentStatus).Value = sSearch_CurrentStatus;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable NoteToFileInsertDal(NoteFileBO NTFBO)
        {
            try
            {
                string query = "QMS.[AizantIT_SP_NoteToFileInsert]";
                string createDate = Convert.ToDateTime(NTFBO.DocCreateDate).ToString("yyyy/MM/dd");
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter deptid = new SqlParameter("@Department", SqlDbType.Int);
                cmd.Parameters.Add(deptid).Value = NTFBO.DeptID;
                SqlParameter DocCreateDate = new SqlParameter("@createDate", SqlDbType.VarChar);
                cmd.Parameters.Add(DocCreateDate).Value = createDate;
                SqlParameter DocumentTypeid = new SqlParameter("@DocumentType", SqlDbType.Int);
                cmd.Parameters.Add(DocumentTypeid).Value = NTFBO.DocumentTypeID;
                if (NTFBO.DocumentTypeID == 0)
                {
                    SqlParameter DocTilt = new SqlParameter("@DocTitle", SqlDbType.VarChar);
                    cmd.Parameters.Add(DocTilt).Value = NTFBO.DocTitle.Trim(); 
                    SqlParameter version = new SqlParameter("@ReferencedNumberOthers", SqlDbType.VarChar);
                    cmd.Parameters.Add(version).Value = NTFBO.versionID.Trim();
                }
                //DocumentType not Others
                else
                {
                    SqlParameter version = new SqlParameter("@versionID", SqlDbType.Int);
                    cmd.Parameters.Add(version).Value = NTFBO.versionID;
                }
                SqlParameter NTfNo = new SqlParameter("@NoteFileNo", SqlDbType.VarChar);
                cmd.Parameters.Add(NTfNo).Value = NTFBO.NoteToFileNo;
                SqlParameter NTF_DES = new SqlParameter("@Description", SqlDbType.VarChar);
                cmd.Parameters.Add(NTF_DES).Value = NTFBO.NTF_Description.Trim();
                SqlParameter Prepardby = new SqlParameter("@PreparedBy", SqlDbType.VarChar);
                cmd.Parameters.Add(Prepardby).Value = NTFBO.PreparedBy;
                SqlParameter AssignHod = new SqlParameter("@HODEMP", SqlDbType.VarChar);
                cmd.Parameters.Add(AssignHod).Value = NTFBO.HOD_EmpID;
                
                SqlParameter InitiatorComment = new SqlParameter("@comments", SqlDbType.VarChar);
                cmd.Parameters.Add(InitiatorComment).Value = NTFBO.InitiatorComments.Trim();
                //con.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                //return i;
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateNoteToFileDal(NoteFileBO NTFBO)
        {
            try
            {
                string query = "QMS.AIzantIT_SP_UpdateNoteToFile";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter NTF_id = new SqlParameter("@NTF_ID", SqlDbType.Int);
                cmd.Parameters.Add(NTF_id).Value = NTFBO.NF_ID;
                SqlParameter Documenttypeid = new SqlParameter("@documentTypeID", SqlDbType.Int);
                cmd.Parameters.Add(Documenttypeid).Value = NTFBO.DocumentTypeID;
                //SqlParameter reference = new SqlParameter("@Referencenumber", SqlDbType.VarChar);
                //cmd.Parameters.Add(reference).Value = NTFBO.ReferenceNumber;
               
                if (NTFBO.DocumentTypeID == 0)
                {
                    SqlParameter DocTitle = new SqlParameter("@docTitle", SqlDbType.VarChar);
                    cmd.Parameters.Add(DocTitle).Value = NTFBO.DocTitle.Trim();
                    SqlParameter version = new SqlParameter("@ReferencedNumberOthres", SqlDbType.VarChar);
                    cmd.Parameters.Add(version).Value = NTFBO.versionID.Trim();
                }
                else
                {
                    SqlParameter version = new SqlParameter("@versionID", SqlDbType.Int);
                    cmd.Parameters.Add(version).Value = NTFBO.versionID;
                }
                SqlParameter description = new SqlParameter("@Description", SqlDbType.VarChar);
                cmd.Parameters.Add(description).Value = NTFBO.NTF_Description.Trim();
                SqlParameter assignhod = new SqlParameter("@Assign_Hod", SqlDbType.Int);
                cmd.Parameters.Add(assignhod).Value = NTFBO.HOD_EmpID;
                SqlParameter PramPreparedBY = new SqlParameter("@preparedBy", SqlDbType.VarChar);
                cmd.Parameters.Add(PramPreparedBY).Value = NTFBO.PreparedBy;
                SqlParameter PramPrecomments = new SqlParameter("@comments", SqlDbType.VarChar);
                cmd.Parameters.Add(PramPrecomments).Value = NTFBO.InitiatorComments.Trim();
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Note file Revert 
        public int RevertHODDAL(NoteFileBO NTFBO)
        {
            try
            {
                string query = "QMS.[AizantIT_SP_NTF_Revert_RejectHOD]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter NTF_id = new SqlParameter("@NTF_ID", SqlDbType.Int);
                cmd.Parameters.Add(NTF_id).Value = NTFBO.NF_ID;
                SqlParameter RevertBY = new SqlParameter("@RevertedBY", SqlDbType.Int);
                cmd.Parameters.Add(RevertBY).Value = NTFBO.PreparedBy;//Reverted by
                if (NTFBO.Status == "HOD")
                {
                    SqlParameter Hod_comments = new SqlParameter("@HodComment", SqlDbType.VarChar);
                    cmd.Parameters.Add(Hod_comments).Value = NTFBO.HODComments.Trim();
                }
                if (NTFBO.Status == "QA" || NTFBO.Status == "HQA")
                {
                    SqlParameter Hod_comments = new SqlParameter("@HodComment", SqlDbType.VarChar);
                    cmd.Parameters.Add(Hod_comments).Value = NTFBO.QAComments.Trim();
                    SqlParameter SelectEMP = new SqlParameter("@SelectEmployee", SqlDbType.Int);
                    cmd.Parameters.Add(SelectEMP).Value = NTFBO.SelectEmployee;
                }
                if (NTFBO.Status == "R")
                {
                    int CurrentStatus = 0;
                    //QA
                    if (!string.IsNullOrEmpty(NTFBO.QAComments))
                    {
                        SqlParameter Hod_comments = new SqlParameter("@HodComment", SqlDbType.VarChar);
                        cmd.Parameters.Add(Hod_comments).Value = NTFBO.QAComments.Trim();
                    }
                    //HOD
                    if (!string.IsNullOrEmpty(NTFBO.HODComments))
                    {
                        SqlParameter Hod_comments1 = new SqlParameter("@HodComment", SqlDbType.VarChar);
                        cmd.Parameters.Add(Hod_comments1).Value = NTFBO.HODComments.Trim();
                    }
                    SqlParameter RoleIdPassed = new SqlParameter("@RoleIdPassed", SqlDbType.Int);
                    cmd.Parameters.Add(RoleIdPassed).Value = NTFBO.RoleID;

                    if (NTFBO.CurrentStatus == "1" || NTFBO.CurrentStatus == "2")
                    {
                        CurrentStatus = 5;
                    }
                    else if (NTFBO.CurrentStatus == "6" || NTFBO.CurrentStatus == "19")
                    {
                        CurrentStatus = 8;
                    }
                    else if (NTFBO.CurrentStatus == "9" || NTFBO.CurrentStatus == "20")
                    {
                        CurrentStatus = 15;
                    }
                    SqlParameter CurrentStatusRejct = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                    cmd.Parameters.Add(CurrentStatusRejct).Value = CurrentStatus;
                }
                    SqlParameter status = new SqlParameter("@Status", SqlDbType.VarChar);
                    cmd.Parameters.Add(status).Value = NTFBO.Status.Trim();
                    con.Open();
                    int i = cmd.ExecuteNonQuery();
                    con.Close();
                    return i;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int NTFSubmittedToQADal(NoteFileBO NTFBO)
        {
            try
            {
                string query = "QMS.AizantIT_SP_NTFSubmitToQA";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter NTF_id = new SqlParameter("@NTF_ID", SqlDbType.Int);
                cmd.Parameters.Add(NTF_id).Value = NTFBO.NF_ID;
                if (NTFBO.Status == "QA" ||NTFBO.Status== "HODUpdate")
                {
                    SqlParameter Hod_comments = new SqlParameter("@HodComments", SqlDbType.VarChar);
                    cmd.Parameters.Add(Hod_comments).Value = NTFBO.HODComments.Trim();
                    SqlParameter QA_id = new SqlParameter("@QA", SqlDbType.Int);
                    cmd.Parameters.Add(QA_id).Value = NTFBO.QA_EmpId;
                    SqlParameter CapaReqired = new SqlParameter("@IsCapaRequired", SqlDbType.VarChar);
                    cmd.Parameters.Add(CapaReqired).Value = NTFBO.IsCAPARequired;
                    SqlParameter NoteToFile_Id = new SqlParameter("@NTFNmber", SqlDbType.VarChar);
                    cmd.Parameters.Add(NoteToFile_Id).Value = NTFBO.NoteToFileNo;

                }

                if (NTFBO.Status == "HQA"|| NTFBO.Status=="QAUpdate")
                {
                    SqlParameter QA_comments = new SqlParameter("@HodComments", SqlDbType.VarChar);
                    cmd.Parameters.Add(QA_comments).Value = NTFBO.QAComments.Trim(); ;
                    SqlParameter HQA_id = new SqlParameter("@QA", SqlDbType.Int);
                    cmd.Parameters.Add(HQA_id).Value = NTFBO.Head_QA;
                }
                if (NTFBO.Status == "Capa")
                {
                    SqlParameter NTF_NO = new SqlParameter("@NTFNmber", SqlDbType.VarChar);
                    cmd.Parameters.Add(NTF_NO).Value = NTFBO.NoteToFileNo;
                    SqlParameter QA_comments = new SqlParameter("@HodComments", SqlDbType.VarChar);
                    cmd.Parameters.Add(QA_comments).Value = NTFBO.QAComments.Trim(); ;
                    SqlParameter capaRequiredorNot = new SqlParameter("@IsCapaRequired", SqlDbType.VarChar);
                    cmd.Parameters.Add(capaRequiredorNot).Value = NTFBO.capaRequiredorNot;
                }
                    SqlParameter RevertBY = new SqlParameter("@createdBy", SqlDbType.Int);
                cmd.Parameters.Add(RevertBY).Value =Convert.ToInt32( NTFBO.PreparedBy);//created by
                SqlParameter Status1 = new SqlParameter("@Status", SqlDbType.VarChar);
                cmd.Parameters.Add(Status1).Value =(NTFBO.Status);



                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetEventsCAPAListDAL(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch,int EventsBaseID, int QualityEventTypeID,int AssessmentID)
        {
            try
            {
            string query = "[QMS].[AizantIT_SP_EventsCAPAList]";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandTimeout = 300;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
            SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
            SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
            cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
            SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
            SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSearchString).Value = sSearch;
            SqlParameter paramNTFID = new SqlParameter("@EventsBaseID", SqlDbType.Int);
            cmd.Parameters.Add(paramNTFID).Value = EventsBaseID;
            SqlParameter paramAssessmentID = new SqlParameter("@AssessmentID", SqlDbType.Int);
            cmd.Parameters.Add(paramAssessmentID).Value = AssessmentID;
                SqlParameter paramQualityEventTypeID = new SqlParameter("@QualityEventTypeID", SqlDbType.Int);
            cmd.Parameters.Add(paramQualityEventTypeID).Value = QualityEventTypeID;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            da.Fill(dt);
            return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Electronic sign verification
        public void ElectronicSignVerificationDb(NoteFileBO NTFBO, out int SignVerified)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UMS.[AizantIT_SP_ElectronicSign_Verify]", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = NTFBO.EmpID;
                SqlParameter SPPassword = new SqlParameter("@Password", SqlDbType.VarChar);
                cmd.Parameters.Add(SPPassword).Value = NTFBO.Esign;
                SqlParameter Verify = cmd.Parameters.Add("@SignVerified", SqlDbType.Int);
                Verify.Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                SignVerified = Convert.ToInt32(cmd.Parameters["@SignVerified"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        #endregion


        #region Errata

        public DataSet GetErrataListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch,
            string Role, int EmployeeID, int RoleIDCharts, int DeptidDb, string FromDateDb, string ToDateDb, 
            string sSearch_DeptName, string sSearch_ErrataFileNo, string sSearch_DocumentNo, string sSearch_DocTitle, string sSearch_EmpName,
            string sSearch_Initiator_SubmitedDate, string sSearch_SectionNo, string sSearch_Status)
        {
            try
            {
                string query = "[QMS].AizantIt_SP_GetErrataList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter parmRole = new SqlParameter("@Role", SqlDbType.VarChar);
                cmd.Parameters.Add(parmRole).Value = Role;
                SqlParameter parmUserid = new SqlParameter("@userid", SqlDbType.Int);
                cmd.Parameters.Add(parmUserid).Value = EmployeeID;
                SqlParameter parmRoleIdDb = new SqlParameter("@RoleIdCht", SqlDbType.Int);
                cmd.Parameters.Add(parmRoleIdDb).Value = RoleIDCharts;
                SqlParameter parmDeptIdDb = new SqlParameter("@DeptidCht", SqlDbType.Int);
                cmd.Parameters.Add(parmDeptIdDb).Value = DeptidDb;
                SqlParameter parmFromDate = new SqlParameter("@FromDateCht", SqlDbType.VarChar);
                cmd.Parameters.Add(parmFromDate).Value = FromDateDb;
                SqlParameter parmToDate = new SqlParameter("@ToDateCht", SqlDbType.VarChar);
                cmd.Parameters.Add(parmToDate).Value = ToDateDb;

                //commented on 03-02-2020
                //SqlParameter parmER_ID = new SqlParameter("@sSearch_ER_ID", SqlDbType.Int);
                //cmd.Parameters.Add(parmER_ID).Value = sSearch_ER_ID;
                SqlParameter parmDepID = new SqlParameter("@sSearch_DeptName", SqlDbType.VarChar);
                cmd.Parameters.Add(parmDepID).Value = sSearch_DeptName;
                SqlParameter parmERFileNo = new SqlParameter("@sSearch_ErrataFileNo", SqlDbType.VarChar);
                cmd.Parameters.Add(parmERFileNo).Value = sSearch_ErrataFileNo;
                SqlParameter parmDocNo = new SqlParameter("@sSearch_DocumentNo", SqlDbType.VarChar);
                cmd.Parameters.Add(parmDocNo).Value = sSearch_DocumentNo;
                SqlParameter parmDocTitle = new SqlParameter("@sSearch_DocTitle", SqlDbType.VarChar);
                cmd.Parameters.Add(parmDocTitle).Value = sSearch_DocTitle;
                SqlParameter parmEmpName = new SqlParameter("@sSearch_EmpName", SqlDbType.VarChar);
                cmd.Parameters.Add(parmEmpName).Value = sSearch_EmpName;
                SqlParameter parmSubmitedDate = new SqlParameter("@sSearch_Initiator_SubmitedDate", SqlDbType.VarChar);
                cmd.Parameters.Add(parmSubmitedDate).Value = sSearch_Initiator_SubmitedDate;
                SqlParameter parmSectionNo = new SqlParameter("@sSearch_SectionNo", SqlDbType.VarChar);
                cmd.Parameters.Add(parmSectionNo).Value = sSearch_SectionNo;
                SqlParameter parmsStatus = new SqlParameter("@sSearch_Status", SqlDbType.VarChar);
                cmd.Parameters.Add(parmsStatus).Value = sSearch_Status;
                //end commented on 03-02-2020

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable ErrataFileInsertDal(ErrataBO ERTABO,string Status)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_ErrataFileInsert]";
                string createDate = Convert.ToDateTime(ERTABO.Initiator_SubmitedDate).ToString("yyyy/MM/dd");
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter deptid = new SqlParameter("@Department", SqlDbType.Int);
                cmd.Parameters.Add(deptid).Value = ERTABO.DeptID;
                SqlParameter DocCreateDate = new SqlParameter("@createDate", SqlDbType.VarChar);
                cmd.Parameters.Add(DocCreateDate).Value = createDate;

                SqlParameter DocumentType = new SqlParameter("@DocumentType", SqlDbType.VarChar);
                cmd.Parameters.Add(DocumentType).Value = ERTABO.DocumentType;
                
                if (ERTABO.DocumentType == 0)
                {
                    SqlParameter DocName = new SqlParameter("@Document_Name", SqlDbType.VarChar);
                    cmd.Parameters.Add(DocName).Value = ERTABO.DocTitle.Trim();
                    //Enter Reference number
                    SqlParameter version = new SqlParameter("@ReferenceNumber", SqlDbType.VarChar);
                    cmd.Parameters.Add(version).Value = ERTABO.versionID.Trim();
                }
                else
                {
                    SqlParameter version = new SqlParameter("@versionID", SqlDbType.Int);
                    cmd.Parameters.Add(version).Value = ERTABO.versionID;
                }
                SqlParameter ER_fileNumber = new SqlParameter("@ER_FileNumber", SqlDbType.VarChar);
                cmd.Parameters.Add(ER_fileNumber).Value = ERTABO.ErrataFileNo.Trim();
                SqlParameter NTF_DES = new SqlParameter("@Description", SqlDbType.VarChar);
                cmd.Parameters.Add(NTF_DES).Value = ERTABO.ProposedCorrectionDoc_Desc.Trim();
                SqlParameter Prepardby = new SqlParameter("@PreparedBy", SqlDbType.Int);
                cmd.Parameters.Add(Prepardby).Value = ERTABO.Initiator_EmpID;
                SqlParameter AssignHod = new SqlParameter("@HODEMP", SqlDbType.Int);
                cmd.Parameters.Add(AssignHod).Value = ERTABO.HOD_EmpID;
                
                SqlParameter Section_No = new SqlParameter("@Section", SqlDbType.VarChar);
                cmd.Parameters.Add(Section_No).Value = ERTABO.SectionNo.Trim();
                SqlParameter InitiatorComment = new SqlParameter("@comments", SqlDbType.VarChar);
                cmd.Parameters.Add(InitiatorComment).Value = ERTABO.initiator_Comments.Trim();
                SqlParameter parmER_ID = new SqlParameter("@Erata_ID", SqlDbType.Int);
                cmd.Parameters.Add(parmER_ID).Value = ERTABO.ER_ID;
                SqlParameter parmTypeCorec = new SqlParameter("@TypeOfCorrection", SqlDbType.Int);
                cmd.Parameters.Add(parmTypeCorec).Value = ERTABO.TypeofCorrection;
                SqlParameter partmstatus = new SqlParameter("@Status", SqlDbType.VarChar);
                cmd.Parameters.Add(partmstatus).Value = Status;
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
         }
        public void ElectronicSignVerificationDb(ErrataBO ERATA, out int SignVerified)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UMS.[AizantIT_SP_ElectronicSign_Verify]", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter SPEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SPEmpID).Value = ERATA.EmpID;
                SqlParameter SPPassword = new SqlParameter("@Password", SqlDbType.VarChar);
                cmd.Parameters.Add(SPPassword).Value = ERATA.Esign;
                SqlParameter Verify = cmd.Parameters.Add("@SignVerified", SqlDbType.Int);
                Verify.Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                SignVerified = Convert.ToInt32(cmd.Parameters["@SignVerified"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int ErrataRevertDal(ErrataBO ERATA)
        {
            try
            {
                string query = "[QMS].AizantIT_SP_Errata_RevertAll";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter NTF_id = new SqlParameter("@ER_ID", SqlDbType.Int);
                cmd.Parameters.Add(NTF_id).Value = ERATA.ER_ID;
                SqlParameter RevertBY = new SqlParameter("@RevertedBY", SqlDbType.Int);
                cmd.Parameters.Add(RevertBY).Value = ERATA.EmpID;//Reverted by
                if (ERATA.Status == "HOD" )
                {
                    SqlParameter Hod_comments = new SqlParameter("@HodComment", SqlDbType.VarChar);
                    cmd.Parameters.Add(Hod_comments).Value = ERATA.HOD_Comments.Trim();
                }
                if (ERATA.Status == "HQA")
                {
                    SqlParameter Hod_comments = new SqlParameter("@HodComment", SqlDbType.VarChar);
                    cmd.Parameters.Add(Hod_comments).Value = ERATA.HQA_Comments.Trim();
                }
                if (ERATA.Status == "R")
                {
                    if (ERATA.CurrentStatus == 4|| ERATA.CurrentStatus==13)
                    {//QA
                        SqlParameter Hod_comments = new SqlParameter("@HodComment", SqlDbType.VarChar);
                        cmd.Parameters.Add(Hod_comments).Value = ERATA.HQA_Comments.Trim();//HeadQA Comments
                    }
                    if (ERATA.CurrentStatus == 1|| ERATA.CurrentStatus == 2)
                    {//HOD
                        SqlParameter Hod_comments = new SqlParameter("@HodComment", SqlDbType.VarChar);
                        cmd.Parameters.Add(Hod_comments).Value = ERATA.HOD_Comments.Trim();
                    }
                    SqlParameter RoleIDPass = new SqlParameter("@RoleIDPass", SqlDbType.Int);
                    cmd.Parameters.Add(RoleIDPass).Value = ERATA.RoleId;

                }
                SqlParameter RevertEmp = new SqlParameter("@SelectEmployee", SqlDbType.VarChar);
                cmd.Parameters.Add(RevertEmp).Value = ERATA.Select_RevertEmp;
                SqlParameter status = new SqlParameter("@Status", SqlDbType.VarChar);
                cmd.Parameters.Add(status).Value = ERATA.Status;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int ErrataSubmittedToQADal(ErrataBO ERTABO,string Status, DataTable dtImpactlst)
        {
            try
            {
                string query = "QMS.AizantIT_SP_ErrataSubmitToQA";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parmEr_ID = new SqlParameter("@ER_ID", SqlDbType.Int);
                cmd.Parameters.Add(parmEr_ID).Value = ERTABO.ER_ID;
                //Hod Submitted to QA
                if (Status == "Submit" ||Status== "Update")
                {
                    //SqlParameter tempTypeOFCorrection = new SqlParameter("@TypeOfCorrection", SqlDbType.Int);
                    //cmd.Parameters.Add(tempTypeOFCorrection).Value = ERTABO.TypeofCorrection;                  
                    SqlParameter parmJustfication = new SqlParameter("@Justfication", SqlDbType.VarChar);
                    cmd.Parameters.Add(parmJustfication).Value = ERTABO.JustificationDesc.Trim();
                    SqlParameter parmComment = new SqlParameter("@Comments", SqlDbType.VarChar);
                    cmd.Parameters.Add(parmComment).Value = ERTABO.HOD_Comments.Trim();

                    cmd.Parameters.AddWithValue("@Impactpointslst", dtImpactlst);
                    SqlParameter AssignToQA = new SqlParameter("@AssignToHQA", SqlDbType.Int);
                    cmd.Parameters.Add(AssignToQA).Value = ERTABO.HeadQA_EmpID;
                }
                //QA submitted to Head QA
                if(Status== "QASubmit"){
                    SqlParameter parmMaster = new SqlParameter("@ISMaster", SqlDbType.Bit);
                    cmd.Parameters.Add(parmMaster).Value = ERTABO.MasterDocument;
                    SqlParameter parmSoftCopy = new SqlParameter("@ISSoftCopy", SqlDbType.Bit);
                    cmd.Parameters.Add(parmSoftCopy).Value = ERTABO.SoftCopy;
                    SqlParameter parmOtherCheck = new SqlParameter("@ISOthers", SqlDbType.Bit);
                    cmd.Parameters.Add(parmOtherCheck).Value = ERTABO.Other_DocCheck;
                    SqlParameter parmOtherDocDescription = new SqlParameter("@OthersDocDescription", SqlDbType.VarChar);
                    cmd.Parameters.Add(parmOtherDocDescription).Value = ERTABO.OtherDocumnets.Trim();
                    SqlParameter parmComment = new SqlParameter("@Comments", SqlDbType.VarChar);
                    cmd.Parameters.Add(parmComment).Value = ERTABO.QA_Comments.Trim();
                  
                 
                }
                //QA submitted to Head QA
                if (Status== "HQASubmit")
                {
                    SqlParameter EvaluationDesc = new SqlParameter("@EvaluationDesc", SqlDbType.VarChar);
                    cmd.Parameters.Add(EvaluationDesc).Value = ERTABO.Evaluation_Desc.Trim();
                    SqlParameter AssignToHQA = new SqlParameter("@AssignToQA", SqlDbType.Int);
                    cmd.Parameters.Add(AssignToHQA).Value = ERTABO.QA_EmpID;
                    SqlParameter parmComment = new SqlParameter("@Comments", SqlDbType.VarChar);
                    cmd.Parameters.Add(parmComment).Value = ERTABO.HQA_Comments.Trim();
                }
                if (Status == "InitiatorComplete")
                {
                    SqlParameter AssignToHQA = new SqlParameter("@AssignToQA", SqlDbType.Int);
                    cmd.Parameters.Add(AssignToHQA).Value = ERTABO.QA_EmpID;
                    SqlParameter parmComment = new SqlParameter("@Comments", SqlDbType.VarChar);
                    cmd.Parameters.Add(parmComment).Value = ERTABO.initiator_Comments.Trim();
                }
                SqlParameter parmStatus = new SqlParameter("@Status", SqlDbType.VarChar);
                cmd.Parameters.Add(parmStatus).Value = Status;

                SqlParameter parmPreparedBy = new SqlParameter("@PreparedBy", SqlDbType.Int);
                cmd.Parameters.Add(parmPreparedBy).Value = ERTABO.EmpID;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region CAPA


        public DataSet GetCapaHistoryDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int CapaId)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_GetHistoryTransactions]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramCapaID = new SqlParameter("@CapaID", SqlDbType.Int);
                cmd.Parameters.Add(paramCapaID).Value = CapaId;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet Get_TransactionCapa(int capaid, string TypeOfTransaction)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetCapaTranasction";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayStart = new SqlParameter("@CAPAID", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = capaid;
                SqlParameter type = new SqlParameter("@type", SqlDbType.VarChar);
                cmd.Parameters.Add(type).Value = TypeOfTransaction;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                    da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet Get_TransactionCapaInsertUpdateAction(CapaModel objcapabo, out int ResultOputparm)
        {
            try
            {
                string query = "QMS.[AizantIT_SP_GetCapaTranasctionInsertUpdateReject]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayStart = new SqlParameter("@CAPAID", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value =objcapabo.CAPAID;
                SqlParameter type = new SqlParameter("@type", SqlDbType.VarChar);
                cmd.Parameters.Add(type).Value =objcapabo.CapaStatusSP;
                SqlParameter EmployeeID = new SqlParameter("@EmpID", SqlDbType.VarChar);
                cmd.Parameters.Add(EmployeeID).Value = objcapabo.EmpID;
                SqlParameter HeadQA = new SqlParameter("@AssignedHeadQA", SqlDbType.Int);
                cmd.Parameters.Add(HeadQA).Value =objcapabo.HeadQa_EmpID;
                SqlParameter Commentsreverted = new SqlParameter("@Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(Commentsreverted).Value =objcapabo.comments;
                SqlParameter CuurentStatus = new SqlParameter("@currentStatus", SqlDbType.VarChar);
                cmd.Parameters.Add(CuurentStatus).Value =objcapabo.CurrentStatus;
                SqlParameter OutPutParmResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                OutPutParmResult.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
               
                if (objcapabo.EmpID != 0)
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    ResultOputparm = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                    con.Close();
                }
                else
                {
                    da.Fill(dt);
                    ResultOputparm = Convert.ToInt32(cmd.Parameters["@Result"].Value);

                }

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet CAPAVerificationApprovalAndRevertDAL(MainListCapaBO objCapaModel, out int Result)
        {
            try
            {
                string query = "QMS.[AizantIT_SP_CapaVerificationApproveOrRevertInHQA]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayStart = new SqlParameter("@CAPAID", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = objCapaModel.CAPAID;
                SqlParameter EmployeeID = new SqlParameter("@EmpID", SqlDbType.VarChar);
                cmd.Parameters.Add(EmployeeID).Value = objCapaModel.EMPId;
                SqlParameter Commentsreverted = new SqlParameter("@Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(Commentsreverted).Value = objCapaModel.VerificationComments.Trim();
                SqlParameter ImplementorDate = new SqlParameter("@CurrentStatus", SqlDbType.VarChar);
                cmd.Parameters.Add(ImplementorDate).Value = objCapaModel.CurrentStatus;
                SqlParameter VerificationID = new SqlParameter("@EVFID", SqlDbType.VarChar);
                cmd.Parameters.Add(VerificationID).Value = objCapaModel.VerificationID;
                SqlParameter IsImplemented = new SqlParameter("@IsChecked", SqlDbType.Int);
                cmd.Parameters.Add(IsImplemented).Value = objCapaModel.ISCheckedFinalorNot;

                SqlParameter GetReuslt = cmd.Parameters.Add("@Result", SqlDbType.Int);
                GetReuslt.Direction = ParameterDirection.Output;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                con.Open();
                cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCAPAVerificationDAL(MainListCapaBO objCapaModel,out int Result)
        {
            try
            {
                string query = "QMS.AizantIT_SP_CapaVerificationUpload";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayStart = new SqlParameter("@CAPAID", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = objCapaModel.CAPAID;
                SqlParameter EmployeeID = new SqlParameter("@EmpID", SqlDbType.VarChar);
                cmd.Parameters.Add(EmployeeID).Value = objCapaModel.EMPId;
                SqlParameter Commentsreverted = new SqlParameter("@Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(Commentsreverted).Value = objCapaModel.VerificationComments.Trim();
                SqlParameter ImplementorDate = new SqlParameter("@ImplementorDate", SqlDbType.VarChar);
                cmd.Parameters.Add(ImplementorDate).Value = objCapaModel.ImplementorDateVerificationCAPA;
                SqlParameter AssignedHQA = new SqlParameter("@AssignedHQA", SqlDbType.Int);
                cmd.Parameters.Add(AssignedHQA).Value = objCapaModel.HeadQAEmpID;
                SqlParameter CurrenStatus = new SqlParameter("@CurrenStatus", SqlDbType.Int);
                cmd.Parameters.Add(CurrenStatus).Value = objCapaModel.CurrentStatus;
                SqlParameter IsChecked = new SqlParameter("@IsChecked", SqlDbType.Int);
                cmd.Parameters.Add(IsChecked).Value = objCapaModel.ISCheckedFinalorNot;
                SqlParameter VerificationID = new SqlParameter("@EVerificationID", SqlDbType.Int);
                cmd.Parameters.Add(VerificationID).Value = objCapaModel.VerificationID;
                SqlParameter GUIDVerificationID = new SqlParameter("@GUIDVerificationID", SqlDbType.VarChar);
                cmd.Parameters.Add(GUIDVerificationID).Value = objCapaModel.GUID;
                SqlParameter GetReuslt = cmd.Parameters.Add("@Result", SqlDbType.Int);
                GetReuslt.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                con.Open();
                cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCAPAHQAApprovalDAL(MainListCapaBO objCapaModel,out int Result)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_CapaHeadQAApproval]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayStart = new SqlParameter("@CAPAID", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = objCapaModel.CAPAID;
                SqlParameter EmployeeID = new SqlParameter("@EmpID", SqlDbType.VarChar);
                cmd.Parameters.Add(EmployeeID).Value = objCapaModel.EMPId;
                SqlParameter Commentsreverted = new SqlParameter("@Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(Commentsreverted).Value = objCapaModel.HQAApprovalComments.Trim();
                SqlParameter GetResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                GetResult.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                con.Open();
                cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCAPACloseDAL(MainListCapaBO objCapaModel, out int Result, out int AllCpapCompleted)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_CAPACloseSubmit]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayStart = new SqlParameter("@CAPAID", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = objCapaModel.CAPAID;
                SqlParameter EmployeeID = new SqlParameter("@EmpID", SqlDbType.VarChar);
                cmd.Parameters.Add(EmployeeID).Value = objCapaModel.EMPId;
                SqlParameter Commentsreverted = new SqlParameter("@Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(Commentsreverted).Value = objCapaModel.ClosureComments.Trim();
                SqlParameter ImplementorDate = new SqlParameter("@EffectiveDate", SqlDbType.DateTime);
                cmd.Parameters.Add(ImplementorDate).Value = objCapaModel.ClosureEffectiveDate;
                SqlParameter GetResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                GetResult.Direction = ParameterDirection.Output;
                SqlParameter GetAllCAPAsCompleted = cmd.Parameters.Add("@AllCAPACompleted", SqlDbType.Int);
                GetAllCAPAsCompleted.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                con.Open();
                cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                AllCpapCompleted = Convert.ToInt32(cmd.Parameters["@AllCAPACompleted"].Value);
                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable CapaInsertDal(CapaModel capaModel)
        {
            try
            {
                string query = "QMS.AizantIT_SP_CapaInsert";
                string capadate = Convert.ToDateTime(capaModel.CAPA_Date).ToString("yyyy/MM/dd");
                string targetdate12 = Convert.ToDateTime(capaModel.TargetDate).ToString("yyyy/MM/dd");
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter deptid = new SqlParameter("@Department", SqlDbType.Int);
                cmd.Parameters.Add(deptid).Value = capaModel.Department;
                //Responsible Person
                SqlParameter emplyeeID = new SqlParameter("@EmployeeName", SqlDbType.Int);
                cmd.Parameters.Add(emplyeeID).Value = capaModel.EmployeeName;
                SqlParameter Capanumber = new SqlParameter("@CAPA_Number", SqlDbType.VarChar);
                cmd.Parameters.Add(Capanumber).Value = capaModel.CAPA_Number;
                SqlParameter capdate = new SqlParameter("@CAPA_Date", SqlDbType.VarChar);
                cmd.Parameters.Add(capdate).Value = capadate;
                SqlParameter QualityEventID = new SqlParameter("@QualityEvent_TypeID", SqlDbType.Int);
                cmd.Parameters.Add(QualityEventID).Value = capaModel.QualityEvent_TypeID;
                SqlParameter QualityEventNumber = new SqlParameter("@QualityEvent_Number", SqlDbType.VarChar);
                cmd.Parameters.Add(QualityEventNumber).Value = capaModel.QualityEvent_Number.Trim();
                SqlParameter ActionPlanID = new SqlParameter("@PlanofAction", SqlDbType.Int);
                cmd.Parameters.Add(ActionPlanID).Value = capaModel.PlanofAction;
                SqlParameter targetdate = new SqlParameter("@TargetDate", SqlDbType.VarChar);
                cmd.Parameters.Add(targetdate).Value = targetdate12;
                SqlParameter Action_desc = new SqlParameter("@Capa_ActionDesc", SqlDbType.VarChar);
                cmd.Parameters.Add(Action_desc).Value = capaModel.Capa_ActionDesc.Trim();
                SqlParameter Createdby = new SqlParameter("@CreatedBy", SqlDbType.Int);
                cmd.Parameters.Add(Createdby).Value = Convert.ToInt32(capaModel.CreatedBy);
                SqlParameter QaEmployee = new SqlParameter("@QA_EmployeeId", SqlDbType.Int);
                cmd.Parameters.Add(QaEmployee).Value = Convert.ToInt32(capaModel.QA_EmpId);
                SqlParameter Comments = new SqlParameter("@comments", SqlDbType.VarChar);
                cmd.Parameters.Add(Comments).Value = capaModel.comments.Trim();
                //con.Open();
                //cmd.ExecuteNonQuery();
                //con.Close();
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetddlBindingCAPACreateDAL(int empid)
        {
            try
            {
                string query = "QMS.AizantIT_SP_ToBindDropdownInCAPA";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramEMpid = new SqlParameter("@EmpdID", SqlDbType.Int);
                cmd.Parameters.Add(paramEMpid).Value = empid;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable CapaReminderDal(CapaReminder capaModel, string Role, out int Result)
        {
            try
            {
                string query = "QMS.AizantIT_SP_CapaReminderInsert";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter emplyeeID = new SqlParameter("@HODEmployeeID", SqlDbType.Int);
                cmd.Parameters.Add(emplyeeID).Value = capaModel.EmployeeName;
                SqlParameter EmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(EmpID).Value = capaModel.EmpID;
                SqlParameter Capanumber = new SqlParameter("@CapaID", SqlDbType.VarChar);
                cmd.Parameters.Add(Capanumber).Value = capaModel.CAPAID;
                SqlParameter capdate = new SqlParameter("@RevisedTargetDate", SqlDbType.VarChar);
                cmd.Parameters.Add(capdate).Value = (capaModel.RevisedTargetDate);
                SqlParameter QAcomments = new SqlParameter("@QA_Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(QAcomments).Value = capaModel.QA_Comments.Trim();
                SqlParameter QualityEventNumber = new SqlParameter("@HOD_Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(QualityEventNumber).Value = capaModel.HOD_Comments.Trim();
                SqlParameter Roles = new SqlParameter("@Role", SqlDbType.VarChar);
                cmd.Parameters.Add(Roles).Value = Role;
                SqlParameter status = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                cmd.Parameters.Add(status).Value = Convert.ToInt32(capaModel.RemainderStatus);
                SqlParameter IsRevisedDate = new SqlParameter("@IsRevisedDate", SqlDbType.Bit);
                cmd.Parameters.Add(IsRevisedDate).Value = Convert.ToInt32(capaModel.IsRevisedDate);
                SqlParameter getResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                getResult.Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                con.Close();
                DataTable dt = new DataTable();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable CapaUpdateDal(CapaModel capaModel,out int ResultStatus )
        {
            try
            {
                string query = "QMS.AizantIT_SP_CapaUpdate";
                string capadate = Convert.ToDateTime(capaModel.CAPA_Date).ToString("yyyy/MM/dd");
                string targetdate12 = Convert.ToDateTime(capaModel.TargetDate).ToString("yyyy/MM/dd");
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter emplyeeID = new SqlParameter("@EmployeeName", SqlDbType.Int);
                cmd.Parameters.Add(emplyeeID).Value = capaModel.EmployeeName;
                SqlParameter Capanumber = new SqlParameter("@CAPA_Number", SqlDbType.VarChar);
                cmd.Parameters.Add(Capanumber).Value = capaModel.CAPA_Number;
                SqlParameter QualityEventID = new SqlParameter("@QualityEvent_TypeID", SqlDbType.Int);
                cmd.Parameters.Add(QualityEventID).Value = capaModel.QualityEvent_TypeID;
                SqlParameter QualityEventNumber = new SqlParameter("@QualityEvent_Number", SqlDbType.VarChar);
                cmd.Parameters.Add(QualityEventNumber).Value = capaModel.QualityEvent_Number.Trim();
                SqlParameter ActionPlanID = new SqlParameter("@PlanofAction", SqlDbType.Int);
                cmd.Parameters.Add(ActionPlanID).Value = capaModel.PlanofAction;
                SqlParameter targetdate = new SqlParameter("@TargetDate", SqlDbType.VarChar);
                cmd.Parameters.Add(targetdate).Value = targetdate12;
                SqlParameter Action_desc = new SqlParameter("@Capa_ActionDesc", SqlDbType.VarChar);
                cmd.Parameters.Add(Action_desc).Value = capaModel.Capa_ActionDesc.Trim();
                SqlParameter modifed = new SqlParameter("@ModifiedBy", SqlDbType.Int);
                cmd.Parameters.Add(modifed).Value = Convert.ToInt32(capaModel.Modified);
                SqlParameter QAEmployeeID = new SqlParameter("@QAEmployeeID", SqlDbType.Int);
                cmd.Parameters.Add(QAEmployeeID).Value = capaModel.QA_EmpId;
                SqlParameter Comments = new SqlParameter("@comments", SqlDbType.VarChar);
                cmd.Parameters.Add(Comments).Value = capaModel.comments.Trim();
                SqlParameter OutPutParmResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                OutPutParmResult.Direction = ParameterDirection.Output;
                
                con.Open();
                cmd.ExecuteNonQuery();
                ResultStatus = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                con.Close();
                DataTable dt = new DataTable();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable CapaEmpStatus(CapaModel capaModel,out int ResultStatus)
        {
            try
            {
                //Employee Update Accepted and Completed Status
                string query = "QMS.AizantIT_SP_CapaEmp_UpdateStatus";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter Capanumber = new SqlParameter("@CapaID", SqlDbType.Int);
                cmd.Parameters.Add(Capanumber).Value = capaModel.CAPAID;
                SqlParameter Action_desc = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                cmd.Parameters.Add(Action_desc).Value = Convert.ToInt32(capaModel.CurrentStatus);
                SqlParameter LoginEMpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(LoginEMpID).Value = Convert.ToInt32(capaModel.EmpID);
                SqlParameter RespPersonCommts = new SqlParameter("@CompleteComment", SqlDbType.VarChar);
                cmd.Parameters.Add(RespPersonCommts).Value = capaModel.RespPersonCommts == null ? "" : capaModel.RespPersonCommts.Trim();
                SqlParameter GetResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                GetResult.Direction = ParameterDirection.Output;
                con.Open();
                int count = cmd.ExecuteNonQuery();
                ResultStatus = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                con.Close();
                DataTable dt = new DataTable();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable CAPA_ApproveQA(CapaModel capaModel)
        {
            try
            {
                //Employee Update Accepted and Completed Status
                string query = "QMS.AizantIT_SP_CapaEmp_UpdateStatus";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter Capanumber = new SqlParameter("@CAPA_Number", SqlDbType.VarChar);
                cmd.Parameters.Add(Capanumber).Value = capaModel.CAPA_Number;
                SqlParameter Action_desc = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                cmd.Parameters.Add(Action_desc).Value = Convert.ToInt32(capaModel.CurrentStatus);
                con.Open();
                int count = cmd.ExecuteNonQuery();
                con.Close();
                DataTable dt = new DataTable();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetRemainderExitsorNot(int EmpID, int CapaID)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetRemainderExitsorNot";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramEmpID).Value = EmpID;
                SqlParameter paramCapaID = new SqlParameter("@CapaID", SqlDbType.Int);
                cmd.Parameters.Add(paramCapaID).Value = CapaID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable DalGetAutoGenaratedNumber(int Deptid,int QualityNo)
        {
            try
            {
                DataTable dt = new DataTable();
                string strQuery = "Select [QMS].[AizantIT_udf_GetQualityNo](@DeptID,@QualityNo)";
                SqlCommand cmd = new SqlCommand(strQuery, con);
                cmd.Parameters.AddWithValue("@DeptID", Deptid);
                cmd.Parameters.AddWithValue("@QualityNo", QualityNo);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DalCAPAAutoGenaratedNumber(int Deptid, out string CAPANumber, out int CAPA_MaxNo)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_GenerateCAPANumber]";
                
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                //cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parmDeptid = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(parmDeptid).Value = Deptid;
                SqlParameter SPCapaNumber = cmd.Parameters.Add("@CAPA_Number", SqlDbType.VarChar,size:50);
                SPCapaNumber.Direction = ParameterDirection.Output;
                SqlParameter SPCAPA_No = cmd.Parameters.Add("@CAPA_No", SqlDbType.Int);
                SPCAPA_No.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                CAPANumber = (cmd.Parameters["@CAPA_Number"].Value).ToString();
                CAPA_MaxNo = Convert.ToInt32(cmd.Parameters["@CAPA_No"].Value);
                con.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
        }
        public DataSet GetCapaFileUploadListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int CapaId)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_GetCapaFileUploadList]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramCapaID = new SqlParameter("@CapaID", SqlDbType.Int);
                cmd.Parameters.Add(paramCapaID).Value = CapaId;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet GetCapaCompltFileUploadDAL(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int CAPAID,string Status,string VerificationID=null)
        {
            try
            {
                string query = "QMS.[AizantIT_SP_GetCapaFileUploadList]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter vaCCnid = new SqlParameter("@CapaID", SqlDbType.Int);
                cmd.Parameters.Add(vaCCnid).Value = CAPAID;
                SqlParameter vaStatus = new SqlParameter("@Status", SqlDbType.VarChar);
                cmd.Parameters.Add(vaStatus).Value = Status;
                SqlParameter parVerificationID = new SqlParameter("@VerificationID", SqlDbType.VarChar);
                cmd.Parameters.Add(parVerificationID).Value = VerificationID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        public DataSet GetCapaAuditDetailsDAL(int ResponseID)
        {
            try
            {
                string query = "[QMS_Audit].[AizantIT_SP_GetAuditObservationDetailsBasedOnCAPAID]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parmCapaId = new SqlParameter("@ResponseID", SqlDbType.Int);
                cmd.Parameters.Add(parmCapaId).Value = ResponseID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Incident

        public DataSet GetIncidentListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch,int UserID,int ShowType, int RoleIDCharts, int DeptidDb, string FromDateDb, string ToDateDb,string IncidentFileterEvent,
           string sSearch_IRNumber, string sSearch_DeptName, string sSearch_DocumentNo, string sSearch_DocumentName, string sSearch_EmpName, string sSearch_CreatedDate, string sSearch_OccurrenceDate, string sSearch_IncidentDueDate, string sSearch_CurrentStatus)
        {
            try
            {
                string query = "[QMS].AizantIt_SP_GetIncidentList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter parmUserid = new SqlParameter("@userid", SqlDbType.Int);
                cmd.Parameters.Add(parmUserid).Value = UserID;
                SqlParameter parmShowType = new SqlParameter("@ShowType", SqlDbType.Int);
                cmd.Parameters.Add(parmShowType).Value = ShowType;
                SqlParameter parmRoleIdDb = new SqlParameter("@RoleIdCht", SqlDbType.Int);
                cmd.Parameters.Add(parmRoleIdDb).Value = RoleIDCharts;
                SqlParameter parmDeptIdDb = new SqlParameter("@DeptidCht", SqlDbType.Int);
                cmd.Parameters.Add(parmDeptIdDb).Value = DeptidDb;
                SqlParameter parmFromDate = new SqlParameter("@FromDateCht", SqlDbType.VarChar);
                cmd.Parameters.Add(parmFromDate).Value = FromDateDb;
                SqlParameter parmToDate = new SqlParameter("@ToDateCht", SqlDbType.VarChar);
                cmd.Parameters.Add(parmToDate).Value = ToDateDb;
                SqlParameter parmTypeofIncident = new SqlParameter("@IncidentFileterEvent", SqlDbType.VarChar);
                cmd.Parameters.Add(parmTypeofIncident).Value = IncidentFileterEvent;

                //commented on 05-02-2020

                SqlParameter parm_IRNumber = new SqlParameter("@sSearch_IRNumber", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_IRNumber).Value = sSearch_IRNumber;
                SqlParameter parm_DeptName = new SqlParameter("@sSearch_DeptName", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_DeptName).Value = sSearch_DeptName;
                //SqlParameter parm_DocumentNo = new SqlParameter("@sSearch_DocumentNo", SqlDbType.VarChar);
                //cmd.Parameters.Add(parm_DocumentNo).Value = sSearch_DocumentNo;
                //SqlParameter parm_DocumentName = new SqlParameter("@sSearch_DocumentName", SqlDbType.VarChar);
                //cmd.Parameters.Add(parm_DocumentName).Value = sSearch_DocumentName;
                SqlParameter parm_EmpName = new SqlParameter("@sSearch_EmpName", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_EmpName).Value = sSearch_EmpName;
                SqlParameter parm_CreatedDate = new SqlParameter("@sSearch_CreatedDate", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_CreatedDate).Value = sSearch_CreatedDate;
                SqlParameter parm_OccurrenceDate = new SqlParameter("@sSearch_OccurrenceDate", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_OccurrenceDate).Value = sSearch_OccurrenceDate;
                SqlParameter parm_IncidentDueDate = new SqlParameter("@sSearch_IncidentDueDate", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_IncidentDueDate).Value = sSearch_IncidentDueDate;
                SqlParameter parm_CurrentStatus = new SqlParameter("@sSearch_CurrentStatus", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_CurrentStatus).Value = sSearch_CurrentStatus;

                //end commented on 05-02-2020

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //For Incident Report
        public DataSet GetIncidentReportListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int UserID, int deptIdReport, string FromDateReport, string ToDateReport,int Role,int isAll,
            string sSearch_IRNumber, string sSearch_DeptName, string sSearch_EmpName, string sSearch_CreatedDate, string sSearch_OccurrenceDate, string sSearch_IncidentDueDate, string sSearch_CurrentStatus, string sSearch_TypeofIncidentName)
        {
            try
            {
                FromDateReport = FromDateReport == "0" ? DateTime.Now.AddMonths(-1).ToString("dd MMM yyyy") : FromDateReport;
                ToDateReport = ToDateReport == "0" ? DateTime.Now.ToString("dd MMM yyyy") : ToDateReport;
                string query = "[QMS].[AizantIt_SP_GetIncidentReportList]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter parmUserid = new SqlParameter("@userid", SqlDbType.Int);
                cmd.Parameters.Add(parmUserid).Value = UserID;
                SqlParameter parmRoleStatus = new SqlParameter("@RoleId", SqlDbType.Int);
                cmd.Parameters.Add(parmRoleStatus).Value = Role;
                SqlParameter parmDeptIdDb = new SqlParameter("@DeptidCht", SqlDbType.Int);
                cmd.Parameters.Add(parmDeptIdDb).Value = deptIdReport;
                SqlParameter parmFromDate = new SqlParameter("@FromDateCht", SqlDbType.DateTime);
                cmd.Parameters.Add(parmFromDate).Value = Convert.ToDateTime(FromDateReport);
                SqlParameter parmToDate = new SqlParameter("@ToDateCht", SqlDbType.DateTime);
                cmd.Parameters.Add(parmToDate).Value = Convert.ToDateTime(ToDateReport);
                SqlParameter parmisAll = new SqlParameter("@isAll", SqlDbType.Int);
                cmd.Parameters.Add(parmisAll).Value = isAll;

                //commented on 05-02-2020
                SqlParameter parm_IRNumber = new SqlParameter("@sSearch_IRNumber", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_IRNumber).Value = sSearch_IRNumber;
                SqlParameter parm_DeptName = new SqlParameter("@sSearch_DeptName", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_DeptName).Value = sSearch_DeptName;
                SqlParameter parm_EmpName = new SqlParameter("@sSearch_EmpName", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_EmpName).Value = sSearch_EmpName;
                SqlParameter parm_CreatedDate = new SqlParameter("@sSearch_CreatedDate", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_CreatedDate).Value = sSearch_CreatedDate;
                SqlParameter parm_sSearch_OccurrenceDate = new SqlParameter("@sSearch_OccurrenceDate", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_sSearch_OccurrenceDate).Value = sSearch_OccurrenceDate;
                SqlParameter parm_IncidentDueDate = new SqlParameter("@sSearch_IncidentDueDate", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_IncidentDueDate).Value = sSearch_IncidentDueDate;
                SqlParameter parm_sSearch_CurrentStatus = new SqlParameter("@sSearch_CurrentStatus", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_sSearch_CurrentStatus).Value = sSearch_CurrentStatus;
                SqlParameter parm_TypeofIncidentName = new SqlParameter("@sSearch_TypeofIncidentName", SqlDbType.VarChar);
                cmd.Parameters.Add(parm_TypeofIncidentName).Value = sSearch_TypeofIncidentName;
                //end commented on 05-02-2020

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable IncidentInsertDal(IncidentBO IncidentBO, string Operation)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_IncidentInsertandUpdate]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter OccurrenceDate = new SqlParameter("@OccurrenceDate", SqlDbType.VarChar);
                cmd.Parameters.Add(OccurrenceDate).Value = IncidentBO.OccurrenceDate;
                SqlParameter IncidentReportDate = new SqlParameter("@IncidentReportDate", SqlDbType.VarChar);
                cmd.Parameters.Add(IncidentReportDate).Value = IncidentBO.IncidentReportDate;
                SqlParameter IncidentDueDate = new SqlParameter("@IncidentDueDate", SqlDbType.VarChar);
                cmd.Parameters.Add(IncidentDueDate).Value = IncidentBO.IncidentDueDate;
                SqlParameter deptid = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(deptid).Value = IncidentBO.DeptID;
                SqlParameter DocumentTypeID = new SqlParameter("@DocumentTypeID", SqlDbType.Int);
                cmd.Parameters.Add(DocumentTypeID).Value = IncidentBO.DocumentType;
                SqlParameter TypeofCategoryID = new SqlParameter("@TypeofCategoryID", SqlDbType.Int);
                cmd.Parameters.Add(TypeofCategoryID).Value = IncidentBO.TypeofCategoryID;
                //SqlParameter version = new SqlParameter("@versionID", SqlDbType.VarChar);
                //cmd.Parameters.Add(version).Value = IncidentBO.ReferenceNumber; 
                if (IncidentBO.DocumentType == 0)
                {
                    SqlParameter OtherDocRefNo = new SqlParameter("@OtherDocRefNo", SqlDbType.VarChar);
                    cmd.Parameters.Add(OtherDocRefNo).Value = IncidentBO.ReferenceNumber;
                    SqlParameter DocName = new SqlParameter("@DocName", SqlDbType.VarChar);
                    cmd.Parameters.Add(DocName).Value = IncidentBO.DocTitle.Trim();
                }
                    else
                {
                    SqlParameter version = new SqlParameter("@versionID", SqlDbType.VarChar);
                    cmd.Parameters.Add(version).Value = IncidentBO.ReferenceNumber;
                }
                SqlParameter IRNumber = new SqlParameter("@IRNumber", SqlDbType.VarChar);
                cmd.Parameters.Add(IRNumber).Value = IncidentBO.IRNumber;
                SqlParameter IncidentDesc = new SqlParameter("@IncidentDesc", SqlDbType.VarChar);
                cmd.Parameters.Add(IncidentDesc).Value = IncidentBO.IncidentDesc.Trim();
                SqlParameter Hod = new SqlParameter("@HODEMPID", SqlDbType.Int);
                cmd.Parameters.Add(Hod).Value = IncidentBO.HODEmpID;
                SqlParameter Initiator = new SqlParameter("@InitiatorEmpID", SqlDbType.Int);
                cmd.Parameters.Add(Initiator).Value = IncidentBO.InitiatorEmpID;
                SqlParameter InitiatorComment = new SqlParameter("@Initiator_Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(InitiatorComment).Value = IncidentBO.InitiatorComments.Trim();
                SqlParameter operation = new SqlParameter("@Operation", SqlDbType.VarChar);
                cmd.Parameters.Add(operation).Value = Operation;
                SqlParameter parmIncdtID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(parmIncdtID).Value = IncidentBO.IncidentMasterID;
                SqlParameter parmTempRefID = new SqlParameter("@TempRefID", SqlDbType.Int);
                cmd.Parameters.Add(parmTempRefID).Value = IncidentBO.TempRefID;//FileUpload RefID
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetIncidentPreliminaryAssessmentListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch,int IncidentMasterID, int isAll)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetIncidentPreliminaryAssessmentList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlParameter paramisAll = new SqlParameter("@isAll", SqlDbType.Int);
                cmd.Parameters.Add(paramisAll).Value = isAll;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //modified on 07-01-2020
        //Added isAll 
        public DataSet GetIncidentAnyotherinvestigationDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int IncidentMasterID, int isAll)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetIncidentAnyotherinvestigationList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlParameter paramisAll = new SqlParameter("@isAll", SqlDbType.Int);
                cmd.Parameters.Add(paramisAll).Value = isAll;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       

        public DataTable SendIncidentNotificationDal(int IncidentMasterID,int EmpID,int Action)
        {
            try
            {
                string query = "QMS.AizantIT_SP_IncidentNotifications";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlParameter paramQAEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramQAEmpID).Value = EmpID;
                SqlParameter paramAction = new SqlParameter("@Action", SqlDbType.Int);
                cmd.Parameters.Add(paramAction).Value = Action;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
        public int IncidentRevertAndRejectDal(IncidentBO objIncidentBO,int Action_No,int WhomToRevertID)
        {
            try
            {
                string query = "QMS.AizantIT_SP_Incident_RevertandReject";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(spmIncidentMasterID).Value = objIncidentBO.IncidentMasterID;
                SqlParameter spmActionBy = new SqlParameter("@Actionby", SqlDbType.Int);
                cmd.Parameters.Add(spmActionBy).Value = objIncidentBO.EmpID; //Reverted by
                SqlParameter Action_Nostatus = new SqlParameter("@Action", SqlDbType.Int);
                cmd.Parameters.Add(Action_Nostatus).Value = Action_No;
                SqlParameter prmWhomToRevertID = new SqlParameter("@WhomToRevertRoleID", SqlDbType.Int);
                cmd.Parameters.Add(prmWhomToRevertID).Value = WhomToRevertID;
                SqlParameter Hod_comments = new SqlParameter("@ActionbyComments", SqlDbType.VarChar);
                cmd.Parameters.Add(Hod_comments).Value = objIncidentBO.Comments.Trim();
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetIncidentWhoarethepeopleinvolvedListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int IncidentMasterID)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetIncidentWhoarethepeopleinvolvedList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Investigation Details Insert
        public int InsertInvestigationDetailsDal(IncidentBO objincidentBO,DataTable dtImpactlst, DataTable dtHasAffedtedlst, DataTable dtWasAffedtedlst,DataTable dtReviewlst)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[QMS].[AizantIT_SP_Incident_InsertInvestigationDetails]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPWhatHappen = new SqlParameter("@Whathappen", SqlDbType.VarChar);
                cmd.Parameters.Add(SPWhatHappen).Value = objincidentBO.WhatHappen == null ? "" : objincidentBO.WhatHappen.Trim();
                SqlParameter SPWhenHappen = new SqlParameter("@WhenHappen", SqlDbType.VarChar);
                cmd.Parameters.Add(SPWhenHappen).Value = objincidentBO.WhenHappen == null ? "" : objincidentBO.WhenHappen.Trim();
                SqlParameter SPWhereHappen = new SqlParameter("@WhereHappen", SqlDbType.VarChar);
                cmd.Parameters.Add(SPWhereHappen).Value = objincidentBO.WhereHappen == null ? "" : objincidentBO.WhereHappen.Trim();
                SqlParameter SPIsHasIncidentAfftectedDoc = new SqlParameter("@IsHasIncidentAfftectedDoc", SqlDbType.VarChar);
                cmd.Parameters.Add(SPIsHasIncidentAfftectedDoc).Value = objincidentBO.IsHasIncidentAfftectedDoc;
                SqlParameter SPRootCause = new SqlParameter("@RootCauseDesc", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRootCause).Value = objincidentBO.RootCauseDesc == null ? "" : objincidentBO.RootCauseDesc.Trim();
                SqlParameter SPQImpConclusion = new SqlParameter("@QImpConclusion", SqlDbType.VarChar);
                cmd.Parameters.Add(SPQImpConclusion).Value = objincidentBO.QImpConclusion == null ? "" : objincidentBO.QImpConclusion.Trim();
                SqlParameter SPActionID = new SqlParameter("@Actionby", SqlDbType.Int);
                cmd.Parameters.Add(SPActionID).Value = objincidentBO.EmpID;
                cmd.Parameters.AddWithValue("@Impactpointslst", dtImpactlst);
                cmd.Parameters.AddWithValue("@HasAffectedlst", dtHasAffedtedlst);
                cmd.Parameters.AddWithValue("@WasAffectedlst", dtWasAffedtedlst);
                cmd.Parameters.AddWithValue("@Reviewlst", dtReviewlst);
                SqlParameter SPCurrentStatus = new SqlParameter("@CurrentStatus", SqlDbType.Int);
                cmd.Parameters.Add(SPCurrentStatus).Value = objincidentBO.CurrentStatus;
                SqlParameter SPIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(SPIncidentMasterID).Value = objincidentBO.IncidentMasterID;
                SqlParameter SPInvestigationOperation = new SqlParameter("@InvestigationOperation", SqlDbType.VarChar);
                cmd.Parameters.Add(SPInvestigationOperation).Value = objincidentBO.InvestigationOperation;
                int count = cmd.ExecuteNonQuery();
                cmd.Dispose();
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public DataSet GetIncidentHasAffectedDocumentListtDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int IncidentMasterID)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetIncidentHasAffectedDocumentList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Investigation Details Insert with Quality Non-Impacting
        public int InsertInvestigationQNImpactingDetailsDal(IncidentBO objincidentBO, DataTable dtQNImpactlst)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[QMS].[AizantIT_SP_Incident_QNonImpactingInvestigationSubmitDetails]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter SPWhatHappen = new SqlParameter("@QNonImpConclusion", SqlDbType.VarChar);
                cmd.Parameters.Add(SPWhatHappen).Value = objincidentBO.QNonImpConclusion.Trim();
                cmd.Parameters.AddWithValue("@Impactpointslst", dtQNImpactlst);
                SqlParameter SPActionID = new SqlParameter("@Actionby", SqlDbType.Int);
                cmd.Parameters.Add(SPActionID).Value = objincidentBO.EmpID;
                SqlParameter SPIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(SPIncidentMasterID).Value = objincidentBO.IncidentMasterID;
                SqlParameter SPInvestigationOperation = new SqlParameter("@InvestigationOperation", SqlDbType.VarChar);
                cmd.Parameters.Add(SPInvestigationOperation).Value = objincidentBO.InvestigationOperation;
                //SqlParameter SPHod = new SqlParameter("@HODEmpID", SqlDbType.Int);
                //cmd.Parameters.Add(SPHod).Value = objincidentBO.HODEmpID;
                int count = cmd.ExecuteNonQuery();
                cmd.Dispose();
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public DataSet GetIncidentFileUploadListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int IncidentMasterID)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetIncidentFileUploadList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet GetIncidentInitiatorFileUploadListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int IncidentMasterID,int Status)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetIncidentInitiatorFileUploadList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlParameter paramStatus = new SqlParameter("@Status", SqlDbType.Int);
                cmd.Parameters.Add(paramStatus).Value = Status;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetIncidentDepartmentReviewHodListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int IncidentMasterID)
        {
            try
            {
                string query = "QMS.AizantIT_SP_GetIncidentDepartmentReviewHodList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int SubmitHODCapaDetailsDal(IncidentBO IncidentBO)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_Incident_SubmitHODCapaDetails]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parmIncdtID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(parmIncdtID).Value = IncidentBO.IncidentMasterID;
                SqlParameter prmImmediateCorrection = new SqlParameter("@ImmediateCorrection", SqlDbType.VarChar);
                cmd.Parameters.Add(prmImmediateCorrection).Value = IncidentBO.ImmediateCorrection;
                SqlParameter prmIsTrainingRequried = new SqlParameter("@IsTrainingRequried", SqlDbType.Int);
                cmd.Parameters.Add(prmIsTrainingRequried).Value = IncidentBO.IsAnyTrainingRequired;
                SqlParameter prmActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(prmActionBy).Value = IncidentBO.EmpID;
                SqlParameter prmComments = new SqlParameter("@Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(prmComments).Value = IncidentBO.HodCapaComments.Trim();
                SqlParameter prmSessionStatus = new SqlParameter("@Inci_TTS_Session", SqlDbType.Int);
                cmd.Parameters.Add(prmSessionStatus).Value = IncidentBO.SessionTTSStatus;

                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteInvestigationFormisTempDal(int IncidentMasterID)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_Incident_DeleteInvestigationFormWhenisTempisOne]";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter prmIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(prmIncidentMasterID).Value = IncidentMasterID;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //For Deleting TTS when click on NO
        public void DeleteTTSwhenNoDal(int IncidentMasterID, int UserID)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_Incident_DeleteTTSWhenSelectNo]";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter prmIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(prmIncidentMasterID).Value = IncidentMasterID;
                SqlParameter prmActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(prmActionBy).Value = UserID;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetEmpNamesbasedonRoleIDDal(int RoleID, String Status)
        {
            try
            {
                string query = "[UMS].[AizantIT_SP_GetEmployeesByRoleIDAndStatus]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramRoleID = new SqlParameter("@RoleID", SqlDbType.Int);
                cmd.Parameters.Add(paramRoleID).Value = RoleID;
                SqlParameter paramStatus = new SqlParameter("@EmpStatus", SqlDbType.VarChar);
                cmd.Parameters.Add(paramStatus).Value = Status;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Incident IsTrainingRequried
        public void CreateTargetTrainingDb(TTS_QIObjects objTTS, out int result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[QMS].[AizantIT_SP_TTS_QIInsert]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Trainers", SqlDbType.Structured).Value = objTTS.dsTrainersAndTrainees.Tables["Trainers"];
                cmd.Parameters.Add("@Trainees", SqlDbType.Structured).Value = objTTS.dsTrainersAndTrainees.Tables["Trainees"];
                SqlParameter SPDept_ID = new SqlParameter("@Dept_ID", SqlDbType.Int);
                SqlParameter SPDocument_Type = new SqlParameter("@Document_Type", SqlDbType.Int);
                SqlParameter SPDocument_ID = new SqlParameter("@Document_ID", SqlDbType.Int);
                SqlParameter SPTypeOFTraining = new SqlParameter("@TypeOfTraining", SqlDbType.VarChar, 1);
                SqlParameter SPAuthorBy = new SqlParameter("@Author_By", SqlDbType.Int);
                SqlParameter SPReviewBy = new SqlParameter("@Review_By", SqlDbType.Int);
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                SqlParameter SPTargetType = new SqlParameter("@Target_Type", SqlDbType.Int);
                SqlParameter SPMonthID = new SqlParameter("@Month_ID", SqlDbType.Int);
                SqlParameter SPDueDate = new SqlParameter("@DueDate", SqlDbType.Date);
                SqlParameter SPIncident_ID = new SqlParameter("@Incident_ID", SqlDbType.Int);
                SqlParameter SPResult = cmd.Parameters.Add("@Result", SqlDbType.Int);

                cmd.Parameters.Add(SPDept_ID).Value = objTTS.Dept_ID;
                cmd.Parameters.Add(SPDocument_Type).Value = objTTS.Document_Type;
                cmd.Parameters.Add(SPDocument_ID).Value = objTTS.Document_ID;
                cmd.Parameters.Add(SPTypeOFTraining).Value = objTTS.TypeOFTraining;
                cmd.Parameters.Add(SPAuthorBy).Value = objTTS.AuthorEmpId;
                cmd.Parameters.Add(SPReviewBy).Value = objTTS.ReviewEmpId;
                cmd.Parameters.Add(SPRemarks).Value = objTTS.Remarks.Trim();
                cmd.Parameters.Add(SPTargetType).Value = objTTS.TargetType;
                cmd.Parameters.Add(SPMonthID).Value = objTTS.MonthID;
                cmd.Parameters.Add(SPDueDate).Value = objTTS.DueDate;
                cmd.Parameters.Add(SPIncident_ID).Value = objTTS.IncidentID;
                SPResult.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
        }

        //Update TargetTraining
        public void UpdateTargetTrainingDb(TTS_QIObjects objTTS, out int result)
        {
            try
            {

                SqlCommand cmd = new SqlCommand("[QMS].[AizantIT_SP_TTS_QIUpdate]", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Trainers", SqlDbType.Structured).Value = objTTS.dsTrainersAndTrainees.Tables["Trainers"];
                cmd.Parameters.Add("@Trainees", SqlDbType.Structured).Value = objTTS.dsTrainersAndTrainees.Tables["Trainees"];
                SqlParameter SPDept_ID = new SqlParameter("@Dept_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPDept_ID).Value = objTTS.Dept_ID;
                SqlParameter SPDocument_Type = new SqlParameter("@Document_Type", SqlDbType.Int);
                cmd.Parameters.Add(SPDocument_Type).Value = objTTS.Document_Type;
                SqlParameter SPDocument_ID = new SqlParameter("@Document_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPDocument_ID).Value = objTTS.Document_ID;
                SqlParameter SPTypeOFTraining = new SqlParameter("@TypeOfTraining", SqlDbType.VarChar, 1);
                cmd.Parameters.Add(SPTypeOFTraining).Value = objTTS.TypeOFTraining;
                SqlParameter SPAuthorBy = new SqlParameter("@AuthorBy", SqlDbType.Int);
                cmd.Parameters.Add(SPAuthorBy).Value = objTTS.AuthorEmpId;
                SqlParameter SPRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar);
                cmd.Parameters.Add(SPRemarks).Value = objTTS.Remarks.Trim();
                SqlParameter SPTargetType = new SqlParameter("@Target_Type", SqlDbType.Int);
                cmd.Parameters.Add(SPTargetType).Value = objTTS.TargetType;
                SqlParameter SPMonthID = new SqlParameter("@Month_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPMonthID).Value = objTTS.MonthID;
                SqlParameter SPYear = new SqlParameter("@Year", SqlDbType.Int);
                cmd.Parameters.Add(SPYear).Value = objTTS.Year;
                SqlParameter SPTargetTS_ID = new SqlParameter("@TargetTS_ID", SqlDbType.Int);
                cmd.Parameters.Add(SPTargetTS_ID).Value = objTTS.TargetTS_ID;
                SqlParameter SPDueDate = new SqlParameter("@DueDate", SqlDbType.Date);
                cmd.Parameters.Add(SPDueDate).Value = objTTS.DueDate;

                SqlParameter SPResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                SPResult.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
        }
        public DataSet GetTargetTrainingDetailsDb(int iTTS_ID, int AccessEmpID, out int CurrentHistoryStatus, out int TargetCurrentStatus)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[QMS].[AizantIT_SP_QIGetTTS_Details]", con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmTTS_ID = new SqlParameter("@TTS_ID", SqlDbType.Int);
                SqlParameter spmAccessEmpID = new SqlParameter("@AccessEmpID", SqlDbType.Int);
                SqlParameter spmCurrentStatus = new SqlParameter("@CurrentHistoryStatus", SqlDbType.Int);
                SqlParameter spmTargetCurrentStatus = new SqlParameter("@TargetCurrentStatus", SqlDbType.Int);
                cmd.Parameters.Add(spmTTS_ID).Value = iTTS_ID;
                cmd.Parameters.Add(spmAccessEmpID).Value = AccessEmpID;
                cmd.Parameters.Add(spmCurrentStatus).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(spmTargetCurrentStatus).Direction = ParameterDirection.Output;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                string rlt = cmd.Parameters["@CurrentHistoryStatus"].Value.ToString();
                CurrentHistoryStatus = Convert.ToInt32(rlt);
                string result = cmd.Parameters["@TargetCurrentStatus"].Value.ToString();
                TargetCurrentStatus = Convert.ToInt32(result);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetTTSIDDal(int IncidentMasterID)
        {
            try
            {
                string query = "QMS.AizantIT_SP_Incident_GetTTSID";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramIncidentMasterID = new SqlParameter("@Incident_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetIstrainingExitsornotDal(int IncidentMasterID)
        {
            try
            {
                string query = "QMS.AizantIT_SP_Incident_GetIstrainingExitsornot";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To Get TTS List in Incident
        public DataTable GetIncidentTTS_ListDb(object iDisplayLength, object iDisplayStart, object iSortCol_0, object sSortDir_0, string sSearch, int IncidentMasterID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[QMS].[AizantIT_SP_Incident_GetTTS_List]", con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // to Insert Has Affected Document details.
        public int IncidentHasAffectedDocInsertandUpdateDal(IncidentBO IncidentBO, string Operation)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_IncidentHasAffectedDocumentInsertandUpdate]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter deptid = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(deptid).Value = IncidentBO.HasAffeDeptID;
                SqlParameter DocumentTypeID = new SqlParameter("@DocumentTypeID", SqlDbType.Int);
                cmd.Parameters.Add(DocumentTypeID).Value = IncidentBO.HasAffectedDocumentID;
                SqlParameter version = new SqlParameter("@versionID", SqlDbType.VarChar);
                cmd.Parameters.Add(version).Value = IncidentBO.HasAffOtherDOCRefNo;
                if (IncidentBO.HasAffectedDocumentID == 0)
                {
                    SqlParameter OtherDocRefNo = new SqlParameter("@OtherDocRefNo", SqlDbType.VarChar);
                    cmd.Parameters.Add(OtherDocRefNo).Value = IncidentBO.HasAffOtherDOCRefNo;
                    SqlParameter DocName = new SqlParameter("@DocName", SqlDbType.VarChar);
                    cmd.Parameters.Add(DocName).Value = IncidentBO.HasAffDocTitle.Trim();
                }
                SqlParameter IRNumber = new SqlParameter("@PageNo", SqlDbType.VarChar);
                cmd.Parameters.Add(IRNumber).Value = IncidentBO.HasAffectedPageNo;
                SqlParameter InitiatorComment = new SqlParameter("@Comments", SqlDbType.VarChar);
                cmd.Parameters.Add(InitiatorComment).Value = IncidentBO.AffectedComments.Trim();
                SqlParameter operation = new SqlParameter("@Operation", SqlDbType.VarChar);
                cmd.Parameters.Add(operation).Value = Operation;
                SqlParameter parmIncdtID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(parmIncdtID).Value = IncidentBO.IncidentMasterID;
                SqlParameter parmUniquePKID = new SqlParameter("@UniquePKID", SqlDbType.Int);
                cmd.Parameters.Add(parmUniquePKID).Value = IncidentBO.UniquePKID;

                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        //For Getting Incident GetFileName in Incident Reports
        public DataSet GetIncidentFileNameDal(int IncidentMasterID)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_Incident_GetFileNames]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramIncidentMasterID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //For Getting Change Control GetFileName in CCN Reports
        public DataSet GetCCNFileNameDal(int CCNID)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_CCN_GetFileNames]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramCCNMasterID = new SqlParameter("@CCNID", SqlDbType.Int);
                cmd.Parameters.Add(paramCCNMasterID).Value = CCNID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetEmployeesDeptandRoleWiseInAdminManageDAL(int DeptID, int QualityEvent)
        {
            try
            {
                string query = "QMS.[AizantIT_SP_BindCCNEmployeesWithRoleWise]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDeptID = new SqlParameter("@DeptId", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlParameter paramQualityEvnet = new SqlParameter("@Qaulity_EventType", SqlDbType.Int);
                cmd.Parameters.Add(paramQualityEvnet).Value = QualityEvent;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region QMS Charts

        public DataSet GetTotalCountToChartDal(int roletype, int DeptID,int Employeeid,string _FromDate,string _ToDate)
        {
            try
            {
                _FromDate = _FromDate == "" ?DateTime.Now.AddDays(-365).ToString("dd MMM yyyy") : _FromDate;
                _ToDate = _ToDate == "" ? DateTime.Now.ToString("dd MMM yyyy") : _ToDate;
                string query = "QMS.AizantIT_SP_GetEventsCount";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter roleType = new SqlParameter("@RoleType", SqlDbType.Int);
                cmd.Parameters.Add(roleType).Value = roletype;
                SqlParameter Deptid = new SqlParameter("@DeptId", SqlDbType.Int);
                cmd.Parameters.Add(Deptid).Value = DeptID;
                SqlParameter EmpId = new SqlParameter("@EmployeeId", SqlDbType.Int);
                cmd.Parameters.Add(EmpId).Value = Employeeid;
                SqlParameter FromDate = new SqlParameter("@ChartFromDate", SqlDbType.DateTime);
                cmd.Parameters.Add(FromDate).Value =Convert.ToDateTime( _FromDate);
                SqlParameter ToDate = new SqlParameter("@chartToDate", SqlDbType.DateTime);
                cmd.Parameters.Add(ToDate).Value =Convert.ToDateTime( _ToDate);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public DataSet GetTotalCountToCAPADeptWiseDal(int roletype, int Employeeid, string _FromDate, string _ToDate)
        {
            try
            {
                _FromDate = _FromDate == "" ? DateTime.Now.AddDays(-365).ToString("dd MMM yyyy") : _FromDate;
                _ToDate = _ToDate == "" ? DateTime.Now.ToString("dd MMM yyyy") : _ToDate;
                string query = "QMS.AizantIT_SP_GetCapaDeptChart";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter roleType = new SqlParameter("@RoleType", SqlDbType.Int);
                cmd.Parameters.Add(roleType).Value = roletype;
                //SqlParameter Deptid = new SqlParameter("@DeptId", SqlDbType.Int);
                //cmd.Parameters.Add(Deptid).Value = DeptID;
                SqlParameter EmpId = new SqlParameter("@EmployeeId", SqlDbType.Int);
                cmd.Parameters.Add(EmpId).Value = Employeeid;
                SqlParameter FromDate = new SqlParameter("@ChartFromDate", SqlDbType.DateTime);
                cmd.Parameters.Add(FromDate).Value = Convert.ToDateTime(_FromDate);
                SqlParameter ToDate = new SqlParameter("@chartToDate", SqlDbType.DateTime);
                cmd.Parameters.Add(ToDate).Value = Convert.ToDateTime(_ToDate);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public DataSet capaStatusInnerChartDAl(string Argument, int RoleType,int Employeeid, string _FromDate, string _ToDate,int QualityEventTypeID)
        {
            try
            {
                _FromDate = _FromDate == "" ? DateTime.Now.AddDays(-365).ToString("dd MMM yyyy") : _FromDate;
                _ToDate = _ToDate == "" ? DateTime.Now.ToString("dd MMM yyyy") : _ToDate;
                string query = "";
                    query = "[QMS].[AizantIT_SP_GetCapaEventChart]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter roleType = new SqlParameter("@RoleType", SqlDbType.Int);
                cmd.Parameters.Add(roleType).Value = RoleType;
                //SqlParameter eventId = new SqlParameter("@DeptId", SqlDbType.Int);
                //cmd.Parameters.Add(eventId).Value = DeptId;
                SqlParameter EmpId = new SqlParameter("@EmployeeId", SqlDbType.Int);
                cmd.Parameters.Add(EmpId).Value = Employeeid;
                SqlParameter FromDateCht = new SqlParameter("@FromDateCht", SqlDbType.DateTime);
                cmd.Parameters.Add(FromDateCht).Value = _FromDate;
                SqlParameter ToDateCht = new SqlParameter("@ToDateCht", SqlDbType.DateTime);
                cmd.Parameters.Add(ToDateCht).Value = _ToDate;
                SqlParameter QualityEventTypeMajorOrMinor = new SqlParameter("@Argument", SqlDbType.VarChar);
                cmd.Parameters.Add(QualityEventTypeMajorOrMinor).Value = Argument;
                SqlParameter QualityTypeID = new SqlParameter("@QualityEventTypeID", SqlDbType.Int);
                cmd.Parameters.Add(QualityTypeID).Value = QualityEventTypeID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetManagementChartDataDAL(DataTable dtDeptlst, DataTable dtQElst, string _FromDate, string _ToDate)
        {

            _FromDate = _FromDate == "" ? DateTime.Now.AddDays(-365).ToString("yyyymmdd") : _FromDate;
            _ToDate = _ToDate == "" ? DateTime.Now.ToString("yyyyMMdd") : _ToDate;

            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[DASHBOARD_SP_GetEventsCount_Test]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            //SqlParameter deptid = new SqlParameter("[tablevalueparametername]", SqlDbType.Int);
            //cmd.Parameters.Add(deptid).Value = DeptID;
            //SqlParameter QEvent_TypeID = new SqlParameter("[tableValueParameterName]", SqlDbType.Int);
            //cmd.Parameters.Add(QEvent_TypeID).Value = QualityEvent_TypeID;

            cmd.Parameters.AddWithValue("@DeptList", dtDeptlst);
            cmd.Parameters.AddWithValue("@QualityEventList", dtQElst);

            //SqlParameter deptId = new SqlParameter("@DeptId", SqlDbType.Int);
            //cmd.Parameters.Add(deptId).Value = DeptID;

            //SqlParameter QEvent_TypeID = new SqlParameter("@QualityEventType", SqlDbType.Int);
            //cmd.Parameters.Add(QEvent_TypeID).Value = x;


            SqlParameter FromDateCht = new SqlParameter("@FromDateCht", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = _FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDateCht", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = _ToDate;
            //pass the parameter
            SqlDataAdapter dr = new SqlDataAdapter(cmd);
            dr.Fill(ds);
            return ds;
        }




        public DataSet GetStatusChartForQualityEventsDAL(DataTable dtDeptlst, int QualityEventType, int CCNClassification,
           int CCNCategory, int IncidentClassification, int IncidentCategory, string _FromDate, string _ToDate)
        {
            _FromDate = _FromDate == "" ? DateTime.Now.AddDays(-365).ToString("dd MMM yyyy") : _FromDate;
            _ToDate = _ToDate == "" ? DateTime.Now.ToString("dd MMM yyyy") : _ToDate;
            string query = "";
            if (QualityEventType == 4)
            {
                query = "[QMS].[Dashboard_SP_NTFEventStatusCount]";
            }
            if (QualityEventType == 2 || CCNClassification == 1 || CCNClassification == 2
                || CCNCategory == 3 || CCNCategory == 4)
            {
                query = "[QMS].[Dashboard_SP_CCNEventStatusCount]";
            }
            if (QualityEventType == 3)
            {
                query = "[QMS].[Dashboard_SP_ErrataEventStatusCount]";

            }
            if (QualityEventType == 1 || IncidentClassification == 5 || IncidentClassification == 6)
            {
                query = "[QMS].[Dashboard_SP_IncidentEventStatusCount]";
            }
            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandTimeout = 300;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@DeptList", dtDeptlst);

            SqlParameter Quality_event = new SqlParameter("@QualityEventType", SqlDbType.Int);
            cmd.Parameters.Add(Quality_event).Value = QualityEventType;

            SqlParameter CCNClassificationMajorOrMinor = new SqlParameter("@CCNClassification", SqlDbType.Int);
            cmd.Parameters.Add(CCNClassificationMajorOrMinor).Value = CCNClassification;

            SqlParameter CCNCategoryPorT = new SqlParameter("@CCNCategory", SqlDbType.Int);
            cmd.Parameters.Add(CCNCategoryPorT).Value = CCNCategory;

            SqlParameter IncidentClassificationQiOrQNi = new SqlParameter("@IncidentClassification", SqlDbType.Int);
            cmd.Parameters.Add(IncidentClassificationQiOrQNi).Value = IncidentClassification;



            SqlParameter Incident_cat = new SqlParameter("@IncidentCategory", SqlDbType.Int);
            cmd.Parameters.Add(Incident_cat).Value = IncidentCategory;



            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = _FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = _ToDate;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            da.Fill(ds);
            return ds;
        }


        public DataSet GetOverdueChartDAL(DataTable dtDeptlst, int QualityEventType, int CCNClassification,
           int CCNCategory, int IncidentClassification, int IncidentCategory, string _FromDate, string _ToDate)
        {

            _FromDate = _FromDate == "" ? DateTime.Now.AddDays(-365).ToString("yyyymmdd") : _FromDate;
            _ToDate = _ToDate == "" ? DateTime.Now.ToString("yyyyMMdd") : _ToDate;

            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[Dashboard_SP_OverdueStatusCount]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DeptList", dtDeptlst);
            SqlParameter Quality_event = new SqlParameter("@QualityEventType", SqlDbType.Int);
            cmd.Parameters.Add(Quality_event).Value = QualityEventType;
            SqlParameter CCNClassificationMajorOrMinor = new SqlParameter("@CCNClassification", SqlDbType.Int);
            cmd.Parameters.Add(CCNClassificationMajorOrMinor).Value = CCNClassification;
            SqlParameter CCNCategoryPorT = new SqlParameter("@CCNCategory", SqlDbType.Int);
            cmd.Parameters.Add(CCNCategoryPorT).Value = CCNCategory;
            SqlParameter IncidentClassificationQiOrQNi = new SqlParameter("@IncidentClassification", SqlDbType.Int);
            cmd.Parameters.Add(IncidentClassificationQiOrQNi).Value = IncidentClassification;
            SqlParameter Incident_cat = new SqlParameter("@IncidentCategory", SqlDbType.Int);
            cmd.Parameters.Add(Incident_cat).Value = IncidentCategory;
            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = _FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = _ToDate;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }


        public DataSet GetPerformanceChartCountDAL(QMS_BO.QMS_Dashboard_BO.PerformanceChartCountBO pobj)
        {
            //pobj.FromDate = pobj.FromDate == "" ? DateTime.Now.AddDays(-365).ToString("yyyymmdd") : pobj.FromDate;
            //pobj.ToDate = pobj.ToDate == "" ? DateTime.Now.ToString("yyyyMMdd") : pobj.ToDate;

            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[Dashboard_SP_DeptPerformanceCount]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter eventId = new SqlParameter("@DeptId", SqlDbType.Int);
            cmd.Parameters.Add(eventId).Value = pobj.DeptID;
            SqlParameter Quality_event = new SqlParameter("@QualityEventType", SqlDbType.Int);
            cmd.Parameters.Add(Quality_event).Value = pobj.QualityEventType;
            SqlParameter CCNClassificationMajorOrMinor = new SqlParameter("@CCNClassification", SqlDbType.Int);
            cmd.Parameters.Add(CCNClassificationMajorOrMinor).Value = pobj.CCNClassification;
            SqlParameter CCNCategoryPorT = new SqlParameter("@CCNCategory", SqlDbType.Int);
            cmd.Parameters.Add(CCNCategoryPorT).Value = pobj.CCNCategory;
            SqlParameter IncidentClassificationQiOrQNi = new SqlParameter("@IncidentClassification", SqlDbType.Int);
            cmd.Parameters.Add(IncidentClassificationQiOrQNi).Value = pobj.IncidentClassification;
            SqlParameter Incident_cat = new SqlParameter("@IncidentCategory", SqlDbType.Int);
            cmd.Parameters.Add(Incident_cat).Value = pobj.IncidentCategory;
            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = pobj.FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = pobj.ToDate;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }


        public DataSet GetPerformanceChartTATDAL(QMS_BO.QMS_Dashboard_BO.PerformanceChartCountBO ptobj)
        {
            //ptobj.FromDate = ptobj.FromDate == "" ? DateTime.Now.AddDays(-365).ToString("yyyymmdd") : ptobj.FromDate;
            //ptobj.ToDate = ptobj.ToDate == "" ? DateTime.Now.ToString("yyyyMMdd") : ptobj.ToDate;

            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[Dashboard_SP_DeptPerformanceTAT]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter eventId = new SqlParameter("@DeptId", SqlDbType.Int);
            cmd.Parameters.Add(eventId).Value = ptobj.DeptID;
            SqlParameter Quality_event = new SqlParameter("@QualityEventType", SqlDbType.Int);
            cmd.Parameters.Add(Quality_event).Value = ptobj.QualityEventType;
            SqlParameter CCNClassificationMajorOrMinor = new SqlParameter("@CCNClassification", SqlDbType.Int);
            cmd.Parameters.Add(CCNClassificationMajorOrMinor).Value = ptobj.CCNClassification;
            SqlParameter CCNCategoryPorT = new SqlParameter("@CCNCategory", SqlDbType.Int);
            cmd.Parameters.Add(CCNCategoryPorT).Value = ptobj.CCNCategory;
            SqlParameter IncidentClassificationQiOrQNi = new SqlParameter("@IncidentClassification", SqlDbType.Int);
            cmd.Parameters.Add(IncidentClassificationQiOrQNi).Value = ptobj.IncidentClassification;
            SqlParameter Incident_cat = new SqlParameter("@IncidentCategory", SqlDbType.Int);
            cmd.Parameters.Add(Incident_cat).Value = ptobj.IncidentCategory;
            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = ptobj.FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = ptobj.ToDate;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }

        public DataSet GetCCNListDAL(DataTable dtDeptlst, int showtype, int CCNClassification, int CCNCategory, string FromDate
                , string ToDate)
        {
            FromDate = FromDate == "" ? DateTime.Now.AddDays(-365).ToString("yyyymmdd") : FromDate;
            ToDate = ToDate == "" ? DateTime.Now.ToString("yyyyMMdd") : ToDate;
            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[Dashboard_SP_CCNList]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DeptList", dtDeptlst);
            SqlParameter ShowType = new SqlParameter("@ShowType", SqlDbType.Int);
            cmd.Parameters.Add(ShowType).Value = showtype;
            SqlParameter CCNClassificationMajorOrMinor = new SqlParameter("@CCNClassification", SqlDbType.Int);
            cmd.Parameters.Add(CCNClassificationMajorOrMinor).Value = CCNClassification;
            SqlParameter CCNCategoryPorT = new SqlParameter("@CCNCategory", SqlDbType.Int);
            cmd.Parameters.Add(CCNCategoryPorT).Value = CCNCategory;
            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = ToDate;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        public DataSet GetIncidentListDAL(DataTable dtDeptlst, int showtype, int IncidentClassification, int IncidentCategory, string FromDate
               , string ToDate)
        {
            FromDate = FromDate == "" ? DateTime.Now.AddDays(-365).ToString("yyyymmdd") : FromDate;
            ToDate = ToDate == "" ? DateTime.Now.ToString("yyyyMMdd") : ToDate;
            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[Dashboard_SP_IncidentList]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DeptList", dtDeptlst);
            SqlParameter ShowType = new SqlParameter("@ShowType", SqlDbType.Int);
            cmd.Parameters.Add(ShowType).Value = showtype;
            SqlParameter IncidentClassificationQiOrQNi = new SqlParameter("@IncidentClassification", SqlDbType.Int);
            cmd.Parameters.Add(IncidentClassificationQiOrQNi).Value = IncidentClassification;
            SqlParameter Incident_cat = new SqlParameter("@IncidentCategory", SqlDbType.Int);
            cmd.Parameters.Add(Incident_cat).Value = IncidentCategory;
            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = ToDate;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        public DataSet GetErrataListDAL(DataTable dtDeptlst, int showtype, string FromDate
              , string ToDate)
        {
            FromDate = FromDate == "" ? DateTime.Now.AddDays(-365).ToString("yyyymmdd") : FromDate;
            ToDate = ToDate == "" ? DateTime.Now.ToString("yyyyMMdd") : ToDate;
            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[Dashboard_SP_ErrataList]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DeptList", dtDeptlst);
            SqlParameter ShowType = new SqlParameter("@ShowType", SqlDbType.Int);
            cmd.Parameters.Add(ShowType).Value = showtype;
            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = ToDate;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }

        public DataSet GetNTFListDAL(DataTable dtDeptlst, int showtype, string FromDate
              , string ToDate)
        {
            FromDate = FromDate == "" ? DateTime.Now.AddDays(-365).ToString("yyyymmdd") : FromDate;
            ToDate = ToDate == "" ? DateTime.Now.ToString("yyyyMMdd") : ToDate;
            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[Dashboard_SP_NTFList]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DeptList", dtDeptlst);
            SqlParameter ShowType = new SqlParameter("@ShowType", SqlDbType.Int);
            cmd.Parameters.Add(ShowType).Value = showtype;
            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = ToDate;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }

        public DataSet GetIncidentListViewForPerformanceChartDAL(PerformanceChartCountBO PLobj)
        {
            PLobj.FromDate = PLobj.FromDate == "" ? DateTime.Now.AddDays(-365).ToString("yyyymmdd") : PLobj.FromDate;
            PLobj.ToDate = PLobj.ToDate == "" ? DateTime.Now.ToString("yyyyMMdd") : PLobj.ToDate;

            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[Dashboard_SP_DeptPerformanceIncidentList]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter eventId = new SqlParameter("@DeptId", SqlDbType.Int);
            cmd.Parameters.Add(eventId).Value = PLobj.DeptID;
            SqlParameter IncidentClassificationQiOrQNi = new SqlParameter("@IncidentClassification", SqlDbType.Int);
            cmd.Parameters.Add(IncidentClassificationQiOrQNi).Value = PLobj.IncidentClassification;
            SqlParameter Incident_cat = new SqlParameter("@IncidentCategory", SqlDbType.Int);
            cmd.Parameters.Add(Incident_cat).Value = PLobj.IncidentCategory;
            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = PLobj.FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = PLobj.ToDate;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        public DataSet GetCCNListViewForPerformanceChartDAL(PerformanceChartCountBO PLobj)
        {
            PLobj.FromDate = PLobj.FromDate == "" ? DateTime.Now.AddDays(-365).ToString("yyyymmdd") : PLobj.FromDate;
            PLobj.ToDate = PLobj.ToDate == "" ? DateTime.Now.ToString("yyyyMMdd") : PLobj.ToDate;

            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[Dashboard_SP_DeptPerformanceCCNList]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter eventId = new SqlParameter("@DeptId", SqlDbType.Int);
            cmd.Parameters.Add(eventId).Value = PLobj.DeptID;
            SqlParameter CCNClassificationMajorOrMinor = new SqlParameter("@CCNClassification", SqlDbType.Int);
            cmd.Parameters.Add(CCNClassificationMajorOrMinor).Value = PLobj.CCNClassification;
            SqlParameter CCNCategoryPorT = new SqlParameter("@CCNCategory", SqlDbType.Int);
            cmd.Parameters.Add(CCNCategoryPorT).Value = PLobj.CCNCategory;
            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = PLobj.FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = PLobj.ToDate;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        public DataSet GetErrataListViewForPerformanceChartDAL(PerformanceChartCountBO PLobj)
        {
            PLobj.FromDate = PLobj.FromDate == "" ? DateTime.Now.AddDays(-365).ToString("yyyymmdd") : PLobj.FromDate;
            PLobj.ToDate = PLobj.ToDate == "" ? DateTime.Now.ToString("yyyyMMdd") : PLobj.ToDate;

            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[Dashboard_SP_DeptPerformanceErrataList]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter eventId = new SqlParameter("@DeptId", SqlDbType.Int);
            cmd.Parameters.Add(eventId).Value = PLobj.DeptID;
            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = PLobj.FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = PLobj.ToDate;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        public DataSet GetNTFListViewForPerformanceChartDAL(PerformanceChartCountBO PLobj)
        {
            PLobj.FromDate = PLobj.FromDate == "" ? DateTime.Now.AddDays(-365).ToString("yyyymmdd") : PLobj.FromDate;
            PLobj.ToDate = PLobj.ToDate == "" ? DateTime.Now.ToString("yyyyMMdd") : PLobj.ToDate;

            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[Dashboard_SP_DeptPerformanceNTFList]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter eventId = new SqlParameter("@DeptId", SqlDbType.Int);
            cmd.Parameters.Add(eventId).Value = PLobj.DeptID;
            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = PLobj.FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = PLobj.ToDate;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }

        public DataSet GetCAPAChartDataDAL(DataTable dtDeptlst, DataTable dtQElst, string _FromDate, string _ToDate)
        {

            _FromDate = _FromDate == "" ? DateTime.Now.AddDays(-365).ToString("yyyymmdd") : _FromDate;
            _ToDate = _ToDate == "" ? DateTime.Now.ToString("yyyyMMdd") : _ToDate;
            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("[QMS].[DASHBOARD_SP_GetCAPA_Eventcount]", con);
            //mention that we are working with sp
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DeptList", dtDeptlst);
            cmd.Parameters.AddWithValue("@QualityEventList", dtQElst);
            //SqlParameter Quality_event = new SqlParameter("@QualityEventType", SqlDbType.Int);
            //cmd.Parameters.Add(Quality_event).Value = QualityEventType;          
            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = _FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = _ToDate;
            //pass the parameter
            SqlDataAdapter dr = new SqlDataAdapter(cmd);
            dr.Fill(ds);
            return ds;
        }

        public DataSet GetCAPAStatusChartDAL(DataTable dtDeptlst, int QualityEventType, int TypeofAction,
           string _FromDate, string _ToDate)
        {
            _FromDate = _FromDate == "" ? DateTime.Now.AddDays(-365).ToString("dd MMM yyyy") : _FromDate;
            _ToDate = _ToDate == "" ? DateTime.Now.ToString("dd MMM yyyy") : _ToDate;
            string query = "";
            query = "[QMS].[DASHBOARD_SP_GetCAPStatus_count]";
            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandTimeout = 300;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DeptList", dtDeptlst);
            SqlParameter Quality_event = new SqlParameter("@QualityEventType", SqlDbType.Int);
            cmd.Parameters.Add(Quality_event).Value = QualityEventType;
            SqlParameter typeof_action = new SqlParameter("@TypeofAction", SqlDbType.Int);
            cmd.Parameters.Add(typeof_action).Value = TypeofAction;
            SqlParameter FromDateCht = new SqlParameter("@FromDate", SqlDbType.VarChar);
            cmd.Parameters.Add(FromDateCht).Value = _FromDate;
            SqlParameter ToDateCht = new SqlParameter("@ToDate", SqlDbType.VarChar);
            cmd.Parameters.Add(ToDateCht).Value = _ToDate;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }



        public DataSet GetPendingTotalCountToChartDAL(string QualityEvent,int RoleType, int DeptID, int Employeeid, string _FromDate, string _ToDate)
        {
            try
            {
                _FromDate = _FromDate == "" ? DateTime.Now.AddDays(-365).ToString("dd MMM yyyy") : _FromDate;
                _ToDate = _ToDate == "" ? DateTime.Now.ToString("dd MMM yyyy") : _ToDate;
                string query = "";
                if (QualityEvent.ToUpper().Trim() == "NOTE TO FILE")
                {
                    query = "[QMS].[AizantIT_SP_NTFEventStatusCount]";
                }
                if (QualityEvent.ToUpper().Trim() == "CHANGE CONTROL NOTE" || QualityEvent=="1"|| QualityEvent == "2"
                    ||QualityEvent == "3"|| QualityEvent == "4")
                {
                    query = "[QMS].[AizantIT_SP_CCNEventStatusCount]";
                }
                if (QualityEvent.ToUpper().Trim() == "ERRATA")
                {
                    query = "[QMS].[AizantIT_SP_ErrataEventStatusCount]";
                   
                }
                if (QualityEvent.ToUpper().Trim() == "INCIDENT" || QualityEvent == "5" || QualityEvent == "6")
                {
                    query = "[QMS].[AizantIT_SP_IncidentEventStatusCount]";
                }
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter roleType = new SqlParameter("@RoleType", SqlDbType.Int);
                cmd.Parameters.Add(roleType).Value = RoleType;
                SqlParameter eventId = new SqlParameter("@DeptId", SqlDbType.Int);
                cmd.Parameters.Add(eventId).Value = DeptID;
                SqlParameter EmpId = new SqlParameter("@EmployeeId", SqlDbType.Int);
                cmd.Parameters.Add(EmpId).Value = Employeeid;
                SqlParameter FromDateCht = new SqlParameter("@FromDateCht", SqlDbType.VarChar);
                cmd.Parameters.Add(FromDateCht).Value = _FromDate;
                SqlParameter ToDateCht = new SqlParameter("@ToDateCht", SqlDbType.VarChar);
                cmd.Parameters.Add(ToDateCht).Value = _ToDate;
                SqlParameter QualityEventTypeMajorOrMinor = new SqlParameter("@QualtyEventType", SqlDbType.VarChar);
                cmd.Parameters.Add(QualityEventTypeMajorOrMinor).Value = QualityEvent;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
       
        //public DataSet GetCCNFileUploadListDal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int CCNID)
        //{
        //    try
        //    {
        //        string query = "[QMS].[AizantIT_SP_GetCCNFileUploadList]";
        //        SqlCommand cmd = new SqlCommand(query, con);
        //        cmd.CommandTimeout = 300;
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
        //        cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
        //        SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
        //        cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
        //        SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
        //        cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
        //        SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
        //        cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
        //        SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
        //        cmd.Parameters.Add(paramSearchString).Value = sSearch;
        //        SqlParameter paramCCNNo = new SqlParameter("@CCNID", SqlDbType.VarChar);
        //        cmd.Parameters.Add(paramCCNNo).Value = CCNID;
        //        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //        DataSet dt = new DataSet();
        //        da.Fill(dt);
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

      
        public DataSet GetAccessibleDepartmentsDAL(int EmpID,int RoleID,int ModuelID)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_GetAccessibleDepartmentWithRole]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramEmpID).Value = EmpID;
                SqlParameter paramRoleID = new SqlParameter("@RoleID", SqlDbType.Int);
                cmd.Parameters.Add(paramRoleID).Value = RoleID;
                SqlParameter paramModuelID = new SqlParameter("@ModuleID", SqlDbType.Int);
                cmd.Parameters.Add(paramModuelID).Value = ModuelID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        public int NTFAdminManageDAL(NoteFileBO NTFBO, out int Result)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_NTFUpdateManage]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter NTF_id = new SqlParameter("@NTF_ID", SqlDbType.Int);
                cmd.Parameters.Add(NTF_id).Value = NTFBO.NF_ID;

                SqlParameter SelectDDLEmpId = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SelectDDLEmpId).Value = NTFBO.SelectEmployee;

                SqlParameter Snumber = new SqlParameter("@Sno", SqlDbType.Int);
                cmd.Parameters.Add(Snumber).Value=NTFBO.Sno;

                SqlParameter paramActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(paramActionBy).Value = NTFBO.ActionBy;

                //SqlParameter status = new SqlParameter("@Status", SqlDbType.VarChar);
                //cmd.Parameters.Add(status).Value = NTFBO.Status.Trim();

                SqlParameter paramComments = new SqlParameter("@ReassignComments", SqlDbType.VarChar);
                cmd.Parameters.Add(paramComments).Value = NTFBO.ReassignComments;

                SqlParameter GetResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                GetResult.Direction = ParameterDirection.Output;

                con.Open();
                int i = cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //end

        public int ERTAdminManageDAL(ErrataBO ERTBO, out int Result)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_ERTUpdateManage]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ERT_id = new SqlParameter("@ERT_ID", SqlDbType.Int);
                cmd.Parameters.Add(ERT_id).Value = ERTBO.ER_ID;

                SqlParameter SelectDDLEmpId = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SelectDDLEmpId).Value = ERTBO.SelectEmployee;

                SqlParameter Snumber = new SqlParameter("@Sno", SqlDbType.Int);
                cmd.Parameters.Add(Snumber).Value = ERTBO.Sno;

                SqlParameter paramActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(paramActionBy).Value = ERTBO.ActionBy;

                //Comments
                SqlParameter paramComments = new SqlParameter("@ReassignComments", SqlDbType.VarChar);
                cmd.Parameters.Add(paramComments).Value = ERTBO.ReassignComments;

                SqlParameter GetResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                GetResult.Direction = ParameterDirection.Output;

                con.Open();
                int i = cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int INCAdminManageDAL(IncidentBO INCBO, out int Result)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_INCUpdateManage]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter Inc_id = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(Inc_id).Value = INCBO.IncidentMasterID;

                SqlParameter SelectDDLEmpId = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(SelectDDLEmpId).Value = INCBO.SelectEmployee;

                SqlParameter Snumber = new SqlParameter("@Sno", SqlDbType.Int);
                cmd.Parameters.Add(Snumber).Value = INCBO.Sno;

                SqlParameter paramActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(paramActionBy).Value = INCBO.ActionBy;

                SqlParameter paramComments = new SqlParameter("@ReassignComments", SqlDbType.VarChar);
                cmd.Parameters.Add(paramComments).Value = INCBO.ReassignComments;

                SqlParameter paramType = new SqlParameter("@Type", SqlDbType.Int);
                cmd.Parameters.Add(paramType).Value = INCBO.Type;

                SqlParameter GetResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                GetResult.Direction = ParameterDirection.Output;

                con.Open();
                int i = cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //For Updation Review HODs in Incident
        public int UpdateReviewHODsDAL(int HodEmpId, int UniqueID, int IncidentMasterID, int ActionBy, string ReviewHODComments, out int Result)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_INCModifyReviewHOD]";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramUnqID = new SqlParameter("@Unique_PKID", SqlDbType.Int);
                cmd.Parameters.Add(paramUnqID).Value = UniqueID;

                SqlParameter paramHodID = new SqlParameter("@Modified_HodEmpID", SqlDbType.Int);
                cmd.Parameters.Add(paramHodID).Value = HodEmpId;

                SqlParameter paramIncID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncID).Value = IncidentMasterID;

                SqlParameter paramActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
                cmd.Parameters.Add(paramActionBy).Value = ActionBy;

                SqlParameter GetResult = cmd.Parameters.Add("@ResultOutParam", SqlDbType.Int);
                GetResult.Direction = ParameterDirection.Output;

                SqlParameter paramAssessComments = new SqlParameter("@ReviewHODComments", SqlDbType.VarChar);
                cmd.Parameters.Add(paramAssessComments).Value = ReviewHODComments;

                int i = cmd.ExecuteNonQuery();
                Result = Convert.ToInt32(cmd.Parameters["@ResultOutParam"].Value);
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // DAL for MAP Quality Notification
        public int AddMapQualityNotificationDAL(DataTable dtMapQultyNtf)
        {
            try
            {
                string query = "[QMS].[AizantIT_MapQualityNotification]";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MapQualityNotf_List", dtMapQultyNtf);

                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //DAL for MAPA INCIDENT Insert
        public int AddMapINCI_InsertDal(DataTable dtMapIncident_lst)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_INC_MapINCIDENTInsert]";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MapINCIDENTList", dtMapIncident_lst);

                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetIncident_Report_From_SingleSP_DAL(int IncidentMasterID)
        {
            try
            {
                //start commented on 19-03-2020
                //string query = "[QMS].[AizantIT_SP_GetIncidentReport_Data]";
                string query = "[QMS].[AizantIT_SP_GetIncidentReport_Data]";
                //end commented on 19-03-2020
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramIncidentMasterID = new SqlParameter("@Incident_ID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetIncident_TTS_DateDAL(int IncidentMasterID)
        {
            try
            {
                string query = "[QMS].[AizantIT_SP_TTSGetMinDate]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramIncidentMasterID = new SqlParameter("@IncidentID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentMasterID).Value = IncidentMasterID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetMapIncidentNumberDal(int IncidentMasterID, int Deptid)
        {
            try
            {
                string query = "[QMS].[AizantIt_SP_GetMapIncidentNumbers]";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter prmIncidentMasterID = new SqlParameter("@IncidentID", SqlDbType.Int);
                cmd.Parameters.Add(prmIncidentMasterID).Value = IncidentMasterID;
                SqlParameter prmDeptid = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(prmDeptid).Value = Deptid;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sqlDataAdapter.Fill(dt);
                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetAllEmployeesDal(int IncidentMasterID, int Deptid)
        {
            try
            {
                string query = "[QMS].[AizantIt_SP_GetAllEmployees]";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter prmIncidentMasterID = new SqlParameter("@IncidentID", SqlDbType.Int);
                cmd.Parameters.Add(prmIncidentMasterID).Value = IncidentMasterID;
                SqlParameter prmDeptid = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(prmDeptid).Value = Deptid;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sqlDataAdapter.Fill(dt);
                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region SingleTonDAL
        //public static QMS_DAL ClassObjQMS_DAL;
        //public static QMS_DAL GetInstanceDAL()
        //{
        //    if (ClassObjQMS_DAL == null)
        //    {
        //        ClassObjQMS_DAL = new QMS_DAL();
        //    }
        //    return ClassObjQMS_DAL;
        //}
        //public DataSet GetErrataList()
        //{
        //    string query = "QMS.AizantIT_SingleTonErrataList";
        //    SqlCommand cmd = new SqlCommand(query, con);
        //    cmd.CommandTimeout = 300;
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataSet dt = new DataSet();
        //    da.Fill(dt);
        //    return dt;
        //}
        #endregion

        public DataTable GetMapQualityNotifDal(int EventTypeID, int DeptID, int IncidentMasterID)
        {
            try
            {
                string query = "[QMS].[AizantIt_SP_GetMapQualityNotif_Numbers]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramEventTypeID = new SqlParameter("@EventTypeID", SqlDbType.Int);
                cmd.Parameters.Add(paramEventTypeID).Value = EventTypeID;
                SqlParameter paramDeptID = new SqlParameter("@DeptID", SqlDbType.Int);
                cmd.Parameters.Add(paramDeptID).Value = DeptID;
                SqlParameter paramIncidentID = new SqlParameter("@IncidentMasterID", SqlDbType.Int);
                cmd.Parameters.Add(paramIncidentID).Value = IncidentMasterID;
                
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
