﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace QMS_BO.QMS_Dashboard_BO
{
    public class Dashboard_QMS
    {
        [Key]
        public int CapaInitiator { get; set; }
        public int CapaHOD { get; set; }
        public int CapaQA { get; set; }

        #region Capa cardBo's


        //CAPA Card BO
        public int CapaQAReview { get; set; }
        public int CapaHeadQAReview { get; set; }
        public int CapaEmployeeAccept { get; set; }
        public int CapaEmployeeComplete { get; set; }
        public int CapaHODRevert { get; set; }
        //CAPA Card Remainder BO
        public int CapaRemainderQAApprove { get; set; }
        public int CapaRemainderHODApprove { get; set; }
        public int CapaRemainderHodRevert { get; set; }
        public int CapaReminder1Count { get; set; }
        public int CapaReminder2Count { get; set; }
        public int CapaVerify { get; set; }
        //commented by pradeep 31-01-2019
        public int CapaClosure { get; set; }
        public int CapaHeadQAApprove { get; set; }

        //end
        public int CapaNoRecoreds { get; set; }
        public int QualityEventNoRecoreds { get; set; }
        #endregion

        #region ChangeControl Card

        //CCN Card BO
        public int CCNHodApprove { get; set; }
        public int CCNHoDRevert { get; set; }
        public int CCNInitiatorRevert { get; set; }
        public int CCNQAApprove { get; set; }
        public int CCNQARevert { get; set; }
        public int CCNHeadQAApprove { get; set; }
        public int CCNHODAssessment { get; set; }
        public int CCNQACloserList { get; set; }
        public int CCNQAVerificationList { get; set; }
        public int CCNQAssessment { get; set; }
        #endregion

        #region NoteToFile Card

        //NoteToFile Card BO
        public int NTFHodRevert { get; set; }
        public int NTFHODReview { get; set; }
        public int NTFQARevert { get; set; }
        public int NTFHEADQA_Review { get; set; }
        public int NTFQA_Review { get; set; }
        public int NTFHeadQA_Revert { get; set; }
        #endregion

        #region Errata Card
        //Errata Card BO
        public int ErrataInitiatorRevert { get; set; }
        public int ErrataHODRevert { get; set; }
        public int ErrataHODReview { get; set; }
        public int ErrataQAReview { get; set; }
        public int ErrataHQAReview { get; set; }
        public int ErrataQaVerify { get; set; }
        public int ErrataComplete { get; set; }
        #endregion

        #region EventTotalCountBo's


        public int ReviewTotalCount { get; set; }
        public int RevertTotalCount { get; set; }
        public int ReminderTotalCount { get; set; }
        public int TotalPending { get; set; }
        public int TotalOverDueCount { get; set; }

        #endregion

        #region Incident Card


        //Inicident Card BO
        public int INI_InitiatorRevert { get; set; }
        public int INI_HODRevert { get; set; }

        public int INI_HODActionPlan { get; set; }
        public int INI_HODReview { get; set; }
        public int INI_QAReview { get; set; }
        public int INI_HeadQAReview { get; set; }
        public int INI_InvestigatorRevert { get; set; }
        public int INI_InvestigatorReview { get; set; }

        public int INI_OtherDeptHodReview { get; set; }
        #endregion
        //bind Roles
        public List<SelectListItem> ddlEventRoles { get; set; }
        public List<SelectListItem> ddlCAPARoles { get; set; }
        public int EMPID { get; set; }

        #region Charts Bo


        //Getting Event Name and Total Count in Charts

        public List<EventList> QulaityEventList { get; set; }
        public List<EventInnerList> QualityInnerList { get; set; }
        public List<EventCAPAList> CapaQualityList { get; set; }
        public List<EventCAPAStatusList> CapaStatusList { get; set; }
        //Capa Onchange Qulality Event Type and GetTotal 
        public int QualityEventTotalCount { get; set; }
        public string QualityEventName { get; set; }
        //Bind total count CCN,NTF,Errata
        public int TotalEventCounts { get; set; }
        //Store Moduel Event Name
        public string QualityEventType { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public List<RolesMaster> RoleListMaster { get; set; }
        #endregion

        #region Audit Cards
        public int Audit_HeadQA_RevertedAudit { get; set; }
        public int Audit_ResponsesReview { get; set; }
        public int Audit_observationDueDateReminderToAuthorQA { get; set; }
        public int Audit_observationDueDateReminderToQAForAllRecords { get; set; }
        public int Audit_ObservationsOverDueCountToQAandHeadQAForAllRecords { get; set; }
        public int Audit_PendingOnComplete { get; set; }
        public int Audit_AuditsPendingOnHeadQA_Approval { get; set; }
        public int Audit_ObservationAcceptancePending { get; set; }
        public int Audit_ObservationResponsePending { get; set; }
        public int Audit_RevertedResponses { get; set; }
        public int Audit_observationDueDateReminderToResponsibleHOD { get; set; }
        public int Audit_ObservationsOverDueCountToResponsibleHOD { get; set; }
        #endregion
        #region Audit Charts
        public bool ShowInProgressAuditChart { get; set; }
        public bool ShowCompletedAuditChart { get; set; }
        #endregion
        //Html Elements
        public bool ShowReviewCard { get; set; }
        public bool ShowRevertCard { get; set; }
        public bool ShowPendingCard { get; set; }
        public bool ShowRemainderCard { get; set; }
        public bool ShowOverdueSticky { get; set; }
        public bool ShowQmsChartsDropdown { get; set; }
        public bool ShowQMSCAPAChart { get; set; }
        public bool ShowQmsQaulityEventChart { get; set; }


    }
    public class RolesMaster
    {
        public RolesMaster(string RoleNameList, int RoleIDList)
        {
            RoleName = RoleNameList; RoleID = RoleIDList;
        }
        private string _RoleName;
        public string RoleName
        {
            get
            {
                return _RoleName;
            }
            set
            {
                _RoleName = value;
            }
        }
        private int _RoleID;
        public int RoleID
        {
            get
            {
                return _RoleID;
            }
            set
            {
                _RoleID = value;
            }
        }
    }
    //Quality EVent chart List
    public class EventList
    {
        public EventList(string EventName1, double TotalRecords1)
        {
            EventName = EventName1; TotalRecords = TotalRecords1;
        }
        private string _EventName;
        public string EventName
        {
            get
            {
                return _EventName;
            }
            set
            {
                _EventName = value;
            }
        }
        private double _TotalRecords;
        public double TotalRecords
        {
            get
            {
                return _TotalRecords;
            }
            set
            {
                _TotalRecords = value;
            }
        }
    }
    //Quality EVent Inner chart List
    public class EventInnerList
    {
        public EventInnerList(string EventInnerName1, double TotalInnerRecords1)
        {
            EventInnerName = EventInnerName1; TotalInnerRecords = TotalInnerRecords1;
        }
        private string _EventInnerName;
        public string EventInnerName
        {
            get
            {
                return _EventInnerName;
            }
            set
            {
                _EventInnerName = value;
            }
        }
        private double _TotalInnerRecords;
        public double TotalInnerRecords
        {
            get
            {
                return _TotalInnerRecords;
            }
            set
            {
                _TotalInnerRecords = value;
            }
        }
    }


    public class ManagementChart
    {
        public ManagementChart(string DepartmentName1, int TotalRecordsErrata1, int TotalRecordNTF1, int TotalRecordCCN1,
            int TotalRecordIncident1)
        {
            DepartmentName = DepartmentName1;
            TotalRecordsErrata = TotalRecordsErrata1;
            TotalRecordNTF = TotalRecordNTF1;
            TotalRecordCCN = TotalRecordCCN1;
            TotalRecordIncident = TotalRecordIncident1;
        }
        public string DepartmentName { get; set; }
        public int TotalRecordsErrata { get; set; }
        public int TotalRecordNTF { get; set; }
        public int TotalRecordCCN { get; set; }
        public int TotalRecordIncident { get; set; }
    }
    public class ManagementChartList
    {
        public List<ManagementChart> Mchart_bind { get; set; }
    }
    public class GetDeptFromEventTable
    {
        public GetDeptFromEventTable(int Department_ID1, string DepartmentName1)
        {
            Department_ID = Department_ID1;
            DepartmentName = DepartmentName1;
        }
        public int Department_ID { get; set; }
        public string DepartmentName { get; set; }
    }
    public class GetDeptFromEventTableList
    {
        public List<GetDeptFromEventTable> DeptFevent_list { get; set; }
    }

    public class GetQualityEventStatusCount
    {

        public GetQualityEventStatusCount(string EventName1, int TotalRecords1)
        {
            EventName = EventName1;
            TotalRecords = TotalRecords1;
        }
        public string EventName { get; set; }
        public int TotalRecords { get; set; }
    }
    public class GetQualityEventStatusCountList
    {
        public List<GetQualityEventStatusCount> QeStatus_List { get; set; }
    }

    public class GetOverdueCount
    {

        public GetOverdueCount(string EventName1, int TotalRecords1)
        {
            EventName = EventName1;
            TotalRecords = TotalRecords1;
        }
        public string EventName { get; set; }
        public int TotalRecords { get; set; }
    }
    public class GetOverdueCountList
    {
        public List<GetOverdueCount> Overdue_List { get; set; }
    }

    public class GetPerformanceChartCount
    {
        public GetPerformanceChartCount(String Monthname1, int Count1, double DeptAvg1, double OrgAvg1)
        {
            Monthname = Monthname1;
            Count = Count1;
            DeptAvg = DeptAvg1;
            OrgAvg = OrgAvg1;
        }
        public string Monthname { get; set; }
        public int Count { get; set; }
        public double DeptAvg { get; set; }
        public double OrgAvg { get; set; }
    }
    public class GetPerformanceChartCountList
    {
        public List<GetPerformanceChartCount> Performance_List { get; set; }
    }

    public class PerformanceChartCountBO
    {
        public int DeptID { get; set; }
        public int QualityEventType { get; set; }
        public int CCNClassification { get; set; }
        public int CCNCategory { get; set; }
        public int IncidentClassification { get; set; }
        public int IncidentCategory { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class CCNList
    {
        public int[] DeptID { get; set; }
        public int showtype { get; set; }
        public int CCNClassification { get; set; }
        public int CCNCategory { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class GetCCNListBO
    {
        public GetCCNListBO(string CCN_No1, string DepartmentName1,
            string CreatedBy1, string CreatedDate1, string DueDate1, string ChangeClassification1,
            string StatusName1, int NumberofDueDays1, string assignTo1)
        {
            CCN_No = CCN_No1;
            DepartmentName = DepartmentName1;
            CreatedBy = CreatedBy1;
            CreatedDate = CreatedDate1;
            DueDate = DueDate1;
            ChangeClassification = ChangeClassification1;
            StatusName = StatusName1;
            NumberofDueDays = NumberofDueDays1;
            assignTo = assignTo1;
        }
        public string CCN_No { get; set; }
        public string DepartmentName { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string DueDate { get; set; }
        public string ChangeClassification { get; set; }
        public string StatusName { get; set; }

        public int NumberofDueDays { get; set; }
        public string assignTo { get; set; }
    }
    public class GetCCNListBOList
    {
        public List<GetCCNListBO> CCN_List { get; set; }
    }
    public class IncidentList
    {
        public int[] DeptID { get; set; }
        public int showtype { get; set; }
        public int IncidentClassification { get; set; }
        public int IncidentCategory { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class GetIncidentListBO
    {
        public GetIncidentListBO(string Incident_No1, string DepartmentName1, string TypeofIncident1,
            string Category1, string CreatedBy1, string DateOfReport1, string DateOfOccurance1, string DueDate1,
           int NumberofDueDays1, string StatusName1, string assignTo1)
        {
            Incident_No = Incident_No1;
            DepartmentName = DepartmentName1;
            TypeofIncident = TypeofIncident1;
            Category = Category1;
            CreatedBy = CreatedBy1;
            DateOfReport = DateOfReport1;
            DateOfOccurance = DateOfOccurance1;
            DueDate = DueDate1;
            NumberofDueDays = NumberofDueDays1;
            StatusName = StatusName1;
            assignTo = assignTo1;
        }

        public string Incident_No { get; set; }
        public string DepartmentName { get; set; }
        public string TypeofIncident { get; set; }
        public string Category { get; set; }
        public string CreatedBy { get; set; }
        public string DateOfReport { get; set; }
        public string DateOfOccurance { get; set; }
        public string DueDate { get; set; }
        public int NumberofDueDays { get; set; }
        public string StatusName { get; set; }
        public string assignTo { get; set; }
    }
    public class GetIncidentListBOList
    {
        public List<GetIncidentListBO> Incident_List { get; set; }
    }
    public class ErrataList
    {
        public int[] DeptID { get; set; }
        public int showtype { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class GetErrataListBO
    {
        public GetErrataListBO(string Errata_No1, string DepartmentName1,
            string referencedocno1, string Docname1, string CreatedBy1, string CreatedDate1,
            string StatusName1, string assignTo1)
        {
            Errata_No = Errata_No1;
            DepartmentName = DepartmentName1;
            referencedocno = referencedocno1;
            Docname = Docname1;
            CreatedBy = CreatedBy1;
            CreatedDate = CreatedDate1;
            StatusName = StatusName1;
            assignTo = assignTo1;
        }
        public string Errata_No { get; set; }
        public string DepartmentName { get; set; }
        public string referencedocno { get; set; }
        public string Docname { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string StatusName { get; set; }
        public string assignTo { get; set; }
    }
    public class GetErrataListBOList
    {
        public List<GetErrataListBO> Errata_List { get; set; }
    }
    public class NTFList
    {
        public int[] DeptID { get; set; }
        public int showtype { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class GetNTFListBO
    {
        public GetNTFListBO(string NTF_No1, string DepartmentName1,
            string referencedocno1, string Docname1, string CreatedBy1, string CreatedDate1,
            string StatusName1, string assignTo1)
        {
            NTF_No = NTF_No1;
            DepartmentName = DepartmentName1;
            referencedocno = referencedocno1;
            Docname = Docname1;
            CreatedBy = CreatedBy1;
            CreatedDate = CreatedDate1;
            StatusName = StatusName1;
            assignTo = assignTo1;
        }
        public string NTF_No { get; set; }
        public string DepartmentName { get; set; }
        public string referencedocno { get; set; }
        public string Docname { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string StatusName { get; set; }
        public string assignTo { get; set; }
    }
    public class GetNTFListBOList
    {
        public List<GetNTFListBO> NTF_List { get; set; }
    }

    public class GetPerformanceChartListIncident
    {
        public GetPerformanceChartListIncident(string Month1, string Incident_No1, string DepartmentName1, string TypeofIncident1,
            string Category1, string CreatedBy1, string DateOfOccurance1, string DateOfReport1, string CreatedDate1,
            string DueDate1, string CompletedDate1, string TAT1)
        {
            Month = Month1;
            Incident_No = Incident_No1;
            DepartmentName = DepartmentName1;
            TypeofIncident = TypeofIncident1;
            Category = Category1;
            CreatedBy = CreatedBy1;
            DateOfOccurance = DateOfOccurance1;
            DateOfReport = DateOfReport1;
            CreatedDate = CreatedDate1;
            DueDate = DueDate1;
            CompletedDate = CompletedDate1;
            TAT = TAT1;
        }
        public string Month { get; set; }
        public string Incident_No { get; set; }
        public string DepartmentName { get; set; }
        public string TypeofIncident { get; set; }
        public string Category { get; set; }
        public string CreatedBy { get; set; }
        public string DateOfOccurance { get; set; }
        public string DateOfReport { get; set; }
        public string CreatedDate { get; set; }
        public string DueDate { get; set; }
        public string CompletedDate { get; set; }
        public string TAT { get; set; }
    }
    public class GetPerformanceChartListIncidentList
    {
        public List<GetPerformanceChartListIncident> Pchart_List { get; set; }
    }
    public class GetPerformanceChartListCCN
    {
        public GetPerformanceChartListCCN(string Month1, string CCN_No1, string DepartmentName1,
            string CreatedBy1, string CreatedDate1, string DueDate1, string CompletedDate1, string ChangeClassification1,
             string TAT1)
        {
            Month = Month1;
            CCN_No = CCN_No1;
            DepartmentName = DepartmentName1;
            CreatedBy = CreatedBy1;
            CreatedDate = CreatedDate1;
            DueDate = DueDate1;
            CompletedDate = CompletedDate1;
            ChangeClassification = ChangeClassification1;
            TAT = TAT1;
        }
        public string Month { get; set; }
        public string CCN_No { get; set; }
        public string DepartmentName { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string DueDate { get; set; }
        public string CompletedDate { get; set; }
        public string ChangeClassification { get; set; }
        public string TAT { get; set; }
    }
    public class GetPerformanceChartListCCNList
    {
        public List<GetPerformanceChartListCCN> Cchart_List { get; set; }
    }
    public class GetPerformanceChartListErrata
    {
        public GetPerformanceChartListErrata(string Month1, string Errata_No1, string DepartmentName1,
            string CreatedBy1, string CreatedDate1, string CompletedDate1, string refdocno1, string DocName1,
             string TAT1)
        {
            Month = Month1;
            Errata_No = Errata_No1;
            DepartmentName = DepartmentName1;
            CreatedBy = CreatedBy1;
            CreatedDate = CreatedDate1;
            CompletedDate = CompletedDate1;
            refdocno = refdocno1;
            DocName = DocName1;
            TAT = TAT1;
        }
        public string Month { get; set; }
        public string Errata_No { get; set; }
        public string DepartmentName { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string CompletedDate { get; set; }
        public string refdocno { get; set; }
        public string DocName { get; set; }
        public string TAT { get; set; }
    }
    public class GetPerformanceChartListErrataList
    {
        public List<GetPerformanceChartListErrata> Echart_List { get; set; }
    }
    public class GetPerformanceChartListNTF
    {
        public GetPerformanceChartListNTF(string Month1, string NTF_No1, string DepartmentName1,
            string CreatedBy1, string refdocno1, string DocName1, string CreatedDate1, string CompletedDate1,
             string TAT1)

        {
            Month = Month1;
            NTF_No = NTF_No1;
            DepartmentName = DepartmentName1;
            CreatedBy = CreatedBy1;
            refdocno = refdocno1;
            DocName = DocName1;
            CreatedDate = CreatedDate1;
            CompletedDate = CompletedDate1;
            TAT = TAT1;
        }
        public string Month { get; set; }
        public string NTF_No { get; set; }
        public string DepartmentName { get; set; }
        public string CreatedBy { get; set; }
        public string refdocno { get; set; }
        public string DocName { get; set; }
        public string CreatedDate { get; set; }
        public string CompletedDate { get; set; }
        public string TAT { get; set; }
    }
    public class GetPerformanceChartListNTFList
    {
        public List<GetPerformanceChartListNTF> Nchart_List { get; set; }
    }

    public class CapaBarChart
    {
        public int[] DeptID { get; set; }
        public int[] QualityEventType { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

    }
    public class CapaBarChartBO
    {
        public CapaBarChartBO(string DepartmentName1, int TotalRecordNTF1, int TotalRecordCCN1,
           int TotalRecordIncident1, int TotalRecordOOS1, int TotalRecordOOT1, int TotalRecordMC1)
        {
            DepartmentName = DepartmentName1;
            TotalRecordNTF = TotalRecordNTF1;
            TotalRecordCCN = TotalRecordCCN1;
            TotalRecordIncident = TotalRecordIncident1;
            TotalRecordOOS = TotalRecordOOS1;
            TotalRecordOOT = TotalRecordOOT1;
            TotalRecordMC = TotalRecordMC1;
        }
        public string DepartmentName { get; set; }
        public int TotalRecordNTF { get; set; }
        public int TotalRecordCCN { get; set; }
        public int TotalRecordIncident { get; set; }
        public int TotalRecordOOS { get; set; }
        public int TotalRecordOOT { get; set; }
        public int TotalRecordMC { get; set; }
    }
    public class CapaBarChartBOList
    {
        public List<CapaBarChartBO> CapaCount { get; set; }
    }
    public class CapaStatusParam
    {
        public int[] DeptID { get; set; }
        public int QualityEventType { get; set; }
        public int TypeofAction { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }






    public class EventCAPAList
    {
        public EventCAPAList(string DepartmentName1, double CapaCount1)
        {
            DepartmentName = DepartmentName1; CapaCount = CapaCount1;
        }
        private string _DepartmentName;
        public string DepartmentName
        {
            get
            {
                return _DepartmentName;
            }
            set
            {
                _DepartmentName = value;
            }
        }
        private double _CapaCount;
        public double CapaCount
        {
            get
            {
                return _CapaCount;
            }
            set
            {
                _CapaCount = value;
            }
        }
    }
    public class EventCAPAStatusList
    {
        public EventCAPAStatusList(string EventStatusName1, double TotalStatusRecords1, int RowID1)
        {
            EventStatusName = EventStatusName1; TotalStatusRecords = TotalStatusRecords1; RowID = RowID1;
        }
        private string _EventStatusName;
        public string EventStatusName
        {
            get
            {
                return _EventStatusName;
            }
            set
            {
                _EventStatusName = value;
            }
        }
        private double _TotalStatusRecords;
        public double TotalStatusRecords
        {
            get
            {
                return _TotalStatusRecords;
            }
            set
            {
                _TotalStatusRecords = value;
            }
        }

        private int _RowID;
        public int RowID
        {
            get
            {
                return _RowID;
            }
            set
            {
                _RowID = value;
            }
        }
    }
}
