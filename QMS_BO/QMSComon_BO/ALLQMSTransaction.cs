﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO.QMSComonBO
{
   //public class ALLQMSTransaction
   // {[Key]
   //     public int id { get; set; }
   //     public string Action { get; set; }
   //     public string ActionBy { get; set; }
   //     public string Designation { get; set; }
   //     public string CurrentStatus { get; set; }
   //     public string Comments { get; set; }
   //     public string signdate { get; set; }

   //     public string Role { get; set; }

   //     public string ActionStatus { get; set; }
   //     public int CommentsLength { get; set; }
   //     //Added AssignedTo name
   //     public string AssignedTo { get; set; }
   //     //Added AssignedTo Role name
   //     public string AssignedToRoleName { get; set; }
   //     public int HistoryID { get; set; }
   // }

    public class DocumentDetialsClass
    {
        public string DocumentNumber { get; set; }
        public string DocumentName { get; set; }
        public int VersionID { get; set; }
        public string DocumentType { get; set; }
    }

}
