﻿namespace QMS_BO.QMS_Audit_BO.AuditMaster
{
    public class AuditClientBO
    {
        public int ClientID { get; set; }
        public string ClientName { get; set; }
        public bool IsActive { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedDate { get; set; }
    }
}
