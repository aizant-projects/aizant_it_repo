﻿namespace QMS_BO.QMS_Audit_BO.AuditMaster
{
    public class TypeOfBO
    {
        public int TypeOfID { get; set; }
        public string TypeOfName { get; set; }
        public int TypeOfRepresents { get; set; }
        public bool IsActive { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedDate { get; set; }

        public TypeOfBO() { }

        public TypeOfBO(int _TypeOfID, string _TypeOfName, int _TypeOfRepresents,
            bool _IsActive, string _CreatedByName, string _CreatedDate,
             string _ModifiedByName, string _ModifiedDate)
        {
            TypeOfID = _TypeOfID;
            TypeOfName = _TypeOfName;
            TypeOfRepresents = _TypeOfRepresents;
            IsActive = _IsActive;
            CreatedByName = _CreatedByName;
            CreatedDate = _CreatedDate;
            ModifiedByName = _ModifiedByName;
            ModifiedDate = _ModifiedDate;
        }
    }
}
