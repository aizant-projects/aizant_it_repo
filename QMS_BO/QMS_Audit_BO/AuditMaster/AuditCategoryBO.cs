﻿namespace QMS_BO.QMS_Audit_BO.AuditMaster
{
    public class AuditCategoryBO
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }

        public string CreatedDate { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedDate { get; set; }
    }
}
