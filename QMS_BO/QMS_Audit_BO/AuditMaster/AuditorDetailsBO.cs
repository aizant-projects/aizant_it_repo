﻿namespace QMS_BO.QMS_Audit_BO.AuditMaster
{
    public class AuditorDetailsBO
    {
        public int AuditorID { get; set; }
        public int RegulatoryAgencyOrClientID { get; set; }
        public string AuditorName { get; set; }
        public string AuditorEmailID { get; set; }
        public string AuditorContactNumber { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedDate { get; set; }
        public string AuditorCategory { get; set; }
        public string AgencyOrClientName { get; set; }
        public AuditorDetailsBO(int _AuditorID, int _RegulatoryAgencyOrClientID, string _AuditorName, string _AuditorEmailID,
            string _AuditorContactNumber, string _CreatedByName, string _CreatedDate, string _ModifiedByName, string _ModifiedDate, string _AuditorCategory, string _AgencyOrClientName)
        {
            AuditorID = _AuditorID;
            RegulatoryAgencyOrClientID = _RegulatoryAgencyOrClientID;
            AuditorName = _AuditorName;
            AuditorEmailID = _AuditorEmailID;
            AuditorContactNumber = _AuditorContactNumber;
            CreatedByName = _CreatedByName;
            CreatedDate = _CreatedDate;
            ModifiedByName = _ModifiedByName;
            ModifiedDate = _ModifiedDate;
            AuditorCategory = _AuditorCategory;
            AgencyOrClientName = _AgencyOrClientName;
        }
        public AuditorDetailsBO()
        {

        }
    }
}
