﻿namespace QMS_BO.QMS_Audit_BO.AuditMaster
{
    public class AuditCategoryTypeBO
    {
        public int TypeID { get; set; }
        public string TypeName { get; set; }
        public bool IsActive { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedDate { get; set; }
    }
}
