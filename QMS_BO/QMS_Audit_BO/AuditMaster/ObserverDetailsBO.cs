﻿namespace QMS_BO.QMS_Audit_BO.AuditMaster
{
    public class ObserverDetailsBO
    {
        public int ObserverID { get; set; }
        public string ObserverName { get; set; }
        public string ObserverEmailID { get; set; }
        public string ObserverContactNumber { get; set; }
        public string ObserverAdditionalDetails { get; set; }
       
        public string ObserverCategory { get; set; }
        public string ClientOrRegulatoryID { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedDate { get; set; }
        public int RowNumber { get; set; }
        public int BaseID { get; set; }
        public int BaseType { get; set; }

        public ObserverDetailsBO(int _ObserverID, string _ObserverName,
            string _ObserverEmailID,string _ObserverContactNumber,
            string _ObserverAdditionalDetails,
          string _ObserverCategory,string _ClientOrRegulatoryID, string _CreatedByName, string _CreatedDate,
          string _ModifiedByName,string _ModifiedDate,int _BaseType, int _BaseID)
        {
            ObserverID = _ObserverID;
            ObserverName = _ObserverName;
            ObserverEmailID = _ObserverEmailID;
            ObserverContactNumber = _ObserverContactNumber;
            ObserverAdditionalDetails = _ObserverAdditionalDetails;
            ObserverCategory = _ObserverCategory;
            ClientOrRegulatoryID = _ClientOrRegulatoryID;
            CreatedByName = _CreatedByName;
            CreatedDate = _CreatedDate;
            ModifiedByName = _ModifiedByName;
            ModifiedDate = _ModifiedDate;
            BaseType = _BaseType;
            BaseID = _BaseID;
            
        }
    }
}
