﻿namespace QMS_BO.QMS_Audit_BO.AuditMaster
{
    public class RegulatoryAgencyBO
    {
        public int RegulatoryAgencyID { get; set; }
        public string RegulatoryAgencyCode { get; set; }
        public string RegulatoryAgencyName { get; set; }
        public string RegionName { get; set; }
        public bool IsActive { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedDate { get; set; }

        public RegulatoryAgencyBO() { }

        public RegulatoryAgencyBO(int _RegulatoryAgencyID,string _RegulatoryAgencyCode, string _RegulatoryAgencyName,
             string _RegionName, bool _IsActive,string _CreatedByName,
            string _CreatedDate,string _ModifiedByName,string _ModifiedDate)
        {
            RegulatoryAgencyID = _RegulatoryAgencyID;
            RegulatoryAgencyCode = _RegulatoryAgencyCode;
            RegulatoryAgencyName = _RegulatoryAgencyName;
            RegionName = _RegionName;
            IsActive = _IsActive;
            CreatedByName = _CreatedByName;
            CreatedDate = _CreatedDate;
            ModifiedByName = _ModifiedByName;
            ModifiedDate = _ModifiedDate;
        }
    }
}
