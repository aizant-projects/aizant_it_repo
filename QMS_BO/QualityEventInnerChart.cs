﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO
{
    public class QualityEventInnerChart
    {
        [Key]
        public int uniqueID { get; set; }
        public int Pending { get; set; }
        public int Approve { get; set; }
        public int Closer { get; set; }
        public int Verify { get; set; }
    }
   
}
