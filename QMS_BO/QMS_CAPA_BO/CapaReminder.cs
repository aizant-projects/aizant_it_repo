﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO.QMS_CAPABO
{
   public class CapaReminder
    {
        [Key]
        public string GetOffComments { get; set; }
        public int GetCommentsLength { get; set; }
        public int RM_ID { get; set; }
        public int UniqueID { get; set; }
        public int CAPAID { get; set; }
        public string Reminder { get; set; }
        public string QA_Comments { get; set; }
        public string EmployeeName { get; set; }
        public int EmpID { get; set; }
        public string Role { get; set; }
        public string Date { get; set; }
        public string HOD_Comments { get; set; }
        public int CapaID { get; set; }
        public string RemainderStatus { get; set; }
        public string SignDate { get; set; }
        public string RevisedTargetDate { get; set; }
        public string SpExecuteStatus { get; set; }
        public bool IsRevisedDate { get; set; }

        //Added AssignedTo name
        public string AssignedTo { get; set; }
        //Added AssignedTo Role name
        public string AssignedToRoleName { get; set; }

    }
    
}
