﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO.QMS_CAPA_BO
{
 public class CapaEventsBO
    {
        public string CAPA_Number { get; set; }
        public string Department { get; set; }
        public string PlanofAction { get; set; }
        public string TargetDate { get; set; }
        public string CurrentStatus { get; set; }
        public string Responsible_Persons { get; set; }
        public string CAPA_Discrption { get; set; }
        public int BaseID { get; set; }
        public int CAPAID { get; set; }
        public string AssignedQA { get; set; }
        public int QualityTypeID { get; set; }
        public string QualityEventNumber { get; set; }
       

        public CapaEventsBO(string _CAPA_Number, string _Department, string _PlanofAction, string _TargetDate, string _CurrentStatus,
          string _Responsible_Persons, string _CAPA_Discrption,int _BaseID, int _CAPAID, string _AssignedQA,
          int _QualityTypeID, string _QualityEventNumber)
        {
            CAPA_Number = _CAPA_Number;
            Department = _Department;
            PlanofAction = _PlanofAction;
            TargetDate = _TargetDate;
            CurrentStatus = _CurrentStatus;
            Responsible_Persons = _Responsible_Persons;
            CAPA_Discrption = _CAPA_Discrption;
            BaseID = _BaseID;
            CAPAID = _CAPAID;
            AssignedQA = _AssignedQA;
            QualityTypeID = _QualityTypeID;
            QualityEventNumber = _QualityEventNumber;
        }
    }
}
