﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO.QMS_CAPA_BO
{
   public class VerificationBO
    {
        public int RowNumber { get; set; }
        public int UniquePKID { get; set; }
        public string Implementor_Date { get; set; }
        public string Description { get; set; }
        public int CAPAID { get; set; }
        public string CurrentStatus { get; set; }
        public string IsImplemented { get; set; }
        public string HeadQAEmpName { get; set; }
        public int CurrentStatusNumber { get; set; }
        public VerificationBO(int _RowNumber,int _UniquePKID, string _Implementor_Date, string _Description, int _CAPAID,
            string _CurrentStatus,string _IsImplemented,string _HeadQAEmpName,int _CurrentStatusNumber)
        {
            RowNumber = _RowNumber;
            UniquePKID = _UniquePKID;
            Implementor_Date = _Implementor_Date;
            Description = _Description;
            CAPAID = _CAPAID;
            CurrentStatus = _CurrentStatus;
            IsImplemented = _IsImplemented;
            HeadQAEmpName = _HeadQAEmpName;
            CurrentStatusNumber = _CurrentStatusNumber;
        }
    }
}
