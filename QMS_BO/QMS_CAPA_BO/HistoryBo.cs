﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace QMS_BO.QMS_CAPA_BO
{
   public class HistoryBo
    {
        public int RowNumber { get; set; }
        public int ActionHistoryID { get; set; }
        public string ActionStatus { get; set; }
        public string ActionRole { get; set; }
        public string ActionBy { get; set; }
        public string ActionDate { get; set; }
        public string Comments { get; set; }
        public string TargetDate { get; set; }
        public int CommLength { get; set; }
        public string offComments { get; set; }
        public string AssignTo { get; set; }
        public string AssignToRoleID { get; set; }
        public string SubmittedData { get; set; }
        public int iDisplayLength { get; set; }
        public int iDisplayStart { get; set; }
        public int iSortCol_0 { get; set; }
        public string sSearch { get; set; }
        public string sSortDir_0 { get; set; }
        public int EventID { get; set; }
        public int EventTypeID { get; set; }
        public int IncidentStatusTTSID { get; set; }
        public int totalRecordsCount { get; set; }
        public HistoryBo()
        {
             iDisplayLength =Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayLength"]);
             iDisplayStart = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayStart"]);
             iSortCol_0 = Convert.ToInt32(HttpContext.Current.Request.Params["iSortCol_0"]);
             sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"];
             sSearch = Convert.ToString(HttpContext.Current.Request.Params["sSearch"]);
             EventID = Convert.ToInt32(HttpContext.Current.Request.Params["EventID"]);
             EventTypeID = Convert.ToInt32(HttpContext.Current.Request.Params["EventTypeID"]);
             IncidentStatusTTSID =Convert.ToInt32( HttpContext.Current.Request.Params["TMS_TTSID"]);
            totalRecordsCount = 0;
        }
        public HistoryBo(int _RowNumber, int _ActionHistoryID,string _ActionRole, string _ActionStatus, string _ActionBy, string _ActionDate, string _Comments, string _TargetDate,
            int _CommLength, string _offComments, string _AssignTo, string _AssignToRoleID, string _SubmittedData)
        {
            RowNumber = _RowNumber;
            ActionHistoryID = _ActionHistoryID;
            ActionStatus = _ActionStatus;
            ActionRole = _ActionRole;
            ActionBy = _ActionBy;
            ActionDate = _ActionDate;
            Comments = _Comments;
            TargetDate = _TargetDate;
            CommLength = _CommLength;
            offComments = _offComments;
            AssignTo = _AssignTo;
            AssignToRoleID = _AssignToRoleID;
            SubmittedData = _SubmittedData;
        }
    }
}
