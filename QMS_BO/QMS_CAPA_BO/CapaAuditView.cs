﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO.QMS_CAPABO
{
    public class CapaAuditView
    {
        public string AuditNumber { get; set; }
        public string CategoryName { get; set; }
        public string ObservationNumber { get; set; }
        public string ObservationCurrentStatus { get; set; }
        public string AuditCurrentStatus { get; set; }
        public string ObservationPriority { get; set; }
        public string ObservationDescription { get; set; }
        public string SiteName { get; set; }
        public string DepartmentName { get; set; }
        public string ResponseDueDate { get; set; }
        public List<ObservationSystemCAPA> ObservationSystems { get; set; }
        public string ResponseDescription {get;set;}
        public string ResponseDate { get; set; }
        public string ResponseExpectedOrCloserDate { get; set; }
    }
    public class ObservationSystemCAPA
    {
        public string FDA_SystemName { get; set; }
        public string AreaName { get; set; }
        public string SubAreaName { get; set; }
        public string Remarks { get; set; }
       
    }
}
