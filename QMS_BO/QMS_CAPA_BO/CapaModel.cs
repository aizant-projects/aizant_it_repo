﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace QMS_BO.QMS_CAPABO
{
    public class CapaModel
    {
        [Key]
        public int CAPAID { get; set; }
        
        public int RowNumber { get; set; }
        public string CAPA_Number { get; set; }
        
        public string CAPA_Date { get; set; }
        public string QualityEvent_TypeID { get; set; }
        public string QualityEventName { get; set; }
        public string QualityEvent_Number { get; set; }
        public string Verified_YesOrNO { get; set; }
        public string Department { get; set; }
        public string EmployeeName { get; set; }
        public int ResponsibleEmpID { get; set; }
        public string ResponsiblePersonName { get; set; }
        public string TargetDate { get; set; }
        
        public string EmpAcceptedDate { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentStatusName { get; set; }
       
        public string Capa_ActionDesc { get; set; }
        public int PlanofAction { get; set; }
        public string ActionPlanName { get; set; }
        public string CreatedBy { get; set; }
        public string Modified { get; set; }
        public string RM1 { get; set; }
        public string RM2 { get; set; }
        public string Trasaction { get; set; }
        public int DeptID { get; set; }
        public int EmpID { get; set; }
        public string comments { get; set; }
        public int OverDue { get; set; }

        public string Revertedcomments { get; set; }
        public int QA_EmpId { get; set; }
        public string QAReviewerName { get; set; }
        public int HeadQa_EmpID { get; set; }
        public string QA_Comments{ get; set; }
        public string HeadQAComments { get; set; }
        public string CapaStatusSP { get; set; }
        public int CapaQAEmpId { get; set; }
        public string Assigned_QA { get; set; }
        public DateTime ImplementorDateVerificationCAPA { get; set; }
        //This is Displayed Icon in CAPA List Page. using CurrentStatusnumbers 
        public int CurrentStatusNumber { get; set; }

        //Assessment Unique Id Store this variable
        public int AssessementUniqueID { get; set; }
        //For EmployeeCompleted Comments
        public string RespPersonCommts { get; set; }
        // [Required(ErrorMessage = "Please select file.")]
        [Display(Name = "Browse File")]
        public HttpPostedFileBase[] CapaFileUpload { get; set; }

        public byte[] CapaAttachments { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public int CapaFileUniqueID { get; set; }

        [Display(Name = "Browse File")]
        public HttpPostedFileBase[] CapaCompletFileUpload { get; set; }

        public byte[] CapaCompletAttachments { get; set; }
        public string FileTypeComplet { get; set; }
        public string FileNameComplet { get; set; }
        public int CapaCompletFileUniqueID { get; set; }

        public int CAPA_MaxNo { get; set; }
        // for incident
        //public int IRCAPAID { get; set; }
        //public string IRCAPA_Number { get; set; }
        //public string IRCAPA_Date { get; set; }
        //public string IRTargetDate { get; set; }
        //public string IRCurrentStatusName { get; set; }

        //ddl List
        public string AuditQAName { get; set; }
        public string AuditHeadQAName { get; set; }
        public string AuditHodName { get; set; }
        public List<SelectListItem> ddlDeptList { get; set; }
        public List<SelectListItem> ddlActionplan { get; set; }
        public List<SelectListItem> ddlResponsiblePerson { get; set; }
        public List<SelectListItem> ddlQAEmployeeID { get; set; }
        public List<SelectListItem> ddlHeadqaEmpId { get; set; }
        public List<SelectListItem> ddlqualityEventList { get; set; }
        public List<SelectListItem> ddlCAPAHODList { get; set; }
        public int EventsBaseID { get; set; }
        public List<ManageAdmin> GetDetailsAdminMange { get; set; }
        public int ReminderStatus1 { get; set; }
        public int ReminderStatus2 { get; set; }
        public int OutputParameterinReminder { get; set; }

    }
    public class ManageAdmin
    {
        public int ManageSelectedEmp { get; set; }
        public string ManageRoleName { get; set; }
        public List<SelectListItem> ddlManageList { get; set; }
        public string SelectedEmployeeName { get; set; }
    }
   
}