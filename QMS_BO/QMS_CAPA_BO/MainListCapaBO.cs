﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace QMS_BO.QMS_CAPABO
{
   public class MainListCapaBO
    {
        [Key]
        public int CAPAID { get; set; }
        public int CurrentStatus { get; set; }
        public string QualityEvent_TypeID { get; set; }
        public string CAPA_Number { get; set; }
        public string Department { get; set; }
        public string EmployeeName { get; set; }
        public string TargetDate { get; set; }
        public string QualityEvent_Number { get; set; }
        public string Verified_YesOrNO { get; set; }
        public string CAPA_Date { get; set; }
        public string QualityEventName { get; set; }
        public string Capa_ActionDesc { get; set; }
        public string PlanofAction { get; set; }
        public string CurrentStatusName { get; set; }
        public string RevisedTargetDate { get; set; }
        public string QA_Remainder_Notes { get; set; }
        public string HOD_Justify_Notes { get; set; }
        public string ReminderStatusName { get; set; }
        public int RemainderStatus { get; set; }
        public Boolean IsRevisedDate { get; set; }
        public int Hod_EmpID { get; set; }
        public string Assigned_QA { get; set; }
        public int HeadQAEmpID { get; set; }
        public int QAEmpID { get; set; }
        public int EventBaseID { get; set; }//Quality Event ID's
        public string PraposedTargetDate1 { get; set; }
        public string AssignHODNameRM1 { get; set; }
        public int HodEmpIDRM1 { get; set; }
        //RM2
        public string HOD_Justify_NotesRM2 { get; set; }
    public string QA_Remainder_NotesRM2 { get; set; }
public string RevisedTargetDateRM2 { get; set; }
        public Boolean IsRevisedDateRM2 { get; set; }
        public int HodEmpIDRM2 { get; set; }
        public string AssignHODNameRM2 { get; set; }
        public int RemainderStatusRM2 { get; set; }
        public string ImplementorDateVerificationCAPA { get; set; }
        public string ReminderStatusNameRM2 { get; set; }
        public string  VerificationComments { get; set; }
        public string PraposedTargetDate2 { get; set; }

        //CAPA Close
        public string ClosureEffectiveDate { get; set; }
        public string ClosureComments { get; set; }
        public int EMPId { get; set; }

        public List<SelectListItem> ddlAssigneToHQA { get; set; }
        //public int ApprovalHQA { get; set; }
        public string HQAApprovalComments { get; set; }
        public string VerificationDueDate { get; set; }
        public string VerifiedDate { get; set; }
        public int CAPACreatedBy { get; set; }
        //For CAPA Report
        public string CAPACreatedByRpt { get; set; }
        public string CAPACreatedSignDateRpt { get; set; }

        public string QAApprovedByRpt{ get; set; }
        public string QAApprovedSignDateRpt { get; set; }
        public string HQAApprovedByRpt { get; set; }
        public string HQAApprovedSignDateRpt { get; set; }
        public string UserAcceptByRpt { get; set; }
        public string UserSignDateRpt { get; set; }
        public int ResponsibleEmpID { get; set; }
        public int VerificationID { get; set; }
        public Boolean ISCheckedFinalorNot { get; set; }
        public string GUID { get; set; }
    }
}
