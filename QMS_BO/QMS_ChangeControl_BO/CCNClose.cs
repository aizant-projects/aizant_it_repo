﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace QMS_BO.QMS_ChangeControl_BO
{
   public class CCNClose
    {
        [Key] public int CCNID { get; set; }
        public int RowNumber { get; set; }
        public int UniqueID { get; set; }
        public string CCN_Number { get; set; }
        public string ClosureDate { get; set; }
        public string Comments { get; set; }
        public string deptname { get; set; }
        public string EmpName { get; set; }
        public string QAName { get; set; }
        public int QAEMpID { get; set; }
        [Display(Name = "Browse File")]
        public HttpPostedFileBase[] CCNCloseFileUpload { get; set; }
        public byte[] CCNCloseAttachments { get; set; }
        public string FileTypeClose { get; set; }
        public string FileNameClose { get; set; }
        public int CCNFileUniqueIDClose { get; set; }
    }
}
