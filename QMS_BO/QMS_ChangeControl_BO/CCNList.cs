﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO.QMS_ChangeControl_BO
{
   public class CCNList
    {
        [Key] public int CCNID { get; set; }
        public int RowNumber { get; set; }
        public string CCN_Number { get; set; }
        public string CCN_Date { get; set; }
        public string Block { get; set; }
        public int BlockID { get; set; }
        public string Initiatedby { get; set; }
        public string Transaction { get; set; }
        public string Department { get; set; }       
        public string CurrentStatus { get; set; }
        public int AssessmentID { get; set; }
        public string Classification { get; set; }
        public string Due_Date { get; set; }
        public int CurrentStatunumber { get; set; }
    }
}
