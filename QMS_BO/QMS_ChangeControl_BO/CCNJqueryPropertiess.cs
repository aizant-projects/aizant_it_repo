﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO.QMS_ChangeControl_BO
{
   public class CCNJqueryPropertiess
    {
        public class CCNReportDetailsListResult
        {
            public int iDisplayLength { get; set; }
            public int iDisplayStart { get; set; }
            public int iSortCol_0 { get; set; }
            public string sSortDir_0 { get; set; }
            public string sSearch { get; set; }

            public string Role { get; set; }
            public int deptIdReport { get; set; }
            public string FromDateReport { get; set; }
            public string ToDateReport { get; set; }
            public int EmployeeID { get; set; }
            public string sSearch_CCN_Number { get; set; }
            public string sSearch_Department { get; set; }
            public string sSearch_Initiatedby { get; set; }
            public string sSearch_CCN_Date { get; set; }
            public string sSearch_Due_Date { get; set; }
            public string sSearch_Classification { get; set; }
            public string sSearch_CurrentStatus { get; set; }
        }
    }
}
