﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO.QMS_ChangeControl_BO
{
   public class BlockList
    {
        public int RowNumber { get; set; }
        public int BlockID { get; set; }
        public string BlockName { get; set; }
        public string CurrentStatus { get; set; }
    }
    public class AssesmentBlock_IDs
    {
        public int AssessmentID { get; set; }
        public int BlockID { get; set; }
    }
}
