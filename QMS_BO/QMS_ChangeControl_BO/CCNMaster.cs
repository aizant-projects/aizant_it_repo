﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace QMS_BO.QMS_ChangeControl_BO
{
    public class CCNMaster
    {
        [Key]
        public int CCN_ID { get; set; }
        public int hdnCCNIsTempReferalID { get; set; }
        public int DeptID { get; set; }
        public string DepartmentName { get; set; }
        public string CCN_Date { get; set; }
        public string DueDate { get; set; }
        public string CCN_No { get; set; }
        public int TC_ID { get; set; }
        public string TypeChange { get; set; }
        //public string Other_Desc { get; set; }
        public int DocumentType { get; set; }
       // public string Market_Region { get; set; }
        public int marketID { get; set; }
        public string MarketName { get; set; }
        //public string MarketCode { get; set; }       
        public string ItemCode { get; set; }
        public int ItemID { get; set; }
        public string ItemName { get; set; }
        //public string ProjectNo { get; set; } //testing
        //public int EmployeeName { get; set; }
        public string DocName { get; set; }
        //public string ReferenceNumberOthers { get; set; }
        public int CategoryofChange { get; set; }
        public string DescriptionofProposedChanges { get; set; }
        public string ReasonforChange { get; set; }
        public string JustificationforChange { get; set; }
        public string ExistingProcedure { get; set; }
        public int CurrentStatus { get; set; }
        public string ChangeClassification { get; set; }
        public string CategoryofChangeName { get; set; }
        public int Initiator_EmpID { get; set; }
        public int HOD_EmpID { get; set; }
        public int QA_EmpID { get; set; }
        public int HeadQA_EmpID { get; set; }
        //public string Initiator_SubmitedDate { get; set; }
        public string Initiator_Comments { get; set; }
        public string HOD_Comments { get; set; }
        public string QA_Comments { get; set; }
        public string HEADQA_Comments { get; set; }

        public int EMPID { get; set; }
        public string DocTitle { get; set; }
        //public string DocTitleEnter { get; set; }
        public string VersionID { get; set; }
        public int ProjectID { get; set; }
        //public string projectName { get; set; }
        public string ReferenceNumber { get; set; }
        //public string DocumentDetails { get; set; }
        //public string DocumentNo { get; set; }
        public string ProjectNumber { get; set; }
       
        public string DocumentTypeName { get; set; }

        

        public string Comments { get; set; }
        //public int UniqueID { get; set; }
        public int WhomeToRevert { get; set; }
        public int AssessmentStaus { get; set; }
        public int ActionAndImpactStatus { get; set; }
        public string HQA_RevertHodEmpName  { get; set; }
        //public int CCNCommitteTableStatus{ get; set; }

        //public string QACloserComments { get; set; }
       // public DateTime EffectiveDateCloserCCN { get; set; }

        public int AssessmentId { get; set; }

        public int AssessmentCompletedOrNotStatus { get; set; }
        public string DatabaseSubmitStatus { get; set; }
        [Display(Name = "Browse File")]
        public HttpPostedFileBase[] CCNhodFileUpload { get; set; }
       // public byte[] CCNhodAttachments { get; set; }
        public string FileTypeHod { get; set; }
        public string FileNameHod { get; set; }
        public int CCNHodFileUniqueID { get; set; }
        public int RowNumberHodAss { get; set; }


        //drop down List
        public List<SelectListItem> ddlDeptList { get; set; }
        public List<SelectListItem> ddlDocumentTypeList { get; set; }
        public List<SelectListItem> ddlMarketList { get; set; }
        public List<SelectListItem> ddlProjectList { get; set; }
        public List<SelectListItem> ddlTypeOfChangeList { get; set; }
        public List<SelectListItem> ddlItemList { get; set; }
        public List<SelectListItem> ddlAssignedQA { get; set; }
        public List<SelectListItem> ddlAssignedHeadQA { get; set; }
        public List<SelectListItem> ddlAssignedHOD { get; set; }
        //public List<SelectListItem> ddlAssignedInitator { get; set; }

        public string AdminMangeRoleName { get; set; }
       // public string ReassignComments { get; set; }
        public List<SelectListItem> ddlAdminManageRoles { get; set; }
        public int SelectedAssessmentHOD { get; set; }
        public string SelectedEmployee { get; set; }
        //Temporary fields
        public string ApplicableBatch { get; set; }
        public string ApplicableEquipment { get; set; }
        public string ApplicableTimeline { get; set; }

    }



}

