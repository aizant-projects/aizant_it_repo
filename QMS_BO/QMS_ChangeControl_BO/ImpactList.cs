﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace QMS_BO.QMS_ChangeControl_BO
{
    public class ImpactList
    {
        public List<SelectListItem> Fruits { get; set; }

        public IEnumerable<string> SelectedNames { get; set; }

    }
    public class ImpactPoints
    {
        public int CCN_ID { get; set; }
        public int ImpactPointID { get; set; }
        public string ImpactName { get; set; }
        public string isyesorNo { get; set; }
        public string DocNo { get; set; }
        public string Comments { get; set; }
        public string EmpoyeeNames { get; set; }
        public int ImpactPointStatus { get; set; }
        public int TransctionID { get; set; }
        public List<EmployeesList> Employees { get; set; }
        
    }
    public class EmployeesList
    {
        //[key]
        public int EmpID { get; set; }
        public string EmployeeName { get; set; }
        //  public string is { get; set; }
    }

    public class ImpactPointsModel
        {
            public List<ImpactPoints> StudentList { get; set; }
        }
        public class Student
        {
            //[key]
            public int ID { get; set; }
            public string EmployeeName { get; set; }
            //  public string is { get; set; }

        }
    }
