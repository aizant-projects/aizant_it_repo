﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace QMS_BO.QMS_ChangeControl_BO
{
   public class CCN_Verification
    {
        [Key] public int CCNID { get; set; }
        public int RowNumber { get; set; }
        public int UniqueID { get; set; }
        public string CCN_Number { get; set; }
        public string Implementation_Date { get; set; }
        public string Comments { get; set; }
        public string deptname { get; set; }
        public string EmpName { get; set; }
        public int ImplementaorID { get; set; }
        public string QAName { get; set; }
        public int QAEMpID { get; set; }
        [Display(Name = "Browse File")]
        public HttpPostedFileBase[] CCNFileUpload { get; set; }
        public byte[] CCNAttachments { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public int CCNFileUniqueID { get; set; }

    }
}
