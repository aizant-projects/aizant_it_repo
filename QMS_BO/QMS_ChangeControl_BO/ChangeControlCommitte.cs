﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace QMS_BO.QMS_ChangeControl_BO
{
    public class ChangeControlCommitte
    {
        [Key]
        public int RowNumber { get; set; }
        public int UniqueID { get; set; }
        public int CCN_ID { get; set; }
        public string DeptID { get; set; }
        public string DeptName { get; set; }

        public string BlockName { get; set; }
        public int BlockID { get; set; }
        public string EmployeeName { get; set; }
        public int EmpID { get; set; }
        public string CuurentStatus { get; set; }
        public string Comments { get; set; }
        //icon  Dispalyed in Assessment using currentStatusNumber
        public int CurrentStatusNumber { get; set; }
        public List<SelectListItem> ddlAssessmentHODs { get; set; }

    }
}
