﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO
{
   public class RolesListBinded
    {
        public int RoleIDNumber { get; set; }
        public int RoleCount { get; set; }
    }
}
