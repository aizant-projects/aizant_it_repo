﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace QMS_BO.QMS_Masters_BO
{
    public class ImpactPointList
    {
        public int RowNumber { get; set; }
        public int Imp_Desc_ID { get; set; }
        public string Impact_Description { get; set; }
        public string CurrentStatus { get; set; }
    }
    public class ImpactPointAndDeptLinkList
    {
        public int RowNumber { get; set; }
        public int ImpPoint_ID { get; set; }
        public string Impact_Description { get; set; }
        public string Department { get; set; }
        public string BlockName { get; set; }
        public string CurrentStatus { get; set; }
       
    }
   
   
}
