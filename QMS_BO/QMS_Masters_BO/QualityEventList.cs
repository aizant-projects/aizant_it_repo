﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO.QMS_Masters_BO
{
    public class QualityEventList
    {
        public int RowNumber { get; set; }
        public int QualityEvent_TypeID { get; set; }
        public string QualityEvent_TypeName { get; set; }
        public string CurrentStatus { get; set; }
    }
}
