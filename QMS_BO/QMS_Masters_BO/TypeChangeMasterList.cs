﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO.QMS_Masters_BO
{
   public class TypeChangeMasterList
    {
        public int RowNumber { get; set; }
        public int TC_ID { get; set; }
        public string TypeofChange { get; set; }
        public string CurrentStatus { get; set; }
    }
}
