﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO.QMS_Masters_BO
{
  public  class ActionPlanList
    {
        public int RowNumber { get; set; }
        public int Capa_ActionID { get; set; }
        public string Capa_ActionName { get; set; }
        public string CurrentStatus { get; set; }
    }
}
