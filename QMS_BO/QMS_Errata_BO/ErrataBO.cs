﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace QMS_BO.QMS_Errata_BO
{
   public class ErrataBO
    {
        [Key]
        public int ER_ID { get; set; }
        public string ErrataFileNo { get; set; }
        public int EmpID { get; set; }
        public int DeptID { get; set; }
        public string DeptName { get; set; }
        public string EmpName { get; set; }
        public string DocTitle { get; set; }
        //document type others, to use the DocTitle1
        public string DocTitle1 { get; set; }

        public string DocumentNo { get; set; } //Reference number
        public int DocumentType { get; set; }
        public string versionID { get; set; }
        public string DocumentTypeName { get; set; }
        public string SectionNo { get; set; }

        public string ProposedCorrectionDoc_Desc { get; set; }

        public int Initiator_EmpID { get; set; }

        public string Initiator_SubmitedDate { get; set; }
        public string ImpactofChange_Desc { get; set; }

        public string JustificationDesc { get; set; }
        public int TypeofCorrection { get; set; }
        public string TypeofChangeName { get; set; }
        public string Evaluation_Desc { get; set; }
        public int CurrentStatus { get; set; }
        public int HOD_EmpID { get; set; }
        public int HeadQA_EmpID { get; set; }

        public int QA_EmpID { get; set; }
        public string LatestCommentsErrata{ get; set; }
        public string initiator_Comments { get; set; }
        public string HOD_Comments { get; set; }
        public string QA_Comments { get; set; }
        public string HQA_Comments { get; set; }
        public int Rownumber { get; set; }
        public string Password { get; set; }
        public string Status { get; set; }
        public string Esign { get; set; }
        public string Javascript { get; set; }
        public int Select_RevertEmp { get; set; }
        public string OtherDocumnets { get; set; }
        public bool MasterDocument { get; set; }
        public bool SoftCopy { get; set; }
        public bool Other_DocCheck { get; set; }
        private  ErrataImpactPoints _Impoints1 =new ErrataImpactPoints();
        public  ErrataImpactPoints Impoints1
        {
            get { return _Impoints1; }
            set { _Impoints1 = value; }
        }
        private ErrataImpactPoints _Impoints2 = new ErrataImpactPoints();
        public ErrataImpactPoints Impoints2
        {
            get { return _Impoints2; }
            set { _Impoints2 = value; }
        }
        private ErrataImpactPoints _Impoints3 = new ErrataImpactPoints();
        public ErrataImpactPoints Impoints3
        {
            get { return _Impoints3; }
            set { _Impoints3 = value; }
        }
        private ErrataImpactPoints _Impoints4 = new ErrataImpactPoints();
        public ErrataImpactPoints Impoints4
        {
            get { return _Impoints4; }
            set { _Impoints4 = value; }
        }
        public int IpactPointsAvailable { get; set; }
        public String Hidden { get; set; }
        public string URL { get; set; }
        public string Action { get; set; }
        public string Page { get; set; }
     

        public string Error { get; set; }
        //Pass Roleid for Rject at Qa and Hod pages
        public int RoleId { get; set; }

        //Dropdown List for Errata Admin Manage
        public List<SelectListItem> DDLEmpList { set; get; }
        //RoleName and SelectDDLEmp properties are used for NTF Admin Manage Roles
        public string RoleName { set; get; }
        public int SelectDDLEmp { set; get; }
        public string SelectedEmployeeName { get; set; }
        public int Sno { set; get; }

        public string ReassignComments { get; set; }

        public int SelectEmployee { set; get; }
        public int ActionBy { set; get; }
       
        public List<SelectListItem> ddlIsYesorNo { set; get; }

        public List<SelectListItem> ddl_HeadQAEmpID { set; get; }

        public List<SelectListItem> ddl_QAEmpID { set; get; }
        //end
    }
    #region ErrataSingleTon
    //public class ErrataListSingleton
    //{
    //    private ErrataListSingleton()
    //    {

    //    }
    //    public static List<ErrataBO> ErrataBOInstanceList;
    //    public static List<ErrataBO> GetErrataInstanceList()
    //    {
    //        if (ErrataBOInstanceList == null)
    //            ErrataBOInstanceList = new List<ErrataBO>();
    //        return ErrataBOInstanceList;
    //    }
    //    //public List<ErrataBO> objErrataBOList { get; set; }
    //}
    #endregion
    public class ErrataAssessImpactPoints
    {
        public int ErrataID { get; set; }
        public int ImpactPointID { get; set; }
        public string ImpactName { get; set; }
        public string isyesorNo { get; set; }
        public string DocNo { get; set; }
        public string Comments { get; set; }
        public SelectList _lstImpactStatus { get; set; }
    }
    public class ErrataImpactPoints
    {
        public string ER_Filenumber { get; set; }
        public int ImpactPointID { get; set; }
        public string ImpactName { get; set; }
        public string isyesorNo { get; set; }
        public string DocNo { get; set; }
        public string Comments { get; set; }
    }
        
    

}
