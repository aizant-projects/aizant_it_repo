﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace QMS_BO.QMS_NoteTofile_BO
{
  public class NoteFileBO
    {
        public int Rownumber { get; set; }
        public int NF_ID { get; set; }
        public string NoteToFileNo { get; set; }
        public string CAPA_Number { get; set; }
        public string EmpName { get; set; }
        //RoleName and SelectDDLEmp properties are used for NTF Admin Manage Roles
        public string RoleName { get; set; }
        public int SelectDDLEmp { get; set; }
        public int DeptID { get; set; }
        public string DeptName { get; set; }
        public string DocCreateDate { get; set; }
        public int DocumentID { get; set; }
        public string versionID { get; set; }
        public string ReferenceNumber { get; set; }
        public string ReferenceNumberEnter { get; set; }
        public string NTF_Description { get; set; }        
        public string ProposedCAPA_Desc { get; set; }
        public DateTime PreparedDate { get; set; }
        public string IsCAPARequired { get; set; }
        public string capaRequiredorNot { get; set; }
        public string CAPA_RefNo { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string CAPA_Closer { get; set; }
        public int versionNumber { get; set; }

        //Employee ID
        public int EmpID { get; set; }
        public int HOD_EmpID { get; set; }
        public int Head_QA { get; set; }
        public int QA_EmpId { get; set; }
        public string PreparedBy { get; set; }


        public string CurrentStatus { get; set; }
        public string Status { get; set; }
        public int  DocumentTypeID { get; set; }       
        public string DocumentTypeName { get; set; }
        //Comments 
        public string HODComments { get; set; }
        public string QAComments { get; set; }
        public string HQAComments { get; set; }
        public string RevertComments { get; set; }
        public string InitiatorComments { get; set; }
        //CAPA model

        public int BaseID { get; set; }
        public string PlanofAction { get; set; }
        public string CapaComments { get; set; }
        public string TargetDate { get; set; }
        public string Responisble_Persons { get; set; }
        public int CAPAID { get; set; }
        public string CAPANumber { get; set; }
        public string QualityEvent_TypeID { get; set; }
        public string QualityEvent_Number { get; set; }
        public int number { get; set; }
        public int SelectEmployee { get; set; }
        public string DocTitle { get; set; }
        public string DocTitle1 { get; set; }
        public int CAPACurrentStatus { get; set; }
        //Electronic Sign
        public string Password { get; set; }
        public string Esign { get; set; }
        public string Javascript { get; set; }
        public string Electronic_Status { get; set; }
        public int CapaQaEmpId { get; set; }
        public int CapaRequeriedSubmitValidation { get; set; }
        //RoleChecking
        public int RoleIdChecking{ get; set; }
        //Assign RoleID
        public int RoleID { get; set; }
        public string CapaDescriptForUpdate { get; set; }
        public string LatestComments { get; set; }
        //For CAPA view
        public string AssignedQaName { get; set; }

        //Dropdown list for NTF Admin Manage
        public List<SelectListItem> DDLEmpList { set; get; }
        public List<SelectListItem> ddlHODManageNTF { set; get; }
        public List<SelectListItem> ddlQAManageNTF { set; get; }
        public List<SelectListItem> ddlHQAManageNTF { set; get; }

        public int ActionBy { set; get; }

        public int Sno { get; set; }
        public string ReassignComments { set; get; }
        public string SelectedEmployeeReAssign { get; set; }
    }
    public class BindListForMAnage
    {
        public string RoleName { get; set; }
        public List<SelectListItem> BindDropdownsForRoleWise { get; set; }
    }
    
   
}
