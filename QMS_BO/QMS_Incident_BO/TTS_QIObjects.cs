﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO.QMS_Incident_BO
{
    public class TTS_QIObjects
    {
        //for TTS_Master
        public DataSet dsTrainersAndTrainees { get; set; }
        public int Dept_ID { get; set; }
        public int Document_Type { get; set; }
        public int Document_ID { get; set; }
        public char TypeOFTraining { get; set; }
        public int AuthorEmpId { get; set; }
        public int ReviewEmpId { get; set; }
        public string Remarks { get; set; }
        public int TargetType { get; set; }
        public int MonthID { get; set; }
        public int Year { get; set; }
        public int TargetTS_ID { get; set; }
        public string DueDate { get; set; }

        public int IncidentID { get; set; }

    }
}
