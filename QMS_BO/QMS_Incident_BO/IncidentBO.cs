﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace QMS_BO.QMS_Incident_BO
{
    public class IncidentBO
    {

        [Key]
        public int SessionTTSStatus { get; set; }
        public int UniquePKID { get; set; }
        public int CountFiles { get; set; }
        public int TempRefID { get; set; }
        public int IncidentMasterID { get; set; }
        public int RowNumber { get; set; }
        public string IRNumber { get; set; }
        public string IncidentViewDeptName { get; set; }
        public string OccurrenceDate { get; set; }
        public string IncidentReportDate { get; set; }
        public string IncidentDueDate { get; set; }
        public int DeptID { get; set; }
        public int AssCapaID { get; set; }
        public int AssIncidentMasterID { get; set; }
        public string IncidentDesc { get; set; }
        public int TypeofIncidentID { get; set; }
        public string TypeofIncidentName { get; set; }
        public int IsAnyTrainingRequired { get; set; }
        public bool IsCAPARequired { get; set; }
        public string CurrentStatus { get; set; }
        public string CAPAClosureVerification_Desc { get; set; }
        public string QAClassComments { get; set; }
        public int InitiatorEmpID { get; set; }
        public int HODEmpID { get; set; }
        public int QAEmpID { get; set; }
        public int InvestigatorEmpID { get; set; }
        public int HeadQAEmpID { get; set; }
        public string Trasaction { get; set; }
        public int EmpID { get; set; }
        public string Comments { get; set; }
        public string QA_Comments { get; set; }
        public string InitiatorComments { get; set; }
        public string HeadQAComments { get; set; }
        public string ActionDate { get; set; }
        public int DocumentType { get; set; }
        public string ReferenceNumber { get; set; }
        public string DocName { get; set; }
        public string HasDocName { get; set; }
        public string DocumentName { get; set; }
        public string DocTitle { get; set; }
        public string DeptName { get; set; }
        public string DocumentNo { get; set; } //Reference number
        public string EmpName { get; set; }
        public string CreatedDate { get; set; }
        public string IsFirstOccurence { get; set; }
        public string AnyOtherInvestigations { get; set; }
        public string FO_Comments { get; set; }
        public string AOI_Comments { get; set; }
        public int CapaID { get; set; }
        public string CapaNumber { get; set; }
        public int CapaStatus { get; set; }
        public string CapaStatusName { get; set; }
        public int OtherIncidentID { get; set; }
        public string HODComments { get; set; }
        public string CurrentStatusName { get; set; }
        public string OtherDOCRefNo { get; set; }
        public string ApproveQA_Comments { get; set; }
        public int CountofPreAssmnt { get; set; }
        public int CurrentStatusID { get; set; }
        public int CountAnyotherInvtign { get; set; }
        public string WhatHappen { get; set; }
        public string WhenHappen { get; set; }
        public string WhereHappen { get; set; }
        public string QImpConclusion { get; set; }
        public string QNonImpConclusion { get; set; }
        public string IsHasIncidentAfftectedDoc { get; set; }
        public int HasAffectedDocumentID { get; set; }
        public string HasAffectedDocument { get; set; }
        public string HasAffectedDocRefNo { get; set; }
        public string HasAffectedPageNo { get; set; }
        public string HasAffOtherDOCRefNo { get; set; }
        public string HasAffDocTitle { get; set; }
        public int HasAffeDeptID { get; set; }
        public string HasDeptName { get; set; }
        public string HasAffectedComments { get; set; }
        public string AffectedComments { get; set; }
        public string HodReviewComments { get; set; }
        public string ImmediateCorrection { get; set; }
        public string HodCapaComments { get; set; }
        public string HodCapaRevertComments { get; set; }
        public string RootCauseDesc { get; set; }
        public string QAEvalComments { get; set; }
        public string EvaluationandDispostion { get; set; }
        public string InvestigationOperation { get; set; }
        public string HodSubmittedDate { get; set; }
        public string HodReviewStatus { get; set; }
        public string DeptHodReviewComments { get; set; }
        public string AssessmntIncStatus { get; set; }
        public int IstrainingExitsorNot { get; set; }
        public string HodRevertedComments { get; set; }
        public string QARevertedComments { get; set; }
        public string ApproveComments { get; set; }
        public string CurrentDateStatus { get; set; }
        public string IncidentReportDateEdit { get; set; }
        public int TypeofDocument { get; set; }
        public int CapaExitsVal { get; set; }
        public string TypeofDocumentName { get; set; }
        public string FileDocumentName { get; set; }
        public string FileType { get; set; }
        public string UploadComments { get; set; }
       
        public string HodName { get; set; }
        public string QAName { get; set; }
        public string RoleName { get; set; }
        public int IsAffectedDocumentID { get; set; }
        public int TypeofCategoryID { get; set; }
        public  string TypeofCategory { get; set; }
        //Added for HOD Role Exist or Not 
        public Boolean _IsRoleExists { get; set; }

        //Added for getting Map Quality Notification for Others Text box
        public string QualityEventOthers { get; set; }
        public string QualityEvent_TypeName { get; set; }
        public string EventNumbers { get; set; }
        //Added for getting IncidentCAPAID's in Map Incident
        public string Inci_CAPA_Numbers { get; set; }
        //Added for retrieving HOD_InvestigationNotes in Incident
        public string HOD_InvestigationNotes { get; set; }
        public int QualityEvent_TypeID { get; set; }
        public int EventBaseID { get; set; }//Events unique ID
        [Display(Name = "Browse File")]
        public HttpPostedFileBase[] files { get; set; }

        public List<SelectListItem> HasDeptIDList { get; set; }
       
        public List<SelectListItem> HasDoctypeList { get; set; }
        public List<SelectListItem> HasDocRefList { get; set; }

        public List<SelectListItem> CAPADeptIDList { get; set; }
        public List<SelectListItem> CAPAPlanofactionList { get; set; }
        public List<SelectListItem> CAPAQualityeventList { get; set; }

        public List<SelectListItem> CAPAEmpNameList { get; set; }
        public int TTSID { get; set; }
        public string TTSDocName { get; set; }
        public string TTSDept { get; set; }
        public string TTSTypeofTraining { get; set; }
        public string TTSAuthor { get; set; }
        public string TTSQA { get; set; }
        public string TTSDate { get; set; }
        public string TTSStatus { get; set; }

        //Dropdown List for Incident Admin Manage
        public List<SelectListItem> DDLEmpList { get; set; }
        //RoleName and SelectDDLEmp properties are used for NTF Admin Manage Roles
        //public string RoleName { set; get; }
        public int SelectDDLEmp { set; get; }
        public int Sno { set; get; }
        public string SelectedEmpReassign{ get; set; }

        public int SelectEmployee { set; get; }
        //For Assessment Dropdown binding
        public List<SelectListItem> ddlAssessmentHODs { get; set; }
        public int ApproveStatus { set; get; }
        public int ActionBy { set; get; }
        public string ReassignComments { set; get; }
        public int Type { set; get; }
       public string InvestigatorEmpName { set; get; }
        //end
        public Boolean MLIncidentStatus { get; set; }

        public List<SelectListItem> ddlDeptList { get; set; }
        public List<SelectListItem> ddlDocumentTypeList { get; set; }
        public List<SelectListItem> ddlCategoryTypeList { get; set; }
        public List<SelectListItem> ddlInvestigatorList { get; set; }
        public List<SelectListItem> ddlQaList { get; set; }
        public class IncidentImpactPoints
        {
            public int IncidentMasterID { get; set; }
            public int ImpactPointID { get; set; }
            public string ImpactName { get; set; }
            public string isyesorNo { get; set; }
            public string DocNo { get; set; }
            public string Comments { get; set; }
            public SelectList _lstImpactStatus { get; set; }
        }

        public class IncidentEffectedPoints
        {
            public int IncidentMasterID { get; set; }
            public int EffectTypeID { get; set; }
            public int EffectedID { get; set; }
            public string EffectedName { get; set; }
            public string isEffect { get; set; }
            public string ReferenceNo { get; set; }
            public string Comments { get; set; }
            public SelectList _lstHasAffectedStatus { get; set; }
        }

        

        public class IncidentWasEffectedPoints
        {
            public int IncidentMasterID { get; set; }
            public int EffectTypeID { get; set; }
            public int EffectedID { get; set; }
            public string EffectedName { get; set; }
            public string isEffect { get; set; }
            public string ReferenceNo { get; set; }
            public string Comments { get; set; }
            public SelectList _lstWasAffectedStatus { get; set; }
        }

        public class IncidentReviewPoints
        {
            public int IncidentMasterID { get; set; }
            public int ReviewID { get; set; }
            public string ReviewName { get; set; }
            public string isReview { get; set; }
            public string ReferenceNo { get; set; }
            public string Comments { get; set; }
            public SelectList _lstisReviewStatus { get; set; }
        }
        public class IncidentHodReviewList
        {
            public string HodName { get; set; }
            public string Department { get; set; }
            public string SubmittedDate { get; set; }
            public string Status { get; set; }
            public string Comments { get; set; }
        }
        public class GetEmpIDs
        {
            public int EmpIDs { get; set; }
        }
    }
}
