﻿using QMS_BO.QMS_ChangeControl_BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_BO
{

        public class ViewModel
        {
            public IEnumerable<ImpactPoints> ImpactPoints { get; set; }
            public IEnumerable<CCNMaster> CCNMaster { get; set; }
        }
   
}
