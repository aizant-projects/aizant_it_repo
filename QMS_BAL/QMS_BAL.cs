﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using QMS_DataLayer;
namespace QMS_BusinessLayer
{
    class QMS_BAL
    {
        QMS_DAL QMS_DAL;
        public DataTable GetUMSLIST(int iDisplayLength, int iDisplayStart, int iSortCol_0,
       string sSortDir_0, string sSearch, string OPeration, string DashBoard, string UnAssignRoles)
        {
            QMS_DAL = new QMS_DAL();
            return objUMS_DAL.GetUMSLIST(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, OPeration, DashBoard, UnAssignRoles);
        }
    }
}
