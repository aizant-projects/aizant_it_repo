﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS_BO
{
    public class VisitorRegisterBO
    {
        private int _SNo;

        public int SNo
        {
            get { return _SNo; }
            set { _SNo = value; }
        }

        private string _VisitorID;

        public string VisitorID
        {
            get { return _VisitorID; }
            set { _VisitorID = value; }
        }
        private string _FirstName;

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        private string _LastName;

        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

        private int _IDCardType;

        public int IDCardType
        {
            get { return _IDCardType; }
            set { _IDCardType = value; }
        }

        private string _IDCardNo;

        public string IDCardNo
        {
            get { return _IDCardNo; }
            set { _IDCardNo = value; }
        }


        private string _MobileNo;

        public string MobileNo
        {
            get { return _MobileNo; }
            set { _MobileNo = value; }

        }

        private string _Address;

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }


        private int _CountryID;

        public int CountryID
        {
            get { return _CountryID; }
            set { _CountryID = value; }
        }

        private int _StateID;

        public int StateID
        {
            get { return _StateID; }
            set { _StateID = value; }
        }

        private int _CityID;

        public int CityID
        {
            get { return _CityID; }
            set { _CityID = value; }
        }

        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }


        private string _Organization;

        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }

        private int _Purpose_of_VisitID;

        public int Purpose_of_VisitID
        {
            get { return _Purpose_of_VisitID; }
            set { _Purpose_of_VisitID = value; }
        }


        private string _Remarks;

        public string Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }


        private int _Department;

        public int Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private int _Whom_To_VisitID;

        public int Whom_To_VisitID
        {
            get { return _Whom_To_VisitID; }
            set { _Whom_To_VisitID = value; }
        }

        private string _InDateTime;

        public string InDateTime
        {
            get { return _InDateTime; }
            set { _InDateTime = value; }
        }

        private string _OutDateTime;

        public string OutDateTime
        {
            get { return _OutDateTime; }
            set { _OutDateTime = value; }
        }

        private string _Photo;

        public string Photo
        {
            get { return _Photo; }
            set { _Photo = value; }
        }

        private byte[] _PhotoFileData;

        public byte[] PhotoFileData
        {
            get { return _PhotoFileData; }
            set { _PhotoFileData = value; }
        }

        private string _VisitorIDCardNo;

        public string VisitorIDCardNo
        {
            get { return _VisitorIDCardNo; }
            set { _VisitorIDCardNo = value; }
        }

        private string _AccessLocation;

        public string AccessLocation
        {
            get { return _AccessLocation; }
            set { _AccessLocation = value; }
        }


        private string _AccompanyPerson;

        public string AccompanyPerson
        {
            get { return _AccompanyPerson; }
            set { _AccompanyPerson = value; }
        }

        private string _RoomNo;

        public string RoomNo
        {
            get { return _RoomNo; }
            set { _RoomNo = value; }
        }

        private int _NoofVisitors;

        public int NoofVisitors
        {
            get { return _NoofVisitors; }
            set { _NoofVisitors = value; }
        }


        private string _VechileNo;

        public string VechileNo
        {
            get { return _VechileNo; }
            set { _VechileNo = value; }
        }


        private string _Belongings;

        public string Belongings
        {
            get { return _Belongings; }
            set { _Belongings = value; }
        }

        private string _PlannedOutTime;

        public string PlannedOutTime
        {
            get { return _PlannedOutTime; }
            set { _PlannedOutTime = value; }
        }

        private int _CreatedBy;

        public int CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }

        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }


        private int _LastChangedBy;

        public int LastChangedBy
        {
            get { return _LastChangedBy; }
            set { _LastChangedBy = value; }
        }

        private string _LastChangedDate;

        public string LastChangedDate
        {
            get { return _LastChangedDate; }
            set { _LastChangedDate = value; }
        }

        private string _LastChangedComments;

        public string LastChangedComments
        {
            get { return _LastChangedComments; }
            set { _LastChangedComments = value; }
        }

        private string _IDProofTxt;

        public string IDProofTxt
        {
            get { return _IDProofTxt; }
            set { _IDProofTxt = value; }
        }

        private string _PurposeofVisitTxt;

        public string PurposeofVisitTxt
        {
            get { return _PurposeofVisitTxt; }
            set { _PurposeofVisitTxt = value; }
        }

        private string _AppointmentID;

        public string AppointmentID
        {
            get { return _AppointmentID; }
            set { _AppointmentID = value; }
        }

        private string _DeptName;
        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }

        private string _EmpName;

        public string EmpName
        {
            get { return _EmpName; }
            set { _EmpName = value; }
        }

        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private string _VisitorName;

        public string VisitorName
        {
            get { return _VisitorName; }
            set { _VisitorName = value; }
        }

        private int _Mode;
        public int Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }

        private int _FromMonth;
        public int FromMonth
        {
            get { return _FromMonth; }
            set { _FromMonth = value; }
        }

        private int _ToMonth;
        public int ToMonth
        {
            get { return _ToMonth; }
            set { _ToMonth = value; }
        }

        private int _FromYear;
        public int FromYear
        {
            get { return _FromYear; }
            set { _FromYear = value; }
        }

        private int _ToYear;
        public int ToYear
        {
            get { return _ToYear; }
            set { _ToYear = value; }
        }
    }
}
