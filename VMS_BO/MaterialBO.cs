﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS_BO
{
    public class MaterialBO
    {
        private int _Sno;

        public int Sno
        {
            get { return _Sno; }
            set { _Sno = value; }
        }
        private int _OutMaterialID;

        public int OutMaterialID
        {
            get { return _OutMaterialID; }
            set { _OutMaterialID = value; }
        }

        private string _MaterialType;

        public string MaterialType
        {
            get { return _MaterialType; }
            set { _MaterialType = value; }
        }

        private string _MaterialName;

        public string MaterialName
        {
            get { return _MaterialName; }
            set { _MaterialName = value; }
        }

        private string _DescriptionOfMaterial;

        public string DescriptionOfMaterial
        {
            get { return _DescriptionOfMaterial; }
            set { _DescriptionOfMaterial = value; }
        }

        private decimal _Quantity;

        public decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        private int _Department;

        public int Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private int _GivenBy;

        public int GivenBy
        {
            get { return _GivenBy; }
            set { _GivenBy = value; }
        }

        private string _VechileNo;

        public string VechileNo
        {
            get { return _VechileNo; }
            set { _VechileNo = value; }
        }

        private string _GPorDCPNo;

        public string GPorDCPNo
        {
            get { return _GPorDCPNo; }
            set { _GPorDCPNo = value; }
        }

        private string _NameOfParty;

        public string NameOfParty
        {
            get { return _NameOfParty; }
            set { _NameOfParty = value; }
        }

        private string _OutDateTime;

        public string OutDateTime
        {
            get { return _OutDateTime; }
            set { _OutDateTime = value; }
        }

        private string _InDateTime;

        public string InDateTime
        {
            get { return _InDateTime; }
            set { _InDateTime = value; }
        }

        private string _Material_ReturnDateAndTime;

        public string Material_ReturnDateAndTime
        {
            get { return _Material_ReturnDateAndTime; }
            set { _Material_ReturnDateAndTime = value; }
        }

        private decimal _Return_Quantity;

        public decimal Return_Quantity
        {
            get { return _Return_Quantity; }
            set { _Return_Quantity = value; }
        }

        private string _Remarks;

        public string Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }


        private int _CreatedBy;

        public int CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }

        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private int _LastChangedBy;

        public int LastChangedBy
        {
            get { return _LastChangedBy; }
            set { _LastChangedBy = value; }
        }

        private string _LastChangedDate;

        public string LastChangedDate
        {
            get { return _LastChangedDate; }
            set { _LastChangedDate = value; }
        }

        private string _LastChangedComments;

        public string LastChangedComments
        {
            get { return _LastChangedComments; }
            set { _LastChangedComments = value; }
        }

        private string _SupplierName;

        public string SupplierName
        {
            get { return _SupplierName; }
            set { _SupplierName = value; }
        }

        private string _DCP_InvoiceNo;

        public string DCP_InvoiceNo
        {
            get { return _DCP_InvoiceNo; }
            set { _DCP_InvoiceNo = value; }
        }

        private string _AssessableValue;

        public string AssessableValue
        {
            get { return _AssessableValue; }
            set { _AssessableValue = value; }
        }

        private int _ReceivedBy;

        public int ReceivedBy
        {
            get { return _ReceivedBy; }
            set { _ReceivedBy = value; }
        }

        private int _UnitsID;

        public int UnitsID
        {
            get { return _UnitsID; }
            set { _UnitsID = value; }
        }

        private string _DeptName;
        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }

        private string _EmpName;

        public string EmpName
        {
            get { return _EmpName; }
            set { _EmpName = value; }
        }

        private string _ExpectedReturnDate;

        public string ExpectedReturnDate
        {
            get { return _ExpectedReturnDate; }
            set { _ExpectedReturnDate = value; }
        }

        private int _ReceivedDepartment;

        public int ReceivedDepartment
        {
            get { return _ReceivedDepartment; }
            set { _ReceivedDepartment = value; }
        }
        private DataTable _MultiDepartment=null;

        public DataTable MultiDepartment
        {
            get { return _MultiDepartment; }
            set { _MultiDepartment = value; }
        }

        private int _EmpID;

        public int EmpID
        {
            get { return _EmpID; }
            set { _EmpID = value; }
        }

        private int _RoleType;

        public int RoleType
        {
            get { return _RoleType; }
            set { _RoleType = value; }
        }

        private int _TransStatus;

        public int TransStatus
        {
            get { return _TransStatus; }
            set { _TransStatus = value; }
        }

        private int _DeptID;

        public int DeptID
        {
            get { return _DeptID; }
            set { _DeptID = value; }
        }

        private int _UpdateMode;

        public int UpdateMode
        {
            get { return _UpdateMode; }
            set { _UpdateMode = value; }
        }

        private int _RowNumber;
        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private string _StatusName;
        public string StatusName
        {
            get { return _StatusName; }
            set { _StatusName = value; }
        }

        private string _StoreStatusName;
        public string StoreStatusName
        {
            get { return _StoreStatusName; }
            set { _StoreStatusName = value; }
        }

        private string _MQuantity;

        public string MQuantity
        {
            get { return _MQuantity; }
            set { _MQuantity = value; }
        }
        private string _ReturnMQuantity;
        public string ReturnMQuantity
        {
            get { return _ReturnMQuantity; }
            set { _ReturnMQuantity = value; }
        }

        private string _ActualReturnDate;
        public string ActualReturnDate
        {
            get { return _ActualReturnDate; }
            set { _ActualReturnDate = value; }
        }

        private int _MaterialMasterID;
        public int MaterialMasterID
        {
            get { return _MaterialMasterID; }
            set { _MaterialMasterID = value; }
        }

        private int _NotificationID;

        public int NotificationID
        {
            get { return _NotificationID; }
            set { _NotificationID = value; }
        }

    }
}
