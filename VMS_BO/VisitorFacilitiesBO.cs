﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS_BO
{
    public class VisitorFacilitiesBO
    {
        private int _FacilityRequestID;
        public int FacilityRequestID
        {
            get { return _FacilityRequestID; }
            set { _FacilityRequestID = value; }
        }

        private int _DeptID;
        public int DeptID
        {
            get { return _DeptID; }
            set { _DeptID = value; }
        }

        private int _EmpID;
        public int EmpID
        {
            get { return _EmpID; }
            set { _EmpID = value; }
        }

        private string _DateofRequest;
        public string DateofRequest
        {
            get { return _DateofRequest; }
            set { _DateofRequest = value; }
        }

        private string _ClientName;
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        private string _Initiatedby;
        public string Initiatedby
        {
            get { return _Initiatedby; }
            set { _Initiatedby = value; }
        }

        private string _DeptName;
        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }

        private int _NoofVisitors;
        public int NoofVisitors
        {
            get { return _NoofVisitors; }
            set { _NoofVisitors = value; }
        }

        private string _DateofVisit;
        public string DateofVisit
        {
            get { return _DateofVisit; }
            set { _DateofVisit = value; }
        }

        private string _TravelArrangement;
        public string TravelArrangement
        {
            get { return _TravelArrangement; }
            set { _TravelArrangement = value; }
        }

        private decimal _T_ECost;
        public decimal T_ECost
        {
            get { return _T_ECost; }
            set { _T_ECost = value; }
        }

        private decimal _T_ACost;
        public decimal T_ACost
        {
            get { return _T_ACost; }
            set { _T_ACost = value; }
        }

        private string _MSnacks;
        public string MSnacks
        {
            get { return _MSnacks; }
            set { _MSnacks = value; }
        }

        private decimal _MS_ECost;
        public decimal MS_ECost
        {
            get { return _MS_ECost; }
            set { _MS_ECost = value; }
        }

        private decimal _MS_ACost;
        public decimal MS_ACost
        {
            get { return _MS_ACost; }
            set { _MS_ACost = value; }
        }

        private string _AFSnacks;
        public string AFSnacks
        {
            get { return _AFSnacks; }
            set { _AFSnacks = value; }
        }

        private decimal _AF_ECost;
        public decimal AF_ECost
        {
            get { return _AF_ECost; }
            set { _AF_ECost = value; }
        }

        private decimal _AF_ACost;
        public decimal AF_ACost
        {
            get { return _AF_ACost; }
            set { _AF_ACost = value; }
        }

        private string _ESnacks;
        public string ESnacks
        {
            get { return _ESnacks; }
            set { _ESnacks = value; }
        }

        private decimal _ES_ECost;
        public decimal ES_ECost
        {
            get { return _ES_ECost; }
            set { _ES_ECost = value; }
        }

        private decimal _ES_ACost;
        public decimal ES_ACost
        {
            get { return _ES_ACost; }
            set { _ES_ACost = value; }
        }

        private decimal _TotalEstimatedCost;
        public decimal TotalEstimatedCost
        {
            get { return _TotalEstimatedCost; }
            set { _TotalEstimatedCost = value; }
        }

        private decimal _TotalActualCost;
        public decimal TotalActualCost
        {
            get { return _TotalActualCost; }
            set { _TotalActualCost = value; }
        }

        private string _Remarks;
        public string Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }

        private string _CreatedDate;
        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }


        private int _LastChangedBy;
        public int LastChangedBy
        {
            get { return _LastChangedBy; }
            set { _LastChangedBy = value; }
        }

        private string _LastChangedDate;
        public string LastChangedDate
        {
            get { return _LastChangedDate; }
            set { _LastChangedDate = value; }
        }

        private int _StatusID;
        public int StatusID
        {
            get { return _StatusID; }
            set { _StatusID = value; }
        }

        private int _RowNumber;
        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }
        private string _ApproveStatus;
        public string ApproveStatus
        {
            get { return _ApproveStatus; }
            set { _ApproveStatus = value; }
        }

        private int _ClientID;
        public int ClientID
        {
            get { return _ClientID; }
            set { _ClientID = value; }
        }

        private int _CreatedBy;
        public int CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }

        private string _Comments;
        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }

        private int _NotificationID;

        public int NotificationID
        {
            get { return _NotificationID; }
            set { _NotificationID = value; }
        }

        private int _Mode;
        public int Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }
    }
}
