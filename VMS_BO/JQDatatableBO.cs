﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS_BO
{
    public class JQDatatableBO
    {
        private int _iDisplayLength = 0;

        public int iDisplayLength
        {
            get { return _iDisplayLength; }
            set { _iDisplayLength = value; }
        }
        private int _iDisplayStart = 0;

        public int iDisplayStart
        {
            get { return _iDisplayStart; }
            set { _iDisplayStart = value; }
        }
        private int _iSortCol = 0;

        public int iSortCol
        {
            get { return _iSortCol; }
            set { _iSortCol = value; }
        }
        private string _sSortDir;

        public string sSortDir
        {
            get { return _sSortDir; }
            set { _sSortDir = value; }
        }
        private string _sSearch;

        public string sSearch
        {
            get { return _sSearch; }
            set { _sSearch = value; }
        }
        private string _OPeration;

        public string OPeration
        {
            get { return _OPeration; }
            set { _OPeration = value; }
        }
        private int _iMode = 0;

        public int iMode
        {
            get { return _iMode; }
            set { _iMode = value; }
        }
        private int _pkid = 0;

        public int pkid
        {
            get { return _pkid; }
            set { _pkid = value; }
        }
        private int _EmpID;
        public int EmpID
        {
            get { return _EmpID; }
            set { _EmpID = value; }
        }

        private string _FromDateTime;

        public string FromDateTime
        {
            get { return _FromDateTime; }
            set { _FromDateTime = value; }
        }

        private string _ToDateTime;

        public string ToDateTime
        {
            get { return _ToDateTime; }
            set { _ToDateTime = value; }
        }

        private int _Status = 0;

        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        private int _StoreStatus = 0;

        public int StoreStatus
        {
            get { return _StoreStatus; }
            set { _StoreStatus = value; }
        }
    }
}
