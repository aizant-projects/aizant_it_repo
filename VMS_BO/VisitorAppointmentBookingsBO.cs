﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS_BO
{
    public class VisitorAppointmentBookingsBO
    {
        private int _Appointment_SNO;

        public int Appointment_SNO
        {
            get { return _Appointment_SNO; }
            set { _Appointment_SNO = value; }
        }

        private string _Appointment_ID;

        public string Appointment_ID
        {
            get { return _Appointment_ID; }
            set { _Appointment_ID = value; }
        }

        private string _Appointment_LastName;

        public string Appointment_LastName
        {
            get { return _Appointment_LastName; }
            set { _Appointment_LastName = value; }
        }

        private string _Appointment_NAME;

        public string Appointment_NAME
        {
            get { return _Appointment_NAME; }
            set { _Appointment_NAME = value; }
        }

        private string _Appointment_MOBILE_NO;

        public string Appointment_MOBILE_NO
        {
            get { return _Appointment_MOBILE_NO; }
            set { _Appointment_MOBILE_NO = value; }
        }

        private string _Appointment_EMAILID;

        public string Appointment_EMAILID
        {
            get { return _Appointment_EMAILID; }
            set { _Appointment_EMAILID = value; }
        }

        private string _Appointment_ORGANISATION;

        public string Appointment_ORGANISATION
        {
            get { return _Appointment_ORGANISATION; }
            set { _Appointment_ORGANISATION = value; }
        }

        private string _Appointment_DATE;

        public string Appointment_DATE
        {
            get { return _Appointment_DATE; }
            set { _Appointment_DATE = value; }
        }

        private string _Appointment_TIME;

        public string Appointment_TIME
        {
            get { return _Appointment_TIME; }
            set { _Appointment_TIME = value; }
        }

        private int _Appointment_DEPARTMENT_TOVISIT;

        public int Appointment_DEPARTMENT_TOVISIT
        {
            get { return _Appointment_DEPARTMENT_TOVISIT; }
            set { _Appointment_DEPARTMENT_TOVISIT = value; }
        }

        private int _Appointment_WHOMTO_VISIT;

        public int Appointment_WHOMTO_VISIT
        {
            get { return _Appointment_WHOMTO_VISIT; }
            set { _Appointment_WHOMTO_VISIT = value; }
        }

        private int _Appointment_PURPOSEOF_VISIT;

        public int Appointment_PURPOSEOF_VISIT
        {
            get { return _Appointment_PURPOSEOF_VISIT; }
            set { _Appointment_PURPOSEOF_VISIT = value; }
        }

        private string _Appointment_REMARKS;

        public string Appointment_REMARKS
        {
            get { return _Appointment_REMARKS; }
            set { _Appointment_REMARKS = value; }
        }

        private string _Appointment_VISITOR_ID;

        public string Appointment_VISITOR_ID
        {
            get { return _Appointment_VISITOR_ID; }
            set { _Appointment_VISITOR_ID = value; }
        }

        private int _Appointment_Status;

        public int Appointment_Status
        {
            get { return _Appointment_Status; }
            set { _Appointment_Status = value; }
        }


        private int _CreatedBy;
        public int CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }

        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private int _LastChangedBy;

        public int LastChangedBy
        {
            get { return _LastChangedBy; }
            set { _LastChangedBy = value; }
        }

        private string _LastChangedDate;

        public string LastChangedDate
        {
            get { return _LastChangedDate; }
            set { _LastChangedDate = value; }
        }

        private string _LastChangedComments;

        public string LastChangedComments
        {
            get { return _LastChangedComments; }
            set { _LastChangedComments = value; }
        }

        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private string _PurposeofVisitTxt;

        public string PurposeofVisitTxt
        {
            get { return _PurposeofVisitTxt; }
            set { _PurposeofVisitTxt = value; }
        }

        private string _DeptName;
        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }

        private string _EmpName;

        public string EmpName
        {
            get { return _EmpName; }
            set { _EmpName = value; }
        }

        private string _AppointmentStatusName; 

        public string AppointmentStatusName
        {
            get { return _AppointmentStatusName; }
            set { _AppointmentStatusName = value; }
        }
    }
}
