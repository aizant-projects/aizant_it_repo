﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS_BO
{
    public class SupplyRegisterBO
    {
        private int _SNo;

        public int SNo
        {
            get { return _SNo; }
            set { _SNo = value; }
        }

        private string _EmployeeID;

        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }

        private string _EmployeeName;

        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }

        private string _MobileNo;

        public string MobileNo
        {
            get { return _MobileNo; }
            set { _MobileNo = value; }

        }

        private string _Remarks;

        public string Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }


        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _IssuedItems;

        public string IssuedItems
        {
            get { return _IssuedItems; }
            set { _IssuedItems = value; }
        }

        private decimal _ItemCost;

        public decimal ItemCost
        {
            get { return _ItemCost; }
            set { _ItemCost = value; }
        }

        private string _Purpose;

        public string Purpose
        {
            get { return _Purpose; }
            set { _Purpose = value; }
        }

        private string _DepartmentArea;

        public string DepartmentArea
        {
            get { return _DepartmentArea; }
            set { _DepartmentArea = value; }
        }

        private int _NoofKeys;

        public int NoofKeys
        {
            get { return _NoofKeys; }
            set { _NoofKeys = value; }
        }

        private string _InDateTime;

        public string InDateTime
        {
            get { return _InDateTime; }
            set { _InDateTime = value; }
        }

        private string _ReturnDateTime;

        public string ReturnDateTime
        {
            get { return _ReturnDateTime; }
            set { _ReturnDateTime = value; }
        }


        private string _AccessCardNo;

        public string AccessCardNo
        {
            get { return _AccessCardNo; }
            set { _AccessCardNo = value; }
        }

        private int _CreatedBy;

        public int CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }

        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private int _LastChangedBy;

        public int LastChangedBy
        {
            get { return _LastChangedBy; }
            set { _LastChangedBy = value; }
        }

        private string _LastChangedDate;

        public string LastChangedDate
        {
            get { return _LastChangedDate; }
            set { _LastChangedDate = value; }
        }

        private string _LastChangedComments;

        public string LastChangedComments
        {
            get { return _LastChangedComments; }
            set { _LastChangedComments = value; }
        }

        private int _ApprovedBy;

        public int ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }

        private string _ApprovedOn;

        public string ApprovedOn
        {
            get { return _ApprovedOn; }
            set { _ApprovedOn = value; }
        }

    }
}
