﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS_BO
{
    public class ServiceAndVechileBO
    {
        private int _SNo;

        public int SNo
        {
            get { return _SNo; }
            set { _SNo = value; }
        }

        private string _FirstName;

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        private string _LastName;

        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

        private string _MobileNo;

        public string MobileNo
        {
            get { return _MobileNo; }
            set { _MobileNo = value; }
        }

        private string _Purpose_of_Visit;

        public string Purpose_of_Visit
        {
            get { return _Purpose_of_Visit; }
            set { _Purpose_of_Visit = value; }
        }


        private string _Remarks;

        public string Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }


        private int _Department;

        public int Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private int _Whom_To_Visit;

        public int Whom_To_Visit
        {
            get { return _Whom_To_Visit; }
            set { _Whom_To_Visit = value; }
        }

        private string _InDateTime;

        public string InDateTime
        {
            get { return _InDateTime; }
            set { _InDateTime = value; }
        }

        private string _OutDateTime;

        public string OutDateTime
        {
            get { return _OutDateTime; }
            set { _OutDateTime = value; }
        }


        private string _VechileNo;

        public string VechileNo
        {
            get { return _VechileNo; }
            set { _VechileNo = value; }
        }

        private int _CreatedBy;

        public int CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }

        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }


        private int _LastChangedBy;

        public int LastChangedBy
        {
            get { return _LastChangedBy; }
            set { _LastChangedBy = value; }
        }

        private string _LastChangedDate;

        public string LastChangedDate
        {
            get { return _LastChangedDate; }
            set { _LastChangedDate = value; }
        }

        private string _LastChangedComments;

        public string LastChangedComments
        {
            get { return _LastChangedComments; }
            set { _LastChangedComments = value; }
        }


        private string _ServiceType;

        public string ServiceType
        {
            get { return _ServiceType; }
            set { _ServiceType = value; }
        }


        private string _CommingFrom;

        public string CommingFrom
        {
            get { return _CommingFrom; }
            set { _CommingFrom = value; }
        }

        private string _DriverName;

        public string DriverName
        {
            get { return _DriverName; }
            set { _DriverName = value; }
        }


        private int _DriverID;

        public int DriverID
        {
            get { return _DriverID; }
            set { _DriverID = value; }
        }

        private string _PlaceVisited;

        public string PlaceVisited
        {
            get { return _PlaceVisited; }
            set { _PlaceVisited = value; }
        }

        private Int64 _OutReading;

        public Int64 OutReading
        {
            get { return _OutReading; }
            set { _OutReading = value; }
        }

        private Int64 _InReading;

        public Int64 InReading
        {
            get { return _InReading; }
            set { _InReading = value; }
        }

        private Int64 _TotalKiloMeters;

        public Int64 TotalKiloMeters
        {
            get { return _TotalKiloMeters; }
            set { _TotalKiloMeters = value; }
        }

        private Int64 _DistanceKMS;

        public Int64 DistanceKMS
        {
            get { return _DistanceKMS; }
            set { _DistanceKMS = value; }
        }

        private string _ServiceTypeTxt;

        public string ServiceTypeTxt
        {
            get { return _ServiceTypeTxt; }
            set { _ServiceTypeTxt = value; }
        }

        private int _VechileID;

        public int VechileID
        {
            get { return _VechileID; }
            set { _VechileID = value; }
        }

        private int _VechileMake;

        public int VechileMake
        {
            get { return _VechileMake; }
            set { _VechileMake = value; }
        }

        private string _VechileType;

        public string VechileType
        {
            get { return _VechileType; }
            set { _VechileType = value; }
        }

        private string _VechileModel;

        public string VechileModel
        {
            get { return _VechileModel; }
            set { _VechileModel = value; }
        }

        private string _VechileRegistrationNo;

        public string VechileRegistrationNo
        {
            get { return _VechileRegistrationNo; }
            set { _VechileRegistrationNo = value; }
        }

        private string _FuelType;

        public string FuelType
        {
            get { return _FuelType; }
            set { _FuelType = value; }
        }

        private decimal _FuelCapacity;

        public decimal FuelCapacity
        {
            get { return _FuelCapacity; }
            set { _FuelCapacity = value; }
        }

        private decimal _FuelConsumptionPerLiter;

        public decimal FuelConsumptionPerLiter
        {
            get { return _FuelConsumptionPerLiter; }
            set { _FuelConsumptionPerLiter = value; }
        }

        private string _EngineNo;

        public string EngineNo
        {
            get { return _EngineNo; }
            set { _EngineNo = value; }
        }

        private int _VechileInchargeID;

        public int VechileInchargeID
        {
            get { return _VechileInchargeID; }
            set { _VechileInchargeID = value; }
        }

        private int _Seater;

        public int Seater
        {
            get { return _Seater; }
            set { _Seater = value; }
        }

        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

    }
}
