﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS_BO
{
    public class RolePrivilegesBO
    {
        private int _PrivilegeID;

        public int PrivilegeID
        {
            get { return _PrivilegeID; }
            set { _PrivilegeID = value; }
        }

        private int _RoleID;

        public int RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }

        private string _PrivilegeName;

        public string PrivilegeName
        {
            get { return _PrivilegeName; }
            set { _PrivilegeName = value; }
        }
    }
}
