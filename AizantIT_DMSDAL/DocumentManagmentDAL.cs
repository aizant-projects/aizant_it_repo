﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using AizantIT_DMSBO;
using System.Collections;

namespace AizantIT_DMSDAL
{
    public class DocumentManagmentDAL
    {

        string AizantIT_DMS_Con = ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ConnectionString;

        #region dropdownFilling
        public DataTable DMS_GetDocumentNameList(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentNameList]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;

                DMS_Cmd.Parameters.AddWithValue("@DMS_DeptName", Convert.ToInt32(doc.dept));
                DMS_Cmd.Parameters.AddWithValue("@DMS_DocType", Convert.ToInt32(doc.DocumentType));
                DMS_Cmd.Parameters.AddWithValue("@DMS_Type", doc.RType);

                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetDepartmentList()//Department Filling
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[UMS].[AizantIT_SP_GetDepartments]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetFormNumberList(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_FormNumberList]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DeptID", doc.DMS_Deparment);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetDocRevertedByList(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetRevertedByList]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@vid", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@DocStatus", doc.DocStatus);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion dropdownFilling

        #region jquerydatatable
        public DataTable DMS_GetReviewDocuments(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            string query = "";
            ArrayList arr = new ArrayList();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                if (_objJQDataTableBO.DocStatus == "R")
                {
                    query = "[DMS].[AizantIT_SP_Reviewer1DocList]";
                }
                else if (_objJQDataTableBO.DocStatus == "A")
                {
                    query = "[DMS].[AizantIT_SP_Approver1DocList]";
                }
                else if (_objJQDataTableBO.DocStatus == "Au")
                {
                    query = "[DMS].[AizantIT_SP_Author1DocList]";
                }
                SqlCommand DMS_Cmd = new SqlCommand(query, DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@EmpID", _objJQDataTableBO.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_InitiatedBy", _objJQDataTableBO.sSearchInitiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_CreatedBy", _objJQDataTableBO.sSearchCreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestType", _objJQDataTableBO.sSearchRequestType);

                DMS_Cmd.Parameters.AddWithValue("@Rtyp", _objJQDataTableBO.RType);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion jquerydatatable

        #region getComments/time
        public DataTable DMS_GetComments(string DocumentID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetComments]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocumentID", Convert.ToInt32(DocumentID));
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public int DMS_SaveReviewComments(ReviewObjects reviewObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_SaveCommentsByReviewer]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@Comments", reviewObjects.Comments);
                DMS_Cmd.Parameters.AddWithValue("@type", reviewObjects.DocStatus);
                DMS_Cmd.Parameters.AddWithValue("@VersionID", reviewObjects.Id);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedBy", reviewObjects.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedDate", DateTime.Now);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedTime", reviewObjects.ReviewedTime);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();


            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_UpdateReviewedTimeToReviewer(ReviewObjects reviewObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_UpdateReviewedTimeByReviewer]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@ReviewedTime", reviewObjects.ReviewedTime);
                DMS_Cmd.Parameters.AddWithValue("@type", reviewObjects.DocStatus);
                DMS_Cmd.Parameters.AddWithValue("@VersionID", reviewObjects.Id);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedBy", reviewObjects.EmpID);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public DataTable DMS_GetPreviousDocumentStatus(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetPreviousDocumentStatus]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@Status", doc.DocStatus);
                DMS_Cmd.Parameters.AddWithValue("@DocVID", doc.Version);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #region GetFormComment
        public DataTable DMS_GetLatestFormComment(string FormVersionID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetLatestFormComment]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", FormVersionID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion GetFormComment
        #endregion getComments/time

        #region DocumentActions
        public int DMS_UpdateReviewstaus(ReviewObjects reviewObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {

                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_UpdateStatusprocesstype]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", reviewObjects.Id);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedBy", reviewObjects.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@type", reviewObjects.DocStatus);
                SqlParameter Status = DMS_Cmd.Parameters.Add("@status", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;

                SqlDataAdapter da = new SqlDataAdapter(DMS_Cmd);

                DMS_Cmd.ExecuteNonQuery();
                string Result = DMS_Cmd.Parameters["@status"].Value.ToString();

                DMS_Return = Convert.ToInt32(Result);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_ApproveDocByReviewer(ReviewObjects reviewObjects,ref SqlTransaction _Transaction,ref SqlConnection DMS_Con)
        {
           DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                _Transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ApproveDocByReviewer]", DMS_Con);
                DMS_Cmd.Transaction = _Transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@process", Convert.ToInt32(reviewObjects.Process));
                DMS_Cmd.Parameters.AddWithValue("@type", reviewObjects.DocStatus);
                DMS_Cmd.Parameters.AddWithValue("@VersionID", reviewObjects.Id);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedBy", reviewObjects.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedDate", DateTime.Now);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedTime", reviewObjects.ReviewedTime);
                DMS_Cmd.Parameters.AddWithValue("@Comments", reviewObjects.Comments);
                DMS_Cmd.Parameters.Add("@IsActive", SqlDbType.Int);
                DMS_Cmd.Parameters["@IsActive"].Direction = ParameterDirection.Output;
                DMS_Cmd.ExecuteNonQuery();
                if (Convert.ToInt32(DMS_Cmd.Parameters["@IsActive"].Value) == 1)
                {
                    DMS_Return = Convert.ToInt32(DMS_Cmd.Parameters["@IsActive"].Value);
                }

                SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_UpdateDocumentStatus]", DMS_Con);
                DMS_Cmd1.Transaction = _Transaction;
                DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                DMS_Cmd1.Parameters.AddWithValue("@type", reviewObjects.DocStatus);
                DMS_Cmd1.Parameters.AddWithValue("@process", Convert.ToInt32(reviewObjects.Process));
                DMS_Cmd1.Parameters.AddWithValue("@VersionID", reviewObjects.Id);
                DMS_Cmd1.Parameters.AddWithValue("@ReviewedBy", reviewObjects.EmpID);
                DMS_Cmd1.Parameters.AddWithValue("@ReviewedDate", DateTime.Now);
                DMS_Cmd1.Parameters.AddWithValue("@ReviewedTime", reviewObjects.ReviewedTime);
                DMS_Cmd1.Parameters.AddWithValue("@Comments", reviewObjects.Comments);
                DMS_Cmd1.Parameters.Add("@IsActive", SqlDbType.Int);
                DMS_Cmd1.Parameters["@IsActive"].Direction = ParameterDirection.Output;
                DMS_Cmd1.ExecuteNonQuery();
                if (Convert.ToInt32(DMS_Cmd1.Parameters["@IsActive"].Value) == 1)
                {
                    DMS_Return = Convert.ToInt32(DMS_Cmd1.Parameters["@IsActive"].Value);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
            }
            return DMS_Return;
        }
        public int DMS_ApproveDocByReviewer(ReviewObjects reviewObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ApproveDocByReviewer]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@process", Convert.ToInt32(reviewObjects.Process));
                DMS_Cmd.Parameters.AddWithValue("@type", reviewObjects.DocStatus);
                DMS_Cmd.Parameters.AddWithValue("@VersionID", reviewObjects.Id);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedBy", reviewObjects.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedDate", DateTime.Now);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedTime", reviewObjects.ReviewedTime);
                DMS_Cmd.Parameters.AddWithValue("@Comments", reviewObjects.Comments);
                DMS_Cmd.Parameters.Add("@IsActive", SqlDbType.Int);
                DMS_Cmd.Parameters["@IsActive"].Direction = ParameterDirection.Output;
                DMS_Cmd.ExecuteNonQuery();
                if (Convert.ToInt32(DMS_Cmd.Parameters["@IsActive"].Value) == 1)
                {
                    DMS_Return = Convert.ToInt32(DMS_Cmd.Parameters["@IsActive"].Value);
                }
                
                SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_UpdateDocumentStatus]", DMS_Con);
                DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                DMS_Cmd1.Parameters.AddWithValue("@type", reviewObjects.DocStatus);
                DMS_Cmd1.Parameters.AddWithValue("@process", Convert.ToInt32(reviewObjects.Process));
                DMS_Cmd1.Parameters.AddWithValue("@VersionID", reviewObjects.Id);
                DMS_Cmd1.Parameters.AddWithValue("@ReviewedBy", reviewObjects.EmpID);
                DMS_Cmd1.Parameters.AddWithValue("@ReviewedDate", DateTime.Now);
                DMS_Cmd1.Parameters.AddWithValue("@ReviewedTime", reviewObjects.ReviewedTime);
                DMS_Cmd1.Parameters.AddWithValue("@Comments", reviewObjects.Comments);
                DMS_Cmd1.Parameters.Add("@IsActive", SqlDbType.Int);
                DMS_Cmd1.Parameters["@IsActive"].Direction = ParameterDirection.Output;
                DMS_Cmd1.ExecuteNonQuery();
                if (Convert.ToInt32(DMS_Cmd1.Parameters["@IsActive"].Value) == 1)
                {
                    DMS_Return = Convert.ToInt32(DMS_Cmd1.Parameters["@IsActive"].Value);
                }
                
            }
            catch (Exception e)
            {
                
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_UpdateCommetaToReviewer(DocUploadBO reviewObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                if (reviewObjects.docLocation != null)
                {
                    SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_InsertDocumentLocation]", DMS_Con);
                    //DMS_Cmd.CommandTimeout = 120;
                    DMS_Cmd.Transaction = transaction;
                    DMS_Cmd.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd.Parameters.AddWithValue("@DocVersionID", reviewObjects.docLocation.Version);
                    DMS_Cmd.Parameters.AddWithValue("@RoleID", reviewObjects.docLocation.RoleID);
                    DMS_Cmd.Parameters.AddWithValue("@EmpID", reviewObjects.docLocation.EmpID);
                    DMS_Cmd.Parameters.AddWithValue("@FilePath1", reviewObjects.docLocation.FilePath1);
                    DMS_Cmd.Parameters.AddWithValue("@FilePath2", reviewObjects.docLocation.FilePath2);
                    DMS_Cmd.Parameters.AddWithValue("@isTemp", reviewObjects.docLocation.isTemp);
                    DMS_Cmd.Parameters.AddWithValue("@FileType1", reviewObjects.docLocation.FileType1);
                    DMS_Cmd.Parameters.AddWithValue("@FileType2", reviewObjects.docLocation.FileType2);
                    DMS_Cmd.Parameters.AddWithValue("@FileName1", reviewObjects.docLocation.FileName1);
                    DMS_Cmd.Parameters.AddWithValue("@FileName2", reviewObjects.docLocation.FileName2);
                    DMS_Return = DMS_Cmd.ExecuteNonQuery();
                }

                if (reviewObjects.docContent != null)
                {
                    SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_SaveContent]", DMS_Con);
                    DMS_Cmd1.Transaction = transaction;
                    DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd1.Parameters.AddWithValue("@VersionID", reviewObjects.docContent.Id);
                    //DMS_Cmd.Parameters.AddWithValue("@Content", reviewObjects.Content);
                    DMS_Cmd1.Parameters.AddWithValue("@FileType", reviewObjects.docContent.Ext);
                    DMS_Cmd1.Parameters.AddWithValue("@FileName", reviewObjects.docContent.TemplateName);
                    DMS_Return = DMS_Cmd1.ExecuteNonQuery();
                }

                if (reviewObjects.docComments != null)
                {
                    SqlCommand DMS_Cmd2 = new SqlCommand("[DMS].[AizantIT_SP_DocComments]", DMS_Con);
                    //DMS_Cmd.CommandTimeout = 120;
                    DMS_Cmd2.Transaction = transaction;
                    DMS_Cmd2.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd2.Parameters.AddWithValue("@DocVID", reviewObjects.docComments.Version);
                    DMS_Cmd2.Parameters.AddWithValue("@EmpID", reviewObjects.docComments.EmpID);
                    DMS_Cmd2.Parameters.AddWithValue("@RoleID", reviewObjects.docComments.RoleID);
                    DMS_Cmd2.Parameters.AddWithValue("@Comments", reviewObjects.docComments.Remarks);
                    DMS_Cmd2.Parameters.AddWithValue("@CommentDate", DateTime.Now);
                    DMS_Cmd2.Parameters.AddWithValue("@Time", reviewObjects.docComments.Time);
                    DMS_Cmd2.Parameters.AddWithValue("@ActionStatusID", reviewObjects.docComments.action);
                    DMS_Cmd2.Parameters.AddWithValue("@CommentType", reviewObjects.docComments.CommentType);
                    SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd2);
                    DMS_da.Fill(DMS_Dt);
                }

                if (reviewObjects.docReviwer != null)
                {
                    SqlCommand DMS_Cmd3 = new SqlCommand("[DMS].[AizantIT_SP_UpdateCommentsByReviewer]", DMS_Con);
                    DMS_Cmd3.Transaction = transaction;
                    DMS_Cmd3.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd3.Parameters.AddWithValue("@Comments", reviewObjects.docReviwer.Comments);
                    DMS_Cmd3.Parameters.AddWithValue("@type", reviewObjects.docReviwer.DocStatus);
                    DMS_Cmd3.Parameters.AddWithValue("@process", Convert.ToInt32(reviewObjects.docReviwer.Process));
                    DMS_Cmd3.Parameters.AddWithValue("@VersionID", reviewObjects.docReviwer.Id);
                    DMS_Cmd3.Parameters.AddWithValue("@ReviewedBy", reviewObjects.docReviwer.EmpID);
                    DMS_Cmd3.Parameters.AddWithValue("@ReviewedDate", DateTime.Now);
                    DMS_Cmd3.Parameters.AddWithValue("@ReviewedTime", reviewObjects.docReviwer.ReviewedTime);
                    DMS_Cmd3.Parameters.AddWithValue("@isUpload", reviewObjects.docReviwer.Status);//Status used for isUpload values-0,1
                    DMS_Return = DMS_Cmd3.ExecuteNonQuery();
                }

                if (reviewObjects.docStatus != null)
                {
                    SqlCommand DMS_Cmd4 = new SqlCommand("[DMS].[AizantIT_SP_UpdateDocumentStatus]", DMS_Con);
                    DMS_Cmd4.Transaction = transaction;
                    DMS_Cmd4.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd4.Parameters.AddWithValue("@Comments", reviewObjects.docStatus.Comments);
                    DMS_Cmd4.Parameters.AddWithValue("@type", reviewObjects.docStatus.DocStatus);
                    DMS_Cmd4.Parameters.AddWithValue("@process", Convert.ToInt32(reviewObjects.docStatus.Process));
                    DMS_Cmd4.Parameters.AddWithValue("@VersionID", reviewObjects.docStatus.Id);
                    DMS_Cmd4.Parameters.AddWithValue("@ReviewedBy", reviewObjects.docStatus.EmpID);
                    DMS_Cmd4.Parameters.AddWithValue("@ReviewedDate", DateTime.Now);
                    DMS_Cmd4.Parameters.AddWithValue("@ReviewedTime", reviewObjects.docStatus.ReviewedTime);
                    DMS_Cmd4.Parameters.Add("@IsActive", SqlDbType.Int);
                    DMS_Cmd4.Parameters["@IsActive"].Direction = ParameterDirection.Output;
                    DMS_Cmd4.ExecuteNonQuery();
                    if (Convert.ToInt32(DMS_Cmd4.Parameters["@IsActive"].Value) == 1)
                    {
                        DMS_Return = Convert.ToInt32(DMS_Cmd4.Parameters["@IsActive"].Value);
                    }
                }
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                DMS_Return = 0;
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #region DocRejectByAdmin
        public int DMS_RejectDocByAdmin(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_RejectDocumentByAdmin]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVersionID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@ApprovedBy", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@comments", doc.Remarks);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion DocRejectByAdmin
        #endregion DocumentActions

        #region UpdateDocPrivacy
        public int DMS_UpdateDocPrivacyLevel(DocObjects doc, bool isPublic)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_UpdateDocPrivacy]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@versionID", Convert.ToInt32(doc.DocumentID));
                DMS_Cmd.Parameters.AddWithValue("@isPublic", isPublic);
                DMS_Cmd.Parameters.AddWithValue("@IntiatedBy", doc.DMS_IntiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@IntiatedDate", DateTime.Now);
                DMS_Cmd.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion UpdateDocPrivacy

        #region EmpModuleDepartmentFilling 
        public DataTable DMS_EmpModuleDepartmentFilling(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetEmpModuleDepartments]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@RoleID", doc.RoleID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion EmpModuleDepartmentFilling

        #region DocRequestActions
        #region DocPrintReqApproveByReviewer
        public int DMS_DocReqApprovedByReviewer(DocPrintRequest docReqObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ApproveDocPrintReqByReviewer]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocRequestID", docReqObjects.DocReqID);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedByID", docReqObjects.ReviewedByID);
                DMS_Cmd.Parameters.AddWithValue("@comments", docReqObjects.Comments);
                DMS_Cmd.Parameters.AddWithValue("@Validity", docReqObjects.Validity);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion DocPrintReqApproveByReviewer
        #region DocPrintReqRejectByReviewer
        public int DMS_DocReqRejectedByReviewer(DocPrintRequest docReqObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_RejectDocPrintReqByReviewer]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocRequestID", docReqObjects.DocReqID);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedByID", docReqObjects.ReviewedByID);
                DMS_Cmd.Parameters.AddWithValue("@comments", docReqObjects.Comments);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion DocPrintReqRejectByReviewer
        #region DocPrintReqApproveByController
        public int DMS_DocReqApprovedByController(DocPrintRequest docReqObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ApproveDocPrintReqByController]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocRequestID", docReqObjects.DocReqID);
                DMS_Cmd.Parameters.AddWithValue("@ControllerID", docReqObjects.ReviewedByID);
                DMS_Cmd.Parameters.AddWithValue("@comments", docReqObjects.Comments);
                DMS_Cmd.Parameters.AddWithValue("@Validity", docReqObjects.Validity);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion DocPrintReqApproveByController
        public int DMS_DocPrintReqByRequestor(DocPrintRequest docReqObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocPrintRequestByRequestor]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocRequestID", docReqObjects.DocReqID);
                DMS_Cmd.Parameters.AddWithValue("@RequestByID", docReqObjects.RequestedByID);
                DMS_Cmd.Parameters.AddWithValue("@Mode", docReqObjects.Mode);
                DMS_Cmd.Parameters.AddWithValue("@Comments", docReqObjects.Comments);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_DocDownloadReqByRequestor(DocPrintRequest docReqObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocDownloadRequestByRequestor]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocRequestID", docReqObjects.DocReqID);
                DMS_Cmd.Parameters.AddWithValue("@RequestByID", docReqObjects.RequestedByID);
                DMS_Cmd.Parameters.AddWithValue("@Mode", docReqObjects.Mode);
                DMS_Cmd.Parameters.AddWithValue("@Comments", docReqObjects.Comments);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion DocRequestActions

        #region FormActions
        #region FormApproveByController
        public int DMS_ApproveFormbyController(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ApproveFormbyController]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.FormVersionID);
                DMS_Cmd.Parameters.AddWithValue("@ApprovedBy", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@comments", doc.Remarks);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion FormApproveByController
        #region FormRevertByController
        public int DMS_RevertFormbyController(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_RevertFormByController]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.FormVersionID);
                DMS_Cmd.Parameters.AddWithValue("@ApprovedBy", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@comments", doc.Remarks);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion FormRevertByController
        #region FormRejectByController
        public int DMS_RejectFormbyController(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_RejectFormByController]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.FormVersionID);
                DMS_Cmd.Parameters.AddWithValue("@ApprovedBy", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@comments", doc.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@status", doc.Status);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion FormRejectByController
        #region FormWithdrawByController
        public int DMS_WithdrawFormbyController(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_WithdrawFormbyController]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.FormVersionID);
                DMS_Cmd.Parameters.AddWithValue("@ApprovedBy", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@comments", doc.Remarks);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion FormWithdrawByController
        #region ApproveFormRevisionbyController
        public int DMS_ApproveFormRevisionbyController(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ApproveFormRevisionbyController]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.FormVersionID);
                DMS_Cmd.Parameters.AddWithValue("@ApprovedBy", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@comments", doc.Remarks);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion ApproveFormRevisionbyController
        #endregion FormActions

        #region FormRequestActions
        #region FormPrintReqRejectByReviewer
        public int DMS_FormPrintReqReject(DocPrintRequest docReqObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_RejectFormPrintReqByReviewer]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormRequestID", docReqObjects.DocReqID);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedByID", docReqObjects.ReviewedByID);
                DMS_Cmd.Parameters.AddWithValue("@comments", docReqObjects.Comments);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion FormPrintReqRejectByReviewer
        #region FormPrintReqApproveByController
        public int DMS_FormReqApprovedByController(DocPrintRequest docReqObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ApproveFormPrintReqByController]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormRequestID", docReqObjects.DocReqID);
                DMS_Cmd.Parameters.AddWithValue("@ControllerID", docReqObjects.ReviewedByID);
                DMS_Cmd.Parameters.AddWithValue("@comments", docReqObjects.Comments);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion FormPrintReqApproveByController
        #region FormPrintReqApproveByReviewer
        public int DMS_FormReqApprovedByReviewer(DocPrintRequest docReqObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ApproveFormPrintReqByReviewer]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormRequestID", docReqObjects.DocReqID);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedByID", docReqObjects.ReviewedByID);
                DMS_Cmd.Parameters.AddWithValue("@comments", docReqObjects.Comments);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion FormPrintReqApproveByReviewer
        public int DMS_FormPrintReqByRequestor(DocPrintRequest docReqObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_FormPrintRequestByRequestor]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormRequestID", docReqObjects.DocReqID);
                DMS_Cmd.Parameters.AddWithValue("@RequestByID", docReqObjects.RequestedByID);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion FormRequestActions

        #region DocumentTypeDetails
        public DataTable DMS_GetIsTraining(int DocumentTypeID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //bool DMS_Return = false;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetIsTraining]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocumentTypeID", DocumentTypeID);
                //DMS_Cmd.Parameters.Add("@Istraining", SqlDbType.Bit);
                //DMS_Cmd.Parameters["@Istraining"].Direction = ParameterDirection.Output;
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion DocumentTypeDetails

        //public int DMS_RejectDocByReviewer(string Comments, string RID, string DocumentID, string ReviewedBy)
        //{
        //    SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //    int DMS_Return = 0;
        //    DataTable DMS_Dt = new DataTable();
        //    try
        //    {
        //        if (DMS_Con.State == ConnectionState.Closed)
        //        {
        //            DMS_Con.Open();
        //        }
        //        SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_RejectDocByReviewer]", DMS_Con);
        //        DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //        DMS_Cmd.Parameters.AddWithValue("@Comments", Comments);
        //        DMS_Cmd.Parameters.AddWithValue("@RID", Convert.ToInt32(RID));
        //        DMS_Cmd.Parameters.AddWithValue("@DocumentID", Convert.ToInt32(DocumentID));
        //        DMS_Cmd.Parameters.AddWithValue("@ReviewedBy", Convert.ToInt32(ReviewedBy));
        //        DMS_Cmd.Parameters.AddWithValue("@ReviewedDate", DateTime.Now);
        //        DMS_Return = DMS_Cmd.ExecuteNonQuery();


        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        DMS_Con.Close();
        //        DMS_Con.Dispose();
        //    }
        //    return DMS_Return;
        //}
        //public DataTable DMS_GetApproverDocuments(string ApprovedBy)
        //{
        //    SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //    int DMS_Return = 0;
        //    DataTable DMS_Dt = new DataTable();
        //    try
        //    {
        //        if (DMS_Con.State == ConnectionState.Closed)
        //        {
        //            DMS_Con.Open();
        //        }
        //        SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ApproverDocList]", DMS_Con);
        //        DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //        DMS_Cmd.Parameters.AddWithValue("@approvedBy", Convert.ToInt32(ApprovedBy));
        //        SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
        //        DMS_da.Fill(DMS_Dt);

        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        DMS_Con.Close();
        //        DMS_Con.Dispose();
        //    }
        //    return DMS_Dt;
        //}
        //public int DMS_SaveApproverComments(string Comments, string AID, string DocumentID, string ApprovedBy)
        //{
        //    SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //    int DMS_Return = 0;
        //    DataTable DMS_Dt = new DataTable();
        //    try
        //    {
        //        if (DMS_Con.State == ConnectionState.Closed)
        //        {
        //            DMS_Con.Open();
        //        }
        //        SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_SaveCommentsByApprover]", DMS_Con);
        //        DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //        DMS_Cmd.Parameters.AddWithValue("@Comments", Comments);
        //        DMS_Cmd.Parameters.AddWithValue("@AID", Convert.ToInt32(AID));
        //        DMS_Cmd.Parameters.AddWithValue("@DocumentID", Convert.ToInt32(DocumentID));

        //        DMS_Cmd.Parameters.AddWithValue("@approvedBy", Convert.ToInt32(ApprovedBy));

        //        DMS_Cmd.Parameters.AddWithValue("@ApprovedDate", DateTime.Now);

        //        DMS_Return = DMS_Cmd.ExecuteNonQuery();


        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        DMS_Con.Close();
        //        DMS_Con.Dispose();
        //    }
        //    return DMS_Return;
        //}
        //public int DMS_UpdateCommetaToApprover(string Comments, string RID, string DocumentID, string ApprovedBy)
        //{
        //    SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //    int DMS_Return = 0;
        //    DataTable DMS_Dt = new DataTable();
        //    try
        //    {
        //        if (DMS_Con.State == ConnectionState.Closed)
        //        {
        //            DMS_Con.Open();
        //        }
        //        SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_UpdateCommentsByApprover]", DMS_Con);
        //        DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //        DMS_Cmd.Parameters.AddWithValue("@Comments", Comments);
        //        DMS_Cmd.Parameters.AddWithValue("@AID", Convert.ToInt32(RID));
        //        DMS_Cmd.Parameters.AddWithValue("@DocumentID", Convert.ToInt32(DocumentID));

        //        DMS_Cmd.Parameters.AddWithValue("@ApprovedBy", Convert.ToInt32(ApprovedBy));

        //        DMS_Cmd.Parameters.AddWithValue("@ApprovedDate", DateTime.Now);

        //        DMS_Return = DMS_Cmd.ExecuteNonQuery();


        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        DMS_Con.Close();
        //        DMS_Con.Dispose();
        //    }
        //    return DMS_Return;
        //}
        //public int DMS_RejectDocByApprover(string Comments, string AID, string DocumentID, string ApprovedBy)
        //{
        //    SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //    int DMS_Return = 0;
        //    DataTable DMS_Dt = new DataTable();
        //    try
        //    {
        //        if (DMS_Con.State == ConnectionState.Closed)
        //        {
        //            DMS_Con.Open();
        //        }
        //        SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_RejectDocByApprover]", DMS_Con);
        //        DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //        DMS_Cmd.Parameters.AddWithValue("@Comments", Comments);
        //        DMS_Cmd.Parameters.AddWithValue("@AID", Convert.ToInt32(AID));
        //        DMS_Cmd.Parameters.AddWithValue("@DocumentID", Convert.ToInt32(DocumentID));

        //        DMS_Cmd.Parameters.AddWithValue("@ApprovedBy", Convert.ToInt32(ApprovedBy));

        //        DMS_Cmd.Parameters.AddWithValue("@ApprovedDate", DateTime.Now);

        //        DMS_Return = DMS_Cmd.ExecuteNonQuery();


        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        DMS_Con.Close();
        //        DMS_Con.Dispose();
        //    }
        //    return DMS_Return;
        //}
        //public int DMS_ApproveDocByApprover(string AID, string DocumentID, string ApprovedBy)
        //{
        //    SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //    int DMS_Return = 0;
        //    DataTable DMS_Dt = new DataTable();
        //    try
        //    {
        //        if (DMS_Con.State == ConnectionState.Closed)
        //        {
        //            DMS_Con.Open();
        //        }
        //        SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ApproveDocByApprover]", DMS_Con);
        //        DMS_Cmd.CommandType = CommandType.StoredProcedure;

        //        DMS_Cmd.Parameters.AddWithValue("@AID", Convert.ToInt32(AID));
        //        DMS_Cmd.Parameters.AddWithValue("@DocumentID", Convert.ToInt32(DocumentID));
        //        DMS_Cmd.Parameters.AddWithValue("@ApprovedBy", Convert.ToInt32(ApprovedBy));
        //        DMS_Cmd.Parameters.AddWithValue("@ApprovedDate", DateTime.Now);
        //        DMS_Return = DMS_Cmd.ExecuteNonQuery();


        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        DMS_Con.Close();
        //        DMS_Con.Dispose();
        //    }
        //    return DMS_Return;
        //}
        //public DataTable DMS_GetDocumentContent(string DMS_SearhValue)
        //{
        //    SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //    int DMS_Return = 0;
        //    DataTable DMS_Dt = new DataTable();
        //    try
        //    {
        //        if (DMS_Con.State == ConnectionState.Closed)
        //        {
        //            DMS_Con.Open();
        //        }
        //        SqlCommand DMS_Cmd = new SqlCommand("select documentID,Content from DMS.AizantIT_Document where Name='" + DMS_SearhValue + "'", DMS_Con);
        //        SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
        //        DMS_da.Fill(DMS_Dt);

        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        DMS_Con.Close();
        //        DMS_Con.Dispose();
        //    }
        //    return DMS_Dt;
        //}
        //public int DMS_DocumentContentUpdation(string DocumentID, string DocumentContent, string EditBy, string Comments)
        //{
        //    SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //    int DMS_Return = 0;
        //    DataTable DMS_Dt = new DataTable();
        //    try
        //    {
        //        if (DMS_Con.State == ConnectionState.Closed)
        //        {
        //            DMS_Con.Open();
        //        }
        //        SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentUpdation]", DMS_Con);
        //        DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //        DMS_Cmd.Parameters.AddWithValue("@DocumentID", DocumentID);
        //        DMS_Cmd.Parameters.AddWithValue("@Content", DocumentContent);
        //        DMS_Cmd.Parameters.AddWithValue("@Comments", Comments);
        //        DMS_Cmd.Parameters.AddWithValue("@EditedBy", EditBy);
        //        DMS_Cmd.Parameters.AddWithValue("@EditedDate", DateTime.Now);

        //        DMS_Return = DMS_Cmd.ExecuteNonQuery();

        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        DMS_Con.Close();
        //        DMS_Con.Dispose();
        //    }
        //    return DMS_Return;
        //}
        //#region MultiQA
        //public int DMS_MultiQAReviewstatus(ReviewObjects reviewObjects)
        //{
        //    SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //    int DMS_Return = 0;
        //    DataTable DMS_Dt = new DataTable();
        //    try
        //    {

        //        if (DMS_Con.State == ConnectionState.Closed)
        //        {
        //            DMS_Con.Open();
        //        }
        //        SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_MultiQAReviewStatus]", DMS_Con);
        //        DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //        DMS_Cmd.Parameters.AddWithValue("@DocumentID", reviewObjects.Id);
        //        DMS_Cmd.Parameters.AddWithValue("@ReviewedBy", reviewObjects.EmpID);
        //        SqlParameter Status = DMS_Cmd.Parameters.Add("@status", SqlDbType.Int);
        //        Status.Direction = ParameterDirection.Output;

        //        SqlDataAdapter da = new SqlDataAdapter(DMS_Cmd);

        //        DMS_Cmd.ExecuteNonQuery();
        //        string Result = DMS_Cmd.Parameters["@status"].Value.ToString();

        //        DMS_Return = Convert.ToInt32(Result);

        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        DMS_Con.Close();
        //        DMS_Con.Dispose();
        //    }
        //    return DMS_Return;
        //}
        //#endregion MultiQA
    }
}
