﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.WebControls;
using AizantIT_DMSBO;
using System.Collections;
using System.Reflection;

namespace AizantIT_DMSDAL
{
    public class DocumentCreationDAL
    {
        string AizantIT_DMS_Con = ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ConnectionString;
        #region DropdownFilling     
        public DataTable DMS_GetApproverReviewerList(DocObjects doc)//Approver Reviewer Filling
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetAPProverReviwerList]", DMS_Con);
                DMS_Cmd.Parameters.AddWithValue("@Dept", Convert.ToInt32(doc.dept));
                DMS_Cmd.Parameters.AddWithValue("@role", doc.UserRole);

                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetRequestTypeList()//RequestType Filling
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_RequestType]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetApproverList(DocObjects doc)//get Employees based on roles(currently creator and author)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetEmployeeList]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@RoleID", doc.UserRole);

                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetApproverReviewerSelectedList(string DocumentID, int Doc_Status)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetselectedReviewerApproverlist]", DMS_Con);
                DMS_Cmd.Parameters.AddWithValue("@DocStatus", Doc_Status);
                DMS_Cmd.Parameters.AddWithValue("@documentID", Convert.ToInt32(DocumentID));
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #region GetControllers
        public DataTable DMS_GetControllersList()//Controllers Filling
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetControllers]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion GetControllers
        #region GetPrintReviewersList
        public DataTable DMS_GetPrintReviewersList(DocObjects doc)//Approver Reviewer Filling
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetPrintReviewers]", DMS_Con);
                DMS_Cmd.Parameters.AddWithValue("@DeptID", Convert.ToInt32(doc.dept));
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion GetPrintReviewersList
        public DataTable DMS_GetCreatorList(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetCreatorList]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@RoleID", doc.UserRole);
                DMS_Cmd.Parameters.AddWithValue("@dept", Convert.ToInt32(doc.dept));

                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetCopyType()//DistributionCopy Type Filling
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetDistributionCopyType]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion DropdownFilling

        #region TemplateActions
        public int DMS_InsertTemplate(ReviewObjects reviewObjects)//Template Insertion
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_SaveTemplateContent]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@name", reviewObjects.TemplateName);
                DMS_Cmd.Parameters.AddWithValue("@ext", reviewObjects.Ext);
                DMS_Cmd.Parameters.AddWithValue("@Content", reviewObjects.Content);
                DMS_Cmd.Parameters.AddWithValue("@Creator", reviewObjects.Creator);
                DMS_Cmd.Parameters.AddWithValue("@date", DateTime.Now);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_DeleteTemplate(ReviewObjects reviewObjects)//Template Deletion
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DeleteTemplate]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@id", reviewObjects.Id);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion TemplateActions

        #region ExistingDocs
        public int DMS_ExistingDocumentInsertion(DocObjects doc, bool isPublic)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction(IsolationLevel.ReadUncommitted, "CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_InsertExistingDocuments]", DMS_Con);
                DMS_Cmd.CommandTimeout = 30;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                //DMS_Cmd.Parameters.AddWithValue("@Content", doc.Content);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.DMS_CreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@date", DateTime.Now);
                DMS_Cmd.Parameters.AddWithValue("@effectivedate", doc.effective);
                DMS_Cmd.Parameters.AddWithValue("@expiration", doc.expiration);
                DMS_Cmd.Parameters.AddWithValue("@filename", doc.Filename);
                DMS_Cmd.Parameters.AddWithValue("@VersionID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@DocumentName", doc.DMS_DocumentName);
                DMS_Cmd.Parameters.AddWithValue("@DocumentNumber", doc.documentNUmber);
                DMS_Cmd.Parameters.AddWithValue("@DocumentType", doc.DMS_DocumentType);
                DMS_Cmd.Parameters.AddWithValue("@deptID", doc.DMS_Deparment);
                DMS_Cmd.Parameters.AddWithValue("@RequestTypeID", doc.RequestTypeID);
                DMS_Cmd.Parameters.AddWithValue("@PreparedBY", doc.PreparedBY);
                DMS_Cmd.Parameters.AddWithValue("@PeparedDate", doc.PreparedDate);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedBy", doc.oldReviewedBy);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedDate", doc.ReviewedDate);
                DMS_Cmd.Parameters.AddWithValue("@ApprovedBY", doc.oldApprovedBY);
                DMS_Cmd.Parameters.AddWithValue("@ApprovedDate", doc.ApprovedDate);
                DMS_Cmd.Parameters.AddWithValue("@pkid", doc.pkid);
                DMS_Cmd.Parameters.AddWithValue("@isPublic", isPublic);
                DMS_Cmd.Parameters.AddWithValue("@FileName2", doc.FileName2);
                DMS_Cmd.Parameters.AddWithValue("@FilePath2", doc.FilePath2);
                DMS_Cmd.Parameters.AddWithValue("@FileType2", doc.FileType2);
                SqlParameter ResultStatus = DMS_Cmd.Parameters.Add("@ResultStatus", SqlDbType.Int);
                ResultStatus.Direction = ParameterDirection.Output;
                SqlDataAdapter da = new SqlDataAdapter(DMS_Cmd);
                DataTable dtExt = new DataTable();
                da.Fill(dtExt);
                DMS_Return = Convert.ToInt32(ResultStatus.Value);
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion ExistingDocs

        #region DocumentType
        public DataTable DMS_GetProcessTypeList()
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].AizantIT_SP_ProcessType", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetRenewYearsDal()
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ToGetRenewYears]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public int DMS_OtherDocumentTypeInsertion(DocObjects doc, bool training)//DocumentType Insertion
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentTypeIns]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocumentType", doc.DocumentType);
                DMS_Cmd.Parameters.AddWithValue("@ProcessType", Convert.ToInt32(doc.ProcessType));
                DMS_Cmd.Parameters.AddWithValue("@ReviewYear", Convert.ToInt32(doc.ReviewYear));
                DMS_Cmd.Parameters.AddWithValue("@training", training);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.DMS_CreatedBy);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();

            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_OtherDocumentTypeInsertionUpdate(DocObjects doc, bool training)//DocumentType Updation
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentTypeUpdate]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@id", Convert.ToInt32(doc.TypeID));
                DMS_Cmd.Parameters.AddWithValue("@ReviewYear", Convert.ToInt32(doc.ReviewYear));
                DMS_Cmd.Parameters.AddWithValue("@type", doc.DocumentType);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.DMS_CreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@Comments", doc.Remarks);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public DataTable DMS_GetDocumentTypeComments(string DocTypeID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetDocTypeComments]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocTypeID", Convert.ToInt32(DocTypeID));
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion DocumentType

        #region NewDocInitiation
        public int DMS_DocumentCreation(DocObjects doc, bool isPublic)//New Document Insertion
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {


                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction(IsolationLevel.ReadUncommitted, "CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                var ActionDate = DateTime.Now;
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertion]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocumentName", doc.DMS_DocumentName);
                DMS_Cmd.Parameters.AddWithValue("@CreatedBy", doc.DMS_CreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@DocumentType", doc.DMS_DocumentType);
                DMS_Cmd.Parameters.AddWithValue("@IntiatedBy", doc.DMS_IntiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@IntiatedDate", ActionDate);
                DMS_Cmd.Parameters.AddWithValue("@AuthorBy", doc.authorBy);
                DMS_Cmd.Parameters.AddWithValue("@DocumentNumber", doc.documentNUmber);
                DMS_Cmd.Parameters.AddWithValue("@EffectiveDate", DBNull.Value);
                DMS_Cmd.Parameters.AddWithValue("@expirationDate", DBNull.Value);
                DMS_Cmd.Parameters.AddWithValue("@Department", doc.DMS_Deparment);
                DMS_Cmd.Parameters.AddWithValue("@RequestType", doc.RequestType);
                DMS_Cmd.Parameters.AddWithValue("@Purpose", doc.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@isPublic", isPublic);
                SqlParameter Status = DMS_Cmd.Parameters.Add("@id", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;
                SqlParameter CommentStatus = DMS_Cmd.Parameters.Add("@CommentsID", SqlDbType.Int);
                CommentStatus.Direction = ParameterDirection.Output;
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                string Result = DMS_Cmd.Parameters["@id"].Value.ToString();
                int id = Convert.ToInt32(Result);
                string Result1 = DMS_Cmd.Parameters["@CommentsID"].Value.ToString();
                int commentid = Convert.ToInt32(Result1);
                int priority = 1;
                SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd1.CommandTimeout = 120;
                DMS_Cmd1.Transaction = transaction;
                DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter = new SqlParameter();
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table1 = new DataTable();
                table1.Columns.Add("EmpID", typeof(Int32));
                table1.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item1 in doc.lstReviewBy)
                {
                    DataRow row = table1.NewRow();
                    row["EmpID"] = item1.EmpID;
                    row["DeptID"] = item1.DeptID;
                    table1.Rows.Add(row);
                }
                parameter.Value = table1;
                parameter.ParameterName = "@TblEmps";
                DMS_Cmd1.Parameters.Add(parameter);
                DMS_Cmd1.Parameters.AddWithValue("@VersionID", id);
                DMS_Cmd1.Parameters.AddWithValue("@type", 'R');
                DMS_Cmd1.Parameters.AddWithValue("@status", 'N');
                DMS_Cmd1.Parameters.AddWithValue("@priority", priority);
                DMS_Cmd1.Parameters.AddWithValue("@DeptID", doc.lstReviewBy[0].DeptID);
                DMS_Cmd1.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd1.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd1.ExecuteNonQuery();
                priority = priority + 1;
                int prioritya = 1;
                SqlCommand DMS_Cmd2 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd2.CommandTimeout = 120;
                DMS_Cmd2.Transaction = transaction;
                DMS_Cmd2.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter1 = new SqlParameter();
                parameter1.SqlDbType = SqlDbType.Structured;
                parameter1.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table2 = new DataTable();
                table2.Columns.Add("EmpID", typeof(Int32));
                table2.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item2 in doc.lstApproveBy)
                {
                    DataRow row1 = table2.NewRow();
                    row1["EmpID"] = item2.EmpID;
                    row1["DeptID"] = item2.DeptID;
                    table2.Rows.Add(row1);
                }
                parameter1.Value = table2;
                parameter1.ParameterName = "@TblEmps";
                DMS_Cmd2.Parameters.Add(parameter1);
                DMS_Cmd2.Parameters.AddWithValue("@VersionID", id);
                DMS_Cmd2.Parameters.AddWithValue("@type", 'A');
                DMS_Cmd2.Parameters.AddWithValue("@status", 'N');
                DMS_Cmd2.Parameters.AddWithValue("@priority", prioritya);
                DMS_Cmd2.Parameters.AddWithValue("@DeptID", doc.lstApproveBy[0].DeptID);
                DMS_Cmd2.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd2.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd2.ExecuteNonQuery();
                prioritya = prioritya + 1;
                int priorityi = 1;
                SqlCommand DMS_Cmd3 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd3.CommandTimeout = 120;
                DMS_Cmd3.Transaction = transaction;
                DMS_Cmd3.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter2 = new SqlParameter();
                parameter2.SqlDbType = SqlDbType.Structured;
                parameter2.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table3 = new DataTable();
                table3.Columns.Add("EmpID", typeof(Int32));
                table3.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item3 in doc.lstQA)
                {
                    DataRow row2 = table3.NewRow();
                    row2["EmpID"] = item3.EmpID;
                    row2["DeptID"] = item3.DeptID;
                    table3.Rows.Add(row2);
                }
                parameter2.Value = table3;
                parameter2.ParameterName = "@TblEmps";
                DMS_Cmd3.Parameters.Add(parameter2);
                DMS_Cmd3.Parameters.AddWithValue("@VersionID", id);
                DMS_Cmd3.Parameters.AddWithValue("@type", 'I');
                DMS_Cmd3.Parameters.AddWithValue("@status", 'N');
                DMS_Cmd3.Parameters.AddWithValue("@priority", priorityi);
                DMS_Cmd3.Parameters.AddWithValue("@DeptID", doc.lstQA[0].DeptID);
                DMS_Cmd3.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd3.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd3.ExecuteNonQuery();
                priorityi = priorityi + 1;
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_DocumentEdit(DocObjects doc, bool isPublic)//New Document Updation
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction(IsolationLevel.ReadUncommitted, "CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                var ActionDate = DateTime.Now;
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentUpdate]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", Convert.ToInt32(doc.DocumentID));
                DMS_Cmd.Parameters.AddWithValue("@DocumentNumber", doc.documentNUmber);
                DMS_Cmd.Parameters.AddWithValue("@stat", doc.Status);
                DMS_Cmd.Parameters.AddWithValue("@DocumentName", doc.DMS_DocumentName);
                DMS_Cmd.Parameters.AddWithValue("@CreatedBy", doc.DMS_CreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@DocumentType", doc.DMS_DocumentType);
                DMS_Cmd.Parameters.AddWithValue("@ModifiedBy", doc.DMS_ModifiedBy);
                DMS_Cmd.Parameters.AddWithValue("@IntiatedBy", doc.DMS_IntiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@IntiatedDate", ActionDate);
                DMS_Cmd.Parameters.AddWithValue("@Department", doc.DMS_Deparment);
                DMS_Cmd.Parameters.AddWithValue("@RequestType", doc.RequestType);
                DMS_Cmd.Parameters.AddWithValue("@author", doc.authorBy);
                DMS_Cmd.Parameters.AddWithValue("@Purpose", doc.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@isPublic", isPublic);
                SqlParameter CommentStatus = DMS_Cmd.Parameters.Add("@CommentsID", SqlDbType.Int);
                CommentStatus.Direction = ParameterDirection.Output;
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                string Result1 = DMS_Cmd.Parameters["@CommentsID"].Value.ToString();
                int commentid = Convert.ToInt32(Result1);
                if (doc.Status != 0)
                {
                    SqlCommand DMS_CmdReset = new SqlCommand("[DMS].[AizantIT_SP_ResetDocumentInsertionmultiple]", DMS_Con);
                    DMS_CmdReset.CommandTimeout = 120;
                    DMS_CmdReset.Transaction = transaction;
                    DMS_CmdReset.CommandType = CommandType.StoredProcedure;
                    DMS_CmdReset.Parameters.AddWithValue("@docid", Convert.ToInt32(doc.DocumentID));
                    int c = DMS_CmdReset.ExecuteNonQuery();
                }
                int pripority = 1;
                SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd1.CommandTimeout = 120;
                DMS_Cmd1.Transaction = transaction;
                DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter = new SqlParameter();
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table1 = new DataTable();
                table1.Columns.Add("EmpID", typeof(Int32));
                table1.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item1 in doc.lstReviewBy)
                {
                    DataRow row = table1.NewRow();
                    row["EmpID"] = item1.EmpID;
                    row["DeptID"] = item1.DeptID;
                    table1.Rows.Add(row);
                }
                parameter.Value = table1;
                parameter.ParameterName = "@TblEmps";
                DMS_Cmd1.Parameters.Add(parameter);
                DMS_Cmd1.Parameters.AddWithValue("@VersionID", Convert.ToInt32(doc.DocumentID));
                DMS_Cmd1.Parameters.AddWithValue("@type", 'R');
                DMS_Cmd1.Parameters.AddWithValue("@status", 'U');
                DMS_Cmd1.Parameters.AddWithValue("@priority", pripority);
                DMS_Cmd1.Parameters.AddWithValue("@DeptID", doc.lstReviewBy[0].DeptID);
                DMS_Cmd1.Parameters.AddWithValue("@isManage", doc.isManage);
                DMS_Cmd1.Parameters.AddWithValue("@LoginUserID", doc.DMS_ModifiedBy);
                DMS_Cmd1.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd1.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd1.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd1.ExecuteNonQuery();
                pripority = pripority + 1;
                int prioritya = 1;
                SqlCommand DMS_Cmd2 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd2.CommandTimeout = 120;
                DMS_Cmd2.Transaction = transaction;
                DMS_Cmd2.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter1 = new SqlParameter();
                parameter1.SqlDbType = SqlDbType.Structured;
                parameter1.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table2 = new DataTable();
                table2.Columns.Add("EmpID", typeof(Int32));
                table2.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item2 in doc.lstApproveBy)
                {
                    DataRow row1 = table2.NewRow();
                    row1["EmpID"] = item2.EmpID;
                    row1["DeptID"] = item2.DeptID;
                    table2.Rows.Add(row1);
                }
                parameter1.Value = table2;
                parameter1.ParameterName = "@TblEmps";
                DMS_Cmd2.Parameters.Add(parameter1);
                DMS_Cmd2.Parameters.AddWithValue("@VersionID", Convert.ToInt32(doc.DocumentID));
                DMS_Cmd2.Parameters.AddWithValue("@type", 'A');
                DMS_Cmd2.Parameters.AddWithValue("@status", 'U');
                DMS_Cmd2.Parameters.AddWithValue("@priority", prioritya);
                DMS_Cmd2.Parameters.AddWithValue("@DeptID", doc.lstApproveBy[0].DeptID);
                DMS_Cmd2.Parameters.AddWithValue("@isManage", doc.isManage);
                DMS_Cmd2.Parameters.AddWithValue("@LoginUserID", doc.DMS_ModifiedBy);
                DMS_Cmd2.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd2.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd2.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd2.ExecuteNonQuery();
                prioritya = prioritya + 1;
                int priorityi = 1;
                SqlCommand DMS_Cmd3 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd3.CommandTimeout = 120;
                DMS_Cmd3.Transaction = transaction;
                DMS_Cmd3.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter2 = new SqlParameter();
                parameter2.SqlDbType = SqlDbType.Structured;
                parameter2.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table3 = new DataTable();
                table3.Columns.Add("EmpID", typeof(Int32));
                table3.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item3 in doc.lstQA)
                {
                    DataRow row2 = table3.NewRow();
                    row2["EmpID"] = item3.EmpID;
                    row2["DeptID"] = item3.DeptID;
                    table3.Rows.Add(row2);
                }
                parameter2.Value = table3;
                parameter2.ParameterName = "@TblEmps";
                DMS_Cmd3.Parameters.Add(parameter2);
                DMS_Cmd3.Parameters.AddWithValue("@VersionID", Convert.ToInt32(doc.DocumentID));
                DMS_Cmd3.Parameters.AddWithValue("@type", 'I');
                DMS_Cmd3.Parameters.AddWithValue("@status", 'U');
                DMS_Cmd3.Parameters.AddWithValue("@priority", priorityi);
                DMS_Cmd3.Parameters.AddWithValue("@DeptID", doc.lstQA[0].DeptID);
                DMS_Cmd3.Parameters.AddWithValue("@isManage", doc.isManage);
                DMS_Cmd3.Parameters.AddWithValue("@LoginUserID", doc.DMS_ModifiedBy);
                DMS_Cmd3.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd3.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd3.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd3.ExecuteNonQuery();
                priorityi = priorityi + 1;
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion NewDocInitiation
        #region RevisionDocInitiation
        public int DMS_DocumentUpdation(DocObjects doc, bool isPublic)//Revision Document Insertion
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction(IsolationLevel.ReadUncommitted, "CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                var ActionDate = DateTime.Now;
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocUpdate]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@ParentID", Convert.ToInt32(doc.DocumentID));
                DMS_Cmd.Parameters.AddWithValue("@DocumentName", doc.DMS_DocumentName);
                DMS_Cmd.Parameters.AddWithValue("@CreatedBy", doc.DMS_CreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@DocumentType", doc.DMS_DocumentType);
                DMS_Cmd.Parameters.AddWithValue("@IntiatedBy", doc.DMS_IntiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@IntiatedDate", ActionDate);
                DMS_Cmd.Parameters.AddWithValue("@AuthorBy", doc.authorBy);
                DMS_Cmd.Parameters.AddWithValue("@EffectiveDate", DBNull.Value);
                DMS_Cmd.Parameters.AddWithValue("@expirationDate", DBNull.Value);
                DMS_Cmd.Parameters.AddWithValue("@Department", doc.DMS_Deparment);
                DMS_Cmd.Parameters.AddWithValue("@RequestType", doc.RequestType);
                DMS_Cmd.Parameters.AddWithValue("@Purpose", doc.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@isPublic", isPublic);
                SqlParameter Status = DMS_Cmd.Parameters.Add("@id", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;
                SqlParameter CommentStatus = DMS_Cmd.Parameters.Add("@CommentsID", SqlDbType.Int);
                CommentStatus.Direction = ParameterDirection.Output;
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                string Result = DMS_Cmd.Parameters["@id"].Value.ToString();
                int id = Convert.ToInt32(Result);
                string Result1 = DMS_Cmd.Parameters["@CommentsID"].Value.ToString();
                int commentid = Convert.ToInt32(Result1);
                int priority = 1;
                SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd1.CommandTimeout = 120;
                DMS_Cmd1.Transaction = transaction;
                DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter = new SqlParameter();
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table1 = new DataTable();
                table1.Columns.Add("EmpID", typeof(Int32));
                table1.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item1 in doc.lstReviewBy)
                {
                    DataRow row = table1.NewRow();
                    row["EmpID"] = item1.EmpID;
                    row["DeptID"] = item1.DeptID;
                    table1.Rows.Add(row);
                }
                parameter.Value = table1;
                parameter.ParameterName = "@TblEmps";
                DMS_Cmd1.Parameters.Add(parameter);
                DMS_Cmd1.Parameters.AddWithValue("@VersionID", id);
                DMS_Cmd1.Parameters.AddWithValue("@type", 'R');
                DMS_Cmd1.Parameters.AddWithValue("@status", 'N');
                DMS_Cmd1.Parameters.AddWithValue("@priority", priority);
                DMS_Cmd1.Parameters.AddWithValue("@DeptID", doc.lstReviewBy[0].DeptID);
                DMS_Cmd1.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd1.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd1.ExecuteNonQuery();
                priority = priority + 1;
                int prioritya = 1;
                SqlCommand DMS_Cmd2 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd2.CommandTimeout = 120;
                DMS_Cmd2.Transaction = transaction;
                DMS_Cmd2.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter1 = new SqlParameter();
                parameter1.SqlDbType = SqlDbType.Structured;
                parameter1.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table2 = new DataTable();
                table2.Columns.Add("EmpID", typeof(Int32));
                table2.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item2 in doc.lstApproveBy)
                {
                    DataRow row1 = table2.NewRow();
                    row1["EmpID"] = item2.EmpID;
                    row1["DeptID"] = item2.DeptID;
                    table2.Rows.Add(row1);
                }
                parameter1.Value = table2;
                parameter1.ParameterName = "@TblEmps";
                DMS_Cmd2.Parameters.Add(parameter1);
                DMS_Cmd2.Parameters.AddWithValue("@VersionID", id);
                DMS_Cmd2.Parameters.AddWithValue("@type", 'A');
                DMS_Cmd2.Parameters.AddWithValue("@status", 'N');
                DMS_Cmd2.Parameters.AddWithValue("@priority", prioritya);
                DMS_Cmd2.Parameters.AddWithValue("@DeptID", doc.lstApproveBy[0].DeptID);
                DMS_Cmd2.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd2.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd2.ExecuteNonQuery();
                prioritya = prioritya + 1;
                int priorityi = 1;
                SqlCommand DMS_Cmd3 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd3.CommandTimeout = 120;
                DMS_Cmd3.Transaction = transaction;
                DMS_Cmd3.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter2 = new SqlParameter();
                parameter2.SqlDbType = SqlDbType.Structured;
                parameter2.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table3 = new DataTable();
                table3.Columns.Add("EmpID", typeof(Int32));
                table3.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item3 in doc.lstQA)
                {
                    DataRow row2 = table3.NewRow();
                    row2["EmpID"] = item3.EmpID;
                    row2["DeptID"] = item3.DeptID;
                    table3.Rows.Add(row2);
                }
                parameter2.Value = table3;
                parameter2.ParameterName = "@TblEmps";
                DMS_Cmd3.Parameters.Add(parameter2);
                DMS_Cmd3.Parameters.AddWithValue("@VersionID", id);
                DMS_Cmd3.Parameters.AddWithValue("@type", 'I');
                DMS_Cmd3.Parameters.AddWithValue("@status", 'N');
                DMS_Cmd3.Parameters.AddWithValue("@priority", priorityi);
                DMS_Cmd3.Parameters.AddWithValue("@DeptID", doc.lstQA[0].DeptID);
                DMS_Cmd3.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd3.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd3.ExecuteNonQuery();
                priorityi = priorityi + 1;
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_DocumentUpdationEdit(DocObjects doc, bool isPublic)//Revision Document Updation
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }

                // Start a local transaction
                transaction = DMS_Con.BeginTransaction(IsolationLevel.ReadUncommitted, "CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                var ActionDate = DateTime.Now;
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocUpdateEDIT]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", Convert.ToInt32(doc.DocumentID));
                DMS_Cmd.Parameters.AddWithValue("@DocumentName", doc.DMS_DocumentName);
                DMS_Cmd.Parameters.AddWithValue("@stat", doc.Status);
                DMS_Cmd.Parameters.AddWithValue("@CreatedBy", doc.DMS_CreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@ModifiedBy", doc.DMS_ModifiedBy);
                DMS_Cmd.Parameters.AddWithValue("@IntiatedBy", doc.DMS_IntiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@author", doc.authorBy);
                DMS_Cmd.Parameters.AddWithValue("@date", ActionDate);
                DMS_Cmd.Parameters.AddWithValue("@Purpose", doc.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@isPublic", isPublic);
                SqlParameter CommentStatus = DMS_Cmd.Parameters.Add("@CommentsID", SqlDbType.Int);
                CommentStatus.Direction = ParameterDirection.Output;
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                string Result1 = DMS_Cmd.Parameters["@CommentsID"].Value.ToString();
                int commentid = Convert.ToInt32(Result1);
                if (doc.Status != 1)
                {
                    SqlCommand DMS_CmdReset = new SqlCommand("[DMS].[AizantIT_SP_ResetDocumentInsertionmultiple]", DMS_Con);
                    DMS_CmdReset.CommandTimeout = 120;
                    DMS_CmdReset.Transaction = transaction;
                    DMS_CmdReset.CommandType = CommandType.StoredProcedure;
                    DMS_CmdReset.Parameters.AddWithValue("@docid", Convert.ToInt32(doc.DocumentID));
                    int c = DMS_CmdReset.ExecuteNonQuery();
                }
                int priority = 1;
                //int intcmd1 = 0;
                SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd1.CommandTimeout = 120;
                DMS_Cmd1.Transaction = transaction;
                DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter = new SqlParameter();
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table1 = new DataTable();
                table1.Columns.Add("EmpID", typeof(Int32));
                table1.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item1 in doc.lstReviewBy)
                {
                    DataRow row = table1.NewRow();
                    row["EmpID"] = item1.EmpID;
                    row["DeptID"] = item1.DeptID;
                    table1.Rows.Add(row);
                }
                parameter.Value = table1;
                parameter.ParameterName = "@TblEmps";
                DMS_Cmd1.Parameters.Add(parameter);
                DMS_Cmd1.Parameters.AddWithValue("@VersionID", Convert.ToInt32(doc.DocumentID));
                DMS_Cmd1.Parameters.AddWithValue("@type", 'R');
                DMS_Cmd1.Parameters.AddWithValue("@status", 'U');
                DMS_Cmd1.Parameters.AddWithValue("@priority", priority);
                DMS_Cmd1.Parameters.AddWithValue("@DeptID", doc.lstReviewBy[0].DeptID);
                DMS_Cmd1.Parameters.AddWithValue("@isManage", doc.isManage);
                DMS_Cmd1.Parameters.AddWithValue("@LoginUserID", doc.DMS_ModifiedBy);
                DMS_Cmd1.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd1.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd1.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd1.ExecuteNonQuery();
                priority = priority + 1;
                int prioritya = 1;
                SqlCommand DMS_Cmd2 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd2.CommandTimeout = 120;
                DMS_Cmd2.Transaction = transaction;
                DMS_Cmd2.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter1 = new SqlParameter();
                parameter1.SqlDbType = SqlDbType.Structured;
                parameter1.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table2 = new DataTable();
                table2.Columns.Add("EmpID", typeof(Int32));
                table2.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item2 in doc.lstApproveBy)
                {
                    DataRow row1 = table2.NewRow();
                    row1["EmpID"] = item2.EmpID;
                    row1["DeptID"] = item2.DeptID;
                    table2.Rows.Add(row1);
                }
                parameter1.Value = table2;
                parameter1.ParameterName = "@TblEmps";
                DMS_Cmd2.Parameters.Add(parameter1);
                DMS_Cmd2.Parameters.AddWithValue("@VersionID", Convert.ToInt32(doc.DocumentID));
                DMS_Cmd2.Parameters.AddWithValue("@type", 'A');
                DMS_Cmd2.Parameters.AddWithValue("@status", 'U');
                DMS_Cmd2.Parameters.AddWithValue("@priority", prioritya);
                DMS_Cmd2.Parameters.AddWithValue("@DeptID", doc.lstApproveBy[0].DeptID);
                DMS_Cmd2.Parameters.AddWithValue("@isManage", doc.isManage);
                DMS_Cmd2.Parameters.AddWithValue("@LoginUserID", doc.DMS_ModifiedBy);
                DMS_Cmd2.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd2.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd2.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd2.ExecuteNonQuery();
                prioritya = prioritya + 1;
                int priorityi = 1;
                SqlCommand DMS_Cmd3 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd3.CommandTimeout = 120;
                DMS_Cmd3.Transaction = transaction;
                DMS_Cmd3.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter2 = new SqlParameter();
                parameter2.SqlDbType = SqlDbType.Structured;
                parameter2.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table3 = new DataTable();
                table3.Columns.Add("EmpID", typeof(Int32));
                table3.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item3 in doc.lstQA)
                {
                    DataRow row2 = table3.NewRow();
                    row2["EmpID"] = item3.EmpID;
                    row2["DeptID"] = item3.DeptID;
                    table3.Rows.Add(row2);
                }
                parameter2.Value = table3;
                parameter2.ParameterName = "@TblEmps";
                DMS_Cmd3.Parameters.Add(parameter2);
                DMS_Cmd3.Parameters.AddWithValue("@VersionID", Convert.ToInt32(doc.DocumentID));
                DMS_Cmd3.Parameters.AddWithValue("@type", 'I');
                DMS_Cmd3.Parameters.AddWithValue("@status", 'U');
                DMS_Cmd3.Parameters.AddWithValue("@priority", priorityi);
                DMS_Cmd3.Parameters.AddWithValue("@DeptID", doc.lstQA[0].DeptID);
                DMS_Cmd3.Parameters.AddWithValue("@isManage", doc.isManage);
                DMS_Cmd3.Parameters.AddWithValue("@LoginUserID", doc.DMS_ModifiedBy);
                DMS_Cmd3.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd3.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd3.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd3.ExecuteNonQuery();
                priorityi = priorityi + 1;
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion RevisionDocInitiation
        #region WithdrawDocInitiation
        public int DMS_DocumentUpdationWithdrawn(DocObjects doc)//Withdraw Document Insertion and Updation
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction(IsolationLevel.ReadUncommitted, "CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                var ActionDate = DateTime.Now;
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentUpdationWithdrawn]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocumentID", doc.DocumentID);
                DMS_Cmd.Parameters.AddWithValue("@RequestType", doc.RequestType);
                DMS_Cmd.Parameters.AddWithValue("@ModifiedBy", doc.DMS_ModifiedBy);
                DMS_Cmd.Parameters.AddWithValue("@IntiatedBy", doc.DMS_IntiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@intiateddate", DateTime.Now);
                DMS_Cmd.Parameters.AddWithValue("@Reason", doc.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@action", doc.action);
                DMS_Cmd.Parameters.AddWithValue("@ControllerID", doc.DMSDocControllerID);
                SqlParameter Status = DMS_Cmd.Parameters.Add("@id", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;
                SqlParameter CommentStatus = DMS_Cmd.Parameters.Add("@CommentsID", SqlDbType.Int);
                CommentStatus.Direction = ParameterDirection.Output;
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                string Result = DMS_Cmd.Parameters["@id"].Value.ToString();
                int id = Convert.ToInt32(Result);
                string Result1 = DMS_Cmd.Parameters["@CommentsID"].Value.ToString();
                int commentid = Convert.ToInt32(Result1);
                int priorityi = 1;
                SqlCommand DMS_Cmd3 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd3.CommandTimeout = 120;
                DMS_Cmd3.Transaction = transaction;
                DMS_Cmd3.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter2 = new SqlParameter();
                parameter2.SqlDbType = SqlDbType.Structured;
                parameter2.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table3 = new DataTable();
                table3.Columns.Add("EmpID", typeof(Int32));
                table3.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item3 in doc.lstQA)
                {
                    DataRow row2 = table3.NewRow();
                    row2["EmpID"] = item3.EmpID;
                    row2["DeptID"] = item3.DeptID;
                    table3.Rows.Add(row2);
                }
                parameter2.Value = table3;
                parameter2.ParameterName = "@TblEmps";
                DMS_Cmd3.Parameters.Add(parameter2);
                DMS_Cmd3.Parameters.AddWithValue("@VersionID", id);
                DMS_Cmd3.Parameters.AddWithValue("@type", 'I');
                DMS_Cmd3.Parameters.AddWithValue("@status", 'W');
                DMS_Cmd3.Parameters.AddWithValue("@priority", priorityi);
                DMS_Cmd3.Parameters.AddWithValue("@DeptID", doc.lstQA[0].DeptID);
                DMS_Cmd3.Parameters.AddWithValue("@isManage", doc.isManage);
                DMS_Cmd3.Parameters.AddWithValue("@LoginUserID", doc.DMS_IntiatedBy);
                DMS_Cmd3.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd3.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd3.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd3.ExecuteNonQuery();
                priorityi = priorityi + 1;
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion WithdrawDocInitiation
        #region RenewDocInitiation
        public int DMS_DocumentInitiationRenew(DocObjects doc)//Renew Document Insertion and Updation
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction(IsolationLevel.ReadUncommitted, "CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                var ActionDate = DateTime.Now;
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentRenewInitiation]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocumentID", doc.DocumentID);
                DMS_Cmd.Parameters.AddWithValue("@RequestType", doc.RequestType);
                DMS_Cmd.Parameters.AddWithValue("@ModifiedBy", doc.DMS_ModifiedBy);
                DMS_Cmd.Parameters.AddWithValue("@IntiatedBy", doc.DMS_IntiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@intiateddate", DateTime.Now);
                DMS_Cmd.Parameters.AddWithValue("@Reason", doc.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@action", doc.action);
                DMS_Cmd.Parameters.AddWithValue("@ControllerID", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@CreatedBy", doc.DMS_CreatedBy);
                SqlParameter Status = DMS_Cmd.Parameters.Add("@id", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;
                SqlParameter CommentStatus = DMS_Cmd.Parameters.Add("@CommentsID", SqlDbType.Int);
                CommentStatus.Direction = ParameterDirection.Output;
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                string Result = DMS_Cmd.Parameters["@id"].Value.ToString();
                int id = Convert.ToInt32(Result);
                string Result1 = DMS_Cmd.Parameters["@CommentsID"].Value.ToString();
                int commentid = Convert.ToInt32(Result1);
                int priority = 1;
                SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd1.CommandTimeout = 120;
                DMS_Cmd1.Transaction = transaction;
                DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter = new SqlParameter();
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table1 = new DataTable();
                table1.Columns.Add("EmpID", typeof(Int32));
                table1.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item1 in doc.lstReviewBy)
                {
                    DataRow row = table1.NewRow();
                    row["EmpID"] = item1.EmpID;
                    row["DeptID"] = item1.DeptID;
                    table1.Rows.Add(row);
                }
                parameter.Value = table1;
                parameter.ParameterName = "@TblEmps";
                DMS_Cmd1.Parameters.Add(parameter);
                DMS_Cmd1.Parameters.AddWithValue("@VersionID", id);
                DMS_Cmd1.Parameters.AddWithValue("@type", 'R');
                DMS_Cmd1.Parameters.AddWithValue("@status", 'N');
                DMS_Cmd1.Parameters.AddWithValue("@priority", priority);
                DMS_Cmd1.Parameters.AddWithValue("@DeptID", doc.lstReviewBy[0].DeptID);
                DMS_Cmd1.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd1.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd1.ExecuteNonQuery();
                priority = priority + 1;
                int prioritya = 1;
                SqlCommand DMS_Cmd2 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd2.CommandTimeout = 120;
                DMS_Cmd2.Transaction = transaction;
                DMS_Cmd2.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter1 = new SqlParameter();
                parameter1.SqlDbType = SqlDbType.Structured;
                parameter1.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table2 = new DataTable();
                table2.Columns.Add("EmpID", typeof(Int32));
                table2.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item2 in doc.lstApproveBy)
                {
                    DataRow row1 = table2.NewRow();
                    row1["EmpID"] = item2.EmpID;
                    row1["DeptID"] = item2.DeptID;
                    table2.Rows.Add(row1);
                }
                parameter1.Value = table2;
                parameter1.ParameterName = "@TblEmps";
                DMS_Cmd2.Parameters.Add(parameter1);
                DMS_Cmd2.Parameters.AddWithValue("@VersionID", id);
                DMS_Cmd2.Parameters.AddWithValue("@type", 'A');
                DMS_Cmd2.Parameters.AddWithValue("@status", 'N');
                DMS_Cmd2.Parameters.AddWithValue("@priority", prioritya);
                DMS_Cmd2.Parameters.AddWithValue("@DeptID", doc.lstApproveBy[0].DeptID);
                DMS_Cmd2.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd2.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd2.ExecuteNonQuery();
                prioritya = prioritya + 1;
                int priorityi = 1;
                SqlCommand DMS_Cmd3 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd3.CommandTimeout = 120;
                DMS_Cmd3.Transaction = transaction;
                DMS_Cmd3.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter2 = new SqlParameter();
                parameter2.SqlDbType = SqlDbType.Structured;
                parameter2.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table3 = new DataTable();
                table3.Columns.Add("EmpID", typeof(Int32));
                table3.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item3 in doc.lstQA)
                {
                    DataRow row2 = table3.NewRow();
                    row2["EmpID"] = item3.EmpID;
                    row2["DeptID"] = item3.DeptID;
                    table3.Rows.Add(row2);
                }
                parameter2.Value = table3;
                parameter2.ParameterName = "@TblEmps";
                DMS_Cmd3.Parameters.Add(parameter2);
                DMS_Cmd3.Parameters.AddWithValue("@VersionID", id);
                DMS_Cmd3.Parameters.AddWithValue("@type", 'I');
                DMS_Cmd3.Parameters.AddWithValue("@status", 'N');
                DMS_Cmd3.Parameters.AddWithValue("@priority", priorityi);
                DMS_Cmd3.Parameters.AddWithValue("@DeptID", doc.lstQA[0].DeptID);
                DMS_Cmd3.Parameters.AddWithValue("@isManage", doc.isManage);
                DMS_Cmd3.Parameters.AddWithValue("@LoginUserID", doc.DMS_IntiatedBy);
                DMS_Cmd3.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd3.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd3.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd3.ExecuteNonQuery();
                priorityi = priorityi + 1;
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_DocumentUpdationRenew(DocObjects doc)//Renew Document Insertion and Updation
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction(IsolationLevel.ReadUncommitted, "CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                var ActionDate = DateTime.Now;
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentRenewInitiation]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocumentID", doc.DocumentID);
                DMS_Cmd.Parameters.AddWithValue("@RequestType", doc.RequestType);
                DMS_Cmd.Parameters.AddWithValue("@ModifiedBy", doc.DMS_ModifiedBy);
                DMS_Cmd.Parameters.AddWithValue("@IntiatedBy", doc.DMS_IntiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@intiateddate", DateTime.Now);
                DMS_Cmd.Parameters.AddWithValue("@Reason", doc.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@action", doc.action);
                DMS_Cmd.Parameters.AddWithValue("@ControllerID", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@CreatedBy", doc.DMS_CreatedBy);
                SqlParameter Status = DMS_Cmd.Parameters.Add("@id", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;
                SqlParameter CommentStatus = DMS_Cmd.Parameters.Add("@CommentsID", SqlDbType.Int);
                CommentStatus.Direction = ParameterDirection.Output;
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                string Result = DMS_Cmd.Parameters["@id"].Value.ToString();
                int id = Convert.ToInt32(Result);
                string Result1 = DMS_Cmd.Parameters["@CommentsID"].Value.ToString();
                int commentid = Convert.ToInt32(Result1);
                int priority = 1;
                SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd1.CommandTimeout = 120;
                DMS_Cmd1.Transaction = transaction;
                DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter = new SqlParameter();
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table1 = new DataTable();
                table1.Columns.Add("EmpID", typeof(Int32));
                table1.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item1 in doc.lstReviewBy)
                {
                    DataRow row = table1.NewRow();
                    row["EmpID"] = item1.EmpID;
                    row["DeptID"] = item1.DeptID;
                    table1.Rows.Add(row);
                }
                parameter.Value = table1;
                parameter.ParameterName = "@TblEmps";
                DMS_Cmd1.Parameters.Add(parameter);
                DMS_Cmd1.Parameters.AddWithValue("@VersionID", id);
                DMS_Cmd1.Parameters.AddWithValue("@type", 'R');
                DMS_Cmd1.Parameters.AddWithValue("@status", 'U');
                DMS_Cmd1.Parameters.AddWithValue("@priority", priority);
                DMS_Cmd1.Parameters.AddWithValue("@DeptID", doc.lstReviewBy[0].DeptID);
                DMS_Cmd1.Parameters.AddWithValue("@isManage", doc.isManage);
                DMS_Cmd1.Parameters.AddWithValue("@LoginUserID", doc.DMS_IntiatedBy);
                DMS_Cmd1.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd1.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd1.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd1.ExecuteNonQuery();
                priority = priority + 1;
                int prioritya = 1;
                SqlCommand DMS_Cmd2 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd2.CommandTimeout = 120;
                DMS_Cmd2.Transaction = transaction;
                DMS_Cmd2.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter1 = new SqlParameter();
                parameter1.SqlDbType = SqlDbType.Structured;
                parameter1.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table2 = new DataTable();
                table2.Columns.Add("EmpID", typeof(Int32));
                table2.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item2 in doc.lstApproveBy)
                {
                    DataRow row1 = table2.NewRow();
                    row1["EmpID"] = item2.EmpID;
                    row1["DeptID"] = item2.DeptID;
                    table2.Rows.Add(row1);
                }
                parameter1.Value = table2;
                parameter1.ParameterName = "@TblEmps";
                DMS_Cmd2.Parameters.Add(parameter1);
                DMS_Cmd2.Parameters.AddWithValue("@VersionID", id);
                DMS_Cmd2.Parameters.AddWithValue("@type", 'A');
                DMS_Cmd2.Parameters.AddWithValue("@status", 'U');
                DMS_Cmd2.Parameters.AddWithValue("@priority", prioritya);
                DMS_Cmd2.Parameters.AddWithValue("@DeptID", doc.lstApproveBy[0].DeptID);
                DMS_Cmd2.Parameters.AddWithValue("@isManage", doc.isManage);
                DMS_Cmd2.Parameters.AddWithValue("@LoginUserID", doc.DMS_IntiatedBy);
                DMS_Cmd2.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd2.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd2.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd2.ExecuteNonQuery();
                prioritya = prioritya + 1;
                int priorityi = 1;
                SqlCommand DMS_Cmd3 = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionmultiple]", DMS_Con);
                DMS_Cmd3.CommandTimeout = 120;
                DMS_Cmd3.Transaction = transaction;
                DMS_Cmd3.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter2 = new SqlParameter();
                parameter2.SqlDbType = SqlDbType.Structured;
                parameter2.TypeName = "[DMS].[AizantIT_EmpDepIDS]";
                DataTable table3 = new DataTable();
                table3.Columns.Add("EmpID", typeof(Int32));
                table3.Columns.Add("DeptID", typeof(Int32));
                foreach (Int_EmployeeBy item3 in doc.lstQA)
                {
                    DataRow row2 = table3.NewRow();
                    row2["EmpID"] = item3.EmpID;
                    row2["DeptID"] = item3.DeptID;
                    table3.Rows.Add(row2);
                }
                parameter2.Value = table3;
                parameter2.ParameterName = "@TblEmps";
                DMS_Cmd3.Parameters.Add(parameter2);
                DMS_Cmd3.Parameters.AddWithValue("@VersionID", id);
                DMS_Cmd3.Parameters.AddWithValue("@type", 'I');
                DMS_Cmd3.Parameters.AddWithValue("@status", 'U');
                DMS_Cmd3.Parameters.AddWithValue("@priority", priorityi);
                DMS_Cmd3.Parameters.AddWithValue("@DeptID", doc.lstQA[0].DeptID);
                DMS_Cmd3.Parameters.AddWithValue("@isManage", doc.isManage);
                DMS_Cmd3.Parameters.AddWithValue("@LoginUserID", doc.DMS_IntiatedBy);
                DMS_Cmd3.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd3.Parameters.AddWithValue("@LinkCommentID", commentid);
                DMS_Cmd3.Parameters.AddWithValue("@CommentDate", ActionDate);
                DMS_Cmd3.ExecuteNonQuery();
                priorityi = priorityi + 1;
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion RenewDocInitiation
        #region effectivedate
        public int DMS_inserteffectivedate(DocObjects doc, ref SqlTransaction transaction, ref SqlConnection DMS_Con)
        {
            DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();

            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                transaction = DMS_Con.BeginTransaction("EffectiveDoc");
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_InsertEffectivedate]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@effective", doc.effective);
                DMS_Cmd.Parameters.AddWithValue("@expiration", doc.expiration);
                DMS_Cmd.Parameters.AddWithValue("@DocVersionID", Convert.ToInt32(doc.DocumentID));
                DMS_Cmd.Parameters.AddWithValue("@empid", doc.DMS_IntiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@date", DateTime.Now);
                DMS_Cmd.Parameters.AddWithValue("@reason", doc.Purpose);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {

            }
            return DMS_Return;
        }
        #endregion effectivedate
        #region renewdate
        public int DMS_insertRenewdate(DocObjects doc, ref SqlTransaction transaction, ref SqlConnection DMS_Con)
        {
            DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();

            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentDateExtension]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@empID", doc.DMS_IntiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@type", doc.TypeID);
                DMS_Cmd.Parameters.AddWithValue("@expiration", doc.expiration);
                DMS_Cmd.Parameters.AddWithValue("@date", DateTime.Now);
                DMS_Cmd.Parameters.AddWithValue("@isPDF", doc.FileType2);
                DMS_Cmd.Parameters.AddWithValue("@Filename", doc.Filename);
                DMS_Cmd.Parameters.AddWithValue("@dyFilename", doc.FileName2);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                //transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {

            }
            return DMS_Return;
        }
        #endregion renewdate

        #region Searches
        public DataTable DMS_DocumentNameSearch(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentNameSearch]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocumentName", doc.DMS_DocumentName);
                DMS_Cmd.Parameters.AddWithValue("@dept", Convert.ToInt32(doc.dept));
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_DocumentNumberSearch(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentNumberSearch]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocumentNumber", doc.documentNUmber);

                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_TemplateNameSearch(ReviewObjects reviewObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_TemplateNameSearch]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@template", reviewObjects.TemplateName);

                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_FormNumberSearch(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_FormNumberSearch]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormNumber", doc.FormNumber);

                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_FormNameSearch(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_FormNameSearch]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormName", doc.FormName);
                DMS_Cmd.Parameters.AddWithValue("@dept", doc.DMS_Deparment);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_LatestActiveDoc(int VersionID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetActiveDocumentVersionID]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVID", VersionID);

                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_LatestActiveForm(int FormVersionID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetActiveFormVersionID]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVID", FormVersionID);

                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion Searches

        #region jquerydatatables
        public DataTable DMS_GetDocumentTypeList(JQDataTableBO _objJQDataTableBO)//DocumentType Filling
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentType]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_ProcessType", _objJQDataTableBO.sSearchProcessType);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_IsTraining", _objJQDataTableBO.sSearchIsTraining);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_ReviewPeriod", _objJQDataTableBO.sSearch_ReviewPeriodP);
            
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetTemplateNameList(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetTemplateNameList]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetNewDocCreatorList(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetNewDocCreatorList]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@CreatedBy", _objJQDataTableBO.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_InitiatedBy", _objJQDataTableBO.sSearchInitiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestType", _objJQDataTableBO.sSearchRequestType);

                DMS_Cmd.Parameters.AddWithValue("@Rtyp", _objJQDataTableBO.RType);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetNewDocQAList(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetDocumentListForApprover1]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@ToEmpID", _objJQDataTableBO.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_InitiatedBy", _objJQDataTableBO.sSearchInitiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Status", _objJQDataTableBO.sSearchStatus);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestType", _objJQDataTableBO.sSearchRequestType);

                DMS_Cmd.Parameters.AddWithValue("@Rtyp", _objJQDataTableBO.RType);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetNewDocIntiatorList(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetDocumentListForIntiator1]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_InitiatedBy", _objJQDataTableBO.sSearchInitiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Status", _objJQDataTableBO.sSearchStatus);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestType", _objJQDataTableBO.sSearchRequestType);

                DMS_Cmd.Parameters.AddWithValue("@intiator", _objJQDataTableBO.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@Rtyp", _objJQDataTableBO.RType);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetManageDocumentList(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ManageDocumentList]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_InitiatedBy", _objJQDataTableBO.sSearchInitiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Status", _objJQDataTableBO.sSearchStatus);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestType", _objJQDataTableBO.sSearchRequestType);

                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_Listofdocuments(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                if (_objJQDataTableBO.ChartStatus == 0)
                {
                    SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ListOFDocuments]", DMS_Con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                    DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                    DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                    DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                    DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                    DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                    DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                    DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_VersionNumber", _objJQDataTableBO.sSearchVersionNumber);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_EffectiveDate", _objJQDataTableBO.sSearchEffectiveDate);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_ExpirationDate", _objJQDataTableBO.sSearchExpirationDate);

                    DMS_Cmd.Parameters.AddWithValue("@EmpID", _objJQDataTableBO.EmpID);
                    SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                    DMS_da.Fill(DMS_Dt);
                }
                ArrayList intarray = new ArrayList() { 1, 2, 3 };
                if (intarray.Contains(_objJQDataTableBO.ChartStatus))
                {
                    SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetReaderChartList]", DMS_Con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.ChartStatus);
                    DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                    DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                    DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                    DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                    DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                    DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                    DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_VersionNumber", _objJQDataTableBO.sSearchVersionNumber);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_EffectiveDate", _objJQDataTableBO.sSearchEffectiveDate);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_ExpirationDate", _objJQDataTableBO.sSearchExpirationDate);

                    DMS_Cmd.Parameters.AddWithValue("@DocTypeId", _objJQDataTableBO.ChartDocTypeID);
                    DMS_Cmd.Parameters.AddWithValue("@DocDepartmentID", _objJQDataTableBO.ChartDeptID);
                    DMS_Cmd.Parameters.AddWithValue("@FromDate", _objJQDataTableBO.ChartFromDate);
                    DMS_Cmd.Parameters.AddWithValue("@ToDate", _objJQDataTableBO.ChartToDate);
                    DMS_Cmd.Parameters.AddWithValue("@DeptCode", _objJQDataTableBO.ChartDeptCode);
                    DMS_Cmd.Parameters.AddWithValue("@DocTypeName", _objJQDataTableBO.ChartDocTypeName);
                    DMS_Cmd.Parameters.AddWithValue("@EmpID", _objJQDataTableBO.EmpID);
                    SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                    DMS_da.Fill(DMS_Dt);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_Listofeffectivedocuments(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_TobeEffectiveDocuments]", DMS_Con);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", _objJQDataTableBO.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_VersionNumber", _objJQDataTableBO.sSearchVersionNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_EffectiveDate", _objJQDataTableBO.sSearchEffectiveDate);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_ExpirationDate", _objJQDataTableBO.sSearchExpirationDate);
                DMS_Cmd.Parameters.AddWithValue("@Rtyp", _objJQDataTableBO.RType);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_ExtendDocument(JQDataTableBO _objJQDataTableBO, out int TotalRecordCount)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ExtendDocuments]", DMS_Con);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", _objJQDataTableBO.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_ExpirationDate", _objJQDataTableBO.sSearchExpirationDate);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestType", _objJQDataTableBO.sSearchRequestType);
                DMS_Cmd.Parameters.AddWithValue("@Rtyp", _objJQDataTableBO.RType);
                DMS_Cmd.Parameters.Add("@TotalRecordCount", SqlDbType.Int);
                DMS_Cmd.Parameters["@TotalRecordCount"].Direction = ParameterDirection.Output;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
                TotalRecordCount = Convert.ToInt32(DMS_Cmd.Parameters["@TotalRecordCount"].Value);
                //if (_objJQDataTableBO.iMode != 2)
                //{
                //    ArrayList arr = new ArrayList();
                //    for (int i = 0; i < DMS_Dt.Rows.Count; i++)
                //    {
                //        string Doc_VID = DMS_Dt.Rows[i]["DocumentID"].ToString();
                //        DataTable DMS_Dt1 = new DataTable();
                //        SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_GetNoOFAttempsts]", DMS_Con);
                //        DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                //        DMS_Cmd1.Parameters.AddWithValue("@DocID", Convert.ToInt32(Doc_VID));
                //        SqlDataAdapter DMS_da1 = new SqlDataAdapter(DMS_Cmd1);
                //        DMS_da1.Fill(DMS_Dt1);
                //        int a = DMS_Dt1.Rows.Count;
                //        //string id_status;
                //        if (a > 0)
                //        {
                //            if (a >= 2)
                //            {
                //                arr.Add(Doc_VID);
                //            }
                //        }
                //    }
                //    foreach (DataRow dr in DMS_Dt.Rows)
                //    {
                //        if (arr.Contains(dr["DocumentID"].ToString()))
                //        {
                //            dr.Delete();
                //        }
                //    }
                //    DMS_Dt.AcceptChanges();
                //}
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetPendingDocCreatorList(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetPendingDocCreatorList]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@CreatedBy", _objJQDataTableBO.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_InitiatedBy", _objJQDataTableBO.sSearchInitiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestType", _objJQDataTableBO.sSearchRequestType);

                DMS_Cmd.Parameters.AddWithValue("@Rtyp", _objJQDataTableBO.RType);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #region ExistingDocumentsList
        public DataTable DMS_GetExistingDocuments(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GETExistingDocuments]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@VID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_VersionNumber", _objJQDataTableBO.sSearchVersionNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_EffectiveDate", _objJQDataTableBO.sSearchEffectiveDate);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_ExpirationDate", _objJQDataTableBO.sSearchExpirationDate);

                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion ExistingDocumentsList
        #region AdminDocumentList
        public DataTable DMS_AdminListOFDocuments(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                if (_objJQDataTableBO.ChartStatus == 0)
                {
                    SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_AdminListOFDocuments]", DMS_Con);
                    DMS_Cmd.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                    DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                    DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                    DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                    DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                    DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                    DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                    DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_VersionNumber", _objJQDataTableBO.sSearchVersionNumber);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestType", _objJQDataTableBO.sSearchRequestType);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_EffectiveDate", _objJQDataTableBO.sSearchEffectiveDate);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_ExpirationDate", _objJQDataTableBO.sSearchExpirationDate);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_Status", _objJQDataTableBO.sSearchStatus);
                    DMS_Cmd.Parameters.AddWithValue("@EmpID", _objJQDataTableBO.EmpID);
                    DMS_Cmd.Parameters.AddWithValue("@IsJQueryList", _objJQDataTableBO.IsJQueyList);
                    SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                    DMS_da.Fill(DMS_Dt);
                }
                ArrayList intarray = new ArrayList() { 1, 2, 3, 4, 5, 6, 7, 8 };
                if (intarray.Contains(_objJQDataTableBO.ChartStatus))
                {
                    SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetAdminChartList]", DMS_Con);
                    DMS_Cmd.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.ChartStatus);
                    DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                    DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                    DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                    DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                    DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                    DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                    DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_VersionNumber", _objJQDataTableBO.sSearchVersionNumber);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestType", _objJQDataTableBO.sSearchRequestType);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_EffectiveDate", _objJQDataTableBO.sSearchEffectiveDate);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_ExpirationDate", _objJQDataTableBO.sSearchExpirationDate);
                    DMS_Cmd.Parameters.AddWithValue("@sSearch_Status", _objJQDataTableBO.sSearchStatus);

                    DMS_Cmd.Parameters.AddWithValue("@EmpID", _objJQDataTableBO.EmpID);
                    DMS_Cmd.Parameters.AddWithValue("@DocTypeId", _objJQDataTableBO.ChartDocTypeID);
                    DMS_Cmd.Parameters.AddWithValue("@DocDepartmentID", _objJQDataTableBO.ChartDeptID);
                    DMS_Cmd.Parameters.AddWithValue("@FromDate", _objJQDataTableBO.ChartFromDate);
                    DMS_Cmd.Parameters.AddWithValue("@ToDate", _objJQDataTableBO.ChartToDate);
                    DMS_Cmd.Parameters.AddWithValue("@docStatus", _objJQDataTableBO.DocStatus);
                    DMS_Cmd.Parameters.AddWithValue("@DeptCode", _objJQDataTableBO.ChartDeptCode);
                    DMS_Cmd.Parameters.AddWithValue("@DocTypeName", _objJQDataTableBO.ChartDocTypeName);
                    DMS_Cmd.Parameters.AddWithValue("@IsJQueryList", _objJQDataTableBO.IsJQueyList);
                    SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                    DMS_da.Fill(DMS_Dt);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion AdminDocumentList
        #region GetDocPrintReqList
        public DataSet DMS_DocPrintReqList(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataSet ds = new DataSet();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetDocPrintRequestList]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestNumber", _objJQDataTableBO.sSearchRequestNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentVersionNumber", _objJQDataTableBO.sSearchVersionNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_PrintPurpose", _objJQDataTableBO.sSearchPurpose);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_PageNumbers", _objJQDataTableBO.sSearchPageNumbers);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_ApprovedBy", _objJQDataTableBO.sSearchApprovedBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestBy", _objJQDataTableBO.sSearchRequestBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestedDate", _objJQDataTableBO.sSearchRequestedDate);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_ActionName", _objJQDataTableBO.sSearchActionName);
                DMS_Cmd.Parameters.AddWithValue("@ToEmpID", _objJQDataTableBO.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@ForRole", _objJQDataTableBO.RType);
                DMS_Cmd.Parameters.AddWithValue("@IsJqueryDataList", _objJQDataTableBO.IsJQueyList);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return ds;
        }
        #endregion GetDocPrintReqList
        #region GetFormPrintRequestList
        public DataSet DMS_GetFormPrintRequestList(JQDataTableBO _objJQDataTableBO)

        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataSet ds = new DataSet();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetFormPrintRequestList]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestNumber", _objJQDataTableBO.sSearchRequestNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_FormName", _objJQDataTableBO.sSearchFormName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_FormNumber", _objJQDataTableBO.sSearchFormNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_FormVersionNumber", _objJQDataTableBO.sSearchVersionNumber);

                DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestPurpose", _objJQDataTableBO.sSearchPurpose);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_NoofCopies", _objJQDataTableBO.sSearchNoofCopies);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_ApprovedBy", _objJQDataTableBO.sSearchApprovedBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestBy", _objJQDataTableBO.sSearchRequestBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestedDate", _objJQDataTableBO.sSearchRequestedDate);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_ActionName", _objJQDataTableBO.sSearchActionName);
                DMS_Cmd.Parameters.AddWithValue("@ToEmpID", _objJQDataTableBO.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@ForRole", _objJQDataTableBO.RType);
                DMS_Cmd.Parameters.AddWithValue("@Verification", _objJQDataTableBO.Verify);
                DMS_Cmd.Parameters.AddWithValue("@IsJqueryDataList", _objJQDataTableBO.IsJQueyList);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return ds;
        }
        #endregion GetFormPrintRequestList
        #region GetFormList
        public DataSet DMS_GetFormList(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataSet ds = new DataSet();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetFormList]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                DMS_Cmd.Parameters.AddWithValue("@sSearch_FormNumber", _objJQDataTableBO.sSearchFormNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_FormName", _objJQDataTableBO.sSearchFormName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Purpose", _objJQDataTableBO.sSearchPurpose);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_VersionNumber", _objJQDataTableBO.sSearchVersionNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_CreatedBy", _objJQDataTableBO.sSearchCreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_ApprovedBy", _objJQDataTableBO.sSearchApprovedBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_ActionName", _objJQDataTableBO.sSearchActionName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestType", _objJQDataTableBO.sSearchRequestType);

                DMS_Cmd.Parameters.AddWithValue("@ToEmpID", _objJQDataTableBO.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@ForRole", _objJQDataTableBO.RType);
                DMS_Cmd.Parameters.AddWithValue("@RType", _objJQDataTableBO.ReqTypeID);
                DMS_Cmd.Parameters.AddWithValue("@IsJqueryDataList", _objJQDataTableBO.IsJQueyList);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return ds;
        }
        #endregion GetFormList
        public DataTable DMS_GetWithdrawDocList(JQDataTableBO _objJQDataTableBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetDocumentListForWithdrawal]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@ToEmpID", _objJQDataTableBO.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@mode", _objJQDataTableBO.iMode);
                DMS_Cmd.Parameters.AddWithValue("@pkID", _objJQDataTableBO.pkid);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentNumber", _objJQDataTableBO.sSearchDocumentNumber);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentName", _objJQDataTableBO.sSearchDocumentName);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Department", _objJQDataTableBO.sSearchDepartment);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_DocumentType", _objJQDataTableBO.sSearchDocumentType);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_InitiatedBy", _objJQDataTableBO.sSearchInitiatedBy);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_Status", _objJQDataTableBO.sSearchStatus);
                DMS_Cmd.Parameters.AddWithValue("@sSearch_RequestType", _objJQDataTableBO.sSearchRequestType);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion jquerydatatables

        #region GetDetailsFormDB
        public DataTable DMS_GetDocByID(string DMS_DocumentID, ref SqlTransaction _transaction, ref SqlConnection DMS_Con)
        {

            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetDocumentByID]", DMS_Con);
                DMS_Cmd.Transaction = _transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", Convert.ToInt32(DMS_DocumentID));
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {

            }
            return DMS_Dt;
        }
        public DataTable DMS_GetDocByID(string DMS_DocumentID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetDocumentByID]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", Convert.ToInt32(DMS_DocumentID));
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetTempalteContent(string Tempalte_ID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetTemplateContent]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@id", Convert.ToInt32(Tempalte_ID));
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetRejectCommentsByID(string DMS_DocumentID, string ProcessID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetRejectComments]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@id", Convert.ToInt32(DMS_DocumentID));
                DMS_Cmd.Parameters.AddWithValue("@Pid", Convert.ToInt32(ProcessID));
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetsavedCommentsByID(ReviewObjects reviewObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetsavedComments]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", reviewObjects.Id);
                DMS_Cmd.Parameters.AddWithValue("@empid", reviewObjects.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@status", reviewObjects.DocStatus);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetNoOFAttempts(string DMS_DocumentID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetNoOFAttempsts]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocID", Convert.ToInt32(DMS_DocumentID));
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetsavedTimeByID(ReviewObjects reviewObjects)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetsavedTime]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", reviewObjects.Id);
                DMS_Cmd.Parameters.AddWithValue("@empid", reviewObjects.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@status", reviewObjects.DocStatus);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #region Version
        public DataTable DMS_GetVersionID(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetVersion]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocumentID", doc.DocumentID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion Version
        #region DocumentRevisionValidation
        public int DMS_DocumentTrainingCount(int ActionType, int DocumentID, int VersionID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            string strQuery = "Select DMS.AizantIT_UDF_DocumentTrainingCount('" + ActionType + "','" + DocumentID + "','" + VersionID + "')";
            try
            {
                SqlCommand DMS_Cmd = new SqlCommand(strQuery, DMS_Con);
                DMS_Cmd.CommandType = CommandType.Text;
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                DMS_Return = Convert.ToInt32(DMS_Cmd.ExecuteScalar().ToString());
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_PendingDocRequestCount(int VersionID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            string strQuery = "Select DMS.AizantIT_UDF_PendingDocRequestCount('" + VersionID + "')";
            try
            {
                SqlCommand DMS_Cmd = new SqlCommand(strQuery, DMS_Con);
                DMS_Cmd.CommandType = CommandType.Text;
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                DMS_Return = Convert.ToInt32(DMS_Cmd.ExecuteScalar().ToString());
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_PendingFormRequestCount(int VersionID, int Status)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            string strQuery = "Select DMS.AizantIT_UDF_PendingFormRequestCount('" + VersionID + "','" + Status + "')";
            try
            {
                SqlCommand DMS_Cmd = new SqlCommand(strQuery, DMS_Con);
                DMS_Cmd.CommandType = CommandType.Text;
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                DMS_Return = Convert.ToInt32(DMS_Cmd.ExecuteScalar().ToString());
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_PendingFormRequestsByUserCount(int FormVersionID, int RequestByID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            string strQuery = "Select DMS.AizantIT_UDF_PendingFormRequestsByUserCount('" + FormVersionID + "','" + RequestByID + "')";
            try
            {
                SqlCommand DMS_Cmd = new SqlCommand(strQuery, DMS_Con);
                DMS_Cmd.CommandType = CommandType.Text;
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                DMS_Return = Convert.ToInt32(DMS_Cmd.ExecuteScalar().ToString());
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public bool TMS_AllowDocumentActions(int ActionType, int DocumentID, int VersionID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            bool DMS_Return = false;
            string strQuery = "Select [TMS].[AizantIT_UDF_AllowDocumentActions]('" + ActionType + "','" + DocumentID + "','" + VersionID + "')";
            try
            {
                SqlCommand DMS_Cmd = new SqlCommand(strQuery, DMS_Con);
                DMS_Cmd.CommandType = CommandType.Text;
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                DMS_Return = Convert.ToBoolean(DMS_Cmd.ExecuteScalar().ToString());
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }

        public bool DMS_AllowDocumentActions(int ActionType, int DocumentID, int VersionID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            bool DMS_Return = false;
            //string strQuery = "Select [TMS].[AizantIT_UDF_AllowDocumentActions]('" + ActionType + "','" + DocumentID + "','" + VersionID + "')";
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("DMS.AizantIT_SP_AllowDocumentActions", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@CheckType", ActionType);
                DMS_Cmd.Parameters.AddWithValue("@DocumentID", DocumentID);
                DMS_Cmd.Parameters.AddWithValue("@DocVersionID", VersionID);
                DMS_Cmd.Parameters.Add("@Result", SqlDbType.Bit);
                DMS_Cmd.Parameters["@Result"].Direction = ParameterDirection.Output;
                DMS_Cmd.ExecuteNonQuery();
                DMS_Return = Convert.ToBoolean(DMS_Cmd.Parameters["@Result"].Value);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion DocumentRevisionValidation       
        #region NotificationDocVersionID
        public DataTable DMS_NotificationDocVersionID(int NotificationID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("DMS.AizantIT_SP_GetNotificationDocVersionID", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@NotifiationID", NotificationID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion NotificationDocVersionID
        public DataTable DMS_NotificationFormVersionID(int NotificationID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("DMS.AizantIT_SP_GetNotificationFormVersionID", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@NotifiationID", NotificationID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_NotificationDocRequestID(int NotificationID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("DMS.[AizantIT_SP_GetNotificationDocRequestID]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@NotifiationID", NotificationID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_NotificationFormRequestID(int NotificationID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("DMS.[AizantIT_SP_GetNotificationFormRequestID]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@NotifiationID", NotificationID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #region ReportFirstPageData
        public DataSet DMS_ShowFirstPageDetails(string DMS_VersionID, ref SqlTransaction _transaction, ref SqlConnection DMS_Con)
        {

            //int DMS_Return = 0;
            DataSet DMS_Dt = new DataSet();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ShowFirstPageDetails]", DMS_Con);
                DMS_Cmd.Transaction = _transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", Convert.ToInt32(DMS_VersionID));
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {

            }
            return DMS_Dt;
        }
        public DataSet DMS_ShowFirstPageDetails(string DMS_VersionID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataSet DMS_Dt = new DataSet();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ShowFirstPageDetails]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", Convert.ToInt32(DMS_VersionID));
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion ReportFirstPageData
        public DataTable DMS_GetTempRevertedReviewersDetails(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetTempRevertedReviewersDetails]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@mode", doc.Mode);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_DocComments(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocComments]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@RoleID", doc.RoleID);
                DMS_Cmd.Parameters.AddWithValue("@Comments", doc.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@CommentDate", DateTime.Now);
                DMS_Cmd.Parameters.AddWithValue("@Time", doc.Time);
                DMS_Cmd.Parameters.AddWithValue("@ActionStatusID", doc.action);
                DMS_Cmd.Parameters.AddWithValue("@CommentType", doc.CommentType);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetFormCopyNum(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetFormRequestCopyNumbers]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormRequestID", doc.FormReqID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion GetDetailsFormDB

        #region CreatorActions 
        public int DMS_UpdateContentByID(ReviewObjects reviewObjects)//Save Content By Creator
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_SaveContent]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", reviewObjects.Id);
                //DMS_Cmd.Parameters.AddWithValue("@Content", reviewObjects.Content);
                DMS_Cmd.Parameters.AddWithValue("@FileType", reviewObjects.Ext);
                DMS_Cmd.Parameters.AddWithValue("@FileName", reviewObjects.TemplateName);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_DocumentToReviewer(DocUploadBO reviewObjects)//Document submited to reviewer
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                if (reviewObjects.docLocation != null)
                {
                    SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_InsertDocumentLocation]", DMS_Con);
                    //DMS_Cmd.CommandTimeout = 120;
                    DMS_Cmd.Transaction = transaction;
                    DMS_Cmd.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd.Parameters.AddWithValue("@DocVersionID", reviewObjects.docLocation.Version);
                    DMS_Cmd.Parameters.AddWithValue("@RoleID", reviewObjects.docLocation.RoleID);
                    DMS_Cmd.Parameters.AddWithValue("@EmpID", reviewObjects.docLocation.EmpID);
                    DMS_Cmd.Parameters.AddWithValue("@FilePath1", reviewObjects.docLocation.FilePath1);
                    DMS_Cmd.Parameters.AddWithValue("@FilePath2", reviewObjects.docLocation.FilePath2);
                    DMS_Cmd.Parameters.AddWithValue("@isTemp", reviewObjects.docLocation.isTemp);
                    DMS_Cmd.Parameters.AddWithValue("@FileType1", reviewObjects.docLocation.FileType1);
                    DMS_Cmd.Parameters.AddWithValue("@FileType2", reviewObjects.docLocation.FileType2);
                    DMS_Cmd.Parameters.AddWithValue("@FileName1", reviewObjects.docLocation.FileName1);
                    DMS_Cmd.Parameters.AddWithValue("@FileName2", reviewObjects.docLocation.FileName2);
                    DMS_Return = DMS_Cmd.ExecuteNonQuery();
                }

                if (reviewObjects.docContent != null)
                {
                    SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_SaveContent]", DMS_Con);
                    DMS_Cmd1.Transaction = transaction;
                    DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd1.Parameters.AddWithValue("@VersionID", reviewObjects.docContent.Id);
                    //DMS_Cmd.Parameters.AddWithValue("@Content", reviewObjects.Content);
                    DMS_Cmd1.Parameters.AddWithValue("@FileType", reviewObjects.docContent.Ext);
                    DMS_Cmd1.Parameters.AddWithValue("@FileName", reviewObjects.docContent.TemplateName);
                    DMS_Return = DMS_Cmd1.ExecuteNonQuery();
                }

                if (reviewObjects.docComments != null)
                {
                    SqlCommand DMS_Cmd2 = new SqlCommand("[DMS].[AizantIT_SP_DocComments]", DMS_Con);
                    //DMS_Cmd.CommandTimeout = 120;
                    DMS_Cmd2.Transaction = transaction;
                    DMS_Cmd2.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd2.Parameters.AddWithValue("@DocVID", reviewObjects.docComments.Version);
                    DMS_Cmd2.Parameters.AddWithValue("@EmpID", reviewObjects.docComments.EmpID);
                    DMS_Cmd2.Parameters.AddWithValue("@RoleID", reviewObjects.docComments.RoleID);
                    DMS_Cmd2.Parameters.AddWithValue("@Comments", reviewObjects.docComments.Remarks);
                    DMS_Cmd2.Parameters.AddWithValue("@CommentDate", DateTime.Now);
                    DMS_Cmd2.Parameters.AddWithValue("@Time", reviewObjects.docComments.Time);
                    DMS_Cmd2.Parameters.AddWithValue("@ActionStatusID", reviewObjects.docComments.action);
                    DMS_Cmd2.Parameters.AddWithValue("@CommentType", reviewObjects.docComments.CommentType);
                    SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd2);
                    DMS_da.Fill(DMS_Dt);
                }

                if (reviewObjects.docReviwer != null)
                {
                    SqlCommand DMS_Cmd3 = new SqlCommand("[DMS].[AizantIT_SP_SubmitToReviewer]", DMS_Con);
                    DMS_Cmd3.Transaction = transaction;
                    DMS_Cmd3.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd3.Parameters.AddWithValue("@VersionID", reviewObjects.docReviwer.Id);
                    DMS_Cmd3.Parameters.AddWithValue("@Creator", reviewObjects.docReviwer.Creator);
                    DMS_Cmd3.Parameters.AddWithValue("@date", DateTime.Now);
                    DMS_Cmd3.Parameters.AddWithValue("@Comments", reviewObjects.docReviwer.Comments);
                    DMS_Return = DMS_Cmd3.ExecuteNonQuery();
                }
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                DMS_Return = 0;
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion CreatorActions 

        #region QAActions
        public int DMS_ApproveDocument(ReviewObjects reviewObjects)//Approved by QA
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ApproveDocument]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@versionID", reviewObjects.Id);
                DMS_Cmd.Parameters.AddWithValue("@Status", reviewObjects.Status);
                DMS_Cmd.Parameters.AddWithValue("@EmpName", reviewObjects.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@Reason", reviewObjects.Comments);
                DMS_Cmd.Parameters.AddWithValue("@date", DateTime.Now);
                SqlParameter LastCount = DMS_Cmd.Parameters.Add("@LastCount", SqlDbType.Int);
                LastCount.Direction = ParameterDirection.Output;
                DMS_Cmd.ExecuteNonQuery();
                string Result = DMS_Cmd.Parameters["@LastCount"].Value.ToString();
                DMS_Return = Convert.ToInt32(Result);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_RejectDocument(ReviewObjects reviewObjects)//Reverted by QA
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_RejectDocument]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", reviewObjects.Id);
                DMS_Cmd.Parameters.AddWithValue("@DocStatus", reviewObjects.DocStatus);
                DMS_Cmd.Parameters.AddWithValue("@Reason", reviewObjects.Comments);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", reviewObjects.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@date", DateTime.Now);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion QAActions        

        #region Referral links
        public DataTable DMS_GetInternalDocumentsbyDepartment(int DocId)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            DataTable DMS_Dt = new DataTable();

            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetInternalDocumentsbyDepartment]", DMS_Con);
                DMS_Cmd.Parameters.AddWithValue("@DeptID", DocId);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public int DMS_insertLinkReferences(ReferralBO obj_referralBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_InsertLinkReferences]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", obj_referralBO.Doc_VersionID);
                DMS_Cmd.Parameters.AddWithValue("@Content", obj_referralBO.Doc_RefContent);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_insertReferalInternalDocs(List<ReferralBO> lst_referralBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                DataTable dtRefIds = new DataTable();
                dtRefIds.Columns.Add("DeptID", typeof(Int32));
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                foreach (ReferralBO obj_ReferralBO in lst_referralBO)
                {
                    SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_InsertReferalInternalDocs]", DMS_Con);
                    DMS_Cmd.CommandTimeout = 120;
                    DMS_Cmd.Transaction = transaction;
                    DMS_Cmd.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd.Parameters.AddWithValue("@VersionID", obj_ReferralBO.Doc_VersionID);
                    DMS_Cmd.Parameters.AddWithValue("@Ref_Doc_VersionID", obj_ReferralBO.Doc_Ref_VersionID);
                    DMS_Return = DMS_Cmd.ExecuteNonQuery();
                    dtRefIds.Rows.Add(obj_ReferralBO.Doc_Ref_VersionID);
                }
                if (lst_referralBO.Count > 0)
                {
                    SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_DeleteReferalInternalDocs]", DMS_Con);
                    DMS_Cmd1.CommandTimeout = 120;
                    DMS_Cmd1.Transaction = transaction;
                    DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd1.Parameters.AddWithValue("@VersionID", lst_referralBO[0].Doc_VersionID);
                    var pList = new SqlParameter("@Ref_Doc_VersionIDs", SqlDbType.Structured);
                    pList.TypeName = "DMS.AizantIT_DocIDS";
                    pList.Value = dtRefIds;
                    DMS_Cmd1.Parameters.Add(pList);
                    DMS_Return = DMS_Cmd1.ExecuteNonQuery();
                }
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_insertReferalExternalFiles(ReferralBO obj_ReferralBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_InsertAizantIT_ReferalExternalFiles]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocExternal_VersionID", obj_ReferralBO.Doc_VersionID);
                DMS_Cmd.Parameters.AddWithValue("@FileName", obj_ReferralBO.Doc_FileName);
                DMS_Cmd.Parameters.AddWithValue("@FileExtension", obj_ReferralBO.Doc_FileExtension);
                DMS_Cmd.Parameters.AddWithValue("@FileContent", obj_ReferralBO.Doc_FileContent);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public string DMS_GetLinkReferences(int VersionID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            string strLinkRef = string.Empty;
            try
            {
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetLinkReferences]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", VersionID);
                object a = DMS_Cmd.ExecuteScalar();
                if (a != null)
                    strLinkRef = Convert.ToString(a);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return strLinkRef;
        }
        public DataTable DMS_GetReferalInternalDocs(int VersionID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetReferalInternalDocs]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@VersionID", VersionID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetReferalExternalFiles(int VersionID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetReferalExternalFiles]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocExternal_VersionID", VersionID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public int DMS_GetExistingFileCount(string File_Name, int VersionID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int FileCount = 0;
            try
            {
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ValidateReferalExternalFiles]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocExternal_FileName", File_Name);
                DMS_Cmd.Parameters.AddWithValue("@DocExternal_VersionID", VersionID);
                object a = DMS_Cmd.ExecuteScalar();
                if (a != null)
                    FileCount = Convert.ToInt32(Convert.ToString(a));
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return FileCount;
        }
        public byte[] DMS_GetExternalFileContent(int FileID)
        {
            // Read Byte [] Value from Sql Table        
            byte[] byteData = null;
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            try
            {
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetReferalExternalFilesContent]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@Ref_FileID", FileID);
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                SqlDataReader reader = DMS_Cmd.ExecuteReader();
                if (reader.Read())
                {
                    byteData = (byte[])reader[0];
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return byteData;
        }
        public int DMS_DeleteExternalFiles(int @Ref_FileID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DeleteReferalExternalFiles]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@Ref_FileID", @Ref_FileID);
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public DataTable DMS_TempCrudExternalFiles(TempExternalFileBO tempExternalFileBO)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_CRUDTempRefExternalFiles]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@CrudType", tempExternalFileBO.CrudType);
                DMS_Cmd.Parameters.AddWithValue("@Ref_FileID", tempExternalFileBO.Ref_FileID);
                DMS_Cmd.Parameters.AddWithValue("@FileName", tempExternalFileBO.FileName);
                DMS_Cmd.Parameters.AddWithValue("@FileExtension", tempExternalFileBO.FileExtension);
                DMS_Cmd.Parameters.AddWithValue("@FileContent", tempExternalFileBO.FileContent);
                DMS_Cmd.Parameters.AddWithValue("@DocExternal_VersionID", tempExternalFileBO.DocExternal_VersionID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion Referral links

        #region StatusNoficationCards
        public int RevertedInitator(int EmpID, string Status)
        {
            int totalcount = 0;
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);

            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_InitiatedRevertedStatus]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@EMPID", EmpID);
                DMS_Cmd.Parameters.AddWithValue("@Status", Status);
                totalcount = (int)DMS_Cmd.ExecuteScalar();
                return totalcount;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
        }
        #region StatusNoficationByRequestType
        public DataTable StatusNoficationByRequestType_dt(int EmpID, string Status, int RequestTypeID)
        {
            //int totalcount = 0;
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_NotificationCardsBasedOnRequestType]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@EMPID", EmpID);
                DMS_Cmd.Parameters.AddWithValue("@Status", Status);
                DMS_Cmd.Parameters.AddWithValue("@RequestTypeID", RequestTypeID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public int StatusNoficationByRequestType(int EmpID, string Status, int RequestTypeID)
        {
            int totalcount = 0;
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_NotificationCardsBasedOnRequestType]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@EMPID", EmpID);
                DMS_Cmd.Parameters.AddWithValue("@Status", Status);
                DMS_Cmd.Parameters.AddWithValue("@RequestTypeID", RequestTypeID);
                totalcount = (int)DMS_Cmd.ExecuteScalar();
                return totalcount;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
        }
        #endregion StatusNoficationByRequestType
        #region GetPrintDistributionNotificationCards
        public DataSet DMS_GetPrintDistributionNotificationCards(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataSet ds = new DataSet();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_PrintDistributionNotificationCards]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@EMPID", doc.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@Status", doc.Status);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return ds;
        }
        #endregion GetPrintDistributionNotificationCards
        #endregion StatusNoficationCards

        #region Charts
        #region HomeDocumentListChart
        public DataTable DMS_HomeDocumentListChart(int DepartmentID, int DocuentTypeID, int mode, int EmpID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_HomeDocumentListChart]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@Department", DepartmentID);
                DMS_Cmd.Parameters.AddWithValue("@DocumentType", DocuentTypeID);
                DMS_Cmd.Parameters.AddWithValue("@mode", mode);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", EmpID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion HomeDocumentListChart
        #region RecentlyRevisedDocumentsChart
        public DataTable DMS_RecentlyRevisedDocumentsChart(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("DMS.AizantIT_SP_RecentlyRevisedDocumentsChart", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FromDate", doc.FromDate);
                DMS_Cmd.Parameters.AddWithValue("@ToDate", doc.ToDate);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion RecentlyRevisedDocumentsChart    
        #region DocumentsReachingReviewDateChart
        public DataTable DMS_ReachingReviewDateChart(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("DMS.AizantIT_SP_ReachingReviewDate", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@Department", doc.DMS_Deparment);
                DMS_Cmd.Parameters.AddWithValue("@DocumentType", doc.DMS_DocumentType);
                DMS_Cmd.Parameters.AddWithValue("@FromDate", doc.FromDate);
                DMS_Cmd.Parameters.AddWithValue("@ToDate", doc.ToDate);
                DMS_Cmd.Parameters.AddWithValue("@mode", doc.Mode);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion DocumentsReachingReviewDateChart
        #region DocumentStatusChart
        public DataTable DMS_DocumentStatusChart(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("DMS.AizantIT_SP_DocumentStatusChart", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@Department", doc.DMS_Deparment);
                DMS_Cmd.Parameters.AddWithValue("@DocumentType", doc.DMS_DocumentType);
                DMS_Cmd.Parameters.AddWithValue("@Status", doc.DocStatus);
                DMS_Cmd.Parameters.AddWithValue("@mode", doc.Mode);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion DocumentStatusChart
        #endregion Charts

        #region DocRequest
        #region SubmitDocPrintRequest
        public int DMS_DocPrintRequestSubmission(DocObjects doc, out int DocReqID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_SubmitDocPrintRequest]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVID", doc.pkid);
                DMS_Cmd.Parameters.AddWithValue("@purpose", doc.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@pageno", doc.Pageno);
                DMS_Cmd.Parameters.AddWithValue("@RequestByID", doc.DMSRequestByID);
                DMS_Cmd.Parameters.AddWithValue("@ReviewerID", doc.DMSReviewedByID);
                DMS_Cmd.Parameters.AddWithValue("@DocControllerID", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@isSoftCopy", doc.IsSoftCopy);
                DMS_Cmd.Parameters.AddWithValue("@CopyType", doc.CopyType);
                DMS_Cmd.Parameters.AddWithValue("@validity", doc.Validity);
                DMS_Cmd.Parameters.AddWithValue("@Comments", doc.PrintReqComments);
                DMS_Cmd.Parameters.AddWithValue("@Mode", doc.Mode);
                SqlParameter ReqNo = DMS_Cmd.Parameters.Add("@RequestNumber", SqlDbType.Int);
                ReqNo.Direction = ParameterDirection.Output;
                SqlParameter DocReqNo = DMS_Cmd.Parameters.Add("@DocRequestID", SqlDbType.Int);
                DocReqNo.Direction = ParameterDirection.Output;
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
                string Result = DMS_Cmd.Parameters["@RequestNumber"].Value.ToString();

                DMS_Return = Convert.ToInt32(Result);
                DocReqID =Convert.ToInt32(DMS_Cmd.Parameters["@DocRequestID"].Value.ToString());
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion SubmitDocPrintRequest
        #region UpdateDocPrintRequestUsers
        public int DMS_UpdateDocPrintRequestUsers(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_UpdateDocPrintRequestUsers]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocReqID", doc.pkid);
                DMS_Cmd.Parameters.AddWithValue("@ManageByID", doc.DMS_ModifiedBy);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedByID", doc.DMSReviewedByID);
                DMS_Cmd.Parameters.AddWithValue("@DocControllerByID", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@Comments", doc.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@CommentedDate", DateTime.Now);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion UpdateDocPrintRequestUsers
        #region GetDocPrintReqDetails
        public DataTable DMS_GetDocPrintReqDetails(string DMS_DocReqID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetDocPrintRequestDetails]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocReqID", Convert.ToInt32(DMS_DocReqID));
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion GetDocPrintReqDetails
        #region GetDocDistributionHistory
        public DataTable DMS_GetDocDistributionHistory(string DMS_DocReqID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetDocPrintDistributionHistory]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocReqID", Convert.ToInt32(DMS_DocReqID));
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion GetDocDistributionHistory       
        #region GetDocReqStatus
        public DataTable DMS_GetDocReqLatestStatus(string DMS_DocReqID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetLatestDocReqStatus]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocReqID", Convert.ToInt32(DMS_DocReqID));
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion GetDocReqStatus
        #endregion DocRequest

        #region Form
        #region FormCreation
        public int DMS_FormInsertion(DocUploadBO doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_FormInsertion]", DMS_Con);
                //DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormNumber", doc.formCreate.FormNumber);
                DMS_Cmd.Parameters.AddWithValue("@FormName", doc.formCreate.FormName);
                DMS_Cmd.Parameters.AddWithValue("@CreatedBy", doc.formCreate.DMS_CreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@Department", doc.formCreate.DMS_Deparment);
                DMS_Cmd.Parameters.AddWithValue("@Purpose", doc.formCreate.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@ApprovedBy", doc.formCreate.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@RefDocID", doc.formCreate.DocRefID);
                DMS_Cmd.Parameters.AddWithValue("@OtherDocNum", doc.formCreate.documentNUmber);
                DMS_Cmd.Parameters.AddWithValue("@VersionNumber", doc.formCreate.Version);
                DMS_Cmd.Parameters.AddWithValue("@Content", doc.formCreate.Content);
                DMS_Cmd.Parameters.AddWithValue("@Action", doc.formCreate.action);
                DMS_Cmd.Parameters.AddWithValue("@Status", doc.formCreate.Status);
                DMS_Cmd.Parameters.AddWithValue("@FileType", doc.formCreate.FileType1);
                DMS_Cmd.Parameters.AddWithValue("@FileName", doc.formCreate.Filename);
                DMS_Cmd.Parameters.AddWithValue("@Comments", doc.formCreate.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@CreatedDate", doc.formCreate.PreparedDate);
                SqlParameter Status = DMS_Cmd.Parameters.Add("@FormVersionID", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                string Result = DMS_Cmd.Parameters["@FormVersionID"].Value.ToString();
                DMS_Return = Convert.ToInt32(Result);

                if (doc.docLocation != null)
                {
                    SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_InsertFormLocation]", DMS_Con);
                    //DMS_Cmd1.CommandTimeout = 120;
                    DMS_Cmd1.Transaction = transaction;
                    DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd1.Parameters.AddWithValue("@FormVersionID", DMS_Return);
                    DMS_Cmd1.Parameters.AddWithValue("@RoleID", doc.docLocation.RoleID);
                    DMS_Cmd1.Parameters.AddWithValue("@EmpID", doc.docLocation.EmpID);
                    DMS_Cmd1.Parameters.AddWithValue("@FilePath1", doc.docLocation.FilePath1);
                    DMS_Cmd1.Parameters.AddWithValue("@FilePath2", doc.docLocation.FilePath2);
                    DMS_Cmd1.Parameters.AddWithValue("@FileType1", doc.docLocation.FileType1);
                    DMS_Cmd1.Parameters.AddWithValue("@FileType2", doc.docLocation.FileType2);
                    DMS_Cmd1.Parameters.AddWithValue("@FileName1", doc.docLocation.FileName1);
                    DMS_Cmd1.Parameters.AddWithValue("@FileName2", doc.docLocation.FileName2);
                    DMS_Cmd1.ExecuteNonQuery();
                }

                if (doc.docComments != null)
                {
                    SqlCommand DMS_Cmd2 = new SqlCommand("[DMS].[AizantIT_SP_FormHistoryList]", DMS_Con);
                    //DMS_Cmd2.CommandTimeout = 120;
                    DMS_Cmd2.Transaction = transaction;
                    DMS_Cmd2.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd2.Parameters.AddWithValue("@FormVersionID", DMS_Return);
                    DMS_Cmd2.Parameters.AddWithValue("@EmpID", doc.docComments.EmpID);
                    DMS_Cmd2.Parameters.AddWithValue("@RoleID", doc.docComments.RoleID);
                    DMS_Cmd2.Parameters.AddWithValue("@Comments", doc.docComments.Remarks);
                    DMS_Cmd2.Parameters.AddWithValue("@CommentDate", doc.docComments.PreparedDate);
                    DMS_Cmd2.Parameters.AddWithValue("@AssignedToRoleID", doc.docComments.AssignedRole);
                    DMS_Cmd2.Parameters.AddWithValue("@AssignedTo", doc.docComments.AssignedTo);
                    DMS_Cmd2.Parameters.AddWithValue("@ActionStatusID", doc.docComments.action);
                    DMS_Cmd2.Parameters.AddWithValue("@CommentType", doc.docComments.CommentType);
                    SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd2);
                    DMS_da.Fill(DMS_Dt);
                }
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }

            return DMS_Return;
        }
        #endregion FormCreation
        #region UpdateFormUsers
        public int DMS_UpdateFormUsers(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_UpdateFormUsers]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.pkid);
                DMS_Cmd.Parameters.AddWithValue("@ManageByID", doc.DMS_ModifiedBy);
                DMS_Cmd.Parameters.AddWithValue("@CreatorID", doc.DMS_CreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@ApproverID", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@Comments", doc.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@CommentedDate", DateTime.Now);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion UpdateFormUsers
        #region GetFormContent
        public DataTable DMS_GetFormContent(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetFormContent]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.FormVersionID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion GetFormContent
        #region GetFormHistory
        public DataTable DMS_GetFormHistory(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetFormHistory]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.FormVersionID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion GetFormHistory
        #region GetFormDetails
        public DataTable DMS_GetFormDetails(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetFormDetails]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.FormVersionID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion GetFormDetails
        public DataTable DMS_FormHistoryList(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_FormHistoryList]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@RoleID", doc.RoleID);
                DMS_Cmd.Parameters.AddWithValue("@Comments", doc.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@CommentDate", doc.PreparedDate);
                DMS_Cmd.Parameters.AddWithValue("@AssignedToRoleID", doc.AssignedRole);
                DMS_Cmd.Parameters.AddWithValue("@AssignedTo", doc.AssignedTo);
                DMS_Cmd.Parameters.AddWithValue("@ActionStatusID", doc.action);
                DMS_Cmd.Parameters.AddWithValue("@CommentType", doc.CommentType);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #region FormUpdate
        public int DMS_FormUpdation(DocUploadBO doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                if (doc.docLocation != null)
                {
                    SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_InsertFormLocation]", DMS_Con);
                    //DMS_Cmd1.CommandTimeout = 120;
                    DMS_Cmd1.Transaction = transaction;
                    DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd1.Parameters.AddWithValue("@FormVersionID", doc.docLocation.Version);
                    DMS_Cmd1.Parameters.AddWithValue("@RoleID", doc.docLocation.RoleID);
                    DMS_Cmd1.Parameters.AddWithValue("@EmpID", doc.docLocation.EmpID);
                    DMS_Cmd1.Parameters.AddWithValue("@FilePath1", doc.docLocation.FilePath1);
                    DMS_Cmd1.Parameters.AddWithValue("@FilePath2", doc.docLocation.FilePath2);
                    DMS_Cmd1.Parameters.AddWithValue("@FileType1", doc.docLocation.FileType1);
                    DMS_Cmd1.Parameters.AddWithValue("@FileType2", doc.docLocation.FileType2);
                    DMS_Cmd1.Parameters.AddWithValue("@FileName1", doc.docLocation.FileName1);
                    DMS_Cmd1.Parameters.AddWithValue("@FileName2", doc.docLocation.FileName2);
                    DMS_Cmd1.ExecuteNonQuery();
                }

                if (doc.docComments != null)
                {
                    SqlCommand DMS_Cmd2 = new SqlCommand("[DMS].[AizantIT_SP_FormHistoryList]", DMS_Con);
                    //DMS_Cmd2.CommandTimeout = 120;
                    DMS_Cmd2.Transaction = transaction;
                    DMS_Cmd2.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd2.Parameters.AddWithValue("@FormVersionID", doc.docLocation.Version);
                    DMS_Cmd2.Parameters.AddWithValue("@EmpID", doc.docComments.EmpID);
                    DMS_Cmd2.Parameters.AddWithValue("@RoleID", doc.docComments.RoleID);
                    DMS_Cmd2.Parameters.AddWithValue("@Comments", doc.docComments.Remarks);
                    DMS_Cmd2.Parameters.AddWithValue("@CommentDate", doc.docComments.PreparedDate);
                    DMS_Cmd2.Parameters.AddWithValue("@AssignedToRoleID", doc.docComments.AssignedRole);
                    DMS_Cmd2.Parameters.AddWithValue("@AssignedTo", doc.docComments.AssignedTo);
                    DMS_Cmd2.Parameters.AddWithValue("@ActionStatusID", doc.docComments.action);
                    DMS_Cmd2.Parameters.AddWithValue("@CommentType", doc.docComments.CommentType);
                    SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd2);
                    DMS_da.Fill(DMS_Dt);
                }

                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_FormUpdate]", DMS_Con);
                //DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormNumber", doc.formCreate.FormNumber);
                DMS_Cmd.Parameters.AddWithValue("@FormName", doc.formCreate.FormName);
                DMS_Cmd.Parameters.AddWithValue("@CreatedBy", doc.formCreate.DMS_CreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@Department", doc.formCreate.DMS_Deparment);
                DMS_Cmd.Parameters.AddWithValue("@Purpose", doc.formCreate.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@ApprovedBy", doc.formCreate.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@RefDocID", doc.formCreate.DocRefID);
                DMS_Cmd.Parameters.AddWithValue("@OtherDocNum", doc.formCreate.documentNUmber);
                DMS_Cmd.Parameters.AddWithValue("@VersionNumber", doc.formCreate.Version);
                DMS_Cmd.Parameters.AddWithValue("@Content", doc.formCreate.Content);
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.formCreate.FormVersionID);
                DMS_Cmd.Parameters.AddWithValue("@Action", doc.formCreate.action);
                DMS_Cmd.Parameters.AddWithValue("@Status", doc.formCreate.Status);
                DMS_Cmd.Parameters.AddWithValue("@FileType", doc.formCreate.FileType1);
                DMS_Cmd.Parameters.AddWithValue("@FileName", doc.formCreate.Filename);
                DMS_Cmd.Parameters.AddWithValue("@Comments", doc.formCreate.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@CreatedDate", doc.formCreate.PreparedDate);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion FormUpdate
        #region FormRevision
        public int DMS_FormRevision(DocUploadBO doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_FormRevision]", DMS_Con);
                //DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@CreatedBy", doc.formCreate.DMS_CreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@FormDescription", doc.formCreate.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@ApproverID", doc.formCreate.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@RefDocID", doc.formCreate.DocRefID);
                DMS_Cmd.Parameters.AddWithValue("@OtherDocNum", doc.formCreate.documentNUmber);
                DMS_Cmd.Parameters.AddWithValue("@FormVersionNumber", doc.formCreate.Version);
                DMS_Cmd.Parameters.AddWithValue("@Content", doc.formCreate.Content);
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.formCreate.FormVersionID);
                DMS_Cmd.Parameters.AddWithValue("@Action", doc.formCreate.action);
                DMS_Cmd.Parameters.AddWithValue("@FileType", doc.formCreate.FileType1);
                DMS_Cmd.Parameters.AddWithValue("@FileName", doc.formCreate.Filename);
                DMS_Cmd.Parameters.AddWithValue("@Comments", doc.formCreate.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@CreatedDate", doc.formCreate.PreparedDate);
                SqlParameter Status = DMS_Cmd.Parameters.Add("@NewFormVersionID", SqlDbType.Int);
                Status.Direction = ParameterDirection.Output;
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                string Result = DMS_Cmd.Parameters["@NewFormVersionID"].Value.ToString();
                DMS_Return = Convert.ToInt32(Result);

                if (doc.docLocation != null)
                {
                    SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_InsertFormLocation]", DMS_Con);
                    //DMS_Cmd1.CommandTimeout = 120;
                    DMS_Cmd1.Transaction = transaction;
                    DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd1.Parameters.AddWithValue("@FormVersionID", DMS_Return);
                    DMS_Cmd1.Parameters.AddWithValue("@RoleID", doc.docLocation.RoleID);
                    DMS_Cmd1.Parameters.AddWithValue("@EmpID", doc.docLocation.EmpID);
                    DMS_Cmd1.Parameters.AddWithValue("@FilePath1", doc.docLocation.FilePath1);
                    DMS_Cmd1.Parameters.AddWithValue("@FilePath2", doc.docLocation.FilePath2);
                    DMS_Cmd1.Parameters.AddWithValue("@FileType1", doc.docLocation.FileType1);
                    DMS_Cmd1.Parameters.AddWithValue("@FileType2", doc.docLocation.FileType2);
                    DMS_Cmd1.Parameters.AddWithValue("@FileName1", doc.docLocation.FileName1);
                    DMS_Cmd1.Parameters.AddWithValue("@FileName2", doc.docLocation.FileName2);
                    DMS_Cmd1.ExecuteNonQuery();
                }

                if (doc.docComments != null)
                {
                    SqlCommand DMS_Cmd2 = new SqlCommand("[DMS].[AizantIT_SP_FormHistoryList]", DMS_Con);
                    //DMS_Cmd2.CommandTimeout = 120;
                    DMS_Cmd2.Transaction = transaction;
                    DMS_Cmd2.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd2.Parameters.AddWithValue("@FormVersionID", DMS_Return);
                    DMS_Cmd2.Parameters.AddWithValue("@EmpID", doc.docComments.EmpID);
                    DMS_Cmd2.Parameters.AddWithValue("@RoleID", doc.docComments.RoleID);
                    DMS_Cmd2.Parameters.AddWithValue("@Comments", doc.docComments.Remarks);
                    DMS_Cmd2.Parameters.AddWithValue("@CommentDate", doc.docComments.PreparedDate);
                    DMS_Cmd2.Parameters.AddWithValue("@AssignedToRoleID", doc.docComments.AssignedRole);
                    DMS_Cmd2.Parameters.AddWithValue("@AssignedTo", doc.docComments.AssignedTo);
                    DMS_Cmd2.Parameters.AddWithValue("@ActionStatusID", doc.docComments.action);
                    DMS_Cmd2.Parameters.AddWithValue("@CommentType", doc.docComments.CommentType);
                    SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd2);
                    DMS_da.Fill(DMS_Dt);
                }
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion FormRevision
        #region FormWithdraw
        public int DMS_FormWithdraw(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_FormWithdraw]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@CreatedBy", doc.DMS_CreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@FormDescription", doc.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@ApproverID", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.FormVersionID);
                DMS_Cmd.Parameters.AddWithValue("@comments", doc.Remarks);

                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion FormWithdraw
        #region FormRevisionUpdate
        public int DMS_FormRevisionUpdate(DocUploadBO doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                if (doc.docLocation != null)
                {
                    SqlCommand DMS_Cmd1 = new SqlCommand("[DMS].[AizantIT_SP_InsertFormLocation]", DMS_Con);
                    //DMS_Cmd1.CommandTimeout = 120;
                    DMS_Cmd1.Transaction = transaction;
                    DMS_Cmd1.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd1.Parameters.AddWithValue("@FormVersionID", doc.docLocation.Version);
                    DMS_Cmd1.Parameters.AddWithValue("@RoleID", doc.docLocation.RoleID);
                    DMS_Cmd1.Parameters.AddWithValue("@EmpID", doc.docLocation.EmpID);
                    DMS_Cmd1.Parameters.AddWithValue("@FilePath1", doc.docLocation.FilePath1);
                    DMS_Cmd1.Parameters.AddWithValue("@FilePath2", doc.docLocation.FilePath2);
                    DMS_Cmd1.Parameters.AddWithValue("@FileType1", doc.docLocation.FileType1);
                    DMS_Cmd1.Parameters.AddWithValue("@FileType2", doc.docLocation.FileType2);
                    DMS_Cmd1.Parameters.AddWithValue("@FileName1", doc.docLocation.FileName1);
                    DMS_Cmd1.Parameters.AddWithValue("@FileName2", doc.docLocation.FileName2);
                    DMS_Cmd1.ExecuteNonQuery();
                }

                if (doc.docComments != null)
                {
                    SqlCommand DMS_Cmd2 = new SqlCommand("[DMS].[AizantIT_SP_FormHistoryList]", DMS_Con);
                    //DMS_Cmd2.CommandTimeout = 120;
                    DMS_Cmd2.Transaction = transaction;
                    DMS_Cmd2.CommandType = CommandType.StoredProcedure;
                    DMS_Cmd2.Parameters.AddWithValue("@FormVersionID", doc.docLocation.Version);
                    DMS_Cmd2.Parameters.AddWithValue("@EmpID", doc.docComments.EmpID);
                    DMS_Cmd2.Parameters.AddWithValue("@RoleID", doc.docComments.RoleID);
                    DMS_Cmd2.Parameters.AddWithValue("@Comments", doc.docComments.Remarks);
                    DMS_Cmd2.Parameters.AddWithValue("@CommentDate", doc.docComments.PreparedDate);
                    DMS_Cmd2.Parameters.AddWithValue("@AssignedToRoleID", doc.docComments.AssignedRole);
                    DMS_Cmd2.Parameters.AddWithValue("@AssignedTo", doc.docComments.AssignedTo);
                    DMS_Cmd2.Parameters.AddWithValue("@ActionStatusID", doc.docComments.action);
                    DMS_Cmd2.Parameters.AddWithValue("@CommentType", doc.docComments.CommentType);
                    SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd2);
                    DMS_da.Fill(DMS_Dt);
                }

                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_FormRevisionUpdate]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@CreatedBy", doc.formCreate.DMS_CreatedBy);
                DMS_Cmd.Parameters.AddWithValue("@FormDescription", doc.formCreate.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@ApproverID", doc.formCreate.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@FormVersionNumber", doc.formCreate.Version);
                DMS_Cmd.Parameters.AddWithValue("@Content", doc.formCreate.Content);
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.formCreate.FormVersionID);
                DMS_Cmd.Parameters.AddWithValue("@FileType", doc.formCreate.FileType1);
                DMS_Cmd.Parameters.AddWithValue("@FileName", doc.formCreate.Filename);
                DMS_Cmd.Parameters.AddWithValue("@Comments", doc.formCreate.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@CreatedDate", doc.formCreate.PreparedDate);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion FormRevisionUpdate
        #endregion Form

        #region FormRequest
        #region SubmitFormPrintRequest
        public int DMS_FormPrintRequestSubmission(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_SubmitFormPrintRequest]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.pkid);
                DMS_Cmd.Parameters.AddWithValue("@purpose", doc.Purpose);
                DMS_Cmd.Parameters.AddWithValue("@NoofCopies", doc.NoofCopies);
                DMS_Cmd.Parameters.AddWithValue("@ProjectNumber", doc.ProjectNumber);
                DMS_Cmd.Parameters.AddWithValue("@RequestByID", doc.DMSRequestByID);
                DMS_Cmd.Parameters.AddWithValue("@ReviewerID", doc.DMSReviewedByID);
                DMS_Cmd.Parameters.AddWithValue("@DocControllerID", doc.DMSDocControllerID);
                SqlParameter ReqNo = DMS_Cmd.Parameters.Add("@RequestNumber", SqlDbType.Int);
                ReqNo.Direction = ParameterDirection.Output;
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
                string Result = DMS_Cmd.Parameters["@RequestNumber"].Value.ToString();
                DMS_Return = Convert.ToInt32(Result);

                // Attempt to commit the transaction.

            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion SubmitFormPrintRequest
        #region UpdateFormPrintRequestUsers
        public int DMS_UpdateFormPrintRequestUsers(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_UpdateFormPrintRequestUsers]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormReqID", doc.pkid);
                DMS_Cmd.Parameters.AddWithValue("@ManageByID", doc.DMS_ModifiedBy);
                DMS_Cmd.Parameters.AddWithValue("@ReviewedByID", doc.DMSReviewedByID);
                DMS_Cmd.Parameters.AddWithValue("@DocControllerByID", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@Comments", doc.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@CommentedDate", DateTime.Now);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion UpdateFormPrintRequestUsers
        #region GetFormPrintReqDetails
        public DataSet DMS_GetFormPrintReqDetails(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataSet DMS_Dt = new DataSet();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetFormPrintRequestDetails]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormReqID", doc.FormReqID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion GetFormPrintReqDetails
        #region GetFormPrintReqHistory
        public DataTable DMS_GetFormPrintReqHistory(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetFormPrintRequestHistory]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormReqID", doc.FormReqID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion GetFormPrintReqHistory
        #region GetEndSNoofPreviousFormRequest
        public DataTable DMS_GetEndSNoofFormRequest(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetEndSerialNoofFormRequest]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.FormVersionID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion GetEndSNoofPreviousFormRequest
        #endregion FormRequest

        #region FormReturn
        public int DMS_SubmitFormReturn(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_SubmitFormReturn]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormRequestID", doc.FormReqID);
                DMS_Cmd.Parameters.AddWithValue("@Remarks", doc.Remarks);
                DMS_Cmd.Parameters.AddWithValue("@ReturnedCopies", doc.NoofCopies);
                //DMS_Cmd.Parameters.AddWithValue("@RecieverID", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@CopyNumbers", doc.CopyNumbers);
                DMS_Cmd.Parameters.AddWithValue("@RequestorID", doc.DMSRequestByID);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public int DMS_VerifyFormReturn(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return;
            DataTable DMS_Dt = new DataTable();
            //SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_VerifyFormReturn]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormRequestID", doc.FormReqID);
                DMS_Cmd.Parameters.AddWithValue("@RecieverID", doc.DMSDocControllerID);
                DMS_Cmd.Parameters.AddWithValue("@Comments", doc.Remarks);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public DataTable DMS_GetFormReqReturnDetails(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetFormReturnDetails]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormReqID", doc.FormReqID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetFormReturnReportDetails(FormPrintRequest formPrint)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                var formdate=formPrint.FromDate==null ?null: Convert.ToDateTime(formPrint.FromDate).ToString("yyyy-MM-dd");
                var TodAte= formPrint.ToDate == null ? null : Convert.ToDateTime(formPrint.ToDate).ToString("yyyy-MM-dd");
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetFormReturnReport]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", formPrint.FormVersionID);
                DMS_Cmd.Parameters.AddWithValue("@FromDate", formdate);
                DMS_Cmd.Parameters.AddWithValue("@ToDate", TodAte);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion FormReturn

        #region TempDocuments
        public int DMS_InsertDocumentLocation(DocObjects doc, ref SqlTransaction _Transaction, ref SqlConnection DMS_Con)
        {

            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();

            try
            {

                // Start a local transaction

                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_InsertDocumentLocation]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = _Transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVersionID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@RoleID", doc.RoleID);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@FilePath1", doc.FilePath1);
                DMS_Cmd.Parameters.AddWithValue("@FilePath2", doc.FilePath2);
                DMS_Cmd.Parameters.AddWithValue("@isTemp", doc.isTemp);
                DMS_Cmd.Parameters.AddWithValue("@FileType1", doc.FileType1);
                DMS_Cmd.Parameters.AddWithValue("@FileType2", doc.FileType2);
                DMS_Cmd.Parameters.AddWithValue("@FileName1", doc.FileName1);
                DMS_Cmd.Parameters.AddWithValue("@FileName2", doc.FileName2);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.

            }
            catch (Exception e)
            {

                throw e;
            }


            return DMS_Return;
        }
        public int DMS_InsertDocumentLocation(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_InsertDocumentLocation]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVersionID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@RoleID", doc.RoleID);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@FilePath1", doc.FilePath1);
                DMS_Cmd.Parameters.AddWithValue("@FilePath2", doc.FilePath2);
                DMS_Cmd.Parameters.AddWithValue("@isTemp", doc.isTemp);
                DMS_Cmd.Parameters.AddWithValue("@FileType1", doc.FileType1);
                DMS_Cmd.Parameters.AddWithValue("@FileType2", doc.FileType2);
                DMS_Cmd.Parameters.AddWithValue("@FileName1", doc.FileName1);
                DMS_Cmd.Parameters.AddWithValue("@FileName2", doc.FileName2);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public DataTable DMS_GetTempDocumentPath(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetTempDocumentsPath]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVID", doc.Version);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetTempDocumentDetails(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetTempDocumentsDetails]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@DocStatus", doc.DocStatus);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetEveryoneTempDocumentPath(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetTempEveryoneDocumentsPath]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@RoleID", doc.RoleID);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetEveryoneChangedDocumentPath(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetEveryoneChangedDocumentPath]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@RoleID", doc.RoleID);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public int DMS_UpdateSaveStatus(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_UpdateSaveStatus]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVersionID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@Mode", doc.DocStatus);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public DataTable DMS_GetSaveStatus(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetSaveStatus]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVersionID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@Mode", doc.DocStatus);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public DataTable DMS_GetDocumentLocationList(DocObjects doc, ref SqlTransaction _transaction, ref SqlConnection DMS_Con)
        {
            // DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }

                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetDocumentLocationList]", DMS_Con);
                DMS_Cmd.Transaction = _transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@RoleID", doc.RoleID);
                DMS_Cmd.Parameters.AddWithValue("@FileType", doc.FileType1);
                DMS_Cmd.Parameters.AddWithValue("@mode", doc.Mode);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {

            }
            return DMS_Dt;
        }
        public DataTable DMS_GetDocumentLocationList(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }

                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetDocumentLocationList]", DMS_Con);

                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@RoleID", doc.RoleID);
                DMS_Cmd.Parameters.AddWithValue("@FileType", doc.FileType1);
                DMS_Cmd.Parameters.AddWithValue("@mode", doc.Mode);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion TempDocuments

        #region TempForms
        public int DMS_InsertFormLocation(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_InsertFormLocation]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@RoleID", doc.RoleID);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@FilePath1", doc.FilePath1);
                DMS_Cmd.Parameters.AddWithValue("@FilePath2", doc.FilePath2);
                DMS_Cmd.Parameters.AddWithValue("@FileType1", doc.FileType1);
                DMS_Cmd.Parameters.AddWithValue("@FileType2", doc.FileType2);
                DMS_Cmd.Parameters.AddWithValue("@FileName1", doc.FileName1);
                DMS_Cmd.Parameters.AddWithValue("@FileName2", doc.FileName2);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        public DataTable DMS_GetFormLocationList(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetFormLocationList]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@FormVersionID", doc.Version);
                DMS_Cmd.Parameters.AddWithValue("@EmpID", doc.EmpID);
                DMS_Cmd.Parameters.AddWithValue("@RoleID", doc.RoleID);
                DMS_Cmd.Parameters.AddWithValue("@FileType", doc.FileType1);
                DMS_Cmd.Parameters.AddWithValue("@mode", doc.Mode);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion TempForms

        #region ViewDocument
        public DataTable DMS_GetPhysicalPathforViewing(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetPhysicalPathforViewing]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@Type", doc.Mode);
                DMS_Cmd.Parameters.AddWithValue("@ID", doc.DocRefID);
                DMS_Cmd.Parameters.AddWithValue("@OfRoleID", doc.RoleID);
                DMS_Cmd.Parameters.AddWithValue("@OfEmpID", doc.EmpID);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #endregion ViewDocument

        #region Document Recommendations
        public int DMS_DeleteDocumentRecommendations(ReviewObjects reviewObjects)//Template Deletion
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            SqlTransaction transaction = null;
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                // Start a local transaction
                transaction = DMS_Con.BeginTransaction("CRUDTransaction");
                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                SqlCommand DMS_Cmd = new SqlCommand("[TMS_ML].[AizantIT_Delete_DocumentRecommendations]", DMS_Con);
                DMS_Cmd.CommandTimeout = 120;
                DMS_Cmd.Transaction = transaction;
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocVersionID", reviewObjects.Id);
                DMS_Return = DMS_Cmd.ExecuteNonQuery();
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception e)
            {
                // Attempt to roll back the transaction
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        #endregion Document Recommendations
        //    public DataTable BindListViewer(int DeptID)
        //    {
        //        SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);

        //        try
        //        {
        //            if (DMS_Con.State == ConnectionState.Closed)
        //            {
        //                DMS_Con.Open();
        //            }
        //            SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_ListViewer]", DMS_Con);
        //            DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //            DMS_Cmd.Parameters.AddWithValue("@DeptID", DeptID);
        //            DataTable dt = new DataTable();
        //            SqlDataAdapter da = new SqlDataAdapter(DMS_Cmd);
        //            da.Fill(dt);
        //            return dt;
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }


        //    }
        //    public DataTable GetDeptCode(int EmpID)
        //    {
        //        SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);

        //        try
        //        {
        //            if (DMS_Con.State == ConnectionState.Closed)
        //            {
        //                DMS_Con.Open();
        //            }
        //            SqlCommand DMS_Cmd = new SqlCommand("UMS.AizantIT_SP_GetDeptCode", DMS_Con);
        //            DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //            DMS_Cmd.Parameters.AddWithValue("@EmpID", EmpID);
        //            DataTable dt = new DataTable();
        //            SqlDataAdapter da = new SqlDataAdapter(DMS_Cmd);
        //            da.Fill(dt);
        //            return dt;
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //    }
        //    public DataTable DMS_GetUserRoles()
        //    {
        //        SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //        int DMS_Return = 0;
        //        DataTable DMS_Dt = new DataTable();
        //        try
        //        {
        //            if (DMS_Con.State == ConnectionState.Closed)
        //            {
        //                DMS_Con.Open();
        //            }
        //            SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_UserRoles]", DMS_Con);
        //            DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //            SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
        //            DMS_da.Fill(DMS_Dt);

        //        }
        //        catch (Exception e)
        //        {
        //            throw e;
        //        }
        //        finally
        //        {
        //            DMS_Con.Close();
        //            DMS_Con.Dispose();
        //        }
        //        return DMS_Dt;
        //    }
        //    public DataSet DMS_GetEmpDeptList(int EmpID)
        //    {
        //        SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //        int DMS_Return = 0;
        //        DataSet DMS_Dt = new DataSet();
        //        try
        //        {
        //            if (DMS_Con.State == ConnectionState.Closed)
        //            {
        //                DMS_Con.Open();
        //            }
        //            SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetModuleWiseRolesDepts]", DMS_Con);
        //            DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //            DMS_Cmd.Parameters.AddWithValue("@EmpID", EmpID);
        //            DMS_Cmd.Parameters.AddWithValue("@ModuleID", 2);

        //            SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
        //            DMS_da.Fill(DMS_Dt);

        //        }
        //        catch (Exception e)
        //        {
        //            throw e;
        //        }
        //        finally
        //        {
        //            DMS_Con.Close();
        //            DMS_Con.Dispose();
        //        }
        //        return DMS_Dt;
        //    }
        //    public DataTable DMS_GetsopList(DocObjects doc)
        //    {
        //        SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //        int DMS_Return = 0;
        //        DataTable DMS_Dt = new DataTable();
        //        try
        //        {
        //            if (DMS_Con.State == ConnectionState.Closed)
        //            {
        //                DMS_Con.Open();
        //            }
        //            SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetSopDocuments]", DMS_Con);
        //            DMS_Cmd.Parameters.AddWithValue("@Dept", Convert.ToInt32(doc.dept));


        //            DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //            SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
        //            DMS_da.Fill(DMS_Dt);

        //        }
        //        catch (Exception e)
        //        {
        //            throw e;
        //        }
        //        finally
        //        {
        //            DMS_Con.Close();
        //            DMS_Con.Dispose();
        //        }
        //        return DMS_Dt;
        //    }
        //    public DataTable DMS_AutoGenerateDocumentID()
        //    {
        //        SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //        int DMS_Return = 0;
        //        DataTable DMS_Dt = new DataTable();
        //        try
        //        {
        //            if (DMS_Con.State == ConnectionState.Closed)
        //            {
        //                DMS_Con.Open();
        //            }
        //            SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentID]", DMS_Con);
        //            DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //            SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
        //            DMS_da.Fill(DMS_Dt);

        //        }
        //        catch (Exception e)
        //        {
        //            throw e;
        //        }
        //        finally
        //        {
        //            DMS_Con.Close();
        //            DMS_Con.Dispose();
        //        }
        //        return DMS_Dt;
        //    }
        //    public DataTable DMS_GetDocumentDetailsByID(string DMS_DocumentID)
        //    {
        //        SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //        int DMS_Return = 0;
        //        DataTable DMS_Dt = new DataTable();
        //        try
        //        {
        //            if (DMS_Con.State == ConnectionState.Closed)
        //            {
        //                DMS_Con.Open();
        //            }
        //            SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetDocumentDetailsByID]", DMS_Con);
        //            DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //            DMS_Cmd.Parameters.AddWithValue("@DocumentID", Convert.ToInt32(DMS_DocumentID));
        //            SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
        //            DMS_da.Fill(DMS_Dt);
        //        }
        //        catch (Exception e)
        //        {
        //            throw e;
        //        }
        //        finally
        //        {
        //            DMS_Con.Close();
        //            DMS_Con.Dispose();
        //        }
        //        return DMS_Dt;
        //    }
        //    public DataTable DMS_GetContent(string DMS_DocumentID)
        //    {
        //        SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //        int DMS_Return = 0;
        //        DataTable DMS_Dt = new DataTable();
        //        try
        //        {
        //            if (DMS_Con.State == ConnectionState.Closed)
        //            {
        //                DMS_Con.Open();
        //            }
        //            SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_Content]", DMS_Con);
        //            DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //            DMS_Cmd.Parameters.AddWithValue("@VersionID", Convert.ToInt32(DMS_DocumentID));
        //            SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
        //            DMS_da.Fill(DMS_Dt);
        //        }
        //        catch (Exception e)
        //        {
        //            throw e;
        //        }
        //        finally
        //        {
        //            DMS_Con.Close();
        //            DMS_Con.Dispose();
        //        }
        //        return DMS_Dt;
        //    }
        public DataTable DMS_GetDocumentTypeList1(DocObjects doc)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentTypeSearch]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocumentType", doc.DocumentType);
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        public int DMS_MultipleEditRestriction(int FormVID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            int DMS_Return = 0;
            string strQuery = "Select DMS.AizantIT_UDF_MultipleWithdrawRevisionRestriction('" + FormVID + "')";
            try
            {
                SqlCommand DMS_Cmd = new SqlCommand(strQuery, DMS_Con);
                DMS_Cmd.CommandType = CommandType.Text;
                if (DMS_Con.State != ConnectionState.Open)
                    DMS_Con.Open();
                DMS_Return = Convert.ToInt32(DMS_Cmd.ExecuteScalar().ToString());
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Return;
        }
        //    public DataTable DMS_GetDocumentTypeID()
        //    {
        //        SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //        int DMS_Return = 0;
        //        DataTable DMS_Dt = new DataTable();
        //        try
        //        {
        //            if (DMS_Con.State == ConnectionState.Closed)
        //            {
        //                DMS_Con.Open();
        //            }
        //            SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentTypeID]", DMS_Con);
        //            DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //            SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
        //            DMS_da.Fill(DMS_Dt);

        //        }
        //        catch (Exception e)
        //        {
        //            throw e;
        //        }
        //        finally
        //        {
        //            DMS_Con.Close();
        //            DMS_Con.Dispose();
        //        }
        //        return DMS_Dt;
        //    }
        //    #region DocInitiationNotification
        //    public int DocInitiationNotification(DocObjects doc)
        //    {
        //        SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
        //        int DMS_Return = 0;
        //        try
        //        {
        //            if (DMS_Con.State == ConnectionState.Closed)
        //            {
        //                DMS_Con.Open();
        //            }
        //            SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_DocumentInsertionUpdationNotification]", DMS_Con);
        //            DMS_Cmd.CommandType = CommandType.StoredProcedure;
        //            DMS_Cmd.Parameters.AddWithValue("@RequestType", doc.RequestType);
        //            DMS_Cmd.Parameters.AddWithValue("@DocumentNumber", doc.documentNUmber);
        //            DMS_Cmd.Parameters.AddWithValue("@versionID", doc.pkid);
        //            DMS_Return = DMS_Cmd.ExecuteNonQuery();
        //            return DMS_Return;
        //        }
        //        catch (Exception e)
        //        {
        //            throw e;
        //        }
        //        finally
        //        {
        //            DMS_Con.Close();
        //            DMS_Con.Dispose();
        //        }
        //    }
        //    #endregion DocInitiationNotification

        public DataTable DMS_GetMainDocumentDetailsList(JQDataTableBO _objJQDataTableBO,int DocumetID)
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            DataTable DMS_Dt = new DataTable();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_OldVersionListOFDocuments]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
                DMS_Cmd.Parameters.AddWithValue("@DocumentID", DocumetID);
                DMS_Cmd.Parameters.AddWithValue("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                DMS_Cmd.Parameters.AddWithValue("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                DMS_Cmd.Parameters.AddWithValue("@SortCol", _objJQDataTableBO.iSortCol);
                DMS_Cmd.Parameters.AddWithValue("@SortDir", _objJQDataTableBO.sSortDir);
                DMS_Cmd.Parameters.AddWithValue("@Search", _objJQDataTableBO.sSearch);

                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(DMS_Dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return DMS_Dt;
        }
        #region GetPrinterNames
        public DataSet DAL_GetPrinterNamest()
        {
            SqlConnection DMS_Con = new SqlConnection(AizantIT_DMS_Con);
            //int DMS_Return = 0;
            DataSet ds = new DataSet();
            try
            {
                if (DMS_Con.State == ConnectionState.Closed)
                {
                    DMS_Con.Open();
                }
                SqlCommand DMS_Cmd = new SqlCommand("[DMS].[AizantIT_SP_GetPrinterNames]", DMS_Con);
                DMS_Cmd.CommandType = CommandType.StoredProcedure;
               
                SqlDataAdapter DMS_da = new SqlDataAdapter(DMS_Cmd);
                DMS_da.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                DMS_Con.Close();
                DMS_Con.Dispose();
            }
            return ds;
        }
        #endregion
    }
}