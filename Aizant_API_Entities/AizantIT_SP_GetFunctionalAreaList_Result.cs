//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_GetFunctionalAreaList_Result
    {
        public Nullable<long> RowNumber { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public Nullable<int> FunctionalAreaID { get; set; }
        public string FunctionalAreaName { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string DeptCode { get; set; }
        public string DeptID { get; set; }
        public string Status { get; set; }
    }
}
