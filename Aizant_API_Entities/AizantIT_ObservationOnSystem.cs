//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_ObservationOnSystem
    {
        public int ObservationOnSystemID { get; set; }
        public int ObservationID { get; set; }
        public int FDA_SystemID { get; set; }
        public int AreaID { get; set; }
        public int SubAreaID { get; set; }
        public string Remarks { get; set; }
    
        public virtual AizantIT_AuditObservation AizantIT_AuditObservation { get; set; }
        public virtual AizantIT_FDA_System AizantIT_FDA_System { get; set; }
        public virtual AizantIT_SubArea AizantIT_SubArea { get; set; }
        public virtual AizantIT_Area AizantIT_Area { get; set; }
    }
}
