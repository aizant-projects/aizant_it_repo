//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_SubArea
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AizantIT_SubArea()
        {
            this.AizantIT_ObservationOnSystem = new HashSet<AizantIT_ObservationOnSystem>();
        }
    
        public int SubAreaID { get; set; }
        public int AreaID { get; set; }
        public string SubAreaName { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AizantIT_ObservationOnSystem> AizantIT_ObservationOnSystem { get; set; }
        public virtual AizantIT_Area AizantIT_Area { get; set; }
        public virtual AizantIT_EmpMaster AizantIT_EmpMaster { get; set; }
        public virtual AizantIT_EmpMaster AizantIT_EmpMaster1 { get; set; }
    }
}
