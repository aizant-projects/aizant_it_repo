//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_GetSIT_BD_List_Result
    {
        public Nullable<long> RowNumber { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public Nullable<int> SiteID { get; set; }
        public Nullable<int> BusinessDivisionID { get; set; }
        public string SiteDeptIDs { get; set; }
        public string SiteName { get; set; }
        public string BusinessDivisionCodeAndName { get; set; }
        public string IsActive { get; set; }
        public string SiteDeptNames { get; set; }
        public string SiteDeptCodes { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedDate { get; set; }
    }
}
