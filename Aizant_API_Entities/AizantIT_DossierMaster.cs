//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_DossierMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AizantIT_DossierMaster()
        {
            this.AizantIT_DeficiencyMaster = new HashSet<AizantIT_DeficiencyMaster>();
        }
    
        public int DossierID { get; set; }
        public Nullable<int> ProductID { get; set; }
        public System.DateTime SubmissionDate { get; set; }
        public Nullable<System.DateTime> ReviewAcceptedDate { get; set; }
        public Nullable<System.DateTime> GoalDate { get; set; }
        public Nullable<System.DateTime> ApprovedDate { get; set; }
        public int AuthorEmpID { get; set; }
        public int ApproverEmpID { get; set; }
        public string CreatedAs { get; set; }
        public string DossierStatus { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AizantIT_DeficiencyMaster> AizantIT_DeficiencyMaster { get; set; }
    }
}
