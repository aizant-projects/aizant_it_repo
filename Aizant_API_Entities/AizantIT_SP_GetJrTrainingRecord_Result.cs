//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_GetJrTrainingRecord_Result
    {
        public Nullable<long> RowNum { get; set; }
        public string DocumentName { get; set; }
        public string Document_RefNo { get; set; }
        public string DeptName { get; set; }
        public string ModeOfTraining { get; set; }
        public string Opinion { get; set; }
        public string EvaluatedBy { get; set; }
        public string EvaluatedDate { get; set; }
    }
}
