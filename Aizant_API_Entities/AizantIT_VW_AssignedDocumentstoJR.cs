//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_VW_AssignedDocumentstoJR
    {
        public int JR_ID { get; set; }
        public int DocumentID { get; set; }
        public int DocVersionID { get; set; }
        public int Status { get; set; }
        public Nullable<int> DocVersion { get; set; }
        public string DocumentNumber { get; set; }
        public string DocumentName { get; set; }
        public int DeptID { get; set; }
    }
}
