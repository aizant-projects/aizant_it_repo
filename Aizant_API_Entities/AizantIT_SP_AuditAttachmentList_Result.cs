//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_AuditAttachmentList_Result
    {
        public Nullable<long> RowNumber { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public Nullable<int> AttachmentID { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string Type { get; set; }
        public string FileExtention { get; set; }
        public string AttachmentDate { get; set; }
        public Nullable<bool> CanDelete { get; set; }
    }
}
