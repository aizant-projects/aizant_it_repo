//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_CCN_InsertAndUpdate_CCNCommitte_Result
    {
        public int Assessment_ID { get; set; }
        public int CCN_ID { get; set; }
        public Nullable<int> AssigndToID { get; set; }
        public int DeptID { get; set; }
        public int BlockID { get; set; }
        public Nullable<int> CurrentStatus { get; set; }
        public string Comments { get; set; }
        public Nullable<int> AssigndByID { get; set; }
        public Nullable<System.DateTime> Hod_SubmitDate { get; set; }
        public Nullable<bool> IsTemp { get; set; }
    }
}
