//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_ProdDMFHolder
    {
        public int ProdDMFHolderID { get; set; }
        public int ProductDossierID { get; set; }
        public int DMFHolderID { get; set; }
        public int DrugSubstanceID { get; set; }
        public int DS_ManufacturingSiteID { get; set; }
        public string DMFHolderNum { get; set; }
    
        public virtual AizantIT_DrugSubstance AizantIT_DrugSubstance { get; set; }
        public virtual AizantIT_DS_ManufacturingSite AizantIT_DS_ManufacturingSite { get; set; }
        public virtual AizantIT_ProductDossier AizantIT_ProductDossier { get; set; }
        public virtual AizantIT_DMF_Holder AizantIT_DMF_Holder { get; set; }
    }
}
