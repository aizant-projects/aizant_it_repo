//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_ImpactAssessmentTransaction
    {
        public int Trans_ID { get; set; }
        public Nullable<int> ImpPoint_ID { get; set; }
        public Nullable<int> CCN_ID { get; set; }
        public Nullable<int> DeptID { get; set; }
        public Nullable<bool> isYesorNo { get; set; }
        public string DocTitleorRefN { get; set; }
        public string Remarks { get; set; }
        public Nullable<int> BlockID { get; set; }
        public Nullable<int> AssessmentId { get; set; }
    
        public virtual AizantIT_DepartmentMaster AizantIT_DepartmentMaster { get; set; }
    }
}
