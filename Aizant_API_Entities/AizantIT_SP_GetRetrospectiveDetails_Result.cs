//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_GetRetrospectiveDetails_Result
    {
        public Nullable<long> RowNo { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        public string RQ_Documents { get; set; }
    }
}
