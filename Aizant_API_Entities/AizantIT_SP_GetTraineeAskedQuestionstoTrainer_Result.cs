//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_GetTraineeAskedQuestionstoTrainer_Result
    {
        public int TraineeQuestionID { get; set; }
        public string Question { get; set; }
        public string RaisedBy { get; set; }
        public string RaisedDate { get; set; }
        public int DocID { get; set; }
    }
}
