//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_GetTrainingSessionTrainees_Result
    {
        public Nullable<long> RowNumber { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public Nullable<int> TTS_TrainingSessionID { get; set; }
        public Nullable<int> TraineeID { get; set; }
        public Nullable<int> EvaluationStatus { get; set; }
        public Nullable<int> TargetExamID { get; set; }
        public string EmpCode { get; set; }
        public string Trainee { get; set; }
        public string DepartmentName { get; set; }
        public Nullable<int> Attempts { get; set; }
    }
}
