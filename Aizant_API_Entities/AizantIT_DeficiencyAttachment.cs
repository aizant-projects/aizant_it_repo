//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_DeficiencyAttachment
    {
        public int DeficiencyAttachmentID { get; set; }
        public int DeficiencyID { get; set; }
        public string FileName { get; set; }
        public string FileExtention { get; set; }
        public byte[] Attachment { get; set; }
        public System.DateTime AttachmentDate { get; set; }
    }
}
