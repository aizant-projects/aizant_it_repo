//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_Incident_QualityImpacting
    {
        public int UniquePKID { get; set; }
        public Nullable<int> IncidentMasterID { get; set; }
        public string WhatHappened_Desc { get; set; }
        public string WhenHappened_Desc { get; set; }
        public string WhereHappened_Desc { get; set; }
        public Nullable<int> IsHasIncidentAfftectedDoc { get; set; }
        public string PossibleRootCause_Desc { get; set; }
        public string Conclusion_Desc { get; set; }
    }
}
