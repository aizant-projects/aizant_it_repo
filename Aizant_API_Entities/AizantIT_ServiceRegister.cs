//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_ServiceRegister
    {
        public int SNo { get; set; }
        public Nullable<int> ServiceTypeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNo { get; set; }
        public string CommingFrom { get; set; }
        public string VechileNo { get; set; }
        public string PurposeofVisit { get; set; }
        public Nullable<int> WhomToVisitID { get; set; }
        public Nullable<System.DateTime> InDateTime { get; set; }
        public Nullable<System.DateTime> OutDateTime { get; set; }
        public string Remarks { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> LastChangedBy { get; set; }
        public Nullable<System.DateTime> LastChangedDate { get; set; }
        public string LastChangedByComments { get; set; }
    }
}
