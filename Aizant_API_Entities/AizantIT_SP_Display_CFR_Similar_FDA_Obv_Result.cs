//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_Display_CFR_Similar_FDA_Obv_Result
    {
        public string CFR_NUM { get; set; }
        public Nullable<int> CFR_Count { get; set; }
        public string CFR_Name { get; set; }
        public string CFR_Desp { get; set; }
    }
}
