//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_RegulatoryActionStatus
    {
        public int RegulatoryActionID { get; set; }
        public int TableObjectID { get; set; }
        public int ActionID { get; set; }
        public string ActionDescription { get; set; }
    
        public virtual AizantIT_RegulatoryHistoryObjects AizantIT_RegulatoryHistoryObjects { get; set; }
    }
}
