//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_ObservationHistory_Result
    {
        public Nullable<long> RowNumber { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public Nullable<int> ActionHistoryID { get; set; }
        public string ActionStatus { get; set; }
        public string ActionRole { get; set; }
        public string ActionBy { get; set; }
        public string ActionDate { get; set; }
        public string Remarks { get; set; }
    }
}
