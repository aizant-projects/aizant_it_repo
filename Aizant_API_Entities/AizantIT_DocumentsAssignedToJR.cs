//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_DocumentsAssignedToJR
    {
        public int JR_ID { get; set; }
        public int DocumentID { get; set; }
        public int DocVersionID { get; set; }
        public int Status { get; set; }
        public int Hours { get; set; }
        public int Minutes { get; set; }
        public int Seconds { get; set; }
        public Nullable<int> TrainerEvaluation { get; set; }
        public Nullable<System.DateTime> EvaluationDate { get; set; }
        public int ExamType { get; set; }
        public string Opinion { get; set; }
        public string ExplainationStatus { get; set; }
    }
}
