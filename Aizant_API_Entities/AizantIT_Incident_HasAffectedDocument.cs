//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_Incident_HasAffectedDocument
    {
        public int UniquePKID { get; set; }
        public Nullable<int> IncidentMasterID { get; set; }
        public Nullable<int> DeptID { get; set; }
        public Nullable<int> AffectedDocumentID { get; set; }
        public Nullable<int> HasDocRefNo { get; set; }
        public string HasOtherDocName { get; set; }
        public string HasOtherRefNo { get; set; }
        public string PageNo { get; set; }
        public string Comments { get; set; }
        public bool isTemp { get; set; }
    }
}
