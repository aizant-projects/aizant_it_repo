//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_GetATC_HistoryList_Result
    {
        public Nullable<long> RowNumber { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public Nullable<int> ATC_HistoryID { get; set; }
        public string ActionName { get; set; }
        public string ActionByName { get; set; }
        public string RoleName { get; set; }
        public string ActionDate { get; set; }
        public string Remarks { get; set; }
    }
}
