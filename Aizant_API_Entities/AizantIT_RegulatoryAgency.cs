//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_RegulatoryAgency
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AizantIT_RegulatoryAgency()
        {
            this.AizantIT_AuditRegulatorBy = new HashSet<AizantIT_AuditRegulatorBy>();
        }
    
        public int RegulatoryAgencyID { get; set; }
        public string Region { get; set; }
        public bool IsActive { get; set; }
        public string RegulatoryAgencyCode { get; set; }
        public string RegulatoryAgencyName { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AizantIT_AuditRegulatorBy> AizantIT_AuditRegulatorBy { get; set; }
        public virtual AizantIT_EmpMaster AizantIT_EmpMaster { get; set; }
        public virtual AizantIT_EmpMaster AizantIT_EmpMaster1 { get; set; }
    }
}
