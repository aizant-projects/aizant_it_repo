//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_DossierNotifyLink
    {
        public int DossierNotifyLinkID { get; set; }
        public Nullable<int> ProductDossierID { get; set; }
        public Nullable<int> NotificationID { get; set; }
    
        public virtual AizantIT_NotificationMaster AizantIT_NotificationMaster { get; set; }
        public virtual AizantIT_ProductDossier AizantIT_ProductDossier { get; set; }
    }
}
