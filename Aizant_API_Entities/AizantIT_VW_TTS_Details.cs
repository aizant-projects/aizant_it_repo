//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_VW_TTS_Details
    {
        public int TTS_ID { get; set; }
        public int DeptID { get; set; }
        public int DocumentType { get; set; }
        public int DocumentID { get; set; }
        public int TargetType { get; set; }
        public string TypeOfTraining { get; set; }
        public int AuthorBy { get; set; }
        public int ReviewBy { get; set; }
        public int Status { get; set; }
        public Nullable<int> Notification_ID { get; set; }
        public int DocVersionID { get; set; }
        public int ModeOfTraining { get; set; }
        public System.DateTime DueDate { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CompletedStatus { get; set; }
    }
}
