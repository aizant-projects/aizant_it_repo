//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_FeedbackActionHistory
    {
        public int ActionID { get; set; }
        public int BaseType { get; set; }
        public int BaseID { get; set; }
        public string Remarks { get; set; }
        public int ActionBy { get; set; }
        public System.DateTime ActionDate { get; set; }
        public int Status { get; set; }
    }
}
