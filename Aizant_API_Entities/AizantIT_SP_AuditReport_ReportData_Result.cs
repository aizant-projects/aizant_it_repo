//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_AuditReport_ReportData_Result
    {
        public int AuditID { get; set; }
        public string AuditNum { get; set; }
        public string AuditCategory { get; set; }
        public string AuditCategoryType { get; set; }
        public int TypeID { get; set; }
        public string TypeOfAudit { get; set; }
        public string RegulatoryOrClientName { get; set; }
        public string AuditedByTitle { get; set; }
        public string AuditOnDates { get; set; }
        public string ReportedDate { get; set; }
        public string AuditDueDate { get; set; }
        public string BusinessDivision { get; set; }
        public string AuditOnSites { get; set; }
        public string Auditors { get; set; }
        public string Observers { get; set; }
        public string ClientName { get; set; }
        public string Region { get; set; }
        public string RegulatoryAgencyName { get; set; }
        public string NatureOfInspection { get; set; }
        public string AuditCreatedBy { get; set; }
        public string AuditCreatedDate { get; set; }
        public string AuditRefFiles { get; set; }
        public string CertifiedDate { get; set; }
        public string CVD { get; set; }
        public string Status { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<int> ActionStatusID { get; set; }
        public Nullable<int> AuditCurrentStatusID { get; set; }
    }
}
