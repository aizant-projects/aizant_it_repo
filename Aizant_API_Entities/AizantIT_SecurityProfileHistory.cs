//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AizantIT_SecurityProfileHistory
    {
        public int PKID { get; set; }
        public Nullable<int> ActionBY { get; set; }
        public Nullable<System.DateTime> ActionDate { get; set; }
        public int ActionStatusID { get; set; }
        public string SubmittedData { get; set; }
        public string Comments { get; set; }
        public Nullable<int> RoleID { get; set; }
    }
}
