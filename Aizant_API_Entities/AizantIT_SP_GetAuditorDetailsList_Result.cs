//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_GetAuditorDetailsList_Result
    {
        public Nullable<long> RowNumber { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public Nullable<int> AuditorID { get; set; }
        public Nullable<int> RegulatoryAgencyOrClientID { get; set; }
        public string AuditorName { get; set; }
        public string AuditorEmailID { get; set; }
        public string AuditorContactNumber { get; set; }
        public string AuditorCategory { get; set; }
        public string AgencyOrClientName { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedDate { get; set; }
    }
}
