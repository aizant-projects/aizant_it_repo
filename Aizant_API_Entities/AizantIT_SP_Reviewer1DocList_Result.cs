//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_Reviewer1DocList_Result
    {
        public int DocumentID { get; set; }
        public int ProcessTypeID { get; set; }
        public string DocumentNumber { get; set; }
        public string DocumentName { get; set; }
        public string DocumentType { get; set; }
        public string RequestType { get; set; }
        public string Department { get; set; }
        public string Creator { get; set; }
        public string ReviewComments { get; set; }
        public string Initiator { get; set; }
    }
}
