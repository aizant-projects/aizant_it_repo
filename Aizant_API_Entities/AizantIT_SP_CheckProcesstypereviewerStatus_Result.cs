//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aizant_API_Entities
{
    using System;
    
    public partial class AizantIT_SP_CheckProcesstypereviewerStatus_Result
    {
        public int PkID { get; set; }
        public int VersionID { get; set; }
        public int ReviewedByID { get; set; }
        public Nullable<System.DateTime> ReviewedDate { get; set; }
        public string ReviewComments { get; set; }
        public string Status { get; set; }
        public string EmpStatus { get; set; }
        public Nullable<int> PriorityLevel { get; set; }
        public string ReviewedTime { get; set; }
        public Nullable<int> DeptID { get; set; }
        public int isTemp { get; set; }
    }
}
